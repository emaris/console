import react from '@vitejs/plugin-react';
import antdViteImportPlugin from 'antd-vite-import-plugin';
import { FastifyInstance } from 'fastify';
import { readdirSync } from 'fs';
import { cwd } from 'process';
import { ConfigEnv, UserConfig, loadEnv, resolveEnvPrefix } from 'vite';
import checker from 'vite-plugin-checker';

const libs = `${__dirname}/src/lib/`
const proxy = { target: 'https://localhost:8443', changeOrigin: true, secure: false, headers: { "Connection": "keep-alive" } }
const wsproxy = { target: proxy.target, ws: true, changeOrigin: true, secure: false }

const autochunked = ['date-fns', 'react-quill', 'quill', 'react-pdf', 'xlsx', 'react-color']

const base = "/console"

const defaultService = 'ravint'

const services = {

  "ravint": {

    root: 'src/integration/rav/server',
    route: '/ravint',
    tsconfigPath: 'tsconfig.rav.json',

  }
}

type Services = typeof services
type Service = Services[keyof Services]

// forks configuration between SSR builds ( backend only) and non-SSR builds (frontend and backend)
export default (cfg: ConfigEnv) => {


  // loads dev env
  Object.entries(loadEnv("development", cwd(), '')).forEach(e => process.env[e[0]] = e[1])

  const service = process.env.service

  if (cfg.mode === 'production') {
    return cfg.isSsrBuild ? backend(services[service ?? defaultService]) : frontend(cfg)
  }

  return fullstackDev()

}

// resolve fragments in all builds.
const resolve = {

  preserveSymlinks: true,

  alias: [

    { find: /^#(.*)$/, replacement: `${__dirname}/src/$1` },

    ...readdirSync(libs).map(name => ({ find: name, replacement: `${libs}/${name}` })),

  ]
}

const frontendResolve = {

  ...resolve, alias: [...resolve.alias,    
    
    // react-pdf depends on this node lib, which doewsn't appear to be required, so we exclude it. 
    // this can be explicitly polyfilled (crypto-browserify), but this brings along a larger chain of deps
    // which add to the bundle and complicate further the picture.
    { find: "crypto", replacement: "rollup-plugin-node-polyfills/polyfills/empty" }]

  }

const backend = (service: Service): UserConfig => ({

  resolve,

  ssr: {
    noExternal: [/.*/],       // bundles all imports  (better than 'true', which captures also builtins),
    external: []              // add here...
  },

  build: {

    copyPublicDir: false,
    sourcemap: true,
    target: "es2022",
    rollupOptions: {
       // bundling root.
      input: `${service.root}/main.ts`
    }
  }

  , plugins: [

    // typechecks asynchronously like in dev mode.
    checker({

      typescript: {
        // tsconfig narrows typechecking to backend code only.     
        tsconfigPath: service.tsconfigPath,
      }
    })
  ]


})

const frontend = (cfg: ConfigEnv) => {

  const config: UserConfig = {
    
    base,
    
    server: {
      host: true
    }


    ,

    resolve: frontendResolve

    ,

    build: {

      // addresses the mixed use of `require` and `import` in some antd3 modules.
      // vite seems to deal with require, but in mixed mode it needs this.
      commonjsOptions: {
        transformMixedEsModules: true
      },


      sourcemap: true,

      rollupOptions: {

        output: {

          manualChunks: id => {

            if (autochunked.some(c => id.includes(c)))
              return

            if (id.includes('nats.ws'))
              return 'natsws'

            if (id.includes('node_modules'))
              return 'vendor'

          }

        }
      }

    }


    , plugins: [


      antdViteImportPlugin(),

      // typechecks async with tsconfig that narrows to frontend code only.
      checker({

        typescript: {
          tsconfigPath: './tsconfig.client.json',
        }
      })
    ]

    ,

    define: {
      global: 'window',
      "process.browser": true,
    }
  }

  return config

}



const fullstackDev = () => {

  //loads dev.env
  Object.entries(loadEnv("development", cwd(), '')).forEach(e => process.env[e[0]] = e[1])

  return {

    base,

    resolve: frontendResolve,

    server: {

      host: true,
      port: 3000,

      proxy: {
        '/console/config.json': proxy,
        '/domain': proxy,
        '/ravint': proxy,
        '/admin': proxy,
        '/testmail': proxy,
        '/oauth2': proxy,
        '/export': proxy,
        '/event': wsproxy
      },

    },



    plugins: [

      { // inline plugin: integrats Fastify as middleware for full stack development with fullstack HRM

        name: 'apprise-fastify-fullstack',

        // filter out backend modules, only client modules trigger a page reload. 
        handleHotUpdate: ctx => ctx.modules.filter(m => m.clientImportedModules.size),

        // starts/stops api server as middleware, reloads 
        configureServer: async server => {

          let serviceInstances: Record<string, FastifyInstance> = {} // store to share between start and close.

          const load = async (name: string) => serviceInstances[name] = (await server.ssrLoadModule(`${services[name].root}/main.ts`)).default

          server.httpServer!.once('listening', () => Object.keys(services).forEach(load))                                                               // start api server
          server.httpServer!.once('close', () => Object.values(serviceInstances).forEach(inst => inst.close()))

          Object.entries(services).forEach(([name, service]) =>
            
              server.middlewares.use(service.route, async (req, res) => (await load(name)).routing(req, res))        // route (live reload, but will bundle only what changed)

          )

        }
      },



      react(),

      checker({
        overlay: {
          initialIsOpen: false,
          position: 'br'
        },
        typescript: true,
        eslint: {
          lintCommand: 'eslint "src/**/*{.ts,.tsx}"'
        }
      })
    ]

    ,

    define: {
      global: 'window',
      "process.browser": true,
    }
  }


}