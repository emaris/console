### Fullstack Repo Monolith.

A template for `apprise` repositoroes that co-locate one frontend frontend service and one or more backend services. 

Co-location enables sharing of code and types, but build, deployment, and release cycles remain separate. Dependencies are instead centralised in `package.json`.

At development time, all code runs in the dev server, and it is typechecked and linted asynchronouly and in real-time as customary in Vite. Backend services are registed as middleware of the dev server.  Frontend changes trigger Hot Module Reload (HMR) in the browser, why backend changes trigger efficient Live Reload of the middleware. 

Access to the DB and other in-cluster services must occur under port-forwarding. No communicaton from cluster to backend services is possible, including event delivery.

At build time, bundling and type-checking are limited to individual services. This means a CI pipeline won't incur the delays associated with bulding other services.



### Git Shortcuts

Add these aliases to `<pwd>/.git/config` to publish changes to frontend, backend, or both.

```sh
[alias]
	console-publish = "!f() { \
		if [ \"$1\" = '--stage' ]; then local auto='-o ci.variable="AUTOSTAGE=yes"'; fi; \
		git push; \
		local ts=console-$(date +'%d-%h-%y-%H.%M.%S'); \
		git tag $ts; \
		git push origin $ts $auto;  \
	  }; f"
	ravint-publish = "!f() { \
		if [ \"$1\" = '--stage' ]; then local auto='-o ci.variable="AUTOSTAGE=yes"'; fi; \
		git push; \
		local ts=ravint-$(date +'%d-%h-%y-%H.%M.%S'); \
		git tag $ts; \
		git push origin $ts $auto;  \
	  }; f"
	publish = "!f() { \
		git console-publish; \
		git ravint-publish $@; \
	  }; f"
```

Then use this to publish frontend changes:

```sh
> git console-publish
```

or this to publish RAV integration changes:

```sh
> git ravint-publish
```

or this to publish changes to both frontend and backend:

```sh
> git publish
```

Add `--stage` to any of the above to automatically deliver the changes in staging, e.g.:

```sh
> git console-publish --stage
```