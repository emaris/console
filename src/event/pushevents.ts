import { useAskReloadOnPushChange, useAskReloadOnPushRemove } from '#app/push/constants';
import { PushEventSlot } from '#app/push/module';
import shortid from 'shortid';
import { eventRoute, eventSingular, eventType } from './constants';
import { Event } from './model';
import { useEventStore } from './store';

export type EventChangeEvent = {

    event: Event
    type: 'add' | 'remove' | 'change'

}


export const usePushEventSlot = (): PushEventSlot<EventChangeEvent> => {

    const events = useEventStore()

    const askReloadOnPushChange = useAskReloadOnPushChange()
    const askReloadOnPushRemove = useAskReloadOnPushRemove()

    return {

        onSubcribe: () => {

            console.log("subscribing for event changes...")

            return [{

                topics: [eventType]

                ,

                onEvent: (pushevent: EventChangeEvent, history, location) => {

                    const { event, type } = pushevent

                
                    console.log(`received ${type} event for events ${event.name.en}...`, { event })


                    const currentUrl = `${location.pathname}${location.search}`


                    // note: we can't tell if the user is designing an instance as that route currently doesn't mention the template (its source).
                    // we should change the route for that.
                    const detailOnScreen = currentUrl.includes(event.id)

                    switch (type) {

                        case 'add': {

                            events.setAll([...events.all(), event])

                            break
                        }
                        case 'change': {

                            const change = () => events.setAll(events.all().map(c => c.id === event.id ? event : c))
                            const changeAndRefresh = () => { change(); history.push(currentUrl, shortid()) } // uses location state to send down a signal for remount.

                            if (detailOnScreen)
                                askReloadOnPushChange({ singular: eventSingular, onCancel: change, onOk: changeAndRefresh })

                            else change()

                            break
                        }

                        case 'remove': {

                            const fallbackRoute = eventRoute

                            const remove = () => events.setAll(events.all().filter(r => r.id !== event.id))
                            const leaveAndRemove = () => { history.push(fallbackRoute); remove() }

                            if (detailOnScreen)
                                askReloadOnPushRemove({ singular: eventSingular, onOk: leaveAndRemove })

                            else remove()

                            break
                        }

                    }

                }

            }]
        }

    }
}