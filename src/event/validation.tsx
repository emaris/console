import { useConfig } from '#app/config/state';
import { useT } from '#app/intl/api';
import { useTagStore } from '#app/tag/store';
import { useLogged } from '#app/user/store';
import { check, checkIt, notdefined, requireLanguages, reservedKeyword, uniqueLanguages, withReport } from '#app/utils/validation';
import { eventPlural, eventSingular, eventType } from './constants';
import { Event } from './model';
import { useEventStore } from './store';

export type EventValidation = ReturnType<typeof useEventValidation>


export const useEventValidation = () => {


    const store = useEventStore()

    const t = useT()

    const logged = useLogged()

    const config = useConfig()

    const tags = useTagStore()

    return {


        validateEvent: (edited: Event, initial: Event) => {

            const allevents = store.all()
            const requiredLangs = config.get().intl.required || [];
            const { validateCategories } = tags

            const [singular, plural] = [t(eventSingular).toLowerCase(), t(eventPlural).toLowerCase()]

            return withReport({

                active: checkIt().nowOr(t("common.fields.active.msg"), t("common.fields.active.help", { plural }))

                ,

                type: check(edited.type).with(notdefined(t)).nowOr(t("common.fields.type.msg"), t("event.fields.type.help"))

                ,


                name: check(edited.name).with(reservedKeyword(t)).with(notdefined(t))
                    .with(requireLanguages(requiredLangs, t))
                    .with(uniqueLanguages(allevents.filter(t => t.id !== edited.id).map(t => t.name), t)).nowOr(
                        t("common.fields.name_multi.msg"),
                        t("common.fields.name_multi.help", { plural, requiredLangs })),


                description: check(edited.description).nowOr(
                    t("common.fields.description_multi.msg"),
                    t("common.fields.description_multi.help", { plural }),
                ),

                managed: checkIt().nowOr(
                    t("event.fields.managed.msg"),
                    t("event.fields.managed.help", { plural }),
                ),

                relatable: checkIt().nowOr(
                    t("event.fields.relatable.msg"),
                    t("event.fields.relatable.help", { plural }),
                ),

                cardinality: checkIt().nowOr(
                    t("event.fields.cardinality.msg"),
                    t("event.fields.cardinality.help", { plural }),
                ),

                static: checkIt().nowOr(
                    t("common.fields.protected.msg"),
                    t("common.fields.protected.help", { plural }),
                ),

                managedExpression: checkIt().nowOr(
                    t("event.fields.managed_expr.msg"),
                    t("event.fields.managed_expr.help", { plural }),
                ),


                managedTypes: checkIt().nowOr(
                    t("event.fields.managed_type.msg"),
                    t("event.fields.managed_type.help", { plural }),
                ),


                note: check(edited.properties.note ? edited.properties.note[logged.tenant] : '').nowOr(t("common.fields.note.msg"), t("common.fields.note.help", { singular })),


                ...validateCategories(edited.tags).for(eventType)


            })

        }
    }

}
