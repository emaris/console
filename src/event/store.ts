import { useContext } from 'react';
import { eventRoute } from "./constants";
import { EventContext, EventNext } from './context';
import { Event, EventDto, newEvent, useNoEvent } from "./model";



//eslint-disable-next-line
//export const useEventStore = () => eventstoreapi()

export const useEventStore = () => {

    const state = useContext(EventContext)

    const noEvent = useNoEvent()

    const self = {

    
        all: () => state.get().events.all as EventDto[]

        ,

        allOf: (...types: (string | undefined)[]) => self.all().filter(e => (types ?? []).includes(e.type))

    
        ,

        lookup: (id: string | undefined) => self.all()?.find(e => e.id === id)

        ,

        safeLookup: (id: string | undefined) => self.lookup(id) ?? noEvent.get()

        ,

        setAll: (events: Event[], props?: { noOverwrite: boolean }) => state.set(s => {

            if (s.events.all && props?.noOverwrite)
                return

            s.events.all = events

        })

        ,

        routeTo: (e: Event) => e ? `${eventRoute}/${e.id}` : eventRoute


        ,

        next: () => {
            const next = state.get().events.next || { model: newEvent() }
            return next;
        },

        resetNext: () => {
            self.setNext(undefined)
        }
        ,

        setNext: (t: EventNext | Event | undefined): EventNext | undefined => {
            
            const model = t ? t.hasOwnProperty('model') ? t as EventNext : { model: t as Event } : undefined

            state.set(s => s.events.next = model)
            return model as EventNext

        }


    }

    return self
}