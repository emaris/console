
import { Button } from "#app/components/Button";
import { IdProperty } from "#app/components/IdProperty";
import { NoSuchRoute } from "#app/components/NoSuchRoute";
import { RouteGuard } from "#app/components/RouteGuard";
import { SideList } from "#app/components/SiderList";
import { Paragraph, Text } from "#app/components/Typography";
import { useFormState } from "#app/form/hooks";
import { iamPlural } from "#app/iam/constants";
import { any, specialise } from "#app/iam/model";
import { icns } from "#app/icons";
import { useT } from '#app/intl/api';
import { PredefinedLabel } from '#app/model/constants';
import { useLocale } from "#app/model/hooks";
import { LifecycleSummary } from "#app/model/lifecycle";
import { PushGuard } from '#app/push/PushGuard';
import { Page } from "#app/scaffold/Page";
import { Titlebar } from "#app/scaffold/PageHeader";
import { Sidebar } from "#app/scaffold/Sidebar";
import { Tab } from "#app/scaffold/Tab";
import { Topbar } from "#app/scaffold/Topbar";
import { TagList } from "#app/tag/Label";
import { useLogged } from '#app/user/store';
import { through } from "#app/utils/common";
import { parentIn } from "#app/utils/routes";
import { useLiveAssetGuard } from '#campaign/LiveGuard';
import { useCampaignUsage } from '#campaign/Usage';
import { EventLabel } from '#event/Label';
import { useEventPermissions } from '#event/Permissions';
import { eventActions } from '#event/actions';
import { eventPlural, eventRoute, eventSingular, eventType } from '#event/constants';
import { Event, EventDto, useEventModel } from '#event/model';
import { useEventStore } from '#event/store';
import { useEventValidation } from '#event/validation';
import * as React from "react";
import { useHistory, useLocation, useParams } from "react-router";
import { EventGeneralForm } from './GeneralForm';
import { useEventClient } from './client';



export const EventDetail = () => {

    const { pathname } = useLocation()
    const { id } = useParams<{ id: string }>()

    const usage = useCampaignUsage()
    const events = useEventStore()

    const current = events.lookup(id)

    React.useEffect(() => {
        usage.fetchFor(eventType,id) 
    // eslint-disable-next-line
    },[id])

    if (!current)
        return <NoSuchRoute backTo={parentIn(pathname)} />


    return <PushGuard>
        <InnerEventDetail key={current.id} isNew={false} detail={current} />
    </PushGuard>

 
}

export type LoaderProps = {

    id: string, 
    placeholder?: JSX.Element
    children: (loaded:Event)=>React.ReactElement
}




export const NewEventDetail = () => {

    const events = useEventStore()

    const detail = events.next()

    return <InnerEventDetail isNew={true} detail={detail.model} />

}

const InnerEventDetail = (props: { isNew: boolean, detail: Event }) => {

    const { l } = useLocale()
    const t = useT()

    const history = useHistory()
    const { pathname, search } = useLocation()

    const { isNew, detail } = props

    const singular = t(eventSingular)

    const store = useEventStore()
    const client = useEventClient()
    const model = useEventModel()
    const validation = useEventValidation()
    
    const { manage } = eventActions

    const logged = useLogged()

    const [Permissions, showPermissions] = useEventPermissions()

    const formstate = useFormState<Event>(detail);

    const { edited, initial, reset, dirty } = formstate

    const { liveGuarded, liveGuard, usage } = useLiveAssetGuard({
        type: eventType,
        id: edited.id,
        disabled: dirty,
        singular
    })



    const name = l(edited.name)

    const plural = t(eventPlural).toLowerCase()

    const manageIt = specialise(eventActions.manage, detail.id);


    const isProtected = edited.predefined || edited.guarded

    // -------------- effetcs

    React.useEffect(() => { if (isProtected) history.push(`${pathname}`) },
        //eslint-disable-next-line
        [isProtected]
    )

    // -------------- error reporting

    const report = { profile: validation.validateEvent(edited, initial) }

    const totalErrors = Object.keys(report).reduce((total, key) => total += report[key].errors(), 0)

    // -------------- actions

    const onSave = (event: EventDto) => client.save(event)
        // reset to saved as initial state (or will appear dirty)
        .then(through(saved => reset(saved, false)))
        .then(saved => { store.resetNext(); return saved })
        .then(saved => history.push(`${store.routeTo(saved)}${search}`))

    const onRemove = () => client.remove(detail.id, () => {

        history.push(parentIn(pathname))

    }, usage.isInUse(edited.id))

    const onClone = () => {
        store.setNext({ model: model.clone(edited) })
        history.push(`${parentIn(pathname)}/new`)
    }

    const onAddEvent = () => Promise.resolve(store.resetNext())

    // -------------- action buttons

    const removeBtn = <Button
        icn={icns.remove}
        enabled={logged.can(manageIt) && edited.lifecycle.state === 'inactive'}
        disabled={dirty || isNew || edited.predefined}
        onClick={onRemove}>
        {t("common.buttons.remove")}
    </Button>

    const saveBtn = <Button
        icn={icns.save}
        enabled={dirty}
        disabled={totalErrors > 0}
        dot={totalErrors > 0}
        type="primary"
        onClick={() => onSave(edited)}>
        {t("common.buttons.save")}
    </Button>

    const cloneBtn = <Button
        enabled={logged.can(manage)}
        disabled={isNew || totalErrors > 0}
        icn={icns.clone}
        onClick={onClone}>
        {t("common.buttons.clone")}
    </Button>

    const rightsBtn = <Button
        icn={icns.permissions}
        disabled={isNew || dirty}
        enabledOnReadOnly
        onClick={showPermissions}>
        {t(iamPlural)}
    </Button>


    const revertBtn = <Button
        icn={icns.revert}
        enabled={dirty}
        type="danger"
        onClick={() => reset()}>
        {t("common.buttons.revert")}
    </Button>

    const addEventBtn = <Button
        type="primary"
        icn={icns.add}
        enabledOnReadOnly
        enabled={logged.can(manage)}
        onClick={onAddEvent}
        linkTo={`${eventRoute}/new`} >
        {t("common.buttons.add_one", { singular })}
    </Button>


    const readonly = liveGuarded || !logged.can(manageIt)

    return <Page readOnly={readonly}>

        <Sidebar>
            {saveBtn}
            {revertBtn}
            {cloneBtn}
            {removeBtn}
            {rightsBtn}

            <br />

            <IdProperty id={edited.id} />

            <br />

            <LifecycleSummary {...edited.lifecycle} />

            <br />

            <div className="sidebar-property">
                <Paragraph>
                    {totalErrors > 0 ?

                        <Text type="danger">{t("common.validation.total_error_count", { totalErrors })}</Text>
                        :
                        <Text smaller className="emphasis">{t("common.validation.zero_error_count")}</Text>

                    }
                </Paragraph>
            </div>

            {(logged.can(manage) && !isNew) &&
                <>
                    <div style={{ marginTop: "25px" }}>{addEventBtn}</div>
                </>
            }
            <SideList data={store.all()}
                filterGroup={eventType} filterBy={model.stringify} filterPlaceholder={t("common.components.table.filter_placeholder", { plural })}
                renderData={e => l(e.name)}
                render={e => <EventLabel selected={e.id === edited.id} event={e} />} />


        </Sidebar>

        <Topbar>

            <Titlebar title={name || (name === undefined ? "<new>" : "")}>
                {edited.predefined && <PredefinedLabel />}
                <TagList taglist={edited.tags} />
            </Titlebar>

            {liveGuard}

            {saveBtn}
            {revertBtn}
            {cloneBtn}
            {removeBtn}
            {rightsBtn}

            <Tab default id="general" icon={icns.form} name={t("common.labels.general")} badge={report.profile.errors() > 0} />

        </Topbar>

        <Permissions resourceCentric resourceRange={[edited]} filter={{ resources: [edited.id, any] }} />

        <EventGeneralForm {...formstate} report={report.profile} />

        <RouteGuard when={dirty} onOk={() => store.resetNext()} />

    </Page>

}