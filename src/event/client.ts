import { useT } from '#app/intl/api';
import { useToggleBusy } from "#app/system/api";
import { through } from "#app/utils/common";
import { notify, showAndThrow, useFeedback } from "#app/utils/feedback";
import { usePreload } from 'apprise-frontend-core/client/preload';
import { eventApi, useEventCalls } from "./calls";
import { eventPlural, eventSingular, eventType } from "./constants";
import { completeEventOnAdd, Event } from "./model";
import { useEventStore } from './store';



export const useEventClient = () => {

    const t = useT()
    const fb = useFeedback()
    const toggleBusy = useToggleBusy()

    const store = useEventStore()
    const call = useEventCalls()

    const preload = usePreload()

    const self = {

        areReady: () => !!store.all()

        ,

        isLoaded: (event: Event | undefined) => !!event?.properties?.layout

        ,

        fetchAll: (forceRefresh = false): Promise<Event[]> =>

            self.areReady() && !forceRefresh ? Promise.resolve(store.all())

                :

                toggleBusy(`${eventType}.fetchAll`, t("common.feedback.load", { plural: t(eventPlural).toLowerCase() }))

                    .then(_ => console.log(`fetching events...`))
                    .then(_ => preload.get<Event[]>(eventApi) ?? call.fetchAll())
                    .then(through($ => store.setAll($, { noOverwrite: true })))


                    .catch(e => showAndThrow(e, t("common.calls.fetch_all_error", { plural: t(eventPlural).toLowerCase() })))
                    .finally(() => toggleBusy(`${eventType}.fetchAll`))


        ,

        fetchOne: (id: string): Promise<Event> =>

            toggleBusy(`${eventType}.fetchOne`, t("common.feedback.load_one", { singular: t(eventSingular).toLowerCase() }))

                .then(_ => call.fetchOne(id))
                .then(through(fetched => { if (!fetched) throw Error(`Invalid resource lookup for ${id}, returned undefined.`) }))
                .then(through(fetched => store.setAll(store.all().map(u => u.id === id ? fetched : u))))

                .catch(e => showAndThrow(e, t("common.calls.fetch_one_error", { singular: t(eventSingular).toLowerCase() })))
                .finally(() => toggleBusy(`${eventType}.fetchOne`))

        ,

      
        save: (event: Event): Promise<Event> =>

            toggleBusy(`${eventType}.save`, t("common.feedback.save_changes"))


                .then(() => event.id ?

                    call.update(event)
                        .then(through(saved => store.setAll(store.all().map(r => r.id === event.id ? saved : r))))

                    :

                    call.add(completeEventOnAdd(event))
                        .then(through(saved => store.setAll([saved, ...store.all()])))

                )
                .then(through(() => notify(t('common.feedback.saved'))))

                .catch(e => showAndThrow(e, t("common.calls.save_one_error", { singular: t(eventSingular).toLowerCase() })))
                .finally(() => toggleBusy(`${eventType}.save`))


        ,

        remove: (id: string, onConfirm: () => void, challenge?: boolean) =>

            fb.askConsent({

                title: t('common.consent.remove_one_title', { singular: t(eventSingular).toLowerCase() }),
                content: t('common.consent.remove_one_msg', { singular: t(eventSingular).toLowerCase() }),
                okText: t('common.consent.remove_one_confirm', { singular: t(eventSingular).toLowerCase() }),

                okChallenge: challenge ? t('common.consent.remove_challenge', { singular: t(eventSingular) }) : undefined,

                onOk: () => {


                    toggleBusy(`${eventType}.removeOne`, t("common.feedback.save_changes"))

                        .then(_ => call.delete(id))
                        .then(_ => store.all().filter(u => id !== u.id))
                        .then(store.setAll)
                        .then(_ => notify(t('common.feedback.saved')))
                        .then(_ => onConfirm && onConfirm())

                        .catch(e => showAndThrow(e, t("common.calls.remove_one_error", { singular: t(eventSingular).toLowerCase() })))
                        .finally(() => toggleBusy(`${eventType}.removeOne`))


                }
            }),

        removeAll: (list: string[], onConfirm?: () => void) =>

            fb.askConsent({


                title: t('common.consent.remove_many_title', { count: list.length, plural: t(eventPlural).toLowerCase() }),
                content: t('common.consent.remove_many_msg', { plural: t(eventPlural).toLowerCase() }),
                okText: t('common.consent.remove_many_confirm', { count: list.length, plural: t(eventPlural).toLowerCase() }),

                onOk: () => {


                    toggleBusy(`${eventType}.removeAll`, t("common.feedback.save_changes"))

                        .then(_ => list.forEach(id => call.delete(id)))
                        .then(_ => store.all().filter(u => !list.includes(u.id)))
                        .then(store.setAll)
                        .then(_ => notify(t('common.feedback.saved')))
                        .then(_ => onConfirm && onConfirm())

                        .catch(e => showAndThrow(e, t("common.calls.remove_one_error", { singular: t(eventSingular).toLowerCase() })))
                        .finally(() => toggleBusy(`${eventType}.removeAll`))


                }
            })


    }

    return self
}