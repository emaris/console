import { campaignType } from '#campaign/constants'
import { useT } from '#app/intl/api'
import { Lifecycle, newLifecycle } from "#app/model/lifecycle"
import { MultilangDto, newMultiLang, noMultilang, useCompareMultiLang, useL } from "#app/model/multilang"
import { Module } from "#app/module/model"
import { newTagged, TagExpression, Tagged } from "#app/tag/model"
import { deepclone } from "#app/utils/common"
import shortid from "shortid"
import { eventSingular, eventType } from "./constants"
import { EventSlot } from "./module"
import { useEventStore } from './store'
import { useModuleRegistry } from '#app/module/registry'

export type EventState = 'active' | 'inactive'

type PredefProps = 'guarded' | 'cardinality'

export type EventDto = Tagged & {

    id: string

    guarded?: boolean
    predefined?: boolean

    lifecycle: Lifecycle<EventState>

    name: MultilangDto
    description: MultilangDto

    type: string,
    cardinality: EventCardinality

    managed?: boolean,
    managedTypes?: string[]                 // managed if target has this type (only for metaevents, when type=event)
    managedExpression?: TagExpression,      // managed if target matches the expression

    predefinedProperties: PredefProps[]

    properties: EventProperties
}

export type EventCardinality = { initial: number, max: number | undefined }

export type CardinalityNames = 'any' | 'zeroOrOne' | 'one' | 'atLeastOne'

export const eventcardinalities: { [key in CardinalityNames]: EventCardinality } = {

    any: { initial: 0, max: undefined },
    zeroOrOne: { initial: 0, max: 1 },
    one: { initial: 1, max: 1 },
    atLeastOne: { initial: 1, max: undefined },


}

export type EventProperties = {

    note?: Record<string, string>
    relatable?: boolean

} & { [key: string]: any }

export type Event = EventDto

export const useNoEvent = () => {

    const t = useT()

    return { get: (): Event => ({ ...newEvent(), id: `${newEventId()}-unknown`, type: campaignType, name: noMultilang(t('common.labels.unknown_one', { singular: t(eventSingular) })) }) }

}

export const newEvent = (): Event => ({

    ...newTagged,

    id: undefined!,

    lifecycle: newLifecycle('inactive'),
    type: undefined!,

    cardinality: eventcardinalities["one"],

    name: newMultiLang(),
    description: newMultiLang(),

    predefinedProperties: [],

    properties: { relatable: false }


}) as Event

export const completeEventOnAdd = (event: Event) => ({ ...event, id: newEventId() })

export const newEventId = () => `E-${shortid()}`


export const useEventModel = () => {

    const registry = useModuleRegistry()

    const l = useL()
    const compare = useCompareMultiLang()

    const store = useEventStore()


    const self = {

        stringify: (e: Event | undefined) => e ? `${l(e.name)} ${l(e.description) ?? ''} ${e.type}}` : ''

        ,

        stringifyRef: (r: string | undefined) => self.stringify(store.safeLookup(r))

        ,

        newEvent: (type: string): Event => ({ id: undefined!, type, name: newMultiLang(), description: newMultiLang() }) as any

        ,

        clone: (e: Event): Event => ({ ...deepclone(e), id: undefined!, predefined: false, predefinedProperties: [], lifecycle: newLifecycle(`inactive`) })

        ,

        registeredModules: (): Module[] => { console.log(registry.allWith(eventType)); return registry.allWith(eventType).filter(m => (m[eventType] as EventSlot).enable)}

        ,

        comparator: (o1: Event, o2: Event) => compare(o1.name, o2.name)
    }

    return self
}