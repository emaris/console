import { Module } from "#app/module/model"
import { pushEventType } from '#app/push/constants'
import { eventIcon, eventPlural, eventSingular, eventType } from './constants'
import { usePushEventSlot } from './pushevents'



export const useEventModule = (): Module => {

    const pushEventSlot = usePushEventSlot()

    return {

        icon: eventIcon,
        type: eventType,

        nameSingular: eventSingular,
        namePlural: eventPlural,

        [eventType]: {

            enable: true

        } as EventSlot

        ,


        [pushEventType]: pushEventSlot


    }
}

export type EventSlot = {

    enable: boolean

}