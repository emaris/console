
import { useRenderGuard } from 'apprise-frontend-core/utils/renderguard'
import * as React from "react"
import { useEventClient } from './client'

type Props = React.PropsWithChildren<{

  placeholder?: React.ReactNode

}>


export const EventLoader = (props:Props) => {

    const client = useEventClient()

    const {content} = useRenderGuard({
      when:client.areReady(),
      render:props.children,
      orRun:client.fetchAll,
      andRender:props.placeholder
  
    })
  
    return content 
  
  }