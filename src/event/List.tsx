import { useCampaignUsage } from '#campaign/Usage'
import { eventActions } from "#event/actions"
import { eventPlural, eventSingular, eventType } from '#event/constants'
import { EventLabel, EventTypeLabel } from '#event/Label'
import { Event, useEventModel } from '#event/model'
import { useEventPermissions } from '#event/Permissions'
import { useEventStore } from '#event/store'
import { buildinfo } from '#app/buildinfo'
import { Button } from '#app/components/Button'
import { ListState, useListState } from '#app/components/hooks'
import { Column, VirtualTable } from '#app/components/VirtualTable'
import { iamPlural } from '#app/iam/constants'
import { specialise } from '#app/iam/model'
import { icns } from '#app/icons'
import { useLocale } from '#app/model/hooks'
import { Page } from '#app/scaffold/Page'
import { Titlebar } from '#app/scaffold/PageHeader'
import { Sidebar } from '#app/scaffold/Sidebar'
import { Topbar } from '#app/scaffold/Topbar'
import { TagList } from '#app/tag/Label'
import { useLogged } from '#app/user/store'
import { useBaseFilter } from '#app/utils/filter'
import { useT } from '#app/intl/api'
import { useHistory, useLocation } from 'react-router'
import { useTagStore } from '#app/tag/store'
import { useEventClient } from './client'

export const eventGroup = eventType

export const EventList = () => {

    const t = useT()
    const { l } = useLocale()

    const history = useHistory()
    const { pathname } = useLocation()

    const [singular, plural] = [t(eventSingular), t(eventPlural)]

    const liststate: ListState<Event> = useListState<Event>()

    const store = useEventStore()
    const client = useEventClient()
    const model = useEventModel()
    const tags = useTagStore()

    const { manage } = eventActions

    const logged = useLogged()

    const [Permissions, showPermissions] = useEventPermissions()
    
    const usage = useCampaignUsage()


    // -------------- authz privileges
     const canManage = (r: Event) => logged.can(specialise(eventActions.manage, r.id))
 
    // -------------- actions
    const onRemove = ({id}:Event) => client.remove(
        id,
        () => { 
            
            liststate.setSelected(liststate.selected.filter(l => l.id !== id))
        },
        usage.isInUse(id)
    )

    const onClone = (event: Event) => {
        store.setNext({model: model.clone(event) })
        history.push(`${pathname}/new`)
    }

    const onAddEvent = () => Promise.resolve(store.resetNext())


    // -------------- buttons

    const addBtn =  <Button 
                        type="primary" 
                        icn={icns.add} 
                        enabled={logged.can(manage)} 
                        onClick={onAddEvent}
                        linkTo={`${pathname}/new`}>
                            {t("common.buttons.add_one", { singular })}
                    </Button>

    const openBtn = (event: Event) =>   <Button 
                                            key={1} 
                                            icn={icns.open} 
                                            linkTo={store.routeTo(event)}>
                                                {t("common.buttons.open")}
                                        </Button>

    const cloneBtn = (event:Event)  =>  
        <Button 
            key={3} 
            icn={icns.clone} 
            onClick={()=>onClone(event)}>
                {t("common.buttons.clone")}
        </Button>
    
    const removeBtn = (event: Event) =>
        <Button
            key={2}
            icn={icns.remove}
            enabled={logged.can(manage) && event.lifecycle.state==='inactive'}
            disabled={event.predefined || event.guarded}
            onClick={() => onRemove(event)
            } >{t("common.buttons.remove")}
        </Button>

    const removeAllBtn =    <Button 
                                key={3} 
                                type="danger" 
                                disabled={liststate.selected.length < 1}
                                onClick={() => client.removeAll(liststate.selected.map(l => l.id), () => liststate.setSelected([]))} >
                                    <span style={{fontVariantCaps:"all-small-caps"}}>(DEV) </span>{t("common.labels.remove_all", { count: liststate.selected.length })}
                            </Button>

    const rightsBtn =   <Button 
                            enabled={liststate.selected.length > 0} 
                            icn={icns.permissions} 
                            style={{ marginTop: 30 }} 
                            onClick={showPermissions}>
                                {t(iamPlural)}
                        </Button>

    
    const unfilteredData = store.all() 

    const {BaseFilter,baseFilteredData} = useBaseFilter({
        data: unfilteredData,
        readonlyUnless: canManage,
        group: eventGroup
    })

    const data = baseFilteredData

    return <Page>

        <Sidebar>
            {addBtn}
            {rightsBtn}

            {buildinfo.development &&
                <>
                    <br />
                    {removeAllBtn}
                </>
            }
        </Sidebar>

        <Topbar>
            <Titlebar title={ plural } />

            {addBtn}
            {rightsBtn}

        </Topbar>
        <VirtualTable data={data} total={unfilteredData.length} rowKey="id" state={liststate} selectable={buildinfo.development}
           
            filterGroup={eventGroup} filterBy={model.stringify} filterPlaceholder={t("common.components.table.filter_placeholder", { plural: plural.toLowerCase() })}
            
            sortBy={[['type', 'asc'], ['name', 'asc']]}
            actions={event => [openBtn(event), removeBtn(event), cloneBtn(event)]}
            
            filters={[BaseFilter]}
            onDoubleClick={e => history.push(store.routeTo(e))} >

            <Column<Event> flexGrow={1} title={t("common.fields.name_multi.name")} dataKey="name" dataGetter={e => l(e.name)}
                cellRenderer={({ rowData: e }) => <EventLabel event={e} readonly={!canManage(e)} />} />

            <Column<Event> flexGrow={1} width={150} title={t("common.fields.type.name")} dataKey="type" dataGetter={e => e.type} cellRenderer={cell => <EventTypeLabel type={cell.rowData?.type} />} />

            {tags.allTagsOf(eventType).length > 0 &&
                <Column sortable={false} flexGrow={2} title={t("common.fields.tags.name")} dataKey="tags" dataGetter={(e: Event) => <TagList taglist={e.tags} />} />
            }

        </VirtualTable>

        <Permissions resourceRange={liststate.selected} />

    </Page>

}