import { useCalls } from "#app/call/call";
import { Event } from "./model";

const svc="admin"
export const eventApi="/event"

export const useEventCalls = () => {

    const {at} = useCalls()

    return {

        fetchAll: () => at(eventApi,svc).get<Event[]>()
        
        , 
    
        fetchOne: (id:String) =>  at(`${eventApi}/${id}`,svc).get<Event>()
        
        ,

        add: (event:Event) =>  at(eventApi,svc).post<Event>(event)
        
        ,

        update: (event:Event) => at(`${eventApi}/${event.id}`,svc).put<Event>(event)
        
        , 

        delete: (id:string) : Promise<void> => at(`${eventApi}/${id}`,svc).delete()


    }
}