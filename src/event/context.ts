import { fallbackStateOver, State } from 'apprise-frontend-core/state/api';
import { createContext } from 'react';
import { Event } from './model';

export type EventNext = {
    model: Event
}

export type EventState = {
    events: {
        all: Event[];
        next?: EventNext
    }
};

export const initialEvents: EventState = {
    events: {
        all: undefined!,
        next: undefined
    }
}


export const EventContext = createContext<State<EventState>>(fallbackStateOver(initialEvents))