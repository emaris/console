
import { any } from '#app/iam/model'
import { ChangesProps } from '#app/iam/permission'
import { usePermissionDrawer } from '#app/iam/PermissionForm'
import { ResourceProps, StateProps, SubjectProps } from '#app/iam/PermissionTable'
import { useLocale } from '#app/model/hooks'
import { User } from '#app/user/model'
import { UserPermissions } from '#app/user/UserPermissionTable'
import * as React from 'react'
import { useT } from '#app/intl/api'
import { eventActions } from './actions'
import { eventPlural, eventSingular, eventType } from './constants'
import { EventLabel } from './Label'
import { Event } from './model'
import { useEventStore } from './store'

type PermissionsProps = ChangesProps & Partial<ResourceProps<Event>> & Partial<SubjectProps<User>> & Partial<StateProps<User,Event>> & {
    edited? : User
}

export const EventPermissions =  (props: PermissionsProps) => {

    const t = useT()
    const {l} = useLocale()
    
    const events = useEventStore()
    
    return <UserPermissions {...props}
                                 id="event-permissions" 
                                 resourceSingular={props.resourceSingular || t(eventSingular) }
                                 renderResource={props.renderResource || ((e:Event) => <EventLabel event={e} /> )}
                                 resourcePlural={props.resourcePlural || t(eventPlural) }
                                 resourceText={t=>l(t.name)}
                                 resourceId={props.resourceId || ((e:Event)=>e.id)}
                                 renderResourceOption={props.renderResourceOption || ((e:Event) => <EventLabel noLink event={e} />)} 
                                 resourceRange={props.resourceRange || events.all() } 
                                 resourceType={eventType}

                                 actions={Object.values(eventActions)} />

}


export const useEventPermissions = () => usePermissionDrawer(EventPermissions, { types:[eventType, any] }  )