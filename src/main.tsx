

import { App } from '#app/app';
import { PushEvents } from '#app/push/PushEvents';
import { VersionUpgradeObserver } from '#app/push/appupgrade';
import { useLogged } from '#app/user/store';
import { Preload } from 'apprise-frontend-core/client/preload';
import ReactDOM from 'react-dom';
import ManagementConsole from './console/management';
import ReportingConsole from './console/reporting';


export const Console = () => useLogged().hasNoTenant() ? <ManagementConsole /> : <ReportingConsole />


ReactDOM.render(

    <Preload>
        <App>
            <PushEvents>
                <VersionUpgradeObserver />
                <Console />
            </PushEvents>
        </App>
    </Preload>

    , document.getElementById('root'))


