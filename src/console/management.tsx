
import { LogoutItem } from '#app/call/Logout';
import { useT } from '#app/intl/api';
import { ExternalLink } from "#app/scaffold/ExternalLink";
import { Scaffold } from "#app/scaffold/Scaffold";
import { Section } from "#app/scaffold/Section";
import { Slider } from "#app/scaffold/Slider";
import { tenantPlural, tenantRoute } from "#app/tenant/constants";
import { useTenantStore } from '#app/tenant/store';
import { UserProfile, UserProfileItem, userprofileId } from "#app/user/Profile";
import { Campaigns } from "#campaign/Campaigns";
import { CampaignLoader } from '#campaign/Loader';
import { CampaignUsage } from '#campaign/Usage';
import { campaignIcon, campaignPlural, campaignRoute } from "#campaign/constants";
import { useCampaignStore } from '#campaign/store';
import { AcIcon, McIcon, acRoute, iotcLogo, iotcRoute, mcbanner } from "#constants";
import { Dashboard } from "#dashboard/Dashboard";
import { summaryName, summaryRoute } from "#dashboard/ViewRouter";
import { dashboardIcon, dashboardName, dashboardRoute } from "#dashboard/constants";
import { Events } from '#event/Events';
import { eventIcon, eventPlural, eventRoute } from "#event/constants";
import { useEventStore } from '#event/store';
import { Home } from "#home/Home";
import { homeIcon, homeRoute } from "#home/constants";
import { messagePlural, messageType } from "#messages/constants";
import { Products } from "#product/Products";
import { productIcon, productPlural, productRoute } from "#product/constants";
import { useProductStore } from '#product/store';
import { Requirements } from "#requirement/Requirements";
import { requirementIcon, requirementPlural, requirementRoute } from "#requirement/constants";
import { useRequirementStore } from '#requirement/store';
import "./mcvariables.css";





export default function ManagementConsole() {

    const t = useT()

    const tenantstore = useTenantStore()
    const campaignstore = useCampaignStore()
    const reqstore = useRequirementStore()
    const prodstore = useProductStore()
    const events = useEventStore()

    return <CampaignLoader>
        <CampaignUsage>
            <Scaffold title={t("mc_home.title")} shortTitle={t("mc_home.short_title")} icon={<McIcon color='white' />} banner={mcbanner} >
                <Section title={t("mc_home.name")} icon={homeIcon} path={homeRoute} exact
                    crumbs={{

                        [homeRoute]: { name: "" },
                        [requirementRoute]: { name: t(requirementPlural) },
                        [`${requirementRoute}/new`]: { name: t("common.labels.new") },
                        [`${requirementRoute}/*`]: { resolver: (id: string) => reqstore.lookup(id)?.name },
                        [productRoute]: { name: t(productPlural) },
                        [`${productRoute}/new`]: { name: t("common.labels.new") },
                        [`${productRoute}/*`]: { resolver: (id: string) => prodstore.lookup(id)?.name },
                        [campaignRoute]: { name: t(campaignPlural) },
                        [`${campaignRoute}/new`]: { name: t("common.labels.new") },
                        [`${campaignRoute}/*`]: { resolver: (id: string) => campaignstore.lookup(id)?.name },
                        [eventRoute]: { name: t(eventPlural) },
                        [`${eventRoute}/new`]: { name: t("common.labels.new") },
                        [`${eventRoute}/*`]: { resolver: (id: string) => events.lookup(id)?.name },
                        [`${campaignRoute}/*${requirementRoute}`]: { name: t(requirementPlural) },
                        [`${campaignRoute}/*${productRoute}`]: { name: t(productPlural) },
                        [`${campaignRoute}/*${tenantRoute}`]: { name: t(tenantPlural) },
                        [`${campaignRoute}/*${eventRoute}`]: { name: t(eventPlural) },

                        [dashboardRoute]: { name: t(dashboardName) },
                        [`${dashboardRoute}/*/${summaryRoute}`]: { name: t(summaryName) },
                        [`${dashboardRoute}/*`]: { resolver: (id: string) => campaignstore.lookup(id)?.name },
                        [`${dashboardRoute}/*${requirementRoute}`]: { name: t(requirementPlural) },
                        [`${dashboardRoute}/*${requirementRoute}/*`]: { resolver: (id: string) => reqstore.lookup(id)?.name },
                        [`${dashboardRoute}/*${requirementRoute}/*/${summaryRoute}`]: { name: t(summaryName) },
                        [`${dashboardRoute}/*${requirementRoute}/*/${messageType}`]: { name: t(messagePlural) },
                        [`${dashboardRoute}/*/*/*${requirementRoute}`]: { name: t(requirementPlural) },
                        [`${dashboardRoute}/*/*/*${requirementRoute}/*`]: { resolver: (id: string) => reqstore.lookup(id)?.name },
                        [`${dashboardRoute}/*${productRoute}`]: { name: t(productPlural) },
                        [`${dashboardRoute}/*${productRoute}/*`]: { resolver: (id: string) => prodstore.lookup(id)?.name },
                        [`${dashboardRoute}/*/*/*${productRoute}`]: { name: t(productPlural) },
                        [`${dashboardRoute}/*/*/*${productRoute}/*`]: { resolver: (id: string) => prodstore.lookup(id)?.name },
                        [`${dashboardRoute}/*${productRoute}/*/${summaryRoute}`]: { name: t(summaryName) },
                        [`${dashboardRoute}/*${productRoute}/*/${messageType}`]: { name: t(messagePlural) },
                        [`${dashboardRoute}/*${tenantRoute}`]: { name: t(tenantPlural) },
                        [`${dashboardRoute}/*${tenantRoute}/*`]: { resolver: (id: string) => tenantstore.lookup(id)?.name },
                        [`${dashboardRoute}/*/*/*${tenantRoute}`]: { omit: true },
                        [`${dashboardRoute}/*/*/*${tenantRoute}/*`]: { resolver: (id: string) => tenantstore.lookup(id)?.name },
                        [`${dashboardRoute}/*${tenantRoute}/*/${summaryRoute}`]: { name: t(summaryName) },
                        [`${dashboardRoute}/*${tenantRoute}/*/${messageType}`]: { name: t(messagePlural) },
                        [`${dashboardRoute}/*/${messageType}`]: { name: t(messagePlural) },
                        [`${dashboardRoute}/*${eventRoute}`]: { name: t("dashboard.calendar.name") },
                        [`${dashboardRoute}/*/*/*${eventRoute}`]: { name: t("dashboard.calendar.name") },
                        [`${dashboardRoute}/*/*/*/*/*/latest`]: { name: t("submission.labels.latest") },
                        [`${dashboardRoute}/*/*/*/*/*/*`]: { name: t("submission.labels.old_version") }
                        ,

                    }}>
                    <Home />
                </Section>

                <Section title={t("dashboard.name")} icon={dashboardIcon} path={dashboardRoute}>
                    <Dashboard />
                </Section>

                <Section title={t(campaignPlural)} icon={campaignIcon} path={campaignRoute}>
                    <Campaigns />
                </Section>

                <Section title={t(requirementPlural)} icon={requirementIcon} path={requirementRoute}>
                    <Requirements />
                </Section>

                <Section title={t(productPlural)} icon={productIcon} path={productRoute}>
                    <Products />
                </Section>

                <Section title={t(eventPlural)} icon={eventIcon} path={eventRoute}>
                    <Events />
                </Section>

                {/* <Section title={t(userPlural)} icon={userIcon} path={userRoute}
                    crumbs={{ [userRoute]: { name: t(userPlural) } }} >
                    <Users />
                </Section>

                <Section title={t(tenantPlural)} icon={tenantIcon} path={tenantRoute}
                    crumbs={{
                        [tenantRoute]: { name: t(tenantPlural) },
                        [`${tenantRoute}/*`]: { resolver: tenants.breadcrumbResolver }
                    }}>
                    <Tenants />
                </Section> */}

                <Slider id={userprofileId}>
                    <UserProfile />
                </Slider>

                {UserProfileItem()}
                {LogoutItem()}

                <ExternalLink title={t("links.iotc")} icon={iotcLogo} href={iotcRoute} />
                <ExternalLink title={t("links.ac")} icon={<AcIcon />} href={acRoute} />

            </Scaffold>
        </CampaignUsage>
    </CampaignLoader>

}
