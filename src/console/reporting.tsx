
import { useCampaignStore } from '#campaign/store';
import { acbanner, AcIcon, acRoute, iotcLogo, iotcRoute, McIcon, mcRoute, rcColor, RcIcon } from "#constants";
import { dashboardIcon, dashboardName, dashboardRoute } from "#dashboard/constants";
import { Dashboard } from "#dashboard/Dashboard";
import { summaryName, summaryRoute } from "#dashboard/ViewRouter";
import { eventRoute } from "#event/constants";
import { homeIcon, homeRoute } from "#home/constants";
import { messagePlural, messageType } from "#messages/constants";
import { productPlural, productRoute } from "#product/constants";
import { useProductStore } from '#product/store';
import { requirementPlural, requirementRoute } from "#requirement/constants";
import { useRequirementStore } from '#requirement/store';
import { LogoutItem } from '#app/call/Logout';
import { icns } from "#app/icons";
import { useT } from '#app/intl/api';
import { ExternalLink } from "#app/scaffold/ExternalLink";
import { ButtonItem } from "#app/scaffold/MenuItem";
import { Scaffold } from "#app/scaffold/Scaffold";
import { Section } from "#app/scaffold/Section";
import { Slider } from "#app/scaffold/Slider";
import { tenantRoute } from "#app/tenant/constants";
import { useTenantStore } from '#app/tenant/store';
import { UserProfile, userprofileId, UserProfileItem } from "#app/user/Profile";
import { useLogged } from '#app/user/store';
import React from 'react';
import { HelpDrawer } from './helpsheet';
import "./rcvariables.css";

export default function ReportingConsole() {

    const t = useT()
    const logged = useLogged()

    const [helpDrawer, helpDrawerSet] = React.useState(false)

    const tenantstore = useTenantStore()
    const campaignstore = useCampaignStore()
    const reqstore = useRequirementStore()
    const prodstore = useProductStore()

    return <Scaffold title={t("rc_home.title")} shortTitle={t("rc_home.short_title")} icon={<RcIcon color='white' /> } banner={acbanner} >

        <Section title={t("rc_home.name")} icon={homeIcon} path={homeRoute} showOnSider={false} exact
            crumbs={{

                [homeRoute]: { name: "" },
                [dashboardRoute]: {name : t(dashboardName)},
                [`${dashboardRoute}/*/${summaryRoute}`]: {name : t(summaryName)},

                [`${dashboardRoute}/*`]: {resolver:(id:string) => campaignstore.lookup(id)?.name},
                [`${dashboardRoute}/*${requirementRoute}`]: { omit: true },
                [`${dashboardRoute}/*${requirementRoute}/*`]:{resolver:(id:string) => reqstore.lookup(id)?.name},
                [`${dashboardRoute}/*/*/*${requirementRoute}`]: { name: t(requirementPlural) },
                [`${dashboardRoute}/*/*/*${requirementRoute}/*`]: {resolver:(id:string) => reqstore.lookup(id)?.name},
                [`${dashboardRoute}/*${productRoute}`]: { omit: true },
                [`${dashboardRoute}/*${productRoute}/*`]:{resolver:(id:string) => prodstore.lookup(id)?.name },
                [`${dashboardRoute}/*/*/*${productRoute}`]: { name: t(productPlural) },
                [`${dashboardRoute}/*/*/*${productRoute}/*`]: {resolver:(id:string) => prodstore.lookup(id)?.name },
                [`${dashboardRoute}/*${tenantRoute}`]: { omit: true },
                [`${dashboardRoute}/*${tenantRoute}/*`]: { omit: true },
                [`${dashboardRoute}/*/*/*${tenantRoute}`]: { omit: true },
                [`${dashboardRoute}/*/*/*${tenantRoute}/*`]: { resolver: (id:string) => tenantstore.lookup(id)?.name},
                [`${dashboardRoute}/*${tenantRoute}/*/${summaryRoute}`]: {name : t(summaryName)},
                [`${dashboardRoute}/*${productRoute}/*/${summaryRoute}`]: {name : t(summaryName)},
                [`${dashboardRoute}/*${requirementRoute}/*/${summaryRoute}`]: {name : t(summaryName)},
                [`${dashboardRoute}/*${productRoute}/*/${messageType}`]: {name : t(messagePlural)},
                [`${dashboardRoute}/*${requirementRoute}/*/${messageType}`]: {name : t(messagePlural)},
                [`${dashboardRoute}/*${eventRoute}`]: { name: t("dashboard.calendar.name") },
                [`${dashboardRoute}/*/*/*${eventRoute}`]: { name: t("dashboard.calendar.name") },
                [`${dashboardRoute}/*/${messageType}`]: { name: t(messagePlural) },
                [`${dashboardRoute}/*/*/*/*/*/latest`]: { name: t("submission.labels.latest") },
                [`${dashboardRoute}/*/*/*/*/*/*`]: { name: t("submission.labels.old_version") }


            }}>
            <Dashboard />
        </Section>
        <Section title={t("dashboard.name")} icon={dashboardIcon} path={dashboardRoute}>
            <Dashboard />
        </Section>
        <Slider id={userprofileId}>
            <UserProfile />
        </Slider>
{/* 
        <Slider id={settingsId}>
            <Settings />
        </Slider> */}

        {UserProfileItem()}
        {/* {SettingsItem()} */}
        {LogoutItem()}

        <ButtonItem title={t("help.title")} icon={icns.help()} onClick={()=>helpDrawerSet(!helpDrawer)} style={{color: rcColor}}>
            <HelpDrawer open={helpDrawer} onClose={()=>helpDrawerSet(!helpDrawer)} />
        </ButtonItem>
        <ExternalLink title={t("links.iotc")} icon={iotcLogo} href={iotcRoute} />

        <ExternalLink enabled={logged.hasNoTenant()} title={t("links.mc")} icon={<McIcon />} href={mcRoute} />
        <ExternalLink enabled={logged.managesSomeTenant()} title={t("links.ac")} icon={<AcIcon />} href={acRoute} />

    </Scaffold>

}
