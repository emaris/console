import { Icon } from 'antd'
import { Button } from '#app/components/Button'
import { Drawer } from '#app/components/Drawer'
import { Paragraph } from '#app/components/Typography'
import { icns } from '#app/icons'
import { messageIcon, messageType } from '#messages/constants'
import { useCurrentCampaign } from '#campaign/hooks'
import { SubmissionReactContext } from '#campaign/submission/context'
import { useCurrentAsset, useCurrentParty, useCurrentDashboard } from '#dashboard/hooks'
import { useContext } from 'react'
import { useT } from '#app/intl/api'
import * as goicns from 'react-icons/go'
import "./styles.scss";

export const HelpDrawer = (props: { open: boolean, onClose: () => void }) => {

    const campaign = useCurrentCampaign()

    return campaign ? <InCampaignHelpDrawer {...props} /> : <Inner {...props} />
}


const InCampaignHelpDrawer = (props: { open: boolean, onClose: () => void }) => {

    const dashboard = useCurrentDashboard()

    const { party } = useCurrentParty()
    const { asset } = useCurrentAsset()

    const submission = useContext(SubmissionReactContext)?.submission

    const messageRoute = party ?

        asset && submission?.trail ?
            `${dashboard.partyRouteToSubmissionWith(asset.instanceType, asset.source, party.source)}?tab=${messageType}`
            : `${dashboard.routeToParty(party)}/${messageType}`

        : undefined

    return <Inner messageRoute={messageRoute} {...props} />
}


const Inner = (props: { open: boolean, messageRoute?: string, onClose: () => void }) => {

    const { open, onClose, messageRoute } = props

    const t = useT()

    return <Drawer icon={icns.help()} title={t("help.title")} width={600} visible={open} closable={true} onClose={onClose}>
        <div className="help-view">
            <Paragraph className="explainer">{t('help.explainer')}</Paragraph>
            <div className='help-actions'>
                <div className='help-blurb'>
                    <div className='blurb-title'>{t("help.guides.title")}</div>
                    <div className='blurb-explainer'>{t("help.guides.explainer")}</div>
                    <Button enabledOnReadOnly icn={<Icon component={goicns.GoLinkExternal} />} iconLeft type='primary' target="_blank" className='blurb-action' href={t("help.guides.action")}>{t("help.guides.action_title")}</Button>
                </div>
                <div className='help-blurb'>
                    <div className='blurb-title'>{t("help.general.title")}</div>
                    <div className='blurb-explainer'>{t("help.general.explainer")}</div>
                    <Button enabledOnReadOnly icn={icns.mail} iconLeft type='primary' className='blurb-action' href={t("help.general.action")}>{t("help.general.action_title")}</Button>
                </div>
                <div className='help-blurb'>
                    <div className='blurb-title'>{t("help.technical.title")}</div>
                    <div className='blurb-explainer'>{t("help.technical.explainer")}</div>
                    <Button enabledOnReadOnly icn={icns.mail} iconLeft type='primary' className='blurb-action' href={t("help.technical.action")}>{t("help.technical.action_title")}</Button>
                </div>
                <div className='help-blurb'>
                    <div className='blurb-title'>{t("help.campaign.title")}</div>
                    <div className='blurb-explainer'>{t("help.campaign.explainer")}</div>
                    <Button enabledOnReadOnly icn={messageIcon} iconLeft type='primary' className='blurb-action' linkTo={messageRoute} disabled={!messageRoute}>{t("help.campaign.action_title")}</Button>
                </div>
            </div>
        </div>
    </Drawer>
}

