
import { Bytestream, Document } from "#app/stream/model"
import { deepclone } from "#app/utils/common"
import { Changelog } from '#campaign/submission/changelog'
import { Container } from "./components/model"
import { Parameter } from "./parameters/model"




export type Layout = {

    parameters: Parameter[]
    components: Container
    documents?: Record<string,Document[]>
}


export const newLayout : () => Layout = () => ({
    
    parameters:[],
    documents: {},

    
    components : {

        // anything will do here, it's just an adhoc container to 'fit' into recusive algorithm 
        spec:"layout",
        id:"layout",

        children: []
    }

})


export type LayoutInstance = Layout & {

    state: LayoutInstanceState

}

export type LayoutInstanceState = {

    data: LayoutData
    changelog?: Changelog
    resources: {[id:string] : Bytestream[] }
}

export type LayoutData = { [id:string] : any }

//  slightly more fluent to fork processing for instances without using undefined explicitly
export const whenInstance = (layout: Layout) => ({

    do: <T>  ( task: (_: LayoutInstance) => T | undefined ) => {

        return isInstance(layout) ? task(layout) : undefined
    },

    promise: <T>  ( task: (_: LayoutInstance) => T | undefined ) => Promise.resolve(whenInstance(layout).do(task)) 

})

export const isInstance = (layout:Layout) : layout is LayoutInstance => !!(layout as LayoutInstance)?.state

export const instanceFrom = ( layout: Layout) : LayoutInstance => ({...deepclone(layout),state:{data: {}, resources:{}}})

