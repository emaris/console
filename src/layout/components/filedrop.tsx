import { SelectBox } from "#app/form/SelectBox"
import { Switch } from "#app/form/Switch"
import { icns } from "#app/icons"
import { useT } from '#app/intl/api'
import { FileBox, FileBoxProps } from "#app/stream/FileBox"
import { useBytestreams } from "#app/stream/api"
import { FileTypeInfo, fileTypeInfo } from "#app/stream/constants"
import { getHostname } from "#app/utils/common"
import { ChangelogRender } from '#campaign/submission/changerender'
import { useLayoutApis } from '#layout/apicontext'
import { useParameters } from '#layout/parameters/hooks'
import StyledView from '#layout/pdf/styledview'
import { usePdfStyles } from '#layout/pdf/stylehook'
import { alignmentStyleId } from "#layout/style/alignment"
import { useLayoutStyles } from '#layout/style/api'
import { borderStyleId } from "#layout/style/border"
import { heightStyleId } from "#layout/style/height"
import { StyleProps } from "#layout/style/model"
import { spacingStyleId } from "#layout/style/spacing"
import { StyledBox } from "#layout/style/styledbox"
import { textStyleId } from '#layout/style/text'
import { inputWidthStyleId, widthStyleId } from "#layout/style/width"
import { Tooltip } from "antd"
import { useComponentData } from './api'
import { DetailProps } from "./detail"
import { FieldProps, partialField } from "./fielddetail"
import { useFieldHelper } from "./fieldhelper"
import { useFieldReferenceResolver, useLazyFieldReferenceResolver } from './fieldref'
import { useLayoutLabel, useSpecOf } from "./helper"
import { Component, PrintProps, RenderProps, newComponentFrom } from "./model"
import { Bytestream, FileDescriptor } from '#app/stream/model'
import { ReferenceFormFields } from './referenceformfield'
import { SubmissionFileDrop } from './submissionfiledrop'



export type FileDrop = Component & FieldProps & StyleProps & {

    mode: FileBoxProps['mode']
    allowedMimeTypes: FileTypeInfo[]
}


export const useFileDropComponent = () => {

    const id = 'filedrop'

    const referenceresolver = useLazyFieldReferenceResolver<FileDrop, Bytestream[]>()

    return useSpecOf({

        id,
        icon: icns.file,
        name: 'layout.components.filedrop.name',


        styles: current => ({
            [textStyleId]: {},
            // defaults to an absolute scale.
            [inputWidthStyleId]: { default: 400, min: 300, max: 600, percentage: 100, ...current[inputWidthStyleId] },
            // bleeds by default, but supports an absolute scale that trackes the input.
            [widthStyleId]: { default: 405, min: 305, max: 605, percentage: 100, ...current[widthStyleId] },
            [heightStyleId]: {},
            [borderStyleId]: {},
            [alignmentStyleId]: {},
            [spacingStyleId]: { top: 5, bottom: 5, ...current[spacingStyleId] }

        })

        ,

        generate: (): FileDrop => ({
            ...newComponentFrom(id),
            ...partialField(),
            style: {},
            // required: false,
            mode: "multi",
            allowedMimeTypes: []
        })

        ,

        submittedDataOf: (c: SubmissionFileDrop, { resources }) => {

            if (resources)
                return undefined

            const { resolvedResources } = referenceresolver(c)

            return { resources: resolvedResources }
        }
        ,

        staticDataOf: () => ({ files: undefined! })

        ,

        dynamicDataOf: (_, componentstate,) => {

            const hostname = getHostname()

            const files = componentstate.resources?.length > 0 ? componentstate.resources.map(r => `<a href="${hostname}/domain/stream/${r.id}">${r.name}</a>`).join("&nbsp;&nbsp;") : undefined

            return { files }

        }

        ,

        Render

        ,

        Detail

        ,

        Print

    })
}


function Render(props: RenderProps<FileDrop>) {

    const { component } = props


    const { r } = useParameters()

    const styles = useLayoutStyles()

    const data = useComponentData()

    const { settings: { ll, resolveLanguage }, bytestreams } = useLayoutApis()

    const language = resolveLanguage()

    const { resolvedResources: defaultResources } = useFieldReferenceResolver<FileDrop>(component)

    const { mode, placeholder, label } = component

    const dragMsg = placeholder && r(ll(placeholder))
    const lbl = useLayoutLabel(label)

    const resources = data.resourcesFor(component)
    const descriptors = data.descriptorsFor(component) ?? []

    const defaulting = defaultResources && !resources


    const { readonly, readOnlyMode, enabledOnReadonly } = useFieldHelper(component)

    const statusmsg = component.msg ? r(ll(component.msg)) : undefined
    const validation = { msg: statusmsg }

    // removes a resource, and if there are no other resources owned by this subission,
    // it removes the timestamp on the entire upload too..
    const onRemove = (stream: Bytestream) => {

        if (defaulting) {

            const removed = defaultResources.filter(r => r.id !== stream.id).map(resource => ({

                resource,

                descriptor: undefined! as FileDescriptor
            }

            ))

            data.addResourcesAndDataFor(component, removed)

            return

        }


        data.removeResourcesFor(component, [stream])

    }

    const onDrop = (files: File[]) => {


        let resourceAndDescriptors = data.resourcesAndDescriptorsFor(files)



        if (defaulting) {

            resourceAndDescriptors.push(...defaultResources.map(resource => ({

                // we're not changing the submission ref to "co-opt" the resource. 
                // it'd create resurce sharing, and risks to rmeove resources from older campaigns, even archived ones. 
                resource,

                // we  haven't uploaded this resource here, so we blank the descriptor.
                descriptor: undefined! as FileDescriptor
            }

            )))

        }


        data.addResourcesAndDataFor(component, resourceAndDescriptors, undefined)
    }

    const reset = () => {

        if (resources)
            data.removeResourcesAndDataFor(component, resources, undefined)
    }

    const style = styles.resolveCustomStyles(component)

    return <ChangelogRender {...props}>
        <StyledBox component={component}>
            <FileBox style={style} label={lbl} validation={validation}

                dragMsg={dragMsg}

                resources={resources}
                defaultValue={defaultResources}

                readonly={readonly}
                enabledOnReadOnly={enabledOnReadonly}
                readonlyMode={readOnlyMode}

                onRemove={onRemove}
                onDrop={onDrop}
                onReset={reset}

                render={stream => {

                    const match = descriptors.find(d => d.resource === stream.id)


                    return <a download href={match ? URL.createObjectURL(match.file) : bytestreams.linkOf(stream.id)}>{stream.name}</a>

                }}

                mode={mode}
                language={language}
                allowedMimeTypes={component.allowedMimeTypes} />
        </StyledBox>
    </ChangelogRender>
}


function Detail(props: DetailProps<FileDrop>) {

    const t = useT()

    const { component, onChange } = props

    const { mode } = component

    return <>

        <Switch label={t("layout.components.filedrop.multi.name")} onChange={v => onChange({ ...component, mode: v ? "multi" : 'single' })}
            validation={{ msg: t("layout.components.filedrop.multi.msg") }}>
            {mode === 'multi'}
        </Switch>

        <ReferenceFormFields {...props} />

        <SelectBox
            label={t("layout.components.filedrop.type.name")}
            getlbl={m => <FileTypeLabel name={t(m.name)} tooltip={t(m.description)} />}
            selectedKey={component.allowedMimeTypes && component.allowedMimeTypes.map(m => m.name)}
            getkey={m => m.name}
            onChange={a => onChange({ ...component, allowedMimeTypes: a ? a.map(m => fileTypeInfo.filter(ft => ft.name === m)[0]) : [] })}
            validation={{ msg: t("layout.components.filedrop.type.msg") }}
        >
            {fileTypeInfo}
        </SelectBox>
    </>
}

const FileTypeLabel = (props: { name: string, tooltip: string }) => {
    const { name, tooltip } = props

    return <Tooltip title={tooltip}><span>{name}</span></Tooltip>
}


function Print(props: PrintProps<FileDrop>) {

    const { component, wrap = true, View, Link, Text, StyleSheet } = props

    const components = useComponentData()

    const { resolvedResources: defaultResources } = useFieldReferenceResolver<FileDrop>(component)

    const { readonlyFallback } = useFieldHelper(component)

    const bytestreams = useBytestreams()

    const resources = components.resourcesFor(component) ?? defaultResources ?? []

    const fileStyle = StyleSheet.create(usePdfStyles(component, 'fileDrop'))
    //const noFileStyle = StyleSheet.create(usePdfStyles(component, 'fileDropMissing'))

    return <StyledView  {...props} component={component}>

        <View style={fileStyle} wrap={wrap}>

            {
                resources.length === 0 ?

                    <Text>
                        {readonlyFallback}
                    </Text>

                    :

                    resources.map((stream, i) => {
                        const hostname = getHostname()
                        const link = `${hostname}${bytestreams.linkOf(stream.id)}`

                        return <Link key={i} src={link}>{stream.name}</Link>
                    })
            }

        </View>
    </StyledView>


}