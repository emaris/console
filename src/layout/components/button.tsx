import * as React from 'react';
import { useT } from '#app/intl/api';
import { BaseComponentSpec, Component } from './model';

type ButtonProps <I extends Component> =   {

    spec:BaseComponentSpec<I>
}

export const ComponentButton =  <I extends Component> (props:ButtonProps<I>) => {

    const t = useT()
    const {id,name,icon} = props.spec

    return  <div className={`component-button sidebar-title ${id}`} >
                <div style={{flexGrow: 1}}>{t(name)}</div>
                <div>{typeof icon === 'function' ? icon() : icon}</div>
            </div>
}