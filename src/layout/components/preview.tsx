import { Button } from '#app/components/Button';
import { icns } from '#app/icons';
import { useT } from '#app/intl/api';
import { useLocale } from '#app/model/hooks';
import { useLayoutApis } from '#layout/apicontext';
import * as React from 'react';
import { Link } from 'react-router-dom';
import { useLayout } from '../context';
import { isInstance } from '../model';
import { ParametricText } from '../parameters/parametricboxes';
import { anchorIcon } from './anchor';
import { isAnchorable } from './anchorable';
import { PreviewContext } from './context';
import { ComponentDetail, useComponentDetail } from './detail';
import { embeddingIcon, isEmbeddable } from './embedding';
import { isField } from './fielddetail';
import { Container, PreviewProps, isContainer } from "./model";
import { LayoutScrollSubscriber } from './scroll';
import "./styles.scss";


export const ComponentPreview = (props: PreviewProps) => {
    const preview = <InnerComponentPreview {...props} />

    return isContainer(props.component) ? preview :

        <LayoutScrollSubscriber component={props.component}>
            {preview}
        </LayoutScrollSubscriber>

}

export type PreviewContextState = {

    openOnDoubleClick?: boolean
}

export const InnerComponentPreview = (props: PreviewProps) => {

    const { path, component, children } = props

    const t = useT()
    const { l } = useLocale()

    const { config, settings, components } = useLayoutApis()

    const { allCollapsed, allExpanded, getCollapsedStateForComponent, collapseComponent, expandComponent } = settings

    const { componentSpecOf, changeComponent, removeComponent, cloneComponent } = components

    const layout = useLayout()

    const [show, visible, route] = useComponentDetail(component.id)

    const componentCollapsedState = getCollapsedStateForComponent(component)

    // cascades global collapse as it will last only for one render.
    // why only one render? because then individual components can be explanded/collapsed without global re-toggle.
    React.useEffect(() => {
        allCollapsed && collapseComponent(component)
        //eslint-disable-next-line
    }, [allCollapsed])

    React.useEffect(() => {
        allExpanded && expandComponent(component)
        //eslint-disable-next-line
    }, [allExpanded])


    const spec = componentSpecOf(component)
    const name = t(spec.name).toLowerCase()

    const onComponentCollapse = collapsed => collapsed ? expandComponent(component) : collapseComponent(component)

    const onExpandChildren = () => (component as Container).children.forEach(expandComponent)
    const onCollapseChildren = () => (component as Container).children.forEach(collapseComponent)

    const collapseIcon = componentCollapsedState ? icns.collapse : icns.expand

    const areAllContainerChildrenCollapsed = (component as Container).children ? !(component as Container).children.map(getCollapsedStateForComponent).some(s => !s) : false

    const collapseAllIcon = areAllContainerChildrenCollapsed ? icns.collapseAll : icns.expandAll
    const collapseAllCallback = areAllContainerChildrenCollapsed ? onExpandChildren : onCollapseChildren

    const collapseTooltip = componentCollapsedState ? t('layout.buttons.expand.tooltip') : t('layout.buttons.collapse.tooltip')
    const collapseAllTooltip = areAllContainerChildrenCollapsed ? t('layout.buttons.expand_all.tooltip') : t('layout.buttons.collapse_all.tooltip')

    const onClone = () => {
        const cloned = cloneComponent(path)

        if (componentCollapsedState) collapseComponent(cloned)

        if (areAllContainerChildrenCollapsed) (cloned as Container).children.forEach(collapseComponent)

    }

    const openBtn = <Button size="large" light enabledOnReadOnly icn={icns.edit} onClick={show} tooltip={t('layout.buttons.open.tooltip', { name })} />
    const removeBtn = <Button size="large" light icn={icns.removeEntry} onClick={() => removeComponent(path)} tooltip={t('layout.buttons.remove.tooltip', { name })} />
    const collapseBtn = <Button size="large" light icn={collapseIcon} onClick={() => onComponentCollapse(componentCollapsedState)} tooltip={collapseTooltip} />
    const collapseAllBtn = <Button size="large" light icn={collapseAllIcon} onClick={collapseAllCallback} tooltip={collapseAllTooltip} />
    const cloneBtn = <Button size="large" light icn={icns.clone} onClick={onClone} tooltip={t('layout.buttons.clone.tooltip', { name })} />


    const commontitle = component.shortname ?

        <ParametricText text={component.shortname} />

        : (isField(component) || isContainer(component)) && config.exportsData ?

            <div style={{ display: 'flex' }}>
                <ParametricText text={t(l(spec.nameOf(component)))} />
                <Link className="add-name-link" to={route}>{t('layout.components.common.set_name')}...</Link>
            </div> :

            <ParametricText text={t(l(spec.nameOf(component)))} />

    const title = spec.Title ? <spec.Title component={component} /> : commontitle

  
    const componentCollapsed = componentCollapsedState && !isInstance(layout)


    const anchorable = isAnchorable(component) && component.anchor?.enabled
    const embeddable = isEmbeddable(component)

  
    const context = React.useContext(PreviewContext)

    if (spec.active === false)
        return <React.Fragment />


    const onDoubleClick = context?.openOnDoubleClick ? (e: React.MouseEvent<HTMLDivElement>) => { show(); e.stopPropagation() } : undefined


    return <div className={`component-preview ${spec.id}`} onDoubleClick={onDoubleClick} >

        {isInstance(layout) ||
            <div className="component-preview-bar" >
                <div className="bar-title-icon">{typeof spec.icon === 'function' ? spec.icon(component) : spec.icon}</div>
                <div className="bar-title">
                    <div className="bar-title-text">{title}</div>
                    {anchorable && <div className='bar-title-decoration'>{anchorIcon}</div>}
                    {embeddable && <div className='bar-title-decoration'>{embeddingIcon}</div>}

                </div>
                <div className="bar-controls">
                    {openBtn}
                    {collapseBtn}
                    {isContainer(component) && collapseAllBtn}
                    {cloneBtn}
                    {removeBtn}
                </div>
            </div>}

        {children &&

            <div className={`component-preview-contents ${componentCollapsed ? "collapsed" : ''}`}>
                {!componentCollapsed && children}
            </div>
        }

        {visible && <ComponentDetail component={component} onChange={component => changeComponent(path, component)} />}

    </div>




}