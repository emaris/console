import { State } from 'apprise-frontend-core/state/api'
import { createContext } from 'react'
import { AugmentedNamedAsset } from './assetsection'
import { LayoutConditions } from './conditions'
import { PreviewContextState } from './preview'
import { defaultSectionData, SectionData } from './section'



export const LayoutLoopContext = createContext<State<LoopState>>(undefined!)

export type LoopState = {

    [key: string] : string[]
}

export const initialLooplState: LoopState = {}



export const LayoutConditionContext = createContext<State<ConditionContextState>>(undefined!)



export type ConditionContextState = { 

    [name: string]: {
        conditions:  LayoutConditions
    }
}

export const initialConditionalState: ConditionContextState = {}



export const LayoutAssetReferenceContext =  createContext<AugmentedNamedAsset[]>([])


export const PreviewContext = createContext<PreviewContextState>({openOnDoubleClick:true})

// references without sections are automatically enabled.
export const SectionDataContext = createContext<SectionData>({...defaultSectionData(),referencesEnabled:true})