import { useT } from '#app/intl/api'
import { MultilangDto } from '#app/model/multilang'
import { useLayoutApis } from '#layout/apicontext'
import { pdfHelper } from '#layout/pdf/helper'
import StyledView from '#layout/pdf/styledview'
import { usePdfStyles } from '#layout/pdf/stylehook'
import { useLayoutSettings } from '#layout/settings/api'
import { alignmentStyleId } from '#layout/style/alignment'
import { backgroundStyleId } from '#layout/style/background'
import { borderStyleId } from '#layout/style/border'
import { heightStyleId } from '#layout/style/height'
import { StyleProps } from '#layout/style/model'
import { spacingStyleId } from '#layout/style/spacing'
import { StyledBox } from '#layout/style/styledbox'
import { textStyleId } from '#layout/style/text'
import { widthStyleId } from '#layout/style/width'
import { Icon } from 'antd'
import { Fragment } from 'react'
import { FaFont } from 'react-icons/fa'
import { useParameters } from '../parameters/hooks'
import { ParametricMultiTextBox, ParametricText } from '../parameters/parametricboxes'
import { useAnchorComponent } from './anchor'
import { Anchorable, AnchorableFields, RenderAnchorable } from './anchorable'
import { DetailProps } from './detail'
import { useSpecOf } from './helper'
import { Component, OffscreenProps, PrintProps, RenderProps, newComponentFrom } from './model'



export type Title = Component & StyleProps & Anchorable & {

    text: MultilangDto
}


export const useTitleComponent = () => {

    const id = 'title'
    const name = 'layout.components.title.name'

    const anchor = useAnchorComponent()

    return useSpecOf<Title>({

        id,
        icon: <Icon component={FaFont} />,
        name

        ,


        styles: (map) => ({

            [textStyleId]: { color: map[textStyleId]?.color, sizeBaseline: 36 },
            [backgroundStyleId]: {},
            [widthStyleId]: {},
            [heightStyleId]: {},
            [borderStyleId]: {},
            [alignmentStyleId]: {},
            [spacingStyleId]: { bottom: map[spacingStyleId]?.bottom ?? 5 }
        })


        ,

        generate: (): Title => ({

            ...newComponentFrom(id),

            style: {},

            text: { en: undefined! },

            anchor: {
                enabled: false,
                ...anchor.generate()
            }

        }),

        Offscreen,

        Render,

        Detail,

        Print

    })
}

function Offscreen(props: OffscreenProps<Title>) {

    const { ll } = useLayoutSettings()

    const { component } = props

    return <Fragment>{ll(component.text)}</Fragment>
}

function Render(props: RenderProps<Title>) {

    const { styles, settings } = useLayoutApis()

    const parameters = useParameters()

    const { component } = props

    const custom = styles.resolveCustomStyles(component)

    return <Fragment>

        <RenderAnchorable component={component} fallback={component.text} />

        <StyledBox component={component}>
            <ParametricText className="title" style={custom} text={parameters.rr(settings.ll(component.text))} />
        </StyledBox>

    </Fragment>
}


function Detail(props: DetailProps<Title>) {

    const t = useT()

    const { component, onChange } = props

    return <Fragment>

        <ParametricMultiTextBox id={component.id} label={t("layout.components.fields.text.name")} onChange={v => onChange({ ...component, text: v })} validation={{ msg: t("layout.components.fields.text.msg") }}>
            {component.text}
        </ParametricMultiTextBox>

        <AnchorableFields {...props} />

    </Fragment>

}

function Print(props: PrintProps<Title>) {

    const { component, wrap = true, View, StyleSheet } = props

    const { settings } = useLayoutApis()

    const { ll } = settings


    const { rr } = useParameters()
    const { convertHtmlToPdfComponents } = pdfHelper(props)

    const titleStyles = { ...StyleSheet.create(usePdfStyles(component, 'title')) }

    return <StyledView {...props} component={component}>
        <View style={titleStyles} wrap={wrap}>{convertHtmlToPdfComponents(rr(ll(component.text)), { noWrap: true })}</View>
    </StyledView>
}