
import { Switch } from '#app/form/Switch';
import { useT } from '#app/intl/api';
import { useLayoutApis, useLiveLayoutApis } from '#layout/apicontext';
import { DetailProps } from "#layout/components/detail";
import { ComponentDraggable, ComponentDroppable } from "#layout/components/dnd";
import { BaseOffscreen, useSpecOf } from "#layout/components/helper";
import { ComponentSpec, Container, PreviewProps, PrintProps, RenderProps, newComponentFrom } from "#layout/components/model";
import { ComponentPreview } from "#layout/components/preview";
import { LayoutAssetContext, useLayout } from '#layout/context';
import { isInstance } from '#layout/model';
import { alignmentStyleId } from "#layout/style/alignment";
import { backgroundStyleId } from "#layout/style/background";
import { borderStyleId } from "#layout/style/border";
import { heightStyleId } from "#layout/style/height";
import { StyleProps } from "#layout/style/model";
import { spacingStyleId } from "#layout/style/spacing";
import { StyledBox } from "#layout/style/styledbox";
import { widthStyleId } from "#layout/style/width";
import { View } from '@react-pdf/renderer';
import { Icon } from 'antd';
import { Fragment, PropsWithChildren, useContext, useEffect, useMemo } from 'react';
import { GoArrowSwitch } from 'react-icons/go';
import { ComponentCustomName } from './componenthelper';
import { ConditionContextPreview, ConditionContextPrint, ConditionContextPrintPreview, ConditionContextRender, conditionContextId, useConditionContext } from './conditioncontext';
import { ConditionFields } from './conditionfields';
import { LayoutConditions, useConditionMatch, useLiveConditionMatch } from './conditions';
import { LayoutConditionContext } from './context';


// return instead of null or Fragment when conditions aren't matched.
// if return a zero-height component, the scroll provider makes us a default of 300px.
export const FalseCondition = () => <div style={{ height:0 }} />

export type ConditionalSection = Container & StyleProps & {

    sharedKey: string
    mode: 'standalone' | 'shared'
    conditions: LayoutConditions

}


export const defaultSharedKey = conditionContextId

const name = 'layout.components.conditionalsection.name'


export const useConditionalSection = (): ComponentSpec<ConditionalSection> => {

    const id = 'conditionalsection'

    return useSpecOf({

        id,
        icon: <Icon component={GoArrowSwitch} />,
        name,

        Title,

        styles: () => ({

            [backgroundStyleId]: {},
            [widthStyleId]: {},
            [heightStyleId]: {},
            [borderStyleId]: {},
            [alignmentStyleId]: {},
            [spacingStyleId]: {}

        }),

        generate: (): ConditionalSection => ({
            ...newComponentFrom(id),
            children: [],
            style: {},
            conditions: [],
            mode: 'standalone',
            sharedKey: defaultSharedKey
        }),

        Offscreen,
        Preview,
        Detail,
        Render,
        Print,
        PrintPreview



    })

}


function Title({ component }: { component: ConditionalSection }) {

    const t = useT()

    const title = component.shortname ? component.shortname : t(name)

    return <span>{title} <span style={{ opacity: .5 }}>[{t(`layout.components.conditionalsection.mode_${component.mode}`)}]</span></span>

}


function Preview(props: PreviewProps<ConditionalSection>) {


    const { component, path } = props

    const is = useRenderHelper(component)

    const layout = useLayout()

    const { components } = useLayoutApis()

    const asset = useContext(LayoutAssetContext)

    const match = useConditionMatch().match(component.conditions)

    const nextPosition = component.children.length

    const childComponents = component.children.map((child, position) => {

        const { Preview } = components.componentSpecOf(child)

        return <Preview key={position} component={child} path={components.extend(path).at(position)} />

    })


    const conditioncontext = useConditionContext()

    if (isInstance(layout) && !is.shared() && !match.givenAsset(asset))
        return <FalseCondition />

    let contents = <Fragment>

        <StyledBox component={component} >

            {match || !isInstance(layout) ? childComponents : null}

        </StyledBox>

       {isInstance(layout) || <ComponentDroppable key={nextPosition} className="component-placeholder" path={components.extend(path).at(nextPosition)} /> }

    </Fragment>


    // if shared and a context is lacking, mount one right now for the children.
    if (is.shared() && !is.inContext()) {

        const initialConditionalState = { [component.sharedKey]: { conditions: component.conditions } }

        contents = <ConditionContextPreview initialState={initialConditionalState} component={{ ...conditioncontext.generate(), children: component.children }} path={path}>
            {contents}
        </ConditionContextPreview >

    }

    return <ComponentDroppable path={path}>
        <ComponentDraggable path={path} component={component}>
            <ComponentPreview path={path} component={component}>
                {contents}
            </ComponentPreview>
        </ComponentDraggable>
    </ComponentDroppable>

}

function Offscreen(props: RenderProps<ConditionalSection>) {

    const { component } = props

    return <BaseRender {...props}>
        <BaseOffscreen component={component} />
    </BaseRender>
}

function Render(props: RenderProps<ConditionalSection>) {

    const { component } = props

    const { components } = useLiveLayoutApis()

    return <BaseRender {...props}>
        <StyledBox component={component}>

            {component.children.map((child, idx) => {

                const { Render } = components.componentSpecOf(child)

                // can't put a scroll subscriber here as children there might be an shared inner that won't render anything at all.
                return <Render key={idx} component={child} />

            })}
        </StyledBox>
    </BaseRender>

}

function BaseRender(props: PropsWithChildren<RenderProps<ConditionalSection>>) {

    const { component } = props

    const { submissionCtx } = useLiveLayoutApis()

    const { submission, asset, campaign } = submissionCtx

    const is = useRenderHelper(component)

    const match = useLiveConditionMatch().match(component.conditions)

    const conditioncontext = useConditionContext()

    // renders nothing in standalone mode without a match.
    if (!is.shared() && !match.givenSubmission({ submission, asset, campaign }))
        return <FalseCondition />



    // if shared and a context is lacking, mount one right now for the children.
    // re-uses condition context renderer, creating a component on the fly and passing this component's children to it. 
    if (is.shared() && !is.inContext()) {

        const initialConditionalState = { [component.sharedKey]: { conditions: component.conditions } }

        return <ConditionContextRender initialState={initialConditionalState} component={{ ...conditioncontext.generate(), children: component.children }} />


    }

    return <Fragment>{props.children}</Fragment>

}

function Detail(props: DetailProps<ConditionalSection>) {

    const { component, onChange } = props

    const t = useT()

    const is = useRenderHelper(component)

    const msg = {

        msg: is.shared() ? is.inContext() ?
            t('layout.components.conditionalsection.mode_ctx')
            :
            t('layout.components.conditionalsection.mode_noctx')
            :
            t(`layout.components.conditionalsection.mode_msg_${component.mode}`)
    }


    const changeConditons = (conditions: LayoutConditions) => onChange({ ...component, conditions })

    return <Fragment>

        <ComponentCustomName {...props} />

        <Switch label={t('layout.components.conditionalsection.mode_lbl')}
            validation={msg}
            onChange={v => onChange({ ...component, mode: v ? 'shared' : 'standalone' })}
        >
            {component.mode === 'shared'}
        </Switch>

        <ConditionFields conditions={component.conditions} onChange={changeConditons} />

    </Fragment>


}

function Print(props: PrintProps<ConditionalSection>) {

    const { component } = props

    const { components, submissionCtx } = useLiveLayoutApis()

    const { submission, asset, campaign } = submissionCtx

    const context = useContext(LayoutConditionContext)

    const shared = component.mode === 'shared'
    const inContext = shared && !!context

    // We can't use effects here because PDF is in a short live render.
    //  This is an hack
    useMemo(() => {
        if (inContext)
            context.set(s => s[component.sharedKey] = { ...s[component.sharedKey], conditions: component.conditions })
    },
        //eslint-disable-next-line    
        [])

    const match = useLiveConditionMatch().match(component.conditions)


    const conditioncontext = useConditionContext()

    // renders nothing in standalone mode without a match.
    if (!shared && !match.givenSubmission({ submission, asset, campaign }))
        return <View />

    // if shared and a context is lacking, mount one right now for the children.
    // re-uses condition context renderer, creating a component on the fly and passing this component's children to it. 
    if (shared && !inContext) {

        const initialConditionalState = { [component.sharedKey]: { conditions: component.conditions } }

        return <ConditionContextPrint initialState={initialConditionalState} {...props} component={{ ...conditioncontext.generate(), children: component.children }} />

    }

    return <View>{component.children.map((child, position) => {

        const { Print } = components.componentSpecOf(child)

        // can't put a scroll subscriber here as children there might be an shared inner that won't render anything at all.
        return <Print key={position} {...props} component={child} />

    })}</View>

}

function PrintPreview(props: PrintProps<ConditionalSection>) {

    const { component } = props

    const is = useRenderHelper(component)

    const layout = useLayout()

    const { components } = useLayoutApis()

    const asset = useContext(LayoutAssetContext)

    const match = useConditionMatch().match(component.conditions)

    const childComponents = component.children.map((child, position) => {

        const { PrintPreview } = components.componentSpecOf(child)

        return <PrintPreview key={position} {...props} component={child} />

    })


    const conditioncontext = useConditionContext()

    if (isInstance(layout) && !is.shared() && !match.givenAsset(asset))
        return <View />


    let contents = <View>{match || !isInstance(layout) ? childComponents : null}</View>


    // if shared and a context is lacking, mount one right now for the children.
    if (is.shared() && !is.inContext()) {

        const initialConditionalState = { [component.sharedKey]: { conditions: component.conditions } }

        contents = <ConditionContextPrintPreview initialState={initialConditionalState} {...props} component={{ ...conditioncontext.generate(), children: component.children }} >
            {contents}
        </ConditionContextPrintPreview >

    }

    return contents


}

const useRenderHelper = (component: ConditionalSection) => {


    const context = useContext(LayoutConditionContext)

    const shared = component.mode === 'shared'
    const inContext = shared && !!context

    useEffect(() => {

        if (inContext)
            // later work: when we collect a "group key" from users, use that as key instead of the conditionSectionId
            context.set(s => s[component.sharedKey] = { ...s[component.sharedKey], conditions: component.conditions })


        // eslint-disable-next-line
    }, [component.conditions])


    return {

        shared: () => shared,

        inContext: () => !!context,

        sharedInContext: () => inContext


    }
}