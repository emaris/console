import { HtmlSnippet } from '#app/components/HtmlSnippet'
import { Label } from '#app/components/Label'
import { useConfig } from '#app/config/state'
import { Item, defaultItemType } from '#app/dnd/model'
import { CheckGroupBox } from '#app/form/CheckBox'
import { Field } from '#app/form/Field'
import { FieldRow } from '#app/form/FieldRow'
import { RadioBox } from '#app/form/RadioBox'
import { SelectBox } from '#app/form/SelectBox'
import { SimpleBox } from '#app/form/SimpleBox'
import { Switch } from '#app/form/Switch'
import { useT } from '#app/intl/api'
import { useIntl } from '#app/intl/store'
import { useLocale } from '#app/model/hooks'
import { MultilangDto, newMultiLang } from '#app/model/multilang'
import { CategoryBox } from '#app/tag/CategoryBox'
import { TagLabel } from '#app/tag/Label'
import { tagIcon } from '#app/tag/constants'
import { Tag, useTagModel } from '#app/tag/model'
import { useTagStore } from '#app/tag/store'
import { stripHtmlTags, stripHtmlTagsForPdf } from '#app/utils/common'
import { Validation, check, notdefined, requireLanguages, uniqueLanguages } from '#app/utils/validation'
import { ChangelogRender } from '#campaign/submission/changerender'
import { useLayoutApis } from '#layout/apicontext'
import { AssetParameter, assetParamId } from '#layout/parameters/asset'
import { baseCheckBoxHeight, baseCheckBoxWidth, basePdfFontSize, pageContentsPdfStylesheets } from '#layout/pdf/constants'
import StyledView from '#layout/pdf/styledview'
import { usePdfStyles } from '#layout/pdf/stylehook'
import { alignmentStyleId } from '#layout/style/alignment'
import { borderStyleId } from '#layout/style/border'
import { heightStyleId } from '#layout/style/height'
import { StyleProps } from '#layout/style/model'
import { spacingStyleId } from '#layout/style/spacing'
import { StyledBox } from '#layout/style/styledbox'
import { textStyleId } from '#layout/style/text'
import { inputWidthStyleId, widthStyleId } from '#layout/style/width'
import { productType } from '#product/constants'
import { Product } from '#product/model'
import { RequirementLabel } from '#requirement/Label'
import { Requirement, RequirementDto } from '#requirement/model'
import { Icon, Radio } from 'antd'
import { useLanguage } from 'apprise-frontend-core/intl/language'
import { Multilang, useL } from 'apprise-frontend-core/intl/multilang'
import React, { Fragment, useCallback, useRef } from 'react'
import { FaSeedling } from 'react-icons/fa'
import { CardlistBox } from '../../app/components/CardlistBox'
import { useLayoutConfig } from '../context'
import { useLayoutParameters } from '../parameters/api'
import { useParameters } from '../parameters/hooks'
import { ParametricMultiTextBox } from '../parameters/parametricboxes'
import { useComponentData } from './api'
import { DetailProps } from './detail'
import { FieldProps, partialField } from './fielddetail'
import { useFieldHelper } from './fieldhelper'
import { useFieldReferenceResolver, useLazyFieldReferenceResolver } from './fieldref'
import { useLayoutLabel, useSpecOf } from './helper'
import { Component, PrintProps, RenderProps, nameOf, newComponentFrom } from './model'
import { ReferenceFormFields } from './referenceformfield'
import "./styles.scss"


export type Option = Item & {
    id: string
    name: MultilangDto
    default: boolean
}

export type MultiChoice = Component & FieldProps & StyleProps & {

    link?: {
        requirement: string
        component: string
    }
    options: Option[]
    multi: boolean
    inline: boolean
    orientation: 'vertical' | 'horizontal'

    mode: 'standalone' | 'tags'
    categoryId?: string
    tagsId?: string[]
    tagDisplayType?: 'code' | 'name'

    referenceMap?: Record<string, string[] | undefined>
}

export const useMultiChoiceComponent = () => {

    const id = 'select-box'
    const name = 'layout.components.multichoice.name'

    const config = useConfig()

    const params = useLayoutParameters()

    const tags = useTagStore()

    const referenceresolver = useLazyFieldReferenceResolver()

    return useSpecOf({

        id,
        icon: <Icon component={FaSeedling} />,
        name

        ,

        styles: current => ({

            [textStyleId]: {},
            // defaults to an absolute scale.
            [inputWidthStyleId]: { default: 300, min: 200, max: 500, percentage: 100, ...current[inputWidthStyleId] },
            // bleeds by default, but supports an absolute scale that trackes the input.
            [widthStyleId]: { default: 305, min: 205, max: 505, percentage: 100, ...current[widthStyleId] },
            [heightStyleId]: {},
            [borderStyleId]: {},
            [alignmentStyleId]: {},
            [spacingStyleId]: { top: 5, bottom: 5, ...current[spacingStyleId] }

        })

        ,

        /* 
            
            Exports paths like choice.<lang>
    
        */
        staticDataOf: () => {

            const { languages } = config.get().intl

            const choice = languages.reduce((acc, l) => ({ ...acc, [l]: undefined }), {})

            return { choice, inline: choice }
        }

        ,

        submittedDataOf: (component, { data }) => {


            if (data)
                return undefined

            const { resolved } = referenceresolver(component)

            return { data: resolved }
        }

        ,

        /*
          chains selected options, or default options if no options are selected.
        */
        dynamicDataOf: (component, componentstate, parameters) => {

            const { data } = componentstate

            const langs = config.get().intl.languages

            const defaults = component.options.filter(o => o.default).map(o => o.id)
            const options = component.mode == 'tags' && component.categoryId ? tags.allTagsOfCategory(component.categoryId).map(t => ({ id: t.id, name: t.name })) as Option[] : component.options

            // // concatenates options by language.
            const inline: any = langs.reduce((acc, l) => ({
                ...acc,

                [l]: options.filter(o => (data ?? defaults).includes(o.id)).map(o => `${stripHtmlTags(params.r(o.name[l], parameters))}`).join(' • '),

            }), [])


            const choice = langs.reduce((acc, l) => {

                const filtered = options.filter(o => (data ?? defaults).includes(o.id))

                if (filtered.length === 0) 
                    return acc
                
                return ({
                ...acc,

                [l]: `<ul style="padding-left:15px">${filtered.map(o => `<li>${stripHtmlTags(params.r(o.name[l], parameters))}</li>`).join('')}</ul>`


            })}, {})

            // console.log({componentstate,choice,data,component})

            return { choice, inline }


        }
        ,

        generate: (): MultiChoice => ({
            ...newComponentFrom(id),
            ...partialField(),
            style: {},
            options: [],
            multi: false,
            inline: false,
            mode: 'standalone',
            categoryId: undefined,
            tagsId: undefined,
            tagDisplayType: 'name',
            // required: false,
            orientation: 'vertical'
        })

        ,

        Render

        ,

        Detail

        ,

        Print

    })
}



function Print(props: PrintProps<MultiChoice>) {

    const { component, wrap = true, Canvas, Text, View, StyleSheet } = props

    const { settings: { ll }, styles } = useLayoutApis()
    const langs = useLanguage().all()

    const store = useTagStore()

    const { rr } = useLayoutParameters()

    const { dataFor } = useComponentData()


    // const t = useT()

    const { inline, options: componentoptions, mode, categoryId, tagsId, tagDisplayType } = component

    const mapToOption = (tag: string | Tag): Option => {

        const computed = typeof tag === 'string' ? store.lookupTag(tag) : tag

        return tagDisplayType === 'name' ?
            { default: false, name: computed.name, type: 'tag', id: computed.id }
            :
            { default: false, name: langs.reduce((acc, cur) => ({ ...acc, [cur]: computed.code }), {} as Multilang), type: 'tag', id: computed.id }


    }

    const trasformTagToOption = (): Option[] =>
        categoryId ? tagsId ? tagsId.map(mapToOption) : store.allTagsOfCategory(categoryId).map(mapToOption) : []


    const { readonlyFallback } = useFieldHelper(component)

    const computedoptions = mode === 'tags' ? trasformTagToOption() : componentoptions

    const { target } = useMultichoiceLink(component)

    const options = target ? target.component.options : computedoptions

    const inlineProps = options.map(o => ({ label: rr(ll(o.name)), value: o.id }))

    const defaults = component.options.filter(o => o.default).map(o => o.id)

    const { resolved: defaultValue } = useFieldReferenceResolver<MultiChoice>(component)


    const data = target?.data ?? dataFor(component) ?? defaultValue ?? defaults


    const multichoiceStyles = StyleSheet.create(usePdfStyles(component))

    const pageStyles = StyleSheet.create(pageContentsPdfStylesheets)

    const textStyles = styles.resolvePdfStyle(component, s => s.id === textStyleId) ?? { fontSize: basePdfFontSize }

    const baseColor = '#000'
    const color = textStyles.color ?? baseColor

    const baseline = Number(`${textStyles.fontSize}`.replace('px', ''))

    const baseCheckBoxSize = 12

    const checkBoxSize = (baseline * baseCheckBoxSize) / basePdfFontSize
    const checkBoxWidth = (baseline * baseCheckBoxWidth) / basePdfFontSize
    const checkBoxHeight = (baseline * baseCheckBoxHeight) / basePdfFontSize
    const checkBoxLabelTopPadding = (baseline * 2) / basePdfFontSize

    const checkBoxStyles = { height: checkBoxHeight, width: checkBoxWidth }

    const checkBoxEmpty = <Canvas style={checkBoxStyles} paint={(p) => p.rect(2, 2, checkBoxSize, checkBoxSize).stroke(color)} />
    const checkBoxChecked = <Canvas style={checkBoxStyles} paint={(p) => p.rect(2, 2, checkBoxSize, checkBoxSize).stroke(color).rect(4, 4, checkBoxSize - 4, checkBoxSize - 4).fillAndStroke(color, color)} />
    // const radioEmpty = <Canvas style={pageStyles.checkBox} paint={(p) => p.circle(8,8,5).stroke()} />      
    // const radioChecked = <Canvas style={pageStyles.checkBox} paint={(p) => p.circle(8,8,5).stroke().circle(8,8,3).fillAndStroke('black', '#000')} />

    const defaultOrientation = 'vertical'

    const orientation = component.orientation ?? defaultOrientation

    const orientationStyles = orientation === 'vertical' ? pageStyles.verticalMultichoice : pageStyles.horizontalMultichoice
    const marginStyles = orientation === 'vertical' ? pageStyles.verticalMultichoiceMargins : pageStyles.horizontalMultichoiceMargins
    const horizontalStyles = (orientation === 'horizontal' && inline) ? { width: `${Math.floor(100 / inlineProps.length)}%` } : {}

    const noAnswerStyle = StyleSheet.create(usePdfStyles(component, 'noAnswer'))

    const labelStyles = { ...textStyles, paddingTop: checkBoxLabelTopPadding }

    return <StyledView  {...props} component={component}>
        <View style={{ ...multichoiceStyles.multichoice, ...orientationStyles }} wrap={wrap}>
            {inline ?
                component.multi ?
                    inlineProps.map((p, i) => <View key={i} style={{ ...pageStyles.inline, ...horizontalStyles }}>
                        {(data && data.includes(p.value)) ? checkBoxChecked : checkBoxEmpty}
                        <Text style={{ ...pageStyles.checkBoxLbl, ...marginStyles, ...labelStyles }}>{stripHtmlTagsForPdf(p.label)}</Text>
                    </View>)
                    :
                    inlineProps.map((p, i) => <View key={i} style={{ ...pageStyles.inline, ...horizontalStyles }}>
                        {/* {(data && data.includes(p.value)) ? radioChecked : radioEmpty} */}
                        {(data && data.includes(p.value)) ? checkBoxChecked : checkBoxEmpty}
                        <Text style={{ ...pageStyles.radioLbl, ...marginStyles, ...labelStyles }}>{stripHtmlTagsForPdf(p.label)}</Text>
                    </View>)
                : (data && data.length > 0) ?
                    <Text style={{ ...pageStyles.selectBoxLbl, ...labelStyles }}>
                        {data.map(d => stripHtmlTagsForPdf(rr(ll(options.find(o => d === o.id)!.name)))).join(', ')}
                    </Text>
                    :
                    <Text style={{ ...noAnswerStyle, ...labelStyles }}>
                        {readonlyFallback}
                    </Text>
            }
        </View>
    </StyledView>


}


function Render(props: RenderProps<MultiChoice>) {

    const { component } = props

    const { settings: { ll }, styles } = useLayoutApis()
    const langs = useLanguage().all()

    const tagstore = useTagStore()
    const model = useTagModel()

    const referenceOptionMapper = (targetOptions: string[] | undefined) => targetOptions?.flatMap(o => component.referenceMap?.[o] ?? [o])

    const reference = useFieldReferenceResolver<MultiChoice, string[] | undefined>(component, referenceOptionMapper)

    const { dataFor, changeDataFor } = useComponentData()

    const { r, rr } = useParameters()

    const { inline, label, /* required, */ options: componentoptions, mode, categoryId, tagsId, tagDisplayType } = component

    const { target } = useMultichoiceLink(component)

    // note: for the time being, we do not allow unlocking for multichoices.
    const { readonly, readOnlyMode, canUnlock, enabledOnReadonly, placeholder } = useFieldHelper(component)

    const mapToOption = (tag: string | Tag): Option => {

        const computed = typeof tag === 'string' ? tagstore.lookupTag(tag) : tag

        return tagDisplayType === 'name' ?
            { default: false, name: computed.name, type: 'tag', id: computed.id }
            :
            { default: false, name: langs.reduce((acc, cur) => ({ ...acc, [cur]: computed.code }), {} as Multilang), type: 'tag', id: computed.id }


    }

    const trasformTagToOption = (): Option[] =>
        categoryId ? tagsId ? tagsId.map(mapToOption) : tagstore.allTagsOfCategory(categoryId).sort(model.comparator).map(mapToOption) : []


    const computedoptions = mode === 'tags' ? trasformTagToOption() : componentoptions

    const options = target ? target.component.options : computedoptions

    const defaults = component.options.filter(o => o.default).map(o => o.id)

    const data = target?.data ?? dataFor(component)

    const lbl = useLayoutLabel(label)

    // const validation = multichoicespec.useValidation!(component)


    const optional = !component.multi

    // console.log("multichoice options", options, "readonly", readonly, "state", dataFor(component), "data", data)

    const defaultOrientation = 'vertical'

    const orientation = component.orientation ?? defaultOrientation

   
    const statusmsg = component.msg ? r(ll(component.msg)) : undefined
    const validation = { msg: statusmsg }

    const defaultValue = reference.resolved ?? defaults

    //console.log({resolved: reference.resolved})

    // terrible hack to have defaulting work with optional mode.
    const defaultMode = useRef(!data?.length && !!defaultValue)

    const style = styles.resolveCustomStyles(component)

    const inlineProps = options.map(o => ({ label: <HtmlSnippet stripHtml snippet={rr(ll(o.name))} />, value: o.id }))

    return <ChangelogRender {...props} >
        <StyledBox component={component}>{

            inline ?

                <CheckGroupBox label={lbl} validation={validation}

                    style={style} className={`checkbox-${orientation}`}

                    enabledOnReadOnly={enabledOnReadonly}
                    readonly={readonly}
                    canUnlock={canUnlock}
                    readonlyMode='chrome'   // no content mode for checkboxes.

                    defaultValue={defaultValue}
                    value={data}
                    onChange={(v: any) => {

                        let value = data

                        if (optional && defaultMode.current) {
                            defaultMode.current = false
                            value = defaultValue
                        }

                        changeDataFor(component, optional ? v?.filter(currentVal => !(value ?? []).includes(currentVal)) : v)

                    }}>
                    {inlineProps}
                </CheckGroupBox>

                :

                <SelectBox placeholder={placeholder} label={lbl} validation={validation}

                    style={style}
                    defaultValue={component.multi ? defaultValue : defaultValue?.[0]}
                    onChange={v => changeDataFor(component, component.multi || !v ? v : [v])}

                    multi={component.multi}
                    
                    readonly={readonly}
                    enabledOnReadOnly={enabledOnReadonly}
                    readonlyMode={readOnlyMode}
                    canUnlock={canUnlock}

                    allowClear
                    selectedKey={component.multi ? data ?? [] : data?.[0]}
                    lblText={o => rr(ll(o.name))}
                    getkey={o => o.id}
                    getlbl={o => <HtmlSnippet stripHtml snippet={rr(ll(o.name))} />}>

                    {options}
                </SelectBox>

        }</StyledBox>
    </ChangelogRender>
}


function Detail(props: DetailProps<MultiChoice>) {

    const t = useT()
    const { l } = useLocale()

    const { importsData } = useLayoutConfig()

    const { component, onChange } = props

    const { requiredLanguages } = useIntl()

    function swap(list: Option[], from: number, to: number) {
        if (to === list.length) {
            return list
        }

        [list[from], list[to]] = [list[to], list[from]];

        return list;
    }

    const onChangeMultiMode = (multi : boolean) => {
    
        const referenceMap =  component.multi === multi || !component.referenceMap ? component.referenceMap

        : Object.entries(component.referenceMap).reduce( ( acc,[k,v]) => ({...acc, [k] : k ? multi ? [v] :  v?.[0] : k }),{})

        onChange({ ...component, multi, referenceMap })
    
    }


    const onEditOption = (o: Option) => {
        const options = [...component.options]
        Promise.resolve(options.splice(options.findIndex(op => op.id === o.id), 1, o)).then(_ => setDefault(options, o))
    }

    const setDefault = (options: Option[], o: Option) => {
        const _options = (o.default && !component.multi) ? options.map(o => ({ ...o, default: false })) : options
        _options.splice(_options.findIndex(op => op.id === o.id), 1, o)
        onChange({ ...component, options: _options })
    }

    const move = (from: number, to: number) => onChange({ ...component, options: swap([...component.options], from, to) })

    const add = () => {
        const maxId = component.options.length > 0 ? Math.max(...component.options.map(o => +o.id)) : -1
        const id = `${maxId + 1}`
        const option = { id, type: defaultItemType, name: newMultiLang(), default: false }
        setDefault([...component.options, option], option)
    }

    const remove = index => {
        const options = [...component.options]
        options.splice(index, 1)
        onChange({ ...component, options: options })
    }


    const validation = (o: Option) => check(o.name).with(notdefined(t))
        .with(requireLanguages(requiredLanguages(), t))
        .with(uniqueLanguages(component.options.filter(oo => oo.id !== o.id).map(o => o.name), t))
        .nowOr(t("common.fields.name_multi.msg"))

    const { targets } = useMultichoiceLink(component)

    const linked = !!component?.link?.component

    const DataPicker = <>


        <Switch label={t("layout.components.multichoice.mode.label")} validation={{ msg: t("layout.components.multichoice.mode.msg") }}
            onChange={v => onChange({ ...component, mode: v ? 'tags' : 'standalone', categoryId: undefined, tagsId: undefined, tagDisplayType: 'name', options: [] })}>
            {component.mode === 'tags' ? true : false}
        </Switch>

        {component.mode === 'tags' ?
            <CodelistSelector component={component} onChange={onChange} />
            :
            <CardlistBox disabled={!!component.link?.component} id={component.id} singleLabel={t("common.labels.option")} label={t("layout.components.multichoice.options.name")} onRemove={remove} onAdd={add} onMove={move}>
                {component.options.map((o, i) => <div key={i} className='multichoice-option-draggable'><OptionDetail disabled={linked} key={i} option={o} validation={validation(o)} onEditOption={onEditOption} /></div>)}
            </CardlistBox>

        }

    </>

    const referenceFilter = useCallback((field: MultiChoice) =>

        (!field.multi || component.multi)
        && ((field.mode ?? 'standalone') === (component.mode ?? 'standalone')
            && (field.mode === 'standalone' || field.categoryId === component.categoryId))

        , [component.multi, component.mode, component.categoryId])



    return <>

        <Switch label={t("layout.components.multichoice.multi.name")} validation={{ msg: t("layout.components.multichoice.multi.msg") }}
            onChange={onChangeMultiMode}>
            {component.multi}
        </Switch>

        <ReferenceFormFields {...props} Extension={ReferenceMapBuilder} filter={referenceFilter} />

        <Switch label={t("layout.components.multichoice.inline.name")} validation={{ msg: t("layout.components.multichoice.inline.msg") }}
            onChange={v => onChange({ ...component, inline: v })}>
            {component.inline}
        </Switch>

        <RadioBox disabled={!component.inline}
            label={t("layout.components.multichoice.orientation.name")} validation={{ msg: t("layout.components.multichoice.orientation.msg") }}
            onChange={v => onChange({ ...component, orientation: v })}
            value={component.orientation ?? 'vertical'}>
            <Radio value='vertical' id='vertical'>
                <React.Fragment>{t("layout.components.multichoice.orientation.vertical")}</React.Fragment>
            </Radio>
            <Radio value='horizontal' id='horizontal'>
                <React.Fragment>{t("layout.components.multichoice.orientation.horizontal")}</React.Fragment>
            </Radio>
        </RadioBox>


        {importsData && <SelectBox label={t("layout.components.multichoice.link.name")} validation={{ msg: t("layout.components.multichoice.link.msg") }}

            onChange={targetComponent => {

                const target = targets.find(t => t.component.id === targetComponent)

                return onChange({ ...component, restricted: !!targetComponent, link: targetComponent ? { requirement: target?.asset?.id!, component: targetComponent } : undefined })

            }}

            placeholder={t("layout.components.multichoice.link.placeholder")}
            allowClear={true}
            selectedKey={component.link?.component}
            getkey={target => target.component.id}
            getlbl={target => <RequirementLabel noLink noDecorations
                requirement={target.asset as RequirementDto}
                title={<><span style={{ fontWeight: 500 }}>{nameOf(target.component)}</span> [<span>{l(target.asset?.name)}</span>]</>} />}>
            {targets}
        </SelectBox>}

        {linked || DataPicker}

    </>
}



type OptionDetailProps = {

    option: Option
    validation: Validation
    disabled: boolean

    onEditOption: (o: Option) => void
}

const OptionDetail = (props: OptionDetailProps) => {

    const { option, onEditOption, disabled, validation } = props
    const t = useT()

    return <FieldRow className="multichoice-option">

        {/* key here is fundamental to tell react order has changed. */}
        <ParametricMultiTextBox rte noToolbar disabled={disabled} grouped key={option.id} id={option.id} style={{ marginRight: 15 }}
            label={t("common.fields.name_multi.name")} validation={validation}
            onChange={(t) => onEditOption({ ...option, name: t })} >
            {option.name}
        </ParametricMultiTextBox>

        <Switch disabled={disabled} grouped label={t("common.fields.default.name")} onChange={(t) => onEditOption({ ...option, default: t })}>
            {option.default}
        </Switch>

    </FieldRow>

}


export const useMultichoiceLink = (component: MultiChoice) => {

    const { contextualParameters } = useParameters()

    const { config: { mode }, prodstore, reqstore } = useLayoutApis()


    const assetOf = (p: AssetParameter) => mode === 'live' ? p.value.liveData?.asset : (p.value?.type === productType ? prodstore : reqstore).lookup(p.value?.id)
    const dataOf = (p: AssetParameter, c: Component) => mode === 'live' ? p.value.liveData?.submission?.content.data[c.id] : undefined


    const targets = contextualParameters
        .filter(p => p.spec === assetParamId)
        .map(p => p as AssetParameter)
        .flatMap(p => {

            return p.components.filter(c => c.spec === component.spec)
                .map(c => ({ asset: assetOf(p), component: c as MultiChoice, data: dataOf(p, c) }))

        })


    const target = targets.find(t => t.component.id === component.link?.component)

    return { targets, target }

}

type CodelistSelectorProps = {
    onChange: (_: MultiChoice) => void
    component: MultiChoice
}

const CodelistSelector = (props: CodelistSelectorProps) => {

    const { onChange, component } = props

    const t = useT()
    const store = useTagStore()
    const model = useTagModel()

    const categories = store.allCategories()

    const selectedCategory = component.categoryId
    const selectedTags = component.tagsId


    return <Fragment>

        <CategoryBox label={t('layout.components.multichoice.tag.category_picker_lbl')}
            validation={{ msg: t('layout.components.multichoice.tag.category_picker_msg') }}
            onChange={cat => onChange({ ...component, categoryId: cat, tagsId: undefined })}
            categories={categories}
        >{selectedCategory}</CategoryBox>


        <SelectBox<string> label={t('layout.components.multichoice.tag.tag_picker_lbl')}
            validation={{ msg: t('layout.components.multichoice.tag.tag_picker_msg') }}
            undefinedOption={selectedCategory ? <Label icon={tagIcon} mode='light' noOptions noTip noLink noDecorations title={t('layout.components.multichoice.tag.all_tags')} /> : undefined}
            onChange={tags => onChange({ ...component, tagsId: tags })}
            selectedKey={selectedTags ?? []}
            getlbl={tag => <TagLabel mode='light' noLink tag={tag} />}
            getkey={t => t}
            lblText={t => model.stringify(store.lookupTag(t))}
            disabled={selectedCategory === undefined}
        >{selectedCategory ? store.allTagsOfCategory(selectedCategory).map(t => t.id) : []}</SelectBox>

        <RadioBox
            label={t('layout.components.multichoice.tag.show_mode_lbl')}
            onChange={v => onChange({ ...component, tagDisplayType: v })}
            value={component.tagDisplayType}
            disabled={component.categoryId === undefined}
        >
            <Radio value='name'>{t('layout.components.multichoice.tag.show_mode_name')}</Radio>
            <Radio value='code'>{t('layout.components.multichoice.tag.show_mode_code')}</Radio>
        </RadioBox>

    </Fragment>

}

const ReferenceMapBuilder = (props: DetailProps<MultiChoice> & {

    target: Requirement | Product
    targetField: MultiChoice


}) => {

    const t = useT()
    const l = useL()

    const { onChange, targetField, component } = props

    const { options, referenceMap } = component

    // if using this very asset and this field, then use this version "under edit"
    // so intervening changes to options are reflected immediately in the UI.
    const field = targetField.id === component.id ? component : targetField

    // if using this very asset, excludes options that might have been just added and not yet edited.
    const targetOptions = field.options.filter(o => o.name.en)

    if (!targetOptions.length)
        return null

    const optionIds = options.map(o => o.id)

    return <Field style={{ marginBottom: 15 }}
        label={t('layout.components.multichoice.mapping.lbl')}
        validation={{ msg: t('layout.components.multichoice.mapping.msg') }}>{targetOptions.map(o => {

            return <div key={o.id} className='option-mapping'>
                <div className='mapping-line'>
                    <SimpleBox className='line-lbl' light>
                        {t('layout.components.multichoice.mapping.from')}:
                    </SimpleBox>
                    <SimpleBox className='line-field' light>
                        <HtmlSnippet stripHtml snippet={l(targetOptions.find(oo => oo.id === o.id)?.name)!} />
                    </SimpleBox>
                </div>
                <div className='mapping-line'>
                    <SimpleBox className='line-lbl' light>
                        {t('layout.components.multichoice.mapping.to')}:
                    </SimpleBox>
                    <SelectBox className='line-field' light
                        allowClear
                        multi={component.multi}
                        placeholder={t('layout.components.multichoice.mapping.none')}
                        defaultValue={optionIds.includes(o.id) ? [o.id] : undefined}
                        selectedKey={referenceMap?.[o.id]}
                        lblText={id => l(options.find(o => o.id === id)!.name)!}
                        getlbl={id => <HtmlSnippet stripHtml snippet={l(options.find(o => o.id === id)?.name)!} />}

                        onChange={(ids: string[]) => onChange({ ...component, referenceMap: { ...referenceMap, [o.id]: ids } })}>
                        {optionIds}
                    </SelectBox>
                </div>
            </div>

        })}</Field>

}