import { icns } from '#app/icons'
import { useT } from "#app/intl/api"
import { DetailProps } from "#layout/components/detail"
import { ComponentDraggable, ComponentDroppable } from "#layout/components/dnd"
import { useSpecOf } from "#layout/components/helper"
import { Component, ComponentSpec, Container, PreviewProps, PrintProps, RenderProps, newComponentFrom } from "#layout/components/model"
import { ComponentPreview } from "#layout/components/preview"
import { StateProvider } from 'apprise-frontend-core/state/provider'

import { useLayoutApis } from '#layout/apicontext'
import { useLayout } from '#layout/context'
import { isInstance } from '#layout/model'
import { Fragment } from 'react'
import { ComponentCustomName } from './componenthelper'
import { ConditionContextState, LayoutConditionContext, initialConditionalState } from './context'


export type ConditionContext = Component & Container

export const conditionContextId = 'conditioncontext'


export const useConditionContext = (): ComponentSpec<ConditionContext> => {


    const t = useT()
    const name = t('layout.components.conditioncontext.name')

    return useSpecOf({

        id: conditionContextId,

        icon: icns.rowSection,
        name,
        showTabsOnDetails: true

        ,

        generate: (): ConditionContext => {

            const component = newComponentFrom(conditionContextId)

            return {
                ...component,
                children: []
            }
        }

        ,

        Detail,
        Render: ConditionContextRender,
        Preview,
        Print: ConditionContextPrint,
        PrintPreview: ConditionContextPrintPreview

    })

}


function Preview(props: PreviewProps<ConditionContext>) {

    const { component, path } = props

    const { components } = useLayoutApis()

    const layout = useLayout()
    
    const nextPosition = component.children.length

    return <ComponentDroppable path={path}>

        <ComponentDraggable path={path} component={component}>
            <ComponentPreview path={path} component={component}>

                {isInstance(layout) || <ComponentDroppable className="component-placeholder" path={components.extend(path).at(0)} />}

                <ConditionContextPreview {...props} />

                {isInstance(layout) || <ComponentDroppable key={nextPosition} className="component-placeholder" path={components.extend(path).at(nextPosition)} />}

            </ComponentPreview>
        </ComponentDraggable>
    </ComponentDroppable>

}

export function ConditionContextPreview(props: PreviewProps<ConditionContext> & { initialState?: ConditionContextState }) {

    const { component, path, initialState = initialConditionalState } = props

    const layout = useLayout()

    const { components} = useLayoutApis()

    const { extend, componentSpecOf } = components

    const nextPosition = component.children.length

    return <StateProvider initialState={initialState} context={LayoutConditionContext} >

        {component.children.map((component, idx) => {

            const { Preview } = componentSpecOf(component)

            return <Preview key={component.id} component={component} path={`${path}@${idx}`} />


        })}

        {isInstance(layout) || <ComponentDroppable key={nextPosition} className="component-placeholder" path={extend(path).at(nextPosition)} />}

    </StateProvider>

}

function Detail(props: DetailProps<ConditionContext>) {


    return <Fragment>

        <ComponentCustomName {...props} />

    </Fragment>
}

export function ConditionContextRender(props: RenderProps<ConditionContext> & { initialState?: ConditionContextState }) {

    const { component, initialState = initialConditionalState } = props

    const { components } = useLayoutApis()

    return <StateProvider initialState={initialState} context={LayoutConditionContext} >

        {component.children.map(child => {

            const { Render } = components.componentSpecOf(child)

            return <Render key={child.id} component={child} />
        })}

    </StateProvider>

}

export function ConditionContextPrint(props: PrintProps<ConditionContext> & { initialState?: ConditionContextState }) {

    const { component, initialState = initialConditionalState } = props

    const { components } = useLayoutApis()

    return <StateProvider initialState={initialState} context={LayoutConditionContext} >

        {component.children.map(child => {

            const { Print } = components.componentSpecOf(child)

            return <Print key={child.id} {...props} component={child} />
        })}

    </StateProvider>

}

export function ConditionContextPrintPreview(props: PrintProps<ConditionContext> & { initialState?: ConditionContextState }) {

    const { component, initialState = initialConditionalState } = props

    const { components } = useLayoutApis()

    return <StateProvider initialState={initialState} context={LayoutConditionContext} >

        {component.children.map(child => {

            const { PrintPreview } = components.componentSpecOf(child)

            return <PrintPreview key={child.id} {...props} component={child} />
        })}

    </StateProvider>

}





