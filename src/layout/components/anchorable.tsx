
import { useConfig } from '#app/config/state'
import { Switch } from '#app/form/Switch'
import { useT } from '#app/intl/api'
import { MultilangDto } from '#app/model/multilang'
import { stripHtmlTags } from '#app/utils/common'
import { useLayoutApis } from '#layout/apicontext'
import { useParameters } from '#layout/parameters/hooks'
import { Fragment } from 'react'
import { Anchor, AnchorFields, PreviewAnchor, RenderAnchor, useAnchorComponent } from './anchor'
import { DetailProps } from './detail'
import { Component, RenderProps } from './model'

export type Anchorable = Component & {

    anchor?: Anchor & {

        enabled: boolean

    }
}

export const isAnchorable = (c:Component) : c is Anchorable => !!(c as any).anchor

export const RenderAnchorable = (props: RenderProps<Anchorable> & {

    fallback?: MultilangDto

}) => {

    const { config } = useLayoutApis()

    return config.mode === 'live' ? <RenderAnchorableLive {...props} /> : <RenderAnchorableDesign {...props} />
}

export const InnerRenderAnchorable = (props: RenderProps<Anchorable> & {

    fallback?: MultilangDto

}) => {

    const { component, fallback } = props

    const { config } = useLayoutApis()

    const requiredLangs = useConfig().get().intl.required || [];

    if (!component.anchor?.enabled)
        return null


    const entryOrFallback = requiredLangs.reduce((acc, lang) => ({

        ...acc,

        [lang]: component.anchor?.entry?.[lang] || (fallback?.[lang] && stripHtmlTags(fallback?.[lang]!))

    }), {} as MultilangDto)

    const jitAnchor = { ...component.anchor, entry: entryOrFallback }

    return config.mode === 'live' ? <RenderAnchor component={jitAnchor} /> : <PreviewAnchor component={jitAnchor} />


}

const RenderAnchorableDesign = (props: RenderProps<Anchorable> & {

    fallback?: MultilangDto

}) => {

    const { fallback } = props

    const { parameters } = useLayoutApis()

    return <InnerRenderAnchorable {...props} fallback={parameters.multir(fallback)} />

}

const RenderAnchorableLive = (props: RenderProps<Anchorable> & {

    fallback?: MultilangDto

}) => {

    const { fallback } = props

    const parameters = useParameters()

    return <InnerRenderAnchorable {...props} fallback={parameters.multir(fallback)} />

}

export const AnchorableFields = <C extends Component & Anchorable>(props: DetailProps<C>) => {

    const { component, onChange } = props

    const t = useT()

    const anchor = useAnchorComponent()

    const anchorOrDefault = { ...component.anchor ?? anchor.generate()}

    return <Fragment>

        <Switch label={t('layout.components.anchor.enable_lbl')}
            validation={{ msg: t('layout.components.anchor.enable_msg') }}
            onChange={v => onChange({ ...component, anchor: { ...component.anchor ?? anchor.generate(), enabled: v } })}>
            {component.anchor?.enabled}
        </Switch>

        {component.anchor?.enabled &&

            <AnchorFields anchorableMode component={anchorOrDefault} onChange={(c: Anchor) => onChange({ ...component, anchor: { ...component.anchor!, ...c } })} />

        }

    </Fragment>

}