import { Button } from "#app/components/Button";
import { RadioBox } from "#app/form/RadioBox";
import { Switch } from "#app/form/Switch";
import { icns } from "#app/icons";
import { useT } from '#app/intl/api';
import { mergeMultilang, MultilangDto } from "#app/model/multilang";
import { randomNumber, stripHtmlTags } from "#app/utils/common";
import { useLayoutApis } from '#layout/apicontext';
import { footerPdfStylesheets, headerPdfStylesheets, pageContentsPdfStylesheets } from '#layout/pdf/constants';
import { pdfHelper } from '#layout/pdf/helper';
import { StyledBox } from "#layout/style/styledbox";
import { Radio } from "antd";
import * as React from 'react';
import { useLayout } from '../context';
import { isInstance } from '../model';
import { ParametricMultiTextBox, ParametricText } from "../parameters/parametricboxes";
import { ComponentDetail, DetailProps, useComponentDetail } from "./detail";
import { ComponentDraggable, ComponentDroppable } from "./dnd";
import { useSpecOf } from "./helper";
import { ComponentSpec, Container, newComponentFrom, PreviewProps, PrintProps, RenderProps } from "./model";
import { useParagraphComponent } from "./paragraph";
import "./styles.scss";


export type Page = Container & {

    name: MultilangDto
    header?: MultilangDto
    footer?: MultilangDto

    base?: boolean

    orientation: 'portrait' | 'landscape'

}

export const pageId = 'page'

export const usePageComponent = (): ComponentSpec<Page> => {

    const name = 'layout.components.page.name'

    return useSpecOf({

        id: pageId,
        icon: icns.form,
        name,

        generate: (): Page => ({ ...newComponentFrom(pageId), children: [], name: { en: undefined }, base: true, orientation: "portrait" })

        ,

        nameOf: (c: Page) => c.name.en ? c.name : { en: name }

        ,

        Preview

        ,

        Render

        ,

        Detail

        ,

        Print

        ,

        PrintPreview

    })

}


export function Preview(props: PreviewProps<Page>) {

    const t = useT()

    const layout = useLayout()

    const { components, settings} = useLayoutApis()

    const { extend, componentSpecOf, changeComponent, removeComponent, cloneComponent, allPages } = components

    const { ll, getCollapsedStateForComponent, expandComponent, collapseComponent } = settings
    
    const paragraph = useParagraphComponent()

    const { component, path, id = `page-${randomNumber(1000)}` } = props

    const { header, footer } = buildPageBasedOnMaster(component, allPages())

    const [show, visible] = useComponentDetail(component.id)

    const [collapsed, collapse] = React.useState<boolean | undefined>(undefined)

    const collapseAll = (toState?: boolean) => {
        component.children.forEach(child => {
            const collapsed = toState !== undefined ? toState : getCollapsedStateForComponent(child)
            collapsed ? expandComponent(child) : collapseComponent(child)
        })
        collapse(toState !== undefined ? toState : !collapsed)
    }

    const name = t(usePageComponent().name).toLowerCase()

    const openBtn = <Button xsize="xsmall" key={1} light icn={icns.edit} onClick={show} tooltip={t('layout.buttons.open.tooltip', { name })} />
    const collapseBtn = <Button xsize="xsmall" light icn={icns.collapse} onClick={() => collapseAll()} tooltip={t('layout.buttons.collapse.tooltip', { name })} />
    const cloneBtn = <Button xsize="xsmall" light icn={icns.clone} onClick={() => cloneComponent(path)} tooltip={t('layout.buttons.clone.tooltip', { name })} />
    const removeBtn = <Button xsize="xsmall" light icn={icns.removeEntry} onClick={() => removeComponent(path)} tooltip={t('layout.buttons.remove.tooltip', { name })} />

    const nextPosition = component.children.length

    const landscapeClass = (isInstance(layout) && component.orientation === 'landscape') ? 'landscape' : ''


    return <ComponentDroppable className={`page-preview ${collapsed ? "collapsed" : ''}`} types={[pageId]} path={props.path}>
        <ComponentDraggable type={pageId} {...props} >

            <div className='page-card' id={id} onDoubleClick={() => collapsed && collapse(false)}>

                <div className={`card-contents ${landscapeClass}`}>

                    <div className={`card-contents-inner ${landscapeClass}`}>

                        <>

                            {header &&
                                <StyledBox className="component-preview header" component={paragraph.generate()}>
                                    <ParametricText className="text" text={ll(header)} />
                                </StyledBox>
                            }
                            {props.component.children.map((child, position) => {

                                const { Preview } = componentSpecOf(child)

                                return <Preview key={position} component={child} path={extend(props.path).at(position)} />

                            })
                            }

                        </>

                        {props.component.children.length === 0 ?
                            isInstance(layout)|| <ComponentDroppable key={nextPosition} className="component-placeholder" path={extend(path).at(nextPosition)} > <div style={{ height: 50 }} /></ComponentDroppable>
                            :

                            isInstance(layout)|| <ComponentDroppable key={nextPosition} className="component-placeholder" path={extend(path).at(nextPosition)} />}
                       
                        {footer &&
                            <StyledBox className="component-preview footer" component={paragraph.generate()}>
                                <ParametricText className="text" text={ll(footer)} />
                            </StyledBox>
                        }
                    </div>
                </div>

                {isInstance(layout) ||

                    <div className="card-bar">

                        <span className='bar-menu'>{icns.menu}</span>

                        <div className='bar-actions' >
                            {removeBtn}
                            {cloneBtn}
                            {openBtn}
                            {collapseBtn}

                        </div>

                    </div>
                }

            </div>

            {visible && <ComponentDetail component={component} onChange={c => changeComponent(path, c)} />}

        </ComponentDraggable>
    </ComponentDroppable>

}

function Render(props: RenderProps<Page>) {

    const { settings, components } = useLayoutApis()

    const { componentSpecOf, allPages } = components
    const { ll } =settings

    const paragraph = useParagraphComponent()

    const { component, className = '', style } = props

    const { header, footer } = buildPageBasedOnMaster(component, allPages())

    return <div style={style} className={`card-contents ${className}`}>
        {header &&
            <StyledBox className="header" component={paragraph.generate()}>
                <ParametricText className="text" text={ll(header)} />
            </StyledBox>
        }


        {component.children.map((child) => {

            const { Render } = componentSpecOf(child)

            // we expect pages to grow large, so we render them lazy.
            return <Render key={child.id} component={child} />

        })
        }
        {footer &&
            <StyledBox className="footer" component={paragraph.generate()}>
                <ParametricText className="text" text={ll(footer)} />
            </StyledBox>
        }

    </div>

}

function Detail(props: DetailProps<Page>) {

    const t = useT()


    const { components } = useLayoutApis()

    const { component, onChange } = props

    const allpages = components.allPages()

    // Get the index of the current page
    const pageIdx = components.allPages().findIndex(p => p.id === component.id)

    const findFirstInheritance = (pageIdx: number): number => {
        if (pageIdx === 0) return 0
        if (allpages[pageIdx].base === true ||  allpages[pageIdx] === undefined) return pageIdx
        return findFirstInheritance(pageIdx - 1)
    }

    // If we want headers and footers resettable we need to compute them for the previous page, that can be based on, to have the proper default values
    // but if we are on page 1 (pageIdx === 0) there is no previous page hence we reset based on the component itself.
    const firstInheritanceIdx = findFirstInheritance(pageIdx)
    const master = pageIdx === 0 ? component : buildPageBasedOnMaster(allpages[firstInheritanceIdx],allpages)

    const { name, header, footer } = component

    return <>
        <RadioBox label={t("layout.components.page.orientation.name")} validation={{ msg: t("layout.components.page.orientation.msg") }}
            onChange={v => onChange({ ...component, orientation: v })}
            defaultValue={'portrait'}
            value={component.orientation ?? 'portrait'}
        >
            <Radio value={'portrait'}>{t("layout.components.page.orientation.portrait")}</Radio>
            <Radio value={'landscape'}>{t("layout.components.page.orientation.landscape")}</Radio>
        </RadioBox>
        <ParametricMultiTextBox id={component.id} label={t("common.fields.name_multi.name")} onChange={v => onChange({ ...component, name: Object.keys(v).reduce((acc, cur) => ({...acc, [cur]: stripHtmlTags(v[cur])}), {} as MultilangDto) })} validation={{ msg: t("common.fields.name_multi.msg") }}>
            {name}
        </ParametricMultiTextBox>
        <Switch id={`${component.id}_share`} label={t("layout.components.page.share.name")} onChange={v => onChange({ ...component, base: v })}>{component.base ?? true}</Switch>
        <ParametricMultiTextBox id={`${component.id}_header`} rte label={t("layout.components.page.header.name")} onChange={v => onChange({ ...component, header: v })} defaultValue={master?.header} validation={{ msg: t("layout.components.page.header.msg") }}>
            {header}
        </ParametricMultiTextBox>
        <ParametricMultiTextBox id={`${component.id}_footer`} rte editorClassName='footer-details' label={t("layout.components.page.footer.name")} onChange={v => onChange({ ...component, footer: v })} defaultValue={master?.footer} validation={{ msg: t("layout.components.page.footer.msg") }}>
            {footer}
        </ParametricMultiTextBox>

    </>
}


const calculatePreviousIndex = (page: Page, allPages: Page[]): number => {
    const pageIdx = allPages.findIndex(p => p.id === page.id)
    if (pageIdx === 0) return pageIdx // if I am page one return it anyway
    if (allPages[pageIdx - 1].base === true || allPages[pageIdx - 1].base === undefined) return pageIdx - 1 // if previous has base (or undef for compatibility with old layouts), return it
    return calculatePreviousIndex(allPages[pageIdx - 1], allPages) // otherwise keep looking backward
}

export const buildPageBasedOnMaster = (page: Page, allPages: Page[]): Page => {
    // Get the current page index
    const pageIdx = allPages.findIndex(p => p.id === page.id)
    // if it's the first page return it. We don't need to do anything more
    if (pageIdx === 0) return page

    const previousIndex = calculatePreviousIndex(page, allPages)
    // Merge header and footer with previous page
    const header = mergeMultilang(page.header!, allPages[previousIndex].header!)
    const footer = mergeMultilang(page.footer!, allPages[previousIndex].footer!)

    return buildPageBasedOnMaster({ ...allPages[previousIndex], header, footer }, allPages)
}


function Print(props: PrintProps<Page>) {

    const { parameters, settings, components} = useLayoutApis() 

    const { componentSpecOf, allPages } = components
    const { ll } = settings
    const { rr } = parameters

    const { convertHtmlToPdfComponents } = pdfHelper(props)

    const { component, wrap = true, Page: PdfPage, Text, View, StyleSheet } = props

    const { header, footer } = buildPageBasedOnMaster(component, allPages())

    const child = component.children.map((child, position) => {

        const { Print } = componentSpecOf(child)

        return <Print key={position} {...props} component={child} />

    })

    const pageStyles = StyleSheet.create(pageContentsPdfStylesheets)
    const headerStyles = StyleSheet.create(headerPdfStylesheets)
    const footerStyles = StyleSheet.create(footerPdfStylesheets)

    return <PdfPage size="A4" style={{ ...pageStyles.page, fontFamily: 'Roboto' }} wrap={wrap} orientation={component.orientation}>

        {header && <View fixed style={headerStyles.header}>
            {convertHtmlToPdfComponents(rr(ll(header)), { style: headerStyles, noWrap: true })}
        </View>}

        <View >
            {child}
        </View>
        <View fixed style={footerStyles.footerContainer}>
            <View style={footerStyles.footerContents}>
                {footer && convertHtmlToPdfComponents(rr(ll(footer)), { style: footerStyles, noWrap: true })}
            </View>
            <View style={footerStyles.footerPages}>
                <Text render={({ pageNumber, totalPages }) => (
                    `${pageNumber} / ${totalPages}`
                )} />
            </View>
        </View>
    </PdfPage>
}


function PrintPreview(props: PrintProps<Page>) {

    const { components, settings } = useLayoutApis()
    const { componentSpecOf, allPages } = components

    const { ll } = settings

    const { convertHtmlToPdfComponents } = pdfHelper(props)

    const { component, wrap = true, Page: PdfPage, Text, View, StyleSheet } = props

    const { header, footer } = buildPageBasedOnMaster(component, allPages())

    const child = component.children.map((child, position) => {

        const { PrintPreview } = componentSpecOf(child)

        return <PrintPreview key={position} {...props} component={child} />

    })

    const pageStyles = StyleSheet.create(pageContentsPdfStylesheets)
    const headerStyles = StyleSheet.create(headerPdfStylesheets)
    const footerStyles = StyleSheet.create(footerPdfStylesheets)

    return <PdfPage size="A4" style={{ ...pageStyles.page, fontFamily: 'Roboto' }} wrap={wrap} orientation={component.orientation}>

        {header && <View fixed style={headerStyles.header}>
            {convertHtmlToPdfComponents(ll(header), { style: headerStyles, noWrap: true })}
        </View>}

        <View >
            {child}
        </View>
        <View fixed style={footerStyles.footerContainer}>
            <View style={footerStyles.footerContents}>
                {footer && convertHtmlToPdfComponents(ll(footer), { style: footerStyles, noWrap: true })}
            </View>
            <View style={footerStyles.footerPages}>
                <Text render={({ pageNumber, totalPages }) => (
                    `${pageNumber} / ${totalPages}`
                )} />
            </View>
        </View>
    </PdfPage>
}