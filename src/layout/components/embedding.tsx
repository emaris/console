import { Label } from '#app/components/Label'
import { Switch } from '#app/form/Switch'
import { VSelectBox } from '#app/form/VSelectBox'
import { icns } from '#app/icons'
import { useT } from '#app/intl/api'
import { Language } from '#app/intl/model'
import { ReadOnly } from '#app/scaffold/ReadOnly'
import { SubmissionLayoutConfig } from '#campaign/submission/Detail'
import { SubmissionReactContext } from '#campaign/submission/context'
import { useSubmissionContext } from '#campaign/submission/hooks'
import { SubmissionContext, useSubmission } from '#campaign/submission/model'
import { useCurrentDashboard } from '#dashboard/hooks'
import { useLayoutApis, useLiveLayoutApis } from '#layout/apicontext'
import { LayoutAssetContext, LayoutConfig, ParameterContext } from '#layout/context'
import { LayoutInstance, instanceFrom } from '#layout/model'
import { AssetParameter, assetParamId } from '#layout/parameters/asset'
import { Parameter } from '#layout/parameters/model'
import { LayoutProvider, LayoutProviderProps } from '#layout/provider'
import { useLayoutSettings } from '#layout/settings/api'
import { useProductLayoutInitialiser } from '#product/layoutinitialiser'
import { ProductDto } from '#product/model'
import { requirementType } from '#requirement/constants'
import { useRequirementLayoutInitialiser } from '#requirement/layoutinitialiser'
import { RequirementDto } from '#requirement/model'
import { View } from '@react-pdf/renderer'
import { Fragment, PropsWithChildren, useContext, useEffect } from 'react'
import { useLayoutComponents } from './api'
import { AugmentedNamedAsset } from './assetsection'
import { LayoutAssetReferenceContext, PreviewContext } from './context'
import { DetailProps } from './detail'
import { ComponentDraggable } from './dnd'
import { useSpecOf } from './helper'
import { Component, PreviewProps, PrintProps, RenderProps, isContainer, newComponentFrom } from './model'
import { ComponentPreview } from './preview'


export type Embeddable = Component & {

    embeddable: boolean
}

export const isEmbeddable = (c: Component): c is Component & Embeddable => !!(c as unknown as Embeddable)?.embeddable


export const useEmbedddable = () => {

    const t = useT()

    const self = {

        switchBtn: <C extends Embeddable>(props: DetailProps<C>) => {

            const { component, onChange } = props

            return <Switch label={t('layout.components.embedding.embeddable_lbl')} validation={{ msg: t('layout.components.embedding.embeddable_msg') }} onChange={embeddable => onChange({ ...component, embeddable })}>{component.embeddable}</Switch>

        }
        ,

        allIn: (asset: RequirementDto | ProductDto | undefined) => {

            const findRecursive = (c: Component | undefined): Embeddable[] => {

                if (!c)
                    return []

                const results = isContainer(c) ? c.children.flatMap(findRecursive) : []

                return isEmbeddable(c) ? [c, ...results] : results
            }

            return findRecursive(asset?.properties.layout?.components)

        }

        ,

        nameOf: (c: Embeddable | undefined) => c?.shortname ?? c?.id

    }

    return self
}


export const embeddingComponentId = 'embedding'
export const embeddingIcon = icns.layout



export type Embedding = Component & {

    embedding?: { target: string, embeddable: string }
}

export const useEmbeddingComponent = () => {

    return useSpecOf<Embedding>(({

        id: embeddingComponentId,
        icon: embeddingIcon,
        name: 'layout.components.embedding.name',

        generate: (): Embedding => ({

            ...newComponentFrom(embeddingComponentId),

            embedding: undefined!

        }),

        Preview,

        Render,

        Detail,

        Print,

        PrintPreview

    }))

}

type AssetAndEmbeddalbe = AugmentedNamedAsset & { embeddable: Component }

const useEmbedding = (component: Embedding) => {

    const assets = useContext(LayoutAssetReferenceContext)

    const embeddable = useEmbedddable()

    const self = {


        all: () => assets?.flatMap(a => embeddable.allIn(a.asset).map(e => ({ ...a, embeddable: e })))

        ,

        target: (fallbackToFirst: 'fallbackToFirts' | 'noFallback' = 'fallbackToFirts') => {

            const all = self.all()

            return all.find(e => e.asset.id === component.embedding?.target && e.embeddable.id === component.embedding?.embeddable) ?? (fallbackToFirst ? all[0] : undefined!)

        }


    }

    return self
}

export const Detail = (props: DetailProps<Embedding>) => {

    const t = useT()
    const { component, onChange } = props

    const embedding = useEmbedding(component)

    const embeddable = useEmbedddable()

    const assetsWithEmbeddables = embedding.all()

    const target = embedding.target('noFallback')

    return <Fragment>


        <VSelectBox disabled={assetsWithEmbeddables.length === 1}

            label={t("layout.components.embedding.target_lbl")}

            clearable

            validation={{ msg: t("layout.components.embedding.target_msg") }}

            placeholder={embeddable.nameOf(assetsWithEmbeddables[0]?.embeddable)}

            options={assetsWithEmbeddables}

            onChange={(target: AssetAndEmbeddalbe | undefined) => onChange({ ...component, embedding: target ? { target: target.asset.id, embeddable: target.embeddable.id } : undefined })}

            renderOption={(target: AssetAndEmbeddalbe) => <Label icon={embeddingIcon} title={target.embeddable.shortname ?? target.embeddable.id} />}

            lblTxt={(target: AssetAndEmbeddalbe) => target.embeddable.shortname ?? target.embeddable.id}

            optionId={(target: AssetAndEmbeddalbe) => target.embeddable.id}>

            {target ? [target] : undefined}

        </VSelectBox>




    </Fragment >
}


export const Preview = (props: PreviewProps<Embedding>) => {

    const t = useT()

    const { components } = useLayoutApis()

    const { component, path } = props

    const embeddable = useEmbedddable()
    const embedding = useEmbedding(component)

    const target = embedding.target()

    const targetLayout = instanceFrom(target?.asset.properties.layout)

    const targetEmbeddable = target?.embeddable ?? { id: t('layout.components.embedding.no_embeddable') }

    const targetLayoutConfig: LayoutConfig = { mode: 'design' }

    const { Preview } = components.componentSpecOf(targetEmbeddable) ?? { Preview: null }

    return <ComponentDraggable path={path} component={component}>
        <ComponentPreview path={path} component={component}>

            {Preview ?

                <EmbeddedLayout embedding={component} target={target} layout={targetLayout} config={targetLayoutConfig}>
                    <PreviewContext.Provider value={{ openOnDoubleClick: false }}>
                        <div className='embedding-container'>
                            <Preview component={targetEmbeddable} path={path} />
                        </div>
                    </PreviewContext.Provider>
                </EmbeddedLayout>

                :

                <div className='embedding-placeholder'>
                    {t('layout.components.embedding.design_placeholder', { embeddable: embeddable.nameOf(targetEmbeddable) })}
                </div>
            }
        </ComponentPreview>
    </ComponentDraggable>

}



const OuterParameterAdapter = (props: PropsWithChildren<{ parameters: Parameter[] }>) => {

    const { children, parameters } = props

    const { parameters: { parameterSpecOf } } = useLayoutApis()


    const adapted = parameters.filter(p => parameterSpecOf(p.spec))

    return <ParameterContext.Provider value={adapted}>
        {children}
    </ParameterContext.Provider>


}

export const Render = (props: RenderProps<Embedding>) => {

    const { component } = props

    const embedding = useEmbedding(component)

    const parameters = useContext(ParameterContext)

    const target = embedding.target()

    const param = parameters.filter(p => p.spec === assetParamId).map(p => p as AssetParameter).find(p => p.value.id === target.asset.id)!

    if (!param)
        return <Fragment />

    return <InnerRender {...props} target={target} param={param} />

}

type InnerRenderExtraProps = { target: AssetAndEmbeddalbe, param: AssetParameter }

const InnerRender = (props: RenderProps<Embedding> & InnerRenderExtraProps) => {

    const { components } = useLiveLayoutApis()

    const { component, target, param } = props

    const dashboard = useCurrentDashboard()

    const currentContext = useSubmissionContext()

    const targetEmbeddable = target?.embeddable

    const { value } = param

    const { instance, submission } = value.liveData!

    const submissionContext: SubmissionContext = { ...currentContext, submission: submission!, asset: instance }

    const targetLayout: LayoutInstance = { ...target.asset.properties.layout, state: submission!.content }

    const targetLayoutConfig: SubmissionLayoutConfig = { mode: 'live', canUnlock: false, ...currentContext, dashboard }

    const ctx = useSubmission().in(submissionContext)

    const { Render } = components.componentSpecOf(target.embeddable)

    return <SubmissionReactContext.Provider value={submissionContext}  >
        <EmbeddedLayout embedding={component} target={target} initialiseParameters={ctx.initialiseParameters} layout={targetLayout} config={targetLayoutConfig}>
            <Render component={targetEmbeddable} />
        </EmbeddedLayout>
    </SubmissionReactContext.Provider>

}

function Print(props: PrintProps<Embedding>) {
    const { component } = props

    const embedding = useEmbedding(component)

    const parameters = useContext(ParameterContext)

    const target = embedding.target()

    const param = parameters.filter(p => p.spec === assetParamId).map(p => p as AssetParameter).find(p => p.value.id === target.asset.id)!

    if (!param)
        return <View />

    return <InnerPrint {...props} target={target} param={param} />
}

const InnerPrint = (props: PrintProps<Embedding> & InnerRenderExtraProps) => {

    const { components } = useLiveLayoutApis()

    const { component, target, param } = props

    const dashboard = useCurrentDashboard()

    const currentContext = useSubmissionContext()

    const targetEmbeddable = target?.embeddable

    const { value } = param

    const { instance, submission } = value.liveData!

    const submissionContext: SubmissionContext = { ...currentContext, submission: submission!, asset: instance }

    const targetLayout: LayoutInstance = { ...target.asset.properties.layout, state: submission!.content }

    const targetLayoutConfig: SubmissionLayoutConfig = { mode: 'live', canUnlock: false, ...currentContext, dashboard }

    const ctx = useSubmission().in(submissionContext)

    const { Print: PrintCompo } = components.componentSpecOf(target.embeddable)

    return <SubmissionReactContext.Provider value={submissionContext}  >
        <EmbeddedLayout embedding={component} target={target} initialiseParameters={ctx.initialiseParameters} layout={targetLayout} config={targetLayoutConfig}>
            <PrintCompo {...props} component={targetEmbeddable} />
        </EmbeddedLayout>
    </SubmissionReactContext.Provider>
}

export const PrintPreview = (props: PrintProps<Embedding>) => {

    const t = useT()

    const components = useLayoutComponents()

    const { component } = props

    const embedding = useEmbedding(component)

    const target = embedding.target()

    const targetLayout = instanceFrom(target?.asset.properties.layout)

    const targetEmbeddable = target?.embeddable ?? { id: t('layout.components.embedding.no_embeddable') }

    const targetLayoutConfig: LayoutConfig = { mode: 'design' }

    const { PrintPreview } = components.componentSpecOf(targetEmbeddable) ?? { PrintPreview: null }

    return PrintPreview ?

        <EmbeddedLayout embedding={component} target={target} layout={targetLayout} config={targetLayoutConfig}>
            <PreviewContext.Provider value={{ openOnDoubleClick: false }}>
                <PrintPreview {...props} component={targetEmbeddable} />
            </PreviewContext.Provider>
        </EmbeddedLayout>

        :

        <View />


}


export const EmbeddedLayout = (props: PropsWithChildren<LayoutProviderProps & { embedding: Embedding, target: AugmentedNamedAsset }>) => {

    const { parameters } = useLayoutApis()
    const settings = useLayoutSettings()

    const { embedding: component, target, children, layout, ...rest } = props

    const useTargetInitialiser = target.type === requirementType ? useRequirementLayoutInitialiser : useProductLayoutInitialiser

    const targetLayout = { ...layout, documents: target.asset.documents }

    return <ReadOnly value={true}>
        <LayoutAssetContext.Provider value={target.asset}>
            <LayoutProvider layout={targetLayout} useInitialiser={useTargetInitialiser} {...rest}>
                <OuterParameterAdapter parameters={parameters.allParameters()}>
                    <EmbeddedContentLanguageObserver lang={settings.resolveLanguage()}>
                        {children}
                    </EmbeddedContentLanguageObserver>
                </OuterParameterAdapter>
            </LayoutProvider>
        </LayoutAssetContext.Provider>
    </ReadOnly >

}

const EmbeddedContentLanguageObserver = (props: PropsWithChildren<{ lang: Language }>) => {

    const settings = useLayoutSettings()

    const { lang, children } = props

    useEffect(() => {
        settings.changeLayoutLanguage(lang)
    //eslint-disable-next-line
    }, [lang])

    return <>{children}</>

}