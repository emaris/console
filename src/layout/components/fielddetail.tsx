
import { MultiBox } from '#app/form/MultiBox'
import { Switch } from '#app/form/Switch'
import { useT } from '#app/intl/api'
import { MultilangDto, newMultiLang } from "#app/model/multilang"
import { stripMultiHtmlTags } from '#app/utils/common'
import { Icon } from "antd"
import { IoMdCube } from "react-icons/io"
import { ParametricMultiTextBox } from "../parameters/parametricboxes"
import { ComponentCustomName } from './componenthelper'
import { DetailProps } from "./detail"
import { useFieldHelper } from './fieldhelper'
import { FieldRef } from './fieldref'
import { Component } from "./model"


export const fieldIcon = <Icon component={IoMdCube} />

export type FieldComponent = Component & FieldProps

export type FieldProps = {

    msg: MultilangDto
    label: MultilangDto
    placeholder: MultilangDto

    reference?: FieldRef
    restricted?: boolean
    postpublish?: boolean
}


export const partialField = (): FieldProps => ({ msg: newMultiLang(), label: newMultiLang(), placeholder: newMultiLang() })

export const isField = (c: Component): c is FieldComponent => c['msg'] || c['label']

export const FieldDetail = (props: DetailProps<FieldComponent>) => {

    const t = useT()

    const { component, onChange } = props

    const { id, label, msg, placeholder } = component

    const { restricted } = useFieldHelper(component)

    
    return <>

        <ComponentCustomName {...props} />

        <Switch label={t("layout.components.field.restricted.name")} onChange={v => onChange({ ...component, restricted: v })}
            validation={ { msg: t("layout.components.field.restricted.msg") }}>
            {restricted}
        </Switch>

        <Switch label={t("layout.components.field.postpublish.name")} onChange={v => onChange({ ...component, postpublish: v })}
            validation={ { msg: t("layout.components.field.postpublish.msg") }}>
            {component.postpublish}
        </Switch>

        <ParametricMultiTextBox label={t("layout.components.field.label.name")} id={`${id}-field`}
            onChange={v => onChange({ ...component, label: v })}
            validation={{ msg: t("layout.components.field.label.msg") }}>
            {label}
        </ParametricMultiTextBox>

        <MultiBox label={t("layout.components.fields.placeholder.name")} id={`${id}-fieldplaceholder`} 
            onChange={v => onChange({ ...component, placeholder: v })}
            validation={{ msg: t("layout.components.fields.placeholder.msg") }}>
            {stripMultiHtmlTags(placeholder)}
        </MultiBox>

        <ParametricMultiTextBox label={t("layout.components.field.help.name")} id={`${id}-fieldmsg`}
            onChange={v => onChange({ ...component, msg: v })}
            validation={{ msg: t("layout.components.field.help.msg") }}>
            {msg}
        </ParametricMultiTextBox>


    </>

}