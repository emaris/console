
import { ReadonlyMode } from '#app/form/ReadonlyBox'
import { specialise } from '#app/iam/model'
import { useLogged } from '#app/user/store'
import { productSubmissionActions, requirementSubmissionActions } from '#campaign/submission/actions'
import { SubmissionReactContext } from '#campaign/submission/context'
import { useRevisions } from '#campaign/submission/revision'
import { useLayoutApis } from '#layout/apicontext'
import { productActions } from '#product/actions'
import { requirementActions } from '#requirement/actions'
import { requirementType } from '#requirement/constants'
import { useContext } from 'react'
import { useLayout } from '../context'
import { isInstance } from '../model'
import { FieldProps } from './fielddetail'


// returns a set of logical properties for "layout fields".
// individual fields are responsible for honouring them.
// typically they do so by configuring the "UI fields" in terms of which they're implemented.
// if the UI fields do not support some of the features, the "layout fields" may complement or drop support.

export const useFieldHelper = (field: FieldProps) => {

    const { config, settings  } = useLayoutApis()

    const { ll} = settings

    const layout = useLayout()

    const instance = isInstance(layout)

    const revisions = useRevisions()

    const ctx = useContext(SubmissionReactContext)

    const logged = useLogged()

    // privileges: user must have edit privileges over templates (SEC) or submissions (CPC).
    const userCanEdit = !!ctx && (logged.hasNoTenant() ?

        logged.can(specialise(ctx.asset.instanceType === requirementType ? requirementActions.edit : productActions.edit, ctx.asset.id))

        :

        logged.can(specialise(ctx.asset.instanceType === requirementType ? requirementSubmissionActions.edit : productSubmissionActions.edit, ctx.asset.id))
    )


    // ----------------------------------------------------------------------------------------------


    // access may be specifically restricted, or by default.
    // note: as of 28/10/24, global setting seems not currently in use.
    const restricted = field.restricted === undefined ? config.fieldsRestricted : field.restricted


     //readonly: should the field be readonly regardless of whether the context is readonly ?
    // if it's restricted or we're in design mode.
    const readonly = !instance || ( restricted && !logged.isAdmin() )

    // if readonly, what type of UI should we show?
    // in live mode we show plain content. in design we show always the chrome.
    const readOnlyMode: ReadonlyMode = instance ? 'content' : 'chrome'

    // enabledOnReadonly: should the field remain editable when the context is readonly?

    const activeRevision = ctx && revisions.on(ctx.trail).isActive(ctx.submission)

    // case 1: field is restricted, submission is managed, user is edit- privileged SEC.
    const enabledOnReadonlyManaged = logged.hasNoTenant() && activeRevision && userCanEdit && restricted && ctx?.submission.lifecycle.state === 'managed'

    // case 2: submission is published and active, field is post-publish, user is edit-privileged. if CPC, instance must be editable too.
    const enabledOnReadonlyPublished = userCanEdit && field.postpublish && ctx?.submission.lifecycle.state === 'published' && activeRevision && (logged.hasNoTenant() || (ctx.asset.properties.editable))

    const enabledOnReadonly = enabledOnReadonlyManaged || enabledOnReadonlyPublished

    // may unlock iin live and preview modes. 
    // conditions are defined in configuration, based on submission state and user privileges.
    const canUnlock = config.canUnlock && instance

    // enabled by default in preview and live, if field restricted user must be privilged.
    // note: for CPC is already readonly, for non-privileged SEC appears disabled. 
    const enabled = instance && (restricted ? logged.isAdmin() : true)

    const readonlyFallback = readOnlyMode === 'content' ? '-' : undefined

    const placeholder = ll(field.placeholder)

    return { restricted, readonly, canUnlock, readOnlyMode, enabled, enabledOnReadonly, readonlyFallback, placeholder }

}