import { PropsWithChildren } from 'react'
import './optionpanel.scss'

export const OptionPanel = (props: PropsWithChildren<{

    show: boolean | undefined

}>) => {

    const { show, children } = props

    return <div className={`option-panel panel-${show ? 'show' : 'hide'}`}>
            <div className='panel-inner'>
                {children}
            </div>
        </div>
    }
