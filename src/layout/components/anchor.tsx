import { useConfig } from '#app/config/state'
import { RadioBox } from '#app/form/RadioBox'
import { useT } from '#app/intl/api'
import { MultilangDto } from '#app/model/multilang'
import { requireLanguages } from '#app/utils/validation'
import { useLayoutApis } from '#layout/apicontext'
import { RenderLoopContext } from '#layout/context'
import { useLayout } from '#layout/initialiser'
import { isInstance } from '#layout/model'
import { useParameters } from '#layout/parameters/hooks'
import { ParametricMultiTextBox } from '#layout/parameters/parametricboxes'
import { View } from '@react-pdf/renderer'
import { Icon, Radio } from 'antd'
import { Fragment, useContext } from 'react'
import { RiBookMarkLine } from 'react-icons/ri'
import { ComponentCustomName } from './componenthelper'
import { DetailProps } from './detail'
import { ComponentDraggable, ComponentDroppable } from './dnd'
import { useSpecOf } from './helper'
import { Component, PreviewProps, RenderProps, newComponentFrom } from './model'
import { ComponentPreview } from './preview'


export type Anchor = Component & {

    entry: MultilangDto
    level?: number

}



export const anchorClassId = 'anchor-id'
export const anchorId = 'anchor'
export const anchorIcon = <Icon component={RiBookMarkLine} />

export const useAnchorComponent = () => {

    const name = 'layout.components.anchor.name'

    return useSpecOf<Anchor>({

        id: anchorId,
        icon: anchorIcon,
        name,

        generate: (): Anchor => ({
            ...newComponentFrom(anchorId),
            entry: { en: undefined! }
        }),

        // if we're in offscreen mode, then still render fully.
        Offscreen: RenderAnchor,

        Render: RenderAnchor,

        Preview,

        Detail,

        Print: () => <View />

    })

}


export function RenderAnchor(props: RenderProps<Anchor>) {

    const { component } = props

    const { settings: { ll } } = useLayoutApis()

    const { rr } = useParameters()

    const loopctx = useContext(RenderLoopContext)

    const id = component.shortname ?? component.id

    const anchorId = loopctx ? `${loopctx}-${component.id}` : id

    const label = rr(ll(component.entry)) || id

    return <span className={anchorClassId} aria-level={component.level ?? 1} aria-label={label} id={anchorId} />

}

export const PreviewAnchor = (props: { component: Anchor }) => {

    const { component } = props

    const { parameters, settings } = useLayoutApis()

    const id = component.shortname ?? component.id

    const label = parameters.rr(settings.ll(component.entry)) || id

    return <div className='anchor-line'>
        <span className={anchorClassId} aria-level={component.level ?? 1} aria-label={label} id={id} />
    </div>
}

function Preview(props: PreviewProps<Anchor>) {

    const { component, path } = props

    const layout = useLayout()

    if (isInstance(layout))
        return <RenderAnchor {...props} />

    return <ComponentDroppable path={path}>
        <ComponentDraggable path={path} component={component}>
            <ComponentPreview path={path} component={component}>
                <PreviewAnchor component={component} />
            </ComponentPreview>
        </ComponentDraggable>
    </ComponentDroppable>
}


function Detail(props: DetailProps<Anchor>) {


    return  <AnchorFields {...props} />

}


export const AnchorFields = (props: DetailProps<Anchor> & { anchorableMode?: boolean }) => {

    const t = useT()

    const config = useConfig()

    const { component, onChange, anchorableMode } = props

    const requiredLangs = config.get().intl.required || [];

    return <Fragment>


        <ComponentCustomName fieldProps={{label: t('layout.components.anchor.id_lbl'), validation: {msg: t('layout.components.anchor.id_msg')}}}  {...props} />


        <ParametricMultiTextBox id={component.id} label={t("layout.components.anchor.entry_lbl")}
            validation={!anchorableMode && requireLanguages(requiredLangs, t).predicate(component.entry) ?
                { status: 'warning', msg: t("layout.components.anchor.entry_warn") }
                :
                { msg: t("layout.components.anchor.entry_msg") }
            }
            onChange={v => onChange({ ...component, entry: v })} >
            {component.entry}
        </ParametricMultiTextBox>

        <RadioBox label={t("layout.components.anchor.level_lbl")} validation={{ msg: t('layout.components.anchor.level_msg') }}
            value={component.level ?? 1}
            onChange={level => onChange({ ...component, level })} >
            <Radio value={1}>{t("layout.components.anchor.level_1")}</Radio>
            <Radio value={2}>{t("layout.components.anchor.level_2")}</Radio>
            <Radio value={3}>{t("layout.components.anchor.level_3")}</Radio>
        </RadioBox>

    </Fragment>
}
