import { TextBox } from '#app/form/TextBox'
import { icns } from '#app/icons'
import { useT } from '#app/intl/api'
import { deepclone } from '#app/utils/common'
import { updateQuery } from '#app/utils/routes'
import { useLayoutApis } from '#layout/apicontext'
import { alignmentStyleId } from '#layout/style/alignment'
import { backgroundStyleId } from '#layout/style/background'
import { borderStyleId } from '#layout/style/border'
import { discreteWidthStyleId } from '#layout/style/discretewidth'
import { StyleProps } from '#layout/style/model'
import { absoluteSpacingStyleId, spacingStyleId } from '#layout/style/spacing'
import { widthStyleId } from '#layout/style/width'
import { TFunction } from 'i18next'
import React from 'react'
import { useLocation } from 'react-router-dom'
import { useLayoutRows } from './api'
import { DetailProps } from './detail'
import { ComponentDraggable, ComponentDroppable } from './dnd'
import { useSpecOf } from './helper'
import { RowSectionContext, useRowSection, useRowSectionContextModifier } from './hooks'
import { Component, ComponentSpec, Container, PreviewProps, PrintProps, RenderProps, clone, newComponentFrom, newIdFor } from './model'
import { ComponentPreview } from './preview'
import { Cell, Row, RowCellCards, cellDetailInnerConditionParam, cellDetailInnerParam, cellDetailParam, cellId, useRowComponent } from './row'
import { LayoutScrollSubscriber } from './scroll'

export type RowSectionSpec = Component & Container<Row> & StyleProps & {
    cells: Cell[]
    alias?: string
}

export const useRowSectionComponent = (): ComponentSpec<RowSectionSpec> => {

    const id = 'rowsection'

    const t = useT()

    const rowcomponent = useRowComponent()

    return useSpecOf({

        id,
        icon: icns.rowSection,
        name: 'layout.components.row_section.name',
        showTabsOnDetails: true,

        Title: ({ component }: { component: RowSectionSpec }) => {


            const alias = component.alias ? component.alias : t('layout.components.row_section.name')

            return <>{alias}</>

        }

        ,

        generate: (t?: TFunction): RowSectionSpec => {

            const component = newComponentFrom(id)

            const row = { ...rowcomponent.generate(t), asTemplate: true }

            const rowSectionName = t ? t('layout.components.row_section.name') : 'Row Section'
            return {
                ...component,
                children: [{ ...row, children: row.children.map(cell => ({ ...cell, asTemplate: true })) }],
                cells: row.children.map(cell => ({ ...cell, id: `${cell.id}-${component.id}`, virtual: true, style: {} })),
                extraStyle: { name: t ? t('layout.components.row.cell.details.name') : 'Cell Styles' },
                style: {},
                alias: rowSectionName
            }
        }

        ,

        cloneFor: (c: RowSectionSpec) => {
            const cloned = clone(c)
            const cells = deepclone(c.cells).map(cell => ({ ...cell, id: `${newIdFor(cellId)}-${cloned.id}}` }))
            return { ...cloned, cells } as RowSectionSpec
        }

        ,

        styles: (map, _) => ({

            [widthStyleId]: { percentage: map[widthStyleId]?.percentage ?? 100 },
            [alignmentStyleId]: { value: map[alignmentStyleId]?.value ?? 'left' },
            [spacingStyleId]: {}
        })

        ,

        extraStyles: (map, __) => {

            return {
                name: t('layout.components.row.cell.details.name'),
                [backgroundStyleId]: map[backgroundStyleId],
                [discreteWidthStyleId]: map[discreteWidthStyleId],
                [borderStyleId]: map[borderStyleId],
                [absoluteSpacingStyleId]: map[absoluteSpacingStyleId]
            }
        }

        ,

        Detail,

        Render,

        Preview,

        Print,

        PrintPreview

    })
}


function Preview(props: PreviewProps<RowSectionSpec>) {

    const { component, path } = props

    const { components } = useLayoutApis()

    const nextPosition = component.children.length

    const contextModifier = useRowSectionContextModifier()

    //eslint-disable-next-line
    React.useEffect(() => contextModifier && contextModifier(component), [component])

    return <ComponentDroppable path={path}>
        <RowSectionContext key={component.id} value={component}>
            <ComponentDraggable path={path} component={component}>
                <ComponentPreview path={path} component={component}>
                    <ComponentDroppable className="component-placeholder" path={components.extend(path).at(0)} />
                    {component.children.map((component, idx) => {

                        const { Preview } = components.componentSpecOf(component)
                        return <Preview key={idx} component={component} path={`${path}@${idx}`} />


                    })}
                    <ComponentDroppable key={nextPosition} className="component-placeholder" path={components.extend(path).at(nextPosition)} />
                </ComponentPreview>
            </ComponentDraggable>
        </RowSectionContext>
    </ComponentDroppable >

}


function Detail(props: DetailProps<RowSectionSpec>) {

    const { component } = props

    const t = useT()

    const { components } = useLayoutApis()

    const { addCellToSectionRows, removeCellFromSectionRows, moveCellInSectionRows, modifyRowsInRowSection } = useLayoutRows()

    const { setRowSectionContext } = useRowSection()
    const { pathname, search } = useLocation()

    const detailRoute = (cell: Cell, inner?: boolean) => `${pathname}?${updateQuery(search).with(params => params[inner ? cellDetailInnerParam : cellDetailParam] = cell.id)}`
    const conditionDetailRoute = (cell: Cell, inner?: boolean) => `${pathname}?${updateQuery(search).with(params => params[cellDetailInnerConditionParam] = cell.id)}`

    const addOneCell = () => setRowSectionContext(addCellToSectionRows(component))

    const changeAndApply = (rowSection: RowSectionSpec) => {
        components.changeComponent(components.pathFor(component), rowSection)
        setRowSectionContext(rowSection)
    }

    const onChangeCellDetail = (_: string, cell: Cell) =>
        changeAndApply(
            {
                ...modifyRowsInRowSection(component, (row) => ({ ...row, children: row.children.map(c => cell.index === c.index ? ({ ...c, alias: c.asTemplate ? cell.alias : c.alias }) : c) })),
                cells: [...component.cells.map(c => c.id === cell.id ? cell : c)]

            }
        )

    const onMoveCell = (to, item) => changeAndApply(moveCellInSectionRows(component, to, item))

    const onRemoveCell = (idx: number) => setRowSectionContext(removeCellFromSectionRows(component, idx))

    return <div className='row-details'>

        <TextBox
            label={t('layout.components.row.field.label')}
            validation={{ msg: t('layout.components.row.field.msg') }}
            onChange={(v) => changeAndApply({ ...component, alias: v })}>
            {component.alias}
        </TextBox>
        <RowCellCards
            cells={component.cells}
            onAddCell={addOneCell}
            onMoveCell={onMoveCell}
            onRemoveCell={onRemoveCell}
            routeToDetail={(cell) => detailRoute(cell, true)}
            routeToConditions={(cell) => conditionDetailRoute(cell, true)}
            onChangeCell={(_, cell) => onChangeCellDetail(_, cell as Cell)}
        />
    </div>

}

function Render(props: RenderProps<RowSectionSpec>) {

    const { component } = props

    const { components } = useLayoutApis()

    const { componentSpecOf } = components

    return <RowSectionContext key={component.id} value={component}>
        {component.children.map(child => {

            const { Render } = componentSpecOf(child)

            return <LayoutScrollSubscriber key={child.id} component={child}>
                <Render component={child} />
            </LayoutScrollSubscriber>
        })}
    </RowSectionContext>

}

Render.displayName = 'RowContextRender'



function Print(props: PrintProps<RowSectionSpec>) {

    const { component, wrap = true, View } = props

    const { components } = useLayoutApis()

    return <RowSectionContext key={component.id} value={component}>
        <View style={{ marginBottom: 15 }}>
            {component.children.map((child, position) => {

                const { Print } = components.componentSpecOf(child)

                return <View wrap={wrap} key={position} style={{ display: 'flex', flexDirection: 'column', width: '100%' }}><Print {...props} key={position} component={child} /></View>

            })}
        </View>
    </RowSectionContext>
}

export default function PrintPreview(props: PrintProps<RowSectionSpec>) {

    const { component, wrap = true, View } = props

    const { components } = useLayoutApis()

    return <RowSectionContext key={component.id} value={component}>
        <View style={{ marginBottom: 15 }}>
            {component.children.map((child, position) => {

                const { PrintPreview } = components.componentSpecOf(child)

                return <View wrap={wrap} key={position} style={{ display: 'flex', flexDirection: 'column', width: '100%' }}>
                    <PrintPreview {...props} key={position} component={child} />
                </View>

            })}
        </View>
    </RowSectionContext>
}