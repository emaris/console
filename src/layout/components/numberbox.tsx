import { NumericBox } from '#app/form/NumericBox'
import { RadioBox } from '#app/form/RadioBox'
import { useT } from '#app/intl/api'
import { stripHtmlTags } from '#app/utils/common'
import { Validation } from '#app/utils/validation'
import { ChangelogRender } from '#campaign/submission/changerender'
import { useLayoutApis } from '#layout/apicontext'
import { useLayoutParameters } from '#layout/parameters/api'
import { useParameters } from '#layout/parameters/hooks'
import { ParametricTextBox } from '#layout/parameters/parametricboxes'
import StyledView from '#layout/pdf/styledview'
import { usePdfStyles } from '#layout/pdf/stylehook'
import { alignmentStyleId } from '#layout/style/alignment'
import { borderStyleId } from '#layout/style/border'
import { heightStyleId } from '#layout/style/height'
import { StyleProps } from '#layout/style/model'
import { spacingStyleId } from '#layout/style/spacing'
import { StyledBox } from '#layout/style/styledbox'
import { textStyleId } from '#layout/style/text'
import { inputWidthStyleId, widthStyleId } from '#layout/style/width'
import { StyleSheet, Text } from '@react-pdf/renderer'
import { Icon, Radio } from 'antd'
import { AiOutlineNumber } from 'react-icons/ai'
import { useComponentData } from './api'
import { DetailProps } from './detail'
import { FieldProps, partialField } from './fielddetail'
import { useFieldHelper } from './fieldhelper'
import { useFieldReferenceResolver, useLazyFieldReferenceResolver } from './fieldref'
import { useLayoutLabel, useSpecOf } from './helper'
import { Component, PrintProps, RenderProps, newComponentFrom } from "./model"
import { ReferenceFormFields } from './referenceformfield'




export type NumberBox = Component & FieldProps & StyleProps & {

    defaultValue?: number

    defaultValueAsText?: string


    numberType: 'int' | 'decimal'
    min?: number
    max?: number

}

export const useNumberBoxComponent = () => {

    const id = 'number-box'
    const name = 'layout.components.numberbox.name'


    const params = useLayoutParameters()

    const referenceresolver = useLazyFieldReferenceResolver<NumberBox, number>()

    return useSpecOf({

        id,
        icon: <Icon className="anticon-number" component={AiOutlineNumber} />,
        name,

        styles: current => {

            return {

                [textStyleId]: {},
                // defaults to an absolute scale.
                [inputWidthStyleId]: { default: 100, min: 60, max: 300, ...current[inputWidthStyleId] },
                // bleeds by default, but supports an absolute scale that trackes the input.
                [widthStyleId]: { default: 105, min: 65, max: 305, percentage: 100, ...current[widthStyleId] },
                [heightStyleId]: {},
                [borderStyleId]: {},
                [alignmentStyleId]: {},
                [spacingStyleId]: { top: 5, bottom: 5, ...current[spacingStyleId] }

            }
        }

        ,

        // [since 11/2024] materialises defaults to replace missing content on submit.
        // considers both derived defaults (field references) and configured defaults, in that order.
        // for configured defaults, resolves parameters.
        submittedDataOf: (c: NumberBox, { data }, parameters) => {

            // if we have data we don't to materialise anything.
            if (data !== undefined)
                return undefined

            // we can't use the default helper hook here, unfortunately. 
            // so we need to replicate the various fallback logic.


            // we resolve the param, strip the tags, and make sure we don't keep an empty string (that's wny we use || operator). 
            const parametricDefault = stripHtmlTags(params.r(c.defaultValueAsText, parameters)) || undefined

            // then make sure the string amounts to an actual number, othwerwise we disregard it.
            const parametricAsNumber = isNaN(Number(parametricDefault)) ? undefined : Number(parametricDefault)

            // next, see if we have a higher-priority default resolved from a field reference.
            const { resolved } = referenceresolver(c)

            // we materialise the ref, fallback to parametric, then to constant.
            // note: ?? here preserves 0s.
            return { data: resolved ?? parametricAsNumber ?? c.defaultValue }
        }


        ,


        // exposes contents for resolving mentions in Asset Sections from external layouts, typically in products.
        // note: the context available in the original layout may not necesseraly exist in the external layout.
        // however, submittedDataOf() should have captured it on submit, materialised it as content, and made it unnecessary from the external layout/
        dynamicDataOf: (_, { data }) => {

            if (data === undefined)
                return undefined

            // note: since 11/2024 we don't have to worry about field defaults, 
            // because they've been materialised as content on submit (cf. submittedDataOf).

            return `${data}`

        }

        ,

        generate: (): NumberBox => ({

            ...newComponentFrom(id),
            ...partialField(),
            style: {},
            numberType: 'int'
        })

        ,


        Render

        ,


        Detail

        ,

        Print


    })
}


const useDefaultHelper = (component: NumberBox) => {

    const parameters = useParameters()

    // we resolve the param, strip the tags. 
    const parametricDefault = stripHtmlTags(parameters.r(component.defaultValueAsText)) || undefined

    // then make sure the string amounts to an acutal number, othwerwise we disregard it.
    const parametricAsNumber = isNaN(Number(parametricDefault)) ? undefined : Number(parametricDefault)

     // next, see if we have a higher-priority default resolved from a field reference.
    const { resolved } = useFieldReferenceResolver<NumberBox, number>(component)

    // ref first, fallback to parametric, then to constant.
    return resolved ?? parametricAsNumber ?? component.defaultValue
}


function Render(props: RenderProps<NumberBox>) {

    const { component } = props

    const t = useT()

    const { settings: { ll }, styles } = useLayoutApis()

    const { enabledOnReadonly, readonly, canUnlock, readOnlyMode, placeholder } = useFieldHelper(component)

    const { numberType, label, min, max } = component

    const { r } = useParameters()

    const lbl = useLayoutLabel(label)

    const defaultValue = useDefaultHelper(component)

    const { dataFor, changeDataFor } = useComponentData()

    const data = dataFor(component)

    const msg = r(ll(component.msg))

    const change = (v: number) => changeDataFor(component, v)

    const validation: Validation = {

        msg,
        ...(data != undefined && (min !== undefined && data < min) ? { msg: t('layout.components.numberbox.min_error', { min }), status: 'error' } : {}),
        ...(data != undefined && (max !== undefined && data > max) ? { msg: t('layout.components.numberbox.max_error', { max }), status: 'error' } : {})

    }

    const style = styles.resolveCustomStyles(component)

    const key = data === undefined ? `defaulted-${defaultValue}` : undefined

    return <ChangelogRender {...props}>
        <StyledBox component={component}>{

            <NumericBox key={key} className='layout-numberbox' style={style} label={lbl} validation={validation}

                placeholder={placeholder}
                defaultValue={defaultValue}
                onChange={change}

                readonly={readonly}                     // override readonly context.
                enabledOnReadOnly={enabledOnReadonly}  // don't honour readonly context.
                readonlyMode={readOnlyMode}
                canUnlock={canUnlock}

                step={numberType === 'decimal' ? 0.1 : 1}>

                {data}

            </NumericBox>

        }</StyledBox>
    </ChangelogRender>

}

function Detail(props: DetailProps<NumberBox>) {

    const t = useT()

    const { component, onChange } = props

    const step = component.numberType === 'decimal' ? 0.1 : 1

    return <div>

        <ReferenceFormFields {...props} />

        <RadioBox label={t("layout.components.numberbox.step_lbl")} validation={{ msg: t("layout.components.numberbox.step_msg") }} value={component.numberType} onChange={v => onChange(v === 'int' ? { ...component, numberType: v, min: component.min ? Math.round(component.min) : undefined, max: component.max ? Math.round(component.max) : undefined } : { ...component, numberType: v })}>
            <Radio key='int' value='int'>{t("layout.components.numberbox.step_int")}</Radio>
            <Radio key='decimal' value='decimal'>{t("layout.components.numberbox.step_decimal")}</Radio>
        </RadioBox>

        <NumericBox label={t("layout.components.numberbox.min_lbl")} validation={{ msg: t("layout.components.numberbox.min_msg") }} step={step} onChange={v => onChange({ ...component, min: v })}>{component.min}</NumericBox>
        <NumericBox label={t("layout.components.numberbox.max_lbl")} validation={{ msg: t("layout.components.numberbox.max_msg") }} step={step} onChange={v => onChange({ ...component, max: v })}>{component.max}</NumericBox>

        <NumericBox style={{ width: 160 }} label={t("layout.components.fields.default_value.name")} id={`numberbox-default-${component.id}`}
            validation={{ msg: t("layout.components.fields.default_value.msg") }}
            onChange={defaultValue => onChange({ ...component, defaultValue })} >
            {component.defaultValue}
        </NumericBox>

        <ParametricTextBox label={t("layout.components.fields.default_value_astext.name")} id={`numberbox-placeholder-${component.id}`} noImage onChange={v => onChange({ ...component, defaultValueAsText: v })}
            validation={{ msg: t("layout.components.fields.default_value_astext.msg") }}>
            {component.defaultValueAsText}
        </ParametricTextBox>

    </div>
}


function Print(props: PrintProps<NumberBox & FieldProps>) {

    const { component } = props

    const { dataFor } = useComponentData()

    const { readonlyFallback } = useFieldHelper(component)

    const defaultValue = useDefaultHelper(component)

    const data = dataFor(component) ?? defaultValue

    const numericBoxStyle = StyleSheet.create(usePdfStyles(component, "numericBox"))

    const formatWidthForPrint = (c: NumberBox): NumberBox => {
        if (c.style?.[widthStyleId] && c.style?.[widthStyleId].percentage && c.style?.[widthStyleId].percentage <= 90)
            return { ...c, style: { ...c.style, [widthStyleId]: { 'percentage': c.style[widthStyleId].percentage + 10 } } }
        return c
    }

    return <StyledView {...props} component={formatWidthForPrint(component)}>
        <Text style={numericBoxStyle}>{data ?? readonlyFallback}</Text>
    </StyledView>
}
