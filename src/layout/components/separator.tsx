import { Switch } from '#app/form/Switch'
import { useT } from '#app/intl/api'
import { Icon } from 'antd'
import { AiOutlineBorderVerticle } from 'react-icons/ai'
import StyledView from '#layout/pdf/styledview'
import { heightStyleId } from '#layout/style/height'
import { lineStyleId } from '#layout/style/line'
import { spacingStyleId } from '#layout/style/spacing'
import { StyledBox } from '#layout/style/styledbox'
import { widthStyleId } from '#layout/style/width'
import { StyleProps } from '#layout/style/model'
import { DetailProps } from './detail'
import { useSpecOf } from './helper'
import { Component, ComponentSpec, PrintProps, RenderProps, newComponentFrom } from './model'



export type Separator = Component & StyleProps & {

    withLine: boolean

}


export const useSeparatorComponent = (): ComponentSpec<Separator> => {

    const id = 'separator'
    
    return useSpecOf({

        id,
        icon: <Icon component={AiOutlineBorderVerticle} />,
        name: 'layout.components.separator.name',

        styles: (map, component) => ({

            [lineStyleId]: { color: map[lineStyleId]?.color, visible: component.withLine },
            [widthStyleId]: {},
            [heightStyleId]: {},
            [spacingStyleId]: {}

        })

        ,

        generate: (): Separator => ({

            ...newComponentFrom(id),
            style: {},
            withLine: true

        })

        ,


        Render

        ,

        Detail

        ,

        Print
    })

}

function Render(props: RenderProps<Separator>) {

    const { component } = props

    return <StyledBox component={component} style={{marginTop: 4, marginBottom: 4}}/>
}


function Detail(props: DetailProps<Separator>) {

    const t = useT()

    const { component, onChange } = props

    return <Switch label={t("layout.components.separator.with_line.name")}
        validation={{ msg: t("layout.components.separator.with_line.msg") }}
        onChange={v => onChange({ ...component, withLine: v })}>
        {component.withLine}
    </Switch>

}

function Print(props: PrintProps<Separator>) {

    const { component } = props

    return <StyledView {...props} component={component} />
}