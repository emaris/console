import { TextBox, TextBoxProps } from '#app/form/TextBox'
import { useT } from '#app/intl/api'
import { Validation } from '#app/utils/validation'
import { useLayoutApis } from '#layout/apicontext'
import { useLayout } from '#layout/context'
import { DetailProps } from './detail'
import { Component, isContainer } from './model'


export const ComponentCustomName = <C extends Component>(props: DetailProps<C> & { fieldProps?: Partial<TextBoxProps>, strict?: boolean }) => {

    const t = useT()

    const { config, components} = useLayoutApis()

    const { component, onChange, strict=true, fieldProps } = props

    const { id, shortname } = component

    const layout = useLayout()

    const findDuplicate = (c: Component) : Component | undefined => 
        
         c.id !== component.id  &&  c.shortname === component.shortname ? c :  isContainer(c) ? c.children.reduce((agg,next) => agg ?? findDuplicate(next),  undefined! as Component) : undefined
    

    let nameValidation: Validation = { msg: t("layout.components.field.name.msg") }
    
    if (strict) {

        if (shortname) {

            const duplicate = findDuplicate(layout.components)

            if (duplicate) 
                nameValidation =  {status: 'error', msg: t("layout.components.field.name.msg_export_duplicate",{spec:t(components.componentSpecOf(duplicate).name)})  } 
            else 
                nameValidation = { msg: t("layout.components.field.name.msg_export") }
        }
        else if (config.exportsData)
             nameValidation = { status: 'warning', msg: t("layout.components.field.name.msg_export_missing") }

    }


    return <TextBox fieldClassName="component-shortname" placeholder={id} label={t("layout.components.field.name.name")}
        onChange={v =>  onChange({ ...component, shortname: v?.replace('.','-')})}
        validation={nameValidation}
        {...fieldProps}>
        {shortname}
    </TextBox>
}