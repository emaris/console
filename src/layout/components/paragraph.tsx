import { useT } from '#app/intl/api'
import { MultilangDto } from '#app/model/multilang'
import { useLayoutApis } from '#layout/apicontext'
import { pageContentsPdfStylesheets } from '#layout/pdf/constants'
import { pdfHelper } from '#layout/pdf/helper'
import StyledView from '#layout/pdf/styledview'
import { usePdfStyles } from '#layout/pdf/stylehook'
import { alignmentStyleId } from '#layout/style/alignment'
import { backgroundStyleId } from '#layout/style/background'
import { borderStyleId } from '#layout/style/border'
import { heightStyleId } from '#layout/style/height'
import { StyleProps } from '#layout/style/model'
import { spacingStyleId } from '#layout/style/spacing'
import { StyledBox } from '#layout/style/styledbox'
import { textStyleId } from '#layout/style/text'
import { widthStyleId } from '#layout/style/width'
import { Icon } from 'antd'
import React, { Fragment } from 'react'
import { FaParagraph } from 'react-icons/fa'
import { useParameters } from '../parameters/hooks'
import { ParametricMultiTextBox, ParametricText } from '../parameters/parametricboxes'
import { useAnchorComponent } from './anchor'
import { Anchorable, AnchorableFields, RenderAnchorable } from './anchorable'
import { DetailProps } from './detail'
import { useSpecOf } from './helper'
import { Component, OffscreenProps, PrintProps, RenderProps, newComponentFrom } from "./model"



export type Paragraph = Component & StyleProps & Anchorable & {

    text: MultilangDto
}

export const paragraphId = 'paragraph'

export const useParagraphComponent = () => {

    const name = 'layout.components.paragraph.name'


    const anchor = useAnchorComponent()

    return useSpecOf({

        id: paragraphId,

        icon: <Icon component={FaParagraph} />,
        name

        ,

        styles: map => ({

            [textStyleId]: {},
            [backgroundStyleId]: {},
            [widthStyleId]: {},
            [heightStyleId]: {},
            [borderStyleId]: {},
            [alignmentStyleId]: {},
            [spacingStyleId]: { top: map[spacingStyleId]?.top ?? 5, bottom: map[spacingStyleId]?.bottom ?? 5 }
        })
        ,

        generate: (): Paragraph => ({

            ...newComponentFrom(paragraphId),
            style: {},
            text: { en: undefined! },
            anchor: {
                enabled: false,
                ...anchor.generate()
            }

        })


        ,


        nameOf: () => ({ en: name })

        ,

        Offscreen

        ,


        Render: React.memo(Render) as any

        ,

        Detail

        ,

        Print

        ,

        PrintPreview: Print

    })
}


function Offscreen(props: OffscreenProps<Paragraph>) {

    const { settings: { ll } } = useLayoutApis()
    const { rr } = useParameters()

    const { component } = props

    return <Fragment>{rr(ll(component.text))}</Fragment>
}

function Render(props: RenderProps<Paragraph>) {

    const { component } = props

    const { styles, settings } = useLayoutApis()



    const { resolveCustomStyles } = styles
    const { ll } = settings

    const custom = resolveCustomStyles(component)

    return <Fragment>

        <RenderAnchorable component={component} fallback={component.text} />

        <StyledBox component={component}>
            <ParametricText style={custom} className="text" text={ll(component.text)} />
        </StyledBox>

    </Fragment>

}

function Detail(props: DetailProps<Paragraph>) {

    const t = useT()

    const { component, onChange } = props

    return <Fragment>

        <ParametricMultiTextBox id={component.id} label={t("layout.components.fields.text.name")} onChange={v => onChange({ ...component, text: v })} validation={{ msg: t("layout.components.fields.text.msg") }}>
            {component.text}
        </ParametricMultiTextBox>

        <AnchorableFields {...props} />

    </Fragment>
}


function Print(props: PrintProps<Paragraph>) {

    const { component, View, StyleSheet } = props

    const { settings } = useLayoutApis()

    const { ll } = settings
    const { rr } = useParameters()

    const { convertHtmlToPdfComponents } = pdfHelper(props)

    const paragraphStyles = { ...StyleSheet.create(usePdfStyles(component)), ...pageContentsPdfStylesheets.p }

    return <StyledView  {...props} component={component} >
        <View style={{...paragraphStyles, marginRight: 5}} >{convertHtmlToPdfComponents(rr(ll(component.text)))}</View>              
    </StyledView>


}