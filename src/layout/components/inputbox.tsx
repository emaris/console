import { useConfig } from '#app/config/state'
import { MultiBox } from '#app/form/MultiBox'
import { RichBox } from '#app/form/RichBox'
import { Switch } from '#app/form/Switch'
import { icns } from '#app/icons'
import { useT } from '#app/intl/api'
import { allLanguages, Language } from '#app/intl/model'
import { MultilangDto } from '#app/model/multilang'
import { ChangelogRender } from '#campaign/submission/changerender'
import { useLayoutApis } from '#layout/apicontext'
import { pdfHelper } from '#layout/pdf/helper'
import StyledView from '#layout/pdf/styledview'
import { usePdfStyles } from '#layout/pdf/stylehook'
import { useLayoutSettings } from '#layout/settings/api'
import { alignmentStyleId } from '#layout/style/alignment'
import { borderStyleId } from '#layout/style/border'
import { heightStyleId } from '#layout/style/height'
import { StyleProps } from '#layout/style/model'
import { spacingStyleId } from '#layout/style/spacing'
import { StyledBox } from '#layout/style/styledbox'
import { textStyleId } from '#layout/style/text'
import { inputWidthStyleId, widthStyleId } from '#layout/style/width'
import * as React from 'react'
import { useLayout } from '../context'
import { isInstance } from '../model'
import { useLayoutParameters } from '../parameters/api'
import { useParameters } from '../parameters/hooks'
import { ParametricMultiTextBox } from '../parameters/parametricboxes'
import { useComponentData } from './api'
import { DetailProps } from './detail'
import { ComponentDraggable, ComponentDroppable } from './dnd'
import { FieldProps, partialField } from './fielddetail'
import { useFieldHelper } from './fieldhelper'
import { useFieldReferenceResolver, useLazyFieldReferenceResolver } from './fieldref'
import { useLayoutLabel, useSpecOf } from './helper'
import { Component, newComponentFrom, PreviewProps, PrintProps, RenderProps } from "./model"
import { ComponentPreview } from './preview'
import { ReferenceFormFields } from './referenceformfield'




export type InputBox = Component & FieldProps & StyleProps & {

    defaultValue?: MultilangDto

    rich?: boolean      // legacy: always true.
    multi?: boolean     // defaults true
    toolbar?: boolean   // defaults true


}


const stripColorStyles = (text: string | undefined) => text ? text.replace(/style="[^"]*color:[^"]*"/g, '').replace(/style="[^"]*background:[^"]*"/g, '').replace(/style="[^"]*background-color:[^"]*"/g, '') : ''
const stripImagesBase64 = (text: string | undefined) => text ? text.replace(/<img src="data:image\/[^;]+;base64,[^"]+"[^>]*>/g, '') : ''

const cleanUpHtml = (text: string | undefined) => stripImagesBase64(stripColorStyles(text))



export const useInputBoxComponent = () => {

    const id = 'input-box'
    const name = 'layout.components.inputbox.name'

    const params = useLayoutParameters()
    const { ll } = useLayoutSettings()

    const config = useConfig()

    const referenceresolver = useLazyFieldReferenceResolver()

    return useSpecOf({

        id,
        icon: icns.form,
        name,


        generate: (): InputBox => ({

            ...newComponentFrom(id),
            ...partialField(),
            style: {},
            multi: true,
            rich: true
        })

        ,

        styles: current => ({

            [textStyleId]: {},
            // defaults to an absolute scale.
            [inputWidthStyleId]: { default: 300, min: 200, max: 500, percentage: 100, ...current[inputWidthStyleId] },
            // bleeds by default, but supports an absolute scale that trackes the input.
            [widthStyleId]: { default: 305, min: 205, max: 505, percentage: 100, ...current[widthStyleId] },
            [heightStyleId]: {},
            [borderStyleId]: {},
            [alignmentStyleId]: {},
            [spacingStyleId]: { top: 5, bottom: 5, ...current[spacingStyleId] }

        })

        ,

        staticDataOf: (c: InputBox) => {

            const { languages } = config.get().intl

            return c.multi ? languages.reduce((acc, l) => ({ ...acc, [l]: undefined }), {}) : undefined

        }

        ,


        // [since 11/2024] materialises defaults to replace missing content on submit.
        // considers both derived defaults (field references) and configured defaults, in that order.
        // for configured defaults, resolves parameters.
        submittedDataOf: (component, { data }, parameters) => {

            const { resolved } = referenceresolver(component)

            const { multi, defaultValue } = component

            // for multilang fields, resolution is lang by lang. 
            if (multi)

                return {
                    data: allLanguages.reduce((acc, lng) => ({

                        ...acc,

                        // note: ?? preserves blanked content (eg. 'I don't want this default' or 'No, nothing to report here').
                        [lng]: data?.[lng] ?? resolved?.[lng] ?? params.rr(defaultValue?.[lng], parameters)

                    }), {})
                }


            // for mono-lang fields, the configured default is in the layout language.
            return data ?? resolved ?? params.rr(ll(defaultValue), parameters)
        }

        ,


        // exposes contents for resolving mentions in Asset Sections from external layouts, typically in products.
        // note: the context available in the original layout may not necesseraly exist in the external layout.
        // however, submittedDataOf() should have captured it on submit, materialised it as content, and made it unnecessary from the external layout/ 
        dynamicDataOf: (component, { data }, parameters) => {

            const { multi, defaultValue } = component

            // note: since 11/2024 falling back onto defaults is no longer necessary, 
            // because defaults are materialised as content on submit (cf. submittedDataOf).
            // however, we must remain retro-compatible for earlier submissions where defaults were not materiaised.
      
            if (!multi)
                 // note: ?? preserves blank content.
                return data ?? params.r(ll(defaultValue), parameters)

            // best effort: if we dont find a specific translation use the first available one.
            // (why: there is no fallback logic in mention resolution, we must prepare the data for each language manually)
            const fallbackLanguage = allLanguages.find(l => data?.[l]) ?? '' 

            return allLanguages.reduce((acc, lng) => ({

                     ...acc, 
                     
                     // note: ?? preserves blanked content but || doesn't.
                     // so: emptying a field is preserved (eg. 'I don't want this default' or 'No, nothing to report here').
                     // but emptying a default isn't necessarily (eg 'I want to unconfigure a default for this field').
                     [lng]: data?.[lng] ??  ( params.rr(defaultValue?.[lng]) || data?.[fallbackLanguage] ) })
                     
                     , {})
            



        }

        ,
        Preview
        ,
        Render
        ,
        Detail
        ,
        Print


    })
}



function Preview(props: PreviewProps<InputBox>) {

    const { path, component } = props

    const { defaultValue } = component

    // default value is honoured only on mount, so we change the key to remount the component and show its changes.
    //eslint-disable-next-line
    const key = React.useMemo(() => Date.now(), [defaultValue])

    return <ComponentDroppable path={path}>
        <ComponentDraggable path={path} component={component}>
            <ComponentPreview path={path} component={component}>
                <Render key={key} component={component} />
            </ComponentPreview>
        </ComponentDraggable>
    </ComponentDroppable>
}





function Render(props: RenderProps<InputBox>) {

    const { component } = props

    const { id, multi, label, toolbar = true } = component

    const { styles, settings: { resolveLanguageFor } } = useLayoutApis()

    const { multir } = useParameters()

    const { data, defaultValue, validation, onChange } = useInputBoxDataHelper(component)

    const { placeholder, readonly, canUnlock, readOnlyMode, enabledOnReadonly } = useFieldHelper(component)

    const lbl = useLayoutLabel(label)

    // const splaceholder = component.placeholder ? r(ll(component.placeholder)) : undefined
    const mplaceholder = component.placeholder ? multir(component.placeholder) : undefined


    const currentLangForMulti = multi ? resolveLanguageFor(data) as Language : undefined

    const noToolbar = !toolbar

    const onInnerChange = (v: any) => {

        if (typeof v === 'string') onChange(cleanUpHtml(v))

        else if (typeof v === 'object') onChange(Object.keys(v).reduce((acc, cur) => ({ ...acc, [cur]: cleanUpHtml(v[cur]) }), {}))

        else onChange(v)
    }

    const style = styles.resolveCustomStyles(component)

    return <ChangelogRender {...props}>
        <StyledBox component={component}>{

            multi ?

                <MultiBox placeholder={mplaceholder} style={style} enabledOnReadOnly={enabledOnReadonly} noToolbar={noToolbar} readOnlyLang={currentLangForMulti} defaultValue={defaultValue as MultilangDto} noImage rte id={id} readonly={readonly} canUnlock={canUnlock} readonlyMode={readOnlyMode} label={lbl} validation={validation} onChange={onInnerChange} withNoTextColor>
                    {data}
                </MultiBox>

                :

                <RichBox placeholder={placeholder} style={style} enabledOnReadOnly={enabledOnReadonly} noToolbar={noToolbar} defaultValue={defaultValue as string} noImage label={lbl} readonly={readonly} readonlyMode={readOnlyMode} canUnlock={canUnlock} validation={validation} onChange={onInnerChange} withNoTextColor>
                    {data}
                </RichBox>

        }</StyledBox>
    </ChangelogRender>
}



function Detail(props: DetailProps<InputBox>) {

    const t = useT()

    const { component, onChange } = props

    const { multi, toolbar = true, defaultValue } = component

    const referenceFilter = React.useCallback((field: InputBox) => component.multi === field.multi && component.rich === field.rich, [component.multi, component.rich])

    return <>

        <Switch label={t("layout.components.inputbox.multilang.name")} onChange={v => onChange({ ...component, /* oneRequired: false,  */multi: v })}
            validation={{ msg: t("layout.components.inputbox.multilang.msg") }}>
            {!!multi}
        </Switch>


        <Switch label={t("layout.components.inputbox.toolbar.name")} onChange={v => onChange({ ...component, toolbar: v })}
            validation={{ msg: t("layout.components.inputbox.toolbar.msg") }}>
            {!!toolbar}
        </Switch>

        <ReferenceFormFields {...props} filter={referenceFilter} />

        <ParametricMultiTextBox label={t("layout.components.fields.default_value.name")} id={`inputbox-placeholder-${component.id}`} noImage onChange={v => onChange({ ...component, defaultValue: v })}
            validation={{ msg: t("layout.components.fields.default_value.msg") }}>
            {defaultValue}
        </ParametricMultiTextBox>

    </>
}


export const useInputBoxDataHelper = (component: InputBox) => {

    const layout = useLayout()

    const { config, settings } = useLayoutApis()

    const reference = useFieldReferenceResolver<InputBox, any>(component)

    const { dataFor, changeDataFor } = useComponentData()

    let referencedDefault = reference.resolved

    const { r, multir } = useParameters()

    const { ll } = settings

    const { multi, defaultValue: initialDefaultValue } = component

    const resolvemode = isInstance(layout) && config.mode === 'live' ? 'livedata' : component.rich ? 'markup' : 'text'

    const defaultValue =  referencedDefault ?? (initialDefaultValue ? multi ? multir(initialDefaultValue, resolvemode) : r(ll(initialDefaultValue), resolvemode) : undefined)

  
    const data = dataFor(component)

    const statusmsg = component.msg ? r(ll(component.msg)) : undefined

    const validation = { msg: statusmsg }

    return { data, onChange: v => changeDataFor(component, v), defaultValue, validation }
}


function Print(props: PrintProps<InputBox>) {

    const { component, wrap = true, View, StyleSheet } = props

    const { ll } = useLayoutSettings()

    const { convertHtmlToPdfComponents } = pdfHelper(props)

    const { readonlyFallback } = useFieldHelper(component)

    const { data, defaultValue } = useInputBoxDataHelper(component)

    const { multi } = component

    const mergedData = multi ? { ...defaultValue as MultilangDto, ...data as MultilangDto } : undefined

    const dataToPrint = multi ? ll(mergedData) : data ?? defaultValue

    const inputBoxStyle = StyleSheet.create(usePdfStyles(component, "inputBox"))

    return <StyledView {...props} component={component}>
        <View wrap={wrap} style={inputBoxStyle}>{convertHtmlToPdfComponents(cleanUpHtml(dataToPrint ?? readonlyFallback))}</View>
    </StyledView>
}
