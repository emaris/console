import { useT } from '#app/intl/api';
import { useBytestreamedHelper } from '#app/stream/BytestreamedHelper';
import { Bytestream, Bytestreamed, bytestreamFor, descriptorFrom, Document, DocumentChange, FileDescriptor, localisedStreamOf } from "#app/stream/model";
import { indexMap } from "#app/utils/common";
import { useFeedback } from '#app/utils/feedback';
import { useLayoutApis } from '#layout/apicontext';
import { backgroundStyleId } from "#layout/style/background";
import { borderStyleId } from "#layout/style/border";
import { discreteWidthStyleId } from "#layout/style/discretewidth";
import { spacingStyleId } from "#layout/style/spacing";
import merge from 'deepmerge';
import produce from "immer";
import { useContext } from 'react';
import { LayoutContext, LayoutSettingsContext, RenderLoopContext } from "../context";
import { LayoutData, whenInstance } from "../model";
import { useLayoutRegistry } from "../registry";
import { RowSectionContextProviderType } from "./hooks";
import { Component, ComponentSpec, Container, isContainer } from "./model";
import { Page, usePageComponent } from "./page";
import { Cell, CellDnd, Row, useCellComponent } from "./row";
import { RowSectionSpec } from "./rowsection";





export const useLayoutComponents = () => {

    const t = useT()

    const fb = useFeedback()

    const layoutRegistry = useLayoutRegistry()

    const documentHelper = useBytestreamedHelper()

    const { layout, change } = useContext(LayoutContext)


    const { lookupComponent } = layoutRegistry

    const pagecomponent = usePageComponent()

    const self = {


        // validate: () => withReport(recvalidate(layout.components)),

        allPages: () => self.allOf(pagecomponent)

        ,


        changeWith: (updater: (_: Container) => void, documentupdater: (_: Record<string, Document[]>) => void = () => { }) => change({ ...layout, documents: produce(layout.documents, documentupdater), components: produce(layout.components, updater) })

        ,

        componentSpecOf: (c: Component) => lookupComponent(c.spec)

        ,

        pathAt: (position: number) => `${position}`

        ,

        pathFor: (c: Component) => {

            const searchCompo = (source: Component, c: Component) => {
                if (source.id === c.id) {
                    return []
                } else if (source.hasOwnProperty('children') && source['children'] && source['children'].length > 0) {
                    for (var i = 0; i < source['children'].length; i++) {
                        var path = searchCompo(source['children'][i], c);
                        if (path !== null) {
                            path.unshift(i);
                            return path;
                        }
                    }
                }
                return null;
            }

            const path = searchCompo(layout.components, c)

            return path !== null ? path.join('@') : ''


        }

        ,

        extend: (path: string) => ({ at: (position: number) => `${path}@${position}` })

        ,

        textOf: (c: Component) => self.componentSpecOf(c).textOf(c)

        ,

        //  resolves a path into a pair {container, position} where to place or find a child.
        resolve: (path: string) => ({

            inCurrentLayout: () => self.resolve(path).in(layout.components)

            ,

            in: (root: Container) =>

                function $rec(parent: Container, segment: string): { parent: Container, position: number } {

                    const [position, tail] = segment.split(/@(.+)/)     // splits at the first @ only

                    if (tail) {

                        const child = parent.children[position]

                        if (!child)
                            throw Error(`invalid path ${path}: no instance at ${position}, can't reach ${tail}`)

                        if (isContainer(child))

                            return $rec(child, tail)

                        else

                            throw Error(`invalid path ${path}: instance at ${position} has no children,can't reach ${tail}`)

                    }


                    return { parent, position: +position }  // converts to number


                }(root, path)
        })


        ,

        allOf: <C extends Component>(spec: ComponentSpec<C> | string) => self.flatten().filter(component => component.spec === (typeof spec === 'string' ? spec : spec.id)) as C[]

        ,

        flatten: (from?: Component) => {
            const start = from ? from['children'] : layout.components.children
            return (start ?? []).reduce((acc, x) => {
                acc = acc.concat(x);
                if (x) {
                    acc = acc.concat(self.flatten(x));
                    x = [];
                }
                return acc;
            }, [] as Component[]);
        }

        ,

        addComponent: (spec: string) => ({

            at: (path: string, with_?: any) =>

                self.changeWith(layout => {

                    const { parent, position } = self.resolve(path).in(layout)

                    const component = lookupComponent(spec).generate(t, with_)

                    parent.children.splice(position, 0, component)

                })

        })



        ,

        addPageWithComponent: (spec: string) =>


            self.changeWith(layout => {

                const component = lookupComponent(spec).generate(t)

                const newpage = pagecomponent.generate() as Page

                newpage.children.push(component)

                layout.children.push(newpage)


            })

        ,



        addPageWithComponentFrom: (path: string) =>
            self.changeWith(layout => {

                const { parent, position } = self.resolve(path).in(layout)

                const newpage = pagecomponent.generate()

                newpage.children.push(parent.children[position])

                layout.children.push(newpage);

                parent.children.splice(position, 1)

            })

        ,



        changeComponent: (path: string, component: Component) =>

            self.changeWith(layout => {

                const { parent, position } = self.resolve(path).in(layout)

                parent.children[position] = component

            })

        ,


        cloneComponent: (path: string) => {
            const { parent, position } = self.resolve(path).in(layout.components)

            const component = parent.children[position]

            const cloned = self.componentSpecOf(component).cloneFor(component)

            self.changeWith(layout => {

                const { parent, position } = self.resolve(path).in(layout)

                if (position + 1 === parent.children.length)
                    parent.children.push(cloned)
                else
                    parent.children.splice(position + 1, 0, cloned)

            })

            return cloned
        }

        ,

        removeComponent: (path: string) => {

            //console.log('removing path', path)

            const { parent, position } = self.resolve(path).inCurrentLayout()

            const singular = t(self.componentSpecOf(parent.children[position]).name).toLowerCase()

            fb.askConsent({

                title: t('common.consent.remove_one_title', { singular }),
                content: t('common.consent.remove_one_msg', { singular }),
                okText: t('common.consent.remove_one_confirm', { singular }),

                onOk: () => {

                    self.changeWith(layout => {

                        const { parent, position } = self.resolve(path).in(layout)

                        parent.children.splice(position, 1)


                    }, docs => {

                        const { parent, position } = self.resolve(path).inCurrentLayout()

                        const id = parent.children[position].id

                        const existingDocs = docs[id] ?? []

                        existingDocs.forEach(d => documentHelper.remove(d.id))

                        delete docs[id]

                    })

                }

            })

        }

        ,

        moveComponent: (fromPath: string, toPath: string) =>


            self.changeWith(layout => {


                const { parent: fromParent, position: fromPosition } = self.resolve(fromPath).in(layout)

                const component = fromParent.children[fromPosition]

                const { parent: toParent, position: toPosition } = self.resolve(toPath).in(layout)

                // if parent is same, order of operation can invalidate paths in input

                // if component is to 'move up' then we first remove it there, then reinsert it.
                if (fromParent.id === toParent.id && fromPosition > toPosition) {

                    fromParent.children.splice(fromPosition, 1)

                    toParent.children.splice(toPosition, 0, component)

                }

                // in all other case, we first copy it then remove the earlier instance.
                else {

                    toParent.children.splice(toPosition, 0, component)

                    fromParent.children.splice(fromPosition, 1)

                }
            })

    }



    return self;
}


export const useComponentData = () => {

    const { config, components, bytestreams } = useLayoutApis()

   
    const documentHelper = useBytestreamedHelper()

    const { layout, change } = useContext(LayoutContext)

    const aux = useContext(LayoutSettingsContext)

    const fileDescriptors = aux.get().fileDescriptors

    // immer-based helper for resource/file management
    const changeDescriptorsWith = (updater: (_: typeof fileDescriptors) => void) => aux.set(s => s.fileDescriptors = produce(s.fileDescriptors, updater))

    const changeResourcesWith = (updater: (_: { [id: string]: Bytestream[] }) => void) => whenInstance(layout).do(layout => change({ ...layout, state: { ...layout.state, resources: produce(layout.state.resources, updater) } }))
    const changeStateWith = (updater: (state: { data: LayoutData, resources: Record<string, Bytestream[]> }) => void) => whenInstance(layout).do(layout => change({ ...layout, state: produce(layout.state, updater) }))

    const editctx = useContext(RenderLoopContext)

    // const recvalidate = (c: Component): ValidationReport => isContainer(c) ?

    //     c.children.map(recvalidate).reduce((acc, next) => ({ ...acc, ...next }), {})

    //     :

    //     { [c.id]: self.componentSpecOf(c).useValidation?.(c) ?? {} }

    const self = {

        allData: () => whenInstance(layout).do(l => l.state.data)

        ,

        resetData: () => whenInstance(layout).promise(l => change({ ...l, state: { ...l.state, data: {} } }))

        ,

        dataFor: <T =any> (component: Component) => {

            const id = self.componentId(component)

            return whenInstance(layout).do(l => l.state.data[id]) as T

        }

        ,

        componentId: (component: Component | string) => {
            if (editctx)
                return typeof component === 'string' ? `${editctx}-${component}` : `${editctx}-${component.id}`
            else
                return typeof component === 'string' ? component : component.id
        }

        ,

        changeDataFor:  <T =any> (component: Component, value:T) =>

            whenInstance(layout).do(l => {

                const id = self.componentId(component)

                change({
                    ...l, state: {
                        ...l.state, data: produce(l.state.data, data => {

                            if (value === undefined)        // could well be 'blank'.
                                delete data[id]
                            else
                                data[id] = value


                        })
                    }
                })
            }
            )


        ,

        allResources: () => whenInstance(layout).do(l => l.state.resources)

        ,


        removeResourcesFor: (component: Component, resources: Bytestream[]) => whenInstance(layout).do(l => {

            const ids = resources.map(r => r.id)

            const id = self.componentId(component)

            changeDescriptorsWith(descriptors => {

                const currentDescriptors = descriptors[id] ?? []
                const modifiedDescriptors = currentDescriptors.filter(d => !ids.includes(d.resource))

                if (modifiedDescriptors.length === 0)
                    delete descriptors[id]
                else
                    descriptors[id] = modifiedDescriptors

            })


            changeResourcesWith(resources => {

                const modifiedResources = self.resourcesFor(component)!.filter(r => !ids.includes(r.id))

                if (modifiedResources.length === 0)
                    delete resources[component.id]
                else
                    resources[component.id] = modifiedResources
            })


        })

        ,

        // awkward api: if data is omitted it gets a default value other than 'undefined', which instead means 'blank' it.
        removeResourcesAndDataFor: (component: Component, resources: Bytestream[], data: any) => whenInstance(layout).do(l => {

            const ids = resources.map(r => r.id)

            const id = self.componentId(component)

            changeDescriptorsWith(descriptors => {

                const currentDescriptors = descriptors[id] ?? []
                const modifiedDescriptors = currentDescriptors.filter(d => !ids.includes(d.resource))

                if (modifiedDescriptors.length === 0)
                    delete descriptors[id]
                else
                    descriptors[id] = modifiedDescriptors

            })

            changeStateWith(state => {

                if (data !== noDataChange)
                    if (data)
                        state.data[id] = data
                    else
                       delete state.data[id]
                    
                const modifiedResources = self.resourcesFor(component)!.filter(r => !ids.includes(r.id))

                if (modifiedResources.length === 0)
                    delete state.resources[id]
                else
                    state.resources[id] = modifiedResources
            })


        })

        ,


        // awkward api: if data is omitted it gets a default value other than 'undefined', which instead means 'blank' it.
        addResourcesAndDataFor: (component: Component, resourcesAndDescriptors: { resource: Bytestream, descriptor: FileDescriptor }[], data: any = noDataChange) => whenInstance(layout).do(l => {

            const id = self.componentId(component)

            changeDescriptorsWith(descriptors => {

                const currentDescriptors = descriptors[id] ?? []
                const modifiedDescriptors = [...currentDescriptors, ...resourcesAndDescriptors.filter(_=>_.descriptor).map(_ => _.descriptor)]

                descriptors[id] = modifiedDescriptors

            })


            changeStateWith(state => {

                if (data !== noDataChange)
                    if (data)
                        state.data[id] = data
                    else
                        delete state.data[id]

                const modifiedResources = [...self.resourcesFor(component) ?? [], ...resourcesAndDescriptors.map(_ => _.resource)]
                state.resources[id] = modifiedResources
            })

        })

        ,

        resourcesAndDescriptorsFor: (files: File[]) => {

            const { submission, party } = config


            return files.map(f => {
                const resource = { ...bytestreamFor(f), ref: submission?.id, tenant: party?.source } as Bytestream
                return { resource, descriptor: descriptorFrom(f, resource) }
            })

        }


        ,


        addResourcesFor: (component: Component, newfiles: File[]) => whenInstance(layout).do(l => {


            const newones = self.resourcesAndDescriptorsFor(newfiles)

            const id = self.componentId(component)

            changeDescriptorsWith(descriptors => {

                const currentDescriptors = descriptors[id] ?? []
                const modifiedDescriptors = [...currentDescriptors, ...newones.map(_ => _.descriptor)]

                descriptors[id] = modifiedDescriptors

            })

            changeResourcesWith(resources => {

                const modifiedResources = [...self.resourcesFor(component)!, ...newones.map(_ => _.resource)]
                resources[id] = modifiedResources
            })

            return newones

        })


        ,

        resourcesFor: (component: Component | string) =>
            whenInstance(layout).do(l => l.state.resources[self.componentId(component)])

        ,

        descriptorsFor: (component: Component | string) => {
            const id = self.componentId(component)
            return fileDescriptors[id]
        }

        ,

        allDescriptors: () => fileDescriptors



        ,

        uploadResources: () => Promise.resolve(whenInstance(layout).do(l => {

            const resourceMap = indexMap(Object.values(l.state.resources).flat()).by(r => r.id)
            const streamsAndBlobs = Object.values(fileDescriptors).flat().map(d => [({ id: d.resource, stream: resourceMap[d.resource] }), d.file] as [Bytestreamed, File])

            return bytestreams.upload(streamsAndBlobs);
        }))

        ,

        removeDescriptors: () => aux.set(s => s.fileDescriptors = {})

        ,


        allDocumentsFor: (id: string) => layout.documents?.[id] ?? [],


        updateDocumentFor: (id: string, doc: Document, streamchange: DocumentChange) => {

            if (streamchange)
                documentHelper.updateWith(localisedStreamOf(doc, streamchange.lang), streamchange)

            components.changeWith(() => { }, docs => {

                if (docs[id])
                    docs[id].splice(docs[id].findIndex(d => d.id = doc.id), 1, doc)
                else
                    docs[id] = [doc]


            })

        }



    }



    return self;
}


export const useLayoutRows = () => {

    const t = useT()

    const { components } = useLayoutApis()

    const cellcomponent = useCellComponent()

    const self = {

        getStyleForCell: (cell: Cell) => {
            let style = {}

            style = cell.style[backgroundStyleId] ? { ...cell, [backgroundStyleId]: cell.style[backgroundStyleId] } : style
            style = cell.style[borderStyleId] ? { ...cell, [borderStyleId]: cell.style[borderStyleId] } : style
            style = cell.style[spacingStyleId] ? { ...cell, [spacingStyleId]: cell.style[spacingStyleId] } : style
            style = cell.style[discreteWidthStyleId] ? { ...cell, [discreteWidthStyleId]: cell.style[discreteWidthStyleId] } : style

            return style

        }

        ,

        newCellFor: (row: Row, asTemplate: boolean = false): Row => {

            const alias = self.cellAlias(row.children)
            const inheritatedStyles = row.children.length > 0 ? self.getStyleForCell(row.children[row.children.length - 1]) : {}
            const newCell = { ...cellcomponent.generate(t), alias, asTemplate, index: row.children ? row.children.length : 0, style: inheritatedStyles }
            return { ...row, children: [...row.children, newCell] }
        }

        ,

        addCellToRow: (row: Row): Row => {
            const path = components.pathFor(row)
            const newRow = self.newCellFor(row)
            components.changeComponent(path, newRow)
            return newRow
        }

        ,

        addCellToSectionRows: (rowSection: RowSectionSpec): RowSectionSpec => {

            const newCell = cellcomponent.generate(t)

            const inheritatedStyles = rowSection.cells.length > 0 ? rowSection.cells[rowSection.cells.length - 1].style : {}

            const virtualCell = {
                ...newCell,
                id: `${newCell.id}-${rowSection.id}`,
                alias: self.cellAlias(rowSection.cells),
                virtual: true,
                index: rowSection.cells ? rowSection.cells.length : 0,
                style: inheritatedStyles
            }

            const modifiedSection = {
                ...self.modifyRowsInRowSection(rowSection, (row) => row.asTemplate ? self.newCellFor(row, true) : row),
                cells: [...rowSection.cells, virtualCell]
            }

            const path = components.pathFor(rowSection)

            components.changeComponent(path, modifiedSection)

            return modifiedSection

        }

        ,

        removeCellFromRow: (row: Row, idx: number): Row => {
            const path = components.pathFor(row)
            const newRow = { ...row, children: row.children.filter((_, i) => i !== idx) } as Row
            components.changeComponent(path, newRow)
            return newRow
        }



        ,

        removeCellFromSectionRows: (rowSection: RowSectionSpec, idx: number) => {
            // const children = rowSection.children.map(row => ({...row, children: row.children.filter((_,i)=>i!==idx)}))
            const cells = rowSection.cells.filter((_, i) => i !== idx)
            const modifiedSection = { ...self.modifyRowsInRowSection(rowSection, (row) => row.asTemplate ? { ...row, children: row.children.filter((_, i) => i !== idx) } : row), cells } as RowSectionSpec
            const path = components.pathFor(rowSection)
            components.changeComponent(path, modifiedSection)
            return modifiedSection
        }

        ,

        moveCells: (cells: Cell[], to: string, item: CellDnd): Cell[] => cells.map((c, i) => {
            if (item.index === i) return cells[to]
            else if (parseInt(to) === i) return cells[item.index]
            else return c
        })


        ,

        moveCellInRow: (row: Row, to: string, item: CellDnd): Row => {
            const children = self.moveCells(row.children, to, item)
            const path = components.pathFor(row)
            const newRow = { ...row, children } as Row
            components.changeComponent(path, newRow)
            return newRow
        }

        ,

        moveCellInSectionRows: (rowSection: RowSectionSpec, to: string, item: CellDnd): RowSectionSpec => {

            const modifiedSection = self.modifyRowsInRowSection(rowSection, (row) => row.asTemplate ? ({ ...row, children: self.moveCells(row.children, to, item) }) : row)

            const cells = self.moveCells(rowSection.cells, to, item)

            return { ...modifiedSection, cells } as RowSectionSpec
        }

        ,

        cellAlias: (cells: Cell[]) => {
            const exists = (alias: string): boolean => cells.some(c => c.alias && (c.alias.toLowerCase() === alias.toLowerCase()))
            const getAlias = (idx: number): string => `${t('layout.components.row.cell.name')}-${idx}`

            let incr = 1
            let alias = getAlias(incr)

            while (exists(alias)) {
                incr = incr + 1
                alias = getAlias(incr)
            }

            return alias
        }

        ,

        allRows: (component: Component & Container) => {
            const findRows = (component) => {
                if (!component.children) return []
                return [
                    ...component.children.filter(compo => compo.spec === 'row'),
                    ...component.children.filter(compo => compo.spec !== 'row').map(compo => findRows(compo)).flatMap(rows => [...rows])]
            }

            return {
                apply: (changeFun: (_: any) => any) => {
                    return findRows(component).map(changeFun)
                }
                ,
                get: () => findRows(component)
            }
        }

        ,

        modifyRowsInRowSection: (component: Component & Container, processor: (_: any) => any) => {

            const findRows = (component, changeFun: (_: any) => any) => {
                if (!component.children) return component
                return {
                    ...component,
                    children: [...component.children.filter(compo => compo.spec === 'row').map(changeFun),
                    ...component.children.filter(compo => compo.spec !== 'row').map(compo => findRows(compo, changeFun))]
                }
            }

            return findRows(component, processor)
        }

        ,

        reconcileCellsWithRowSectionTemplate: (cells: Cell[], template: RowSectionContextProviderType): Cell[] => {
            const templateCell = template.rowSectionContext.cells ?? []
            const currentCells = cells ?? []

            const nextIndex = (cells: Cell[], from: number) => {
                const getIdx = (cells: Cell[], idx) => cells.findIndex(cell => cell.index === idx)
                let idx = from
                while (getIdx(cells, idx) > -1) idx += 1
                return idx
            }

            const unOrderedReconcile = (currentCells.length > templateCell.length) ?
                currentCells.slice(0, templateCell.length).map((cell, idx) => ({ ...cell, alias: templateCell[idx].alias }))
                : (currentCells.length === templateCell.length) ? currentCells
                    : (currentCells.length < templateCell.length) ?
                        [...currentCells, ...templateCell.slice(currentCells.length).map((_, idx) => ({ ...cellcomponent.generate(t), index: nextIndex(currentCells, idx), alias: undefined }))]
                        : []

            return unOrderedReconcile.map((cell, idx) => templateCell[idx].index === cell.index ? cell : unOrderedReconcile.find(c => c.index === templateCell[idx].index) ?? cell)
                .map((cell, idx) => ({
                    ...cell, asTemplate: true, virtual: false,
                    alias: cell.alias ??
                        templateCell[idx].alias
                }))
        }

        ,

        getRowStyle: (row: Row, rowSection: RowSectionSpec | undefined) =>
            rowSection ?
                row.asTemplate ?
                    merge({ style: row.style, extraStyle: row.extraStyle }, { style: rowSection.style, extraStyle: rowSection.extraStyle })
                    :
                    merge({ style: rowSection.style, extraStyle: rowSection.extraStyle }, { style: row.style, extraStyle: row.extraStyle })
                :
                { style: row.style, extraStyle: row.extraStyle }

        ,

        getRowStyleTemplate: (row: Row, rowSection: RowSectionSpec | undefined): Cell[] => {

            const withRowSectionStyles = self.getRowStyle(row, rowSection)

            return (rowSection && row.asTemplate) ?
                rowSection.cells ? rowSection.cells.map(cell => ({ ...cell, style: merge(withRowSectionStyles.extraStyle ?? {}, cell.style) })) : []
                :
                row.children.map(cell => ({ ...cell, style: merge(withRowSectionStyles.extraStyle ?? {}, cell.style) }))



        }

    }

    return self

}


export const noDataChange = Symbol()