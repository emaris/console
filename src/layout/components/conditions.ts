import { TagExpression } from '#app/tag/model'
import { TenantAudience } from '#app/tenant/AudienceList'
import { AssetInstance, Campaign } from '#campaign/model'
import { Submission } from '#campaign/submission/model'
import { useLayoutApis, useLiveLayoutApis } from '#layout/apicontext'
import { useLayoutParameters } from '#layout/parameters/api'
import { predefinedPartyId } from '#layout/parameters/constants'
import { ProductDto } from '#product/model'
import { RequirementDto } from '#requirement/model'
import { useContext } from 'react'
import { defaultSharedKey } from './conditionalsection'
import { LayoutConditionContext } from './context'

type ExpressionType = 'tag' | 'assessment' | 'lineageassessment' | 'tenant' | 'tenantgroup'


export type LayoutConditions = LayoutCondition[]


export type LayoutCondition<T = any> = {

    type: ExpressionType
    expression: T

}

export type TagTerm = LayoutCondition<TagExpression>
export type AssesmentTerm = LayoutCondition<TagExpression>
export type TenantGroupTerm = LayoutCondition<TagExpression>
export type TenantTerm = LayoutCondition<TenantAudience>

type ValueMap = {


    tags: string[]
    assessment: string[]
    lineageassessment: string[]
    tenantId: string | undefined
    tenantTags: string[]

}

export type LiveData = {

    submission: Submission
    asset: AssetInstance | undefined
    campaign: Campaign

}

export const useConditionMatch = () => {


    const context = useContext(LayoutConditionContext)
    const { parameters, tenantstore } = useLayoutApis()

    const match = useMatchHelper()

    const api = {

        matchContext: (sharedkey: string = defaultSharedKey) => api.match(context?.get()[sharedkey]?.conditions ?? [])

        ,

        match: (conditions: LayoutConditions = []) => {

            const self = {

                givenAsset: (asset: RequirementDto | ProductDto, debug?: 'debug') => {

                    const partyParam = parameters.lookupParameterById(predefinedPartyId)

                    return match(conditions, {

                        tags: asset?.properties.submissionTagMap?.flatMap(tagMap => tagMap.tags) ?? [],
                        assessment: [],
                        lineageassessment: [],
                        tenantId: partyParam?.value,
                        tenantTags: tenantstore.safeLookup(partyParam?.value)?.tags

                    }, debug)

                }



                ,

                givenAssets: (assets: (RequirementDto | ProductDto)[], debug?: 'debug') => assets.reduce((acc, asset) => acc && self.givenAsset(asset, debug), true)


            }

            return self

        }

    }

    return api

}



export const useLiveConditionMatch = () => {

    const context = useContext(LayoutConditionContext)

    const match = useMatchHelper()

    const { tenantstore, lineage } = useLiveLayoutApis()

    const parameters = useLayoutParameters()


    const api = {


        matchContext: (sharedkey: string = defaultSharedKey) => api.match(context?.get()[sharedkey]?.conditions ?? [])

        ,

        match: (conditions: LayoutConditions = []) => {

            const self = {

                givenSubmission: (data: LiveData, debug?: 'debug') => {

                    const { submission, asset, campaign } = data

                    const tags = submission?.lifecycle.tags ?? Object.values(asset?.properties.submissionTagMap ?? {}).flatMap(t => t.tags)

                    const assessment = submission?.lifecycle?.compliance?.state


                    // use a single lineage for now
                    const lineageassessment = campaign ?
                        lineage.on(campaign).given({ trail: submission?.trail }).assets[0]?.submission?.lifecycle.compliance?.state
                        : undefined

                    const partyParam = parameters.lookupParameterById(predefinedPartyId)

                    return match(conditions, {

                        tags: tags ?? [],
                        assessment: assessment ? [assessment] : [],
                        lineageassessment: lineageassessment ? [lineageassessment] : [],
                        tenantId: partyParam?.value,
                        tenantTags: tenantstore.safeLookup(partyParam?.value)?.tags
                    }
                        , debug)
                }

                ,


                givenSubmissions: (submissions: LiveData[], debug?: 'debug') =>

                    submissions.reduce((acc, data) => acc && self.givenSubmission(data, debug), true)

            }

            return self

        }

    }

    return api

}




const useMatchHelper = () => {

    const { tags } = useLayoutApis()


    // matcher shared between asset check and submission checks
    return (conditions: LayoutConditions, valuemap: ValueMap, debug?: 'debug') => {

        debug && console.log("matching", { conditions, values: valuemap })

        const overall = conditions.reduce((acc, term) => {

            // no point testing the rest
            if (!acc)
                return false

            let match = true
            switch (term.type) {

                case 'tag':

                    match = tags.expression(term.expression).matches({ tags: valuemap.tags })
                    break

                case 'assessment':

                    match = tags.expression(term.expression).matches({ tags: valuemap.assessment });
                    break

                case 'lineageassessment':

                    match = tags.expression(term.expression).matches({ tags: valuemap.lineageassessment });
                    break

                case 'tenantgroup':

                    match = tags.expression(term.expression).matches({ tags: valuemap.tenantTags });
                    break

                case 'tenant': {


                    if (!valuemap?.tenantId)
                        return true

                    const expression = (term as TenantTerm).expression

                    const includes = expression.includes ? expression.includes.includes(valuemap.tenantId) || expression.includes.length === 0 : true
                    const excludes = expression.excludes ? !expression.excludes.includes(valuemap.tenantId) || expression.excludes.length === 0 : true

                    match = includes && excludes
                    break

                }
                default: match
            }

            debug && console.log(`matching ${term.type}`, match)

            return match

        }, true as boolean)

        return overall
    }

}
