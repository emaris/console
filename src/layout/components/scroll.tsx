
import { useLayoutApis } from '#layout/apicontext';
import { LayoutConfigContext, RenderLoopContext } from '#layout/context';
import { useLayoutSettings } from '#layout/settings/api';
import * as React from 'react';
import { flushSync, unstable_batchedUpdates } from 'react-dom';
import { Component } from './model';
import { rowspecid } from './row';
import "./styles.scss";

export const defaultPlaceholderHeight = 300

export const LayoutScrollContext = React.createContext<LayoutScrollSubscriptionApi>(undefined!)


export type LayoutScrollSubscriptionApi = {

    subscribe: (subscription: LayoutScrollSubscription) => void

    unsubscribe: (element: Element) => void

    height: (componentId: string) => ({

        report: (height: number) => void,

        get: () => number
    })
}

const scrollapi = (heightMap: Map<string, number>, root = document.getElementById('layout-main')) => {

    const subscriptions = new WeakMap<Element, LayoutScrollSubscription>()

    const callback = (entries: IntersectionObserverEntry[]) => {

        flushSync(() => unstable_batchedUpdates(() => {

            entries.forEach(e => {

                const subscription = subscriptions.get(e.target)

                if (!subscription)
                    return

                subscription.setVisible(e.isIntersecting)

            })

        }))

    }

    const observerConf = { rootMargin: '700px 0%', root }

    const observer = new IntersectionObserver(callback, observerConf)

    return {

        subscribe: (subscription: LayoutScrollSubscription) => {

            // store subscription to notify it later as the observer reports on its visibility status.
            subscriptions.set(subscription.element, subscription)

            // register element with observer.
            observer.observe(subscription.element)

        },


        unsubscribe: (element: Element) => {

            subscriptions.delete(element)
            observer.unobserve(element)
        }

        ,

        disconnect: () => observer.disconnect()

        ,

        height: (componentId: string) => ({

            report: (height: number) => heightMap[componentId] = height,

            get: () => heightMap[componentId]

        })

    }
}



// layer 1: mounts optimisation enabling API and guards children with a spinner durng switch.
// spinner is custom, as global one is too expensive to toggle here (each render costs).
export const LayoutScrollProvider = (props: React.PropsWithChildren<{}>) => {
    
    const { children } = props

    const root = document.getElementById('layout-main')

    const heightMapRef = React.useRef(new Map<string, number>())

    const [api, setApi] = React.useState<LayoutScrollSubscriptionApi>(undefined!)

    React.useEffect(() => {

        const api = scrollapi(heightMapRef.current, root)

        setApi(api)

        return () => api.disconnect()

    }, [root])

    return <LayoutScrollContext.Provider value={api}>
        {children}
    </LayoutScrollContext.Provider>

}

export type LayoutScrollSubscription = {

    setVisible: (_: boolean) => any

    element: Element
}

export const LayoutScrollSubscriber = (props: React.PropsWithChildren<{ component: Component, height?: number, debug?: boolean }>) => {

    const config = React.useContext(LayoutConfigContext)

    const { component } = props

    const lazyMode = config?.lazyRender || component.spec === rowspecid  // no eager rows for now.

    // if (lazyMode)
    //     console.log("lazy", component.spec)

    return lazyMode ? <InnerLayoutScrollSubscriber {...props} /> : <>{props.children}</>
}


const InnerLayoutScrollSubscriber = (props: React.PropsWithChildren<{ component: Component, height?: number, debug?: boolean }>) => {

    const { component, children, height: placeholderHeight = defaultPlaceholderHeight, debug } = props

    const scrollContext = React.useContext(LayoutScrollContext)
    const ref = React.useRef<HTMLDivElement>(undefined!)

    const { components } = useLayoutApis()

    const { allCollapsed, allExpanded, expandComponent, collapseComponent } = useLayoutSettings()

    const loopctx = React.useContext(RenderLoopContext)

    const componentId = loopctx ? `${loopctx}-${component.id}` : component.id

    // even if we don't render the component we need to cascade to it the global collapse/expand as it happens.
    React.useEffect(() => {
        allCollapsed && collapseComponent(component)
        //eslint-disable-next-line
    }, [allCollapsed])

    React.useEffect(() => {
        allExpanded && expandComponent(component)
        //eslint-disable-next-line
    }, [allExpanded])

    const [visible, setVisible] = React.useState(false)

    React.useEffect(() => {

        scrollContext?.subscribe({ element: ref.current, setVisible })

    }, [scrollContext])


    // (un)subscribe on (un)mount.
    React.useEffect(() => {

        const thisref = ref.current

        return () => scrollContext?.unsubscribe(thisref)

    }, [scrollContext])


    React.useLayoutEffect(() => {

        if (visible) {

            const resizeObserver = new ResizeObserver(([{ target }]) => {

                const $ = getComputedStyle(target as HTMLElement)
                const h = [$.height, $.marginTop, $.marginBottom, $.paddingTop, $.paddingBottom].reduce((acc, next) => acc + parseInt(next, 10) || 0, 0)

                // doesn't set 0 height, to avoid scroll bumps (safari does this when component scrolls out of view).
                if (!ref.current.querySelector(':scope > .layout-subscriber-placeholder') && ref.current.children.length) {
                    ref.current.style.height = `${h}px`
                    scrollContext.height(componentId).report(h)
                }
            })

            resizeObserver.observe(ref.current.firstElementChild as HTMLElement)

            return () => resizeObserver.disconnect()

        }
        //eslint-disable-next-line
    }, [visible])

    const height = scrollContext?.height(componentId).get() ?? placeholderHeight

    debug && console.log("rendering", height, { ref: ref.current, visible })

    let contents = <div>{children}</div>

    if (!visible) {

        const { Offscreen } = components.componentSpecOf(component)

        contents = <div className="layout-subscriber-placeholder" style={{ height, width: 200 }}>
            <Offscreen component={component} />
        </div>
    }

    //The placeholder has a fixed width to avoid text pushing the layout over the boundaries.
    return <div ref={ref} style={{ height, width: "100%" }}>
        {contents}
    </div>

}
