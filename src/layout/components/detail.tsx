import { Drawer } from "#app/components/Drawer"
import { Form } from "#app/form/Form"
import { useFormState } from "#app/form/hooks"
import { useT } from '#app/intl/api'
import { Tab } from "#app/scaffold/Tab"
import { Topbar } from "#app/scaffold/Topbar"
import { paramsInQuery, updateQuery } from "#app/utils/routes"
import { useLayoutApis } from '#layout/apicontext'
import { isExtraStyled, isStyled, styleIcon } from "#layout/style/styledetail"
import { Icon } from "antd"
import { useRef, useState } from 'react'
import { MdDataUsage } from "react-icons/md"
import { useHistory, useLocation } from "react-router-dom"
import { FieldDetail, fieldIcon, isField } from "./fielddetail"
import { useRow, useRowSection } from "./hooks"
import { Component, DetailsTabs } from "./model"


export type DetailProps<C extends Component = Component> = {

    component: C
    onChange: (_: C) => void
    detailParam?: string
    width?: number
    children?: React.ReactNode | JSX.Element

}

export const detailParam = "component-drawer"
const tabParam = "component-tab"


// returns drawer controls and reconciles closing animation with client render optimisations.
// (clients renders only if drawer is visible, but we say it's visible until we've closed it, not as the route changes.)

export const useComponentDetail = (id: string) => {

    const history = useHistory()
    const { pathname, search } = useLocation()

    const { [detailParam]: detail } = paramsInQuery(search)

    const visible = id === detail

    // delayed copy of visbility flag,
    const delayedVisible = useRef(visible)

    // forces re-render after we've delayed reports of visbility change.
    const [, render] = useState(0)

    // syncs copy immediately on transition to visible.
    if (visible && !delayedVisible.current)
        delayedVisible.current = true

    // delays sync on transition to hidden.
    if (delayedVisible.current && !visible)
        setTimeout(() => {
            delayedVisible.current = false
            render(i => ++i)
        }, 200)
    

    const route = `${pathname}?${updateQuery(search).with(params => params[detailParam] = id)}`

    const show = () => history.push(route)

    return [show, delayedVisible.current, route] as const

}


export const ComponentDetail = (props: DetailProps) => {

    const t = useT()
    const history = useHistory()
    const { pathname, search } = useLocation()

    const { components } = useLayoutApis()

    const rowSection = useRowSection()
    const row = useRow()

    const { component, onChange } = props

    const innerOnChange = (c: Component) => {
        onChange(c)
        if (rowSection && c.spec === 'rowsection') rowSection.setRowSectionContext(c)
        if (row && c.spec === 'row') row.setRowContext(c)
    }

    const { icon, name, Detail, showTabsOnDetails, tabsOnDetails, StyleDetail, ExtraStyleDetail } = components.componentSpecOf(component)

    const computedDetailParam = props.detailParam ?? detailParam

    const computedTabParam = props.detailParam ? `${tabParam}-${props.detailParam}` : tabParam

    const { [computedDetailParam]: detail, [computedTabParam]: tab } = paramsInQuery(search)

    const setTab = (tab: string) => history.push(`${pathname}?${updateQuery(search).with(params => params[computedTabParam] = tab)}`)

    const formstate = useFormState(component)

    const visible = component.id === detail

    const close = () => history.push(`${pathname}?${updateQuery(search).with(params => { params[computedDetailParam] = null; params[computedTabParam] = null })}`)

    const showTabs = (tabsOnDetails && tabsOnDetails.length > 1) ? true : showTabsOnDetails

    const getSingleTab = () => {
        const tab: DetailsTabs = tabsOnDetails?.length === 1 ? tabsOnDetails[0] : 'data'

        switch (tab) {
            case 'data': return <Detail component={component} onChange={innerOnChange} />
            case 'field': return isField(component) && <FieldDetail component={component} onChange={innerOnChange} />
            case 'style': return isStyled(component) && <StyleDetail component={component} onChange={innerOnChange} />
            default: return <Detail component={component} onChange={innerOnChange} />
        }
    }

    const hasFieldTab = isField(component) ? tabsOnDetails !== undefined ? tabsOnDetails.includes('field') : true : false
    const hasStyleTab = isStyled(component) ? tabsOnDetails !== undefined ? tabsOnDetails.includes('style') : true : false
    const hasExtraStyleTab = isExtraStyled(component) ? tabsOnDetails !== undefined ? tabsOnDetails.includes('extrastyle') : true : false
    const hasDataTab = tabsOnDetails !== undefined ? tabsOnDetails.includes('data') : true


    // console.log('component', component)

    const defaultTab = (tabsOnDetails !== undefined && tabsOnDetails.length === 1) ? tabsOnDetails[0] : isField(component) ? 'field' : 'data'

    const currentTab = (tab && tab !== null) ? tab : defaultTab

    return <Drawer className={`component-detail ${component.id}`} width={props.width ?? 600} routeId={computedDetailParam} icon={typeof icon === 'function' ? icon(component) : icon} title={component['alias'] ?? t(name)} visible={visible} onClose={close}>

        {!showTabs ? <Form state={formstate}>{getSingleTab()}</Form> : <>

            <Topbar offset={63} onTabChange={setTab} activeTab={currentTab as string}>

                {hasFieldTab && <Tab id="field" icon={fieldIcon} name={t("layout.components.field.tabs.field")} />}
                {hasDataTab && <Tab id="data" icon={<Icon component={MdDataUsage} />} name={t("layout.components.field.tabs.data")} />}
                {hasStyleTab && <Tab id="style" disabled={component.asTemplate} icon={styleIcon} name={t("layout.components.field.tabs.style")} />}
                {hasExtraStyleTab && <Tab id="extrastyle" disabled={component.asTemplate} icon={styleIcon} name={component['extraStyle'].name} />}

            </Topbar>

            <Form state={formstate}>

                {currentTab === 'field' && isField(component) ?


                    <FieldDetail component={component} onChange={innerOnChange} />


                    :

                    currentTab === 'style' && isStyled(component) ?

                        <StyleDetail component={component} onChange={innerOnChange} />


                        :

                        currentTab === 'extrastyle' && isExtraStyled(component) ?

                            <ExtraStyleDetail component={component} onChange={innerOnChange} />


                            :

                            <Detail component={component} onChange={innerOnChange} />


                }

            </Form>

            {props.children && props.children}
        </>
        }
    </Drawer>
}