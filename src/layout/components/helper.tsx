import { HtmlSnippet } from "#app/components/HtmlSnippet"
import { Multilang, MultilangDto } from "#app/model/multilang"
import { stripHtmlTags } from "#app/utils/common"
import { useStable } from '#app/utils/function'
import { useLayoutApis } from '#layout/apicontext'
import { StyleDetail, isExtraStyled, isStyled } from '#layout/style/styledetail'
import { Fragment } from 'react'
import { useLayoutParameters } from '../parameters/api'
import { useParameters } from "../parameters/hooks"
import { ComponentButton } from "./button"
import { ComponentDraggable, ComponentDroppable, ComponentSpecDraggable } from "./dnd"
import { BaseComponentSpec, Component, ComponentSpec, DetailStyleProps, PDFProps, PreviewProps, RenderProps, clone, isContainer, newComponentFrom } from "./model"
import { ComponentPreview } from "./preview"



export const useSpecOf = <C extends Component>(spec: BaseComponentSpec<C> & Partial<ComponentSpec<C>>): ComponentSpec<C> => {

    const params = useLayoutParameters()

    const DefaultPreview = useStable(({ component, path }: PreviewProps<C>) =>

        <ComponentDroppable path={path}>
            <ComponentDraggable path={path} component={component}>
                <ComponentPreview path={path} component={component}>
                    <spec.Render component={component} />
                </ComponentPreview>
            </ComponentDraggable>
        </ComponentDroppable>

    )

    const DefaultButton = useStable(() =>

        <ComponentSpecDraggable spec={spec.id} type={spec.id} >
            <ComponentButton spec={spec} />
        </ComponentSpecDraggable>

    )

    const DefaultPrintPreview = useStable((props: PDFProps) => {

        const { View } = props

        return <View></View>

    })

    const DefaultPrint = useStable(((props: PDFProps) => {

        const { View, Text } = props


        return <View>

            <Text>{params.rr("<span class=\"mention\" contenteditable=\"false\" data-index=\"7\" data-denotation-char=\"@\" data-content-editable=\"false\" data-id=\"requirement.title\" data-value=\"requirement.title\">@requirement.title</span>")}</Text>
        </View>

    }))

    const DefaultStyleDetail = useStable(({ component, onChange }: DetailStyleProps<C>) => {

        return isStyled(component) ? <StyleDetail component={component} onChange={c => onChange(c as any)} /> : <></>

    })

    const DefaultExtraStyleDetail = useStable(({ component, onChange }: DetailStyleProps<C>) => {

        return isExtraStyled(component) ? <StyleDetail component={component} type='extrastyle' onChange={c => onChange(c as any)} /> : <></>

    })

    return {

        showTabsOnDetails: true

        ,

        tabsOnDetails: undefined

        ,

        cloneFor: (c: C) => clone(c)

        ,

        ...spec

        ,

        active: spec.active ?? true

        ,

        staticDataOf: spec.staticDataOf ?? (() => undefined)

        ,

        dynamicDataOf: spec.dynamicDataOf ?? ((_, componentstate) => componentstate.data)

        ,

        generate: spec.generate || (() => ({ ...newComponentFrom(spec), style: {} }) as any)

        ,


        nameOf: spec.nameOf || (() => ({ en: spec.name }))


        ,

        styles: spec.styles || (stylemap => stylemap)

        ,

        extraStyles: spec.extraStyles || (stylemap => ({ ...stylemap, name: '' }))

        ,


        Button: spec.Button || DefaultButton

        ,

        textOf: spec.textOf ?? (c => spec.Title ? <spec.Title component={c} /> : c.shortname)

        ,

        Offscreen: spec.Offscreen ?? BaseOffscreen

        ,

        Preview: spec.Preview ?? DefaultPreview

        ,

        Print: spec.Print ?? spec.PrintPreview ?? DefaultPrint

        ,

        PrintPreview: spec.PrintPreview ?? spec.Print ?? DefaultPrintPreview

        ,

        StyleDetail: spec.StyleDetail ?? DefaultStyleDetail

        ,

        ExtraStyleDetail: spec.ExtraStyleDetail ?? DefaultExtraStyleDetail

    }

}

export const BaseOffscreen = (props: Pick<RenderProps<Component>, 'component'>) => {

    const { components } = useLayoutApis()

    const { component } = props

    if (!isContainer(component))
        return <Fragment />

    return < Fragment>
        {component.children.map(child => {

            const Child = components.componentSpecOf(child)

            return <Child.Offscreen key={child.id} component={child} />

        })}
    </Fragment>
}


export type PdfHelperConfig = {
    debug?: boolean
    style?: any
    noWrap?: boolean
}


export const useLayoutLabel = (label: Multilang | MultilangDto) => {

    const { settings: { ll } } = useLayoutApis()

    const { rr } = useParameters()

    const value = rr(ll(label))

    return value === undefined || stripHtmlTags(value).trim() === '' ? undefined : <HtmlSnippet snippet={value} />


}