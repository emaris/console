
import { MultilangDto } from "#app/model/multilang"
import { Bytestream } from '#app/stream/model'
import { deepclone } from "#app/utils/common"
import { StyleMap } from "#layout/style/model"
import ReactPDF from '@react-pdf/renderer'
import { TFunction } from "i18next"
import * as React from "react"
import { Parameter } from '../parameters/model'
import { DetailProps } from "./detail"



export type RenderProps<C extends Component> = React.PropsWithChildren<{

    component:C

    className?:string
    style?: React.CSSProperties
}>

export type OffscreenProps<C extends Component> = Pick<RenderProps<C>,'component'>

export type PDFProps = Omit<typeof ReactPDF,'usePdf'>

export type PrintProps<C extends Component=Component> = RenderProps<C> & {wrap?: boolean} & PDFProps


export type PreviewProps<C extends Component=Component> =  React.PropsWithChildren<{

    component:C
    path:string
    id?: string

}>

export type DetailStyleProps<C extends Component=Component> =  React.PropsWithChildren<{

    component:C
    onChange: (_:C) => void

}>

export type BaseComponentSpec<C extends Component=Component> = {

    // internal identifier for relationship, primarily from components.
    id: string

    // human-readable string for display, primarily in toolbar.
    name: string

    icon?: JSX.Element | ((_?:C) => JSX.Element)

    Render: (_:RenderProps<C>) => JSX.Element

    Detail:(_: DetailProps<C>) => JSX.Element

}

export type DetailsTabs = 'field' | 'style' | 'data' | 'extrastyle'

//  behaviour common to all components of the same 'type'. 
export type ComponentSpec<C extends Component=Component> = BaseComponentSpec<C> & {


    styles: (_:StyleMap,___:C) => StyleMap

    extraStyles: (_:StyleMap,___:C) => StyleMap & {name: string}

    //  behaviours
    generate: (t?:TFunction, state?:any) => C
    
    nameOf: (_:C) => MultilangDto

    textOf: (_:C) =>  React.ReactNode

    Title?: (_:{component:C}) => JSX.Element

    Button:() => JSX.Element
    
    Preview:(_:PreviewProps<C>) => JSX.Element

    StyleDetail:(_:DetailStyleProps<C>) => JSX.Element

    ExtraStyleDetail:(_:DetailStyleProps<C>) => JSX.Element

    // useValidation?: (_:C) => Validation

    staticDataOf: (_:C) => any

    /**

        [since 11/2024] called to post-process component state just before it gets submitted.

        takes the current state — both data and resource parts — and returns the processed parts, if any.  
        takes also parameters to use in post-processing, and may return new ones for children to use in turn.

        it's mostly intended for fields, to materialise defaults into missing content.  
        the motivating use case for materialisation is *field reference*s, that resolve into defaults but must be mentioned as content from products.

        it's also implemented by sections that mount contextual parameters on render, so fields inside sections can use them to materialise defaults.  
        the motivating use case here is *Integration Sections*, which inject contextual parameters. 

    **/
    submittedDataOf?: ( component :C, state:{data:any, resources:Bytestream[]}, parameters: Parameter[] | undefined ) => Partial<{data:any | undefined, resources:Bytestream[] | undefined, parameters: Parameter[] | undefined }> | undefined
    
    /**

        called to post-process component state into a "mentionable" form, typically from Asset Sections in products.

        takes the current state — both data and resource parts — and returns it transformed, typically for the layout language.  
        takes also parameters to use in post-processing.

        note: as this is invoked from an external layout, not all original context may be available.
        howver, submittedDataOf() above can use the original context at the point of submission and materialise a value that no longer requires it.


    **/
    dynamicDataOf: (component:C, componentstate:{data:any, resources:Bytestream[]}, parameters: Parameter[]) => any

    Offscreen: (_:Pick<RenderProps<C>,'component'>) => JSX.Element

    Print: React.FC<PrintProps<C>>

    PrintPreview:  React.FC<PrintProps<C>>
    
    active: boolean

    showTabsOnDetails: boolean

    tabsOnDetails: DetailsTabs[] | undefined

    cloneFor: (c:C) => C

}





export type Component =  {

    id: string
    shortname?: string
    spec: string
    asTemplate?: boolean
}

export type Container<T extends Object=Component> = Component & {

    children: T[]

}


export const nameOf = (c: Component) => c.shortname?.replace(/\s/g, '-').toLowerCase() ?? c.id
   
// type guard
export const isContainer = (c: Component) : c is Container =>  !!(c as Container).children


export const newIdFor = ( spec: string) => {

    const floor = Math.floor(Math.random()*1000000) < 500000 ? true : false

    const rand = floor ? Math.floor(Math.random()*100) : Math.ceil(Math.random()*100) 
    const toAddForDate = floor ? Math.floor(Math.random()*5000) : Math.ceil(Math.random()*5000) 

    return `${spec}-${Date.now()+toAddForDate}${rand}`
}

export const newComponentFrom = <C extends Component> (spec:BaseComponentSpec<C> | string ) : Component => {

    const id = typeof spec === 'string' ? spec : spec.id

    return {
    
        id: newIdFor(id),
        spec:id
        
    } 

}

export const clone = (c:Component | Container) => {
    if (Object.keys(c).includes('children')) return {...deepclone(c), id: newIdFor(c.spec), children: (c as Container).children.map(clone)}
    return {...deepclone(c), id:newIdFor(c.spec) }


}
    
