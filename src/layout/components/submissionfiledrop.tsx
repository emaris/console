import { SelectBox } from "#app/form/SelectBox"
import { Switch } from "#app/form/Switch"
import { icns } from "#app/icons"
import { useT } from '#app/intl/api'
import { useIntl } from '#app/intl/store'
import { FileBox, FileBoxProps } from "#app/stream/FileBox"
import { useBytestreams } from "#app/stream/api"
import { FileTypeInfo, fileTypeInfo } from "#app/stream/constants"
import { Bytestream, FileDescriptor } from '#app/stream/model'
import { useTime } from '#app/time/api'
import { getHostname } from "#app/utils/common"
import { ChangelogRender } from '#campaign/submission/changerender'
import { useLiveLayoutApis } from '#layout/apicontext'
import { DetailProps } from '#layout/components/detail'
import { ComponentDraggable, ComponentDroppable } from '#layout/components/dnd'
import { FieldProps, partialField } from '#layout/components/fielddetail'
import { useFieldHelper } from '#layout/components/fieldhelper'
import { useLayoutLabel, useSpecOf } from '#layout/components/helper'
import { Component, PreviewProps, PrintProps, RenderProps, newComponentFrom } from '#layout/components/model'
import { ComponentPreview } from '#layout/components/preview'
import { useLayoutParameters } from '#layout/parameters/api'
import StyledView from '#layout/pdf/styledview'
import { usePdfStyles } from '#layout/pdf/stylehook'
import { useLayoutSettings } from "#layout/settings/api"
import { alignmentStyleId } from '#layout/style/alignment'
import { useLayoutStyles } from '#layout/style/api'
import { borderStyleId } from '#layout/style/border'
import { heightStyleId } from '#layout/style/height'
import { StyleProps } from '#layout/style/model'
import { spacingStyleId } from '#layout/style/spacing'
import { StyledBox } from '#layout/style/styledbox'
import { textStyleId } from '#layout/style/text'
import { inputWidthStyleId, widthStyleId } from '#layout/style/width'
import { Tooltip } from "antd"
import moment from "moment"
import { useCallback } from 'react'
import { noDataChange, useComponentData } from './api'
import { useFieldReferenceResolver, useLazyFieldReferenceResolver } from './fieldref'
import { ReferenceFormFields } from './referenceformfield'

/*
The SubmissionFileDrop is an extension of the file drop to keep track of the uploaded files within the submissions
When a new submissions is created users can delete files uploaded on previous submissions.
This component keeps track of them and associate uploaded files to their submissions.
*/

export type SubmissionFileDrop = Component & FieldProps & StyleProps & {
    // required: boolean
    mode: FileBoxProps['mode']
    allowedMimeTypes: FileTypeInfo[]
}



export type UploadTimestamp = {
    timestamp: number
    submission: string
}

export const submissionFileDropId = 'submissionfiledrop'

export const useSubmissionFileDropComponent = () => {

    const name = 'layout.components.filedrop.name'

    const time = useTime()
    const intl = useIntl()

    const settings = useLayoutSettings()

    const referenceresolver = useLazyFieldReferenceResolver<SubmissionFileDrop, Bytestream[]>()

    return useSpecOf({

        id: submissionFileDropId,
        icon: icns.file,
        name,


        styles: current => ({
            [textStyleId]: {},
            // defaults to an absolute scale.
            [inputWidthStyleId]: { default: 400, min: 300, max: 600, percentage:100, ...current[inputWidthStyleId] },
            // bleeds by default, but supports an absolute scale that trackes the input.
            [widthStyleId]: {default: 405, min: 305, max: 605, percentage:100,...current[widthStyleId] },
            [heightStyleId]: {},
            [borderStyleId]: {},
            [alignmentStyleId]: {},
            [spacingStyleId]: { top: 5, bottom: 5, ...current[spacingStyleId] }

        })

        ,

        generate: (): SubmissionFileDrop => ({
            ...newComponentFrom(submissionFileDropId),
            ...partialField(),
            style: {},
            // required: false,
            mode: "multi",
            allowedMimeTypes: []
        })

        /*     ,
        
            useValidation: (input: SubmissionFileDrop) => {
        
                const t = useT()
        
                const { ll, r, resourcesFor, isInstance } = useLayout()
        
                const statusmsg = input.msg ? r(ll(input.msg)) : undefined
        
                const resources = resourcesFor(input) ?? []
        
                return isInstance() && input.required ? check(resources).with(empty(t)).nowOr(statusmsg) : { msg: statusmsg }
        
            }
         */
        ,

        staticDataOf: (_) => ({ submitted: undefined!, submissionDate: undefined!, files: undefined! })

        ,


        submittedDataOf: (c: SubmissionFileDrop, { resources }) => {

            if (resources)
                return undefined

            const { resolvedResources } = referenceresolver(c)

            return { resources: resolvedResources }
        }

        ,
        
        dynamicDataOf: (_, componentstate) => {

            const hostname = getHostname()
            const language = settings.resolveLanguage()

            const t = intl.getFixedT(language)

            const uploads = componentstate.data as UploadTimestamp[]
            const uploadNoEmpty = componentstate.resources?.length

            const firstUpload = uploadNoEmpty ? uploads?.[0] : undefined

            const submissionDate = firstUpload ? time.format(moment(firstUpload.timestamp), 'long', language) : undefined
            const submitted = (!!componentstate.data && componentstate.resources?.length > 0) ? t("common.labels.yes") : t("common.labels.no")

            const files = componentstate.resources?.length > 0 ? componentstate.resources.map(r => `<a href="${hostname}/domain/stream/${r.id}">${r.name}</a>`).join("&nbsp;&nbsp;") : undefined

            return { submitted, submissionDate, files }

        }

        ,

        Render

        ,

        Detail

        ,

        Preview

        ,

        Print

    })

}

function Preview(props: PreviewProps<SubmissionFileDrop>) {

    const { component, path } = props

    const compnentData = useComponentData()

    const bytestreams = useBytestreams()
    const resources = compnentData.resourcesFor(component) ?? []
    const descriptors = compnentData.descriptorsFor(component) ?? []

    const onDrop = files => compnentData.addResourcesFor(component, files)
    const onRemove = (stream: Bytestream) => compnentData.removeResourcesFor(component, [stream])
    const onRender = (stream: Bytestream) => {

        const match = descriptors.find(d => d.resource === stream.id)

        return <a download href={match ? URL.createObjectURL(match.file) : bytestreams.linkOf(stream.id)}>{stream.name}</a>
    }

    return <ComponentDroppable path={path}>
        <ComponentDraggable path={path} component={component}>
            <ComponentPreview path={path} component={component} >
                <FileDropRender {...props} resources={resources} onDrop={onDrop} onRender={onRender} onRemove={onRemove} />
            </ComponentPreview>
        </ComponentDraggable>
    </ComponentDroppable>


}


function Render(props: RenderProps<SubmissionFileDrop>) {

    const { component } = props

    const componentData = useComponentData()

    const { submissionCtx: { submission }, bytestreams } = useLiveLayoutApis()

    const { resolvedResources: defaultResources } = useFieldReferenceResolver<SubmissionFileDrop>(component)

    const time = useTime()


    const resources = componentData.resourcesFor(component)
    const descriptors = componentData.descriptorsFor(component) ?? []

    const defaulting = defaultResources && !resources


    // the current value in state is a list of timestamps.
    const timestamps: UploadTimestamp[] = componentData.dataFor(component) ?? []

    // note: if submission hasn't been saved, id null still. doesn't change the logic in practice.
    const isOwned = (ts: UploadTimestamp) => ts.submission === submission.id
    const isOwnedResource = (r: Bytestream) => r.ref === submission.id

    // the timestamp that corresponds to this submission (should be the last inserted).
    // if this submission hasn't been saved, the timestamp will be unassociated.
    const currentTimestamp = timestamps.find(isOwned)

    // the timestamp we add next if the upload changes.
    const nextTimestamp: UploadTimestamp = { timestamp: Date.now(), submission: submission.id }

    //console.log({ resources, timestamps, currentTimestamp, nextTimestamp })

    // adds new resources, and timestamps the upload for this submission if it hasn't already.
    const onDrop = (files: File[]) => {

        const newtimestamps = currentTimestamp ? noDataChange : [...timestamps, nextTimestamp]

        let resourceAndDescriptors = componentData.resourcesAndDescriptorsFor(files)

        if (defaulting)
            resourceAndDescriptors.push(...defaultResources.map(resource => ({

                // we're not changing the submission ref to "co-opt" the resource. 
                // it'd create resurce sharing, and risks to rmeove resources from older campaigns, even archived ones. 
                resource,

                // we  haven't uploaded this resource here, so we blank the descriptor.
                descriptor: undefined! as FileDescriptor
            }

            )))


        componentData.addResourcesAndDataFor(component, resourceAndDescriptors, newtimestamps)
    }


    // removes a resource, and if there are no other resources owned by this subission,
    // it removes the timestamp on the entire upload too..
    const onRemove = (stream: Bytestream) => {

        if (defaulting) {

            const removed = defaultResources.filter(r => r.id !== stream.id).map(resource => ({

                resource,

                descriptor: undefined! as FileDescriptor
            }

            ))

            componentData.addResourcesAndDataFor(component, removed, undefined)

            return

        }

        const owned = resources?.filter(r => isOwnedResource(r) && r.id !== stream.id) ?? []

        let data

        // some owned left, and no current timestamp, add it now.
        if (owned.length > 0 && !currentTimestamp)
            data = [...timestamps, nextTimestamp]

        // no uploads left, remove existing timestamp.
        else if (owned && owned.length === 0 && currentTimestamp)
            data = timestamps.filter(t => !isOwned(t))

        componentData.removeResourcesAndDataFor(component, [stream], data)

    }

    const reset = () => {
        

        if (resources)
            componentData.removeResourcesAndDataFor(component, resources, undefined)
    }

    const onRender = (stream: Bytestream) => {

        const match = descriptors.find(d => d.resource === stream.id)

        const isOlder = stream.ref !== submission.id

        const className = isOlder ? 'link-faded' : ''

        const timestamp = timestamps.find(t => t.submission === stream.ref)?.timestamp

        const suffix = isOlder ? <span style={{ color: 'cadetblue' }}> - {time.format(moment(timestamp!), 'short')}</span> : ''

        return <span className={className}><a download href={match ? URL.createObjectURL(match.file) : bytestreams.linkOf(stream.id)}>{stream.name} {suffix}</a></span>

    }

    return <FileDropRender {...props} onReset={reset} resources={resources} defaultValue={defaultResources} onDrop={onDrop} onRender={onRender} onRemove={onRemove} />

}

type FullProps = RenderProps<SubmissionFileDrop> & {

    resources?: Bytestream[]
    defaultValue?: Bytestream[]
    onRemove: (_: Bytestream) => any
    onReset?: () => any
    onRender: (_: Bytestream) => any
    onDrop: (_: File[]) => any

}


function FileDropRender(props: FullProps) {

    const { component, resources, onRemove, onRender, onReset, onDrop, defaultValue } = props

    const styles = useLayoutStyles()

    const { resolveLanguage, ll } = useLayoutSettings()

    const language = resolveLanguage()

    const { r } = useLayoutParameters()

    const { readonly, readOnlyMode, canUnlock, enabledOnReadonly } = useFieldHelper(component)

    const { mode, placeholder, label } = component

    const dragMsg = placeholder && r(ll(placeholder))

    
    const lbl = useLayoutLabel(label)

    const statusmsg = component.msg ? r(ll(component.msg)) : undefined
    const validation = { msg: statusmsg }

    const style = styles.resolveCustomStyles(component)

    return <ChangelogRender {...props}>
        <StyledBox component={component}>

            <FileBox style={style} label={lbl}  validation={validation}
                
                dragMsg={dragMsg}
             
                resources={resources}
                defaultValue={defaultValue}
                

                readonly={readonly}
                enabledOnReadOnly={enabledOnReadonly}
                readonlyMode={readOnlyMode}
                canUnlock={canUnlock}
                
                onDrop={onDrop}
                onReset={onReset}
                onRemove={onRemove}
                render={onRender}
                
                mode={mode}
                language={language}               
                allowedMimeTypes={component.allowedMimeTypes} />

        </StyledBox>
    </ChangelogRender>

}


function Detail(props: DetailProps<SubmissionFileDrop>) {

    const t = useT()

    const { component, onChange } = props

    const { mode } = component


    const referenceFilter = useCallback((f: SubmissionFileDrop) => component.mode === f.mode, [component.mode])

    return <>

        <Switch label={t("layout.components.filedrop.multi.name")} onChange={v => onChange({ ...component, mode: v ? "multi" : 'single' })}
            validation={{ msg: t("layout.components.filedrop.multi.msg") }}>
            {mode === 'multi'}
        </Switch>

        <ReferenceFormFields {...props} filter={(referenceFilter)} />

        <SelectBox
            label={t("layout.components.filedrop.type.name")}
            getlbl={m => <FileTypeLabel name={t(m.name)} tooltip={t(m.description)} />}
            selectedKey={component.allowedMimeTypes && component.allowedMimeTypes.map(m => m.name)}
            getkey={m => m.name}
            onChange={a => onChange({ ...component, allowedMimeTypes: a ? a.map(m => fileTypeInfo.filter(ft => ft.name === m)[0]) : [] })}
            validation={{ msg: t("layout.components.filedrop.type.msg") }}>
            {fileTypeInfo}
        </SelectBox>
    </>
}

const FileTypeLabel = (props: { name: string, tooltip: string }) => {

    const { name, tooltip } = props

    return <Tooltip title={tooltip}><span>{name}</span></Tooltip>
}


function Print(props: PrintProps<SubmissionFileDrop>) {

    const { component, wrap = true, View, Link, Text, StyleSheet } = props

    const components = useComponentData()

    const { resolvedResources: defaultResources } = useFieldReferenceResolver<SubmissionFileDrop>(component)

    const { readonlyFallback } = useFieldHelper(component)
    
    const bytestreams = useBytestreams()
    const resources = components.resourcesFor(component) ?? defaultResources ?? []

    const fileStyle = StyleSheet.create(usePdfStyles(component, 'fileDrop'))
    // const noFileStyle = StyleSheet.create(usePdfStyles(component, 'fileDropMissing'))

    return <StyledView {...props} component={component}>

        <View style={fileStyle} wrap={wrap}>

            {
                resources.length === 0 ? 
                    
                    <Text>
                        {readonlyFallback}
                    </Text> 
                    
                    :
                    
                    resources.map((stream, i) => {
                        const hostname = getHostname()
                        const link = `${hostname}${bytestreams.linkOf(stream.id)}`

                        return <Link key={i} src={link}>{stream.name}</Link>
                    })
            }

        </View>
    </StyledView>

}



