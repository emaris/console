
import { useT } from '#app/intl/api';
import { TagRefBox } from "#app/tag/TagRefBox";
import { useTagStore } from '#app/tag/store';
import { complianceType } from '#campaign/constants';
import { submissionType } from "#campaign/submission/constants";
import { PropsWithChildren } from 'react';
import { LayoutCondition, LayoutConditions } from './conditions';
import { tenantType } from '#app/tenant/constants';
import { AudienceList } from '#app/tenant/AudienceList';

type EditorProps = {

    conditions: LayoutConditions
    onChange: (_: LayoutConditions) => void
}

export const ConditionFields = (props: EditorProps) => {

    const { conditions = [], onChange } = props

    const t = useT()

    const tagstore = useTagStore()

    const find = (type: string) => conditions.find(exp => exp.type === type)

    const update = (term: LayoutCondition) => {

        const match = conditions.findIndex(exp => exp.type === term.type)

        conditions.splice(match > -1 ? match : conditions.length, 1, term)

        onChange(conditions)
    }


    return <>

        <ConditionField>
            <AudienceList label={t("layout.conditions.tenant_lbl")}
                onChange={expression => update({ type: 'tenant', expression })}>
                {find('tenant')?.expression}
            </AudienceList>
        </ConditionField>
        
        <ConditionField>
            <TagRefBox mode='multi' label={t('layout.conditions.tenantgroup_lbl')}
                categories={tagstore.allCategoriesOf(tenantType)}
                expression={find('tenantgroup')?.expression}
                onChange={expression => update({ type: 'tenantgroup', expression })} />
        </ConditionField>

        <ConditionField>
            <TagRefBox mode='multi' label={t('layout.conditions.tag_lbl')}
                categories={tagstore.allCategoriesOf(submissionType)}
                expression={find('tag')?.expression}
                onChange={expression => update({ type: 'tag', expression })} />
        </ConditionField>

        <ConditionField>
            <TagRefBox label={t('layout.conditions.assessment_lbl')}
                categories={tagstore.allCategoriesOf(complianceType)}
                expression={find('assessment')?.expression}
                onChange={expression => update({ type: 'assessment', expression })} />
        </ConditionField>

        <ConditionField>
            <TagRefBox label={t('layout.conditions.lineage_assessment_lbl')}
                categories={tagstore.allCategoriesOf(complianceType)}
                expression={find('lineageassessment')?.expression}
                onChange={expression => update({ type: 'lineageassessment', expression })} />
        </ConditionField>
    </>
}

const ConditionField = (props: PropsWithChildren<{}>) => {

    return <div className='condition-field'>
        {props.children}
    </div>
}