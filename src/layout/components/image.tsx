import { icns } from '#app/icons'
import { useFixedT } from '#app/intl/api'
import { DocumentBox } from '#app/stream/DocumentBox'
import { ImageLabel } from '#app/stream/Label'
import { fileMimeType } from '#app/stream/constants'
import { useDocument } from '#app/stream/hooks'
import { getHostname, stripHtmlTags } from '#app/utils/common'
import { useLayoutApis } from '#layout/apicontext'
import StyledView from '#layout/pdf/styledview'
import { alignmentStyleId } from '#layout/style/alignment'
import { backgroundStyleId } from '#layout/style/background'
import { borderStyleId } from '#layout/style/border'
import { heightStyleId } from '#layout/style/height'
import { StyleProps } from '#layout/style/model'
import { spacingStyleId } from '#layout/style/spacing'
import { StyledBox } from '#layout/style/styledbox'
import { widthStyleId } from '#layout/style/width'
import { Icon } from 'antd'
import * as React from "react"
import { BsCardImage } from 'react-icons/bs'
import { useComponentData } from './api'
import { DetailProps } from './detail'
import { useSpecOf } from './helper'
import { Component, PrintProps, RenderProps, newComponentFrom } from "./model"



export type Document = Component & StyleProps & {

}


export const useImageComponent = () => {
   
    const  id = 'image' 
    
    return useSpecOf({

    id,

    icon: <Icon component={BsCardImage} />,
    name: 'layout.components.image.name',

    styles: () => ({

        [backgroundStyleId]: {},
        [widthStyleId]: {},
        [heightStyleId]: {},
        [borderStyleId]: {},
        [alignmentStyleId]: {},
        [spacingStyleId]: { top: 5, bottom: 5 }
    })
    ,

    generate: (): Document => ({ ...newComponentFrom(id), style: {} })

    ,


    Render

    ,

    Detail

    ,

    Print



})}


function Render(props: RenderProps<Document>) {

    const { component } = props

    const {settings, styles} = useLayoutApis()

    const componentData = useComponentData()
   
    const language = settings.resolveLanguage()

    const t = useFixedT(language)
  
    const custom = styles.resolveCustomStyles(component)

    const document = componentData.allDocumentsFor(component.id)[0]

    const hasTitle = !!document?.title?.[language]

    const error = (document && document.streams) ? Object.keys(document.streams).reduce((acc, cur) => {

        const stream = document.streams[cur]

        const valid = stream ? stream.type === fileMimeType.png || stream.type === fileMimeType.jpeg : true

        return [...acc, valid]

    }, [] as boolean[]).some(e => !e) : false

    return error ?
        <div className='image-error'>
            <div className="icon">{icns.file}</div>
            <div className="text">{t("layout.components.image.error")}</div>
            <div className="attention">!</div>
        </div>
        : <StyledBox component={component}>
            <ImageLabel document={document} lang={language} style={custom} noDecorations={!hasTitle} />
        </StyledBox>


}


function Detail(props: DetailProps<Document>) {

    const { component } = props

    const componentData = useComponentData()

    const {settings} = useLayoutApis()

    const language = settings.resolveLanguage()

    const documents = componentData.allDocumentsFor(component.id)

    return <DocumentBox allowedMimeTypes={[fileMimeType.jpeg, fileMimeType.png]} language={language} multi onChange={(d, sc) => componentData.updateDocumentFor(component.id, d, sc!)} >
        {documents[0]}
    </DocumentBox>

}

function Print(props: PrintProps<Document>) {

    const { component, wrap = true, Text, Image, View } = props
    const components = useComponentData()

    const {settings} = useLayoutApis()

    const lang = settings.resolveLanguage()

    const document = components.allDocumentsFor(component.id)[0]

    const { stream, title, isUploaded, link: relativeLink, noDecorations } = useDocument({ document, lang, noDecorations: stripHtmlTags(document.title[lang] ?? '').trim() === '' })

    const link = stream && isUploaded ? <Image source={`${getHostname()}${relativeLink}`} /> : <Text>{title}</Text>
    const caption = (!noDecorations && title) ? <Text style={{textAlign: 'center', fontSize: '9px', color: '#828282'}}>{title}</Text> : <React.Fragment />

    return <StyledView {...props} component={component}>
        <View wrap={wrap} style={{display: 'flex', flexDirection: 'column'}}>
            {link}
            {caption}
        </View>
    </StyledView>

}