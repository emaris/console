import { Draggable, DraggableProperties } from '#app/dnd/Draggable';
import { Droppable } from '#app/dnd/Droppable';
import { Item } from '#app/dnd/model';
import { ReadOnlyContext } from '#app/scaffold/ReadOnly';
import { useLayoutApis } from '#layout/apicontext';
import * as React from 'react';
import { placeholderPosition } from '../designer';
import { useRowSectionContext } from './hooks';
import { Component } from "./model";
import { pageId } from './page';


export const componentdragtype = 'component'
export const specdragtype = 'spec'

export type ComponentSpecItem =  Item & {
    
    spec: string
    specType: string
}

export const newComponentItem = { type:specdragtype, specType: specdragtype}

export type ComponentItem = Item & {


    component: Component
    path: string
}

const isComponentItem = (c: ComponentSpecItem | ComponentItem):c is ComponentItem => !!(c as any).path
export const isSpecItem =(item:ComponentSpecItem | ComponentItem) : item is ComponentSpecItem => item['specType'] ? item['specType']===specdragtype : false



type DraggableSpecProps= React.PropsWithChildren<{

    spec: string
    type?: string
}>

export const ComponentSpecDraggable = (props:DraggableSpecProps) => {

    const {spec,children, type} = props 

    const item = type ? { ...newComponentItem ,type: type ?? undefined,spec} : { ...newComponentItem , spec}

    const readonly = React.useContext(ReadOnlyContext)

    return readonly ? <div style={{opacity: 0.5}}>{children}</div> : <Draggable<ComponentSpecItem>  item={ item } >{ (p:DraggableProperties) => 
                    <div style={{opacity: p.isDragging ? 0.5:1}}>
                            {children} 
                    </div>
                    }
            </Draggable>
}


type DraggableComponentProps = React.PropsWithChildren<{

    component: Component
    path:string
    type?:string
}>

export const ComponentDraggable = (props:DraggableComponentProps) => {

    const {component,path,children} = props 

    const readonly = React.useContext(ReadOnlyContext)

    return readonly ? <>{children}</> : <Draggable<ComponentItem> item={{ type:component.spec,component,path}} >
                {children}  
            </Draggable>
}


type DroppableProps = React.PropsWithChildren<{

    className?:string
    path:string
    types?: string[]
}>

export const ComponentDroppable = (props:DroppableProps) => {

    const {registry,components} = useLayoutApis()

    const {resolve,addPageWithComponent,addPageWithComponentFrom,addComponent,moveComponent} = components

    const {path,className,types= registry.allComponents().map(c=>c.id),children} = props

    const readonly = React.useContext(ReadOnlyContext)

    const rowSectionContext = useRowSectionContext()
    
    const onDrop = (path,item: ComponentSpecItem | ComponentItem)=> {

        const {position} = resolve(path).inCurrentLayout()

        if(isSpecItem(item)) {
        
            if (position===placeholderPosition)
                addPageWithComponent(item.spec)
            else  
                addComponent(item.spec).at(path, rowSectionContext)

        }
        else  {

            if (position===placeholderPosition)
                addPageWithComponentFrom(item.path)
            else
                moveComponent(item.path, path)

        }
    }

    const unless = (draggedItem:ComponentSpecItem | ComponentItem) => {

        return isComponentItem(draggedItem) && 
            ( 
                
                (draggedItem.type===pageId && path.length>1 )   // page in chilad
                ||
            
            path.startsWith(draggedItem.path))  // cycle
    }
    return  readonly ? <>{children}</> : <Droppable<ComponentSpecItem | ComponentItem> className={className} types={types} path={path} onDrop={onDrop} unless={unless}>
                                                                
                {children}

            </Droppable>
}





