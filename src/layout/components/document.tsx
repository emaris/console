import { DocumentBox } from '#app/stream/DocumentBox'
import { DocumentLabel } from '#app/stream/Label'
import { useDocument } from '#app/stream/hooks'
import { getHostname } from '#app/utils/common'
import { useLayoutApis } from '#layout/apicontext'
import StyledView from '#layout/pdf/styledview'
import { usePdfStyles } from '#layout/pdf/stylehook'
import { alignmentStyleId } from '#layout/style/alignment'
import { backgroundStyleId } from '#layout/style/background'
import { borderStyleId } from '#layout/style/border'
import { heightStyleId } from '#layout/style/height'
import { StyleProps } from '#layout/style/model'
import { spacingStyleId } from '#layout/style/spacing'
import { StyledBox } from '#layout/style/styledbox'
import { textStyleId } from '#layout/style/text'
import { widthStyleId } from '#layout/style/width'
import { Icon } from 'antd'
import { GrAttachment } from 'react-icons/gr'
import { useComponentData } from './api'
import { DetailProps } from './detail'
import { useSpecOf } from './helper'
import { Component, PrintProps, RenderProps, newComponentFrom } from "./model"



export type Document = Component & StyleProps & {

}


export const useDocumentComponent = () => {

    const id = 'document'


    return useSpecOf({

        id,

        icon: <Icon component={GrAttachment} />,
        name: 'layout.components.document.name',

        styles: (map) => ({

            [textStyleId]: { color: map[textStyleId]?.color, sizeBaseline: "m" },
            [backgroundStyleId]: {},
            [widthStyleId]: {},
            [heightStyleId]: {},
            [borderStyleId]: {},
            [alignmentStyleId]: {},
            [spacingStyleId]: { top: map[spacingStyleId]?.top ?? 5, bottom: map[spacingStyleId]?.bottom ?? 5 }
        })
        ,

        generate: (): Document => ({ ...newComponentFrom(id), style: {} })

        ,


        Render

        ,

        Detail

        ,

        Print



    })
}


function Render(props: RenderProps<Document>) {

    const { component } = props

    const {settings, styles }= useLayoutApis()
    const componentData = useComponentData()

    const custom = styles.resolveCustomStyles(component)

    const document = componentData.allDocumentsFor(component.id)[0]

    const lang = settings.resolveLanguage()

    return <StyledBox component={component}>
        <DocumentLabel document={document} lang={lang} style={custom} className='document-box' noDecorations />
    </StyledBox>


}


function Detail(props: DetailProps<Document>) {

    const { component } = props

    const componentData = useComponentData()

    const documents = componentData.allDocumentsFor(component.id)

    return <DocumentBox multi onChange={(d, sc) => componentData.updateDocumentFor(component.id, d, sc!)} >
        {documents[0]}
    </DocumentBox>

}

function Print(props: PrintProps<Document>) {

    const { component, wrap = true, Link, Text, View, StyleSheet } = props

    const {settings} = useLayoutApis()
   
    const componentData = useComponentData()

    const documentStyles = StyleSheet.create(usePdfStyles(component, 'links'))

    const lang = settings.resolveLanguage()

    const document = componentData.allDocumentsFor(component.id)[0]

    const { stream, title, isUploaded, link: relativeLink } = useDocument({ document, lang })

    const link = stream && isUploaded ? <Link src={`${getHostname()}${relativeLink}`}>{title}</Link> : <Text>{title}</Text>

    return <StyledView {...props} component={component}>
        <View style={documentStyles} wrap={wrap}>
            {link}
        </View>
    </StyledView>

}