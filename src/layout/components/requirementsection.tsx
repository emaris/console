
import { CardlistBox } from '#app/components/CardlistBox'
import { TextBox } from '#app/form/TextBox'
import { VSelectBox } from '#app/form/VSelectBox'
import { icns } from '#app/icons'
import { useT } from '#app/intl/api'
import { useL } from '#app/model/multilang'
import { shortid } from '#app/utils/common'
import { Validation } from '#app/utils/validation'
import { submissionIcon, submissionType } from '#campaign/submission/constants'
import { useSubmissionContext } from '#campaign/submission/hooks'
import { useLayoutApis, useLiveLayoutApis } from '#layout/apicontext'
import { DetailProps } from '#layout/components/detail'
import { ComponentDraggable, ComponentDroppable } from '#layout/components/dnd'
import { useSpecOf } from '#layout/components/helper'
import { Component, ComponentSpec, Container, PreviewProps, PrintProps, RenderProps, isContainer, nameOf, newComponentFrom } from '#layout/components/model'
import { ComponentPreview } from '#layout/components/preview'
import { ParameterContext } from '#layout/context'
import { newParameterFrom } from '#layout/parameters/model'
import StyledView from '#layout/pdf/styledview'
import { useLayoutRegistry } from '#layout/registry'
import { alignmentStyleId } from '#layout/style/alignment'
import { backgroundStyleId } from '#layout/style/background'
import { borderStyleId } from '#layout/style/border'
import { heightStyleId } from '#layout/style/height'
import { StyleProps } from '#layout/style/model'
import { spacingStyleId } from '#layout/style/spacing'
import { StyledBox } from '#layout/style/styledbox'
import { widthStyleId } from '#layout/style/width'
import { RequirementLabel } from '#requirement/Label'
import { useRequirementClient } from '#requirement/client'
import { requirementIcon, requirementPlural, requirementSingular, requirementType } from '#requirement/constants'
import { Requirement, useRequirementModel } from '#requirement/model'
import { useRequirementStore } from '#requirement/store'
import { Empty, Tooltip } from 'antd'
import * as React from 'react'
import { LayoutConfigProvider } from '../config'
import { isLiveParameter } from '../parameters/model'
import { LiveData, RequirementParameter, requirementParamId, useRequirementParameter } from '../parameters/requirement'
import './styles.scss'


export type NamedRequirement = {

    id: string
    requirement: string
    alias: string

}

export const sectionRequirementOf = (nr: NamedRequirement | undefined): string => (nr?.requirement ?? nr?.id)!

export type RequirementSection = Container & StyleProps & {


    requirements: NamedRequirement[]

}

export const requirementSectionId = 'reqsection'

export const useRequirementSection = (): ComponentSpec<RequirementSection> => {

    const name = 'layout.components.requirement.name'

    const t = useT()
    const l = useL()
    const store = useRequirementStore()


    return useSpecOf({


        id: requirementSectionId,
        icon: requirementIcon,
        name,
        active: false,

        styles: () => ({

            [backgroundStyleId]: {},
            [widthStyleId]: {},
            [heightStyleId]: {},
            [borderStyleId]: {},
            [alignmentStyleId]: {},
            [spacingStyleId]: {}

        })

        ,

        Title: ({ component }: { component: RequirementSection }) => {


            const firstRequirement = sectionRequirementOf(component.requirements[0])

            let title

            if (firstRequirement) {
                title = l(store.safeLookup(firstRequirement).name)
                if (component.requirements.length > 1)
                    title = t("layout.components.requirement.title_and_others", { name: title, count: component.requirements.length - 1 })
            }

            return title ? <RequirementLabel noMemo requirement={firstRequirement} title={title} noDecorations noLink /> : t(name)

        }

        ,

        generate: (): RequirementSection => ({ ...newComponentFrom(requirementSectionId), requirements: [], style: {}, children: [] })

        ,


        Detail,
        Render,
        Print,
        PrintPreview
    })
}



export function Preview(props: PreviewProps<RequirementSection>) {

    const { components } = useLayoutApis()

    const t = useT()

    const { component, path } = props

    const nextPosition = component.children.length

    const { parameters } = useSectionParameters(component)

    const renderChildren = component.requirements?.length > 0 || component.children?.length > 0

    return <ComponentDroppable path={path}>
        <ComponentDraggable path={path} component={component}>
            <ComponentPreview path={path} component={component}>{

                renderChildren ?

                    <ParameterContext.Provider value={parameters}>
                        <div className='section'>
                            <StyledBox component={component}>

                                {
                                    component.children.map((child, position) => {

                                        const { Preview } = components.componentSpecOf(child)

                                        return <Preview key={position} component={child} path={components.extend(path).at(position)} />

                                    })
                                }

                            </StyledBox>

                            <ComponentDroppable key={nextPosition} className="component-placeholder" path={components.extend(path).at(nextPosition)} />

                        </div>

                    </ParameterContext.Provider>


                    :

                    <Empty image={Empty.PRESENTED_IMAGE_SIMPLE} description={t('common.labels.select_one_or_more', { plural: t(requirementPlural) })} />
            }
            </ComponentPreview>
        </ComponentDraggable>
    </ComponentDroppable>
}


function Render(props: RenderProps<RequirementSection>) {

    const { submissionCtx, dashboard, components } = useLiveLayoutApis()

    const { component, className = '', style } = props

    const { parameters } = useLiveSectionParameters(component)

    const warningLabelStyle = { color: 'orange' }

    const hasWarnings = parameters.map(p => !!p.value.liveData?.submission).some(p => p === false)


    const linksToRequirementsSubmissions = parameters.map(p => {

        // this is always live
        const { requirement, submission } = p.value.liveData!

        const baseroute = dashboard.partyRouteToSubmissionWith(requirementType, requirement.id, submissionCtx.party.source)

        const linkTo = submission ? `${baseroute}/${submission.id}` : baseroute

        return <RequirementLabel key={p.id}
            linkTarget={submissionType}
            titleStyle={p.value.liveData?.submission ? undefined : warningLabelStyle}
            icon={p.value.liveData?.submission ? submissionIcon : icns.unknown}
            requirement={requirement.id}
            linkTo={linkTo} />
    }
    )

    const containerRef = React.useRef<HTMLDivElement>(null);

    return <ParameterContext.Provider value={parameters}>
        <StyledBox component={component}>

            <div style={style} className={`requirement-section-contents ${className} ${hasWarnings ? 'section-missing-submission' : ''}`} ref={containerRef} >

                <span className="section-requirement-placeholder" >

                    <Tooltip
                        placement="right"
                        title={linksToRequirementsSubmissions}
                        getPopupContainer={() => containerRef.current!}
                        autoAdjustOverflow={true}
                    >{requirementIcon}</Tooltip>

                </span>

                {component.children.map((child, position) => {

                    const { Render } = components.componentSpecOf(child)

                    return <Render key={position} component={child} />

                })
                }

            </div>
        </StyledBox>
    </ParameterContext.Provider>


}


function Detail(props: DetailProps<RequirementSection>) {

    const l = useL()
    const t = useT()
    const reqmodel = useRequirementModel()
    const reqstore = useRequirementStore()
    const client = useRequirementClient()


    const { component, onChange } = props

    const reqs = component.requirements

    const { targets } = useSectionParameters(component)

    const isEmpty = index => !reqs[index]?.alias || reqs[index].alias.length === 0
    const isDuplicate = index => !isEmpty(index) && reqs.find((r, j) => j !== index && reqs[index]?.alias === r?.alias)
    const validation = index => {

        const filled = !!sectionRequirementOf(reqs[index])
        const empty = filled && isEmpty(index)
        const duplicate = filled && isDuplicate(index)

        return {

            status: empty ? 'warning' : duplicate ? 'error' : 'success',
            msg: empty ? t("layout.components.requirement.empty_alias_warning") :
                duplicate ? t("layout.components.requirement.duplicate_alias_error") :
                    t("layout.components.requirement.full_alias")

        } as Validation

    }

    const options = reqstore.all().sort(reqmodel.comparator).filter(r => !reqs.find(rr => sectionRequirementOf(rr) === r.id))

    const element = (index: number) => {

        const placeholder = targets[index]?.name.en?.replace(/\s/g, '-').toLowerCase() ?? t("layout.components.requirement.alias_placeholder")

        return <div key={index} className="requirement-section-element">
            <VSelectBox placeholder={t("layout.components.requirement.select_placeholder")} light

                options={options}

                onChange={(req: Requirement) => client.fullFetch(req).then(fetched => onChange({
                    ...component,
                    requirements: fetched ? reqs.map((rr, i) => i === index ? { ...rr, requirement: fetched.id } : rr) : reqs.filter((_, i) => i !== index)
                }))}

                renderOption={r => <RequirementLabel requirement={r} noLink />}

                lblTxt={r => l(r.name)}
                optionId={r => ({ id: r.id })}>

                {targets[index] ? [targets[index]?.id] : undefined}

            </VSelectBox>
            <TextBox validation={validation(index)} placeholder={placeholder} disabled={!sectionRequirementOf(reqs[index])}
                onChange={alias => onChange({
                    ...component,
                    requirements: reqs.map((r, i) => i === index ? { ...reqs[index], alias } : r)
                })}>
                {reqs[index]?.alias}
            </TextBox>
        </div>
    }



    const addIf = options.length > 0
    const onAdd = () => onChange({ ...component, requirements: [...reqs, { id: shortid(), requirement: null!, alias: undefined! }] })
    const onRemove = index => onChange({ ...component, requirements: component.requirements.filter((_, i) => i !== index) })

    return <CardlistBox id={component.id} singleLabel={t(requirementSingular)} addIf={addIf} onAdd={onAdd} onRemove={onRemove} >
        {reqs.map((_, i) => <div key={i}>{element(i)}</div>)}
    </CardlistBox>

}


const makeParameterFor = (id: string, r: Requirement, alias: string): RequirementParameter => ({

    ...newParameterFrom(requirementParamId),
    bridgeMode: true,
    fixedValue: true,
    id,
    name: alias ?? r.name.en?.replace(/\s/g, '-').toLowerCase(),
    components: [],
    data: undefined!,
    value: { id: r.id, liveData: { requirement: r } as LiveData }

})

const flatten = (c: Component): Component[] => isContainer(c) ? c.children.flatMap(flatten) : [c]





/* 
    synthesises parameters from the requirements in scope, 
    including metadata exported by _named_ components in the requirements.

    returns the requirements and the synthesised parameters.

*/
export const useSectionParameters = (section: RequirementSection) => {

    const requirements = useRequirementStore()
    const componentReqs = section.requirements

    const layoutRegistry = useLayoutRegistry()

    return React.useMemo(() => {

        // resolves the requirements in this section and pairs them with their aliases.
        const targets = section.requirements.filter(t => t.id !== undefined && t.requirement !== null).map(t => ({ id: t.id, requirement: requirements.safeLookup(sectionRequirementOf(t)), alias: t.alias }))

        // creates parameters for each requirement adding the data exported by their named components.
        const parameters = targets.map(({ id, requirement, alias }) => {

            const param = makeParameterFor(id, requirement, alias)

            // collects all the named components with their specifications specifications.
            const components = flatten(requirement.properties.layout.components).filter(r => !!r.shortname)

            // collects data from each component and indexes it by component name.
            const data = components.reduce((acc, c) => ({ ...acc, [nameOf(c)]: layoutRegistry.lookupComponent(c.spec).staticDataOf(c) }), {})

            return { ...param, components, data }
        })


        return { targets: targets.map(t => t.requirement), parameters }


    }

        //eslint-disable-next-line
        , [componentReqs])


}


/* 

    synthesises parameters from the requirements in scope, including any data exported by their components.
    
    returns the requirements AND the synthesised parameters.

*/
export const useLiveSectionParameters = (component: RequirementSection) => {

    const requirements = useRequirementStore()
    const ctx = useSubmissionContext()
    const componentReqs = component.requirements

    const layoutRegistry = useLayoutRegistry()
    const requirementParam = useRequirementParameter()

    return React.useMemo(() => {

        // resolves the requirements in this section and pairs them with their aliases.
        const targets = component.requirements.filter(t => sectionRequirementOf(t) !== undefined).map(t => ({ id: t.id, requirement: requirements.safeLookup(sectionRequirementOf(t)), alias: t.alias }))

        // creates parameters for each requirement adding to the data the current state of their named components.
        const parameters: RequirementParameter[] = targets.map(({ id, requirement, alias }) => {

            const param = makeParameterFor(id, requirement, alias)

            const value = requirementParam.valueFor(ctx, param)

            // collects all the named components with their specifications specifications.
            const components = flatten(requirement.properties.layout?.components).filter(r => !!r.shortname)

            // all the parameters of the target requirement, so they can be passed to components to resolve the data.
            // live parameters from the requirement need to be initialised to this context.
            const requirementparameters = requirement.properties.layout.parameters.map(p => {

                const spec = layoutRegistry.lookupParameter(p.spec)

                const initialised = isLiveParameter(spec) ? { ...p, value: spec.valueFor({ ...ctx, submission: value.liveData?.submission! }, p) } : p

                //console.log("initialised external requirement's param",initialised)

                return initialised

            })

            // asks each component to derive data from their state in the current submission, and indexes it by component name.
            const data = components.reduce((acc, c) => ({
                ...acc,

                [nameOf(c)]: value.liveData?.submission ?

                    layoutRegistry.lookupComponent(c.spec).dynamicDataOf(c,
                        {
                            data: value.liveData.submission.content.data[c.id],
                            resources: value.liveData.submission.content.resources[c.id]
                        },

                        requirementparameters) : undefined

            }), {})

            //console.log("components", components, "data",data)

            return { ...param, value, components, data }
        })


        return { targets: targets.map(t => t.requirement), parameters }


    }

        //eslint-disable-next-line
        , [componentReqs])
}

function Print(props: PrintProps<RequirementSection>) {

    const { components } = useLayoutApis()

    const { component, wrap = true, View } = props

    //const config = { fieldsRestricted: true }

    const { parameters } = useLiveSectionParameters(component)

    return <LayoutConfigProvider config={{}}>
        <ParameterContext.Provider value={parameters}>
            <StyledView {...props} component={component}>
                <View wrap={wrap}>
                    {
                        component.children.map((child, position) => {

                            const { Print } = components.componentSpecOf(child)

                            return <Print  {...props} key={position} component={child} />

                        })
                    }
                </View>
            </StyledView>
        </ParameterContext.Provider>
    </LayoutConfigProvider>

}

function PrintPreview(props: PrintProps<RequirementSection>) {

    const { components } = useLayoutApis()

    const { component, View } = props

    const { parameters } = useSectionParameters(component)

    return <ParameterContext.Provider value={parameters}>
        <StyledView {...props} component={component}>
            <View>
                {
                    component.children.map((child, position) => {

                        const { PrintPreview } = components.componentSpecOf(child)

                        return <PrintPreview  {...props} key={position} component={child} />

                    })
                }
            </View>
        </StyledView>
    </ParameterContext.Provider>

}

