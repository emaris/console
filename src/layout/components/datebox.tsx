import { SelectBox } from '#app/form/SelectBox'
import { Switch } from '#app/form/Switch'
import { icns } from '#app/icons'
import { useT } from '#app/intl/api'
import { useTimePicker } from '#app/time/TimePickerBox'
import { stripHtmlTags } from '#app/utils/common'
import { ChangelogRender } from '#campaign/submission/changerender'
import { useLayoutApis } from '#layout/apicontext'
import { useLayoutParameters } from '#layout/parameters/api'
import { useParameters } from '#layout/parameters/hooks'
import { ParametricTextBox } from '#layout/parameters/parametricboxes'
import StyledView from '#layout/pdf/styledview'
import { usePdfStyles } from '#layout/pdf/stylehook'
import { useLayoutSettings } from '#layout/settings/api'
import { alignmentStyleId } from '#layout/style/alignment'
import { borderStyleId } from '#layout/style/border'
import { heightStyleId } from '#layout/style/height'
import { StyleProps } from '#layout/style/model'
import { spacingStyleId } from '#layout/style/spacing'
import { StyledBox } from '#layout/style/styledbox'
import { textStyleId } from '#layout/style/text'
import { inputWidthStyleId, widthStyleId } from '#layout/style/width'
import { StyleSheet, Text } from '@react-pdf/renderer'
import moment, { Moment } from 'moment-timezone'
import { useComponentData } from './api'
import { DetailProps } from './detail'
import { ComponentDraggable, ComponentDroppable } from './dnd'
import { FieldProps, partialField } from './fielddetail'
import { useFieldHelper } from './fieldhelper'
import { useFieldReferenceResolver, useLazyFieldReferenceResolver } from './fieldref'
import { useLayoutLabel, useSpecOf } from './helper'
import { Component, PreviewProps, PrintProps, RenderProps, newComponentFrom } from "./model"
import { ComponentPreview } from './preview'
import { ReferenceFormFields } from './referenceformfield'


const dateFormats = [
    "DD-MM-YYYY",
    "DD/MM/YYYY",
    "DD MMM YYYY",
    "DD/MMM/YYYY",
    "DD MMMM YYYY",
    "YYYY-MM-DD",
    "YYYY/MM/DD",
    "YYYY-MM-DD",
    "YYYY-MMM-DD",
    "YYYY MMM DD",
    "YYYY MMMM DD",
]

const defaultFormat = dateFormats[0]

export type DateBox = Component & FieldProps & StyleProps & {

    defaultValue?: Moment

    // use to resolve parameters
    defaultValueAsText?: string

    format: string
    showTime: boolean
    min?: Moment
    max?: Moment

}

const formatTime = (component: DateBox) => component.format ? component.showTime ? `${component.format} HH:mm` : component.format : defaultFormat

export const useDateBoxComponent = () => {

    const id = 'date-box'
    const name = 'layout.components.datebox.name'

    const params = useLayoutParameters()

    const referenceresolver = useLazyFieldReferenceResolver()

    return useSpecOf({

        id,
        icon: icns.calendar,
        name,


        // [since 11/2024] materialises defaults to replace missing content on submit.
        // considers both derived defaults (field references) and configured defaults, in that order.
        // for configured defaults, resolves parameters.
        submittedDataOf: (component, { data }, parameters) => {

            // if we have data we don't to materialise anything.
            if (data)
                return undefined

            // we resolve the param, strip the tags, and make sure we don't keep an empty string (that's wny we use || operator). 
            const parametricDefault = stripHtmlTags(params.r(component.defaultValueAsText, parameters)) || undefined

            // resolves the field reference if any, otherwise fallback to parsed default, then default.
            const { resolved } = referenceresolver(component)

            // ref first, fallback to parametric, then to constant.
            const defaultValue = resolved ?? parametricDefault ?? component.defaultValue

            return { data: defaultValue }
        }


        ,

        dynamicDataOf: (component, { data }) => {

            return data && moment(data).format(formatTime(component))

        }

        ,

        styles: current => ({

            [textStyleId]: {},
            // defaults to an absolute scale.
            [inputWidthStyleId]: { default: 130, min: 130, max: 300, ...current[inputWidthStyleId] },
            // bleeds by default, but supports an absolute scale that trackes the input.
            [widthStyleId]: { default: 135, min: 135, max: 305, percentage: 100, ...current[widthStyleId] },
            [heightStyleId]: {},
            [borderStyleId]: {},
            [alignmentStyleId]: {},
            [spacingStyleId]: { top: 5, bottom: 5, ...current[spacingStyleId] }

        })
        ,

        generate: (): DateBox => ({

            ...newComponentFrom(id),
            ...partialField(),
            style: {},
            format: defaultFormat,
            showTime: false

        })

        ,

        Preview

        ,

        Render

        ,


        Detail

        ,

        Print


    })
}



function Preview(props: PreviewProps<DateBox>) {

    const { path, component } = props

    return <ComponentDroppable path={path}>
        <ComponentDraggable path={path} component={component}>
            <ComponentPreview path={path} component={component}>
                <Render component={component} />
            </ComponentPreview>
        </ComponentDraggable>
    </ComponentDroppable>
}



const useDefaultHelper = (component: DateBox) => {

    const parameters = useParameters()

    // we resolve the param, strip the tags, and make sure we don't keep an empty string (that's wny we use || operator). 
    const parametricDefault = stripHtmlTags(parameters.rr(component.defaultValueAsText)) || undefined

    // resolves the field reference if any, otherwise fallback to parsed default, then default.

    const { resolved } = useFieldReferenceResolver<DateBox, Moment>(component)

    // ref first, fallback to parametric, then to constant.
    const defaultValue = resolved ?? parametricDefault ?? component.defaultValue

    return defaultValue ? moment(defaultValue) : undefined
}


function Render(props: RenderProps<DateBox>) {

    const { component } = props

    const { styles } = useLayoutApis()

    const parameters = useParameters()

    const { ll } = useLayoutSettings()

    const { readOnlyMode, enabledOnReadonly, canUnlock, readonly, placeholder } = useFieldHelper(component)

    const { min, max } = component

    const { r } = parameters

    const { dataFor, changeDataFor } = useComponentData()

    const { Picker: TimePicker } = useTimePicker()

    const data = dataFor<Moment>(component)

    const label = useLayoutLabel(component.label)

    const msg = r(ll(component.msg))

    const currentFormat = formatTime(component)

    const defaultValue = useDefaultHelper(component)

    const style = styles.resolveCustomStyles(component)

    const contents = <TimePicker style={style} fieldClassName='datebox-input-picker' label={label} validation={{ msg }}

        placeholder={placeholder ?? ''}  // fallback overrides generic placeholder. 

        showTime={component.showTime}
        format={currentFormat}
        initialTime='00:00'

        disabledDate={current => (component.min ? !moment(min).isBefore(current) : false) || (max ? !moment(max).isAfter(current) : false)}

        readonly={readonly}
        enabledOnReadOnly={enabledOnReadonly}
        readonlyMode={readOnlyMode}
        canUnlock={canUnlock}

        value={data}
        defaultValue={defaultValue}
        onChange={v => changeDataFor(component, v)}

    />

    return <ChangelogRender {...props}>
        <StyledBox component={component} className='layout-datebox'>
            {contents}
        </StyledBox>
    </ChangelogRender>
}

function Detail(props: DetailProps<DateBox>) {

    const t = useT()

    const { component, onChange } = props

    const { Picker: TimePicker } = useTimePicker()

    const currentFormat = formatTime(component)

    return <div>

        <ReferenceFormFields {...props} />

        <div style={{ marginBottom: 15 }}>

            <Switch
                label={t('layout.components.datebox.show_time.name')}
                validation={{ msg: t('layout.components.datebox.show_time.msg') }}
                onChange={v => onChange({ ...component, showTime: v })}>{component.showTime}</Switch>


            <SelectBox style={{ width: 240 }}
                label={t('layout.components.datebox.format.name')}
                validation={{ msg: t('layout.components.datebox.format.msg') }}
                onChange={v => onChange({ ...component, format: v })}
                selectedKey={component.format}>
                {dateFormats}
            </SelectBox>

        </div>

        <TimePicker style={{ width: 200 }} label={t('layout.components.datebox.select_min.name')} className='layout-datebox-details'
            value={component.min}
            validation={{ msg: t('layout.components.datebox.select_min.msg') }}
            placeholder=''
            allowClear
            disabledDate={current => component.max ? !moment(component.max).isAfter(current) : false}
            showTime={component.showTime}
            format={currentFormat}
            initialTime='00:00'
            onChange={v => onChange({ ...component, min: v ?? undefined })} />

        <TimePicker style={{ width: 180 }} label={t('layout.components.datebox.select_max.name')} className='layout-datebox-details'
            value={component.max}
            validation={{ msg: t('layout.components.datebox.select_max.msg') }}
            placeholder=''
            allowClear
            disabledDate={current => component.min ? !moment(component.min).isBefore(current) : false}
            showTime={component.showTime}
            format={currentFormat}
            initialTime='00:00'
            onChange={v => onChange({ ...component, max: v ?? undefined })} />


        <TimePicker style={{ width: 180 }} fieldClassName='datebox-input-picker'
            label={t("layout.components.fields.default_value.name")} id={`datebox-default-${component.id}`}
            validation={{ msg: t("layout.components.fields.default_value.msg") }}

            showTime={component.showTime}
            placeholder=''
            format={currentFormat}
            initialTime='00:00'

            value={component.defaultValue}
            onChange={defaultValue => onChange({ ...component, defaultValue })}

        />

        <ParametricTextBox label={t("layout.components.fields.default_value_astext.name")} id={`numberbox-placeholder-${component.id}`} noImage onChange={v => onChange({ ...component, defaultValueAsText: v })}
            validation={{ msg: t("layout.components.fields.default_value_astext.msg") }}>
            {component.defaultValueAsText}
        </ParametricTextBox>

    </div>
}


function Print(props: PrintProps<DateBox>) {

    const { component } = props

    const { dataFor } = useComponentData()

    const { readonlyFallback } = useFieldHelper(component)

    const defaultValue = useDefaultHelper(component)

    const data = dataFor(component) ?? defaultValue

    const numericBoxStyle = StyleSheet.create(usePdfStyles(component, "dateBox"))

    const formatWidthForPrint = (c: DateBox): DateBox => {
        if (c.style?.[widthStyleId] && c.style?.[widthStyleId].percentage && c.style?.[widthStyleId].percentage <= 90)
            return { ...c, style: { ...c.style, [widthStyleId]: { 'percentage': c.style[widthStyleId].percentage + 10 } } }
        return c
    }

    const currentFormat = formatTime(component)

    return <StyledView {...props} component={formatWidthForPrint(component)}>
        <Text style={numericBoxStyle}>{data ? moment(data).format(currentFormat) : readonlyFallback}</Text>
    </StyledView>
}
