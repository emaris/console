import { Button } from '#app/components/Button'
import { CardlistBox } from '#app/components/CardlistBox'
import { MultiBox } from '#app/form/MultiBox'
import { NumericBox } from '#app/form/NumericBox'
import { Switch } from '#app/form/Switch'
import { icns } from '#app/icons'
import { useT } from '#app/intl/api'
import { allLanguages } from '#app/intl/model'
import { Multilang, MultilangDto, newMultiLang } from '#app/model/multilang'
import { ReadOnlyContext } from '#app/scaffold/ReadOnly'
import { randomString } from '#app/utils/common'
import { Validation } from '#app/utils/validation'
import { RenderLoopContext, useLayout } from '#layout/context'
import { isInstance } from '#layout/model'
import { useLayoutSettings } from '#layout/settings/api'
import { alignmentStyleId } from '#layout/style/alignment'
import { backgroundStyleId } from '#layout/style/background'
import { borderStyleId } from '#layout/style/border'
import { heightStyleId } from '#layout/style/height'
import { StyleProps } from '#layout/style/model'
import { spacingStyleId } from '#layout/style/spacing'
import { StyledBox } from '#layout/style/styledbox'
import { widthStyleId } from '#layout/style/width'
import { View } from '@react-pdf/renderer'
import { Icon } from 'antd'
import { useL } from 'apprise-frontend-core/intl/multilang'
import { Fragment, useContext, useEffect, useMemo } from 'react'
import { RxSection } from 'react-icons/rx'
import { useComponentData, useLayoutComponents } from './api'
import { ComponentCustomName } from './componenthelper'
import { SectionDataContext } from './context'
import { DetailProps } from './detail'
import { ComponentDraggable, ComponentDroppable } from './dnd'
import { Embeddable, useEmbedddable } from './embedding'
import { useSpecOf } from './helper'
import { ComponentSpec, Container, PreviewProps, PrintProps, RenderProps, newComponentFrom } from "./model"
import { OptionPanel } from './optionpanel'
import { ComponentPreview } from './preview'
import { rowspecid } from './row'
import "./section.scss"

type DynamicProps = {
    size: number
    min: number
    max?: number
    addSingular?: Multilang
}

type ReferenceProps = {
    btn?: Multilang
}

export type Section = Container & StyleProps & Embeddable & {

    name: MultilangDto
    dynamic: boolean
    dynamicProps?: DynamicProps
    reference?: boolean
    referenceProps?: ReferenceProps

}

export type SectionData = {

    dynamicEntries: string[] | undefined
    referencesEnabled: boolean | undefined
}

export const defaultSectionData = (component?: Section): SectionData => ({

    dynamicEntries: undefined,

    //enabled if called without a section (cf. context fallback) or if trigger is off.
    referencesEnabled: !component || !component.reference
})

const defaultDynamicProps = { min: 1, size: 1 } as DynamicProps
const sectionName = 'layout.components.section.name'

// const newDynamicId = (): string => `dynamic-section-${randomString(10)}-${randomString(5)}`
const newDynamicId = (component: Section): string => `dynamic-section-${component.id}-${randomString(10)}`
const newDynamicBag = (component: Section, min: number): string[] => Array.from(Array(min).keys()).map(_ => newDynamicId(component))
const reorderElements = (arr: string[], from: number, to: number) => {
    let tmpArr = [...arr]

    var element = tmpArr[from];
    tmpArr.splice(from, 1);
    tmpArr.splice(to, 0, element);

    return tmpArr
}

export const useSectionComponent = (): ComponentSpec<Section> => {

    const id = 'section'

    return useSpecOf({

        id,
        icon: <Icon component={RxSection} />,
        name: sectionName,


        styles: () => ({

            [backgroundStyleId]: {},
            [widthStyleId]: {},
            [heightStyleId]: {},
            [borderStyleId]: {},
            [alignmentStyleId]: {},
            [spacingStyleId]: {}

        })
        ,

        generate: (): Section => ({

            ...newComponentFrom(id),
            style: {},
            children: [],
            name: newMultiLang(),
            dynamic: false,
            embeddable: false,
            dynamicProps: defaultDynamicProps

        })

        ,

        nameOf: (c: Section) => c.name.en ? c.name : { en: sectionName }

        ,

        Preview

        ,

        Render

        ,

        Detail

        ,

        PrintPreview

        ,

        Print

    })

}

function Detail(props: DetailProps<Section>) {

    const t = useT()

    const { component, onChange } = props

    const { switchBtn } = useEmbedddable()

    const defaultMaxValidation: Validation = { status: 'success', msg: t('layout.components.section.dynamic.max.msg') }

    const isMaxError = (min: number | undefined, max: number | undefined): boolean => max !== undefined && max !== null && `${max}`.trim() !== '' && max <= (min ?? 0)

    const maxValidation: Validation = component.dynamic ?
        isMaxError(component.dynamicProps?.min, component.dynamicProps?.max) ?
            { status: 'error', msg: t('layout.components.section.dynamic.max.err') }
            :
            defaultMaxValidation
        : defaultMaxValidation

    return <Fragment>

        <ComponentCustomName {...props} />

        <MultiBox id={component.id} label={t("common.fields.name_multi.name")} style={{ flexGrow: 1, marginRight: 30 }} onChange={v => onChange({ ...component, name: v })} validation={{ msg: t("common.fields.name_multi.msg") }}>
            {component.name}
        </MultiBox>

        {switchBtn(props)}

        <Switch label={t('layout.components.section.dynamic.label')} validation={{ msg: t('layout.components.section.dynamic.msg') }}
            onChange={v => onChange({ ...component, dynamic: v, dynamicProps: v ? defaultDynamicProps : undefined })}>
            {component.dynamic}
        </Switch>

        <OptionPanel show={component.dynamic}>

            <MultiBox
                id='add-btn-name'
                onChange={v => onChange({ ...component, dynamicProps: { ...(component.dynamicProps ?? defaultDynamicProps), addSingular: v } })}
                label={t('layout.components.section.dynamic.add_singular.label')}
                validation={{ msg: t('layout.components.section.dynamic.add_singular.msg') }}>{component.dynamicProps?.addSingular}</MultiBox>

            <NumericBox
                min={0}
                onChange={v => v && onChange({ ...component, dynamicProps: { ...(component.dynamicProps ?? defaultDynamicProps), min: v, size: v } })}
                label={t('layout.components.section.dynamic.min.label')}
                validation={{ msg: t('layout.components.section.dynamic.min.msg') }}>{component.dynamicProps?.min}</NumericBox>

            <NumericBox
                min={0}
                onChange={v => onChange({ ...component, dynamicProps: { ...(component.dynamicProps ?? defaultDynamicProps), max: v } })}
                label={t('layout.components.section.dynamic.max.label')}
                validation={maxValidation}>{component.dynamicProps?.max}</NumericBox>

        </OptionPanel>

        <Switch label={t('layout.components.section.reference.label')} validation={{ msg: t('layout.components.section.reference.msg') }}
            onChange={v => onChange({ ...component, reference: v/* , dynamicProps: v ? defaultDynamicProps : undefined */ })}>
            {component.reference}
        </Switch>

        <OptionPanel show={component.reference}>

            <MultiBox
                id='trigger-btn'
                defaultValue={allLanguages.reduce((acc, lng) => ({ ...acc, [lng]: t('layout.components.section.reference.btn_default', { lng }) }), {})}
                onChange={btn => onChange({ ...component, referenceProps: { ...component.referenceProps, btn } })}
                label={t('layout.components.section.reference.btn_lbl')}
                validation={{ msg: t('layout.components.section.reference.btn_msg') }}>
                {component.referenceProps?.btn}
            </MultiBox>

        </OptionPanel>

    </Fragment>
}

function Render(props: RenderProps<Section>) {

    const t = useT()
    const l = useL()

    const readonly = useContext(ReadOnlyContext)

    const { dataFor, changeDataFor } = useComponentData()

    const { component } = props

    const contents = component.dynamic ? <DynamicRender {...props} /> : <SingleRender {...props} />

    if (readonly || !component.reference)
        return contents

    const data = dataFor(component) as SectionData ?? defaultSectionData(component)

    const trigger = l(component.referenceProps?.btn) ?? t('layout.components.section.reference.btn_default')

    const triggerReferences = () => changeDataFor<SectionData>(component, { ...data, referencesEnabled: true })

    return <SectionDataContext.Provider value={data} >
        <div className='reference-section'>

            {data.referencesEnabled ||

                <div className='section-separator separator-start'>
                    <Button disabled={data.referencesEnabled} icn={icns.download} iconLeft className='section-trigger' size='small' onClick={triggerReferences}>
                        {trigger}
                    </Button>
                </div>

            }

            {contents}


            {data.referencesEnabled ||

                <div className='section-separator separator-end'>
                    <div>{trigger} ({t('layout.components.section.reference.end')})</div>
                </div>

            }

        </div>
    </SectionDataContext.Provider>
}

function Preview(props: PreviewProps<Section>) {

    const { extend, componentSpecOf } = useLayoutComponents()

    const { component, path } = props

    const layout = useLayout()

    const instance = isInstance(layout)

    const nextPosition = component.children.length

    return <ComponentDroppable path={path}>
        <ComponentDraggable path={path} component={component}>
            <ComponentPreview path={path} component={component} >
                <div className={`section`}>
                    <StyledBox component={component}>

                        {
                            (instance && component.dynamic) ? <DynamicPreview {...props} /> :

                                component.children.map((child, position) => {

                                    const { Preview } = componentSpecOf(child)

                                    return <Preview key={position} component={child} path={extend(path).at(position)} />

                                })
                        }

                    </StyledBox>

                    <ComponentDroppable key={nextPosition} className="component-placeholder" path={extend(path).at(nextPosition)} />

                </div>
            </ComponentPreview>
        </ComponentDraggable>
    </ComponentDroppable>
}

const DynamicRender = (props: RenderProps<Section>) => {

    const { component } = props

    const { componentSpecOf } = useLayoutComponents()
    const { dataFor, changeDataFor, allData } = useComponentData()

    const allDataKeys = Object.keys(allData() ?? {})

    const { ll } = useLayoutSettings()

    const max = component.dynamicProps?.max
    const min = component.dynamicProps?.min ?? 1

    //eslint-disable-next-line
    const defaultDynamicEntries = useMemo(() => newDynamicBag(component, min), [min])

    const data = dataFor<SectionData>(component) ?? defaultSectionData(component)

    const { dynamicEntries = defaultDynamicEntries, ...rest } = data

    const addSingular = component.dynamicProps?.addSingular ? ll(component.dynamicProps?.addSingular) : ''

    const removeEntry = (id: string) => changeDataFor<SectionData>(component, { dynamicEntries: dynamicEntries.filter(entry => entry !== id), ...rest })

    // Detects child fields that have saved some data to commit entries ids so that they survive remounts
    useEffect(() => {

        if (dataFor(component)?.dynamicEntries) return

        const entriesHaveData = allDataKeys.some(k => k.indexOf(component.id) > 0)

        if (entriesHaveData) 
            changeDataFor(component, { dynamicEntries })
        
        // eslint-disable-next-line
    }, [allDataKeys.length])

    return <Fragment>
        <CardlistBox
            addIf={max === undefined || dynamicEntries.length < max}
            removeIf={(_) => dynamicEntries.length > min}
            id='dynamic-section'
            className='dynamic-section'
            singleLabel={addSingular}
            onAdd={() => changeDataFor<SectionData>(component, { dynamicEntries: [...dynamicEntries, newDynamicId(component)], ...rest })}
            onRemove={index => removeEntry(dynamicEntries[index])}
            onMove={(from, to) => changeDataFor<SectionData>(component, { dynamicEntries: reorderElements(dynamicEntries, from, to), ...rest })}>
            {

                dynamicEntries.map((card, cardPosition) => <RenderLoopContext.Provider key={cardPosition} value={card}>
                    {component.children.map((child, position) => {
                        const { Render, id } = componentSpecOf(child)

                        const contents = <Render key={position} component={child} />

                        return id === rowspecid ? <div key={`div_${position}`} style={{ marginTop: -10 }}>{contents}</div> : contents
                    })}
                </RenderLoopContext.Provider>)

            }
        </CardlistBox>

    </Fragment>

}

const SingleRender = (props: RenderProps<Section>) => {

    const { component } = props

    const { componentSpecOf } = useLayoutComponents()

    return <Fragment>{
        component.children.map((child, position) => {
            const { Render } = componentSpecOf(child)

            return <Render key={position} component={child} />

        })
    }
    </Fragment>
}

const DynamicPreview = (props: PreviewProps<Section>) => {

    const { component, path } = props

    const { componentSpecOf, extend } = useLayoutComponents()
    const { dataFor, changeDataFor } = useComponentData()

    const { ll } = useLayoutSettings()

    const max = component.dynamicProps?.max
    const min = component.dynamicProps?.min ?? 1

    //eslint-disable-next-line
    const defaultDynamicEntries = useMemo(() => newDynamicBag(component, min), [min])

    const { dynamicEntries = defaultDynamicEntries, ...rest } = dataFor<SectionData>(component) ?? defaultSectionData(component)

    const addSingular = component.dynamicProps?.addSingular ? ll(component.dynamicProps?.addSingular) : ''

    const removeEntry = (id: string) => changeDataFor<SectionData>(component, { dynamicEntries: dynamicEntries.filter(entry => entry !== id), ...rest })


    return <Fragment>
        <CardlistBox
            addIf={max === undefined || dynamicEntries.length < max}
            removeIf={(_) => dynamicEntries.length > min}
            id='dynamic-section'
            className='dynamic-section'
            singleLabel={addSingular}
            onAdd={() => changeDataFor<SectionData>(component, { dynamicEntries: [...dynamicEntries, newDynamicId(component)], ...rest })}
            onRemove={index => removeEntry(dynamicEntries[index])}
            onMove={(from, to) => changeDataFor<SectionData>(component, { dynamicEntries: reorderElements(dynamicEntries, from, to), ...rest })}>
            {

                dynamicEntries.map(card => <div key={card}>
                    <RenderLoopContext.Provider value={card}>
                        {component.children.map((child, position) => {
                            const { Preview, id } = componentSpecOf(child)

                            const contents = <Preview key={position} component={child} path={extend(path).at(position)} />

                            return id === rowspecid ? <div style={{ marginTop: -10 }}>{contents}</div> : contents
                        })}
                    </RenderLoopContext.Provider>
                </div>)

            }
        </CardlistBox>

    </Fragment>

}

function PrintPreview(props: PrintProps<Section>) {

    const { component } = props

    const { componentSpecOf } = useLayoutComponents()
    const { dataFor } = useComponentData()

    const { dynamicEntries = newDynamicBag(component, component.dynamicProps?.min ?? 1) } = dataFor<SectionData>(component) ?? defaultSectionData(component)

    const children = component.children.map((child, position) => {
        const { PrintPreview } = componentSpecOf(child)
        return <PrintPreview {...props} key={position} component={child} />
    })

    const contents = component.dynamic ? dynamicEntries.map(card => <View key={card}>
        <RenderLoopContext.Provider value={card}>
            {children}
        </RenderLoopContext.Provider>
    </View>) : children

    return <View>{contents}</View>
}

function Print(props: PrintProps<Section>) {

    const { component } = props

    const { componentSpecOf } = useLayoutComponents()

    const { dataFor } = useComponentData()

    const { dynamicEntries = newDynamicBag(component, component.dynamicProps?.min ?? 1) } = dataFor<SectionData>(component) ?? defaultSectionData(component)

    const children = component.children.map((child, position) => {
        const { Print } = componentSpecOf(child)
        return <Print {...props} key={position} component={child} />
    })


    const contents = component.dynamic ? dynamicEntries.map(card => <View key={card}>
        <RenderLoopContext.Provider value={card}>
            {children}
        </RenderLoopContext.Provider>
    </View>) : children

    return <View>{contents}</View>
}