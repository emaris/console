
import { CardlistBox } from '#app/components/CardlistBox'
import { Switch } from '#app/form/Switch'
import { TextBox } from '#app/form/TextBox'
import { VSelectBox } from '#app/form/VSelectBox'
import { icns } from '#app/icons'
import { useT } from '#app/intl/api'
import { useL } from '#app/model/multilang'
import { shortid } from '#app/utils/common'
import { Validation } from '#app/utils/validation'
import { useCurrentCampaign } from '#campaign/hooks'
import { submissionIcon, submissionType } from '#campaign/submission/constants'
import { SubmissionReactContext } from '#campaign/submission/context'
import { Submission } from '#campaign/submission/model'
import { useLayoutApis, useLiveLayoutApis } from '#layout/apicontext'
import { DetailProps } from '#layout/components/detail'
import { ComponentDraggable, ComponentDroppable } from '#layout/components/dnd'
import { BaseOffscreen, useSpecOf } from '#layout/components/helper'
import { Component, ComponentSpec, Container, PreviewProps, PrintProps, RenderProps, isContainer, nameOf, newComponentFrom } from '#layout/components/model'
import { ComponentPreview } from '#layout/components/preview'
import { LayoutAssetContext, ParameterContext, RenderLoopContext, useLayout } from '#layout/context'
import { isInstance } from '#layout/model'
import { newParameterFrom } from '#layout/parameters/model'
import { alignmentStyleId } from '#layout/style/alignment'
import { backgroundStyleId } from '#layout/style/background'
import { borderStyleId } from '#layout/style/border'
import { heightStyleId } from '#layout/style/height'
import { StyleProps } from '#layout/style/model'
import { spacingStyleId } from '#layout/style/spacing'
import { StyledBox } from '#layout/style/styledbox'
import { widthStyleId } from '#layout/style/width'
import { ProductLabel } from '#product/Label'
import { useProductClient } from '#product/client'
import { productIcon, productType } from '#product/constants'
import { Product, ProductDto, useProductModel } from '#product/model'
import { useProductStore } from '#product/store'
import { RequirementLabel } from '#requirement/Label'
import { useRequirementClient } from '#requirement/client'
import { requirementIcon, requirementType } from '#requirement/constants'
import { Requirement, RequirementDto, useRequirementModel } from '#requirement/model'
import { useRequirementStore } from '#requirement/store'
import { View } from '@react-pdf/renderer'
import { Empty, Icon, Tooltip } from 'antd'
import { utils } from 'apprise-frontend-core/utils/common'
import { taskPool } from 'apprise-frontend-core/utils/taskpool'
import { partition } from 'lodash'
import { Fragment, PropsWithChildren, ReactNode, useContext, useMemo, useRef } from 'react'
import { MdOutlineLoop } from 'react-icons/md'
import { AssetLiveData, AssetParameter, assetParamId } from '../parameters/asset'
import { isLiveParameter } from '../parameters/model'
import { ComponentCustomName } from './componenthelper'
import { FalseCondition } from './conditionalsection'
import { LiveData, useConditionMatch, useLiveConditionMatch } from './conditions'
import { LayoutAssetReferenceContext } from './context'
import { NamedRequirement } from './requirementsection'
import { LayoutScrollSubscriber } from './scroll'
import './styles.scss'


export type NamedAsset = {

    id: string
    asset: string
    alias: string
    type: 'product' | 'requirement'

}

export type AugmentedNamedAsset = Omit<NamedAsset, 'asset'> & {

    asset: RequirementDto | ProductDto


}

const defaultLoopAlias = 'asset'
const defaultLoopId = 'i-th'

export const isNamedAsset = (asset: NamedAsset | NamedRequirement | undefined): asset is NamedAsset => {
    if (asset === undefined) return false
    return (asset as NamedAsset).asset !== undefined
}

export const sectionAssetOf = (nasset: NamedAsset | NamedRequirement | undefined): string => isNamedAsset(nasset) ? (nasset?.asset ?? nasset?.id)! : (nasset?.requirement ?? nasset?.id)!
export const sectionAssetFor = (nasset: NamedAsset | undefined) => nasset?.type


export type AssetSection = Container & StyleProps & {

    mode: 'single' | 'loop'
    assets: NamedAsset[]

}

export const assetSectionid = 'assetsection'
const assetSectionName = 'layout.components.asset.name'

export const useAssetSectionComponent = (): ComponentSpec<AssetSection> => {



    return useSpecOf({

        id: assetSectionid,
        icon: c => c?.mode === 'loop' ? <Icon component={MdOutlineLoop} /> : icns.asset,
        name: assetSectionName,


        styles: () => ({

            [backgroundStyleId]: {},
            [widthStyleId]: {},
            [heightStyleId]: {},
            [borderStyleId]: {},
            [alignmentStyleId]: {},
            [spacingStyleId]: {}

        })

        ,

        Title

        ,

        generate: (): AssetSection => ({ ...newComponentFrom(assetSectionid), assets: [], mode: 'single', style: {}, children: [] })


        ,

        Offscreen,
        Detail,
        Preview,
        Render,
        Print,
        PrintPreview
    })
}

function Title({ component }: { component: AssetSection }) {



    const t = useT()
    const l = useL()

    const reqstore = useRequirementStore()
    const prodstore = useProductStore()

    const assets = component.assets ?? []

    if (component.mode === 'loop') {
        return <span>{component.shortname ?
            t('layout.components.asset.loop_alias_title', { name: component.shortname ?? t(assetSectionName), count: assets.length })
            : t('layout.components.asset.loop_title', { count: assets.length })}
        </span>
    }


    const firstAssetType = assets[0]?.type

    const firstAsset = firstAssetType === requirementType ? reqstore.safeLookup(sectionAssetOf(assets[0]))
        : firstAssetType === productType ? prodstore.safeLookup(sectionAssetOf(assets[0]))
            : undefined


    if (firstAsset) {

        let title = l(firstAsset.name) as ReactNode

        if (assets.length > 1)
            title = t("layout.components.asset.title_and_others", { name: title, count: assets.length - 1 })

        return firstAssetType === requirementType ?

            <RequirementLabel noMemo noIcon requirement={firstAsset as Requirement} title={title} noDecorations noLink />
            :
            <ProductLabel noMemo noIcon product={firstAsset as Product} title={title} noDecorations noLink />


    }

    else

        return <span>{t(component.shortname ?? assetSectionName)}</span>


}


function Detail(props: DetailProps<AssetSection>) {


    const source = useContext(LayoutAssetContext)

    const t = useT()
    const reqstore = useRequirementStore()
    const reqmodel = useRequirementModel()
    const prodstore = useProductStore()
    const prodmodel = useProductModel()

    const { component, onChange } = props

    const assets = component.assets

    const allRequirements = reqstore.all()
    const allProducts = prodstore.all()


    const allTargets: TargetAsset[] = useMemo(() => [

        ...allRequirements.sort(reqmodel.comparator).map(r => ({ ...r, label: <RequirementLabel requirement={r} noLink />, type: requirementType })),
        ...allProducts.sort(prodmodel.comparator).map(p => ({ ...p, label: <ProductLabel product={p} noLink />, type: productType }))

    ].filter(a => a.id !== source.id)

        // eslint-disable-next-line
        , [allRequirements, allProducts])



    return <Fragment>

        <ComponentCustomName {...props} />

        <Switch disabled={!!assets.length} label={t('layout.components.asset.mode_lbl')}
            validation={{ msg: t(`layout.components.asset.mode_${component.mode}`) }}
            onChange={v => onChange({ ...component, mode: v ? 'loop' : 'single' })}>
            {component.mode === 'loop'}
        </Switch>

        {component.mode === 'loop' ? <LoopDetail {...props} allTargets={allTargets} /> : <SingleDetail {...props} allTargets={allTargets} />}

    </Fragment>

}

function SingleDetail(props: DetailProps<AssetSection> & { allTargets: TargetAsset[] }) {


    const l = useL()
    const t = useT()

    const reqclient = useRequirementClient()
    const prodclient = useProductClient()

    const { component, onChange, allTargets } = props

    const alltargetmap = utils().index(allTargets).by(t => t.id)

    const [boundTargets, unboundTargets] = partition(allTargets, asset => component.assets.some(a => a.asset === asset.id))

    const isEmpty = index => !component.assets[index]?.alias || component.assets[index].alias.length === 0
    const isDuplicate = index => !isEmpty(index) && component.assets.find((r, j) => j !== index && component.assets[index]?.alias === r?.alias)
    const validation = index => {

        const filled = !!sectionAssetOf(component.assets[index])
        const empty = filled && isEmpty(index)
        const duplicate = filled && isDuplicate(index)

        return {

            status: empty ? 'warning' : duplicate ? 'error' : 'success',
            msg: empty ? t("layout.components.asset.empty_alias_warning") :
                duplicate ? t("layout.components.asset.duplicate_alias_error") :
                    t("layout.components.asset.full_alias")

        } as Validation

    }

    const addIf = unboundTargets.length > 0

    const onAdd = () => onChange({ ...component, assets: [...component.assets, { id: shortid(), asset: null!, alias: undefined!, type: undefined! }] })
    const onRemove = (index: number) => onChange({ ...component, assets: component.assets.filter((_, i) => i !== index) })

    return <CardlistBox id={`asset-${component.id}`} singleLabel={t("asset.singular")} addIf={addIf} onAdd={onAdd} onRemove={onRemove} >

        {component.assets.map((_, index) => {

            const placeholder = boundTargets[index]?.name.en?.replace(/\s/g, '-').toLowerCase() ?? t("layout.components.asset.alias_placeholder")

            return <div key={index} className="requirement-section-element">

                <VSelectBox placeholder={t("layout.components.asset.select_placeholder")} light

                    options={unboundTargets}

                    onChange={async (target: TargetAsset) => {

                        const id = target.id

                        // load target's layout if not loaded already.
                        await (target.type === requirementType ? reqclient : prodclient).fullFetch(target as any)

                        // keeping parameter id stable as target changes keeps existing references working.
                        return onChange({ ...component, assets: component.assets.map((a, i) => i === index ? { ...a, asset: id, type: target.type } as NamedAsset : a) })


                    }}


                    lblTxt={asset => l(asset.name)}
                    renderOption={(target: string | TargetAsset) => typeof target === 'string' ? alltargetmap[target].label : target.label}
                    optionId={target => 'LvbbsZCxX'}
                >

                    {boundTargets[index] ? [boundTargets[index]?.id] : undefined}

                </VSelectBox>


                <TextBox validation={validation(index)} placeholder={placeholder} disabled={!sectionAssetOf(component.assets[index])}
                    onChange={alias => onChange({
                        ...component,
                        assets: component.assets.map((r, i) => i === index ? { ...component.assets[index], alias } : r)
                    })}>
                    {component.assets[index]?.alias}
                </TextBox>
            </div>


        })}
    </CardlistBox>

}

type TargetAsset = (Requirement | Product) & { label: ReactNode, type: string }


function LoopDetail(props: DetailProps<AssetSection> & { allTargets: TargetAsset[] }) {


    const l = useL()
    const t = useT()
    const reqmodel = useRequirementModel()
    const { fullFetch: reqFullFetch } = useRequirementClient()
    const { fullFetch: prodFullFetch } = useProductClient()

    const { component, onChange, allTargets } = props

    const allTargetMap = useMemo(() => utils().index(allTargets).by(t => t.id), [allTargets])

    const boundTargets = component.assets.map(asset => allTargetMap[asset.asset])

    const boundtargetmap = utils().index(boundTargets).by(t => t.id)

    const onRemove = (index: number) => onChange({ ...component, assets: component.assets.filter((_, i) => i !== index) })
    const onMove = (from: number, to: number) => onChange({ ...component, assets: component.assets.flatMap((a, i) => i === to ? [component.assets[from], a] : i === from ? [] : [a]) })

    const setAssets = async (assets: TargetAsset[]) => {

        // fetches all requirements in throttling pool, lest we overwhelm the servers at scale.
        const added = assets.length === 0 ? [] : await taskPool<TargetAsset>({

            capacity: 3,
            tasks: assets,
            taskOf: target => target.type === requirementType ? reqFullFetch(target as Requirement) : prodFullFetch(target as Product)

        })
            .wait()
            // random id keep asset params apart for mgmt purposes but we don't need to worry about stability of references.
            // when we render, we use a fixed id anyway.
            .then(outcome => outcome.settled.map(target => ({ id: shortid(), alias: defaultLoopAlias, asset: target.id, type: target.type } as NamedAsset)))


        onChange({ ...component, assets: added })
    }

    const addAllRequirements = () => setAssets(allTargets.filter(r => r.type === requirementType))


    const reorder = () => onChange({ ...component, assets: component.assets.sort((a1, a2) => reqmodel.comparator(boundtargetmap[a1.asset] as Requirement, boundtargetmap[a2.asset] as Requirement)) })

    return <Fragment>

        <VSelectBox

            label={<span>{`${t("layout.components.asset.loop_items_lbl")}`} <span className='asset-loop-link' onClick={addAllRequirements}>({t("layout.components.asset.loop_items_allreqs")}):</span></span>}

            mode='multiple'

            placeholder={t("layout.components.asset.select_placeholder")}

            options={allTargets}

            onChange={setAssets}

            renderOption={(target: TargetAsset) => target.label}

            lblTxt={(target: TargetAsset) => l(target.name)}

            optionId={(target: TargetAsset) => target.id}>

            {boundTargets}

        </VSelectBox>


        <CardlistBox noPlaceholder id={`asset-${component.id}`} singleLabel={t("asset.singular")} addIf={false} onRemove={onRemove} onMove={onMove}

            label={<span>{t("layout.components.asset.loop_item_cards_lbl")} <span className='asset-loop-link' onClick={reorder}>({t("layout.components.asset.loop_items_reorder")}):</span></span>}>

            {component.assets.map((asset, index) =>

                <div key={index} className="asset-loop-item">
                    {boundtargetmap[asset.asset].label}
                </div>

            )}
        </CardlistBox>

    </Fragment>


}

function Preview(props: PreviewProps<AssetSection>) {

    const { component, path } = props

    const PreviewMode = props.component.mode === 'loop' ? LoopPreview : SinglePreview

    return <ComponentDroppable path={path}>
        <ComponentDraggable path={path} component={component}>
            <ComponentPreview path={path} component={component}>
                <PreviewMode {...props}>
                    <PreviewChildren {...props} />
                </PreviewMode>
            </ComponentPreview>
        </ComponentDraggable>
    </ComponentDroppable>

}


function PreviewChildren(props: PreviewProps<AssetSection>) {

    const { component, path } = props

    const t = useT()

    const layout = useLayout()

    const { components } = useLayoutApis()

    const empty = !component.assets?.length && !component.children

    return empty ?

        <Empty image={Empty.PRESENTED_IMAGE_SIMPLE} description={t('common.labels.select_one_or_more', { plural: t("asset.plural") })} />

        :

        <div className='section'>
            <StyledBox component={component} >

                {
                    component.children.map((child, position) => {

                        const { Preview } = components.componentSpecOf(child)

                        return <Preview key={position} component={child} path={components.extend(path).at(position)} />

                    })
                }

            </StyledBox>

            {!isInstance(layout) && <ComponentDroppable key={component.children.length} className="component-placeholder" path={components.extend(path).at(component.children.length)} />}

        </div>

}
function SinglePreview(props: PropsWithChildren<PreviewProps<AssetSection>>) {



    const { component, children: contents } = props

    const { reqstore, prodstore } = useLayoutApis()

    const layout = useLayout()

    const parameters = useSectionParameters(component)

    const augmented = component.assets.map(a => ({ ...a, asset: (a.type === requirementType ? reqstore : prodstore).safeLookup(a.asset) }))

    const match = useConditionMatch().matchContext()

    if (isInstance(layout) && !match.givenAssets(augmented.map(a => a.asset)))
        return <FalseCondition />


    return <LayoutAssetReferenceContext.Provider value={augmented}>
        <ParameterContext.Provider value={parameters}>
            {contents}
        </ParameterContext.Provider>
    </LayoutAssetReferenceContext.Provider>


}



function LoopPreview(props: PropsWithChildren<PreviewProps<AssetSection>>) {

    const { component, children: contents } = props

    const { reqstore, prodstore } = useLayoutApis()

    const layout = useLayout()

    const parameters = useSectionParameters(component)

    const parametermap = utils().index(parameters).by(p => p.value.id)

    const first = Object.values(parametermap)[0]

    const augmented: AugmentedNamedAsset[] = component.assets.map(a => ({ ...a, asset: (a.type === requirementType ? reqstore : prodstore).safeLookup(a.asset) }))

    const match = useConditionMatch().matchContext()

    return isInstance(layout) ?

        <Fragment>

            {augmented.map((augmented, i) => {

                if (!match.givenAsset(augmented.asset))
                    return <FalseCondition key={i} />

                // mount asset param with fixed id used also in design.
                return <LayoutAssetReferenceContext.Provider value={[augmented]}>
                    <ParameterContext.Provider key={augmented.asset.id} value={[loopParameterOf(parametermap[augmented.asset.id])]}>
                        <RenderLoopContext.Provider value={augmented.asset.id}>
                            {contents}
                        </RenderLoopContext.Provider>
                    </ParameterContext.Provider>
                </LayoutAssetReferenceContext.Provider>


            })}
        </Fragment>

        :

        // design with asset parameter at a fixed id.
        <LayoutAssetReferenceContext.Provider value={[augmented[0]]}>
            <ParameterContext.Provider value={[loopParameterOf(first)]}>
                {contents}
            </ParameterContext.Provider>
        </LayoutAssetReferenceContext.Provider>




}


function Render(props: RenderProps<AssetSection>) {

    const { component } = props

    return component.mode === 'loop' ? <LoopRender {...props} /> : <SingleRender {...props} />
}

function RenderChildren(props: RenderProps<AssetSection> & { missing: string[], parameters: AssetParameter[] }) {

    const t = useT()

    const { components: { componentSpecOf }, submissionCtx, reqinst, prodinst, dashboard, reqstore, prodstore } = useLiveLayoutApis()


    // const { allRows } = useLayoutRows()

    const { component, className = '', style, missing, parameters } = props

    const { campaign, party } = submissionCtx

    //const logged = useLogged()

   // const canSeeNotApplicable = party && logged.isTenantUser() ? partyinst.canSeeNotApplicable(party) : true

    // const rowSectionContext = useRowSectionContext()

    const containerRef = useRef<HTMLDivElement>(null);


    const conditionData: LiveData[] = parameters.map(p => ({
        submission: p.value.liveData?.submission as Submission,
        asset: p.value.liveData?.instance,
        campaign
    }))

    const match = useLiveConditionMatch().matchContext()

    if (!match.givenSubmissions(conditionData))
        return <FalseCondition />

    const warningLabelStyle = { color: 'orange' }
    const missingLabelStyle = { color: 'lightcoral' }
    const notInAudienceLabelStyle = { color: 'gray' }

    const isForParty = (asset: AssetParameter): boolean => {
        const type = asset.value.type
        const instance = asset.value.liveData?.instance
        return (party && instance) ? type === requirementType ? reqinst.isForParty(party, instance) :
            type === productType ? prodinst.isForParty(party, instance) : true : true

    }

    // const allRowsInRowSection = rowSectionContext ? allRows(rowSectionContext).get() : []
    // const idx = allRowsInRowSection.findIndex(row => component.children.some(innerrow => innerrow.id === row.id))
    // const top = (idx === -1 ? 0 : idx) * -1
    const top = 0

    const filteredParameters = parameters.filter(isForParty)

    const hasWarnings = filteredParameters.map(p => ['submitted', 'published'].includes(p.value.liveData?.submission?.lifecycle.state ?? '')).some(p => p === false)
    const hasMissing = filteredParameters.map(p => p.value.liveData?.submission?.lifecycle.state === 'missing').some(p => p === true)
    const isApplicable = filteredParameters.length > 0


    const linksToAssetsSubmissions = () => parameters.map(p => {

        // this is always live
        const { asset, submission } = p.value.liveData!

        const type = p.value.type

        const baseroute = dashboard.partyRouteToSubmissionWith(type, asset.id, party.source)

        const linkTo = submission ? `${baseroute}/${submission.id}` : baseroute

        const assessedAsMissing = p.value.liveData?.submission?.lifecycle.state === 'missing'

        const missingTarget = missing.includes(p.id)

        const inAudience = isForParty(p)

        const official = ['submitted', 'published'].includes(p.value.liveData?.submission?.lifecycle.state ?? '')

        const titleStyle = !inAudience ? notInAudienceLabelStyle : official ? undefined : (missingTarget || assessedAsMissing) ? missingLabelStyle : warningLabelStyle

        const tip = !inAudience ? () => t("submission.labels.not_applicable_badge") : missingTarget ? () => t("layout.components.asset.missing_target_warning") : assessedAsMissing ? () => t("layout.components.asset.assessed_missing_target") : undefined

        return type === requirementType ? <RequirementLabel key={p.id}
            linkTarget={submissionType}
            titleStyle={titleStyle}
            icon={p.value.liveData?.submission?.lifecycle.state === 'submitted' ? submissionIcon : p.value.liveData?.submission?.lifecycle.state === 'published' ? icns.publish : !inAudience ? { ...requirementIcon, props: { ...requirementIcon.props, style: { ...requirementIcon.props.style ?? {}, ...notInAudienceLabelStyle } } } : icns.unknown}
            requirement={asset.id}
            tip={tip}
            noLink={!inAudience || missingTarget}
            linkTo={linkTo} />
            :
            <ProductLabel key={p.id}
                linkTarget={submissionType}
                titleStyle={titleStyle}
                icon={p.value.liveData?.submission?.lifecycle.state === 'submitted' ? submissionIcon : p.value.liveData?.submission?.lifecycle.state === 'published' ? icns.publish : !inAudience ? { ...productIcon, props: { ...productIcon.props, style: { ...productIcon.props.style ?? {}, ...notInAudienceLabelStyle } } } : icns.unknown}
                product={asset.id}
                tip={tip}
                noLink={!inAudience || missingTarget}
                linkTo={linkTo} />
    })


    const wrapperIcon = hasWarnings ? icns.edit : requirementIcon

    // if (!isApplicable/*  && !canSeeNotApplicable */) return <Fragment />

    const warningClass = !isApplicable ? 'section-notapplicable-submission' : hasWarnings ? 'section-missing-submission' : ''
    const missingClass = missing.length > 0 || hasMissing ? 'section-missing-target' : ''

    const filterStyles = top < 1 ? ['paddingBottom', 'paddingTop'] : []

    const augmented = component.assets.map(a => ({ ...a, asset: (a.type === requirementType ? reqstore : prodstore).safeLookup(a.asset) }))

    // const augmented = component.assets.map(a => {

    //     const param = parameters.find(p=>p.value.id===a.asset)!

    //     const { asset } = param.value.liveData!

    //     return {...a, asset }

    // })

    return <LayoutAssetReferenceContext.Provider value={augmented}>

        <StyledBox component={component} style={{ top }} filterStyle={filterStyles}>

            <div style={{ ...style, top }} className={`requirement-section-contents ${className} ${missingClass} ${warningClass}`} ref={containerRef} >

                <span className="section-requirement-placeholder" >

                    <Tooltip
                        overlayStyle={{ minWidth: 'fit-content' }}
                        placement="left"
                        title={linksToAssetsSubmissions}
                        getPopupContainer={() => containerRef.current!}
                        autoAdjustOverflow={false}

                    >{wrapperIcon}</Tooltip>

                </span>

                {component.children.map(child => {

                    const { Render } = componentSpecOf(child)

                    return <LayoutScrollSubscriber key={child.id} component={child}>
                        <Render component={child} />
                    </LayoutScrollSubscriber>
                })
                }

            </div>

        </StyledBox>
    </LayoutAssetReferenceContext.Provider>
}

function SingleRender(props: RenderProps<AssetSection>) {

    const { component } = props

    const campaign = useCurrentCampaign()

    const { missing, parameters } = useLiveSectionParameters(component)

    const conditionData: LiveData[] = parameters.map(p => ({
        submission: p.value.liveData?.submission as Submission,
        asset: p.value.liveData?.instance,
        campaign
    }))

    const match = useLiveConditionMatch().matchContext()

    if (!match.givenSubmissions(conditionData))
        return <FalseCondition />

    return <ParameterContext.Provider value={parameters}>
        <RenderChildren {...props} parameters={parameters} missing={missing} />
    </ParameterContext.Provider>
}

function LoopRender(props: RenderProps<AssetSection>) {

    const { component } = props

    const { missing, parameters } = useLiveSectionParameters(component)

    const parametermap = utils().index(parameters).by(p => p.value.id)


    return <Fragment>

        {component.assets?.map(asset => {

            const parameter = [loopParameterOf(parametermap[asset.asset])]

            const missingparameter = missing.includes(parameter[0].id) ? [parameter[0].value.id] : []

            const ithComponent = { ...component, assets: [asset] } as AssetSection

            return <ParameterContext.Provider key={asset.asset} value={parameter}>
                <RenderLoopContext.Provider value={asset.asset}>
                    <LayoutScrollSubscriber component={ithComponent}>
                        <RenderChildren {...props} component={ithComponent} parameters={parameter} missing={missingparameter} />
                    </LayoutScrollSubscriber>
                </RenderLoopContext.Provider>
            </ParameterContext.Provider>
        })}

    </Fragment>
}

function Offscreen(props: RenderProps<AssetSection>) {

    const { component } = props

    return component.mode === 'loop' ? <OffscreenLoopRender {...props} /> : <OffscreenSingleRender {...props} />
}


function OffscreenSingleRender(props: RenderProps<AssetSection>) {

    const { component } = props

    const campaign = useCurrentCampaign()

    const { parameters } = useLiveSectionParameters(component)

    const conditionData: LiveData[] = parameters.map(p => ({
        submission: p.value.liveData?.submission as Submission,
        asset: p.value.liveData?.instance,
        campaign
    }))

    const match = useLiveConditionMatch().matchContext()

    if (!match.givenSubmissions(conditionData))
        return <Fragment />

    return <ParameterContext.Provider value={parameters}>
        <OffscreeRenderChildren {...props} parameters={parameters} />
    </ParameterContext.Provider>
}

function OffscreenLoopRender(props: RenderProps<AssetSection>) {

    const { component } = props

    const { parameters } = useLiveSectionParameters(component)

    const parametermap = utils().index(parameters).by(p => p.value.id)

    return <Fragment>
        {component.assets?.map(asset => {

            const parameter = [loopParameterOf(parametermap[asset.asset])]

            return <ParameterContext.Provider key={asset.asset} value={parameter}>
                <RenderLoopContext.Provider value={asset.asset}>
                    <OffscreeRenderChildren {...props} parameters={parameter} />
                </RenderLoopContext.Provider>
            </ParameterContext.Provider>
        })}
    </Fragment>
}

function OffscreeRenderChildren(props: RenderProps<AssetSection> & { parameters: AssetParameter[] }) {

    const { component, parameters } = props

    const { submissionCtx/* , reqinst, prodinst, partyinst  */} = useLiveLayoutApis()

    const { campaign/* , party  */} = submissionCtx

    // const canSeeNotApplicable = party ? partyinst.canSeeNotApplicable(party) : true

    const conditionData: LiveData[] = parameters.map(p => ({
        submission: p.value.liveData?.submission as Submission,
        asset: p.value.liveData?.instance,
        campaign
    }))

    const match = useLiveConditionMatch().matchContext()

    if (!match.givenSubmissions(conditionData))
        return <Fragment />

    // const isForParty = (asset: AssetParameter): boolean => {
    //     const type = asset.value.type
    //     const instance = asset.value.liveData?.instance
    //     return (party && instance) ? type === requirementType ? reqinst.isForParty(party, instance) :
    //         type === productType ? prodinst.isForParty(party, instance) : true : true

    // }

    // const filteredParameters = parameters.filter(isForParty)

    // const isApplicable = filteredParameters.length > 0

    // if (!isApplicable /* && !canSeeNotApplicable */)
    //     return <Fragment />


    return <BaseOffscreen component={component} />

}




// plain helper functions

const makeParameterFor = (id: string, type: string, asset: Requirement | Product, alias: string): AssetParameter => ({

    ...newParameterFrom(assetParamId),
    bridgeMode: true,
    fixedValue: true,
    id,
    name: alias ?? asset.name.en?.replace(/\s/g, '-').toLowerCase(),
    components: [],
    data: undefined!,
    value: { id: asset.id, type }

})

// used on render in loop mode to synchronuse references to asset parameters across design and preview/live.
const loopParameterOf = (a: AssetParameter) => ({ ...a, id: defaultLoopId })


const flatten = (c: Component): Component[] => isContainer(c) ? c.children.flatMap(flatten) : [c]





/* 
    synthesises parameters from the requirements in scope, 
    including metadata exported by _named_ components in the requirements.
 
    returns the requirements and the synthesised parameters.
 
*/
export const useSectionParameters = (section: AssetSection) => {

    const { registry, reqstore, prodstore } = useLayoutApis()

    const assets = section.assets ?? []

    return useMemo(() => {

        // attaches sources to assets, for convenience of computations below.
        const targets = assets
            .filter(a => a.id !== undefined && a.asset !== null)
            .map(a => ({ ...a, asset: (a.type === requirementType ? reqstore : prodstore).safeLookup(sectionAssetOf(a)) }))

        // creates parameters for each ass adding the data exported by their named components.
        const parameters = targets.map(({ id, asset, alias, type }) => {

            const param = makeParameterFor(id, type, asset, alias)

            // collects all the named components with their specifications.
            const components = flatten(asset.properties.layout.components ?? []).filter(r => !!r.shortname)

            // collects data from each component and indexes it by component name.
            const data = components.reduce((acc, c) =>

                ({ ...acc, [nameOf(c)]: registry.lookupComponent(c.spec).staticDataOf(c) })

                , {})

            return { ...param, components, data }
        })

        // returns parameters
        return parameters


    }

        //eslint-disable-next-line
        , [assets])


}


/* 
 
    synthesises parameters from the requirements in scope, including any data exported by their components.
    
    returns the requirements AND the synthesised parameters.
 
*/
export const useLiveSectionParameters = (component: AssetSection) => {

    const { settings, reqinst, reqstore, prodinst, prodstore, registry, assetParam } = useLiveLayoutApis()

    const reqinstances = reqinst.all()
    const prodinstances = prodinst.all()

    const instances = useMemo(() => [...reqinstances, ...prodinstances], [reqinstances, prodinstances])

    const ctx = useContext(SubmissionReactContext)

    const language = settings.resolveLanguage()

    // some requriements can be left incomplete in the detail UI
    const isComplete = (a:NamedAsset) => sectionAssetOf(a) && a.type

    const componentAssets = component.assets.filter(isComplete)

    return useMemo(() => {

    
        // resolves the requirements in this section and pairs them with their aliases.
        const targets = componentAssets.filter(isComplete).map(t => ({
            id: t.id,
            asset: t.type === requirementType ? reqstore.safeLookup(sectionAssetOf(t)) : prodstore.safeLookup(sectionAssetOf(t)),
            alias: t.alias,
            type: t.type,
            missing: !instances.some(i => i.source === t.asset)
        }))
        
        // creates parameters for each requirement adding to the data the current state of their named components.
        const parameters: AssetParameter[] = targets.map(({ id, asset, alias, type }) => {

            const param = makeParameterFor(id, type, asset, alias)

            param.value.liveData = { asset } as AssetLiveData

            const value = assetParam.valueFor(ctx, param)

            // collects all the named components with their specifications specifications.
            const components = flatten(asset.properties.layout.components).filter(r => !!r.shortname)

            // all the parameters of the target requirement, so they can be passed to components to resolve the data.
            // live parameters from the requirement need to be initialised to this context.
            const assetparameters = asset.properties.layout.parameters.map(p => {

                const spec = registry.lookupParameter(p.spec)

                const liveParameterValue = isLiveParameter(spec) ? spec.valueFor({ ...ctx, submission: value.liveData?.submission! }, p) : p.value

                const resolvedvalue = value.liveData?.instance?.properties?.parameterOverlay?.[p.name]?.value ?? liveParameterValue

                const initialised = { ...p, value: resolvedvalue }

                return initialised

            })

            // asks each component to derive data from their state in the current submission, and indexes it by component name.
            const data = {}

            components.reduce((acc, c) => {

                acc[nameOf(c)] = value.liveData?.submission ?

                    registry.lookupComponent(c.spec).dynamicDataOf(c,
                        {
                            data: value.liveData.submission.content.data[c.id],
                            resources: value.liveData.submission.content.resources[c.id]
                        },

                        assetparameters) : undefined

                return acc

            }, data)

            return { ...param, value, components, data }
        })


        return { missing: targets.filter(t => t.missing).map(t => t.id), parameters }


    }

        //eslint-disable-next-line
        , [componentAssets, language])
}


function PrintPreview(props: PrintProps<AssetSection>) {

    const PreviewMode = props.component.mode === 'loop' ? LoopPrintPreview : SinglePrintPreview

    return <PreviewMode {...props}>
        <PrintPreviewChildren {...props} />
    </PreviewMode>


}

function PrintPreviewChildren(props: PrintProps<AssetSection>) {

    const { component } = props

    const { components } = useLayoutApis()

    const empty = !component.assets?.length && !component.children

    return empty ?

        <View />

        :

        <View>

            {
                component.children.map((child, position) => {

                    const { PrintPreview } = components.componentSpecOf(child)

                    return <PrintPreview key={position} {...props} component={child} />

                })
            }

        </View>

}

function SinglePrintPreview(props: PropsWithChildren<PrintProps<AssetSection>>) {

    const { component, children: contents } = props

    const reqstore = useRequirementStore()
    const prodstore = useProductStore()

    const parameters = useSectionParameters(component)

    const assets = component.assets.map(a => (a.type === requirementType ? reqstore : prodstore).safeLookup(a.asset))

    const match = useConditionMatch().matchContext()

    const augmented = component.assets.map(a => ({ ...a, asset: (a.type === requirementType ? reqstore : prodstore).safeLookup(a.asset) }))

    if (!match.givenAssets(assets))
        return <View />

    return <LayoutAssetReferenceContext.Provider value={augmented}>
        <ParameterContext.Provider value={parameters}>
            {contents}
        </ParameterContext.Provider>
    </LayoutAssetReferenceContext.Provider>


}

function LoopPrintPreview(props: PropsWithChildren<PrintProps<AssetSection>>) {

    const { component, children: contents } = props

    const reqstore = useRequirementStore()
    const prodstore = useProductStore()

    const parameters = useSectionParameters(component)

    const parametermap = utils().index(parameters).by(p => p.value.id)

    const assets = component.assets.map(a => (a.type === requirementType ? reqstore : prodstore).safeLookup(a.asset))

    const assetmap = utils().index(assets).by(a => a.id)

    const match = useConditionMatch().matchContext()

    const augmented: AugmentedNamedAsset[] = component.assets.map(a => ({ ...a, asset: (a.type === requirementType ? reqstore : prodstore).safeLookup(a.asset) }))

    return <View>

        {component.assets?.map(asset => {

            if (!match.givenAsset(assetmap[asset.asset]))
                return <View />

            // mount asset param with fixed id used also in design.
            return <LayoutAssetReferenceContext.Provider value={augmented}>
                <ParameterContext.Provider key={asset.asset} value={[loopParameterOf(parametermap[asset.asset])]}>
                    <RenderLoopContext.Provider value={asset.asset}>
                        {contents}
                    </RenderLoopContext.Provider>
                </ParameterContext.Provider>
            </LayoutAssetReferenceContext.Provider>


        })}
    </View>


}

function Print(props: PrintProps<AssetSection>) {
    const { component } = props

    return component.mode === 'loop' ? <LoopPrintRender {...props} /> : <SinglePrintRender {...props} />
}

function PrintChildren(props: PrintProps<AssetSection> & { missing: string[], parameters: AssetParameter[] }) {

    const { components, submissionCtx, reqinst, prodinst, reqstore, prodstore } = useLiveLayoutApis()

    const { campaign, party } = submissionCtx

    const { component, parameters } = props

    //const logged = useLogged()

    //const canSeeNotApplicable = party && logged.isTenantUser() ? partyinst.canSeeNotApplicable(party) : true

    const conditionData: LiveData[] = parameters.map(p => ({
        submission: p.value.liveData?.submission as Submission,
        asset: p.value.liveData?.instance,
        campaign: campaign
    }))

    const match = useLiveConditionMatch().matchContext()

    if (!match.givenSubmissions(conditionData))
        return <View />

    const isForParty = (asset: AssetParameter): boolean => {
        const type = asset.value.type
        const instance = asset.value.liveData?.instance
        return (party && instance) ? type === requirementType ? reqinst.isForParty(party, instance) :
            type === productType ? prodinst.isForParty(party, instance) : true : true

    }

    const isApplicable = !!parameters.find(isForParty)

    if (!isApplicable) return <View />

    const warningClass = !isApplicable ? { color: 'gray' } : {}

    const augmented = component.assets.map(a => ({ ...a, asset: (a.type === requirementType ? reqstore : prodstore).safeLookup(a.asset) }))

    return <LayoutAssetReferenceContext.Provider value={augmented}>
        <View>

            {component.children.map(child => {

                const { Print } = components.componentSpecOf(child)

                return <View key={child.id} style={warningClass}><Print {...props} component={child} key={child.id} /></View>


            })
            }

        </View>
    </LayoutAssetReferenceContext.Provider>
}

function SinglePrintRender(props: PrintProps<AssetSection>) {

    const { component } = props

    const campaign = useCurrentCampaign()

    const { missing, parameters } = useLiveSectionParameters(component)

    const conditionData: LiveData[] = parameters.map(p => ({
        submission: p.value.liveData?.submission as Submission,
        asset: p.value.liveData?.instance,
        campaign
    }))

    const match = useLiveConditionMatch().matchContext()

    if (!match.givenSubmissions(conditionData))
        return <View />

    return <ParameterContext.Provider value={parameters}>
        <PrintChildren {...props} parameters={parameters} missing={missing} />
    </ParameterContext.Provider>
}

function LoopPrintRender(props: PrintProps<AssetSection>) {

    const { component } = props

    const { missing, parameters } = useLiveSectionParameters(component)

    const parametermap = utils().index(parameters).by(p => p.value.id)


    return <View >
        {component.assets?.map(asset => {

            const parameter = [loopParameterOf(parametermap[asset.asset])]
            const missingparameter = missing.includes(parameter[0].id) ? [parameter[0].value.id] : []

            return <ParameterContext.Provider key={asset.asset} value={parameter}>
                <RenderLoopContext.Provider value={asset.asset}>
                    <PrintChildren {...props} parameters={parameter} missing={missingparameter} />
                </RenderLoopContext.Provider>
            </ParameterContext.Provider>
        })}
    </View>
}

