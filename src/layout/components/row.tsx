import { Button } from "#app/components/Button"
import { Drawer, DrawerAnchor } from "#app/components/Drawer"
import { Draggable } from "#app/dnd/Draggable"
import { Droppable } from "#app/dnd/Droppable"
import { Form } from "#app/form/Form"
import { Switch } from "#app/form/Switch"
import { TextBox } from "#app/form/TextBox"
import { icns } from "#app/icons"
import { useT } from '#app/intl/api'
import { TocEntry, TocLayout } from "#app/scaffold/TocLayout"
import { useFeedback } from '#app/utils/feedback'
import { paramsInQuery, updateQuery } from "#app/utils/routes"
import { useLayoutApis, useLiveLayoutApis } from '#layout/apicontext'
import StyledView from '#layout/pdf/styledview'
import { alignmentStyleId } from "#layout/style/alignment"
import { useLayoutStyles } from '#layout/style/api'
import { backgroundStyleId, useBackgroundStyle } from "#layout/style/background"
import { borderStyleId, useBorderStyle } from "#layout/style/border"
import { discreteWidthStyleId, useDiscreteWidthStyle } from "#layout/style/discretewidth"
import { StyleProps } from "#layout/style/model"
import { absoluteSpacingStyleId, spacingStyleId, useAbsoluteSpacingStyle } from "#layout/style/spacing"
import { StyledBox } from "#layout/style/styledbox"
import { widthStyleId } from "#layout/style/width"
import { Icon, Input, Tooltip } from "antd"
import merge from 'deepmerge'
import { TFunction } from "i18next"
import * as React from "react"
import { BiText } from "react-icons/bi"
import { GoArrowSwitch } from "react-icons/go"
import { MdBorderStyle } from "react-icons/md"
import { Link, useHistory, useLocation } from "react-router-dom"
import { LayoutAssetContext, LayoutContext, useLayout } from "../context"
import { isInstance } from '../model'
import { useLayoutRows } from './api'
import { ConditionFields } from "./conditionfields"
import { LayoutConditions, useConditionMatch, useLiveConditionMatch } from "./conditions"
import { ComponentDetail, DetailProps } from "./detail"
import { ComponentDraggable, ComponentDroppable } from "./dnd"
import { useSpecOf } from "./helper"
import { RowContext, useRow, useRowContext, useRowSection, useRowSectionContext } from "./hooks"
import { Component, ComponentSpec, Container, DetailStyleProps, PreviewProps, PrintProps, RenderProps, newComponentFrom } from "./model"
import { ComponentPreview } from "./preview"
import { RowSectionSpec } from "./rowsection"
import { LayoutScrollSubscriber } from "./scroll"
import { FalseCondition } from './conditionalsection'


export const rowspecid = 'row'
export const cellId = 'row-cell'

export type Cell = StyleProps & Container & {
    type: string
    alias?: string
    virtual: boolean
    index: number
    conditions: LayoutConditions
}

export type Row = StyleProps & Container<Cell> & {
    alias: string
}

export const cellDetailParam = 'component-row-cell'
export const cellDetailInnerParam = 'component-row-cell-inner'
export const cellDetailInnerConditionParam = `${cellDetailInnerParam}-cond`

const defaultBorder = {
    top: 0,
    bottom: 0,
    left: 0,
    right: 0,
    all: 0,
    type: 'solid',
    color: '#9E9E9E'
}

const defaultDesignerBorder = {
    top: 1,
    bottom: 1,
    left: 1,
    right: 1,
    all: 1,
    type: 'solid',
    color: '#F5F5F5'
}

const defaultBg = { color: '#FFFFFF' }

const defaultSpacing = {
    bottom: 0,
    left: 0,
    right: 0,
    top: 0
}

const defaultWidth = { percentage: 56 }

export const useRowComponent = (): ComponentSpec<Row> => {

    const t = useT()

    const cellcomponent = useCellComponent()


    return useSpecOf({

        id: rowspecid,
        icon: icns.row,
        name: 'layout.components.row.name',
        showTabsOnDetails: true,

        Title: ({ component }: { component: Row }) => {


            const alias = component.alias ? component.alias : t('layout.components.row.name')

            return <>{alias}</>

        }

        ,

        generate: (t?: TFunction, state?: any): Row => {

            const component = newComponentFrom(rowspecid)

            const rowName = t ? t('layout.components.row.name') : 'Row'
            const cellName = t ? t('layout.components.row.cell.name') : 'Cell'

            const cell1 = { ...cellcomponent.generate(t), alias: `${cellName}-1`, index: 0 }
            // const cell2 = {...cellspec.generate(t), alias: `${cellName}-2`, index: 1}

            const cells = state ? (state as RowSectionSpec).cells.map(cell => ({ ...cellcomponent.generate(t, state), alias: cell.alias, index: cell.index })) : [cell1]

            return {
                ...component,
                asTemplate: state ? true : false,
                children: cells,
                alias: rowName,
                style: {},
                extraStyle: { name: t ? t('layout.components.row.cell.details.name') : 'Cell Styles' }
            }
        }

        ,

        styles: (map, _) => ({

            [widthStyleId]: map[widthStyleId],
            [alignmentStyleId]: map[alignmentStyleId],
            [spacingStyleId]: map[spacingStyleId]

        }),

        extraStyles: (map, state) => {

            return {
                name: t('layout.components.row.cell.details.name'),
                [backgroundStyleId]: map[backgroundStyleId],
                [discreteWidthStyleId]: map[discreteWidthStyleId],
                [borderStyleId]: map[borderStyleId],
                [absoluteSpacingStyleId]: map[absoluteSpacingStyleId]
            }
        }

        ,

        Offscreen: () => <React.Fragment />,  // no offscreen render inside rows.

        Detail,

        Render,

        Print,

        PrintPreview,

        Preview,

    })

}

function Preview(props: PreviewProps<Row>) {

    const { path, component } = props

    const layout = useLayout()
    const rows = useLayoutRows()
    const rowSectionContext = useRowSectionContext()

    const templateCells = useRowTemplate()

    const discreteWidthStyle = useDiscreteWidthStyle()

    const direction = isInstance(layout) ? 'horizontal' : 'vertical'

    const style = rows.getRowStyle(component, rowSectionContext).style

    const marginBottom = rowSectionContext ? 0 : 15

    // const offsetStyle = { position: 'relative', top: `-${offset}px` } as React.CSSProperties
    const offsetStyle = { position: 'relative', marginTop: -1, marginBottom } as React.CSSProperties

    const asset = React.useContext(LayoutAssetContext)

    const matcher = useConditionMatch()

    const getConditionsForCell = (idx) => isInstance(layout) ? templateCells?.[idx]?.conditions : undefined

    const filteredComponents = !isInstance(layout) ? component.children : component.children.filter((_, idx) =>
        matcher.match(getConditionsForCell(idx)).givenAsset(asset)
    )

    const prev = <div className={`row-widget ${direction}`} style={offsetStyle}>{

        filteredComponents.map((cell, idx) => {

            // const cellIdx = cell.index

            // const cellTemplate = (templateCells && cellIdx > -1) ?
            //     templateCells.length >= cellIdx ? templateCells[cellIdx] :
            //         { style: {}, alias: 'unknown' } : { style: {}, alias: 'unknown' }

            const cellTemplate = templateCells?.find(template => template.index === cell.index) ?? { style: {}, alias: 'unknown' }

            const cellWidth = isInstance(layout) ?
                component.asTemplate ?
                    cellTemplate.style[discreteWidthStyleId] ?? defaultWidth
                    // : cell.style[discreteWidthStyleId] ?? { percentage: 100 / filteredComponents.length }
                    : cell.style[discreteWidthStyleId] ?? { percentage: 50 }
                : { percentage: 100 }

            const className = (isInstance(layout) && idx > 0) ? 'cell-container cell-hide-left-border' : 'cell-container'

            const computedWidth = discreteWidthStyle.cssOf(cellWidth)

            return <div key={idx} style={computedWidth} className={className}>
                <CellPreview key={`cell-${idx}`} component={{ ...cell, conditions: getConditionsForCell(idx)! }} path={`${path}@${idx}`} id={cell.id} />
            </div>
        })


    }</div>

    return <LayoutScrollSubscriber component={component}>
        <ComponentDroppable path={path}>
            <RowContext key={component.id} value={component}>
                <ComponentDraggable path={path} component={component}>
                    <ComponentPreview path={path} component={component}>
                        {isInstance(layout) ? <StyledBox component={{ ...component, style }}>{prev}</StyledBox> : prev}
                    </ComponentPreview>
                </ComponentDraggable>
            </RowContext>
        </ComponentDroppable>
    </LayoutScrollSubscriber>

}


function Render(props: RenderProps<Row>) {

    const { component } = props

    return <RowContext key={component.id} value={component}>
        {/* render lazy in any orientation, because likely to contain many cells. */}
        <LayoutScrollSubscriber component={component} >
            <InnerRender component={component} />
        </LayoutScrollSubscriber>
    </RowContext>

}

function InnerRender(props: RenderProps<Row>) {

    const { component } = props

    const rowSectionContext = useRowSectionContext()

    const { submissionCtx: subctx } = useLiveLayoutApis()

    const layout = useLayout()

    const rows = useLayoutRows()

    const templateCells = useRowTemplate()

    const style = rows.getRowStyle(component, rowSectionContext).style

    const marginBottom = rowSectionContext ? 0 : 15

    const offsetStyle = { position: 'relative', marginTop: -1, marginBottom } as React.CSSProperties

    const discreteWidthStyle = useDiscreteWidthStyle()

    const matcher = useLiveConditionMatch()

    const getConditionsForCell = (index:number) => isInstance(layout) ? templateCells?.[index]?.conditions : undefined

    const filteredComponents = component.children.filter((_, idx:number) =>

        matcher.match(getConditionsForCell(idx)).givenSubmission({ submission: subctx.submission, asset: subctx.asset, campaign: subctx.campaign })

    )

    if (filteredComponents.length === 0) return <FalseCondition />

    return <StyledBox component={{ ...component, style }}><div className={`row-widget horizontal`} style={offsetStyle}>{

        filteredComponents.map((cell, idx) => {

            const cellTemplate = templateCells?.find(template => template.index === cell.index) ?? { style: {}, alias: 'unknown' }

            const cellWidth = isInstance(layout) ?
                component.asTemplate ?
                    cellTemplate.style[discreteWidthStyleId] ?? defaultWidth
                    : cell.style[discreteWidthStyleId] ?? { percentage: 100 / filteredComponents.length }
                : { percentage: 100 }

            // console.log({cell, cellTemplate, isInstance: isInstance(layout), cellWidth})

            const className = (isInstance(layout) && idx > 0) ? 'cell-container cell-hide-left-border' : 'cell-container'

            const computedWidth = discreteWidthStyle.cssOf(cellWidth)

            return <div key={idx} style={computedWidth} className={className}>
                <CellRender component={{ ...cell, conditions: getConditionsForCell(idx)! }} />
            </div>
        })


    }
    </div>
    </StyledBox>


}

Render.displayName = 'RowRender'



function Detail(props: DetailProps<Row>) {

    const { component, onChange } = props

    const t = useT()
    const fb = useFeedback()

    const { removeCellFromRow, addCellToRow, moveCellInRow, reconcileCellsWithRowSectionTemplate } = useLayoutRows()

    const { pathname, search } = useLocation()

    const rowSection = useRowSection()
    const row = useRow()

    const detailRoute = (cell: Cell, inner?: boolean) => `${pathname}?${updateQuery(search).with(params => params[inner ? cellDetailInnerParam : cellDetailParam] = cell.id)}`
    const conditionDetailRoute = (cell: Cell, inner?: boolean) => `${pathname}?${updateQuery(search).with(params => params[inner ? cellDetailInnerConditionParam : cellDetailParam] = cell.id)}`

    const onAddCell = () => row.setRowContext(addCellToRow(component))
    const onRemoveCell = (idx) => row.setRowContext(removeCellFromRow(component, idx))
    const onMoveCell = (to, item) => row.setRowContext(moveCellInRow(component, to, item))

    const onFollowTemplate = (asTemplate: boolean) => {
        const modifiedRow = { ...component, asTemplate, children: asTemplate ? reconcileCellsWithRowSectionTemplate(component.children, rowSection) : component.children.map(cell => ({ ...cell, asTemplate: false })) }
        row.setRowContext(modifiedRow)
        onChange(modifiedRow)
    }

    const onFollowTemplateChange = (asTemplate: boolean) => {
        fb.askConsent({
            title: t('layout.components.row.follow_template.warn.title'),
            content: t('layout.components.row.follow_template.warn.msg'),
            onOk: () => onFollowTemplate(asTemplate)
        })
    }

    const onChangeAlias = (alias) => {
        const modifiedRow = { ...component, alias }
        row.setRowContext(modifiedRow)
        onChange(modifiedRow)
    }


    return <div className="row-details">
        <TextBox
            label={t('layout.components.row.field.label')}
            validation={{ msg: t('layout.components.row.field.msg') }}
            onChange={onChangeAlias}>
            {component.alias}
        </TextBox>
        {rowSection && <Switch
            label={t('layout.components.row.follow_template.name')}
            validation={{ msg: t('layout.components.row.follow_template.msg') }}
            onChange={onFollowTemplateChange}
        >{component.asTemplate}</Switch>}
        <RowCellCards
            cells={component.children}
            onAddCell={onAddCell}
            onRemoveCell={onRemoveCell}
            onMoveCell={onMoveCell}
            routeToDetail={(cell) => detailRoute(cell, true)}
            routeToConditions={(cell) => conditionDetailRoute(cell, true)}
            disabled={rowSection && component.asTemplate}
        />
    </div>
}


/* For Cells */
export const useCellComponent = (): ComponentSpec<Cell> => {


    return useSpecOf({

        id: cellId,
        icon: icns.rectangle,
        name: 'layout.components.row.cell.name',
        active: false,
        tabsOnDetails: ['style'],
        showTabsOnDetails: false,


        generate: (_?: TFunction, state?: any): Cell =>
        ({
            ...newComponentFrom(cellId),
            style: {},
            children: [],
            type: 'cell',
            virtual: false,
            asTemplate: state ? true : false,
            index: undefined!,
            conditions: undefined!
        })

        ,

        styles: (map, _) => ({

            [backgroundStyleId]: map[backgroundStyleId],
            [discreteWidthStyleId]: map[discreteWidthStyleId],
            [borderStyleId]: map[borderStyleId],
            [absoluteSpacingStyleId]: map[absoluteSpacingStyleId]
        })

        ,

        Detail: CellDetail

        ,

        Render: CellRender

        ,

        Preview: CellPreview

        ,

        Print: CellPrintCompo

        ,

        PrintPreview: CellPrintCompo

        ,

        StyleDetail
    })
}



function CellPreview(props: PreviewProps<Cell>) {

    const state = React.useContext(LayoutContext)

    const { settings, components } = useLayoutApis()
    const { extend, componentSpecOf, changeComponent } = components

    const { getCollapsedStateForComponent } = settings

    const { pathname, search } = useLocation()

    const { component, path } = props

    const { alias, border, bg, spacing } = useCellStyles({ cell: props.component })

    const rowSection = useRowSection()

    const collapsed = getCollapsedStateForComponent(component)

    const detailRoute = (cell: Cell, inner?: boolean) => `${pathname}?${updateQuery(search).with(params => params[inner ? cellDetailInnerParam : cellDetailParam] = cell.id)}`

    const nextPosition = component.children.length

    const isLayoutInstance = isInstance(state.layout)

    const showBadge = !isLayoutInstance

    const asTemplate = rowSection ? component.asTemplate : false

    return <div className={`cell ${!isLayoutInstance ? 'cell-margins' : ''} ${collapsed && !isLayoutInstance ? 'collapsed' : ''}`} style={{ ...border, ...bg }}>
        {showBadge && <div className="cell-badge-wrapper"><div className="cell-badge">{asTemplate ? <div>{alias}</div> : <Link to={detailRoute(component)}>{alias}</Link>}</div></div>}

        <div className='cell-contents' style={{ ...spacing, wordBreak: 'break-word' }}>
            {component.children.map((component, idx) => {
                const { Preview } = componentSpecOf(component)
                return <Preview component={component} path={`${path}@${idx}`} key={component.id} />
            })}
        </div>

        <ComponentDroppable key={nextPosition} className="component-placeholder" path={extend(path).at(nextPosition)} />

        <ComponentDetail component={component} onChange={component => changeComponent(path, component)} detailParam={cellDetailParam} />

    </div>

}


function CellRender(props: RenderProps<Cell>) {

    const layout = useLayout()

    const { settings, components } = useLayoutApis()

    const { componentSpecOf } = components
    const { getCollapsedStateForComponent } = settings

    const { component } = props

    const { bg, spacing, border } = useCellStyles({ cell: component })

    const collapsed = getCollapsedStateForComponent(component)

    return <div className={`cell ${!isInstance(layout) ? 'cell-margins' : ''} ${collapsed && !isInstance(layout) ? 'collapsed' : ''}`} style={{ ...border, ...bg }}>
        <div className='cell-contents' style={{ ...spacing }}>
            {component.children.map((component) => {

                const { Render } = componentSpecOf(component)

                return <Render key={component.id} component={component} />

            })}
        </div>
    </div>
}


function CellDetail(props: DetailProps<Cell>) {

    const t = useT()

    const { component, onChange } = props

    const rowsection = useRowSection()

    return <>

        {!component.virtual &&
            <Switch
                label={t('layout.components.row.follow_template.name')}
                validation={{ msg: t('layout.components.row.follow_template.msg') }}
                onChange={asTemplate => onChange({ ...component, asTemplate })}
            >{component.asTemplate}</Switch>}

        <TextBox
            label={t('layout.components.row.cell.field.label')}
            disabled={rowsection && component.asTemplate}
            validation={{ msg: t('layout.components.row.cell.field.msg') }}
            onChange={alias => onChange({
                ...component,
                alias
            })}>
            {component.alias}
        </TextBox>
    </>
}

function StyleDetail(props: DetailStyleProps<Cell>) {

    const { component, onChange } = props

    const t = useT()

    const { registry, components, } = useLayoutApis()

    const { componentSpecOf, pathFor } = components

    const template = useRowTemplate()
    const { row, setRowContext } = useRow()

    const idx = (pathFor(component) !== '' ? parseInt(pathFor(component).split('@').pop()) : template?.findIndex(t => t.id === component.id)) ?? 0

    const style = component.style

    const cspec = componentSpecOf(component)

    const overlay = cspec.styles(style, component)

    const specs = Object.keys(overlay).map(registry.lookupStyle)

    const onChangeStyle = (style) => {
        onChange(style)
        row && setRowContext({ ...row, children: row.children.map(cell => cell.id === component.id ? style : cell) })
    }

    const onModifyStyle = (v, id) => onChangeStyle({ ...component, style: { ...component.style, [id]: v } })
    const onResetStyle = (id) => {
        const style = component.style
        delete style[id]
        onChangeStyle({ ...component, style })
    }
    const onResetAllStyles = () => onChangeStyle({ ...component, style: {} })

    const valueprops = (template && idx >= 0) ? merge(template[idx].style, style) : style

    const resetStyle = (id, name) => <Tooltip title={t('layout.components.row.cell.details.reset_one.tooltip', { style: name })} placement="left">
        <Button className="reset-style-btn" onClick={() => onResetStyle(id)} type='link' >{t('layout.components.row.cell.details.reset_one.name')}...</Button>
    </Tooltip>

    const resetAll = <Tooltip title={t('layout.components.row.cell.details.reset_all.tooltip')} placement="left">
        <Button className="reset-style-btn all" iconLeft icn={icns.revert} onClick={() => onResetAllStyles()}>{t('layout.components.row.cell.details.reset_all.name')}</Button>
    </Tooltip>

    return <TocLayout className="styles-tab" offsetTop={168}>

        <div className="cell-styles-heading">
            <div>
                {resetAll}
            </div>
        </div>

        {specs.map(({ name, id }, i) => <TocEntry key={i} id={id} title={name} />)}

        <div className="styles-sections">{

            specs.map(({ id, name, Render }, i) =>

                <DrawerAnchor id={id} offset={158} key={i} className={`style-section section-${id}`}>

                    <div className="cell-style-heading-wrapper">
                        <div className="cell-style-title">
                            <h1 className="section-title">{t(name, { id })}</h1>
                        </div>
                        <div className="cell-style-revert">{resetStyle(id, t(name, { id }))}</div>
                    </div>

                    <Render id={id} key={i} valueprops={{ ...valueprops[id], ...overlay[id] }} allStyles={style} onChange={v => onModifyStyle(v, id)} />

                </DrawerAnchor>


            )}
        </div>

    </TocLayout>

}



/* ----- Some Helper ----- */

export type CellDnd = {
    type: 'cell',
    index: number
}

export type RowCellCardsProps = {
    cells: Cell[]
    onAddCell: () => void
    onRemoveCell: (idx: number) => void
    routeToDetail: (_: Cell) => string
    routeToConditions: (_: Cell) => string
    onMoveCell: (to: string, item: CellDnd) => void
    onChangeCell?: (path: string, c: Component) => void
    disabled?: boolean
}

export const RowCellCards = (props: RowCellCardsProps) => {
    const t = useT()

    const { components: { changeComponent, pathFor } } = useLayoutApis()

    const history = useHistory()
    const fb = useFeedback()

    const rowSection = useRowSection()
    const rowSectionContext = useRowSectionContext()

    const [edit, editCell] = React.useState<Cell>(undefined!)

    const { cells, onAddCell, onRemoveCell, routeToDetail, routeToConditions, onMoveCell, onChangeCell = changeComponent, disabled = false } = props

    const editRef = React.useRef<Input[]>([])

    React.useEffect(() => { editRef.current = editRef.current.slice(0, cells.length) }, [cells])

    //eslint-disable-next-line
    React.useEffect(() => editRef.current?.[cells.findIndex(c => c.id === edit?.id)]?.focus(), [edit])

    const innerRemoveCell = (index) => {
        const title = cells[index].virtual ? t('layout.components.row.cell.remove.title_virtual') : t('layout.components.row.cell.remove.title')
        const content = cells[index].virtual ? t('layout.components.row.cell.remove.msg_virtual') : t('layout.components.row.cell.remove.msg')
        fb.askConsent({
            title,
            content,
            onOk: () => onRemoveCell(index)
        })
    }

    const addBtn = <Button
        className="add-one"
        type="primary"
        disabled={disabled}
        onClick={onAddCell}>{t('layout.components.row.cell.add_one')}</Button>

    const removebtn = index => <Tooltip title={t("common.buttons.remove")}><Button disabled={disabled || !!edit} noborder icn={icns.removeEntry} onClick={() => innerRemoveCell(index)} /></Tooltip>
    const editBtn = index => <Tooltip title={t("common.buttons.edit")}><Button disabled={disabled || !!edit} noborder icn={<Icon component={BiText} />} onClick={() => editCell(cells[index])} /></Tooltip>
    const openBtn = route => <Tooltip title={t("common.buttons.edit_styles")} placement="topLeft"><Button disabled={disabled || !!edit} noborder icn={<Icon component={MdBorderStyle} />} onClick={() => history.push(route)} /></Tooltip>
    const openCondBtn = route => <Tooltip title={t("layout.components.row.cell.edit_conditions")} placement="topLeft"><Button disabled={disabled || !!edit} noborder icn={<Icon component={GoArrowSwitch} />} onClick={() => history.push(route)} /></Tooltip>

    // const followTemplateBtn = index => 
    //     !cells[index].virtual ? <Tooltip title={cells[index].asTemplate ? t("layout.components.row.follow_template.unlock_msg") : t("layout.components.row.follow_template.lock_msg")} placement="topRight">
    //         <Button disabled={disabled || !!edit} noborder icn={cells[index].asTemplate ? icns.lock() : icns.unlock({color: 'orange'})} onClick={()=>onChangeCell(pathFor(cells[index]), {...cells[index], asTemplate: !cells[index].asTemplate} as Component)} />
    //     </Tooltip> : <></>


    const onChange = (idx: number, val?: string) => {
        const currentVal = val ?? editRef.current[idx].state.value

        if (currentVal !== undefined && currentVal.trim() !== '') onChangeCell(pathFor(cells[idx]), { ...cells[idx], alias: currentVal } as Component)

    }

    const onPressEnter = (idx: number) => {
        onChange(idx)
        editCell(undefined!)
    }

    const renderCellInner = (cell: Cell, idx: number) => {
        const cellDetailRoute = routeToDetail(cell)
        const cellDetailConditionRoute = routeToConditions(cell)
        const disabledClass = (disabled || edit) ? 'disabled' : ''
        const editClass = (edit && edit.id === cell.id) ? 'edit' : ''
        const asCellTemplate = rowSection && cell.asTemplate
        const alias = asCellTemplate ? rowSectionContext!.cells[idx].alias : cell.alias
        return <div className={`row-card ${disabledClass} ${editClass}`} key={idx}>
            <div className="cell-badge-container">
                {disabled ? alias :
                    edit && edit.id === cell.id ?
                        <Input
                            className="cell-badge-edit-input"
                            ref={(el: any) => editRef.current[idx] = el}
                            defaultValue={cell.alias}
                            onChange={el => onChange(idx, el.target.value)}
                            onPressEnter={_ => onPressEnter(idx)}

                            onBlur={_ => editCell(undefined!)} />
                        :
                        <Button type={"ghost"} noborder onClick={() => editCell(cell)}>{alias}</Button>


                }</div>

            <div className="row-buttons">
                {/* {rowSectionContext && <div className="cell-badge-template">
                            {followTemplateBtn(idx)}
                        </div>} */}
                <div className="cell-badge-edit">
                    {editBtn(idx)}
                </div>
                <div className="cell-badge-edit">
                    {openBtn(cellDetailRoute)}
                </div>
                <div className="cell-badge-edit">
                    {openCondBtn(cellDetailConditionRoute)}
                </div>
                <div className="row-remove">
                    {removebtn(idx)}
                </div>
            </div>
        </div>
    }

    const renderCell = (cell: Cell, idx: number) => {
        return (disabled || edit) ? renderCellInner(cell, idx) :
            <Droppable<CellDnd> key={idx} path={`${idx}`} types={['cell']} onDrop={(to, item) => onMoveCell(to, item)}>
                <Draggable<CellDnd> item={{ type: 'cell', index: idx }}>
                    {renderCellInner(cell, idx)}
                </Draggable>
            </Droppable>
    }


    return <div className="row-cells">
        {cells.map((cell, i) => <ComponentDetail key={i} component={cell} onChange={component => onChangeCell(pathFor(cell), component)} detailParam={cellDetailInnerParam} />)}
        {cells.map((cell, i) => <CellConditions key={i} component={cell} onChange={component => onChangeCell(pathFor(cell), component)} routeId={cellDetailInnerConditionParam} />)}
        {addBtn}
        <div className="row-cards">
            {cells.map(renderCell)}
        </div>
    </div>

}

type CellConditionsProps = {
    routeId: string
    component: Cell
    onChange: (component: any) => void
}

const CellConditions = (props: CellConditionsProps) => {

    const { routeId, component } = props

    const t = useT()
    const { pathname, search } = useLocation()
    const history = useHistory()

    const { [cellDetailInnerConditionParam]: detail } = paramsInQuery(search)

    const visible = component.id === detail

    const onChange = (conditions: LayoutConditions) => props.onChange({ ...component, conditions })

    const close = () => history.push(`${pathname}?${updateQuery(search).with(params => { params[cellDetailInnerConditionParam] = null })}`)

    const name = `${component.alias ?? component.id} ${t('layout.components.row.cell.conditions')}`

    return <Drawer title={name} routeId={routeId} visible={visible} onClose={close} width={700}>
        <Form>
            <div style={{ paddingTop: 10 }}>
                <ConditionFields conditions={component.conditions} onChange={onChange} />
            </div>
        </Form>
    </Drawer>
}


export const useRowTemplate = (): Cell[] | undefined => {

    const rowSectionContext = useRowSectionContext()

    const rowContext = useRowContext()

    const { getRowStyleTemplate } = useLayoutRows()

    return rowContext ? getRowStyleTemplate(rowContext, rowSectionContext) :
        rowSectionContext ?
            rowSectionContext.cells.map(cell => ({ ...cell, style: merge(rowSectionContext.extraStyle ?? {}, cell.style) }))
            : undefined
}


export const useCellStyles = (props: { cell: Cell }) => {

    const { cell: component } = props

    const layout = useLayout()
    
    // const { pathFor } = useLayoutComponents()

    const templateCells = useRowTemplate()

    // const cellIdx = parseInt(pathFor(component).split("@").pop() || '-1')

    const cellTemplate = templateCells?.find(template => template.index === component.index)

    const isLayoutInstance = isInstance(layout)

    const getBorderProp = (c: Cell, prop: string) => {

        const defaultTemplate = isLayoutInstance ? defaultBorder : defaultDesignerBorder

        const styleAsTemplate = cellTemplate?.style[borderStyleId] ? cellTemplate.style[borderStyleId][prop] ?? (prop === 'type' ? 'solid' : defaultTemplate[prop]) : defaultTemplate[prop]

        const styleForCompo = c.style?.[borderStyleId] ? c.style?.[borderStyleId][prop] ?? (prop === 'type' ? 'solid' : defaultTemplate[prop]) : defaultTemplate[prop]

        return cellTemplate ? styleAsTemplate : styleForCompo
    }

    const borderFor = (c: Cell) => ({
        top: getBorderProp(c, 'top'),
        bottom: getBorderProp(c, 'bottom'),
        left: getBorderProp(c, 'left'),
        right: getBorderProp(c, 'right'),
        type: getBorderProp(c, 'type'),
        color: getBorderProp(c, 'color'),
        all: getBorderProp(c, 'all')
    })

    const border = borderFor(component)

    const bg = {
        ...defaultBg,
        ...cellTemplate ? cellTemplate.style[backgroundStyleId] ?? {} : component.style[backgroundStyleId] ?? {}
    }

    const spacing = {
        ...defaultSpacing,
        ...cellTemplate ? cellTemplate.style[absoluteSpacingStyleId] ?? {} : component.style[absoluteSpacingStyleId] ?? {}
    }


    const computedBorder = useBorderStyle().cssOf(border)

    const computedBg = useBackgroundStyle().cssOf(bg)

    const computedSpacing = useAbsoluteSpacingStyle().cssOf(spacing)

    const alias = (cellTemplate && component.asTemplate) ? cellTemplate.alias : component.alias

    return { bg: computedBg, border: computedBorder, spacing: computedSpacing, alias }
}

function PrintPreview(props: PrintProps<Row>) {

    const { component, View } = props
    const rows = useLayoutRows()
    const rowSectionContext = useRowSectionContext()
    const row = useRowContext()

    const marginLeft = row ? row.children.length === 0 ? 0 : row.children.length - 1 : 0
    const style = { ...rows.getRowStyle(component, rowSectionContext).style }

    const marginBottom = rowSectionContext ? 0 : 15

    const marginTop = -1

    // We might want wrap={true} for View that build up a row,
    //  this would allow to never have rows cutting between pages
    //  unfortunatelly happens that sometimes contents of a row exceed a full page space
    //  if we do not wrap in this case we might encounter problems of garbage text or infinite loop
    //  because the library does not know how to fit the contents in a single page where there is not enough space.
    //  To avoid this we have to accept the fact that rows can be splitted across multiple pages.
    return <RowContext key={component.id} value={component}>
        <StyledView {...props} style={{ paddingBottom: 0, padding: 0, marginBottom }} component={{ ...component, style }} >
            <View wrap={false} style={{ display: 'flex', flexDirection: 'row', minHeight: "50px", width: "100%", marginLeft, marginTop, marginBottom }}>
                {
                    component.children.map((cell, idx) => <CellPrintCompo {...props} key={idx} component={cell} preview />)
                }
            </View>
        </StyledView>
    </RowContext>
}


function Print(props: PrintProps<Row>) {

    const { component, View } = props
    const layout = useLayoutRows()
    const rowSectionContext = useRowSectionContext()
    const row = useRowContext()

    const style = layout.getRowStyle(component, rowSectionContext).style

    const marginLeft = row ? row.children.length === 0 ? 0 : row.children.length - 1 : 0

    const marginBottom = rowSectionContext ? 0 : 15

    const marginTop = -1

    return <RowContext key={component.id} value={component}>
        <StyledView {...props} style={{ paddingBottom: 0, padding: 0, marginBottom }} component={{ ...component, style }} >
            <View style={{ display: 'flex', flexDirection: 'row', minHeight: "50px", width: "100%", marginLeft, marginTop, marginBottom }}>
                {
                    component.children.map((cell, idx) => <CellPrintCompo {...props} key={idx} component={cell} />)
                }
            </View>
        </StyledView>
    </RowContext>
}

export const CellPrintCompo = (props: PrintProps<Cell> & { preview?: boolean }) => {

    const layout = useLayout()
    const styles = useLayoutStyles()

    const { component, preview = false } = props

    const { bg, border } = useCellStyles({ cell: component })
    const templateCells = useRowTemplate()

    const row = useRowContext()

    const cellTemplate = templateCells?.find(template => template.index === props.component.index)

    const cellWidth = isInstance(layout) ?
        props.component.asTemplate ?
            cellTemplate?.style[discreteWidthStyleId] ?? defaultWidth
            : props.component.style[discreteWidthStyleId] ?? { percentage: 50 }
        : { percentage: 100 }


    const computedWidth = useDiscreteWidthStyle().cssOf(cellWidth)

    const resolvedWidth = Object.keys(computedWidth).reduce((acc, next) => {
        const style = styles.mapHtmlStyleToPdfStyle(next, computedWidth[next])
        return style ? { ...acc, ...style } : acc
    }, {})

    const resolvedbg = Object.keys(bg).reduce((acc, next) => {
        const style = styles.mapHtmlStyleToPdfStyle(next, bg[next])
        return style ? { ...acc, ...style } : acc
    }, {})

    const resolvedborder = Object.keys(border).reduce((acc, next) => {
        const style = styles.mapHtmlStyleToPdfStyle(next, border[next])
        return style ? { ...acc, ...style } : acc
    }, {})


    const leftOffset = (row ? row.children.findIndex(c => c.id === props.component.id) > -1 ? row.children.findIndex(c => c.id === props.component.id) : 0 : 0) * -1

    const outerStyles = { ...resolvedWidth, ...resolvedbg, ...resolvedborder, left: leftOffset }

    return preview ? CellPrintPreview({ ...props, outerStyles }) : CellPrint({ ...props, outerStyles })

}

type CellPrinterProps = PrintProps<Cell> & {
    outerStyles: any
}

function CellPrint(props: CellPrinterProps) {

    const { component, wrap = true, View, outerStyles } = props

    const { styles, components, submissionCtx } = useLiveLayoutApis()

    const { submission, asset, campaign } = submissionCtx

    const templateCells = useRowTemplate()

    const layout = useLayout()

    const { spacing } = useCellStyles({ cell: component })

    const cellTemplate = templateCells?.find(template => template.index === props.component.index)

    const matcher = useLiveConditionMatch()
    const conditions = (isInstance(layout) && cellTemplate) ? cellTemplate.conditions : undefined

    if (!matcher.match(conditions).givenSubmission({ submission, asset, campaign })) {
        return <View />
    }

    const resolvedspacing = Object.keys(spacing).reduce((acc, next) => {
        const style = styles.mapHtmlStyleToPdfStyle(next, spacing[next])
        return style ? { ...acc, ...style } : acc
    }, {})

    return <View style={outerStyles} wrap={wrap}  >
        <View style={{ ...resolvedspacing }} wrap={wrap}>
            {component.children.map((child, position) => {

                const { Print } = components.componentSpecOf(child)

                return <Print {...props} key={position} component={child} wrap />

            })}
        </View>
    </View>
}


function CellPrintPreview(props: CellPrinterProps) {
    const { component, wrap = true, View, outerStyles } = props

    const { components, styles } = useLayoutApis()

    const layout = useLayout()

    const { spacing } = useCellStyles({ cell: component })

    const resolvedspacing = Object.keys(spacing).reduce((acc, next) => {
        const style = styles.mapHtmlStyleToPdfStyle(next, spacing[next])
        return style ? { ...acc, ...style } : acc
    }, {})



    const asset = React.useContext(LayoutAssetContext)

    const matcher = useConditionMatch()

    if (isInstance(layout) && !matcher.match(component.conditions).givenAsset(asset)) return <View />

    return <View style={outerStyles} wrap={wrap}  >
        <View style={{ ...resolvedspacing }} wrap={wrap}>
            {component.children.map((child, position) => {

                const { PrintPreview } = components.componentSpecOf(child)

                return <PrintPreview {...props} key={position} component={child} wrap />

            })}
        </View>
    </View>
}