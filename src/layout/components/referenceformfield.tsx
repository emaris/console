import { Label } from '#app/components/Label'
import { Switch } from '#app/form/Switch'
import { VSelectBox } from '#app/form/VSelectBox'
import { useT } from '#app/intl/api'
import { useLayoutApis } from '#layout/apicontext'
import { useProductClient } from '#product/client'
import { productType } from '#product/constants'
import { ProductLabel } from '#product/Label'
import { Product, useProductModel } from '#product/model'
import { useProductStore } from '#product/store'
import { useRequirementClient } from '#requirement/client'
import { requirementType } from '#requirement/constants'
import { RequirementLabel } from '#requirement/Label'
import { Requirement, useRequirementModel } from '#requirement/model'
import { useRequirementStore } from '#requirement/store'
import { useL } from 'apprise-frontend-core/intl/multilang'
import { utils } from 'apprise-frontend-core/utils/common'
import { FC, useContext, useEffect, useMemo } from 'react'
import { DetailProps } from './detail'
import { FieldComponent, isField } from './fielddetail'
import { Component, isContainer, nameOf } from './model'

import { OptionPanel } from '#layout/components/optionpanel'
import { noFieldRefeference, useFieldReference } from './fieldref'
import { LayoutAssetContext } from '#layout/context'


type FormFieldsProperities<T extends FieldComponent> = DetailProps<T> & {

    filter?: (field: T) => boolean
    Extension?: FC<DetailProps<T> & { target: Requirement | Product, targetField: T }>
}

export const ReferenceFormFields = <T extends FieldComponent>(props: FormFieldsProperities<T>) => {

    const l = useL()
    const t = useT()

    const { component, onChange, Extension } = props

    const { components } = useLayoutApis()

    const spec = components.componentSpecOf(component)

    const { reference, assetMap, assetIds, fieldIds, fieldMap, validReference, defaultTargetId, targetAsset, field } = useFormFieldsHelper(props)

    const retroCompatibleOnChange = (enabled: boolean) => {

        const newRef = { ...reference ?? {}, enabled }

        const changed: T = { ...component, reference: newRef }

        // this is to avoid dirtying pre-existing assets.
        if (utils().deepequals(noFieldRefeference, newRef))
            delete changed.reference

        onChange(changed)

    }

    return <>

        <Switch label={t("layout.components.fieldref.enabled_lbl")} onChange={retroCompatibleOnChange}
            validation={{
                msg: validReference ? t("layout.components.fieldref.enabled_msg") : t("layout.components.fieldref.missing"),
                status: validReference ? 'success' : 'warning'

            }}>
            {!!reference.enabled}
        </Switch>


        <OptionPanel show={reference.enabled}>

            <Switch label={t("layout.components.fieldref.lineage_lbl")} onChange={(lineage: boolean) => onChange({ ...component, reference: { ...component.reference, lineage } })}
                validation={{
                    msg: t("layout.components.fieldref.lineage_msg")
                }}>
                {!!reference.lineage}
            </Switch>

            <VSelectBox placeholder={defaultTargetId ? reference.lineage ? t("layout.components.fieldref.default_asset_lineage") : t("layout.components.fieldref.default_asset") : undefined} clearable

                label={t("layout.components.fieldref.target_asset_lbl")}

                validation={{
                    msg: t("layout.components.fieldref.target_asset_msg")
                }}

                options={assetIds}

                onChange={(asset: string | undefined) => onChange({ ...component, reference: { ...component.reference, asset, field: undefined } })}

                renderOption={(target: string) => assetMap[target]?.label}

                lblTxt={(target: string) => l(assetMap[target]?.asset.name) ?? ''}>

                {reference.asset ? [reference.asset] : undefined}

            </VSelectBox>


            <VSelectBox

                placeholder={t("layout.components.fieldref.default_field")}

                label={t("layout.components.fieldref.target_field_lbl")}

                validation={{
                    msg: t("layout.components.fieldref.target_field_msg")
                }}

                options={fieldIds}

                onChange={(field: string | undefined) => onChange({ ...component, reference: { ...component.reference, field } })}

                renderOption={(field: string) => <Label icon={spec.icon} title={nameOf(fieldMap[field] ?? {})} />}

                lblTxt={(field: string) => nameOf(fieldMap[field] ?? {})}>

                {reference.field ? [reference.field] : undefined}

            </VSelectBox>

            {Extension && targetAsset?.asset && fieldMap[field] && <Extension key={targetAsset.asset.id} target={targetAsset.asset} targetField={fieldMap[field] as T} {...props} />}

        </OptionPanel>

    </>
}


// handles state management
const useFormFieldsHelper = <T extends FieldComponent>(props: FormFieldsProperities<T>) => {

    const { component, filter = () => true } = props

    const { original: reference, defaultTargetId, defaultFieldId, field } = useFieldReference(component)

    const reqclient = useRequirementClient()
    const reqmodel = useRequirementModel()

    const prodclient = useProductClient()
    const prodmodel = useProductModel()

    const allRequirements = useRequirementStore().all()
    const allProducts = useProductStore().all() ?? []

    const thisAsset = useContext(LayoutAssetContext)

    const { assetIds, assetMap } = useMemo(() => {

        const assets = [

            ...allRequirements.sort(reqmodel.comparator).map(a => ({

                // replaces this asset with "under edit" version to better support "self" references
                asset: a.id === thisAsset.id ? thisAsset : a,
                type: requirementType,
                label: <RequirementLabel requirement={a} noLink noDecorations />
            })),
            ...allProducts.sort(prodmodel.comparator).map(a => ({

                // replaces this asset with "under edit" version to better support "self" references
                asset: a.id === thisAsset.id ? thisAsset : a,
                type: productType,
                label: <ProductLabel product={a} noLink noDecorations />

            }))]

        const assetIds = assets.map(({ asset }) => asset.id)
        const assetMap = utils().index(assets).by(({ asset }) => asset.id)

        return { assetIds, assetMap }

        // eslint-disable-next-line
    }, [allRequirements, allProducts])



    // if lineage mode is on we default target asset to this asset's lineage, if any.

    const targetAssetIdOrDefault = reference.asset ?? defaultTargetId

    const fieldIdOrDefault = reference.field ?? defaultFieldId

    // if enabled, we must have targets, perhaps defaults.
    const validReference = !reference.enabled || (targetAssetIdOrDefault && fieldIdOrDefault)


    const targetAsset = targetAssetIdOrDefault ? assetMap[targetAssetIdOrDefault] : undefined


    const { fieldIds, fieldMap } = useMemo(() => {

        const rec = (c: Component | undefined): Component[] => c ?

            isContainer(c) ?
                c.children.reduce((a, c) => [...a, ...rec(c)], [] as Component[])
                :
                isField(c) && c.spec === component.spec && filter(c as T) ? [c] : []
            :

            []


        const targetLayout = targetAsset ? targetAsset.asset.properties.layout : undefined

        const fields = targetLayout ? rec(targetLayout?.components) : [] as Component[]
        const fieldIds = fields.map(f => f.id).sort()
        const fieldMap = utils().index(fields).by(f => f.id)

        return { fieldIds, fieldMap, }


        // eslint-disable-next-line
    }, [targetAsset,filter])



    // load up all assets (if we haven't yet).
    useEffect(() => {

        Promise.all([reqclient.fetchAll(), prodclient.fetchAll()])

        // eslint-disable-next-line
    }, [])

    // if the target asset changes, load up its layout (if don't have it yet).
    useEffect(() => {

        if (!targetAsset)
            return

        const type = targetAsset?.type

        const client = type === requirementType ? reqclient : prodclient

        client.fetchOne(targetAsset.asset.id)

        // eslint-disable-next-line
    }, [reference.asset])

    return { reference, validReference, defaultTargetId, defaultFieldId, assetMap, assetIds, fieldIds, fieldMap, targetAsset, field }
}
