
import React from "react";
import { Row } from "./row";
import { RowSectionSpec } from "./rowsection";




export type RowSectionContextProviderType = {
    rowSectionContext: RowSectionSpec, 
    setRowSectionContext : (_:any) => void
}


const RowSectionContextProvider = React.createContext<RowSectionContextProviderType>(undefined!)

export type RowSectionContextType = React.PropsWithChildren<{value: RowSectionSpec}>

export const RowSectionContext = (props: RowSectionContextType) => {

    const [rowSectionContext, setRowSectionContext] = React.useState<RowSectionSpec>(props.value)

    return <RowSectionContextProvider.Provider value={{ rowSectionContext, setRowSectionContext }}>
        {props.children}
    </RowSectionContextProvider.Provider>
}

export const useRowSection = () => React.useContext(RowSectionContextProvider)

export const useRowSectionContext = () => React.useContext(RowSectionContextProvider)?.rowSectionContext


export const useRowSectionContextModifier = () => React.useContext(RowSectionContextProvider)?.setRowSectionContext


export type RowContextProviderType = {
    row: Row, 
    setRowContext : (_:any) => void
}


const RowContextProvider = React.createContext<RowContextProviderType>(undefined!)

export type RowContextType = React.PropsWithChildren<{value: Row}>

export const RowContext = (props: RowContextType) => {

    const [row, setRowContext] = React.useState<Row>(props.value)

    return <RowContextProvider.Provider value={{ row, setRowContext }}>
        {props.children}
    </RowContextProvider.Provider>
}

export const useRow = () => React.useContext(RowContextProvider) ?? {row: undefined, setRowContext: () => null}

export const useRowContext = () => {
    const context = React.useContext(RowContextProvider)
    return context === undefined ? undefined : context.row
}
