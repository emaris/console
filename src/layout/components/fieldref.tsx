import { SubmissionReactContext } from '#campaign/submission/context'
import { useContext } from 'react'
import { FieldComponent, isField } from './fielddetail'
import { Component, isContainer } from './model'

import { Bytestream } from '#app/stream/model'
import { useCurrentCampaign } from '#campaign/hooks'
import { useSubmissionCalls } from '#campaign/submission/calls'
import { SubmissionStub, Trail } from '#campaign/submission/model'
import { useSubmissionProfile } from '#campaign/submission/profile'
import { useRevisions } from '#campaign/submission/revision'
import { useSubmissionLineage, useTrailLookups } from '#campaign/submission/store'
import { LayoutAssetContext } from '#layout/context'
import { partition } from 'lodash'
import moment from 'moment-timezone'
import { SectionDataContext } from './context'



export type FieldRef = Partial<{

    enabled: boolean
    asset: string
    field: string
    lineage: boolean

}>

export const noFieldRefeference: FieldRef = {

    enabled: false,
    lineage: true,
}

// augments the reference info inside a field, if one exists.
export const useFieldReference = <T extends FieldComponent>(component: T) => useLazyFieldReference()(component)


export const useLazyFieldReference = <T extends FieldComponent>() => {

    // instance exists in live mode
    const { asset: currentAssetInstance, relatedSubmissions = [], campaign } = useContext(SubmissionReactContext) ?? {}

    // asset is available in any mode.
    const currentAsset = useContext(LayoutAssetContext)

    return (component: T) => {

        // instance is most authoritative source for lineage, asset is fallback.
        const currentLineage = currentAssetInstance?.lineage?.source ?? currentAsset?.lineage?.[0]

        const reference = component.reference ?? noFieldRefeference

        // derives defaults values
        const defaultTargetId = reference.lineage ? currentLineage ?? currentAsset.id : undefined
        const defaultFieldId = component.id

        // materialises default values, or lacking those, fallbacks.
        const asset = reference.asset ?? defaultTargetId
        const field = reference.field ?? defaultFieldId ?? 'none'

        return {
            ...reference,
            asset,
            field,
            defaultTargetId,
            defaultFieldId,
            original: reference,
            currentAsset,
            currentAssetInstance,
            currentCampaign: campaign,
            targetCampaign: reference.lineage ? currentAssetInstance?.lineage?.campaign ?? campaign?.lineage : currentAssetInstance?.campaign,
            targetSubmissions: relatedSubmissions

        }
    }

}

export const useReferencedSubmissionsLoader = () => {

    const currentCampaign = useCurrentCampaign()

    const sublineage = useSubmissionLineage().on(currentCampaign)

    const subtrails = useTrailLookups()

    const subprofile = useSubmissionProfile().on(currentCampaign)

    const subrevisions = useRevisions()

    const subcalls = useSubmissionCalls()

    return {

        given: async (submission: SubmissionStub, trail: Trail) => {

            const { source, instance } = subprofile.profileOf(trail.key.assetType)

            const subasset = source(trail.key.asset)
            const subinstance = instance(trail.key.asset)


            // all fielsd with references 
            const flattenfields = (c: Component): FieldComponent[] => isContainer(c) ? c.children.flatMap(flattenfields) : isField(c) && c.reference?.enabled ? [c] : []

            // those to resolve in lineage campaign, and those to resolve in current campaign.
            const [referenceLineageAssets, referenceCurrentAssets] = partition(flattenfields(subasset.properties.layout.components), f => f.reference!.lineage)

            const temporallyCompatible = (s: SubmissionStub) => {

                // we take published date, or submission date if it doesn't exist
                const submissionCompatible = moment(s.lifecycle.lastSubmitted).isSameOrBefore(submission.lifecycle.lastSubmitted)

                const changeDate = submission.lifecycle.state === 'managed' ? undefined : submission.lifecycle.state === 'published' ? submission.lifecycle.lastPublished : submission.lifecycle.lastSubmitted ?? undefined

                const changeCompatible = moment(s.lifecycle.lastPublished ?? s.lifecycle.lastSubmitted).isSameOrBefore(changeDate)

                return submissionCompatible && changeCompatible
            }

            const currenttrails = subtrails.on(currentCampaign)

            // identify related submissions.

            const submissionsInCurrentCampaign = (currenttrails.allTrails() ?? [])

                .filter(t => referenceCurrentAssets.find(r => r.reference?.asset === t.key.asset && t.key.party === trail.key.party))
                .map(t => subrevisions.on(t).allOfficial().find(temporallyCompatible)!)
                .filter(s => s !== undefined)


            const lineage = sublineage.given({ trail })

            const lineageAsset = subinstance?.lineage?.source ?? subasset.lineage?.[0] ?? subasset.id

            const lineagetrails = subtrails.on(lineage.campaign!)

            const submissionsInLineageCampaign = lineage.campaign ? (lineagetrails.allTrails() ?? [])
                .filter(t => referenceLineageAssets.find(r => (r.reference!.asset ?? lineageAsset) === t.key.asset && t.key.party === trail.key.party))
                .map(t => subrevisions.on(t).all().find(temporallyCompatible)!)
                .filter(s => s !== undefined) : []


            const fetched = await Promise.all([

                submissionsInCurrentCampaign.length > 0 ? subcalls.on(currentCampaign).fetchManySubmissions(submissionsInCurrentCampaign).then(submissions => submissions.map(s => ({ submission: s, trail: currenttrails.lookupTrail(s.trail)! }))) : Promise.resolve([]),
                submissionsInLineageCampaign.length > 0 ? subcalls.on(lineage.campaign!).fetchManySubmissions(submissionsInLineageCampaign).then(submissions => submissions.map(s => ({ submission: s, trail: lineagetrails.lookupTrail(s.trail)! }))) : Promise.resolve([]),

            ]).then(([fetched, fetchedInLineage]) => [...fetched, ...fetchedInLineage])

            //console.log({ lineageAsset, referenceLineageAssets, submissionsInLineageCampaign, submissionsInCurrentCampaign, fetched })

            return fetched
        }
    }
}




export const useFieldReferenceResolver = <T extends FieldComponent, D extends any = any>(component: T, process: (_: any) => D = t => t) => useLazyFieldReferenceResolver()(component, process)

export const useLazyFieldReferenceResolver = <T extends FieldComponent, D extends any = any>() => {

    const { referencesEnabled } = useContext(SectionDataContext)

    const ctx = useContext(SubmissionReactContext) ?? []


    const fieldref = useLazyFieldReference()

    return (component: T, process: (_: any) => D = t => t as D) => {

        const { enabled, asset, field, targetSubmissions, targetCampaign } = fieldref(component)

        let resolved: D = undefined!
        let resolvedResources: Bytestream[] | undefined


        if (referencesEnabled && enabled) {

            const selfSubmission = asset === ctx.asset?.source && ctx.campaign?.id === targetCampaign

            const targetSubmission = selfSubmission ? ctx.submission :

                targetSubmissions.find(s => s.trail.key.asset === asset && s.trail.key.campaign === targetCampaign)?.submission

            resolved = process(targetSubmission?.content?.data[field])
            resolvedResources = targetSubmission?.content?.resources[field]

            //console.log({ name: component.shortname, data: targetSubmission?.content?.data, targetCampaign, asset, field, resolved, resources: targetSubmission?.content?.resources, resolvedResources })

        }

        return { resolved, resolvedResources }
    }
}

