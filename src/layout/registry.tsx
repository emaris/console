import { StyleSpec } from '#layout/style/model';
import { useContext } from 'react';
import { ComponentSpec } from "./components/model";
import { LayoutRegistryContext } from './context';
import { ParameterSpec } from "./parameters/model";





export const useLayoutRegistry = () => {

    const state = useContext(LayoutRegistryContext)

    const self = {

        addComponents: (specs: ComponentSpec<any> | ComponentSpec<any>[]) => {

            const all = Array.isArray(specs) ? specs : [specs]

            state.set(s => all.forEach(spec => s.components[spec.id] = spec))

            return self

        }

        ,

        addParameters: (specs: ParameterSpec<any, any> | ParameterSpec<any, any>[]) => {

            const all = Array.isArray(specs) ? specs : [specs]

            state.set(s => all.forEach(spec => s.parameters[spec.id] = spec))

            return self

        }

        ,

        addStyles: (specs: StyleSpec<any>[]) => {

            const all = Array.isArray(specs) ? specs : [specs]

            state.set(s => all.forEach(spec => s.styles[spec.id] = spec))

            return self

        }

        ,

        lookupComponent: (id: string) => state.get().components[id]

        ,

        allComponents: () => Object.values(state.get().components)

        ,

        lookupParameter: (id: string) => state.get().parameters[id]


        ,

        allParameters: () => Object.values(state.get().parameters)

        ,

        lookupStyle: (id: string) => state.get().styles[id] ?? unknownStyle(id)

        ,

        allStyles: () => Object.values(state.get().styles)
    }

    return self;

}

export const unknownStyle = (id: string) => ({

    id,
    name: 'layout.styles.unknown.name',
    type: "custom",
    cssOf: () => ({}),

    Render: () => <div>unknown style</div>

}) as StyleSpec
