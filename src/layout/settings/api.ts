import { useCurrentLanguage } from '#app/intl/api'
import { allLanguages, defaultLanguage, Language } from '#app/intl/model'
import { MultilangDto } from '#app/model/multilang'
import { Component } from '#layout/components/model'
import { LayoutSettingsContext } from "#layout/context"
import { useContext, useMemo } from 'react'


var rotatingLanguage: Language = undefined!

export const useLayoutSettings = () => {

    const settings = useContext(LayoutSettingsContext)

    // const params = parameterapi(state)

    const currentLanguage = useCurrentLanguage()

    const currentSettings = settings?.get()

    const self = useMemo(() => ({

        ...currentSettings

        ,

        collapseAll: () => {
            
                Promise.resolve(settings.set(s => s.allCollapsed= true)).then(() => settings.set(s => s.allCollapsed = false ))
}
        ,

        expandAll: () => Promise.resolve(settings.set(s => s.allExpanded = true)).then(() => settings.set(s => s.allExpanded = false))

        ,

        collapseComponent: (c: Component) => settings.set(s => s.collapsed[c.id] = true)

        ,

        expandComponent: (c: Component) => Promise.resolve(settings.set(s =>s.collapsed[c.id] = false ))

        ,

        getCollapsedStateForComponent: (c: Component) => settings.get().collapsed[c.id] ?? false

        ,

        changeLayoutLanguage: (language: Language) => settings.set(s => s.layoutLanguage = language )

        ,

        resolveLanguage: () => 
            rotatingLanguage ??
            settings.get().layoutLanguage ??
            currentLanguage ??
            defaultLanguage

        ,

        resolveLanguageFor: (text: MultilangDto): Language => {

            const lang = self.resolveLanguage()

            return text?.[lang] ? lang : text?.[currentLanguage] ? currentLanguage : defaultLanguage
        }

        ,

        // returns a multilanguage obtained reducing all languages with a provided function.
        // liases with resolveLanguage() to short-circuit the standard language resolution strategy. 
        // and prioritise the language being iterated
        // switches the language internally using global state, though private.
        forEachLanguage: (apply: (lang: Language) => any) => {

            const result = allLanguages.reduce((acc, lang) => {

                rotatingLanguage = lang

                return { ...acc, [lang]: apply(lang) }

            }, {} as MultilangDto)

            rotatingLanguage = undefined!

            return result

        }
    
        ,

        ll: (text: MultilangDto | undefined) : string => text?.[self.resolveLanguageFor(text)]!
    
    // eslint-disable-next-line
    }),[currentSettings])

    return self

}