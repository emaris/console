
import { Button } from "#app/components/Button"
import { FormState } from "#app/form/hooks"
import { icns } from "#app/icons"
import { useT } from '#app/intl/api'
import { FoldablePanel } from "#app/scaffold/FoldablePanel"
import { useComponentData } from '#layout/components/api'
import { useLayout } from '#layout/context'
import { isInstance } from '#layout/model'
import { useLayoutSettings } from './api'
import "./styles.scss"


export const LayoutSettings = (props: { state: FormState }) => {

    const t = useT()
    const layout = useLayout()
    const { allData, resetData}  = useComponentData()
    
    const {collapseAll, expandAll} = useLayoutSettings()
    
    const { state } = props

    const data = allData()

    return <FoldablePanel height={125} title={t('layout.buttons.settings.title')} icon={icns.settings} className="layout-settings">
            <Button enabledOnReadOnly disabled={isInstance(layout)} iconLeft icn={icns.contract} size="small" light onClick={collapseAll} >
                {t('layout.buttons.settings.collapse_all')}
            </Button>
            <Button enabledOnReadOnly disabled={isInstance(layout)} iconLeft icn={icns.expandSquare} size="small" light onClick={expandAll} >
                {t('layout.buttons.settings.expand_all')}
            </Button>
            <Button enabledOnReadOnly enabled={isInstance(layout) && Object.keys(data ?? {}).length > 0} iconLeft icn={icns.revert} size="small" light onClick={() => resetData().then(_ => state.reset(data, false))} >
                {t('layout.buttons.settings.revert_preview')}
            </Button>

        </FoldablePanel>

}