import { Language } from '#app/intl/model';
import { FileDescriptor } from "#app/stream/model";
import { StyleSpec } from '#layout/style/model';
import { ProductDto } from '#product/model';
import { RequirementDto } from '#requirement/model';
import { State } from 'apprise-frontend-core/state/api';
import { createContext, useContext } from 'react';
import { LayoutApis, LiveLayoutApis } from './apicontext';
import { ComponentSpec } from './components/model';
import { Layout, LayoutInstance } from "./model";
import { Parameter, ParameterSpec } from './parameters/model';



export const LayoutSettingsContext = createContext<State<LayoutSettings>>(undefined!)


export const LayoutContext = createContext<LayoutState>(undefined!)

export const useLayout = () => useContext(LayoutContext).layout


export const LayoutRegistryContext = createContext<State<LayoutRegistryState>>(undefined!)

export const LayoutConfigContext = createContext<LayoutConfig>({})

export const useLayoutConfig = <T extends any=LayoutConfig> () => useContext(LayoutConfigContext) as LayoutConfig & T


// used at design time, by clients to make available the "host" asset of the current layout.
export const LayoutAssetContext = createContext<RequirementDto | ProductDto>(undefined!)


// holds a namespace for the state of input fields in live mode.
// used in loops (cf. Asset Sections), to separate field state across iteration steps.
export const RenderLoopContext =  createContext<string | undefined>(undefined)

export const LayoutApiContext = createContext<LayoutApis>(undefined!)
export const LiveLayoutApiContext = createContext<LiveLayoutApis>(undefined!)

export const ParameterOverlayContext = createContext<Parameter[]>(undefined!)

export const ParameterContext = createContext<Parameter[]>([])

export const initialSettings = {

    allCollapsed: false,
    allExpanded: false,

    fileDescriptors: {},
    collapsed: {}

} 

export type LayoutState = {

    layout: Layout
    change: (_: Layout | LayoutInstance) => void
}


export type LayoutAuxState = LayoutSettings & {

    layout: Layout
    change: (_: Layout | LayoutInstance) => void

    changeAuxState: React.Dispatch<React.SetStateAction<LayoutSettings>>

    for?: string
}


// this is the state of the designer itself, not available when the layout is 'live'.
export type LayoutSettings = {

    layoutLanguage: Language

    allCollapsed: boolean
    allExpanded: boolean

    fileDescriptors: { [componentId: string]: FileDescriptor[] }

    collapsed: { [componentId: string]: boolean }

}



export type LayoutRegistryState = {

    components: { [id: string]: ComponentSpec }
    parameters: { [id: string]: ParameterSpec }
    styles: { [id: string]: StyleSpec<any> }

}

export const initialLayoutRegistry: LayoutRegistryState = {

    components: {},
    parameters: {},
    styles: {}
}


export type LayoutMode = 'design' | 'live'

export type LayoutConfig = Record<string, any> & Partial<{

    lazyRender: boolean

    // The initial language use to render the layout if and until one is set in state.
    defaultLanguage: Language

    // the layout may render differently if it's mounted for design or to collect data.
    mode: LayoutMode

    // whether the layout imports data from other layouts.
    importsData : boolean

    // whether the layout exports the data it collects in live mode.
    exportsData : boolean

    // a default used by interactive components to force a readonly status in a non-readonly context.
    fieldsRestricted: boolean

    // whethe the current user can temporarily unlock readonly fields.
    canUnlock: boolean

}>

export const defaultLayoutConfig: LayoutConfig = {

    mode: 'live',
    importsData:false,
    exportsData: false,
    fieldsRestricted: false
}
