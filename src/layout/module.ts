import { Module } from "#app/module/model"
import { layoutIcon, layoutType } from "./constants"



export const useLayoutModule = (): Module => {


      return {


            icon: layoutIcon,
            type: layoutType,

            nameSingular: "Layout",
            namePlural: "Layout",

            contextNameSingular: "system.categories.module.name_singular",
            contextNamePlural: "system.categories.module.name_plural"

      }
}