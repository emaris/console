import { RadioBox } from "#app/form/RadioBox"
import { SliderBox } from "#app/form/SliderBox"
import { useT } from '#app/intl/api'
import { useLayoutParameters } from "#layout/parameters/api"
import { Radio } from "antd"
import { ColorStyle } from "./bolor"
import { StyleRenderProps, StyleSpec } from "./model"



export type BorderProps = {

    top: number
    bottom: number
    left: number
    right: number
    all: number

    color: string
    type: string

}

export const borderStyleId = 'border'

export const useBorderStyle = (): StyleSpec<BorderProps> => {

    const t = useT()
           
    const { r } = useLayoutParameters()

    return {

        id: borderStyleId,
        name: 'layout.styles.border.name',
        type: "inner",


        cssOf: (props) => {

 
            const { top = 0, bottom = 0, left = 0, right = 0, color, type = 'solid' } = props

            const value = (n: number): string | undefined => (n && color) ? `${n}px ${type} ${r(color || 'inherit')}` : undefined

            const borderTop = value(top)
            const borderBottom = value(bottom)
            const borderLeft = value(left)
            const borderRight = value(right)

            let borderStyle = {}

            borderStyle = borderTop ? { ...borderStyle, borderTop } : borderStyle
            borderStyle = borderBottom ? { ...borderStyle, borderBottom } : borderStyle
            borderStyle = borderLeft ? { ...borderStyle, borderLeft } : borderStyle
            borderStyle = borderRight ? { ...borderStyle, borderRight } : borderStyle

            return borderStyle


        }

        ,

        Render: (props: StyleRenderProps<BorderProps>) => {

            const { valueprops, onChange, id } = props

            const { top = 0, bottom = 0, left = 0, right = 0, all = 0, type = 'solid', color } = valueprops

            const values = { top, bottom, left, right }

            const onChangeColor = color => {
                onChange({
                    color: color,
                    top: valueprops.top ?? 1,
                    bottom: valueprops.bottom ?? 1,
                    left: valueprops.left ?? 1,
                    right: valueprops.right ?? 1,
                    all: valueprops.all ?? 1,
                    type: valueprops.type ?? 'solid'
                })
            }

            return <>

                <ColorStyle label={t("common.fields.color.name")} id={`${id}-border-colorbox`}
                    validation={{ msg: t("common.fields.color.msg") }}
                    onChange={onChangeColor} >
                    {color}
                </ColorStyle>

                <SliderBox key={`${color}-all`} value={all}
                    marks={{ [all]: all ? all : '' }}
                    label={t(`layout.styles.border.all.name`)}
                    showValues={false}
                    min={0}
                    max={10}
                    onChange={s =>  onChange({ ...valueprops, top: s as number, bottom: s as number, left: s as number, right: s as number, all: s as number })} 
                />


                {Object.keys(values).map((v, i) =>

                    <SliderBox key={`${color}-${all}-${i}`} value={values[v]}
                        defaultValue={values[v]}
                        marks={{ [values[v]]: values[v] }}
                        label={t(`layout.styles.border.${v}.name`)}
                        showValues={false}
                        min={0}
                        max={10}
                        onChange={s => onChange({ ...valueprops, [v]: s as number })} />)

                }

                <RadioBox label={t("layout.styles.line.style.name")}
                    validation={{ msg: t("layout.styles.line.style.msg") }}
                    onChange={type => onChange({ ...valueprops, type })}
                    value={type}>
                    <Radio value='solid'>{t('layout.styles.line.style.solid')}</Radio>
                    <Radio value='dashed'>{t('layout.styles.line.style.dashed')}</Radio>
                    <Radio value='dotted'>{t('layout.styles.line.style.dotted')}</Radio>
                </RadioBox>
            </>

        }

    }
}

