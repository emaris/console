import { useT } from '#app/intl/api'
import { useLayoutParameters } from "#layout/parameters/api"
import * as React from "react"
import { ColorStyle } from "./bolor"
import { StyleRenderProps, StyleSpec } from "./model"



export type BackgroundProps = {

    color: string

}

export const backgroundStyleId = 'background'

export const useBackgroundStyle = (): StyleSpec<BackgroundProps> => {

    const t = useT()

    const { r } = useLayoutParameters()

    return {

        id: backgroundStyleId,
        name: 'layout.styles.background.name',
        type: "inner",

        cssOf: (props): React.CSSProperties => {

           
            return { background: r(props.color ?? "inherit") }


        }

        ,

        Render: (props: StyleRenderProps<BackgroundProps>) => {

            const { id, valueprops, onChange } = props
            const { color } = valueprops

            return <>

                <ColorStyle label={t("common.fields.color.name")} id={`${id}-font-colorbox`} validation={{ msg: t("common.fields.color.msg") }} onChange={color => onChange({ color })} >{color}</ColorStyle>

            </>
        }

    }

}
