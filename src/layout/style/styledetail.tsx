
import { DrawerAnchor } from "#app/components/Drawer"
import { useT } from '#app/intl/api'
import { TocEntry, TocLayout } from "#app/scaffold/TocLayout"
import { useLayoutApis } from '#layout/apicontext'
import { DetailProps } from "#layout/components/detail"
import { Component } from "#layout/components/model"
import { Icon } from "antd"
import { MdInvertColors } from "react-icons/md"
import "./styles.scss"


export const styleIcon = <Icon component={MdInvertColors} />


type StyledProps = {

    style: { [key: string]: any }
    extraStyle?: { [key: string]: any, name: string }
}


type ComponentType = 'style' | 'extrastyle'


export const isStyled = (c: Component): c is Component & StyledProps => !!(c as any as StyledProps).style
export const isExtraStyled = (c: Component): c is Component & StyledProps => !!(c as any as StyledProps).extraStyle

const offset = 158

export const StyleDetail = (props: DetailProps<Component & StyledProps> & { type?: ComponentType }) => {


    const t = useT()

    const { registry, components } = useLayoutApis()

    const { component, onChange, type = 'style' } = props

    // const {style} = component
    const style = type === 'style' ? component.style : component.extraStyle!


    const cspec = components.componentSpecOf(component)

    const overlay = type === 'style' ? cspec.styles(style, component) : cspec.extraStyles(style, component)

    const specs = type === 'style' ?

        Object.keys(overlay).map(registry.lookupStyle) :
        Object.keys(overlay).filter(key => key !== 'name').map(registry.lookupStyle)//.sort((s1,s2)=> t(s1.name).localeCompare(t(s2.name)))

    const onChangeStyle = (v, id) => {
        if (type === 'style')
            onChange({ ...component, style: { ...component.style, [id]: v } })
        else
            onChange({ ...component, extraStyle: { ...component.extraStyle!, [id]: v } })
    }

    return <TocLayout className="styles-tab" offsetTop={offset + 10}>

        {specs.map(({ name, id }, i) => <TocEntry key={i} id={id} title={name} />)}

        <div className="styles-sections">{

            specs.map(({ id, name, Render }, i) =>

                <DrawerAnchor id={id} offset={offset} key={i} className={`style-section section-${id}`}>

                    <h1 className="section-title">{t(name, { id })}</h1>

                    <Render id={id} key={i} valueprops={{ ...overlay[id], ...style[id] }} allStyles={style} onChange={v => onChangeStyle(v, id)} />

                </DrawerAnchor>


            )}
        </div>

    </TocLayout>

}


