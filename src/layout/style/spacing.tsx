import { SliderBox } from "#app/form/SliderBox"
import * as React from "react"
import { useT } from '#app/intl/api'
import { StyleSpec } from "./model"



export type SpacingProps = {

    top: number
    bottom: number
    left: number
    right: number

}

const verticalIncrement = 2


export const spacingStyleId = 'spacing'

export const useSpacingStyle = (): StyleSpec<SpacingProps> => {


    const t = useT()

    return {

        id: spacingStyleId,
        name: 'layout.styles.spacing.name',
        type: "outer",

        cssOf: ({ top, bottom, left, right }) => ({

            paddingTop: (top ?? 0) * verticalIncrement,
            paddingBottom: (bottom ?? 0) * verticalIncrement,

            paddingLeft: `${left ?? 0}%`,
            paddingRight: `${right ?? 0}%`

        })

        ,

        Render: props => {


            const { valueprops, onChange } = props

            const { top = 0, bottom = 0, left = 0, right = 0 } = valueprops

            return <>
                <SliderBox value={top} showValues={false} marks={{ [top]: `${top}%` }} label={t("layout.styles.spacing.top.msg")} onChange={s => onChange({ ...valueprops, top: s as number })} />
                <SliderBox value={bottom} showValues={false} marks={{ [bottom]: `${bottom}%` }} label={t("layout.styles.spacing.bottom.msg")} onChange={s => onChange({ ...valueprops, bottom: s as number })} />
                <SliderBox value={left} showValues={false} marks={{ [left]: `${left}%` }} label={t("layout.styles.spacing.left.msg")} onChange={s => onChange({ ...valueprops, left: s as number })} />
                <SliderBox value={right} showValues={false} marks={{ [right]: `${right}%` }} label={t("layout.styles.spacing.right.msg")} onChange={s => onChange({ ...valueprops, right: s as number })} />
            </>
        }

    }
}

export const absoluteSpacingStyleId = 'absolutespacing'

export const useAbsoluteSpacingStyle = (): StyleSpec<SpacingProps> => {

    const t = useT()

    return {

        id: absoluteSpacingStyleId,
        name: 'layout.styles.spacing.name',
        type: "outer",


        cssOf: ({ top, bottom, left, right }) => ({

            paddingTop: (top ?? 0) * verticalIncrement,
            paddingBottom: (bottom ?? 0) * verticalIncrement,

            paddingLeft: left ?? 0,
            paddingRight: right ?? 0

        })

        ,


        Render: props => {

            const { valueprops, onChange } = props

            const { top = 0, bottom = 0, left = 0, right = 0 } = valueprops

            return <>
                <SliderBox value={top} showValues={false} marks={{ [top]: `${top}px` }} label={t("layout.styles.spacing.top.msg")} onChange={s => onChange({ ...valueprops, top: s as number })} />
                <SliderBox value={bottom} showValues={false} marks={{ [bottom]: `${bottom}px` }} label={t("layout.styles.spacing.bottom.msg")} onChange={s => onChange({ ...valueprops, bottom: s as number })} />
                <SliderBox value={left} showValues={false} marks={{ [left]: `${left}px` }} label={t("layout.styles.spacing.left.msg")} onChange={s => onChange({ ...valueprops, left: s as number })} />
                <SliderBox value={right} showValues={false} marks={{ [right]: `${right}px` }} label={t("layout.styles.spacing.right.msg")} onChange={s => onChange({ ...valueprops, right: s as number })} />
            </>
        }

    }
}

