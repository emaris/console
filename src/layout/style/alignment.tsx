import { RadioBox } from "#app/form/RadioBox"
import { useT } from '#app/intl/api'
import { Radio } from "antd"
import { WidthProps, widthStyleId } from "./width"
import { StyleRenderProps, StyleSpec } from "./model"

export type Alignment = 'left' | 'center' | 'right'

export type AlignmentProps = {

    value: Alignment

}


const style2css = { left: "flex-start", center: "center", right: "flex-end" }

export const alignmentStyleId = 'alignment'

export const useAlignmentStyle = (): StyleSpec<AlignmentProps> => {

    return {

        id: alignmentStyleId,
        name: 'layout.styles.alignment.name',
        type: "outer",

        cssOf: props => ({ justifyContent: style2css[props.value ?? 'left'], flexDirection: 'row' })
        ,

        Render: (renderprops: StyleRenderProps<AlignmentProps>) => {

            const t = useT()
            const { valueprops, onChange, allStyles } = renderprops
            const { value } = valueprops

            const validation = { msg: t('layout.styles.alignment.msg') }

            const currentWidth = (allStyles[widthStyleId] as WidthProps)?.percentage

            return <RadioBox validation={validation} disabled={currentWidth === undefined || currentWidth === 100}
                value={value ?? 'left'} onChange={value => onChange({ ...valueprops, value })}>
                <Radio value="left" >{t('layout.styles.alignment.buttons.left')}</Radio>
                <Radio value="center" >{t('layout.styles.alignment.buttons.center')}</Radio>
                <Radio value="right" >{t('layout.styles.alignment.buttons.right')}</Radio>
            </RadioBox>

        }

    }
}


