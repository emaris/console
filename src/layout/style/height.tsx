import { SliderBox } from "#app/form/SliderBox"
import { useT } from '#app/intl/api'
import { StyleRenderProps, StyleSpec } from "./model"



export type HeightProps = {

    value: number

}


export const heightStyleId = 'height'

export const useHeightStyle = (): StyleSpec<HeightProps> => {

    const t = useT()

    return {

        id: heightStyleId,
        name: 'layout.styles.height.name',
        type: "inner",


        cssOf: props => {

            const { value = 0 } = props

            const padding = value / 2

            return { paddingTop: padding, paddingBottom: padding }


        }

        ,

        Render: (props: StyleRenderProps<HeightProps>) => {

            const { valueprops, onChange } = props

            const { value = 0 } = valueprops

            const label = t("layout.styles.height.value")
            const validation = { msg: t("layout.styles.height.msg") }


            return <SliderBox label={label} validation={validation}
                marks={{ [value]: `${value}` }}
                showValues={false}
                value={value} onChange={s => onChange({ value: s as number })} />

        }

    }

}
