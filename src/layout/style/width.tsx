import { RadioBox } from '#app/form/RadioBox'
import { SliderBox } from "#app/form/SliderBox"
import { useT } from '#app/intl/api'
import { Radio } from 'antd'
import * as React from "react"
import { StyleRenderProps, StyleSpec } from "./model"
import { valuemarks } from './size'


export type WidthProps = Partial<{

    percentage: number
    default: number
    size: number
    min: number, max: number

}>


export const defaultPercentage = 100;

export const widthStyleId = 'width'

export const useWidthStyle = (): StyleSpec<WidthProps> => {

    return {

        id: widthStyleId,
        name: 'layout.styles.width.name',
        type: "inner",


        cssOf: props => {

            const { percentage, size, min = 1, max = 100 } = props

            // (max-min) : 100 = X-min : size

            // absolute if we have a default and have no percentage yet.
            // we use a specific size if we have it, or fallback to the default.

            const width = props.default !== undefined && percentage === undefined ?

                // interpolates wrt to default
                size ? Math.round( (max - min)  * size / 100) + min : props.default

                :

                `${props.percentage ?? defaultPercentage}%`


            return { width }

        }
        ,

        Render

    }
}


const Render = (props: StyleRenderProps<WidthProps>) => {

    const t = useT()

    const { valueprops, onChange } = props

    const [absolute, setAbsolute] = React.useState(!!valueprops.default && !valueprops.percentage)

    const { percentage = defaultPercentage, default: defaultSize = 50, size, min = 1, max = 100 } = valueprops

    // (max-min) : 100 = X-min : size

    const calcDefault = Math.round(((defaultSize-min) * 100) / (max - min))

    const sizeOrDefault = size ?? calcDefault

    const label = t("layout.styles.width.value")
    const validation = { msg: t("layout.styles.width.msg") }

    const changeScale = (absolute: boolean) => {

        if (absolute)
            onChange({ ...valueprops, size: valuemarks[valueprops.default!], percentage: undefined })
        else
            onChange({ ...valueprops, percentage: defaultPercentage, size: undefined })

        setAbsolute(absolute)
    }

    return <>

        {valueprops.default === undefined ||

            <RadioBox label={'Scale'} validation={{ msg: "Pick an option." }} value={absolute} onChange={changeScale}>
                <Radio key={1} value={false}>{'Relative'}</Radio>
                <Radio key={2} value={true}>{'Absolute'}</Radio>
            </RadioBox>

        }


        {absolute ?

            <SliderBox key='absolute' label={t("layout.styles.size.name")} validation={{ msg: t("layout.styles.size.msg") }}
                marks={{ [sizeOrDefault]: `${sizeOrDefault}` }}
                showValues={false}
                value={sizeOrDefault} onChange={s => onChange({ ...valueprops, size: s as number})} />

            :

            <SliderBox key='relative' label={label} validation={validation}
                marks={{ [percentage]: `${percentage}%` }}
                showValues={false}
                value={percentage} onChange={s => onChange({ ...valueprops, percentage: s as number })} />


        }
    </>
}




export const inputWidthStyleId = 'inputwidth'

export const useInputWidthStyle = (): StyleSpec<WidthProps> => {

    return {

        ...useWidthStyle()
        ,

        id: inputWidthStyleId,
        name: 'layout.styles.width.name_input',
        type: "custom"

    }
}
