import { Field, FieldProps } from "#app/form/Field"
import { useReadonly } from "#app/form/ReadonlyBox"
import { TextBox } from "#app/form/TextBox"
import * as React from "react"
import { SwatchesPicker } from 'react-color'
import { useLayoutParameters } from '#layout/parameters/api'

type Props = FieldProps & {

    id:string
    onChange: (value:any) => void
    children:string
}

export const ColorStyle = (props:Props) => {

    const { id,children, className, ReadOnlyBox, readonly, onChange, ...rest} = useReadonly(props)

    const {r} = useLayoutParameters()

    const disabled = props.disabled || readonly

    const [pickerVisible, showPicker] = React.useState(false)

    const [reset,setReset] = React.useState(false)

    React.useEffect(() => { if (reset) setReset(false)},[reset])

    const resolvedColor = children ? r(children) : undefined

    return <Field {...rest} className={`colorbox colorbox-style ${className || ''}`} disabled={disabled} >

        <div style={{display:"flex", alignItems:"center"}}>

            <div className="colorbox-value" onClick={ ()=> showPicker(true) }>
            <div className="colorbox-value-inner" style={{background:resolvedColor}} />
            </div>

            { <div className="text-companion">
                
                {/* <ParametricMentionBox reset={reset} light id={id} onChange={innerOnChange}>{children}</ParametricMentionBox> 
                    It used to be a parametric text box but then it was adding html tags when manually changing the hex color value
                */}
                <TextBox className='colorbox-hex-value' onChange={onChange}>{children}</TextBox>
                            
            </div>}

        </div>

        { pickerVisible &&  

            <div className="colorbox-popover">
                <div className="colorbox-overlay" onClick={ ()=>showPicker(false)} />
                <ReadOnlyBox>
                    <SwatchesPicker color={resolvedColor} disabled={disabled} onChange={c=>{
                        if (!readonly) Promise.resolve(onChange(c.hex)).then(_=>setReset(true))
                    }} />
                </ReadOnlyBox>
            </div> 
        }

    </Field>

}