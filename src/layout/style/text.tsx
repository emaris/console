import * as React from "react"
import { useT } from '#app/intl/api'
import { useLayoutParameters } from "#layout/parameters/api"
import { ColorStyle } from "./bolor"
import { StyleRenderProps, StyleSpec } from "./model"
import { Size, SizeStyle } from "./size"
import { Alignment } from "./alignment"
import { Field } from "#app/form/Field"
import { Radio } from "antd"
import { ReadOnlyContext } from "#app/scaffold/ReadOnly"
import { stripHtmlTags } from "#app/utils/common"
import { predefinedFontSizeBaseLineParameterId } from "#layout/parameters/constants"


export type TextAlignment = Alignment | "justify"

export const defaultBaseline = 14

export type TextProps = {

    color: string
    size: Size
    alignment: TextAlignment
    sizeBaseline?: Size | number

}

const isNumber = (n: Size | number) => !isNaN(Number(n))

export const sizemap: { [key in Size]: number } = {

    'xxs': defaultBaseline - 6,
    'xs': defaultBaseline - 4,
    'sm': defaultBaseline - 2,
    "m": defaultBaseline,
    "l": defaultBaseline + 2,
    "xl": defaultBaseline + 4,
    "xxl": defaultBaseline + 8

}

export const textStyleId = 'text'

export const useTextStyle = (): StyleSpec<TextProps> => {

    const t = useT()

    const readonly = React.useContext(ReadOnlyContext)

    const parameters = useLayoutParameters()

    return {

        id: textStyleId,
        name: 'layout.styles.text.name',
        type: "custom",

        cssOf: (props) => {

            

            const propBaseline = props.sizeBaseline ? isNumber(props.sizeBaseline) ? props.sizeBaseline : sizemap[props.sizeBaseline] : undefined

            const propAwareBaseline = propBaseline ?? defaultBaseline

            const rawParameterBaseline = parameters.lookupParameter(predefinedFontSizeBaseLineParameterId)?.value
            const parameterBaseline = Number(stripHtmlTags(`${rawParameterBaseline}`))
            const computedBaseline = !isNaN(parameterBaseline) ? (propAwareBaseline * parameterBaseline) / defaultBaseline : propAwareBaseline

            const baseSize = sizemap[props.size ?? 'm']

            const size = (baseSize * computedBaseline) / defaultBaseline

            return {

                color: parameters.r(props.color),
                fontSize: `${size}px`,
                textAlign: props?.alignment ?? 'left'

            }
        }

        ,

        Render: (props: StyleRenderProps<TextProps>) => {

           
            const { id, valueprops, onChange } = props
            const { color, size } = valueprops

            const vertical = {
                display: 'block',
                height: '30px',
                lineHeight: '30px',
            }

            const alignmentValidation = { msg: t('layout.styles.alignment.msg') }

            return <>

                <ColorStyle label={t("common.fields.color.name")} id={`${id}-font-colorbox`} validation={{ msg: t("common.fields.color.msg") }} onChange={color => onChange({ ...valueprops, color })} >{color}</ColorStyle>
                <SizeStyle label={t("layout.styles.size.name")} validation={{ msg: t("layout.styles.size.msg") }} onChange={s => onChange({ ...valueprops, size: s })} >{size}</SizeStyle>


                <Field label={t('layout.styles.alignment.name')} validation={alignmentValidation}>
                    <Radio.Group disabled={readonly} value={valueprops.alignment || 'left'} onChange={({ target }) => onChange({ ...valueprops, alignment: target.value })}>
                        <Radio style={vertical} value="left" >
                            {t('layout.styles.alignment.buttons.left')}
                        </Radio>
                        <Radio style={vertical} value="center" >
                            {t('layout.styles.alignment.buttons.center')}
                        </Radio>
                        <Radio style={vertical} value="right" >
                            {t('layout.styles.alignment.buttons.right')}
                        </Radio>
                        <Radio style={vertical} value="justify" >
                            {t('layout.styles.alignment.buttons.justify')}
                        </Radio>
                    </Radio.Group>
                </Field>

            </>
        }

    }
}
