import { FieldProps } from "#app/form/Field"
import { SliderBox } from "#app/form/SliderBox"
import * as React from "react"
import { useT } from '#app/intl/api'


export type Size = "xxs" | "xs" | "sm" | "m" | "l" | "xl" | "xxl"

export const sizeprops: { [key in Size] : any} = {


        "xxs": { name : "layout.styles.size.xxsmall.name"},
        "xs": { name : "layout.styles.size.xsmall.name"},
        "sm": { name : "layout.styles.size.small.name"},
        "m": { name : "layout.styles.size.medium.name"},
        "l": { name : "layout.styles.size.large.name"},
        "xl": {  name : "layout.styles.size.xlarge.name"},
        "xxl": {  name : "layout.styles.size.xxlarge.name"}

}

export const sizemarks: { [key in number] : Size} = {

    0: "xxs",
    17: "xs",
    35: "sm",
    50: "m",
    67: "l",
    85: "xl",
    100: "xxl"
}

// entry-based methods work with strings in JS and aren't handled with generics in TS, so we need to re-assert the type
export const valuemarks = Object.fromEntries(Object.entries(sizemarks).map(([key, value]) => [value, key])) as unknown as  { string : number} 



type Props = FieldProps & {

    onChange: (value:Size) => void
    children: Size | undefined
}

export const SizeStyle = (props:Props) => {

    const t = useT()

    const {children, className, onChange, ...rest} = props

    const value = Object.keys(sizemarks).find(n=>sizemarks[n]===children) 
    const  numvalue = value ? parseInt(value) : undefined

    return <SliderBox {...rest}  
                        value={numvalue} onChange={n=>onChange(sizemarks[n as number])} 
                        marks={sizemarks} defaultValue={50} 
                        step={null} 
                        showValues={false}
                        tipFormatter={v=>t(sizeprops[sizemarks[v]].name)}  />
}