import { SliderBox } from "#app/form/SliderBox"
import { useT } from '#app/intl/api'
import { StyleRenderProps, StyleSpec } from "./model"


export type Step = 'smaller' | 'small' | 'normal' | 'large' | 'larger'


export type DiscreteWidthProps = {

    percentage: number
    factor?: number
    labels? : {
        label?: string
        validation?: string
    }

}

export const defaultPercentage = 56;

export const discreteWidthStyleId = 'discretewidth'

export const useDiscreteWidthStyle = () : StyleSpec<DiscreteWidthProps> => {

    const t = useT()
       

    return  {

    id:discreteWidthStyleId,
    name: 'layout.styles.width.name',
    type: "inner",


    // cssOf: props =>({width: `${props.factor ? props.percentage*props.factor : props.percentage}%`, maxWidth: `${props.factor ? props.percentage*props.factor : props.percentage}%`})
    cssOf: props =>({width: `${props.percentage}%`, maxWidth: `${props.percentage}%`})

    ,

    Render : (props:StyleRenderProps<DiscreteWidthProps>) => {

        const {valueprops,onChange} = props

        const {percentage=defaultPercentage, labels} = valueprops

        const label = labels?.label ?? t("layout.styles.discrete_width.value")
        const validation = {msg: labels?.validation ?? t("layout.styles.discrete_width.msg") }

        const marks = {
            10: t("layout.styles.discrete_width.smaller"),
            33: t("layout.styles.discrete_width.small"),
            56: t("layout.styles.discrete_width.normal"),
            78: t("layout.styles.discrete_width.large"),
            100: t("layout.styles.discrete_width.larger")        
        }

        return  <SliderBox label={label} validation={validation} tooltipVisible={false}
                            marks={marks} 
                            showValues={false}
                            min={10}
                            max={100}
                            step={1}
                            value={percentage} 
                            onChange={s=>onChange({...valueprops, percentage:s as number})}
                />
                
    }

}}


