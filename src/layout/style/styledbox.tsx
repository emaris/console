import { useLayoutApis } from '#layout/apicontext'
import { Component } from '#layout/components/model'
import * as React from 'react'
import { StyleProps } from './model'

export type StyledBoxProps = {

    component: Component & StyleProps
    children?: any

    className?: string
    style?: React.CSSProperties

    filterStyle?: string[]

}



export const StyledBox = (props: StyledBoxProps) => {

    const { children, component, className, style = {} } = props

    const { styles } = useLayoutApis()

    const filteredStyleComponent = props.filterStyle ?
        { ...component, style: Object.keys(component.style).reduce((acc, cur) => (props.filterStyle?.includes(cur) ? { ...acc } : { ...acc, [cur]: component.style[cur] }), {}) }
        : component

    const outerStyle = styles.resolveStyle(filteredStyleComponent, spec => spec.type === "outer")
    const innerStyle = styles.resolveStyle(filteredStyleComponent, spec => spec.type === "inner")

    return <div className={className} style={{ ...style, ...outerStyle, display: 'flex' }}>
        <div style={innerStyle}>
            {children}
        </div>
    </div>


}