import { isColor } from "#app/utils/common"
import { useLayoutComponents } from '#layout/components/api'
import { Component } from "#layout/components/model"
import { useLayoutRegistry } from '#layout/registry'
import { CSSProperties } from 'react'
import { StyleProps, StyleSpec } from "./model"



export const useLayoutStyles = () => {

   const { componentSpecOf } = useLayoutComponents()

   const registry = useLayoutRegistry()


   const self = {

      resolveCustomStyles: (c: Component & StyleProps) => self.resolveStyle(c, s => s.type === 'custom')

      ,

      resolveStyle: (c: Component & StyleProps, filter: (_: StyleSpec) => boolean) => {

         const overlay = componentSpecOf(c).styles(c.style ?? {}, c)

         const props =  Object.keys(overlay).map(registry.lookupStyle)
            .filter(filter)
            .map(s => s.cssOf({ ...c.style[s.id], ...overlay[s.id]}))
            .reduce((acc, next) => {

               Object.entries(next).forEach(e => acc[e[0]] = e[1])

               return acc

            },  {} as CSSProperties)
            
            return props
      }

      ,

      resolvePdfCustomStyle: (c: Component & StyleProps) => self.resolvePdfStyle(c, s => s.type === 'custom')

      ,

      resolvePdfStyle: (c: Component & StyleProps, filter: (_: StyleSpec) => boolean) => {
       
         const resolvedHtmlStyles = self.resolveStyle(c, filter)
        
         const resolved = Object.keys(resolvedHtmlStyles).reduce((acc, next) => {
            const style = self.mapHtmlStyleToPdfStyle(next, resolvedHtmlStyles[next])
            return style ? { ...acc, ...style } : acc
         }, {}) as any
         
         return resolved
      }

      ,

      mapHtmlStyleToPdfStyle: (key: string, value: string) => {

         if (key === 'fontSize') {
            if (value.indexOf('px') > -1) {
               const oldVal = Number(value.substring(0, value.indexOf('px')))
               return { fontSize: `${oldVal - (oldVal * 0.3)}px` }
            }

         }
         // react-pdf's debug mode chokes on 0%.
         if (value === '0%') {
            return { [key]: 0 }

         }
         if (key === 'background' && isColor(value)) return { backgroundColor: value }
         return { [key]: value }

      }

   }

   return self
}