import { RadioBox } from '#app/form/RadioBox'
import { SliderBox } from "#app/form/SliderBox"
import { useT } from '#app/intl/api'
import { Radio } from 'antd'
import * as React from "react"
import { ColorStyle } from "./bolor"
import { StyleRenderProps, StyleSpec } from "./model"

export type LineType = 'solid' | 'dashed' | 'dotted'


export type LineProps = {

    color: string
    visible: boolean
    thickness: number
    type: LineType

}



const defaultThickeness = 5
const defaultType = 'solid'
const maxsize = 40

export const lineStyleId = 'line'

export const useLineStyle = (): StyleSpec<LineProps> => {

    const t = useT()

    return {

        id: lineStyleId,
        name: 'layout.styles.line.name',
        type: "inner",

        cssOf: (props): React.CSSProperties => {

            const thickness = props?.thickness ?? defaultThickeness
            const color = props?.color ?? ''
            const type = props?.type ?? defaultType

            const height = (maxsize / 100) * thickness

            return {
                borderTop: `${height}px ${type} ${color}`,
                height,
                minHeight: 1,
                visibility: props?.visible ? "visible" : "hidden"
            }
        }

        ,

        Render: (props: StyleRenderProps<LineProps>) => {

            const { id, valueprops, onChange } = props

            const thickness = valueprops.thickness ?? defaultThickeness
            const type = valueprops.type ?? defaultType

            return <>
                <SliderBox value={thickness} min={1} max={100}
                    defaultValue={thickness}
                    marks={{ [thickness]: thickness }}
                    label={t("layout.styles.line.thickness.name")}
                    validation={{ msg: t("layout.styles.line.thickness.msg") }}
                    onChange={s => onChange({ ...valueprops, thickness: s as number })} />

                <ColorStyle label={t("common.fields.color.name")} id={`${id}-line-colorbox`}
                    validation={{ msg: t("common.fields.color.msg") }}
                    onChange={color => onChange({ ...valueprops, color })} >{valueprops.color}</ColorStyle>

                <RadioBox label={t("layout.styles.line.style.name")}
                    validation={{ msg: t("layout.styles.line.style.msg") }}
                    onChange={type => onChange({ ...valueprops, type })}
                    value={type}>
                    <Radio value='solid'>{t('layout.styles.line.style.solid')}</Radio>
                    <Radio value='dashed'>{t('layout.styles.line.style.dashed')}</Radio>
                    <Radio value='dotted'>{t('layout.styles.line.style.dotted')}</Radio>
                </RadioBox>
            </>
        }

    }

}
