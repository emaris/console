import * as React from "react"

export type StyleRenderProps<T=any> = {

    id:string
    onChange: (value:T) => void
    valueprops: T
    allStyles: StyleMap

}

export type StyleMap = { [id:string] : any}


export const styleWith = (...specs:StyleSpec[]) => ({style: specs.reduce( (acc,{id}) => ({...acc,[id]:{}}) ,{})} )

export type StyleProps = {

    style: StyleMap,
    extraStyle?: {name: string} & StyleMap
}

export type StyleSpec<T=any> = {


    id:string,
    name:string,

    type: "outer" | "inner" | "custom"

    cssOf: (props:T) => React.CSSProperties

    Render: (_:StyleRenderProps<T>) => JSX.Element
}
