import { Button } from '#app/components/Button'
import { Form } from '#app/form/Form'
import { FormState, useFormState } from '#app/form/hooks'
import { icns } from '#app/icons'
import { useT } from '#app/intl/api'
import { PageSider } from '#app/scaffold/Page'
import { BytestreamedContext } from '#app/stream/BytestreamedHelper'
import { paramsInQuery, updateQuery } from '#app/utils/routes'
import { usePdfLayout } from '#layout/pdf/hook'
import { LazyPdfViewer } from '#layout/pdf/lazyviewer'
import { LayoutSettings } from '#layout/settings/settings'
import * as React from 'react'
import { AutoResizer } from 'react-base-table'
import { useHistory, useLocation } from 'react-router-dom'
import { useLayoutComponents } from './components/api'
import { ComponentDroppable } from './components/dnd'
import { PDFProps } from './components/model'
import { pageId, usePageComponent } from './components/page'
import { useLayout } from './initialiser'
import { Layout, LayoutInstance, instanceFrom, isInstance } from './model'
import { useLayoutParameters } from './parameters/api'
import { ParameterList } from './parameters/list'
import { paramIcon } from './parameters/model'
import { ParameterPanel } from './parameters/panel'
import { LayoutProvider, LayoutProviderProps } from './provider'
import { useLayoutRegistry } from './registry'
import { RenderStrategy } from './renderstrategy'
import './styles.scss'
import { Toc } from './toc'




const previewMode = "preview"
const pdfPreviewMode = "preview-pdf"
const modeparam = "mode"

// mounts an input layout for design and preview.

export const LayoutDesigner = (props: LayoutProviderProps) => {

    const history = useHistory()

    const { onChange: clientOnChange = () => { } } = props

    //  manages preview
    const { pathname, search } = useLocation()

    // takes mode from route.
    const mode = paramsInQuery(search)[modeparam]

    // initialises preview instance based on mode.
    const initPreview = () => (mode === previewMode || mode === pdfPreviewMode) ? instanceFrom(props.layout) : undefined

    // holds a layout instance for preview.
    const [preview, setPreview] = React.useState<LayoutInstance | undefined>(initPreview)


    const [pdfPreview, setPdfPreview] = React.useState<boolean>(mode === pdfPreviewMode)

    const changePdfMode = (pdf: boolean) => {
        setPdfPreview(pdf)
        history.push(`${pathname}?${updateQuery(search).with(params => params[modeparam] = pdf ? pdfPreviewMode : previewMode)}`)
    }

    // in preview, swaps input layout and change function to work on preview instance.
    const layout = preview ?? props.layout

    const onChange = isInstance(layout) ?

        layout => {

            // updates preview instance.
            setPreview(layout)

            // components can still be configured in preview, so propagates changes to input layout. 
            clientOnChange(({ ...props.layout, components: layout.components }))

        }
        :
        clientOnChange


    // manages the state of the preview instance
    const formstate = useFormState(preview?.state.data)


    // changes mode on demand (eg. when clicking a button)
    const showPreview = (v: boolean) =>

        Promise.resolve(formstate.reset(preview, false))  // silently clear form state in any case
            .then(_ => setPreview(v ? instanceFrom(props.layout) : undefined)) // clear or set the preview
            .then(_ => history.push(`${pathname}?${updateQuery(search).with(params => params[modeparam] = v ? previewMode : null)}`)) // sync url for refreshes



    return <LayoutProvider {...props} onChange={onChange} layout={layout}>
        <AutoResizer>{ ({height,width}) =>

            <div style={{height,width}} className={`layout-designer ${preview ? 'view-mode' : 'design-mode'}`}>

                <DesignerToc showPreview={showPreview} layout={layout} pdf={pdfPreview} setPdf={changePdfMode} />

                {pdfPreview ? <PDFPreview layout={layout} /> : <Pages state={formstate} />}

                <div className="layout-sidebar">
                    <LayoutSettings state={formstate} />
                    {preview ? <ParameterPreviewPanel /> : <ButtonBar />}
                </div>

            </div>
        }</AutoResizer>
    </LayoutProvider>

}

const PDFPreview = (props: { layout: Layout }) => {

    const { document } = usePdfLayout({ layout: instanceFrom(props.layout), preview: true })

    const previewDocument = (props: PDFProps) => <BytestreamedContext  >
        {document?.(props)}
    </BytestreamedContext>

    return <div style={{ width: '80%' }}>
        <LazyPdfViewer style={{ width: '100%' }}>

            {previewDocument}

        </LazyPdfViewer>
    </div>
}




const ParameterPreviewPanel = () => {

    const t = useT()

    const { allParameters, changeParametersWith } = useLayoutParameters()

    const parameters = allParameters()

    const change = (index: number, value: any) => changeParametersWith(ps => { ps[index] = { ...ps[index], value } })

    return <>

        <div className="sidebar-title preview-parameter-title">
            <div>{paramIcon}</div>
            <div style={{ marginLeft: 5, flexGrow: 1 }}>{t('layout.buttons.preview_title')}</div>
        </div>

        <ParameterPanel parameters={parameters} onChange={change} />
    </>
}

const Pages = (props: { state: FormState }) => {

    const layout = useLayout()
    const { allPages, pathAt } = useLayoutComponents()

    const { state } = props

    const { Preview } = usePageComponent()

    return <RenderStrategy>
        <div className={`layout-pages ${isInstance(layout) ? 'preview-mode' : ''}`}>
            <Form state={state} >

                {allPages().map((p, position) => <Preview id={p.id} key={position} component={p} path={pathAt(position)} />)}

                {isInstance(layout) || <Placeholder />}

            </Form>
        </div>
    </RenderStrategy>

}



const ButtonBar = () => {


    const layoutRegistry = useLayoutRegistry()

    return <div className="layout-buttonbar">

        {layoutRegistry.allComponents().filter(d => d.id !== pageId).filter(d => d.active).map(({ Button }, i) => <Button key={i} />)}

    </div>

}


type TocProp = {

    showPreview: (_: boolean) => void
    layout: Layout
    pdf: boolean
    setPdf: (_: boolean) => void
}

const DesignerToc = (props: TocProp) => {

    const t = useT()

    const history = useHistory()
    const { showPreview, pdf, setPdf } = props

    const layout = useLayout()
    const { parametersRoute } = useLayoutParameters()

    // const layoutInstance = instanceFrom(props.layout)
    // const {document, downloadPdf} = usePdfLayout({layout: layoutInstance, preview: true})

    // const downloadBtn = <Button iconLeft icn={icns.download} key="download" size="small" light onClick={()=>downloadPdf({fileName:`designer_preview_${Date.now()}.pdf`, document})}>{t("layout.buttons.preview_pdf")}</Button>

    const previewTypeButton = !pdf ? <Button enabledOnReadOnly iconLeft icn={icns.pdf} key="pdfpreview" size={"small"} light onClick={() => setPdf(!pdf)}>{t("layout.buttons.preview_pdf")}</Button>
        : <Button enabledOnReadOnly iconLeft icn={icns.html} key="htmlpreview" size={"small"} light onClick={() => setPdf(!pdf)}>{t("layout.buttons.preview_html")}</Button>

    const exitPreview = () => {
        showPreview(!isInstance(layout))
        setPdf(false)
    }

    return <PageSider>

        <div className="navbar-links">
            <Button enabledOnReadOnly disabled={isInstance(layout)} iconLeft icn={paramIcon} size="small" light onClick={() => history.push(parametersRoute())} >
                {t("common.buttons.edit_one", { singular: t('layout.parameters.plural') })}
            </Button>
            {/* {
                isInstance() && downloadBtn
            } */}
            {
                isInstance(layout) && previewTypeButton
            }
            <Button enabledOnReadOnly iconLeft icn={icns.preview} size="small" light onClick={() => exitPreview()} >
                {isInstance(layout) ? t("layout.buttons.preview_exit") : t("layout.buttons.preview_enter")}
            </Button>
        </div>


        <Toc mode='design' />

        <ParameterList />

    </PageSider>
}


export const placeholderPosition = -1

export const Placeholder = () => {

    const t = useT()
    const { pathAt } = useLayoutComponents()

    return <div className="page-preview page-card" id={'placeholder'} >

        <ComponentDroppable className="fake-component-placeholder" path={pathAt(placeholderPosition)}>
            <div className="fake-component-placeholder">
                <span className="placeholder-text">{t('layout.placeholder.msg')}</span>
            </div>

        </ComponentDroppable>

    </div>

}