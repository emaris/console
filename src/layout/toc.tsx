
import { Text } from "#app/components/Typography"
import { useT } from '#app/intl/api'
import { useLocale } from "#app/model/hooks"
import { Anchor } from "antd"
import * as React from "react"
import { useLayoutComponents } from './components/api'
import "./styles.scss"
import { stripHtmlTags } from "#app/utils/common"




type Props = React.PropsWithChildren<{

    mode: 'design' | 'live'
    noTitle?:boolean

}>

export const Toc = (props:Props) => {

    const {l} = useLocale()
    const t = useT()

    const {allPages} = useLayoutComponents()

    const {mode,noTitle} = props

    const pages = allPages()

    return  <div className="layout-toc">
                        { pages.length>1 && 
                        
                            <div className="toc-contents">
                                {noTitle || <Text smallcaps smaller className="toc-title">{t("layout.pages")}</Text>}
                                <Anchor  affix={false}>
                                    { pages.map((p,i) => <Anchor.Link key={i} href={`#${p.id}`} target="" title={l(p.name) ? stripHtmlTags(l(p.name)) : `${t('layout.components.page.name')} ${i+1}`} />)} 
                                    {mode==='live' || <Anchor.Link className="add-page" key={999} href={'#placeholder'} target="" title={t('common.buttons.new_one',{singular:t('layout.components.page.name')})} />   }                
                                </Anchor>
                            </div>
                        }
            </div>
}
