
import { useRef } from 'react'
import { LayoutConfig, LayoutConfigContext, useLayoutConfig } from './context'



type Props = React.PropsWithChildren<{

    config: LayoutConfig
}>

// mounts layout configuration, overlaying any that was previously mounted. 
export const LayoutConfigProvider = (props:Props) => {

    const {children,config} = props

    // is this an overlay?
    const parent = useLayoutConfig()


    // for the time being, we consider configuration as readonly after mount.
    // it is curently used to inject context-specific properties that woukld be difficult to lookup
    // in other contexts with the hook-driven approach we have.
    const merge = useRef({...parent, ...config})

    
    return <LayoutConfigContext.Provider value={merge.current}>
        {children}
    </LayoutConfigContext.Provider>
}


