

// puts up a blank screen first, to make sure we're not blocking tab transitions.

import { useT } from '#app/intl/api'
import { ScrollTop } from '#app/utils/scrolltotop'
import { Spin } from 'antd'
import { ScrollToHash } from 'apprise-frontend-core/utils/scrolltohash'
import { PropsWithChildren, useEffect, useState } from 'react'
import { AutoResizer } from 'react-base-table'
import { LayoutConfig, LayoutConfigContext, useLayoutConfig } from './context'

// we also start a spinner for the same reasons.  then we puts up a real UI immediately after, which may block.
export const RenderStrategy = (props: PropsWithChildren<{}>) => {

    const t = useT()

    const [rendered, setRendered] = useState(false)

    useEffect(() => {

        setTimeout(() => setRendered(true))

        // eslint-disable-next-line
    }, [])

    return <div id='layout-container' className='layout-container'>
        <Spin spinning={!rendered} delay={100} tip={t('layout.rendering')} size={'small'}>
                <AutoResizer>
                    {({ height,width }) => <div style={{ height, width }} >
                        {rendered ? <SinglePass {...props} /> : null}
                    </div>}
                </AutoResizer>
        </Spin >
        <ScrollTop containerId='layout-container' />
    </div>

}

//
// marks the final tree with the rendered layout
export const layoutFinalSelector = 'layout-final'

// in this strategy we don't have a know performance issue (eg. rows are disabled, we're in next-gen processors, we pushed the thresdold, etc.)
// but we still make allowances for slow devices or huge reports by allowing a first render to go through with no content, so that we cna 
// transition from another tab.

// eslint-disable-next-line
const SinglePass = (props: PropsWithChildren<{}>) => {

    const { children } = props

    return <div style={{ height: "100%", width: '100%' }}  className={layoutFinalSelector}>
        {children}
        <ScrollToHash adjustments={2} />
    </div>

}


// in this strategy we acknowledge we have still a performance problem and do a first render with no content to transition.
// a second with optimizations to replace the no content, and a final one in full to unlock functionality.

// eslint-disable-next-line
const TwoPass = (props: PropsWithChildren<{}>) => {

    const t = useT()

    const { children } = props

    const config = useLayoutConfig()

    const [configOverlay, setConfigOverlay] = useState<LayoutConfig>(config)

    useEffect(() => {

        if (configOverlay.lazyRender) {

            console.log('render lazy first')

            // render is complete, but may still not show on screen. we can't tell when that is.
            // if the second re-render starts before then, the browser may abort flush and show white screen.
            // so we find a compromise between starting too late and keep the spinner up for longer than we have to.
            // or starting sooner and risk a bad transition. this depends on how heavy is the rendering and the device.
            // we go with the wait that feels zippier. 
            setTimeout(() => {

                setConfigOverlay({ ...config, lazyRender: false })

            }, 200)

        }
        else
            console.log("render full")


        //eslint-disable-next-line
    }, [configOverlay])

    return <LayoutConfigContext.Provider value={configOverlay}>
        <Spin spinning={configOverlay.lazyRender} tip={t('layout.rendering')} size='small'>
            <div className={configOverlay.lazyRender ? '' : layoutFinalSelector}>
                {children}
                <ScrollToHash adjustments={2} />
            </div>
        </Spin>
    </LayoutConfigContext.Provider>


}