
import { useCurrentLanguage } from '#app/intl/api'
import { defaultLanguage } from '#app/intl/model'
import { StateProvider } from 'apprise-frontend-core/state/provider'
import { useMemo } from 'react'
import { LayoutApiProvider } from './apicontext'
import { LayoutScrollProvider } from './components/scroll'
import { LayoutConfigProvider } from './config'
import { LayoutConfig, LayoutContext, LayoutRegistryContext, LayoutSettingsContext, ParameterOverlayContext, defaultLayoutConfig, initialLayoutRegistry, initialSettings } from './context'
import { LayoutInitialiser, useLayout } from './initialiser'
import { Layout } from "./model"
import { Parameter, ParameterSpec } from './parameters/model'
import { useLayoutRegistry } from './registry'




export type LayoutProviderProps = {

    // client owns the layout and know how to change it. we change it and get it back changed.
    layout: Layout
    onChange?: (_: Layout) => void

    // this allows client to register components and parameters, which rely on layout apis.
    // so registration must occur below the provider, when all contexts are mounted.
    useInitialiser?: () => () => void

    initialiseParameters?: (_: [Parameter, ParameterSpec][]) => Parameter[]

    // some its standard, some can be custom. we put it in context and make it available below.
    config?: LayoutConfig

}


export const LayoutProvider = (props: React.PropsWithChildren<LayoutProviderProps>) => {

    const currentLang = useCurrentLanguage()

    const { layout, onChange=()=>{}, config: clientConfig = {}, initialiseParameters } = props

    const layoutConfig = { ...defaultLayoutConfig, ...clientConfig }

    const settings = { ...initialSettings, layoutLanguage: layoutConfig.defaultLanguage ?? currentLang ?? defaultLanguage }


    // client owns this state, somewhere and we're told how to change it.
    // so can't use a standard state provider to propagate it.
    const state = useMemo(() => ({

        layout,
        change: onChange

        // eslint-disable-next-line
    }), [layout])


    return <LayoutContext.Provider value={state}>
        <StateProvider initialState={initialLayoutRegistry} context={LayoutRegistryContext}>
            <StateProvider initialState={settings} context={LayoutSettingsContext}>
                <LayoutConfigProvider config={layoutConfig}>
                    <LayoutInitialiser {...props} >
                        <ParameterInitializer initialiseParameters={initialiseParameters}>
                            <LayoutApiProvider>
                                <LayoutScrollProvider>
                                        {props.children}
                                </LayoutScrollProvider>
                            </LayoutApiProvider>
                        </ParameterInitializer>
                    </LayoutInitialiser>
                </LayoutConfigProvider>
            </StateProvider>
        </StateProvider>
    </LayoutContext.Provider >
}

// runs client's logic to initialised parameters, then moounts them as an overlay.
// the parameter's api will use the overlay instead of the parameters in the layout.
export const ParameterInitializer = (props: React.PropsWithChildren<Pick<LayoutProviderProps, 'initialiseParameters'>>) => {

    const { initialiseParameters, children } = props

    const layout = useLayout()
    const registry = useLayoutRegistry()

    // pairs layout parameters with their specifications, so client doesn't need to look them up.
    // this is because the parameter specificatons have only just been registered by the layout initialiser.
    const params = layout.parameters.map(p => [p, registry.lookupParameter(p.spec)] as [Parameter, ParameterSpec])

    const value = initialiseParameters?.(params)!

    return <ParameterOverlayContext.Provider value={value!}>
        {children}
    </ParameterOverlayContext.Provider>

}