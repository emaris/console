import { MultilangDto } from '#app/model/multilang'
import { useContext } from 'react'

import { useLayoutApis } from '#layout/apicontext'
import { ParameterContext } from '#layout/context'
import { ResolveMode } from './api'




export const useParameters = () => {

    const contextual = useContext(ParameterContext)
    const { parameters: predefined } = useLayoutApis()

    return {

        ...predefined,

        contextualParameters:contextual,

        r: (text: string | undefined, resolvemode?: ResolveMode) => predefined.r(text, contextual, resolvemode),
        rr: (text: string | undefined, resolvemode?: ResolveMode) => predefined.rr(text, contextual, resolvemode),
        multir: (ml: MultilangDto | undefined, resolvemode?: ResolveMode) => predefined.multir(ml, contextual, resolvemode)

    }

}

