

import { useTenantParameter } from "#layout/parameters/tenant"
import { SubmissionContext } from "#campaign/submission/model"
import { LiveParameter } from "./model"


export const usePartyParameter = (): LiveParameter => {

    const tenantparam = useTenantParameter()

    return {


        ...tenantparam,


        valueFor: (ctx: SubmissionContext) => ctx.party.source

    }

}