import { HtmlSnippet } from "#app/components/HtmlSnippet"
import { MultiBox, MultiboxProps } from '#app/form/MultiBox'
import { RichBox, RichBoxProps } from "#app/form/RichBox"
import { useT } from '#app/intl/api'
import { stripHtmlTags } from "#app/utils/common"
import mexp from "math-expression-evaluator"
import * as React from "react"
import { useHistory } from "react-router-dom"
import { useLayoutParameters } from './api'
import { useParameters } from "./hooks"
import { Parameter } from './model'

type Props = {

    text: string
    className?: string
    style?: React.CSSProperties
}

export const magicAnchor = (url: string, html: string) => `<a onclick="window.push('${url}')">${html}</a>`


// computes and memoises param references for mentions
const useParameterReferences = (filter?: (_: Parameter) => boolean) => {

    const { allParameters, allParameterReferences, contextualParameters } = useParameters()

    const explicitParams = allParameters()

    //eslint-disable-next-line
    return React.useMemo(() => allParameterReferences(filter, contextualParameters), [explicitParams, contextualParameters])
}

export const ParametricText = (props: Props) => {

    const { text, className, style } = props

    //   horrible, but need to push onto history from resolved text mounted onto dom
    const { push } = useHistory()

    const parameters = useParameters()

    window["push"] = window["push"] || push

    const resolvedText = parameters.rr(text)

    return <HtmlSnippet className={`parametric-text ${className ?? ''}`} style={style} snippet={resolvedText} />

}



export const ParametricMultiTextBox = (props: MultiboxProps) => {

    const references = useParameterReferences()

    const { children, ...rest } = props

    return <MultiBox delay={400} rte {...rest} mentions={references}  >
        {children}
    </MultiBox>

}

export const ParametricTextBox = (props: RichBoxProps) => {

    const references = useParameterReferences()

    const { children, ...rest } = props

    return <RichBox {...rest} mentions={references}  >
        {children}
    </RichBox>

}


export const ParametricMentionBox = (props: Omit<RichBoxProps, "mentions"> & { reset?: boolean }) => {

    const references = useParameterReferences()

    const { children, ...rest } = props

    return <RichBox noToolbar {...rest} mentions={references}  >
        {children}
    </RichBox>

}




export const ParametricNumberBox = (props: RichBoxProps & { filter?: (p: Parameter) => boolean }) => {

    const t = useT()

    const { r } = useLayoutParameters()

    const { filter = (_) => true } = props

    // const references = useParameterReferences(p => p.spec === numspec.id)
    const references = useParameterReferences(filter)

    const { children, ...rest } = props

    // casts to string, in case it's a number.
    const stringchildren = children ? `${children}` : undefined

    // avoids spurious changes due just to type conversion.
    const onChange = (v: string) => v === stringchildren || props.onChange(v)

    const evalerror = "??"

    let eagereval;

    try {
        const resolved = stripHtmlTags(r(stringchildren))

        eagereval = children ? mexp.eval(resolved) : children

    }
    catch (e) {
        eagereval = evalerror
    }

    //TODO: add config to disable all formatting modules in quill.

    return <RichBox height='min' {...rest}  className="param-number" mentions={references} onChange={onChange}

        validation={{
            status: eagereval === evalerror ? 'warning' : 'success',
            msg: eagereval && eagereval !== parseInt(stringchildren!) ? t("layout.parameters.number.eval_msg", { result: eagereval }) : ''
        }}  >

        {stringchildren}

    </RichBox>

}