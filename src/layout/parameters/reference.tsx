import { useLayoutParameters } from '#layout/parameters/api';
import { TextParam, useTextParameter } from "#layout/parameters/text";
import { ParameterSpec } from "#layout/parameters/model";
import { MultilangDto } from "#app/model/multilang";
import { predefinedProductReferenceId } from './constants';

// based on text param for now.
export type ReferenceParam = TextParam

export const referenceParamId = 'reference'
// extends base spec with utlity methods for for predefined instances.
export const useReferenceParameter = (): ParameterSpec<MultilangDto, ReferenceParam> => {

    const textparam = useTextParameter()

    return {

        ...textparam,

        id: referenceParamId,
        name: 'layout.parameters.reference.name'

    }
}

export const useReferenceParam = () => {

    const params = useLayoutParameters()

    const self = {

        param: () => params.lookupParameterById(predefinedProductReferenceId),
        value: () => self.param()?.value

    }

    return self
}


