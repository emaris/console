
import { useCapProperties } from '#app/components/CapList'
import { useConfig } from '#app/config/state'
import { VSelectBox } from '#app/form/VSelectBox'
import { icns } from "#app/icons"
import { useT } from '#app/intl/api'
import { intlconfig } from "#app/intl/model"
import { useIntl } from '#app/intl/store'
import { useL } from '#app/model/multilang'
import { Tag, TagMap } from '#app/tag/model'
import { useTagStore } from '#app/tag/store'
import { useTime } from '#app/time/api'
import { randomIn, recwalk } from "#app/utils/common"
import { useEventInstanceStore } from '#campaign/event/store'
import { useCampaignModel } from '#campaign/model'
import { ProductInstance, useProductInstances, useProductInstanceStore } from "#campaign/product/api"
import { RequirementInstance, useRequirementInstances, useRequirementInstanceStore } from '#campaign/requirement/api'
import { SubmissionLayoutConfig } from "#campaign/submission/Detail"
import { Submission, SubmissionContext, SubmissionWithTrail, Trail } from '#campaign/submission/model'
import { useTrailSummary } from '#campaign/submission/statistics/trail'
import { useSubmissionLineage, useSubmissionStore } from '#campaign/submission/store'
import { Component } from '#layout/components/model'
import { useLayoutConfig } from '#layout/context'
import { useLayoutSettings } from "#layout/settings/api"
import { ProductLabel } from "#product/Label"
import { Product, ProductDto, useProductModel } from "#product/model"
import { useProductStore } from '#product/store'
import { RequirementLabel } from "#requirement/Label"
import { requirementType } from "#requirement/constants"
import { Requirement, RequirementDto, useRequirementModel } from '#requirement/model'
import { useRequirementStore } from '#requirement/store'
import { utils } from 'apprise-frontend-core/utils/common'
import moment from "moment-timezone"
import { ResolveMode, useLayoutParameters } from './api'
import { isLiveParameter, LiveParameter, newParameterFrom, Parameter, ParameterReference, ParameterSpec, partialParameterSpec } from "./model"


export type AssetParameterValue = {

    id: string
    liveData?: AssetLiveData
    type: string

}

const tagseparator = '·'

export const resolve_NA = 'layout.parameters.asset.resolve_na'
export const resolve_missing = 'layout.parameters.asset.resolve_missing'

export const sanitize = (name: string | undefined) => name?.replace(/[\s.,'/#!$%^&*;:{}=\-_`~()]/g, "")

export const missingOr = (submission: Submission | undefined, msg) => submission?.lifecycle.state === 'missing' ? "submission.status.missing" : msg



export type AssetLiveData = {

    // the asset.
    asset: Requirement | Product

    // the instance associated with this asset in this campaign.
    instance: RequirementInstance | ProductInstance

    // a related submission of the instance in this campaign, if any (inside reports, req sections, there may be no related submissions).
    submission?: Submission

    // the trail of the related submission.
    trail: Trail

}

export type AssetParameter = Parameter<AssetParameterValue> & {

    // in bridge mode, references and resolves any other non-live custom parameter.
    bridgeMode: boolean
    fixedValue: boolean
    components: Component[]
    data: Record<string, any>
}


// helpers
const flatten = (idprefix: string, nameprefix: string, data: any): ParameterReference[] =>

    typeof data === 'object' ?
        Object.keys(data).flatMap(k => flatten(`${idprefix}.${k}`, `${nameprefix}.${k}`, data[k]))
        :

        [{ id: idprefix, value: nameprefix }]    // basecase




export const assetParamId = 'asset'


const nameOfAsset = (asset: Requirement | Product) => asset.name.en!.replace(/\s/g, '-').toLowerCase()

export const useAssetParameter = (): LiveParameter<AssetParameterValue, AssetParameter> => {

    const t = useT()
    const l = useL()

    const intl = useIntl()
    const time = useTime()

    const tagstore = useTagStore()

    const reqstore = useRequirementStore()
    const prodstore = useProductStore()

    const reqmodel = useRequirementModel()
    const prodmodel = useProductModel()

    const prodinst = useProductInstances()
    const reqinst = useRequirementInstances()
    
    const prodinststore = useProductInstanceStore()
    const reqinststore = useRequirementInstanceStore()
    const eventinst = useEventInstanceStore()

    const substore = useSubmissionStore()
    const lineage = useSubmissionLineage()

    const appConfig = useConfig()

    const cmpmodel = useCampaignModel()

    const layoutConfig = useLayoutConfig()
    const settings = useLayoutSettings()
    const params = useLayoutParameters()

    const cap = useCapProperties()

    const summary = useTrailSummary()


    const { resolveLanguage } = settings

    const language = resolveLanguage()

    const fixedT = intl.getFixedT(language)

    const otherCustomParameters = (asset: Requirement | Product) => {

        return asset.properties.layout.parameters

            .filter(p => !isLiveParameter(params.parameterSpecOf(p)))  // excludes live params: specially handled
            .filter(p => params.parameterSpecOf(p).id !== assetParamId) // excludes other requirement parameters, would be cyclic.


    }

    const makeLineageFor = (props: { id?: string, type: string, name?: string, asset: string }): AssetParameter => {

        const { type, asset, id = 'any', name = 'any' } = props

        return {

            ...newParameterFrom(assetParamId),
            id,
            bridgeMode: false,
            fixedValue: true,
            name,
            components: [],
            data: undefined!,
            value: { id: asset, type }

        }
    }


    const self = {


        ...partialParameterSpec as any as ParameterSpec<AssetParameterValue, AssetParameter>,

        id: assetParamId,
        name: 'layout.parameters.asset.name',
        icon: icns.asset

        ,

        valueFor: (ctx: SubmissionContext, parameter: AssetParameter): AssetParameterValue => {


            const assetstore = parameter.value.type === requirementType ? reqstore : prodstore
            const inststore = (parameter.value.type === requirementType ? reqinststore : prodinststore).on(ctx.campaign)

            // the asset the parameter is about.
            const targetAsset = assetstore.safeLookup(parameter.value.id)

            // we search for target data in related submissions.
            const trailAndSubmission = ctx.relatedSubmissions.find(sub => sub.trail.key.asset === targetAsset.id)

                ?? {
                trail: { id: 'notrail', key: { campaign: ctx.campaign.id, asset: parameter.value.id, assetType: parameter.value.type, party: ctx.party.source } } as Trail,
                submission: { id: 'latest', trail: 'notrail', submission: [], content: { resources: {}, data: {} }, tags: [], lifecycle: { state: 'draft' } }

            }

            const value = {

                id: parameter.value?.id,

                liveData: {

                    asset: targetAsset,
                    instance: inststore.safeLookupBySource(parameter.value.id),
                    ...trailAndSubmission as SubmissionWithTrail
                }
                ,
                type: parameter.value.type
            }

            // console.log({ liveparamvalue: value })

            return value
        }

        ,

        mode: (path: string[], defaultMode: ResolveMode) => defaultMode === 'live' && path[0] === 'data' ? 'livedata' : defaultMode

        ,

        textValue: (p: AssetParameter, path: string[], clientLayoutConfig?: SubmissionLayoutConfig) => {

            const { ll } = settings

            const { id, liveData, type } = (p as AssetParameter).value

            const assetstore = type === requirementType ? reqstore : prodstore
            const inststore = type === requirementType ? reqinststore : prodinststore


            // fetch source where it's guaranteed to exist both inside and outside campaigns (can't just use live data.)



            const catvalue = (tags: Tag[]) => {

                return { name: tags.map(t => ll(t.name)).join(tagseparator), code: tags.filter(t => t.code).map(t => t.code).join(tagseparator) ?? fixedT, exists: tags.length ? 'true' : 'false' }

            }



            if (!liveData) {

                const asset = assetstore.safeLookup(id)

                const tags: TagMap[] = Object.entries(utils().index(asset.tags).byGroup(t => tagstore.lookupTag(t).category)).map(e => ({ category: e[0], tags: e[1] }))

                const subtags = (asset.properties.submissionTagMap ?? [])

                // if the asset has a lineage, we make a param for it on the fly 
                // and delegate to it resolving the rest of the path.
                if (path?.[0] === 'lineage') {

                    const lineage = asset.lineage?.[0]  // todo: handled multiple lineages.

                    return lineage ? self.textValue(makeLineageFor({ type: p.value.type, asset: lineage }), path.slice(1)) : fixedT(resolve_NA)
                }


                const data = {
                    name: ll(asset.name),
                    title: ll(asset.description),
                    source: asset.properties.source?.title ? ll(asset.properties.source.title) : '',
                    complianceObservation: undefined, // so it appears as missing, vs. not-existing (red error).
                    category: [...tags, ...subtags].reduce((acc, { category, tags }) => ({ ...acc, [category]: catvalue(tags.map(tagstore.lookupTag)) }), {}),
                    cap: cap.propertiesFor(type).reduce((acc, next) => ({ ...acc, [next.id]: cap.referenceFrom(asset.properties.cap, next) }), {}),

                    data: p.data

                }

                // console.log({staticwalk: data})

                return recwalk(data, path)
            }


            const config = clientLayoutConfig ?? layoutConfig as SubmissionLayoutConfig

            const { dashboard, campaign, party, tenant } = config

            const { instance, submission, asset, trail } = liveData

            //helper
            const deadlineOf = () => {

                const currentZone = cmpmodel.timeZone(campaign)!

                const deadline = dashboard.summaries.dueDates[asset.id] ?? eventinst.on(campaign).allDueDatesByRequirementId[asset.id]

                return time.format(moment(deadline).tz(currentZone) ?? fixedT("submission.labels.not_assessed"), 'short', language)
            }

            if (path[0] === 'lineage') {

                // we bail out if we can't resolve static data: no lineage asset, no campaign, no instance.
                // but we simulate dynamic data, so static data can still be resolved.
                // to separate these cases, we do it all ourselves withiout 

                const lineagedata = lineage.on(campaign).given({ trail, asset, party: tenant }, 'useFallback')

                const campaignLineage = lineagedata.campaign

                const data = lineagedata.assets?.[0]  //work only with a single lineage for now.

                if (!campaignLineage || !data.asset || data.trail?.id === 'notrail')
                    return fixedT(resolve_NA)


                const liveparam = {

                    ...makeLineageFor({ type: p.value.type, asset: data.asset.id }),

                    value: {

                        id: data.asset.id,
                        type,
                        liveData: {
                            asset: data.asset,
                            trail: data.trail,
                            submission: data.submission,
                            instance: inststore.on(campaignLineage).lookupBySource(data.asset.id)

                        } as AssetLiveData

                    } as AssetParameterValue

                }

                const layoutConfig: SubmissionLayoutConfig = { ...config, campaign: campaignLineage, dashboard }

                // console.log({liveparam,slice:path.slice(1)})

                return self.textValue(liveparam, path.slice(1), layoutConfig)


            }

            const isForParty = () => {

                const assetinst = instance.instanceType === requirementType ? reqinst.on(campaign) : prodinst.on(campaign)

                return assetinst.isForParty(party, instance)

            }
           
            switch (path[0]) {

                case 'name': return recwalk({ name: ll(liveData.asset.name) }, path)
                case 'title': return recwalk({ title: liveData.asset.description ? ll(liveData.asset.description) : ll(liveData.asset.name) }, path)
                case 'source': return recwalk({ source: liveData.asset.properties.source?.title ? ll(liveData.asset.properties.source?.title) : '' }, path)
                case 'deadline': return deadlineOf()
                case 'assessed': return isForParty() ? submission?.lifecycle.compliance?.state ? 'true' : 'false' : fixedT('submission.status.nostatus')
                case 'submitted': return submission?.lifecycle.lastSubmitted && submission?.lifecycle.state !== 'missing' ? 'true' : 'false'

            }

            if (!campaign || !party)
                return undefined


            const submitted = submission?.lifecycle.lastSubmitted && submission?.lifecycle.state !== 'missing'

            switch (path[0]) {

                case 'submissionDate': return submitted ? time.format(submission.lifecycle.lastSubmitted, 'long', language) : fixedT(missingOr(submission, "submission.labels.not_submitted"))

            }

            const trailSummary = dashboard.summaries.trailSummaryMap[submission?.trail!] ?? summary.on(campaign)(trail?.key, eventinst.on(campaign).allDueDatesByRequirementId[asset.id])

            // If the submission is the latest we can get the compliance from trailsummary cache instead of recomputing because the trailsummary holds computations based on the current situtation
            const complianceProfile = () => submission ?
                trailSummary?.official?.id === submission.id ? trailSummary.compliance
                    : substore.on(campaign).complianceProfile(submission, instance)
                : undefined

            

            switch (path[0]) {

                case 'complianceObservation': {

                    if (!isForParty()) return fixedT('submission.status.nostatus')

                    if (!instance.properties.assessed)
                        return fixedT(resolve_NA)

                    const profile = complianceProfile()

                    if (!profile)
                        return fixedT("submission.labels.not_assessed")

                    if (!profile.complianceObservation)
                        return fixedT(resolve_missing)

                    return recwalk({ complianceObservation: profile.complianceObservation }, path)
                }

                // no assessment yet (show penbding)
                case 'complianceRate': {

                    if (!isForParty()) return fixedT('submission.labels.not_applicable_badge')

                    if (!instance.properties.assessed)
                        return fixedT(resolve_NA)

                    return recwalk({ complianceRate: ll(complianceProfile()?.name) ?? fixedT("submission.labels.not_assessed") }, path)
                }

                // no assessment yet (show pending) vs. don't have a code (show error)
                case 'complianceCode': {

                    if (!isForParty()) return fixedT('submission.status.nostatus')

                    if (!instance.properties.assessed)
                        return fixedT(resolve_NA)

                    const codedata = complianceProfile()
                    
                    // lack of codes should be visble as error, so undefined.
                    return codedata ? codedata.code : fixedT("submission.labels.not_assessed")
                }
            }

            const computedTimeliness = trailSummary?.computedTimeliness

            // If the submission is the latest we can get the timeliness from trailsummary cache instead of recomputing because the trailsummary holds computations based on the current situtation
            const timelinessProfile = () => submission ? trailSummary?.official?.id === submission.id ?
                trailSummary.timeliness
                : substore.on(campaign).timelinessProfile(submission, computedTimeliness, instance) : undefined

            switch (path[0]) {

                case 'timelinessRate': return isForParty() ? recwalk({ timelinessRate: ll(timelinessProfile()?.tag?.name) ?? fixedT(resolve_missing) }, path) : fixedT('submission.labels.not_applicable_badge')

                case 'timelinessCode': {

                    if (!isForParty()) return fixedT('submission.status.nostatus')

                    // 02/04/24: we don't show computed timeliness if the was not explict compliance assessment. 
                    if (!instance.properties.assessed)
                        return fixedT(resolve_NA)

                    const codedata = complianceProfile()

                    if (!codedata?.code)
                        return fixedT("submission.labels.not_assessed")

                    const timelinessdata = timelinessProfile()
                    // lack of codes should be visble as error, so undefined.
                    return timelinessdata ? (timelinessdata.code ?? undefined) : fixedT("submission.labels.not_assessed")

                }

                case 'timeliness': return isForParty() ? fixedT(`dashboard.labels.timeliness.${computedTimeliness}`) : fixedT('submission.labels.not_applicable_badge')

                case 'category': {

                    const [, cat, ...rest] = path

                    const tags = [...asset.tags ?? [], ...submission?.lifecycle.tags ?? []]

                    const matches = tagstore.allTagsOfCategory(cat).filter(t => tags.includes(t.id))

                    if (!matches.length)
                        return fixedT(resolve_missing)

                    return recwalk(catvalue(matches), rest)

                }

                case `cap`: {

                    const [, propid, ...rest] = path

                    const prop = tagstore.lookupTag(propid)

                    if (!prop)
                        return fixedT(resolve_NA)

                    const properties = cap.referenceFrom(asset.properties.cap, prop)

                    return recwalk(properties, rest)
                }
            }


            // prepares to resolve all parameters other than 
            const customs = p.bridgeMode ? otherCustomParameters(liveData.asset).reduce((acc, op) => {

                // overlays instance overrides 
                const opWithOverrides = { ...op, value: p.value.liveData?.instance.properties.parameterOverlay?.[op.name]?.value ?? op.value }

                return {

                    ...acc,
                    [`${op.id}`]: params.parameterSpecOf(op).textValue(opWithOverrides, path)

                }

            }, {}) : {}


            const profile = {


                ...customs,
                data: p.data
            }

            return recwalk(profile, path)

        }

        ,

        randomValue: () => randomIn(reqstore.all())?.id

        ,

        references: (p: AssetParameter, noLineage?: 'nolineage'): ParameterReference[] => {


            const store = p.value.type === requirementType ? reqstore : prodstore

            const asset = store.safeLookup(p.value.id)
            const name = p.name || nameOfAsset(asset)

            // 'bridges' to other custom, non-live params.
            const customParamReferencesPrefixed = p.bridgeMode ?

                otherCustomParameters(asset).flatMap(p => params.parameterSpecOf(p).references(p)).map(cp => ({ id: `${p.id}.${cp.id}`, value: `${name}.${cp.value}` }))

                :

                []


            const languages = appConfig.get().intl.languages || intlconfig.languages

            const complianceObservationReferences = languages.map(l => ({ id: `${p.id}.complianceObservation.${l}`, value: t('layout.parameters.asset.references.complianceObservation', { name, lang: l }) }))

            const subtagnames = (cat: string) => sanitize(tagstore.lookupCategory(cat).name.en)

            const tagCategories = tagstore.allCategoriesOf(requirementType).filter(cat => !!cat.properties.field).map(c => c.id)

            const tagNamesReferences = tagCategories.map((category) => ({ id: `${p.id}.category.${category}.name`, value: t('layout.parameters.asset.references.category_name', { name: p.name, cat: subtagnames(category) }) })) ?? []
            const tagCodesReferences = tagCategories.map((category) => ({ id: `${p.id}.category.${category}.code`, value: t('layout.parameters.asset.references.category_code', { name: p.name, cat: subtagnames(category) }) })) ?? []

            const subtagReferences = asset.properties.submissionTagMap?.flatMap(({ category }) => [

                { id: `${p.id}.category.${category}.name`, value: t('layout.parameters.asset.references.category_name', { name: p.name, cat: subtagnames(category) }) },
                { id: `${p.id}.category.${category}.code`, value: t('layout.parameters.asset.references.category_code', { name: p.name, cat: subtagnames(category) }) },
                { id: `${p.id}.category.${category}.exists`, value: t('layout.parameters.asset.references.category_exists', { name: p.name, cat: subtagnames(category) }) },

            ]) ?? []

            const cappropname = (prop: Tag) => sanitize(prop.code ?? prop.name.en)

            const capvalueproperties = cap.propertiesFor(p.value.type).flatMap((prop) => [
                ({ id: `${p.id}.cap.${prop.id}.value`, value: t('layout.parameters.asset.references.cap_value', { name: p.name, cap: cappropname(prop) }) }),
                ({ id: `${p.id}.cap.${prop.id}.name`, value: t('layout.parameters.asset.references.cap_name', { name: p.name, cap: cappropname(prop) }) }),
                ({ id: `${p.id}.cap.${prop.id}.code`, value: t('layout.parameters.asset.references.cap_code', { name: p.name, cap: cappropname(prop) }) }),
                ({ id: `${p.id}.cap.${prop.id}.exists`, value: t('layout.parameters.asset.references.cap_exists', { name: p.name, cap: cappropname(prop) }) }),


            ]) ?? []


            // if the asset has a lineage, we a create params that stands for it and get its references recursively.
            // the lineage param has same id as this param, but postfixed with `lineage`. 
            // so the references that come back will be like references of this param, prefixed by `lineage`.
            // so will be resolved by textValue() based on that prefix, like any other reference. 
            let lineageReferences = [] as ParameterReference[]

            const lineage = asset.lineage?.[0] ?? asset.id

            if (!noLineage) {

                const lineageParam = makeLineageFor({ id: `${p.id}.lineage`, name: `${name}.lineage`, type: p.value.type, asset: lineage! })

                lineageReferences = self.references(lineageParam, 'nolineage')

            }

            const references = [
                { id: `${p.id}.name`, value: t('layout.parameters.asset.references.name', { name }) },
                { id: `${p.id}.title`, value: t('layout.parameters.asset.references.title', { name }) },
                { id: `${p.id}.source`, value: t('layout.parameters.asset.references.source', { name }) },
                { id: `${p.id}.deadline`, value: t('layout.parameters.asset.references.deadline', { name }) },
                { id: `${p.id}.submissionDate`, value: t('layout.parameters.asset.references.submissionDate', { name }) },
                { id: `${p.id}.submitted`, value: t('layout.parameters.asset.references.submitted', { name }) },
                { id: `${p.id}.assessed`, value: t('layout.parameters.asset.references.assessed', { name }) },
                { id: `${p.id}.complianceRate`, value: t('layout.parameters.asset.references.complianceRate', { name }) },
                { id: `${p.id}.complianceCode`, value: t('layout.parameters.asset.references.complianceCode', { name }) },
                { id: `${p.id}.timelinessRate`, value: t('layout.parameters.asset.references.timelinessRate', { name }) },
                { id: `${p.id}.timelinessCode`, value: t('layout.parameters.asset.references.timelinessCode', { name }) },
                { id: `${p.id}.timeliness`, value: t('layout.parameters.asset.references.timeliness', { name }) },

                ...complianceObservationReferences,

                ...tagNamesReferences,
                ...tagCodesReferences,
                ...subtagReferences,

                ...customParamReferencesPrefixed,
                ...capvalueproperties,

                ...lineageReferences,

                // data references

                ...(p.data ?

                    flatten(`${p.id}.data`, t('layout.parameters.asset.references.data', { name }), p.data)


                    : [])
            ]

            //console.log({ lineage, lineageReferences, references: [...references] })



            return references

        }

        ,

        Label: ({ parameter }) => parameter.value ? <RequirementLabel requirement={parameter.value.id ? parameter.value.id : parameter.value} /> : null

        ,

        ValueBox: props => {

            const reqsorted = reqstore.all().sort(reqmodel.comparator)
            const prodsorted = prodstore.all().sort(prodmodel.comparator)

            const { parameter, onChange, ...rest } = props

            const isRequirement = !!(reqstore.lookup(parameter.value.id))

            const currentValue = parameter.value ?

                parameter.value.type === requirementType ? reqstore.lookup(parameter.value.id) : prodstore.lookup(parameter.value.id)
                : undefined

            return <VSelectBox disabled={parameter.fixedValue} {...rest}
                clearable
                options={[...reqsorted, ...prodsorted]}
                onChange={(asset: RequirementDto | ProductDto) => onChange({ id: asset.id, type: parameter.value.type })}
                renderOption={asset => isRequirement ? <RequirementLabel requirement={asset} noLink noDecorations /> : <ProductLabel product={asset} noLink noDecorations />}
                lblTxt={r => l(r.name)}
                optionId={asset => ({ id: asset.id })}>

                {[currentValue]}

            </VSelectBox>

        }

    }

    return self
}