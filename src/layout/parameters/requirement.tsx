import { useCapProperties } from '#app/components/CapList'
import { useConfig } from '#app/config/state'
import { VSelectBox } from '#app/form/VSelectBox'
import { useT } from '#app/intl/api'
import { intlconfig } from "#app/intl/model"
import { useIntl } from '#app/intl/store'
import { useL } from '#app/model/multilang'
import { Tag, TagMap } from '#app/tag/model'
import { useTagStore } from '#app/tag/store'
import { useTime } from '#app/time/api'
import { randomIn, recwalk } from "#app/utils/common"
import { useCampaignModel } from '#campaign/model'
import { RequirementInstance, useRequirementInstanceStore } from '#campaign/requirement/api'
import { SubmissionLayoutConfig } from "#campaign/submission/Detail"
import { Submission, SubmissionContext, SubmissionWithTrail, Trail } from '#campaign/submission/model'
import { useSubmissionStore } from '#campaign/submission/store'
import { Component } from '#layout/components/model'
import { useLayoutConfig } from '#layout/context'
import { useLayoutSettings } from "#layout/settings/api"
import { RequirementLabel } from "#requirement/Label"
import { requirementIcon, requirementType } from "#requirement/constants"
import { Requirement, RequirementDto, useRequirementModel } from '#requirement/model'
import { useRequirementStore } from '#requirement/store'
import { utils } from 'apprise-frontend-core/utils/common'
import moment from 'moment-timezone'
import { useLayoutParameters } from './api'
import { AssetParameter, missingOr, resolve_NA, sanitize } from "./asset"
import { LiveParameter, Parameter, ParameterReference, ParameterSpec, isLiveParameter, partialParameterSpec } from "./model"


export type RequirementParameterValue = {

    id: string
    liveData?: LiveData

}

export type LiveData = {

    // the requirement.
    requirement: Requirement

    // the instance associated with this requirement in this campaign.
    instance: RequirementInstance

    // a related submission of the instance in this campaign, if any (inside reports, req sections, there may be no related submissions).
    submission?: Submission

    // the trail of the related submission.
    trail: Trail

}

const tagseparator = '·'

export type RequirementParameter = Parameter<RequirementParameterValue> & {

    // in bridge mode, references and resolves any other non-live custom parameter.
    bridgeMode: boolean
    fixedValue: boolean
    components: Component[]
    data: Record<string, any>
}


// helpers
const flatten = (idprefix: string, nameprefix: string, data: any): ParameterReference[] =>

    typeof data === 'object' ?
        Object.keys(data).flatMap(k => flatten(`${idprefix}.${k}`, `${nameprefix}.${k}`, data[k]))
        :

        [{ id: idprefix, value: nameprefix }]    // basecase





export const requirementParamId = requirementType


export const useRequirementParameter = (): LiveParameter<RequirementParameterValue, RequirementParameter> => {

    const intl = useIntl()
    const t = useT()
    const l = useL()

    const tagstore = useTagStore()

    const store = useRequirementStore()
    const model = useRequirementModel()

    const instances = useRequirementInstanceStore()
    const cmpmodel = useCampaignModel()
    const substore = useSubmissionStore()

    const time = useTime()
    const config = useConfig()

    const layoutConfig = useLayoutConfig()
    const settings = useLayoutSettings()
    const params = useLayoutParameters()

    const cap = useCapProperties()

    const otherCustomParameters = (requirement: Requirement) => {

        return requirement.properties.layout.parameters

            .filter(p => !isLiveParameter(params.parameterSpecOf(p)))  // excludes live params: specially handled
            .filter(p => params.parameterSpecOf(p).id !== requirementParamId) // excludes other requirement parameters, would be cyclic.


    }

    return {


        ...partialParameterSpec as any as ParameterSpec<RequirementParameterValue, RequirementParameter>,

        id: requirementParamId,
        name: 'layout.parameters.requirement.name',
        icon: requirementIcon

        ,

        valueFor: (ctx: SubmissionContext, parameter: RequirementParameter) => {

            const requirement = store.safeLookup(parameter.value.id)

            // lookup the requirement this parameter is configured with.
            const instance = instances.on(ctx.campaign).safeLookupBySource(parameter.value.id)

            // if we're rendering a requirement, take live data. 
            // if we're rendering a report, seek a match among related submissions.
            const trailAndSubmission = ctx.asset.instanceType === requirementType ? { submission: ctx.submission, trail: ctx.trail } :

                ctx.relatedSubmissions.find(sub => sub.trail.key.asset === instance?.source) ?? {} as SubmissionWithTrail



            return {

                id: parameter.value?.id,

                liveData: {

                    requirement,
                    instance,
                    ...trailAndSubmission
                }
            }
        }

        ,

        textValue: (p, path) => {


            const { ll } = settings

            const { id, liveData } = (p as AssetParameter).value

            // fetch source where it's guaranteed to exist both inside and outside campaigns (can't just use live data.)


            const requirement = store.lookup(id)!

            const catvalue = (tags: Tag[]) => {

                return { name: tags.map(t => ll(t.name)).join(tagseparator), code: tags.filter(t => t.code).map(t => t.code).join(tagseparator), exists: tags.length ? 'true' : 'false' }

            }


            if (!liveData) {


                const tags: TagMap[] = Object.entries(utils().index(requirement.tags).byGroup(t => tagstore.lookupTag(t).category)).map(e => ({ category: e[0], tags: e[1] }))

                const subtags = (requirement.properties.submissionTagMap ?? [])

                const data = {
                    name: ll(requirement.name),
                    title: ll(requirement.description),
                    source: requirement.properties.source?.title ? ll(requirement.properties.source.title) : '',
                    complianceObservation: undefined, // so it appears as missing, vs. not-existing (red error).
                    category: [...tags, ...subtags].reduce((acc, { category, tags }) => ({ ...acc, [category]: catvalue(tags.map(tagstore.lookupTag)) }), {}),
                    cap: cap.propertiesFor(requirementType).reduce((acc, next) => ({ ...acc, [next.id]: cap.referenceFrom(requirement.properties.cap, next) }), {}),

                    data: p.data
                }

                return recwalk(data, path)
            }

            const { parameterSpecOf } = params

            const { resolveLanguage } = settings

            const language = resolveLanguage()

            const t = intl.getFixedT(language)

            const { instance, submission } = liveData

            const { dashboard, campaign, party } = layoutConfig as SubmissionLayoutConfig

            const deadlineOf = () => {
                const currentZone = cmpmodel.timeZone(campaign)!

                const deadline = layoutConfig.dashboard.summaries.dueDates[instance.source]

                return time.format(moment(deadline).tz(currentZone) ?? t("submission.labels.not_assessed"),
                    'short',
                    language
                )
            }

            const submitted = submission?.lifecycle.lastSubmitted && submission?.lifecycle.state !== 'missing'

            switch (path[0]) {

                case 'name': return recwalk({ name: ll(requirement.name) }, path)
                case 'title': return recwalk({ title: requirement.description ? ll(requirement.description) : ll(requirement.name) }, path)
                case 'source': return recwalk({ source: requirement.properties.source?.title ? ll(requirement.properties.source?.title) : '' }, path)
                case 'deadline': return deadlineOf()
                case 'assessed': return submission?.lifecycle.compliance?.state ? 'true' : 'false'
                case 'submitted': return submitted ? 'true' : 'false'

            }

            if (!campaign || !party) return undefined



            switch (path[0]) {

                case 'submissionDate': return submitted ? time.format(submission.lifecycle.lastSubmitted, 'long', language) : t(missingOr(submission, "submission.labels.not_submitted"))

            }
            const trailSummary = dashboard.summaries.trailSummaryMap[submission?.trail!]

            // If the submission is the latest we can get the compliance from trailsummary cache instead of recomputing because the trailsummary holds computations based on the current situtation
            const complianceProfile = () => submission ?
                trailSummary.official?.id === submission.id ? trailSummary.compliance
                    : substore.on(campaign).complianceProfile(submission, instance)
                : undefined



            switch (path[0]) {

                case 'complianceObservation': return recwalk({

                    complianceObservation: complianceProfile()?.complianceObservation ?? (config.get().intl.languages ?? intlconfig.languages).reduce((acc, cur) => ({ ...acc, [cur]: 'N/A' }), {})

                }, path)

                case 'complianceRate': return recwalk({ complianceRate: ll(complianceProfile()?.name) ?? t("submission.labels.not_assessed") }, path)
                case 'complianceCode': return complianceProfile()?.code ?? t("submission.labels.not_assessed")

            }

            const computedTimeliness = trailSummary?.computedTimeliness

            // If the submission is the latest we can get the timeliness from trailsummary cache instead of recomputing because the trailsummary holds computations based on the current situtation
            const timelinessProfile = () => submission ? trailSummary.official?.id === submission.id ?
                trailSummary.timeliness
                : substore.on(campaign).timelinessProfile(submission,computedTimeliness, instance) : undefined

            switch (path[0]) {

                case 'timelinessRate': return recwalk({ timelinessRate: ll(timelinessProfile()?.tag?.name) ?? t("submission.labels.not_assessed") }, path)
                case 'timelinessCode': {


                    // 02/04/24: we don't show computed timeliness if the was not explict compliance assessment. 
                    if (!instance.properties.assessed)
                        return t(resolve_NA)

                    const codedata = complianceProfile()

                    if (!codedata?.code)
                        return t("submission.labels.not_assessed")

                    const timelinessdata = timelinessProfile()
                    // lack of codes should be visble as error, so undefined.
                    return timelinessdata ? (timelinessdata.code ?? undefined) : t("submission.labels.not_assessed")
                        

                }
                case 'timeliness': return t(`dashboard.labels.timeliness.${computedTimeliness}`)
                case 'category': {

                    const [, cat, ...rest] = path

                    const tags = [...requirement.tags ?? [], ...submission?.lifecycle.tags ?? []]

                    const matches = tagstore.allTagsOfCategory(cat).filter(t => tags.includes(t.id))

                    return recwalk(catvalue(matches), rest)
                }
                case `cap`: {

                    const [, propid, ...rest] = path

                    const prop = tagstore.lookupTag(propid)

                    const properties = cap.referenceFrom(requirement.properties.cap, prop)

                    return recwalk(properties, rest)
                }
            }


            // prepares to resolve all parameters other than 
            const customs = p.bridgeMode ? otherCustomParameters(requirement).reduce((acc, op) => {

                // overlays instance overrides 
                const opWithOverrides = { ...op, value: p.value.liveData?.instance.properties.parameterOverlay?.[op.name]?.value ?? op.value }

                return {

                    ...acc,
                    [`${op.id}`]: parameterSpecOf(op).textValue(opWithOverrides, path)

                }

            }, {}) : {}


            const profile = {


                ...customs,
                data: p.data
            }

            return recwalk(profile, path)

        }

        ,

        randomValue: () => (randomIn(store.all()) as RequirementDto)?.id

        ,

        references: (p): ParameterReference[] => {



            const requirement = store.safeLookup(p.value.id)
            const name = p.name || requirement.name.en?.replace(/\s/g, '-').toLowerCase()

            // 'bridges' to other custom, non-live params.
            const customParamReferencesPrefixed = p.bridgeMode ?

                otherCustomParameters(requirement).flatMap(p => params.parameterSpecOf(p).references(p)).map(cp => ({ id: `${p.id}.${cp.id}`, value: `${name}.${cp.value}` }))

                :

                []

            const languages = config.get().intl.languages || intlconfig.languages

            const complianceObservationReferences = languages.map(l => ({ id: `${p.id}.complianceObservation.${l}`, value: t('layout.parameters.requirement.references.complianceObservation', { name, lang: l }) }))

            const subtagnames = (cat: string) => sanitize(tagstore.lookupCategory(cat).name.en)

            const tagCategories = tagstore.allCategoriesOf(requirementType).filter(cat => !!cat.properties.field).map(c => c.id)

            const tagNamesReferences = tagCategories.map((category) => ({ id: `${p.id}.category.${category}.name`, value: t('layout.parameters.asset.references.category_name', { name: p.name, cat: subtagnames(category) }) })) ?? []
            const tagCodesReferences = tagCategories.map((category) => ({ id: `${p.id}.category.${category}.code`, value: t('layout.parameters.asset.references.category_code', { name: p.name, cat: subtagnames(category) }) })) ?? []


            const subtagReferences = requirement.properties.submissionTagMap?.flatMap(({ category }) => [

                { id: `${p.id}.category.${category}.name`, value: t('layout.parameters.asset.references.category_name', { name: p.name, cat: subtagnames(category) }) },
                { id: `${p.id}.category.${category}.code`, value: t('layout.parameters.asset.references.category_code', { name: p.name, cat: subtagnames(category) }) },
                { id: `${p.id}.category.${category}.exists`, value: t('layout.parameters.asset.references.category_exists', { name: p.name, cat: subtagnames(category) }) }

            ]) ?? []


            const cappropname = (prop: Tag) => sanitize(prop.code ?? prop.name.en)

            const capvalueproperties = cap.propertiesFor(requirementType).flatMap((prop) => [
                ({ id: `${p.id}.cap.${prop.id}.value`, value: t('layout.parameters.asset.references.cap_value', { name: p.name, cap: cappropname(prop) }) }),
                ({ id: `${p.id}.cap.${prop.id}.exists`, value: t('layout.parameters.asset.references.cap_exists', { name: p.name, cap: cappropname(prop) }) }),
                ({ id: `${p.id}.cap.${prop.id}.name`, value: t('layout.parameters.asset.references.cap_name', { name: p.name, cap: cappropname(prop) }) }),
                ({ id: `${p.id}.cap.${prop.id}.code`, value: t('layout.parameters.asset.references.cap_code', { name: p.name, cap: cappropname(prop) }) }),

            ]) ?? []


            const references = [
                { id: `${p.id}.name`, value: t('layout.parameters.requirement.references.name', { name }) },
                { id: `${p.id}.title`, value: t('layout.parameters.requirement.references.title', { name }) },
                { id: `${p.id}.source`, value: t('layout.parameters.requirement.references.source', { name }) },
                { id: `${p.id}.deadline`, value: t('layout.parameters.requirement.references.deadline', { name }) },
                { id: `${p.id}.submissionDate`, value: t('layout.parameters.requirement.references.submissionDate', { name }) },
                { id: `${p.id}.assessed`, value: t('layout.parameters.requirement.references.assessed', { name }) },
                { id: `${p.id}.submitted`, value: t('layout.parameters.requirement.references.submitted', { name }) },
                { id: `${p.id}.complianceRate`, value: t('layout.parameters.requirement.references.complianceRate', { name }) },
                { id: `${p.id}.complianceCode`, value: t('layout.parameters.requirement.references.complianceCode', { name }) },
                { id: `${p.id}.timelinessRate`, value: t('layout.parameters.requirement.references.timelinessRate', { name }) },
                { id: `${p.id}.timelinessCode`, value: t('layout.parameters.requirement.references.timelinessCode', { name }) },
                { id: `${p.id}.timeliness`, value: t('layout.parameters.requirement.references.timeliness', { name }) },

                ...complianceObservationReferences,

                ...customParamReferencesPrefixed,

                ...tagNamesReferences,
                ...tagCodesReferences,
                ...subtagReferences,

                ...capvalueproperties,

                // data references

                ...(p.data ?

                    flatten(`${p.id}.data`, t('layout.parameters.requirement.references.data', { name }), p.data)


                    : [])
            ]

            return references

        }

        ,

        Label: ({ parameter }) => parameter.value ? <RequirementLabel requirement={parameter.value.id ? parameter.value.id : parameter.value} /> : null

        ,

        ValueBox: props => {

            const { parameter, onChange, ...rest } = props

            const currentValue = parameter.value && store.lookup(parameter.value.id)

            return <VSelectBox disabled={parameter.fixedValue} {...rest}
                clearable
                options={store.all().sort(model.comparator)}
                onChange={(requirement: RequirementDto) => onChange({ id: requirement.id })}
                renderOption={r => <RequirementLabel requirement={r} noLink noDecorations />}
                lblTxt={r => l(r.name)}
                optionId={r => ({ id: r.id })}>

                {[currentValue]}

            </VSelectBox>

        }

    }
}

