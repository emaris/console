import { FieldProps } from "#app/form/Field"
import { Language } from "#app/intl/model"
import { deepclone } from "#app/utils/common"
import { CampaignNext } from '#campaign/context'
import { SubmissionContext } from '#campaign/submission/model'
import { Icon } from 'antd'
import { AiOutlineControl } from 'react-icons/ai'
import { newIdFor } from "../components/model"
import { ResolveMode } from "./api"


// a layut parameter that takes specific values in a campaign.
export type LiveParameter <V=any, P extends Parameter=Parameter<V>> = ParameterSpec<V,P> & {


    valueFor: (_:SubmissionContext, p:P) => V


}

export const isLiveParameter = (spec:ParameterSpec) : spec is LiveParameter => !!(spec as LiveParameter)?.valueFor


type ParameterSpecLabelProps<V=any,P extends Parameter<V>=Parameter> = { parameter: P, stripHtml?: boolean}

export type ParameterReference = {
    id:string,
    value:string
}
export type ParameterSpec<V=any,P extends Parameter<V>=Parameter> = {

    id: string
    name: string
    icon: JSX.Element
 
    
    Label: (props:ParameterSpecLabelProps) => JSX.Element | null
    ValueBox: (_:ValueBoxProps<V,P>) => JSX.Element

    references: (p:P) => ParameterReference[]

    defaultValue: ()=>V

    // some parameters may want to be resolved in different modes based on the reference path
    mode: (path: string[], defaultMode : ResolveMode) => ResolveMode
    textValue: (p:P, path:string[]) => string
    resolvedTextValue: (text:string) => string

    randomValue: () => any

    language?: (p:P) => Language | undefined

    clone: (p:P, _?:CampaignNext) => P | undefined

}

export type ValueBoxProps<V=any,P extends Parameter<V>=Parameter> = FieldProps & {

    parameter: P,
    onChange: (_:V) => void
    live?: boolean

}

export type Parameter<V=any> = {

    id: string

    spec: string

    name: string

    required: boolean
    
    value: V

    fixed: boolean
}

export type ParameterRef<V=any> = {
    value: V
    original: Parameter<V>
}

// export const isParameterRef = (ref: any): ref is ParameterRef => Object.keys(ref).includes('original') && Object.keys(ref).includes('value')

export const paramIcon = <Icon component={AiOutlineControl} />

export const partialParameterSpec : ParameterSpec = {

    id: undefined!,

    name: undefined!,

    icon: paramIcon,
    
    references: p=>[{id:p.id,value:p.name}],

    textValue: p => `${p.value}`,

    defaultValue: () => undefined,

    resolvedTextValue: s => s,
    
    randomValue: () => undefined,
    
    Label: props => <span>{JSON.stringify(props.parameter.value)}</span>,
   
    ValueBox: ()=> <div>coming soon...</div>,

    clone: (p) => ({...p, id: newIdFor(p.spec)}),

    mode: (_, defaultMode: ResolveMode) => defaultMode

}


export const newParameterFrom = (spec:string) : Parameter => {

    return {
    
        id: newIdFor(spec),
        spec:spec,
        required:false,
        name:undefined!,
        value: undefined!,
        fixed: false
    }

}

export const cloneParameter = (p:Parameter) => ({...deepclone(p), id:newIdFor(p.spec), name: `${p.name} (cloned)` }) as Parameter




