
import { useCapProperties } from '#app/components/CapList'
import { useConfig } from '#app/config/state'
import { VSelectBox } from '#app/form/VSelectBox'
import { useT } from '#app/intl/api'
import { intlconfig } from "#app/intl/model"
import { useIntl } from '#app/intl/store'
import { useL } from '#app/model/multilang'
import { Tag, TagMap } from '#app/tag/model'
import { useTagStore } from '#app/tag/store'
import { useTime } from '#app/time/api'
import { randomIn, recwalk } from "#app/utils/common"
import { useCampaignModel } from '#campaign/model'
import { ProductInstance, useProductInstanceStore } from '#campaign/product/api'
import { SubmissionLayoutConfig } from "#campaign/submission/Detail"
import { Submission, SubmissionContext, Trail } from "#campaign/submission/model"
import { useSubmissionStore } from '#campaign/submission/store'
import { useLayoutConfig } from '#layout/context'
import { useLayoutSettings } from "#layout/settings/api"
import { ProductLabel } from "#product/Label"
import { productIcon, productType } from "#product/constants"
import { Product, ProductDto, useProductModel } from '#product/model'
import { useProductStore } from '#product/store'
import { utils } from 'apprise-frontend-core/utils/common'
import moment from 'moment-timezone'
import { AssetParameter, missingOr, resolve_NA, sanitize } from "./asset"
import { LiveParameter, Parameter, ParameterReference, partialParameterSpec } from "./model"


export type ProductParameterValue = {

    id: string,

    liveData?: LiveData
}

const tagseparator = '·'

export type LiveData = {

    product: Product

    // the instance associated with this product in this campaign, if any.
    instance: ProductInstance

    // a related submission of the instance in this campaign, if any.
    submission: Submission

    // the trail of the related submission, if any.
    trail: Trail

}


export type ProductParameter = Parameter<ProductParameterValue> & {

    fixedValue: boolean
}

export const productParamId = productType

export const useProductParameter = (): LiveParameter<ProductParameterValue, ProductParameter> => {

    const intl = useIntl()

    const t = useT()
    const l = useL()

    const tagstore = useTagStore()

    const store = useProductStore()
    const model = useProductModel()

    const instances = useProductInstanceStore()
    const cmpmodel = useCampaignModel()
    const substore = useSubmissionStore()

    const time = useTime()
    const config = useConfig()

    const layoutConfig = useLayoutConfig()

    const settings = useLayoutSettings()

    const cap = useCapProperties()

    return {


        ...partialParameterSpec as any as LiveParameter<ProductParameterValue, ProductParameter>,

        id: productParamId,
        name: 'layout.parameters.product.name',
        icon: productIcon

        ,

        valueFor: (ctx: SubmissionContext, parameter: Parameter) => {

            const product = store.safeLookup(parameter.value.id)

            const instance = instances.on(ctx.campaign).safeLookupBySource(parameter.value.id)

            return {

                id: parameter.value.id,

                liveData: {
                    product,
                    instance,
                    submission: ctx.submission,
                    trail: ctx.trail
                }

            }
        }

        ,

        textValue: (p, path) => {

            const { ll } = settings

            const { id, liveData } = (p as AssetParameter).value

            // fetch source where it's guaranteed to exist both inside and outside campaigns (can't just use live data.)

            const product = store.safeLookup(id)!

            const catvalue = (tags: Tag[]) => {

                return { name: tags.map(t => ll(t.name)).join(tagseparator), code: tags.filter(t => t.code).map(t => t.code).join(tagseparator), exists: tags.length ? 'true' : 'false'}

            }


            if (!liveData) {

                const tagsAndCategories: TagMap[] = Object.entries(utils().index(product.tags).byGroup(t => tagstore.lookupTag(t).category)).map(e => ({ category: e[0], tags: e[1] }))

                const subtagsAndCategories = (product.properties.submissionTagMap ?? [])

                const target = {
                    name: ll(product.name),
                    title: ll(product.description),
                    source: product.properties.source?.title ? ll(product.properties.source.title) : '',
                    complianceObservation: undefined, // so it appears as missing, vs. not-existing (red error).
                    category: [...tagsAndCategories, ...subtagsAndCategories].reduce((acc, { category, tags }) => ({ ...acc, [category]: catvalue(tags.map(tagstore.lookupTag)) }), {}),
                    cap: cap.propertiesFor(productType).reduce((acc, next) => ({...acc, [next.id] : cap.referenceFrom(product.properties.cap, next) }),{}),

                }

                return recwalk(target, path)
            }

            const { resolveLanguage } = settings

            const language = resolveLanguage()

            const t = intl.getFixedT(language)

            const { instance, submission } = liveData

            const { dashboard, campaign, party } = layoutConfig as SubmissionLayoutConfig

            const deadlineOf = () => {
                const currentZone = cmpmodel.timeZone(campaign)!

                const deadline = layoutConfig.dashboard.summaries.dueDates[instance.source]

                return time.format(moment(deadline).tz(currentZone) ?? t("submission.labels.not_assessed"),
                    'short',
                    language
                )
            }

            const submitted = submission?.lifecycle.lastSubmitted && submission?.lifecycle.state !== 'missing'
            const published = submission?.lifecycle.lastPublished && submission?.lifecycle.state !== 'missing'

            switch (path[0]) {

                case 'name': return recwalk({ name: ll(product.name) }, path)
                case 'title': return recwalk({ title: product.description ? ll(product.description) : ll(product.name) }, path)
                case 'source': return recwalk({ source: product.properties.source?.title ? ll(product.properties.source?.title) : '' }, path)
                case 'deadline': return deadlineOf()
                case 'assessed': return submission?.lifecycle.compliance?.state ? 'true' : 'false'
                case 'submitted': return submitted ? 'true' : 'false' 
                case 'published': return published ? 'true' : 'false'
                case 'reference': return recwalk({ reference: ll(submission?.lifecycle.reference) ?? '' }, path)

            }

            if (!campaign || !party) return undefined


           
            switch (path[0]) {

                case 'submissionDate': return submitted ? time.format(submission.lifecycle.lastSubmitted, 'long', language) : t(missingOr(submission,"submission.labels.not_submitted")) 
                case 'publicationDate': return published ? time.format(submission.lifecycle.lastPublished, 'long', language) : t(missingOr(submission,"submission.labels.not_published"))

            }
            const trailSummary = dashboard.summaries.trailSummaryMap[submission?.trail!]

            // If the submission is the latest we can get the compliance from trailsummary cache instead of recomputing because the trailsummary holds computations based on the current situtation
            const complianceProfile = () => submission ?
                trailSummary.official?.id === submission.id ? trailSummary.compliance
                    : substore.on(campaign).complianceProfile(submission, instance)
                : undefined

            switch (path[0]) {

                case 'complianceObservation': return recwalk({

                    complianceObservation: complianceProfile()?.complianceObservation ?? (config.get().intl.languages ?? intlconfig.languages).reduce((acc, cur) => ({ ...acc, [cur]: 'N/A' }), {})

                }, path)

                case 'complianceRate': return recwalk({ complianceRate: ll(complianceProfile()?.name) ?? t("submission.labels.not_assessed") }, path)
                case 'complianceCode': return complianceProfile()?.code ?? t("submission.labels.not_assessed")

            }

            const computedTimeliness = trailSummary?.computedTimeliness

            // If the submission is the latest we can get the timeliness from trailsummary cache instead of recomputing because the trailsummary holds computations based on the current situtation
            const timelinessProfile = () => submission ? trailSummary.official?.id === submission.id ?
                trailSummary.timeliness
                : substore.on(campaign).timelinessProfile(submission,computedTimeliness, instance) : undefined

            switch (path[0]) {

                case 'timelinessRate': return recwalk({ timelinessRate: ll(timelinessProfile()?.tag?.name) ?? t("submission.labels.not_assessed") }, path)
                case 'timelinessCode': {


                    // 02/04/24: we don't show computed timeliness if the was not explict compliance assessment. 
                    if (!instance.properties.assessed)
                        return t(resolve_NA)

                    const codedata = complianceProfile()

                    if (!codedata?.code)
                        return t("submission.labels.not_assessed")

                    const timelinessdata = timelinessProfile()
                    // lack of codes should be visble as error, so undefined.
                    return timelinessdata ? (timelinessdata.code ?? undefined) : t("submission.labels.not_assessed")
                        

                }
                case 'timeliness': return t(`dashboard.labels.timeliness.${computedTimeliness}`)
                case 'category': {

                    const [, cat, ...rest] = path

                    const tags = [...product.tags ?? [], ...submission?.lifecycle.tags ?? []]

                    const matches = tagstore.allTagsOfCategory(cat).filter(t => tags.includes(t.id))

                    return recwalk(catvalue(matches), rest)
                }

                case `cap`: {

                    const [, id, ...rest] = path

                    const prop = tagstore.lookupTag(id)

                    const properties = cap.referenceFrom(product.properties.cap,prop) 
                    
                    return recwalk(properties, rest)
                }

            }


            const profile = {
                name: ll(product.name),
                title: ll(product.description),
            }

            return recwalk(profile, path)


        }
        ,

        randomValue: () => randomIn(store.all())?.id

        ,

        references: (p): ParameterReference[] => {

            const name = { name: p.name }

            const product = store.safeLookup(p.value.id)

            const languages = config.get().intl.languages || intlconfig.languages

            const complianceObservationReferences = languages.map(l => ({ id: `${p.id}.complianceObservation.${l}`, value: t('layout.parameters.product.references.complianceObservation', { name: p.name, lang: l }) }))


            const subtagnames = (cat: string) => sanitize(tagstore.lookupCategory(cat).name.en)

            const tagCategories = tagstore.allCategoriesOf(productType).filter(cat => !!cat.properties.field).map(c => c.id)

            const tagNamesReferences = tagCategories.map((category) => ({ id: `${p.id}.category.${category}.name`, value: t('layout.parameters.asset.references.category_name', { name: p.name, cat: subtagnames(category) }) })) ?? []
            const tagCodesReferences = tagCategories.map((category) => ({ id: `${p.id}.category.${category}.code`, value: t('layout.parameters.asset.references.category_code', { name: p.name, cat: subtagnames(category) }) })) ?? []

            const subtagReferences = product.properties.submissionTagMap?.flatMap(({ category }) => [
                
                { id: `${p.id}.category.${category}.name`, value: t('layout.parameters.asset.references.category_name', { name: p.name, cat: subtagnames(category) }) },
                { id: `${p.id}.category.${category}.code`, value: t('layout.parameters.asset.references.category_code', { name: p.name, cat: subtagnames(category) }) },
                { id: `${p.id}.category.${category}.exists`, value: t('layout.parameters.asset.references.category_exists', { name: p.name, cat: subtagnames(category) }) }
            
            ]) ?? []



            const cappropname = (prop: Tag) => sanitize(prop.code ?? prop.name.en)

            const capvalueproperties = cap.propertiesFor(productType).flatMap((prop) => [
                ({ id: `${p.id}.cap.${prop.id}.value`, value: t('layout.parameters.asset.references.cap_value', { name: p.name, cap: cappropname(prop) }) }),
                ({ id: `${p.id}.cap.${prop.id}.name`, value: t('layout.parameters.asset.references.cap_name', { name: p.name, cap: cappropname(prop) }) }),
                ({ id: `${p.id}.cap.${prop.id}.code`, value: t('layout.parameters.asset.references.cap_code', { name: p.name, cap: cappropname(prop) }) }),

            ]) ?? []

            return [
                { id: `${p.id}.name`, value: t('layout.parameters.product.references.name', name) },
                { id: `${p.id}.title`, value: t('layout.parameters.product.references.title', name) },
                { id: `${p.id}.deadline`, value: t('layout.parameters.product.references.deadline', name) },
                { id: `${p.id}.submissionDate`, value: t('layout.parameters.product.references.submissionDate', name) },
                { id: `${p.id}.publicationDate`, value: t('layout.parameters.product.references.publicationDate', name) },
                { id: `${p.id}.assessed`, value: t('layout.parameters.product.references.assessed', name) },
                { id: `${p.id}.submitted`, value: t('layout.parameters.product.references.submitted', name) },
                { id: `${p.id}.published`, value: t('layout.parameters.product.references.published', name) },
                { id: `${p.id}.complianceRate`, value: t('layout.parameters.product.references.complianceRate', name) },
                { id: `${p.id}.complianceCode`, value: t('layout.parameters.product.references.complianceCode', name) },
                { id: `${p.id}.timelinessRate`, value: t('layout.parameters.product.references.timelinessRate', name) },
                { id: `${p.id}.timelinessCode`, value: t('layout.parameters.product.references.timelinessCode', name) },
                { id: `${p.id}.timeliness`, value: t('layout.parameters.product.references.timeliness', name) },
                { id: `${p.id}.reference`, value: t('layout.parameters.product.references.reference', name) },

                ...complianceObservationReferences,

                ...tagNamesReferences,
                ...tagCodesReferences,
                ...subtagReferences,
                ...capvalueproperties
            ]

        }

        ,

        Label: ({ parameter }) => parameter.value ? <ProductLabel product={parameter.value.id ? parameter.value.id : parameter.value} /> : null

        ,

        ValueBox: props => {

            const { parameter, onChange, ...rest } = props

            const currentValue = parameter.value && store.lookup(parameter.value.id)

            return <VSelectBox disabled={parameter.fixedValue} {...rest}
                clearable
                options={store.all().sort(model.comparator)}
                onChange={(product: ProductDto) => onChange({ id: product.id })}
                renderOption={p => <ProductLabel product={p} noLink />}
                lblTxt={p => l(p.name)}
                optionId={r => ({ id: r.id })}>

                {[currentValue]}

            </VSelectBox>

        }

    }
}

