
import { Drawer } from "#app/components/Drawer";
import { FieldRow } from "#app/form/FieldRow";
import { Form } from "#app/form/Form";
import { SelectBox } from "#app/form/SelectBox";
import { Switch } from "#app/form/Switch";
import { TextBox } from "#app/form/TextBox";
import { useFormState } from "#app/form/hooks";
import { useT } from '#app/intl/api';
import * as React from "react";
import { useLayoutRegistry } from '../registry';
import { ParameterSpecLabel } from "./label";
import { useLayoutParameters } from './api';
import { Parameter, ParameterSpec } from "./model";


type Props = React.PropsWithChildren<{

    detail: Parameter

    onClose: (_: Parameter, __: boolean) => void
    onClone: (_: Parameter) => Promise<Parameter>

}>

export const ParameterDetail = (props: Props) => {

    const t = useT()

    const { detail, onClose } = props

    const isNew = !detail.id

    const { parameterParam, parameterSpecOf, validateParameter } = useLayoutParameters()
   
    const state = useFormState(detail)

    const { edited, change } = state

    const spec = parameterSpecOf(edited)

    const { ValueBox } = spec

    const report = validateParameter(edited)

    const specs = useLayoutRegistry().allParameters()

    const hasErrors = report.errors() > 0
    const isValid = !hasErrors

    return <Drawer routeId={parameterParam()} width={450} icon={spec.icon} title={detail.name ?? t('layout.parameters.labels.new_title')} visible={true} onClose={_ => onClose(edited, isValid)} warnOnClose={hasErrors}>

        <Form state={state}>

            <FieldRow >

                <SelectBox grouped disabled={!isNew} style={{ flexGrow: 1, marginRight: 10 }} label={t("common.fields.type.name")} onChange={change((t, v) => { t.spec = v; if (parameterSpecOf(v).defaultValue()) t.value = parameterSpecOf(v).defaultValue(); else delete t.value; })}
                    getkey={(s: ParameterSpec) => s.id}
                    getlbl={(s: ParameterSpec) => <ParameterSpecLabel spec={s} />}
                    selectedKey={edited.spec}>
                    {specs}
                </SelectBox>

                <Switch grouped disabled={edited.fixed} label={t("common.fields.required.name")} onChange={change((p, v) => p.required = v)}>
                    {edited.fixed || edited.required}
                </Switch>

            </FieldRow>

            <TextBox label={t("common.fields.name.name")} disabled={edited.fixed} onChange={change((p, v) => p.name = v)} validation={report.name}>
                {edited.name}
            </TextBox>

            <ValueBox label={t('common.fields.value.name')} parameter={edited} onChange={change((t, v) => { if (v) t.value = v; else delete t.value })} />

        </Form>


    </Drawer>

}