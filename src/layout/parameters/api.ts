import { useT } from '#app/intl/api';
import { MultilangDto } from "#app/model/multilang";
import { isQuillBlockValue, normalizeQuillValue } from "#app/utils/common";
import { useFeedback } from '#app/utils/feedback';
import { resolve as ternaryresolver } from "#app/utils/resolver";
import { updateQuery } from "#app/utils/routes";
import { check, checkIt, empty, uniqueWith, withReport } from '#app/utils/validation';
import { notReported } from "#campaign/constants";
import { useLayoutSettings } from '#layout/settings/api';
import produce from "immer";
import { useContext } from 'react';
import { useLocation } from 'react-router-dom';
import { newIdFor } from "../components/model";
import { LayoutContext, ParameterOverlayContext, useLayoutConfig } from '../context';
import { isInstance } from "../model";
import { useLayoutRegistry } from '../registry';
import { listqueryparam } from "./constants";
import { Parameter, ParameterSpec, cloneParameter, newParameterFrom } from "./model";
import { magicAnchor } from "./parametricboxes";



export type ResolveMode = 'text' | 'markup' | 'link' | 'live' | 'livedata'

const collator = new Intl.Collator()

// matches mention spans capturing data-id and data-value attributes.
export const defaultRegexp = `<span.[*]?class=["|']mention["|'].[^>]*data-id=["|'](.*?)["|'].[^>]*data-value=["|'](.*?)["|'].*?>.*?</span>`

const rrregexp = new RegExp(defaultRegexp, 'g')


export const useLayoutParameters = () => {

    const t = useT()
    const fb = useFeedback()

    const { pathname, search } = useLocation()

    const overlay = useContext(ParameterOverlayContext)

    const state = useContext(LayoutContext)

    const registry = useLayoutRegistry()
    const settings = useLayoutSettings()
    const config = useLayoutConfig()

    const self = {


        validateParameter: (edited: Parameter) => {


            const plural = t("layout.parameters.plural").toLowerCase()

            return withReport({

                name: check(edited.name)
                    .with(empty(t))
                    .with(uniqueWith(self.allParameters(), p => p.name.toLowerCase() === edited.name?.toLowerCase() && p.id !== edited.id, t))
                    .nowOr(t("common.fields.name.msg", { plural }))
                ,

                spec: checkIt().nowOr(t("common.fields.type.msg"))

                ,

                required: checkIt().nowOr(t("common.fields.required.msg"))

            })

        }
        ,


        allParameters: () => overlay ?? state.layout.parameters


        ,

        lookupParameter: (name: string) => self.allParameters().find(p => p.name === name)

        ,

        lookupParameterById: (id: string) => self.allParameters().find(p => p.id === id)

        ,

        parameterParam: () => "param-drawer"

        ,

        parametersRoute: () => self.parameterRoute(listqueryparam)

        ,

        // eslint-disable-next-line
        parameterRoute: (name: string | undefined) => `${pathname}?${updateQuery(search).with(params => params[self.parameterParam()] = name ?? null)}`

        ,

        // invoke change() with a new layout that is like the current one except for a "modified copy" of its parameters.
        changeParametersWith: (updater: (_: Parameter[]) => void) => Promise.resolve(state.change({ ...state.layout, parameters: produce(state.layout.parameters, updater) }))

        ,

        parameterSpecOf: (p: Parameter | string) => registry.lookupParameter(typeof p === 'string' ? p : p.spec)

        ,

        nextParameter: (spec: ParameterSpec) => ({ ...newParameterFrom(spec.id), id: undefined!, value: spec.defaultValue() })

        ,

        saveParameter: (parameter: Parameter) => {

            if (parameter.id)

                return self.changeParametersWith(parameters => {

                    const index = parameters.findIndex(p => p.id === parameter.id)

                    parameters.splice(index, 1, parameter)

                })
                    .then(_ => parameter)


            else {

                const newparameter = { ...parameter, id: newIdFor(parameter.spec) }

                return self.changeParametersWith(parameters => {

                    parameters.splice(parameters.findIndex(p => !p.fixed) ?? 0, 0, newparameter); // insert after the first non-fixed

                })
                    .then(_ => newparameter)

            }

        }

        ,


        removeParameter: (parameter: Parameter) => {

            const singular = t(self.parameterSpecOf(parameter).name).toLowerCase()

            fb.askConsent({

                title: t('common.consent.remove_one_title', { singular }),
                content: t('common.consent.remove_one_msg', { singular }),
                okText: t('common.consent.remove_one_confirm', { singular }),

                onOk: () => self.changeParametersWith(parameters => {

                    const index = parameters.findIndex(p => p.id === parameter.id)

                    parameters.splice(index, 1)

                })

            })

        }

        ,



        moveParameter: (from: number, to: number) =>

            self.changeParametersWith(parameters => {

                const parameter = parameters[from]

                if (from > to) {

                    parameters.splice(from, 1)

                    parameters.splice(to, 0, parameter)

                }

                else {

                    parameters.splice(to, 0, parameter)

                    parameters.splice(from, 1)
                }


            })


        ,

        cloneParameter: (parameter: Parameter): Promise<Parameter> => {

            const clone = cloneParameter(parameter)

            return self.changeParametersWith(parameters => {

                const index = parameters.findIndex(p => p.id === parameter.id)

                parameters.splice(index + 1, 0, clone)

            })
                .then(_ => clone)

        }

        ,


        allParameterReferences: (filter: (_: Parameter) => boolean = () => true, additionalParameters: Parameter[]) => {

            return [...self.allParameters(), ...additionalParameters]
                .filter(filter)
                .flatMap(p => self.parameterSpecOf(p).references(p))
                .sort((a, b) => collator.compare(a?.value, b?.value))


        }
        ,

        // returns some text after resolving any parameter within to its text value.
        r: (text: string | undefined, parameters: Parameter[] = [], resolvemode: ResolveMode = 'markup') => self.rr(text, parameters, resolvemode)

        ,

        multir: (multilang: MultilangDto = {}, parameters: Parameter[] = [], resolvemode: ResolveMode = 'markup') =>

            settings.forEachLanguage(lang => self.r(multilang[lang], parameters, resolvemode))


        ,

        // returns some text after resolving the parameters within, either to either their value or to some markup that link the value to the parameter definition.
        // by default, it resolves parameters to their value inside layout instances (live mode) and to markup in layout templates (design mode).  
        rr: (text: string | undefined, contextualParameters: Parameter[] = [], resolvemode: ResolveMode = isInstance(state.layout) ? config.mode === 'live' ? 'live' : 'markup' : 'link'): string => {

            if (!text)
                return text!


            const map = {} as {
                [_: string]: Parameter<any>;
            }

            const allParams = [...self.allParameters(), ...contextualParameters]

            allParams.reduce((acc, p) => {

                acc[p.id] = p

                return acc

            }, map)


            function recresolve(text: string, resolvepath: string = '', depth: number = 0): string {


                return text.replace(rrregexp, (_, match: string, matchname: string) => {

                    const [id, ...valuepath] = match.split(".")
                    const [name] = matchname.split(".")

                    if (resolvepath.split(":").includes(match)) {
                        console.log("cycle error", resolvepath, "includes", match)
                        return markError(match)
                    }


                    // console.log({id,match,valuepath})

                    if (id in map)

                        try {

                            const resolved = map[id]

                            const { textValue, resolvedTextValue, mode: modeFor } = self.parameterSpecOf(resolved)

                            const mode = modeFor(valuepath, resolvemode)

                            // nothing to resolve in this value, leave it unresolved withuout implying error.
                            if (!resolved.value)
                                return markUnresolved(matchname, mode)

                            const textval = textValue(resolved, valuepath)

                            const value = normalizeQuillValue(textval)
                            // const value = stripHtmlTags(textValue(resolved, state, valuepath), ['a'])

                            if (!value)
                                return markUnresolved(matchname, mode)

                            //console.log("resolved",resolved, "to value",value)

                            const replaced = resolvedTextValue(recresolve(value, `${resolvepath}:${match}`, depth + 1))

                            
                           

                            //console.log("replacing parameterless",value, "to",replaced)

                            return depth > 0 ? replaced : markResolved(replaced, name, mode, isQuillBlockValue(replaced))
                            // return depth > 0 ? replaced : markResolved(replaced, name, mode)

                        }
                        catch (e) {
                            console.log("error when resolving", id, "in", text, e)
                            return markError(matchname)
                        }



                    return markError(matchname)
                })

            }


            function markResolved(text: string, param: string = text, mode: string, block: boolean = false) {

                const wrappedText = mode == 'livedata' ? block ? `<div class="${mode}">${text}</div>` : `<span class="${mode}">${text}</span>` : text
                // console.log({text})
                // const wrappedText = `<span class="${mode}" style="display: block;">${text}</span>`

                const markup = `<code class="param-value">${wrappedText}</code>`

                return resolvemode === 'link' ? magicAnchor(self.parameterRoute(param), markup) : wrappedText

            }

            function markUnresolved(text: string, mode: ResolveMode) {

                const markup = `<code class="param-value unresolved">${text}</code>`
                const live = `<code class="param-value unresolved">${text}</code>`
                const livedata = `<code class="param-value unresolved error">${t(notReported)}</code>`

                switch (mode) {

                    case 'text': return `<${text}>`
                    case 'markup': return markup
                    case 'live': return live
                    case 'livedata': return livedata
                    default: return markup
                }

            }

            function markError(text: string) {

                const markup = `<code class="param-value error">${text}!!</code>`
                const live = `<code class="param-value error">${text}!!</code>`

                switch (resolvemode) {

                    case 'text': return `<${text}!!>`
                    case 'markup': return markup
                    case 'live': return live
                    default: return markup
                }

            }

            const result = recresolve(text)

            return isInstance(state.layout) ? ternaryresolver(result) : recresolve(result)
        },

        lookupLanguageProvider: () => self.allParameters().find(p => self.parameterSpecOf(p).language)


    }


    return self;
}