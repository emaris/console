import { Form } from "#app/form/Form"
import { useFormState } from "#app/form/hooks"
import { LanguageBox } from "#app/intl/LanguageBox"
import { useT } from '#app/intl/api'
import { Validation } from "#app/utils/validation"
import { ReadOnly } from '../../app/scaffold/ReadOnly'
import { useLayoutSettings } from '#layout/settings/api'
import { ParameterLabel } from "./label"
import { useLayoutParameters } from './api'
import { Parameter } from "./model"
import "./styles.scss"
import { useCampaignModel } from '#campaign/model'
import { useCurrentCampaign } from '#campaign/hooks'


type PanelProps = {

    parameters: Parameter[]
    onChange: (index: number, _: any) => void
    withLanguageSelector?: boolean
    forInstance?: boolean

}

export const ParameterPanel = (props: PanelProps) => {

    const t = useT()

    const campaign = useCurrentCampaign()

    const model = useCampaignModel()
    const { parameterSpecOf } = useLayoutParameters()
    const { changeLayoutLanguage, resolveLanguage } = useLayoutSettings()

    const { parameters, onChange, withLanguageSelector = false, forInstance = false } = props

    const validateRequired = (p: Parameter): Validation => {

        const valid = !!p.value || !p.required

        return { status: valid ? "success" : "error", msg: valid ? '' : t("layout.parameters.feedback.required_error") }
    }
    const languageSelector = <LanguageBox className='layout-language-selector max-width' iconOnLabel onChange={changeLayoutLanguage} selectedKey={resolveLanguage()} />


    const formstate = useFormState(parameters)

    const parametersList = parameters.map((p, i) => {

        const { ValueBox } = parameterSpecOf(p)

        return <ValueBox validation={validateRequired(p)} key={i} live={forInstance} label={<ParameterLabel parameter={p} />} parameter={p} onChange={v => onChange(i, v)} />

    })
 

    return <ReadOnly value={campaign && model.isArchived(campaign)}>

        <div className="parameter-panel">
            <Form state={formstate}>
                {withLanguageSelector && languageSelector}
                {parametersList}
            </Form>
        </div>

        </ReadOnly>

}

