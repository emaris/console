import { Label } from "#app/components/Label"
import { useT } from '#app/intl/api'
import * as React from "react"
import { useLayoutParameters } from './api'
import { Parameter, ParameterSpec } from "./model"


type SpecProps = {

    spec : ParameterSpec
    style?: React.CSSProperties
}

export const ParameterSpecLabel = (props:SpecProps) => {

    const t = useT()

    const {spec,style} = props

    let contents : React.ReactNode = t(spec.name)

   return <Label className="parameter-spec-label" icon={spec.icon} titleStyle={style} title={contents} />
}



type ParameterProps = {

    parameter : Parameter 
    style?: React.CSSProperties         
    linkTo?: string
    decorations?: React.ReactNode[]
}


export const ParameterLabel = (props:ParameterProps) => {

    const {parameter,style, ...rest} = props

    const {parameterSpecOf} = useLayoutParameters()

    const specs = parameterSpecOf(parameter)

    let contents : React.ReactNode = parameter.name

    return <Label className="parameter-label" icon={specs.icon} titleStyle={style} title={contents} {...rest} />
}