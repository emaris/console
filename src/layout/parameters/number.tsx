import { HtmlSnippet } from "#app/components/HtmlSnippet";
import { randomNumber } from "#app/utils/common";
import { Icon } from "antd";
import mexp from "math-expression-evaluator";
import * as aiicns from "react-icons/ai";
import { ParametricNumberBox } from "./parametricboxes";
import { ParameterSpec, partialParameterSpec } from "./model";

export const numberParamId = 'number'

export const useNumberParameter = (): ParameterSpec => {


    return {

        ...partialParameterSpec,

        id: numberParamId,
        name: 'layout.parameters.number.name',
        icon: <Icon className="anticon-number" component={aiicns.AiOutlineNumber} />,


        resolvedTextValue: v => `${mexp.eval(v)}`

        ,

        randomValue: () => randomNumber(100)

        ,

        Label: ({ parameter }) => parameter.value ? <HtmlSnippet snippet={`${parameter.value}`} /> : null

        ,

        ValueBox: ValueBox

        ,

        defaultValue: () => 0
    }


    function ValueBox(props) {

        const { parameter, onChange, ...rest } = props

        return <ParametricNumberBox {...rest} onChange={onChange} filter={(p => p.id !== parameter.id)}>
            {parameter.value}
        </ParametricNumberBox>
    }

}