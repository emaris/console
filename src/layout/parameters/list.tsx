import { Button } from "#app/components/Button"
import { Drawer } from "#app/components/Drawer"
import { UnknownLabel } from "#app/components/Label"
import { Column, VirtualTable } from "#app/components/VirtualTable"
import { icns } from "#app/icons"
import { useT } from '#app/intl/api'
import { Topbar } from "#app/scaffold/Topbar"
import { through } from "#app/utils/common"
import { paramsInQuery } from "#app/utils/routes"
import { Tooltip } from "antd"
import { useHistory, useLocation } from "react-router-dom"
import { ParameterDetail } from "./detail"
import { ParameterLabel } from "./label"
import { useTextParameter } from "./text"
import { useLayoutParameters } from './api'
import { newparam } from "./constants"
import { Parameter, paramIcon } from "./model"
import "./styles.scss"



export const ParameterList = () => {

    const t = useT()
 
    const history = useHistory()
    const {search} = useLocation()
    
    const textParam = useTextParameter()

    const {parameterParam,lookupParameter, lookupLanguageProvider, nextParameter, parameterSpecOf, allParameters, saveParameter, removeParameter, moveParameter,cloneParameter, parameterRoute,parametersRoute } = useLayoutParameters()

    const {[parameterParam()]:parameter} = paramsInQuery(search)

    const showList = !!parameter   
    const detail =  parameter === newparam ? nextParameter(textParam): lookupParameter(parameter as string) 

    const singular = t("layout.parameters.singular")

    const onClose = ()=>history.push(parameterRoute(undefined))
    const onParameterDetailClose = (p:Parameter, valid: boolean) => {
        if (valid) saveParameter(p)
        history.push(parametersRoute())
    }
    const onAdd = ()=>history.push(parameterRoute(newparam)) 
    const onClone = (p:Parameter) => cloneParameter(p).then(through(clone=>history.push(parameterRoute(clone.name))))

    const addBtn = < Button type="primary" size="small" onClick={onAdd}  icn={icns.add}>{t("common.buttons.add_one",{singular})}</ Button>
    const editBtn =  (p:Parameter) => <Button key="edit" icn={icns.edit} onClick={()=>history.push(parameterRoute(p.name))} >{t("common.buttons.edit")}</Button>
    const removeBtn =  (p:Parameter) => <Button key="remove" disabled={p.fixed} icn={icns.remove} onClick={ ()=>removeParameter(p)} >{t("common.buttons.remove")}</Button>
    const cloneBtn =  (p:Parameter) => <Button key="clone" disabled={p.fixed} icn={icns.clone} onClick={ ()=>cloneParameter(p)} >{t("common.buttons.clone")}</Button>

    const languageProvider = lookupLanguageProvider()
    const languageProviderIcon = (p:Parameter)=> p.id===languageProvider?.id ? <Tooltip title={t("layout.parameters.feedback.language_provider")}>{icns.language}</Tooltip> : <></>
    const fixedParamIcon = (p:Parameter)=> p.fixed ? <Tooltip title={t("layout.parameters.feedback.predefined")}>{icns.readonly()}</Tooltip> : <></>

    const data = allParameters()


    return <Drawer width={620} routeId={parameterParam()} icon={paramIcon} title={t('layout.parameters.plural')} visible={showList} onClose={onClose}>
       
       <Topbar offset={64}>
            {addBtn}
        </Topbar>

       <VirtualTable<Parameter> fixedHeight 
                selectable={false} 
                rowHeight={45}
                rowKey="id"
                actions={p=>[editBtn(p),cloneBtn(p),removeBtn(p)]}
                filtered={data.length>15} 
                onDrop ={ moveParameter }
                dropIf = { p => !p.fixed }
                data={data} 
                style={{lineHeight:2}}
                onDoubleClick={p=>history.push(parameterRoute(p.name))} >

            <Column<Parameter> sortable={false} dataKey="name" flexShrink={2} minWidth={150} width={170} title={t("common.fields.name_multi.name")} 
                dataGetter={p=>p.name}
                cellRenderer={({rowData:p}) => {
                    const lpi = languageProviderIcon(p)
                    const fpi = fixedParamIcon(p)
                    return <ParameterLabel parameter={p} linkTo={parameterRoute(p.name)} decorations={[fpi, lpi]}/>}
                }/>
           
            <Column<Parameter> sortable={false} flexGrow={1} title={t("common.fields.value.name")} dataKey="value" 
                                dataGetter={p=> { 
                                    if (p.value) {
                                        const { Label } = parameterSpecOf(p) 
                                        return <Label parameter={p} stripHtml /> 
                                    } 
                                    else
                                     return <UnknownParameterValueLabel />
                                }}/>
           
            <Column<Parameter> sortable={false} flexShrink={3} minWidth={95} width={95} style={{textAlign:"center"}} title={t("common.fields.required.name")} dataKey="mandatory" dataGetter={p=>p.required && icns.checkGreen} />



        </VirtualTable>

        { detail && <ParameterDetail detail={detail} onClose={onParameterDetailClose} onClone={onClone} /> }


    </Drawer>

}

export const UnknownParameterValueLabel = () => {
    const t = useT()
    return <UnknownLabel noLink title={t("layout.parameters.labels.unknown_value")} tip={()=>t("layout.parameters.labels.unknown_value_msg")} disabled={true} />
}