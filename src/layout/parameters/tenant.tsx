import { VSelectBox } from "#app/form/VSelectBox"
import { useT } from '#app/intl/api'
import { useL } from '#app/model/multilang'
import { TenantLabel } from "#app/tenant/Label"
import { tenantIcon, tenantType } from "#app/tenant/constants"
import { useTenantStore } from '#app/tenant/store'
import { randomIn, recwalk } from "#app/utils/common"
import { useLayoutSettings } from '#layout/settings/api'
import { ParameterReference, ParameterSpec, partialParameterSpec } from "./model"


export const tenantParamId = tenantType

export const useTenantParameter = () : ParameterSpec => {


    const t = useT()
    const l = useL()
    
    const store = useTenantStore()
    const { ll } = useLayoutSettings()

    return {


        ...partialParameterSpec,

        id: tenantParamId,
        name: 'layout.parameters.tenant.name',
        icon: tenantIcon

        ,

        textValue: (p, path) => {


            const party = store.lookup(p.value)!  // worst case scenario: parameter doesn't resolve.

            const profile = { name: ll(party.name), language: party.preferences.language }

            return recwalk(profile, path)


        }
        ,

        randomValue: () => {
            return randomIn(store.all())?.id

        }

        ,

        references: (p): ParameterReference[] => {


            return [
                { id: `${p.id}.name`, value: t('layout.parameters.tenant.references.name', { name: p.name }) },
                { id: `${p.id}.language`, value: t('layout.parameters.tenant.references.language', { name: p.name }) }
            ]

        },

        language: (p) => {

            const party = store.lookup(p.value)  // worst case scenario: parameter doesn't resolve.

            return party?.preferences.language
        }


        ,

        Label: ({ parameter }) => parameter.value ? <TenantLabel tenant={parameter.value} /> : null

        ,

        ValueBox: props => {



            const { parameter, onChange, ...rest } = props

            const currentValue = store.lookup(parameter.value)

            return <VSelectBox {...rest}
                clearable
                lblTxt={t => l(t.name)}
                options={store.allSorted()}
                onChange={(v: any) => onChange(v?.id)}
                renderOption={r => <TenantLabel noLink tenant={r} />}
                optionId={r => r.id}>

                {[currentValue]}

            </VSelectBox>

        }



    }
}