import { VSelectBox } from "#app/form/VSelectBox"
import { useT } from '#app/intl/api'
import { ParameterReference, partialParameterSpec } from "./model"
import { useLayoutSettings } from '#layout/settings/api'
import { useL } from '#app/model/multilang'
import { randomIn, recwalk } from "#app/utils/common"
import { CampaignLabel } from "#campaign/Label"
import { campaignIcon, campaignType } from "#campaign/constants"
import { CampaignDto, useCampaignModel } from "#campaign/model"
import { useCampaignStore } from '#campaign/store'
import { LiveParameter } from "./model"

export const campaignParamId = campaignType

export const useCampaignParameter = (): LiveParameter<string> => {

    const t = useT()
    const l = useL()

    const model = useCampaignModel()
    const store = useCampaignStore()

    const { ll } = useLayoutSettings()


    return ({

        ...partialParameterSpec,

        id: campaignParamId,
        name: 'layout.parameters.campaign.name',
        icon: campaignIcon


        ,


        valueFor: ctx => ctx.campaign.id

        ,

        textValue: (p, path) => {

          
            const campaign = store.lookup(p.value)!  // worst case scenario: parameter doesn't resolve.

            const profile = { name: ll(campaign.name) }

            return recwalk(profile, path)


        }
        ,

        randomValue: () => {
            return (randomIn(store.all()) as CampaignDto)?.id

        }

        ,

        references: (p): ParameterReference[] => {

            return [
                { id: `${p.id}.name`, value: t('layout.parameters.campaign.references.name', { name: p.name }) }
            ]

        }

        ,

        Label: ({ parameter }) => parameter.value ? <CampaignLabel noLiveView campaign={parameter.value} /> : null

        ,

        ValueBox: props => {


            const { parameter, onChange, ...rest } = props

            const currentValue = store.lookup(parameter.value)

            return <VSelectBox
                {...rest}
                clearable
                lblTxt={c => l(c.name)}
                options={store.all().sort(model.comparator)}
                onChange={(v: any) => onChange(v?.id)}
                renderOption={c => <CampaignLabel noLink noLiveView campaign={c} />}
                optionId={c => c.id}>

                {[currentValue]}

            </VSelectBox>

        }

    })

}
