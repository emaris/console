import { HtmlSnippet } from "#app/components/HtmlSnippet"
import { icns } from "#app/icons"
import { MultilangDto, newMultiLang, randomMultilang, useL } from "#app/model/multilang"
import { useLayoutSettings } from '#layout/settings/api'
import { ParametricMultiTextBox } from "./parametricboxes"
import { useLayoutParameters } from './api'
import { Parameter, ParameterSpec, partialParameterSpec } from "./model"

export type TextParam = Parameter<MultilangDto>

export const textParamId = 'text'

export const useTextParameter = (): ParameterSpec<MultilangDto, TextParam> => {

    const l = useL()
    const params = useLayoutParameters()
    const settings = useLayoutSettings()

    return {

        ...partialParameterSpec,

        id: textParamId,
        name: 'layout.parameters.text.name',
        icon: icns.form,

        defaultValue: () => newMultiLang(),

        randomValue: () => randomMultilang()

        ,

        textValue: (p) => {
          

            return params.rr(settings.ll(p.value))
        }

        ,


        Label: ({ parameter, stripHtml }) => {

           

            // we don't want parameter resolution here
            return parameter.value ? <HtmlSnippet snippet={l(parameter.value)} stripHtml={stripHtml} /> : null

        },

        ValueBox: props => {

            const { parameter, onChange, label, required, ...rest } = props

            return <ParametricMultiTextBox {...rest} label={label} id={`${parameter.id}-value`} onChange={onChange} >
                {parameter.value}
            </ParametricMultiTextBox>

        }



    }
}