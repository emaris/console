
import { useT } from '#app/intl/api'
import { numberParamId } from '#layout/parameters/number'
import { RequirementParameter, RequirementParameterValue, requirementParamId } from "#layout/parameters/requirement"
import { defaultBaseline } from "#layout/style/text"
import { campaignParamId } from './campaign'
import { dateyearParamId, defaultDateParameterValue } from "./dateyear"
import { ProductParameter, productParamId } from "./product"
import { ReferenceParam, referenceParamId } from "./reference"
import { tenantParamId } from './tenant'
import { Parameter, newParameterFrom } from "./model"



export const listqueryparam="_list"
export const newparam="_new"

export const predefinedPartyId = "party"
export const predefinedCampaignId = "campaign"

export const predefinedParameterIds = [predefinedPartyId, predefinedCampaignId]

export const usePredefinedParameters = () => {

    const t = useT()

    return {
        get: (): Parameter[] => {

            return [

                {
                    ...newParameterFrom(tenantParamId),
                    name: t('layout.parameters.tenant.predefined_no_translation'),
                    id: predefinedPartyId,
                    fixed: true,
                    required: true,
                    flag: true

                } as Parameter,

                {
                    ...newParameterFrom(campaignParamId),
                    id: predefinedCampaignId,
                    name: t('layout.parameters.campaign.predefined_no_translation'),
                    fixed: true,
                    required: true
                },

                {
                    ...newParameterFrom(dateyearParamId),
                    id: predefinedRequirementReportingYearParameterId,
                    name: t('layout.parameters.reported_on.predefined_no_translation'),
                    fixed: true,
                    required: true,
                    value: defaultDateParameterValue()
                },

                {
                    ...newParameterFrom(numberParamId),
                    id: predefinedFontSizeBaseLineParameterId,
                    name: predefinedFontSizeBaseLineParameterId,
                    fixed: false,
                    required: false,
                    value: defaultBaseline
                }
            ]

        }
    }
}

export const predefinedFontSizeBaseLineParameterId = "layout-font-baseline"
export const predefinedRequirementParameterId = "requirement"
export const predefinedRequirementReportingYearParameterId = "requirement-reporting-year"

export const usePredefinedRequirementParameters = () => {

    const t = useT()

    return {
        get: (): Parameter[] => {

            const thisRequirementParam = {
                ...newParameterFrom(requirementParamId),

                id: predefinedRequirementParameterId,
                name: t('layout.parameters.requirement.predefined_no_translation'),
                fixed: true,
                required: true,
                fixedValue: true,
                value: {} as RequirementParameterValue

            } as RequirementParameter


            // const thisReportingYearParam =
            //     {...newParameterFrom(dateyearspec),
            //         id: predefinedRequirementReportingYearParameterId,
            //         name: t('layout.parameters.reported_on.predefined_no_translation'), 
            //         fixed:true,
            //         required:true }

            return [thisRequirementParam]

        }
    }
}

export const predefinedProductParameterId = "product"
export const predefinedProductReferenceId = "ref-template"
export const predefinedProductParameterIds = [predefinedProductParameterId, predefinedProductReferenceId]

const defaultReferenceValue = '<p><span class="mention" contenteditable="false" data-index="3" data-denotation-char="@" data-content-editable="false" data-id="product.name" data-value="report.name">@report.name</span>-<span class="mention" contenteditable="false" data-index="0" data-denotation-char="@" data-content-editable="false" data-id="campaign.name" data-value="campaign.name">@campaign.name</span>-<span class="mention" contenteditable="false" data-index="1" data-denotation-char="@" data-content-editable="false" data-id="party.name" data-value="party.name">@party.name</span>'

export const usePredefinedProductParameters = () => {

    const t = useT()

    return {
        get: (): Parameter[] => {


            const thisProductParameter: ProductParameter = {
                ...newParameterFrom(productParamId),
                id: predefinedProductParameterId,
                name: t('layout.parameters.product.predefined_no_translation'),
                fixed: true,
                required: true,
                fixedValue: true,
                value: { id: undefined!, liveData: undefined }

            }

            const thisProductReferenceSpecParameter: ReferenceParam = {
                ...newParameterFrom(referenceParamId),
                id: predefinedProductReferenceId,
                name: t('layout.parameters.reference_format.predefined_no_translation'),
                fixed: true,
                required: true,
                value: { en: defaultReferenceValue, fr: defaultReferenceValue }

            }

            return [thisProductParameter, thisProductReferenceSpecParameter]

        }
    }
}