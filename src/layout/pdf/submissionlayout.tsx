import { useCurrentLanguage } from '#app/intl/api'
import { BytestreamedContext } from '#app/stream/BytestreamedHelper'
import { stripHtmlTags } from '#app/utils/common'
import { CurrentCampaignContext, useCurrentCampaign } from '#campaign/hooks'
import { SubmissionReactContext } from '#campaign/submission/context'
import { useSubmissionContext } from '#campaign/submission/hooks'
import { PDFProps } from '#layout/components/model'
import { LayoutInstance } from '#layout/model'
import { usePdfLayout } from '#layout/pdf/hook'
import { useLayoutSettings } from '#layout/settings/api'
import { productType } from '#product/constants'
import { useProductStore } from '#product/store'
import { useRequirementStore } from '#requirement/store'
import { AppState, useCurrentAppState } from '#state'
import { useLayoutParameters } from '../parameters/api'

export const useSubmissionDownload = (layout: LayoutInstance) => {

    const ctx = useSubmissionContext()

    const { document, downloadPdf, generatePdf } = usePdfLayout({ layout, initialiseParameters: ctx.initialiseParameters })

    const currentCampaign = useCurrentCampaign()

    const { party, submission } = ctx

    // const publicName = usePublicName()

    // const name = `${publicName}.pdf`

    const appstate = useCurrentAppState()

    const submissionDocument = (props: PDFProps) =>
        <AppState value={appstate}>
            <CurrentCampaignContext campaign={currentCampaign}>
                <SubmissionReactContext.Provider value={ctx}>
                    <BytestreamedContext tenant={party.id} target={submission.id}  >
                        {document?.(props)}
                    </BytestreamedContext>
                </SubmissionReactContext.Provider>
            </CurrentCampaignContext>
        </AppState>

    const downloadSubmissionPdf = (name: string) => downloadPdf({ fileName: name, document: submissionDocument })
    const generateSubmissionPdf = () => generatePdf({ document: submissionDocument })
    // const generateBytestreamForPdf = () => generateSubmissionPdf().then(doc => bytestreamForBlob(name, doc))

    return { downloadSubmissionPdf, document: submissionDocument, generateSubmissionPdf }

}


export const usePublicName = () => {
    const { rr } = useLayoutParameters()
    const { layoutLanguage, ll } = useLayoutSettings()
    const ctx = useSubmissionContext()
    const currentLang = useCurrentLanguage()

    const language = layoutLanguage ?? currentLang

    const isPublished = ctx.submission.lifecycle.state === 'published'

    const products = useProductStore()
    const requirements = useRequirementStore()

    const campaign = ll(ctx.campaign.name)
    const title = ctx.asset.instanceType === productType ? ll(products.lookup(ctx.asset.source)?.name!) : ll(requirements.lookup(ctx.asset.source)?.name!)

    const referenceParamInLang = ctx.submission.lifecycle.reference ? ctx.submission.lifecycle.reference[language] ? ctx.submission.lifecycle.reference[language] : undefined : undefined

    const formatFile = (fileName: string): string => stripHtmlTags(fileName).substring(0, 254).replaceAll(',', '').replaceAll(' ', '_')

    const fileName = (referenceParamInLang && isPublished) ? rr(referenceParamInLang) : `${title}-${language}-${campaign}`

    return formatFile(`${fileName}.pdf`)

}