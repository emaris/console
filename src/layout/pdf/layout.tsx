
import { buildinfo } from '#app/buildinfo'
import { BytestreamedContext } from '#app/stream/BytestreamedHelper'
import { LayoutApiProvider } from '#layout/apicontext'
import { useLayoutComponents } from '#layout/components/api'
import { PDFProps } from '#layout/components/model'
import { usePageComponent } from '#layout/components/page'
import { LayoutConfigProvider } from '#layout/config'
import { LayoutAssetContext, LayoutConfig, LayoutContext, LayoutRegistryContext, LayoutRegistryState, LayoutSettings, LayoutSettingsContext, LayoutState } from '#layout/context'
import { Layout } from '#layout/model'
import { Parameter, ParameterSpec } from '#layout/parameters/model'
import { ParameterInitializer } from '#layout/provider'
import { Product } from '#product/model'
import { Requirement } from '#requirement/model'
import { AppState } from '#state'
import ReactPDF, { Document, Page, Text, View } from '@react-pdf/renderer'
import { Language } from 'apprise-frontend-core/intl/language'
import { StateProvider } from 'apprise-frontend-core/state/provider'
import { BrowserRouter, Route, Switch, useRouteMatch } from 'react-router-dom'
import 'react-toastify/dist/ReactToastify.css'


type PDFLayoutProps = {
    appstate: AppState
    layout: Layout
    layoutConfig: LayoutConfig
    layoutRegistry: LayoutRegistryState
    lang: Language
    preview: boolean
    asset: Requirement | Product
    routeMatch: ReturnType<typeof useRouteMatch>
    initialiseParameters?: (_: [Parameter, ParameterSpec][]) => Parameter[]


} & Pick<typeof ReactPDF,'Page'>


export default function PDFLayout(props: PDFProps & PDFLayoutProps) {

    const { appstate, lang, layout, layoutRegistry, layoutConfig, Font, asset, routeMatch, initialiseParameters } = props

    Font.register({
        family: "Roboto", fonts: [
            { src: `${buildinfo.prefix}/pdf-fonts/Roboto-Regular.ttf` },
            { src: `${buildinfo.prefix}/pdf-fonts/Roboto-Italic.ttf`, fontStyle: 'italic' },
            { src: `${buildinfo.prefix}/pdf-fonts/Roboto-Bold.ttf`, fontWeight: 600 },
            { src: `${buildinfo.prefix}/pdf-fonts/Roboto-BoldItalic.ttf`, fontWeight: 600, fontStyle: 'italic' }
        ]
    })

    const state = { layout } as LayoutState
    const settings = (lang ? { layoutLanguage: lang } : {}) as LayoutSettings

    return <BrowserRouter basename={location.pathname.split("/")[1]}>
        <AppState value={appstate}>
            <LayoutContext.Provider value={state}>
                <StateProvider initialState={settings} context={LayoutSettingsContext}>
                    <StateProvider initialState={layoutRegistry} context={LayoutRegistryContext}>
                        <LayoutConfigProvider config={layoutConfig}>
                            <LayoutApiProvider>
                                <BytestreamedContext>
                                    <LayoutAssetContext.Provider value={asset}>
                                        <ParameterInitializer initialiseParameters={initialiseParameters}>
                                            <LayoutApiProvider>
                                                <Switch>
                                                    <Route path={routeMatch?.path}>
                                                        <DocumentContent {...props} />
                                                    </Route>
                                                    <PDFError />
                                                </Switch>
                                            </LayoutApiProvider>
                                        </ParameterInitializer>
                                    </LayoutAssetContext.Provider>
                                </BytestreamedContext>
                            </LayoutApiProvider>
                        </LayoutConfigProvider>
                    </StateProvider>
                </StateProvider>
            </LayoutContext.Provider>
        </AppState>
    </BrowserRouter >

}


const DocumentContent = (props: PDFProps & PDFLayoutProps) => {

    const { Print, PrintPreview } = usePageComponent()

    const { preview, Document } = props

    const components = useLayoutComponents()

    return <Document onRender={console.warn}>{components.allPages().map((p, position) => preview ?
        <PrintPreview {...props} className='page-card' style={{ display: "flex", flexDirection: "row" }} key={position} component={p} />
        : <Print {...props} className='page-card' style={{ display: "flex", flexDirection: "row" }} key={position} component={p} />)
    }
    </Document>
}

const PDFError = () => <Document>
    <Page>
        <View>
            <Text style={{ fontSize: 10, textAlign: 'center' }}>Error: couldn't generate contents</Text>
        </View>
    </Page>
</Document>


