
import { useLayoutApis } from '#layout/apicontext'
import { PDFProps } from '#layout/components/model'
import { useParameters } from '#layout/parameters/hooks'
import { StyledBoxProps } from '../style/styledbox'
import { pageContentsPdfStylesheets } from './constants'
import { pdfHelper } from './helper'


export const StyledView = (props: PDFProps & StyledBoxProps) => {

    const { children, component, style = {}, View, Text, StyleSheet } = props

    const label = component.hasOwnProperty('label') && component['label']

    const { styles: { resolvePdfStyle }, settings: { ll } } = useLayoutApis()
    const { rr } = useParameters()

    const { convertHtmlToPdfComponents } = pdfHelper(props)

    const outerStyle = StyleSheet.create({ ...resolvePdfStyle(component, spec => spec.type === "outer"), ...style })
    const innerStyle = StyleSheet.create({ ...resolvePdfStyle(component, spec => spec.type === "inner") })

    const styles = StyleSheet.create(pageContentsPdfStylesheets)

    // on large docs (like CQs), padding can glitch at bottom of page, mis-interacts with page bottom padding.
    // we remove it and simulate it with minHeight.
   // const patchedStyle = { ...outerStyle, display: "flex", alignItems: 'flex-start', flexDirection: "column", justifyContent: "default", padding: 0 }

    return <View style={{...outerStyle, padding:0}}>
        {/* {outerStyle.paddingTop ? <View style={{ minHeight: outerStyle.paddingTop }} /> : null} */}
        <View style={innerStyle}>
            {label && <Text style={styles.label}>{convertHtmlToPdfComponents(rr(ll(label)), { noWrap: true })}</Text>}
            {children}
        </View>
        {/* {outerStyle.paddingBottom ? <View style={{ minHeight: outerStyle.paddingBottom }} /> : null} */}
    </View>

    //  pre-patch version: padding implemented directly in css but glitches on large documents spilling compressed lines at page bottom.
    // //return <View style={{...outerStyle, display:"flex", flexDirection:"row" /* , padding:0 */}}>
    //     {/* {outerStyle.paddingTop ? <View style={{minHeight:outerStyle.paddingTop}}/> : null} */}
    //     <View style={innerStyle}>
    //         {label && <Text style={styles.label}>{convertHtmlToPdfComponents(ll(label), { noWrap: true })}</Text>}
    //         {children}
    //     </View>
    //     {/* {outerStyle.paddingBottom ? <View style={{minHeight:outerStyle.paddingBottom}}/> : null} */}
    // </View>

}

export default StyledView