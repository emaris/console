import { useLayoutApis } from '#layout/apicontext'
import { Component } from '#layout/components/model'
import { StyleProps } from '../style/model'
import { pageContentsPdfStylesheets } from './constants'



export const usePdfStyles = (component: Component & StyleProps, style?: string) => {

    const {styles: {resolvePdfCustomStyle}} = useLayoutApis()
    
    const defaultStyle = style && pageContentsPdfStylesheets[style]

    return {...defaultStyle, ...resolvePdfCustomStyle(component)}

}