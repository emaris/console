import { arrayPartition, randomNumber, randomString } from '#app/utils/common'
import { PDFProps } from '#layout/components/model'
import ReactHtmlParser from 'react-html-parser'
import { liveDataColour, pageContentsPdfStylesheets, paramValueStyles } from './constants'

export type PdfHelperConfig = {
    debug?: boolean
    style?: any
    noWrap?: boolean
}

const complexTypes = ['ol', 'ul', 'img', 'div']

export const pdfHelper = (props: PDFProps) => {

    const { Text, View, Link, Canvas, Image: PDFImage, StyleSheet } = props

    const pageStyles = StyleSheet.create(pageContentsPdfStylesheets)

    let helperConfig: PdfHelperConfig = { debug: false, noWrap: true, style: pageStyles }

    const carriageReturn = () => <Text key={`carriagereturn_${randomNumber(10000)}_${randomString(25)}`}>{'\n'}</Text>

    const wrapChunk = (chunk: string | undefined): string => (chunk || '').toLocaleLowerCase().startsWith('<p>') ? chunk || '' : `<p>${chunk}</p>`

    const log = (...msg) => helperConfig.debug && console.log(...msg)

    let stylesAccumulator = [] as any[]

    let liveMode = false as boolean

    const self = {

        addStyleToAccumulator: (style) => stylesAccumulator.push(style)

        ,

        resetStyleAccumulator: () => stylesAccumulator = []

        ,

        toggleLiveMode: () => liveMode = !liveMode

        ,

        isLiveMode: () => liveMode

        ,

        // Removes all control unicode codes (at least most of them)
        normalizeText: (text: string | undefined): string =>

            text === undefined ? '' : text
                .replace(/\t/g, ' ') // The tab is kept as a space
                // eslint-disable-next-line
                .replace(/[\u{0001}-\u{001F}]/gu, '')
                .replace(/\u{007F}/u, '')
                .replace(/[\u{0080}-\u{008F}]/gu, '')
                .replace(/[\u{0090}-\u{009F}]/gu, '')
                .replace(/\u{00A0}/u, '')
                .replace(/\u{027B}/u, '')

        ,

        convertHtmlToPdfComponents: (htmlChunk: string, config?: PdfHelperConfig) => {
            helperConfig = config ? { ...helperConfig, ...config } : helperConfig
            log("convertHtmlToPdfComponents received chunk", htmlChunk)

            const contents = helperConfig.noWrap ? htmlChunk : wrapChunk(htmlChunk)

            if (contents === undefined) return

            const parsed = ReactHtmlParser(contents, { transform: (node, _) => self.mapHtmlEntitiesToPdfComponents(node) })
            log("Resolved document tree", parsed)
            // return self.testPdf()
            return parsed
        }

        ,

        resolveQuillStyles: (node: any) => {
            if (node === undefined || node === null) return {}
            if (node.attribs === undefined || node.attribs === null) return {}
            let pStyle = {}
            if (Object.keys(node.attribs).includes('class')) {
                const className = node.attribs.class

                if (className === 'ql-align-center') {
                    pStyle = { ...pStyle, textAlign: 'center' }
                }
                if (className === 'ql-align-right') {
                    pStyle = { ...pStyle, textAlign: 'right' }
                }
                if (className === 'ql-align-justify') {
                    pStyle = { ...pStyle, textAlign: 'justify' }
                }
            }
            return pStyle
        }

        ,

        hasLiveMode: (node: any) => node.attribs['class'] && node.attribs['class'].includes('livedata')

        ,

        testPdf: () => {
            return <View>
                <View>
                    <Text>
                        <Text>This is a text </Text>
                        <Text style={{ fontWeight: 600 }}>this is a bold text that should go beside</Text>
                    </Text>
                </View>
                <View key={1} style={{ width: '98%' }}>
                    <View key={`inner_list`} style={{ display: 'flex', flexDirection: 'row', alignSelf: 'stretch' }}>
                        <Canvas style={helperConfig.style.bullet} paint={(painter) => painter.circle(5, 8, 3).fillAndStroke('black', "#fff")} />
                        <View style={{ marginTop: 1.3, flexGrow: 1 }}>
                            <Text>test1</Text>
                        </View>
                    </View>
                </View>
                <View key={2} style={{ width: '98%' }}>
                    <View key={`inner_list`} style={{ display: 'flex', flexDirection: 'row', alignSelf: 'stretch' }}>
                        <Canvas style={helperConfig.style.bullet} paint={(painter) => painter.circle(5, 8, 3).fillAndStroke('black', "#fff")} />
                        <View style={{ marginTop: 1.3, flexGrow: 1 }}>
                            <Text>test2</Text>
                        </View>
                    </View>
                </View>
            </View>
        }

        ,

        mapHtmlEntitiesToPdfComponents: (node: any) => {

            log('mapping node', node)

            const key = randomNumber(10000)

            if (node.name === 'div') {
                const divStyle = self.convertHtmlStyleToPdfStyle(node)
                if (self.hasLiveMode(node)) self.toggleLiveMode()
                const contents = <View key={`div_${key}`} style={divStyle}>{node.children.map(self.mapHtmlEntitiesToPdfComponents)}</View>
                if (self.hasLiveMode(node)) self.toggleLiveMode()
                return contents
            }

            else if (node.name === 'p') {

                // When a <p> tag is rendered we suppose that inside there might be the beginning of a block of html.
                //  So we partition the children to divide the text blocks (that should be wrapped in a <Text> tag) and the complex types (that should be wrapped in a <View> tag)
                //  complex type can be bullet lists, divs, images...
                //  we need to divide this because if you try to print multiple texts within a <View> all the texts are printed line by line, not matching the actual html.
                const partitioned = arrayPartition(node.children, [(child) => child.findIndex(n => complexTypes.includes(n.name)), (child) => child.findIndex(n => !complexTypes.includes(n.name))])

                const contents = partitioned.map((part, i) => complexTypes.includes(part[0].name) ?
                    <View key={`part_view_${i}_${key}`}>{part.map(self.mapHtmlEntitiesToPdfComponents)}</View>
                    :
                    <Text key={`part_text_${i}_${key}`}>{part.map(self.mapHtmlEntitiesToPdfComponents)}</Text>)

                return <View key={`p_${key}`} style={self.resolveQuillStyles(node)}>
                            {contents}
                        </View>


            }
            else if (node.name === 'a') {
                return <Text key={`text_${key}`}><Link key={`link_${key}`} src={node.attribs.href} style={{ ...self.resolveQuillStyles(node), ...helperConfig.style.link }}>{node.children[0].data}</Link></Text>
            }
            else if (node.name === 'strong') {

                self.addStyleToAccumulator(helperConfig.style.bold)
                self.addStyleToAccumulator(self.convertHtmlStyleToPdfStyle(node))
                return node.children.map(self.mapHtmlEntitiesToPdfComponents)
            }
            else if (node.name === 'em') {
                self.addStyleToAccumulator(helperConfig.style.italic)
                self.addStyleToAccumulator(self.convertHtmlStyleToPdfStyle(node))
                return node.children.map(self.mapHtmlEntitiesToPdfComponents)
            }
            else if (node.name === 'u') {
                self.addStyleToAccumulator(helperConfig.style.underline)
                self.addStyleToAccumulator(self.convertHtmlStyleToPdfStyle(node))
                return node.children.map(self.mapHtmlEntitiesToPdfComponents)
            }
            else if (node.name === 'h1') {
                return <>
                    <View style={{minHeight: 6}} />
                    <Text key={`h1_${key}`} style={{ ...self.resolveQuillStyles(node), ...helperConfig.style.h1 }}>{node.children.map(self.mapHtmlEntitiesToPdfComponents)}</Text>
                    <View style={{minHeight: 6}} />
                    </>
            }
            else if (node.name === 'h2') {
                return <>
                    <View style={{minHeight: 6}} />
                    <Text key={`h2_${key}`} style={{ ...self.resolveQuillStyles(node), ...helperConfig.style.h2 }}>{node.children.map(self.mapHtmlEntitiesToPdfComponents)}</Text>
                    <View style={{minHeight: 6}} />
                    </>
            }
            else if (node.name === 'h3') {
                return <>
                    <View style={{minHeight: 6}} />
                    <Text key={`h3_${key}`} style={{ ...self.resolveQuillStyles(node), ...helperConfig.style.h3 }}>{node.children.map(self.mapHtmlEntitiesToPdfComponents)}</Text>
                    <View style={{minHeight: 6}} />
                    </>
            }
            else if (node.name === 'br') {
                return <View key={`br_${key}`} >{carriageReturn()}</View>
            }
            else if (node.name === 'ol') {
                return <View key={`ol_${key}`} style={{ ...self.resolveQuillStyles(node), ...helperConfig.style.listOuter, width: '97%' }}>
                    {
                        node.children.map((n, i) => <View key={i} style={{ width: '98%' }}>
                            <View key={`inner_list_${i}`} style={{ display: 'flex', flexDirection: 'row', alignSelf: 'stretch' }}>
                                <Text style={helperConfig.style.bulletNumber}>{`${i + 1}.`}</Text>
                                <View >{self.mapHtmlEntitiesToPdfComponents(n)}</View>
                            </View>
                        </View>)
                    }
                    {/* {carriageReturn()} */}
                </View>
            }
            else if (node.name === 'ul') {
                // const liveModeColour = self.isLiveMode() ? liveDataColour : "black"
                return <View key={`ul_${key}`} style={{ ...self.resolveQuillStyles(node), ...helperConfig.style.listOuter, width: '97%' }}>
                    {
                        /* 
                            Simulate a carriage return that should wrap to the next page to force the first bullet to go to the
                            next page if there is not enough space.
                        */
                    }
                    <View wrap={true} style={{ width: '100%' }}><Text>&#13;</Text></View>
                    {
                        node.children.map((n, i) => <View key={i} style={{ width: '98%' }}>
                            <View key={`inner_list_${i}`} style={{ display: 'flex', flexDirection: 'row' }}>
                                {/* <Canvas style={helperConfig.style.bullet} paint={(painter) => painter.circle(5, 8, 3).fillAndStroke(liveModeColour, "#fff")} /> */}
                                <Text style={{ color: 'blue' , marginRight: 8}}>{'\u2022'}</Text>
                                <View style={{ flexGrow: 1 }}>{self.mapHtmlEntitiesToPdfComponents(n)}</View>
                            </View>
                        </View>)
                    }
                    {/* {carriageReturn()} */}
                </View>
            }
            else if (node.name === 'li') {
                const resolved = <Text style={{ marginRight: '10px' }}>{node.children.map(self.mapHtmlEntitiesToPdfComponents)}</Text>
                return <View key={`li_${key}`} style={helperConfig.style.listInner}>{resolved}</View>
            }
            else if (node.name === 'span') {
                let spanStyle = self.convertHtmlStyleToPdfStyle(node)

                return <Text key={`span_${key}`} wrap style={{ ...self.resolveQuillStyles(node), ...spanStyle }}>{node.children.map(self.mapHtmlEntitiesToPdfComponents)}</Text>
            }
            else if (node.name === 'code') {
                let codeStyle = self.convertHtmlStyleToPdfStyle(node)
                self.addStyleToAccumulator(codeStyle)
                return <Text key={`code_${key}`} style={{ ...self.resolveQuillStyles(node), marginLeft: 3, paddingRight: 3 }}>{node.children.map(self.mapHtmlEntitiesToPdfComponents)}</Text>
            }
            else if (node.name === 'img') {
                return <View key={`img_${key}`} style={helperConfig.style.imageContainer}>
                    <PDFImage source={node.attribs.src} style={{ ...self.resolveQuillStyles(node), objectFit: "fill" }} />
                </View>
            }
            else {
                if (node.data) {
                    const normalizedText = self.normalizeText(node.data)
                    if (normalizedText.trim() === '') return <Text key={`normailized_${randomNumber(8000)}`}>{normalizedText}</Text>
                    const styles = stylesAccumulator.reduce((acc, cur) => ({ ...acc, ...cur }), {})
                    self.resetStyleAccumulator()

                    return <Text key={`text_${key}`} style={{ ...self.resolveQuillStyles(node), ...styles }}>{normalizedText}</Text>
                } else if (node.children && node.children.length > 0) {
                    return <View key={`span_${key}`} >{node.children.map(self.mapHtmlEntitiesToPdfComponents)}</View>
                }
            }
        }

        ,

        convertHtmlStyleToPdfStyle: (node: any) => {

            const formatColor = (color: string, fallBack?: string) => fallBack ? colorFormatter(fallBack).formatColorToHex(color) : colorFormatter().formatColorToHex(color)

            const formatStyle = (style, value) => {

                if (style.trim().toLowerCase() === 'color') {
                    return self.isLiveMode() ? liveDataColour : formatColor(value)
                } else if (style.trim().toLowerCase() === 'background') {
                    return formatColor(value, '#FFFFFF')
                } else if (style.trim().toLowerCase() === 'background-color') {
                    return formatColor(value, '#FFFFFF')
                } else {
                    return value
                }

            }

            const formatStyles = (styles: any) => Object.keys(styles).reduce((acc, cur) => ({ ...acc, [cur]: formatStyle(cur, styles[cur]) }), {})

            const formatClasses = (classes: string) => {
                const classArray = classes.split(' ').map(c => c.trim().toLowerCase())
                const paramStyles = StyleSheet.create(paramValueStyles)

                let styles = {}

                if (classArray.includes('param-value')) {
                    styles = { ...styles, ...paramStyles.param }
                    if (classArray.includes('unresolved')) styles = { ...styles, ...paramStyles.unresolved }
                    if (classArray.includes('error')) styles = { ...styles, ...paramStyles.error }
                }
                if (classArray.includes('livedata')) {
                    styles = { ...styles, ...paramStyles.livedata }
                }
                if (classArray.includes('live')) {
                    styles = { ...styles, ...paramStyles.live }
                }

                return styles;
            }


            let nodeStyles = {}

            if (Object.keys(node.attribs).length > 0) {

                if (node.attribs.style) {

                    // Remap all styles in format "color: rgb(255, 0, 0); background: #AA0044;" to a Map {color: 'rgb(255, 0, 0)', background: '#AA0044'}
                    const styles = node.attribs.style.split(";").filter(n => n.trim() !== '').map(n => n.toLowerCase()).reduce((acc, cur) => ({ ...acc, [cur.substring(0, cur.indexOf(':')).trim()]: cur.substring(cur.indexOf(':') + 1, cur.length).trim() }), {})

                    nodeStyles = { ...nodeStyles, ...formatStyles(styles) }

                }

                if (node.attribs.class) {
                    nodeStyles = { ...nodeStyles, ...formatClasses(node.attribs.class) }
                }

            }

            return nodeStyles

        }
    }

    return self

}


const colorFormatter = (defaultColor: string = '#000000') => {

    const isNumber = (val: string | number) => !Number.isNaN(val)
    const toNumber = (val: string | number) => Number(val)

    const getColorFromComplex = (color: string) => {

        const match = /\(([^)]+)\)/.exec(color)

        return match ? match[1].split(',').map(c => c.trim()) : undefined

    }

    const isValidArrayOfColors = (arr: string[]): boolean => arr.length === 3 && !arr.reduce((acc, cur) => [...acc, isNumber(cur)], [] as boolean[]).some(v => v === false)

    const self = {


        formatColorToHex: (color: string): string => {

            const formattedColor = color.trim().toLocaleLowerCase()

            if (self.isHex(formattedColor)) return color

            const tripletNumbers = getColorFromComplex(color)

            if (!tripletNumbers) return defaultColor

            else if (formattedColor.startsWith('rgba')) return self.rgbaToHex(tripletNumbers)

            else if (formattedColor.startsWith('hsla')) return self.hslaToHex(tripletNumbers)

            else if (formattedColor.startsWith('rgb')) return self.rgbToHex(tripletNumbers)

            else if (formattedColor.startsWith('hsl')) return self.hslToHex(tripletNumbers)

            else return defaultColor
        }

        ,

        isHex: (color: string): boolean => {

            if (color.startsWith('#')) return false

            const rest = color.substring(1);
            if (rest.length !== 6) return false

            const parts = rest.match(/.{1,2}/g) //split the string in colors rgb

            if (!parts) return false

            const isHexNumber = (val: string): boolean => /^[0-9A-Fa-f]{2}$/.test(val)

            return !parts.map(isHexNumber).some(v => v === false)

        }

        ,

        rgbToHex: (color: string[]): string => {

            if (isValidArrayOfColors(color)) {

                let r = Math.round(toNumber(color[0])).toString(16).padStart(2, '0')
                let g = Math.round(toNumber(color[1])).toString(16).padStart(2, '0')
                let b = Math.round(toNumber(color[2])).toString(16).padStart(2, '0')

                return `#${r}${g}${b}`

            }

            return defaultColor

        }

        ,

        hslToHex: (color: string[]): string => {

            if (isValidArrayOfColors(color)) {

                let h = toNumber(color[0])
                let s = toNumber(color[1])
                let l = toNumber(color[2])

                l /= 100;
                const a = s * Math.min(l, 1 - l) / 100;
                const f = n => {
                    const k = (n + h / 30) % 12;
                    const color = l - a * Math.max(Math.min(k - 3, 9 - k, 1), -1);
                    return Math.round(255 * color).toString(16).padStart(2, '0');   // convert to Hex and prefix "0" if needed
                };
                return `#${f(0)}${f(8)}${f(4)}`;

            }

            return defaultColor
        }

        ,

        rgbaToHex: (color: string[]): string => self.rgbToHex(self.removeAlpha(color))

        ,

        hslaToHex: (color: string[]): string => self.hslToHex(self.removeAlpha(color))

        ,

        removeAlpha: (color: string[]): string[] => {
            const noAlphaColor = [...color]
            noAlphaColor.pop()
            return noAlphaColor
        }



    }

    return self

}