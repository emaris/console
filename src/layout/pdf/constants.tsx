import { CSSProperties } from 'react'


export const pdfTextGray = "#484848"
export const basePdfFontSize = 10

export const baseCheckBoxHeight = 16
export const baseCheckBoxWidth = 20

export const liveDataColour = "#1162AD"

export const commonPdfStylesheets: Record<string,CSSProperties> = {

    label: {fontSize: basePdfFontSize, paddingBottom: 3, color: pdfTextGray},
    msg: {fontSize: basePdfFontSize, paddingTop: 3, color: pdfTextGray},
    page: {padding: 20, paddingBottom: 40},
    checkBox: {width: baseCheckBoxWidth, height: baseCheckBoxHeight},
    inline: {display: 'flex', flexDirection: 'row'},
    radioLbl: {fontSize: basePdfFontSize},
    checkBoxLbl: {fontSize: basePdfFontSize, paddingRight: 5},
    selectBoxLbl: {fontSize: basePdfFontSize},
    
    listOuter: {width: '100%', margin: 5, marginLeft: 0},
    listInner: {display: 'flex', flexDirection: 'row', width: '100%', flexWrap: 'wrap'},
    bullet: {width: 18, height: 16, alignSelf: 'stretch', fontSize: '1em'},
    bulletNumber: {
        width: 18, 
        height: 16, 
        fontStyle: 'normal', 
        fontWeight: 400, 
        textDecoration: 'none',
        marginTop: 0.3,
        flexWrap: 'wrap'
    },

    bold: {fontWeight: 600},
    italic: {fontStyle: 'italic'},
    underline: {textDecoration: 'underline'},

    link: {color: 'blue'},
    
    imageContainer: {width: '100%'},

    verticalMultichoice : {display: 'flex', flexDirection: 'column'},
    horizontalMultichoice : {display: 'flex', flexDirection: 'row'},

    verticalMultichoiceMargins : {marginRight: 0},
    horizontalMultichoiceMargins : {marginRight: '25px'},
}

const pageStyles = {

    h1: {fontWeight: 600, fontSize: 24/* , marginTop: 6, marginBottom: 6 */},
    h2: {fontWeight: 600, fontSize: 18/* , marginTop: 6, marginBottom: 6 */},
    h3: {fontWeight: 600, fontSize: 14/* , marginTop: 6, marginBottom: 6 */},

    links: {
        paddingBottom: 15,
        fontSize: basePdfFontSize,
        color: 'blue'
    },
    fileDrop: {
        fontSize: basePdfFontSize,   
        color: 'blue'
    },
    fileDropMissing: {
        paddingBottom: 2,
        fontSize: 9,
        paddingLeft: 15,
        fontStyle: 'italic',
        color: pdfTextGray
    },

    noAnswer: {
        paddingBottom: 2,
        fontSize: 9,
        paddingLeft: 2,
        fontStyle: 'italic',
        color: pdfTextGray
    },

    p: { display: 'flex', flexWrap: 'wrap'}

    ,

    title: {
        marginBottom: 12
    },

    inputBox: { fontSize: basePdfFontSize},
    inputBoxPlaceholder: { fontSize: basePdfFontSize, fontStyle: 'italic', color: pdfTextGray},
    
    numberBox: { fontSize: basePdfFontSize },
    dateBox: { fontSize: basePdfFontSize }
}

const headerStyles:  Record<string,CSSProperties> = {
    h1: {fontWeight: 600, fontSize: 11},
    h2: {fontWeight: 600, fontSize: 10},
    h3: {fontWeight: 600, fontSize: 9},

    header: {
        position:'relative', 
        textAlign: 'left',
        borderBottom: '1px solid #cecece',
        width: '100%',
        fontSize: 8,
        paddingBottom:8,
        marginBottom: 10,
        height: 'auto'
    }
}

const footerStyles :  Record<string,CSSProperties>  = {

    h1: {fontWeight: 600, fontSize: 11},
    h2: {fontWeight: 600, fontSize: 10},
    h3: {fontWeight: 600, fontSize: 9},

    p: {fontSize: 8},

    footerContainer: {
        display: 'flex',
        flexDirection: 'row',
        // justifyContent: 'space-between',
        position:'absolute', 
        borderTop: '1px solid #cecece',
        height: '5%',
        fontStyle: 'italic',
        fontSize: 8,
        color: '#cecece',
        width: '88%',
        bottom: 0,
        marginLeft: 20,
        marginRight: 20
    },

    footerContents: {
        // width: '90%',
        flexGrow: 10,
        marginTop: 10
    },

    footerPages: {
        // right: 10,
        // flexGrow: 1,
        // textAlign: 'end'
        marginTop: 10
    }
}

export const pageContentsPdfStylesheets: Record<string,any> =  {
    ...commonPdfStylesheets,
    ...pageStyles 
}

export const headerPdfStylesheets : Record<string,any> = {
    ...commonPdfStylesheets,
    ...headerStyles
}

export const footerPdfStylesheets: Record<string,any>  ={
    ...commonPdfStylesheets,
    ...footerStyles
} 

export const paramValueStyles : Record<string,any>  = {

    param: {
        // backgroundColor: '#FFEDCC'
    }
    ,
    unresolved: {
        color: '#4D4D4d'
    }
    ,
    error: {
        color: '#FF0000'
    }
    ,
    livedata: {
        color: liveDataColour,
        marginRight: '5px'
    }
    ,
    live: {
        marginRight: '5px'
    }

}