import ReactPDF from '@react-pdf/renderer';
import { PDFProps } from '#layout/components/model';

export type ViewerProps = Exclude<ReactPDF.ViewProps,'children'> & {

    children: (_:PDFProps) => JSX.Element
}


export default function Viewer(props:ViewerProps) {

    const {children, ...rest} = props

    return <ReactPDF.PDFViewer {...rest}>
            {children(ReactPDF)}
        </ReactPDF.PDFViewer>

}