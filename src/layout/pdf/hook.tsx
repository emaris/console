
import { useT } from '#app/intl/api'
import { useToggleBusy } from '#app/system/api'
import { wait } from '#app/utils/common'
import { showFailure } from '#app/utils/feedback'
import { PDFProps } from '#layout/components/model'
import { LayoutAssetContext, LayoutRegistryContext, useLayoutConfig } from '#layout/context'
import { LayoutInstance } from '#layout/model'
import { useCurrentAppState } from '#state'
import { saveAs } from 'file-saver'
import { useContext } from 'react'
import 'react-toastify/dist/ReactToastify.css'
import { useLayoutSettings } from '../settings/api'
import PDFLayout from './layout'
import { useRouteMatch } from 'react-router-dom'
import { Parameter, ParameterSpec } from '#layout/parameters/model'



type UsePdfLayoutProps = {
    layout: LayoutInstance,
    initialiseParameters?: (_: [Parameter, ParameterSpec][]) => Parameter[]
    preview?: boolean, 
    
}

export const usePdfLayout = (props: UsePdfLayoutProps) => {

    const { layout, preview = false, initialiseParameters } = props

    const routeMatch = useRouteMatch()

    const appstate = useCurrentAppState()

    const layoutConfig = useLayoutConfig()
    const settings = useLayoutSettings()
    const layoutRegistry = useContext(LayoutRegistryContext).get()

    const asset = useContext(LayoutAssetContext)
    
    const toggleBusy = useToggleBusy()

    const t = useT()

    const innerProps = {
        appstate, layoutConfig, layoutRegistry, lang: settings.resolveLanguage(), layout, preview, asset, routeMatch, initialiseParameters
    }

    const doc = (props: PDFProps) => <PDFLayout {...props}  {...innerProps} />

    const generatePdf = ({ document = doc }: { document: (props: any ) => JSX.Element }) => {


        return toggleBusy("generate-doc", t("layout.feedback.render_doc"))
            .then(() => import(/* webpackChunkName: "pdf" */ '@react-pdf/renderer'))
            .then(wait(150))
            .then(PDF => PDF.pdf(document(PDF.default)).toBlob())
            .catch(e => {
                showFailure({ message: e.message, details: e.stack })
            })
            .finally(() => toggleBusy("generate-doc"))

    }

    const downloadPdf = ({ fileName, document }: { fileName: string, document: (props: PDFProps) => JSX.Element }) => generatePdf({ document }).then(blob => blob && saveAs(blob, fileName))


    return { document: doc, downloadPdf, generatePdf, changePdfLanguage: settings.changeLayoutLanguage }

}
