import ReactPDF, { Page as PDfPage } from '@react-pdf/renderer'
import React from 'react'

export const Page = PDfPage 

export default Page as unknown as React.FC<ReactPDF.PageProps>