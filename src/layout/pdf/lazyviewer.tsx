import React from 'react'
import { ViewerProps } from './viewer'



export const LazyPdfViewer = (props:ViewerProps) => {

    const PDFViewer = React.lazy(() => import (/* webpackChunkName: "pdf" */'./viewer')) 

    return <React.Suspense fallback={null}>
        <PDFViewer {...props} />
    </React.Suspense>
}