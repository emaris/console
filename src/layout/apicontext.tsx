import { useBytestreams } from '#app/stream/api'
import { useTagModel } from '#app/tag/model'
import { useTenantStore } from '#app/tenant/store'
import { usePartyInstances } from '#campaign/party/api'
import { useProductInstances } from '#campaign/product/api'
import { useRequirementInstances } from '#campaign/requirement/api'
import { SubmissionReactContext } from '#campaign/submission/context'
import { SubmissionContext } from '#campaign/submission/model'
import { useSubmissionLineage } from '#campaign/submission/store'
import { useCurrentDashboard } from '#dashboard/hooks'
import { useProductStore } from '#product/store'
import { useRequirementStore } from '#requirement/store'
import { PropsWithChildren, useContext, useMemo } from 'react'
import { useLayoutComponents } from './components/api'
import { usePageComponent } from './components/page'
import { LayoutApiContext, LayoutContext, LiveLayoutApiContext, useLayoutConfig } from './context'
import { useLayoutParameters } from './parameters/api'
import { useAssetParameter } from './parameters/asset'
import { useLayoutRegistry } from './registry'
import { useLayoutSettings } from './settings/api'
import { useLayoutStyles } from './style/api'


export type LayoutApis = {

    config: ReturnType<typeof useLayoutConfig>
    settings: ReturnType<typeof useLayoutSettings>
    components: ReturnType<typeof useLayoutComponents>
    parameters: ReturnType<typeof useLayoutParameters>
    styles: ReturnType<typeof useLayoutStyles>
    registry: ReturnType<typeof useLayoutRegistry>

    tenantstore: ReturnType<typeof useTenantStore>
    tags: ReturnType<typeof useTagModel>
    reqstore: ReturnType<typeof useRequirementStore>
    prodstore: ReturnType<typeof useProductStore>
    bytestreams: ReturnType<typeof useBytestreams>

    assetParam: ReturnType<typeof useAssetParameter>
    pageComponent:ReturnType<typeof usePageComponent>
  
}

export type LiveLayoutApis = LayoutApis & {
 

    submissionCtx: SubmissionContext
    reqinst: ReturnType<ReturnType<typeof useRequirementInstances>['on']>
    prodinst: ReturnType<ReturnType<typeof useProductInstances>['on']>
    partyinst: ReturnType<ReturnType<typeof usePartyInstances>['on']>
    dashboard: ReturnType<typeof useCurrentDashboard>
    lineage: ReturnType<typeof useSubmissionLineage>


}


export const LayoutApiProvider = (props: PropsWithChildren<{}>) => {

    const config = useLayoutConfig()

    const { children } = props

    return config.mode === 'live' ?

        <ApiContext>
            <LiveApiContext>
                {children}
            </LiveApiContext>
        </ApiContext>

        :

        <ApiContext>
            {children}
        </ApiContext>
}

export const useLayoutApis = () => useContext(LayoutApiContext)

export const useLiveLayoutApis = () => useContext(LiveLayoutApiContext)

// shares a number of apis commonly required on render so as to initialise them only once.
// apis that adapt to context (like useParameters()) can't be shared here.
export const ApiContext = (props: PropsWithChildren<{}>) => {

    const { children } = props

    const { layout } = useContext(LayoutContext)
    const config = useLayoutConfig()
    const tags = useTagModel()
    const components = useLayoutComponents()
    const parameters = useLayoutParameters()

    const settings = useLayoutSettings()
    const styles = useLayoutStyles()
    const registry = useLayoutRegistry()

    const tenantstore = useTenantStore()
    const reqstore = useRequirementStore()
    const prodstore = useProductStore()
    const bytestreams = useBytestreams()


    const assetParam = useAssetParameter()
    const pageComponent = usePageComponent()


    const apis = useMemo(() => {

        return {

            config,
            settings,
            parameters,
            components,
            styles,
            registry,

            pageComponent,
            assetParam,

            tags,
            tenantstore,
            reqstore,
            prodstore,
            bytestreams

        }

    //eslint-disable-next-line  
    }, [layout,settings,config])

    return <LayoutApiContext.Provider value={apis}>
        {children}
    </LayoutApiContext.Provider>

}

// extends the shared api with those required only during live render.
export const LiveApiContext = (props: PropsWithChildren<{}>) => {

    const { children } = props

    const baseapis = useContext(LayoutApiContext)

    const submissionCtx = useContext(SubmissionReactContext)

    const reqinst = useRequirementInstances().on(submissionCtx?.campaign)
    const prodinst = useProductInstances().on(submissionCtx?.campaign)
    const partyinst = usePartyInstances().on(submissionCtx?.campaign)
    const dashboard = useCurrentDashboard()
    const lineage = useSubmissionLineage()


    const apis = useMemo(() => {

       return {

            ...baseapis,

            submissionCtx,

            reqinst,
            prodinst,
            partyinst,

            dashboard,
            lineage

        }

    //eslint-disable-next-line  
    }, [baseapis])

    return <LiveLayoutApiContext.Provider value={apis}>
        {children}
    </LiveLayoutApiContext.Provider>

}

