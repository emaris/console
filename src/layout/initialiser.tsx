
import { noop } from '#app/utils/function'
import { useAlignmentStyle } from '#layout/style/alignment'
import { useBackgroundStyle } from '#layout/style/background'
import { useBorderStyle } from '#layout/style/border'
import { useDiscreteWidthStyle } from '#layout/style/discretewidth'
import { useHeightStyle } from '#layout/style/height'
import { useLineStyle } from '#layout/style/line'
import { useAbsoluteSpacingStyle, useSpacingStyle } from '#layout/style/spacing'
import { useTextStyle } from '#layout/style/text'
import { useWidthStyle } from '#layout/style/width'
import { useRenderGuard } from 'apprise-frontend-core/utils/renderguard'
import { useContext, useEffect, useState } from 'react'
import { usePageComponent } from './components/page'
import { LayoutContext } from './context'
import { LayoutProviderProps } from './provider'
import { useLayoutRegistry } from './registry'
import { useInputWidthStyle } from './style/width'



export const useLayout = () => useContext(LayoutContext).layout




export const LayoutInitialiser = (props: React.PropsWithChildren<LayoutProviderProps>) => {

    const { children } = props

    const [initialised, setInitialised] = useState(false)

    const { content } = useRenderGuard({
        when: initialised,
        render: children,
        orRun: noop,
        andRender: () => <OneShotInitialiser {...props} onInitialised={()=>setInitialised(true)}/>
    })

    return content

}

const OneShotInitialiser = (props: React.PropsWithChildren<LayoutProviderProps & { onInitialised: () => void}>) => {

    const registry = useLayoutRegistry()

    const { useInitialiser, onInitialised } = props

    const initialiseComponents = useInitialiser?.()
    const initStyles = useStyleInitialiser()

    const page = usePageComponent()

    useEffect(() => {

        registry.addComponents(page)

        initialiseComponents?.()
        initStyles?.()

        onInitialised()
        // eslint-disable-next-line
    }, [])

    return null;

}

const useStyleInitialiser = () => {

    const registry = useLayoutRegistry()

    const text = useTextStyle()
    const background = useBackgroundStyle()
    const border = useBorderStyle()
    const alignment = useAlignmentStyle()
    const spacing = useSpacingStyle()
    const absolutespacing = useAbsoluteSpacingStyle()
    const width = useWidthStyle()
    const fieldwidth = useInputWidthStyle()
    const discretewidth = useDiscreteWidthStyle()
    const height = useHeightStyle()
    const line = useLineStyle()

    return () => {


        registry.addStyles([

            text, background, border, alignment, height, width, fieldwidth, discretewidth, spacing, absolutespacing, line
        ])
    }


}