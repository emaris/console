

import { AccordionProvider } from '#app/components/Accordion'
import { NoSuchRoute } from "#app/components/NoSuchRoute"
import { Placeholder } from "#app/components/Placeholder"
import { useT } from '#app/intl/api'
import { useL } from '#app/model/multilang'
import { useToggleBusy } from '#app/system/api'
import { tenantType } from "#app/tenant/constants"
import { useLogged } from '#app/user/store'
import { clock, wait } from "#app/utils/common"
import { showAndThrow } from '#app/utils/feedback'
import { CampaignLoader } from "#campaign/Loader"
import { campaignType } from "#campaign/constants"
import { useEventInstanceStore } from '#campaign/event/store'
import { CurrentCampaignContext, useCurrentCampaign } from "#campaign/hooks"
import { Campaign } from "#campaign/model"
import { PartyInstanceDto, usePartyInstances } from '#campaign/party/api'
import { useCampaignUserPreferences } from '#campaign/preferences/api'
import { useProductInstances } from '#campaign/product/api'
import { useRequirementInstances } from '#campaign/requirement/api'
import { useCampaignDetail, useCampaignStore } from '#campaign/store'
import { TrailKey } from '#campaign/submission/model'
import { useSubmissionStatistics } from '#campaign/submission/statistics/api'
import { TrailSummary } from '#campaign/submission/statistics/trail'
import { useSubmissionStore } from '#campaign/submission/store'
import { homeRoute } from "#home/constants"
import { productType } from "#product/constants"
import { requirementType } from "#requirement/constants"
import { useRenderGuard } from 'apprise-frontend-core/utils/renderguard'
import moment from "moment-timezone"
import * as React from "react"
import { Redirect, Route, Switch } from "react-router-dom"
import { ViewRouter } from "./ViewRouter"
import { useCampaignStatistics, useRoutableCampaigns } from './api'
import { CalendarContext } from './calendar'
import { dashboardRoute } from "./constants"
import { StatisticsContext } from './context'
import { Summaries } from "./hooks"
import { useDashboardStore } from './store'
import "./styles.scss"



export const Dashboard = () => {

    return <CampaignLoader placeholder={Placeholder.page}>
        <CampaignRouter />
    </CampaignLoader>

}

const CampaignRouter = () => {

    const t = useT()

    const campaignstore = useCampaignStore()
    const routable = useRoutableCampaigns()

    return <Switch>

        <Route exact path={homeRoute} render={() => <Redirect to={`${dashboardRoute}`} />} />

        <Route exact path={dashboardRoute} render={() => {

            const defaultCamapaign = routable.getDefault()

            return defaultCamapaign ? <Redirect to={routable.routeTo(defaultCamapaign)} /> : <NoSuchRoute fullMsg={t("dashboard.no_campaign")} />
        }} />



        <Route path={`${dashboardRoute}/:id`} render={({ match: { params } }) => {

            const resolved = campaignstore.lookup(params?.id as string)

            return resolved ? <CampaignDataLoader campaign={resolved} /> : <NoSuchRoute />

        }} />

    </Switch>

}


const CampaignDataLoader = ({ campaign }: { campaign: Campaign }) => {


    const store = useCampaignStore()
    const detail = useCampaignDetail()

    const [fetched, setFetched] = React.useState<Campaign | undefined>(undefined)

    const logged = useLogged()

    const fetchLineage = async () => {
        const shouldLoad = fetched && campaign.lineage && !store.isLoaded(campaign.lineage)

        if (!shouldLoad) 
            return


        try {
            await detail.fetchOneQuietly(store.safeLookup(campaign.lineage), false, false)    // exclude archive
        } catch (e) { 
            // Lineage might not exist for tenant that were not included in the campaign
            console.log(`cannot load lineage for ${logged.tenant}`, e)
        }

    }

    React.useEffect(() => {

        fetchLineage()

        // eslint-disable-next-line
    }, [fetched])

    // after fetch, campaign change could come from push notifications
    React.useEffect(()=> {

        if(campaign.id === fetched?.id)
            setFetched(campaign)

    // eslint-disable-next-line
    },[campaign])


    const { content } = useRenderGuard({

        when: !!fetched && fetched.id === campaign.id,

        // We mount the campaign both for reading and for read/write (e.g for Settings)
        render: () => <CurrentCampaignContext campaign={fetched!}>
            <ContextBuilder />
        </CurrentCampaignContext>,

        // fetch only if it hasn't been already loaded.
        orRun: () => Promise.resolve(store.isLoaded(campaign.id) ? campaign : detail.fetchOne(campaign))
            .then(setFetched)

        ,

        andRender: Placeholder.page

    })

    return content

}

const ContextBuilder = () => {

    const logged = useLogged()
    const campaigns = useCampaignUserPreferences()

    const campaign = useCurrentCampaign()

    // updates user preferences with last visited campaigns

    const newVisit = campaign.id !== logged.details.preferences.lastVisitedCampaign

    const updatePreferences = React.useCallback(() => {

        if (newVisit) {
            console.log("updating user preferences with last visited campaign")
            campaigns.setLastVisitedCampaign(logged, campaign.id)
        }


    }, [newVisit, campaign.id, logged, campaigns])


    React.useEffect(() => updatePreferences(), [updatePreferences])

    const [summaries, computeSummaries] = useSummaries()

    // const {calendars={} as CalendarContextType} = useCalendarContext() ?? {}

    // const generalCalendar = React.useMemo(() => calendars.general, [calendars])
    // const partyCalendar = React.useMemo(() => calendars.party, [calendars])
    // const assetCalendar = React.useMemo(() => calendars.asset, [calendars])

    const { content } = useRenderGuard({

        when: !!summaries,

        render: () => <StatisticsContext.Provider value={summaries}>
            <CalendarContext >
                <AccordionProvider>
                    <ViewRouter />
                </AccordionProvider>
            </CalendarContext>
        </StatisticsContext.Provider >,

        orRun: computeSummaries,


        andRender: Placeholder.page

    })

    return content

}


const useSummaries = () => {

    const t = useT()
    const l = useL()

    const toggleBusy = useToggleBusy()

    const campaign = useCurrentCampaign()

    const now = moment(Date.now())

    const campaignstore = useCampaignStore()
    const routables = useRoutableCampaigns()
    const campaignstatistics = useCampaignStatistics()
    const dashboard = useDashboardStore()

    const allCampaigns = campaignstore.all()
    const allInstances = campaignstore.allInstances()


    // builds a cache of expensively derived data.

    const substats = useSubmissionStatistics().on(campaign)

    // at the moment we might not need to recompute stats when the deps below change, because changes require navigating away
    // from the dashboard and this cache, so that when we get back here stats are recomputed on mount.

    // yet we may effect some changes from the dashboard now or in the future (eg settings). other changes may also come
    // from server pushes whilst we remain in this component tree. so it's safer to stay in sycn with these deps

    const eventinststore = useEventInstanceStore().on(campaign)
    const events = eventinststore.all()
    const parties = usePartyInstances().on(campaign).all()
    const requirements = useRequirementInstances().on(campaign).all()
    const products = useProductInstances().on(campaign).all()
    const trails = useSubmissionStore().on(campaign).allTrails()

    const deps = [campaign, events, parties, requirements, products, trails] as const

    type ExtendedSummaries = Summaries & {
        partyTrails: [PartyInstanceDto, TrailSummary[]][]
    }

    const [summaries, setSummaries] = React.useState<ExtendedSummaries>(undefined!)

    const horizon = dashboard.submissionHorizon(campaign)

    const computeSummaries = async () => {

        const dueDates = { ...eventinststore.allDueDatesByRequirementId() ?? [], ...eventinststore.allDueDatesByProductId() ?? [] }

        const now = moment(new Date())

        const [allTrailSummaries, summaryMap] = clock('trail summaries', () => {

            const summaries = {} as Record<string, TrailSummary[]>

            const summaryMap = {} as Record<string, TrailSummary>


            trails?.forEach(trail => {

                const summary = substats.trailSummaryNoDeps({ now: now.toDate(), trail, due: dueDates[trail.key.asset] })

                const assetSummaries = summaries[trail.key.asset]
                if (assetSummaries) assetSummaries.push(summary)
                else summaries[trail.key.asset] = [summary]

                const partySummaries = summaries[trail.key.party]
                if (partySummaries) partySummaries.push(summary)
                else summaries[trail.key.party] = [summary]

                summaryMap[trail.id] = summary


            })

            return [summaries, summaryMap]
        })


        const campaignstats = clock(`summary campaign stats`, () => campaignstatistics.get(campaign))

        const requirementstats = clock(`requirement stats`, () => substats.allRequirementsSummary(allTrailSummaries, dueDates))

        const productstats = clock(`product stats`, () => substats.allProductsSummary(allTrailSummaries, dueDates))

        //eslint-disable-next-line
        const partyTrails = substats.partiesWithTrailSummaries(allTrailSummaries)

        const horizonToEndOfCampaign = now.add(campaignstats.days - campaignstats.progress, 'days')

        // party stats to end of campaign (horizon-independent).
        const staticPartystats = clock("static party stats", () => substats.allPartiesSummary(partyTrails, horizonToEndOfCampaign))

        const trailSummaryOf = (key: TrailKey) => allTrailSummaries[key.asset]?.find(s => s.trail.key.party === key.party) ?? substats.trailSummary(key, dueDates[key.asset])

        return {

            dueDates,
            trails: allTrailSummaries,
            trailSummaryMap: summaryMap,
            trailSummaryOf,

            [campaignType]: campaignstats,
            [requirementType]: requirementstats,
            [productType]: productstats,
            partyTrails,
            staticparties: staticPartystats,
            ...computePartyStats(partyTrails),
            ...computeStartedCampaigns()
        }

    }

    const computePartyStats = (partyTrails: ExtendedSummaries['partyTrails']) => {

        const stats = clock("dynamic party stats", () => substats.allPartiesSummary(partyTrails, moment(now).add(horizon, 'days')))

        return { [tenantType]: stats }

    }


    const computeStartedCampaigns = () => ({ startedCampaigns: routables.all() })

    const computeAndSetSummaries = () => Promise.resolve().then(wait(150)).then(computeSummaries).then(computed => setSummaries(s => ({ ...s, ...computed })))

    React.useEffect(() => {

        if (summaries)
            computeAndSetSummaries()

        //eslint-disable-next-line
    }, deps)

    React.useEffect(() => {

        if (summaries)
            setSummaries(s => ({ ...s, ...computePartyStats(summaries.partyTrails) }))

        //eslint-disable-next-line
    }, [horizon])


    React.useEffect(() => {

        if (summaries)
            setSummaries(s => ({ ...s, ...computeStartedCampaigns() }))

        //eslint-disable-next-line
    }, [allCampaigns, allInstances])



    // const {  } = useFeedback()

    const computeTask = () => toggleBusy(`computestats`, t("dashboard.statistics.generating"))

        .then(() => computeAndSetSummaries().then(() => console.log("done with stats.")))
        .catch(e => showAndThrow(e, t("dashboard.statistics.generating_error", { campaign: l(campaign.name) })))
        .finally(() => toggleBusy(`computestats`))

    return [summaries, computeTask] as const
}
