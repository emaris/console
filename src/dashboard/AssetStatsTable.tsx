
import { Column, TableProps, VirtualTable } from "#app/components/VirtualTable"
import { useT } from '#app/intl/api'
import { useLocale } from "#app/model/hooks"
import { useTagHolderFilter } from "#app/tag/filter"
import { tenantType } from "#app/tenant/constants"
import { useTime } from "#app/time/api"
import { compareDates, compareNumbers } from "#app/utils/common"
import { useEventInstanceDates } from '#campaign/event/store'
import { useCurrentCampaign } from "#campaign/hooks"
import { CampaignInstance, useCampaignModel } from "#campaign/model"
import { SubmissionLabel } from "#campaign/submission/Label"
import { SubmissionStub } from "#campaign/submission/model"
import { useSubmissionProfile } from '#campaign/submission/profile'
import { AssetSummary } from "#campaign/submission/statistics/api"
import { useSubmissionStore } from '#campaign/submission/store'
import { productType } from "#product/constants"
import { requirementType } from "#requirement/constants"
import * as React from "react"
import { useHistory } from "react-router-dom"
import { useDueDateFilter } from './SubmissionFilter'
import { AssessmentStatsBar, ComplianceStatsBar, SubmissionStatsBar } from "./SummaryStatsBars"
import { useCurrentDashboard } from "./hooks"


export type Row = { instance: CampaignInstance, summary: AssetSummary }

export type Props = Partial<Omit<TableProps<Row>, 'data'>> & {

    type?: string
    mode?: 'full' | 'summary'
    scope?: 'past' | 'future'
    route: (_: CampaignInstance) => string
    data: CampaignInstance[]
}


export const AssetStatsTable = (props: Props) => {

    const t = useT()
    const { l } = useLocale()

    const history = useHistory()

    const { data, route, scope, type, mode = 'full', ...rest } = props

    const time = useTime()
    const campaignmodel = useCampaignModel()

    const dashboard = useCurrentDashboard()

    const now = time.current()

    const campaign = useCurrentCampaign()

    const {  duedateComparator } = useEventInstanceDates().on(campaign)

    const subprofile = useSubmissionProfile().on(campaign)
    const substore = useSubmissionStore().on(campaign)

    // prepares profiles to avoid hidden costs in a tight loop. 
    const profiles = {

        [requirementType]: subprofile.profileOf(requirementType),
        [productType]: subprofile.profileOf(productType)

    }


    // helper
    const p = (a: CampaignInstance) => profiles[a.instanceType]

    // helpers
    const partyRoute = (a: CampaignInstance) => (sub: SubmissionStub) => {
        const trail = substore.lookupTrail(sub.trail)!
        return () => `${route(a)}/${tenantType}/${dashboard.partyParam(trail.key.party)}`
    }

    // not expecting singificant changes to data during the lifecycle of this table.
    const assetStatsGroup = `${campaign.id}-${type}-stats`

    const summaries = type ? dashboard.summaries[type] : { ...dashboard.summaries[requirementType], ...dashboard.summaries[productType] }

 
    // not expecting changes to data during the lifecycle of this table.
    // eslint-disable-next-line
    const rows = React.useMemo(() => data.filter(a => summaries[a.source]).map(a => ({ id: a.source, instance: a, summary: summaries[a.source] })), [data,scope])

    const { Filter: DueDateFilter, filteredData: filteredByDueDate } = useDueDateFilter({

        initiallySelected: scope === 'past' ? ['due'] : scope === 'future' ? ['next'] : undefined,
        rows,
        dueDate: row => row.summary.dueDate,
        dueFilter: scope === 'future' ? row => profiles[row.instance.instanceType].isDue(row.instance) : undefined,
        group: assetStatsGroup,
        key: 'duedate'
    })

    const initialData = scope ? filteredByDueDate : data

    const { TagFilter: AudienceFilter, tagFilteredData: audienceFilteredData } = useTagHolderFilter({
        filtered: filteredByDueDate,
        tagged: filteredByDueDate,
        tagsOf: t => t.instance.audience?.terms.flatMap(t => t.tags),
        placeholder: t("common.fields.audience.name"),
        group: assetStatsGroup,
        key: `audience`
    })

    const { TagFilter: UserProfileFilter, tagFilteredData: userProfileFilteredData } = useTagHolderFilter({
        filtered: audienceFilteredData,
        tagged: filteredByDueDate,
        tagsOf: t => t.instance.userProfile?.terms.flatMap(t => t.tags),
        placeholder: t("common.fields.user_profile.name"),
        group: assetStatsGroup,
        key: `profile`
    })

    const filteredData = userProfileFilteredData


    const columns = mode === 'full' ?
        ['asset', 'duedate', 'submissionrate', 'assessmentrate', 'compliancerate', 'lastsubmission', 'lastedit'] :
        scope === 'past' ?
            ['asset', 'duedate', 'submissionrate', 'assessmentrate', 'compliancerate'] :
            ['asset', 'duedate', 'submissionrate', 'lastsubmission']


    const filters = mode === 'full' ? scope ? [UserProfileFilter, AudienceFilter] : [DueDateFilter, UserProfileFilter, AudienceFilter] : []

    return <VirtualTable<Row> selectable={false}
        filtered={mode === 'full' || initialData.length > 5}
        filterGroup={assetStatsGroup}
        filters={filters}
        data={filteredData}
        total={initialData.length}
        filterWith={(f, i) => p(i.instance).stringify(i.instance).toLowerCase().includes(f.toLowerCase())}
        onDoubleClick={r => history.push(route(r.instance))}
        {...rest} >


        {columns.includes('asset') && <Column<Row> width={250} dataKey="asset"
            title={t("common.fields.name_multi.name")}
            flexGrow={1}
            dataGetter={r => l(p(r.instance).source(r.instance.source)?.name)}
            cellRenderer={({ rowData: a }) => p(a.instance).label(a.instance, { noDecorations: true, linkTo: route(a.instance) })} />}


        {columns.includes('duedate') && <Column<Row> width={150} dataKey="duedate"

            title={t("dashboard.labels.due_date.title")}
            headerTooltip={t("dashboard.labels.due_date.asset_tip")}

            comparator={duedateComparator}

            dataGetter={r => p(r.instance).deadlineOf(r.instance)}
            cellRenderer={({ rowData: r }) => p(r.instance).label(r.instance, { deadlineOnly: true, accentScheme: "weeks" })} />}


        {columns.includes('submissionrate') && <Column<Row> flexGrow={1} dataKey="submissionrate"

            title={t("dashboard.labels.submission_rate.title")}
            headerTooltip={t("dashboard.labels.submission_rate.asset_tip")}

            dataGetter={r => r.summary.submissionProfile.rate}
            cellRenderer={({ rowData: r }) => <SubmissionStatsBar className={r.summary.dueDate && now.isAfter(new Date(r.summary.dueDate)) ? 'submissions-due' : undefined} range='party' route={partyRoute(r.instance)} stats={r.summary} />} />}

        {columns.includes('assessmentrate') && <Column<Row> flexGrow={1} dataKey="assessmentrate"

            title={t("dashboard.labels.assessment_rate.title")}
            headerTooltip={t("dashboard.labels.assessment_rate.asset_tip")}
            comparator={(r1,r2) => compareNumbers(r2,r1)}
            dataGetter={r => r.summary.assessmentProfile.rate}
            cellRenderer={({ rowData: r }) => <AssessmentStatsBar className={r.summary.dueDate && now.isAfter(new Date(r.summary.dueDate)) ? 'submissions-due' : undefined} range='party' route={partyRoute(r.instance)} stats={r.summary} />} />}

        {campaignmodel.currentComplianceScale(campaign) && columns.includes('compliancerate') &&

            <Column<Row> flexGrow={1} dataKey="compliancerate"

                title={t("dashboard.labels.compliance_rate.title")}
                headerTooltip={t("dashboard.labels.compliance_rate.asset_tip")}
                comparator={(r1,r2) => compareNumbers(r2,r1)}
                dataGetter={r => r.summary.complianceProfile.rate}
                cellRenderer={({ rowData: r }) => <ComplianceStatsBar range='party' route={partyRoute(r.instance)} stats={r.summary} />} />

        }

        {columns.includes('lastsubmission') && <Column<Row> width={150} dataKey="latestsubmission"

            title={t("dashboard.labels.submitted.aggregated_title")}
            headerTooltip={t("dashboard.labels.submitted.assets_tip")}

            comparator={[(d1, d2) => compareDates(d1, d2, false), (d1, d2) => compareDates(d1, d2, true)]}

            dataGetter={r => r.summary.lastSubmitted?.lifecycle.lastSubmitted}
            cellRenderer={({ rowData: r }) =>
                r.summary.lastSubmitted && r.summary.lastSubmitted && r.summary.lastSubmitted.lifecycle.state !== 'missing' && <SubmissionLabel tipMode='party' linkTo={partyRoute(r.instance)(r.summary.lastSubmitted)} displayMode='date-only' submission={r.summary.lastSubmitted} />} />
        }

        {columns.includes('lastedit') && <Column<Row> width={150} dataKey="latestedit"

            title={t("dashboard.labels.last_edit.aggregated_title")}
            headerTooltip={t("dashboard.labels.last_edit.assets_tip")}

            comparator={[(d1, d2) => compareDates(d1, d2, false), (d1, d2) => compareDates(d1, d2, true)]}

            dataGetter={r => r.summary.last?.lifecycle.lastEdited ?? r.summary.last?.lifecycle.lastModified}
            cellRenderer={({ rowData: r }) =>
                r.summary.last && <SubmissionLabel tipMode='party' linkTo={partyRoute(r.instance)(r.summary.last)} displayMode='date-change' submission={r.summary.last} noPseudoNames />} />

        }


    </VirtualTable>

}



