import { tenantType } from "#app/tenant/constants";
import { Campaign, CampaignInstance, useCampaignStatus } from "#campaign/model";
import { PartyInstance, usePartyInstances } from "#campaign/party/api";
import { useCampaignUserPreferences } from '#campaign/preferences/api';
import { useProductInstances } from '#campaign/product/api';
import { useRequirementInstances } from '#campaign/requirement/api';
import { SubmissionStub, Trail, TrailDto } from "#campaign/submission/model";
import { useSubmissionProfile } from '#campaign/submission/profile';
import { useTrailLookups } from "#campaign/submission/store";
import moment from "moment-timezone";
import { dashboardRoute } from "./constants";
import { useDashboardStore } from './store';
import { roundPercent } from '#campaign/submission/statistics/api';



export const useRoutableCampaigns = () => {

    const status = useCampaignStatus()
    const preferences = useCampaignUserPreferences()

    const self = {

        all: () => status.allStarted()

        ,

        routeTo: (c: Campaign) => `${dashboardRoute}/${c?.id}`

        ,

        getDefault: () => {

            return self.all().find(c => c.id === preferences.lastVisitedCampaign())
                ?? self.all().find(status.isRunning)
                ?? self.all()[0]
        }
    }

    return self

}


export const useCampaignStatistics = () => {

    const status = useCampaignStatus()
    const reqinst = useRequirementInstances()
    const prodinst = useProductInstances()


    return {

        get: (c: Campaign, party?: PartyInstance) => {

            const now = Date.now()

            const start = status.startDate(c)
            const started = moment(start).isBefore(now)

            const end = status.endDate(c)
            const ended = end && moment(end).isBefore(now)

            const days = Math.max(0, (end ? moment(end) : moment(start).add(1.5, 'year')).diff(start, 'days'))
            const progress = Math.min(days, moment(now).diff(start, 'days'))
            const progressRate = ended ? 100 : roundPercent(progress * 100, Math.max(1, days))

            const requirements = reqinst.on(c)
            const requirementPastDue = requirements.pastDue().length

            const products = prodinst.on(c)
            const productsPastDue = products.pastDue().length

            const totalRequirements = (party ? requirements.allForParty(party, false) : requirements.all()).length

            const totalProducts = (party ? products.allForParty(party, false) : products.all()).length

            return {

                start,
                started,
                end,
                ended,

                days,
                progress,
                progressRate,

                totalRequirements,
                totalProducts,

                requirementPastDue,
                productsPastDue

            }

        }

    }

}



export const useDashboard = () => {

    const profile = useSubmissionProfile()
    const partyinst = usePartyInstances()
    const substore = useTrailLookups()
    const dashboard = useDashboardStore()


    const self = {


        routeTo: (c: Campaign) => self.on(c).routeToCampaign()

        ,

        on: (c: Campaign) => {


            const self = {


                ...dashboard

                ,

                resolveParty: partyinst.on(c).lookupBySource

                ,

                resolveAsset: (type: string, source: string) => profile.on(c).profileOf(type).instance(source)

                ,

                routeTo: (c: Campaign) => `${dashboardRoute}/${c?.id}`

                ,


                preserveRouteTo: (c: Campaign) => {

                    const innerrouteRegexp = new RegExp(`(${dashboardRoute}/(.+?))(/|$)`.replace("/", "\\/"))

                    const { pathname, search } = window.location

                    const routeMatch = pathname.match(innerrouteRegexp)?.[1]
                    const campaignMatch = pathname.match(innerrouteRegexp)?.[2]

                    const linkname = c.id

                    return routeMatch ? `${routeMatch.replace(campaignMatch!, linkname!)}${search}` : self.routeTo(c)



                }

                ,

                routeToCampaign: () => self.routeTo(c)

                ,

                partyParam: (source: string) => {

                    const param = partyinst.on(c).lookupSource(source)?.id

                    return param ? encodeURIComponent(param) : source
                }

                ,

                routeToParty: (instance: PartyInstance | string) => `${self.routeToCampaign()}/${tenantType}/${self.partyParam(typeof instance === 'string' ? instance : instance.source)}`

                ,

                assetParam: (type: string, source: string) => {

                    const param = profile.on(c).profileOf(type).source(source).id

                    return param ?? source
                }

                ,

                routeToAsset: (instance: CampaignInstance) => `${self.routeToCampaign()}/${instance.instanceType}/${self.assetParam(instance.instanceType, instance.source)}`

                ,

                routeToAssetWith: (type: string, source: string) => `${self.routeToCampaign()}/${type}/${self.assetParam(type, source)}`

                ,

                assetRouteToSubmission: (submission: SubmissionStub) => {

                    const { key: { assetType, asset, party } } = substore.on(c).lookupTrail(submission.trail) ?? {} as Trail

                    return `${self.routeToCampaign()}/${assetType}/${self.assetParam(assetType, asset)}/${tenantType}/${self.partyParam(party)}`

                }

                ,

                assetRouteToSubmissionWith: (type: string, source: string, party: string) => {

                    return `${self.routeToCampaign()}/${type}/${self.assetParam(type, source)}/${tenantType}/${self.partyParam(party)}`

                }

                ,

                partyRouteToSubmission: (submission: SubmissionStub) => {

                    const trail = substore.on(c).lookupTrail(submission.trail) ?? {} as Trail

                    return self.partyRouteToSubmissionWithTrail(submission, trail)

                }

                ,

                partyRouteToSubmissionWith: (type: string, source: string, party: string) => {


                    return `${self.routeToCampaign()}/${tenantType}/${self.partyParam(party)}/${type}/${self.assetParam(type, source)}`

                }


                ,

                partyRouteToSubmissionWithTrail: (submission: SubmissionStub, trail: TrailDto) => {

                    const campaignRoute = self.routeToCampaign()
                    const partyParam = self.partyParam(trail.key.party)
                    const assetParam = self.assetParam(trail.key.assetType, trail.key.asset)

                    return `${campaignRoute}/${tenantType}/${partyParam}/${trail.key.assetType}/${assetParam}/${submission.id}`

                }

            }

            return self

        }


    }

    return self
}


