
import { NoSuchRoute } from '#app/components/NoSuchRoute'
import { Placeholder } from '#app/components/Placeholder'
import { parentIn } from '#app/utils/routes'
import { useCurrentCampaign } from '#campaign/hooks'
import { AssetInstance } from '#campaign/model'
import { PartyInstance } from '#campaign/party/api'
import { SubmissionLoader } from '#campaign/submission/Loader'
import { useRevisions } from '#campaign/submission/revision'
import { useTrailLookups } from '#campaign/submission/store'
import { useCurrentAsset, useCurrentParty } from '#dashboard/hooks'
import { ProductDetailLoader } from '#product/DetailLoader'
import { RequirementDetailLoader } from '#requirement/DetailLoader'
import { requirementType } from '#requirement/constants'
import * as React from 'react'
import { Redirect, useLocation } from 'react-router-dom'
import { SubmissionDetail } from '../campaign/submission/Detail'
import { latestSubmission } from "../campaign/submission/constants"
import { TrailKey, newSubmission, newTrail } from '../campaign/submission/model'
import { Tenant } from '#app/tenant/model'

export type Props = {

    submission: string

}

// resolves a submission from the context and pushes it down.
// (split into outer and inner component or we'd have to use hooks conditionally.)
export const SubmissionRouter = (props: Props) => {

    const { submission: id } = props

    const { party, tenant } = useCurrentParty()
    const { asset } = useCurrentAsset()

    // remounts if asset type changes.
    // this cover cases wheen we move from report to linked requirements
    return id && asset && party ? <InnerSubmissionRouter key={asset.instanceType} {...props} party={party} tenant={tenant} asset={asset} /> : <NoSuchRoute />
}


export type InnerProps = {

    submission: string
    party: PartyInstance
    tenant: Tenant
    asset: AssetInstance


}

export const InnerSubmissionRouter = (props: InnerProps) => {

    const { submission: id, party,tenant, asset } = props

    const campaign = useCurrentCampaign()
    const trails = useTrailLookups().on(campaign)

    const { pathname, search } = useLocation()

    // buidls a key for the trail in context.
    const key = { campaign: campaign.id, party: party?.source, asset: asset?.source, assetType: asset?.instanceType } as TrailKey

    // resolves trail from the key, or creates a new one with it.
    let trail = trails.lookupTrailKey(key) ?? newTrail(key)

    const revisions = useRevisions().on(trail)

    // stabilises new submission in case we need to work with the very first of an empty trail.
    const newSub = React.useRef(newSubmission(trail?.id, asset)) 

    const latest = revisions.latest()

    if (id === latest?.id)
        return <Redirect to={`${parentIn(pathname)}/${latestSubmission}${search}`} />
   
    // resolves route to submission.
    const submission = id === latestSubmission ? (latest ?? newSub.current) : trail?.submissions?.find(s => s.id === id)

    // if we had an id and still couldn't resolve it.
    if (!submission)
        return <NoSuchRoute msg={id} />


    // at this point we have 1) an existing trail with an existing or new submission, or 2) a new trail with a new submission.

    const AssetLoader = trail.key.assetType === requirementType ? RequirementDetailLoader : ProductDetailLoader



    return <AssetLoader id={trail.key.asset} placeholder={Placeholder.page}  >{
        () =>

            // note: when we discard the latest submission we need to load a new submission.  
            // but the route remains @ 'latest' but  and the router doesn't get remounted.
            // so we force-unmount the loader instead, using the submission id as the key.
            // note: we also add the creation date to the key, as removing a draft may reuse the same newSub starting point
            // and still we want to resync all data with the convenience of a key.
            <SubmissionLoader key={`${submission.id}-${submission.lifecycle.created}`} trail={trail} submission={submission} campaign={campaign}>

                {(loaded, related, onReload) => <SubmissionDetail

                    submission={loaded}
                    relatedSubmissions={related}
                    trail={trail}
                    campaign={campaign}
                    asset={asset}
                    tenant={tenant}
                    party={party}
                    onReload={onReload} />}

            </SubmissionLoader>

    }</AssetLoader>
}