import { Tooltip } from 'antd'
import { useListState } from '#app/components/hooks'
import { Label } from '#app/components/Label'
import { Column, TableProps, VirtualTable } from '#app/components/VirtualTable'
import { icns } from '#app/icons'
import { tenantType } from '#app/tenant/constants'
import { TenantDto } from '#app/tenant/model'
import { useTime } from '#app/time/api'
import { TimeLabel } from '#app/time/Label'
import { compareDates } from '#app/utils/common'
import { useEventInstanceStore } from '#campaign/event/store'
import { useCurrentCampaign } from '#campaign/hooks'
import { eventPlural, eventType } from '#event/constants'
import { productType } from '#product/constants'
import { ProductDto } from '#product/model'
import { requirementType } from '#requirement/constants'
import { RequirementDto } from '#requirement/model'
import moment from 'moment-timezone'
import * as React from 'react'
import { useT } from '#app/intl/api'

export type Group = {
    key: any,
    group: CalendarRow[]
}
export type CalendarRow = {
    id: string
    label: JSX.Element
    date: moment.Moment
    type: string
    stringify: (f: string, i: CalendarRow) => boolean
    filter: (selection: { [requirementType]: string[], [eventType]: string[], [productType]: string[], [tenantType]: string[], 'action' : string[] }) => boolean
    party?: TenantDto
    target?: ProductDto | RequirementDto
    targetType?: typeof requirementType | typeof productType
}

export type GroupedRow = CalendarRow | {
    id: any;
    title: any;
}


export type CalendarTableProps = TableProps<any> & {
    startingRow?: number
    flexGrowFirstColumn?: boolean
    firstColumnWidth?: number
    dateFormat?: string
}

export const CalendarTable = (props: CalendarTableProps) => {
    
    const t = useT()

    const { data, startingRow, firstColumnWidth=200, dateFormat=t("time.default_format_short") /*"MMMM DD, dddd"*/, ...rest } = props

    const time = useTime()
    const campaign = useCurrentCampaign()
    const campaignTimeZone = campaign.properties.timeZone ? {'original' : campaign.properties.timeZone} : undefined
    const api = useEventInstanceStore().on(campaign)
    const { absolute } = api
    const plural = t(eventPlural)
    const liststate = useListState<CalendarRow>()

    return <VirtualTable<CalendarRow> data={data} state={liststate} rowKey={'id'}
        filterWith={(f, i) => (i as any).title || i.stringify(f, i)}
        filterPlaceholder={t("common.components.table.filter_placeholder", { plural: plural.toLowerCase() })}
        customProperties={({ rowData }) => ({ title: (rowData as any).title })}
        customRowRenderer={InstanceRow}
        selectable={false}
        scrollToRow={startingRow}
        countFilter={t=> (t && t.date) ? true : false}
        {...rest}>

        <Column<CalendarRow> 
            sortable={false} 
            width={firstColumnWidth} 
            title={t("common.fields.date.name")} 
            comparator={compareDates} 
            dataKey="t.date" 
            dataGetter={t => t.date} 
            refreshOn={data}
            cellRenderer={({ rowData: e, rowIndex: i }) => {
                
                    if (e.date) {
                        var format = time.preferredFormatFor(e.date,campaignTimeZone)

                        //We are showing on the table absolute date (probably UTC) rather than localized... NOT THE HOVERED ONE BUT THE ONE ON THE TABLE
                        const sameDate = i > 0 && data[i-1].date &&  e.date.tz(format.zone).isSame(moment(new Date(absolute(data[i-1].date)!)).tz(format.zone), 'date')
                        
                        const date = <span className={`day ${sameDate?'same-day':''}`}>{e.date.locale(moment.locale()).tz(format.zone).format(dateFormat)}</span>
                        const tooltip = <Tooltip title={() => <TimeLabel icon={icns.calendar} value={e.date} timezones={campaignTimeZone} />}>
                            {date}
                        </Tooltip>
                        
                        return tooltip
                    }
                }
                
            }/>

        <Column<CalendarRow> sortable={false} flexGrow={1} title={t("common.fields.name_multi.name")} dataKey="t.label" 
                        dataGetter={e => e.label} 
                        cellRenderer={row => row.rowData.label} />
    
    </VirtualTable>

}

const InstanceRow = props => {

    const { title, ...rest } = props
    const t = useT()

    if (!title)
        return <div {...rest} />

    return <div {...rest} className={`${rest.className} event-table-title`}>
        <Label icon={title.icon} title={t(title.name)} />
    </div>

}
