import { Divider, Row } from 'antd'
import { Label } from '#app/components/Label'
import { Text } from "#app/components/Typography"
import { tenantIcon, tenantPlural, tenantType } from "#app/tenant/constants"
import { complianceIcon, complianceType } from '#campaign/constants'
import { CampaignInstance } from '#campaign/model'
import { PartyInstance } from '#campaign/party/api'
import { performanceGroups, submissionIcon } from "#campaign/submission/constants"
import { AggregateSummaries } from '#campaign/submission/statistics/aggregateMany'
import { AssetSummary } from '#campaign/submission/statistics/api'
import { PartySummary } from '#campaign/submission/statistics/api'
import { productIcon, productPlural, productSingular, productType } from "#product/constants"
import { requirementIcon, requirementPlural, requirementSingular, requirementType } from "#requirement/constants"
import { useT } from '#app/intl/api'
import { useCurrentAsset, useCurrentParty, useCurrentDashboard } from "./hooks"
import { PerformanceCard, StatCol } from "./SummaryCard"


// compliance rates, classes, and stats for parties, assets, and across both. 

// can be based only on submissions and assessments available.
// an assessment gives the same contribution whether it is about an asset due 6 months ago, tomorrow, or in 6 months.
// so we never need a horizon to limit which assets and submissions we should look at, whichever the view or the type.

type Props = {

    active: boolean
}


export const ComplianceCard = (props: Props) => {

    const { active } = props

    const { asset } = useCurrentAsset()
    const { party } = useCurrentParty()

    return active ? asset ? <AssetCard asset={asset} /> : party ? <PartyCard party={party} /> : <AllCard /> : null

}




const AllCard = () => {

    const t = useT()

    const dashboard = useCurrentDashboard()


    // assessment stats: piece together requirement and product stats, no need to look at parties.
    const reqstats = dashboard.summaries[requirementType]
    const prodstats = dashboard.summaries[productType]
    const partystats = dashboard.summaries.staticparties

    // adapts PerformanceCard to compliance.
    const selector = (type: string) => {


        // note: if we're aggregating by party, we use a horizon that covers the entire campaign, ie. includes all assets.
        const summaries: AggregateSummaries = type === tenantType ? partystats : dashboard.summaries[type]

        return {
            tips: {
                1: t(`dashboard.summary.performance.tips.compliance_other.${type}`, { range: performanceGroups.multicompliance[1].label }),
                2: t(`dashboard.summary.performance.tips.compliance_other.${type}`, { range: performanceGroups.multicompliance[2].label }),
                3: t(`dashboard.summary.performance.tips.compliance_other.${type}`, { range: performanceGroups.multicompliance[3].label }),
                4: t(`dashboard.summary.performance.tips.compliance_best.${type}`),

                overall: t(`dashboard.summary.performance.tips.overall.compliance.${type}`)
            },

            statstype: 'multicompliance' as const,
            groups: summaries.complianceGroups,
            performanceRate: summaries.singleComplianceRate,
            performanceClass: summaries.singleComplianceClass,
        }
    }


    return <PerformanceCard noHorizon selector={selector}>

        <Divider orientation='left'><Label icon={submissionIcon} title={t("dashboard.summary.performance.compliance.assessments")} /></Divider>

        <Row >

            <StatCol tip={t('dashboard.summary.performance.compliance.req_assessed_tip')} title={t(requirementPlural)}>
                <Label icon={requirementIcon} title={<Text>{reqstats.assessed}/<Text secondary>{reqstats.assessable}</Text></Text>} />
            </StatCol>

            <StatCol tip={t('dashboard.summary.performance.compliance.prod_assessed_tip')} title={t(productPlural)}>
                <Label icon={productIcon} title={<Text>{prodstats.assessed}/<Text secondary>{prodstats.assessable}</Text></Text>} />
            </StatCol>

            <StatCol tip={t('dashboard.summary.performance.compliance.rate_tip')} title={t("dashboard.summary.performance.compliance.rate")}>
                <Label icon={complianceIcon} title={<Text className={partystats.singleAssessmentClass}>{isNaN(partystats.singleAssessmentRate) ? "--" : `${partystats.singleAssessmentRate}%`}</Text>} />
            </StatCol>

        </Row>

    </PerformanceCard>

}


const PartyCard = (props: { party: PartyInstance }) => {

    const t = useT()

    const { party } = props

    const dashboard = useCurrentDashboard()

    // over entire campaign, no horizon required.
    const stats = dashboard.summaries.staticparties[party.source] as PartySummary

    // adapts PerformanceCard to compliance.
    const selector = (type: string) => {

        const plural = t(type === requirementType ? requirementPlural : type === productType ? productPlural : 'dashboard.summary.performance.asset_plural').toLowerCase()

        //console.log(complianceGroups,complianceRate,complianceClass)

        return {
            tips: {
                1: t(`dashboard.summary.performance.tips.compliance_na.party_asset`, { plural, range: performanceGroups.singlecompliance[1].label }),
                2: t(`dashboard.summary.performance.tips.compliance_other.party_asset`, { plural, range: performanceGroups.singlecompliance[2].label }),
                3: t(`dashboard.summary.performance.tips.compliance_other.party_asset`, { plural, range: performanceGroups.singlecompliance[3].label }),
                4: t(`dashboard.summary.performance.tips.compliance_best.party_asset`, { plural }),

                overall: t(`dashboard.summary.performance.tips.overall.compliance.party_asset`, { plural })
            },

            statstype: 'singlecompliance' as const,

            //  note: we're still working with party stats, but look at subsets of its statistics.
            groups: type === requirementType ? stats.complianceProfile.requirementGroups : type === productType ? stats.complianceProfile.productGroups : stats.complianceProfile.groups,
            performanceRate: type === requirementType ? stats.complianceProfile.requirementRate :  type === productType ? stats.complianceProfile.productRate : stats.complianceProfile.rate,
            performanceClass: type === requirementType ? stats.complianceProfile.requirementComplianceClass :  type === productType ? stats.complianceProfile.productComplianceClass: stats.complianceProfile.complianceClass

            // ...regroup(stats.submitted.filter(ts => ts.trail.key.assetType === type && ts.compliance?.rate))
        }
    }

    // children extend PerformanceCard for compliance.

    return <PerformanceCard noHorizon selector={selector} types={[complianceType, requirementType, productType]}>

        <Divider orientation='left'><Label icon={submissionIcon} title={t("dashboard.summary.performance.compliance.assessments")} /></Divider>

        <Row >

            <StatCol tip={t('dashboard.summary.performance.compliance.req_assessed_tip')} title={t(requirementPlural)}>
                <Label icon={requirementIcon} title={<Text>{stats.assessedRequirements.length}/<Text secondary>{stats.assessableRequirements.length}</Text></Text>} />
            </StatCol>

            <StatCol tip={t('dashboard.summary.performance.compliance.prod_assessed_tip')} title={t(productPlural)}>
                <Label icon={productIcon} title={<Text>{stats.assessedProducts.length}/<Text secondary>{stats.assessableProducts.length}</Text></Text>} />
            </StatCol>

            <StatCol tip={t('dashboard.summary.performance.compliance.rate_tip')} title={t("dashboard.summary.performance.compliance.rate")}>
                <Label icon={complianceIcon} title={<Text className={stats.assessmentProfile.assessmentClass}>{isNaN(stats.assessmentProfile.rate) ? "--" : `${stats.assessmentProfile.rate}%`}</Text>} />
            </StatCol>

        </Row>

    </PerformanceCard>

}



const AssetCard = (props: { asset: CampaignInstance }) => {

    const t = useT()

    const { asset } = props

    const dashboard = useCurrentDashboard()

    const stats = dashboard.summaries[asset.instanceType][asset.source] as AssetSummary

    const singular = t(asset.instanceType === requirementType ? requirementSingular : productSingular).toLowerCase()

    const selector = () => {

        return {
            tips: {
                1: t(`dashboard.summary.performance.tips.compliance_other.asset_tenant`, { singular, range: performanceGroups.singlecompliance[1].label }),
                2: t(`dashboard.summary.performance.tips.compliance_other.asset_tenant`, { range: performanceGroups.singlecompliance[2].label }),
                3: t(`dashboard.summary.performance.tips.compliance_other.asset_tenant`, { range: performanceGroups.singlecompliance[3].label }),
                4: t(`dashboard.summary.performance.tips.compliance_best.asset_tenant`, { singular }),

                overall: t(`dashboard.summary.performance.tips.overall.compliance.asset_tenant`, { singular })
            },

            statstype: 'singlecompliance' as const,
            groups: stats.complianceProfile.groups,
            performanceRate: stats.complianceProfile.rate,
            performanceClass: stats.complianceProfile.complianceClass,
        }
    }

    // children extend PerformanceCard for compliance.

    return <PerformanceCard selector={selector} types={[tenantType]}>

        <Divider style={{ marginTop: 10 }} orientation='left'><Label icon={submissionIcon} title={t("dashboard.summary.performance.compliance.assessments")} /></Divider>

        <Row >

            <StatCol span={8} tip={t('dashboard.summary.performance.compliance.parties_assessed_tip', { singular })} title={t(tenantPlural)}>
                <Label icon={tenantIcon} title={<Text>{stats.assessed.length}/<Text secondary>{stats.submitted.length + stats.submittedAsMissing.length}</Text></Text>} />
            </StatCol>

            <StatCol span={8} tip={t('dashboard.summary.performance.submission.parties_missing_rate_tip', { singular })} title={t('dashboard.summary.performance.submission.missing')}>
                <Label icon={tenantIcon} title={<Text emphasis='error'>{isNaN(stats.submissionProfile.missingRate) ? "--" : `${stats.submissionProfile.missingRate}%`}</Text>} />
            </StatCol>

            <StatCol span={8} tip={t('dashboard.summary.performance.compliance.party_rate_tip', { singular: t(asset.instanceType === requirementType ? requirementSingular : productSingular).toLowerCase() })}
                title={t("dashboard.summary.performance.compliance.rate")}>
                <Label icon={complianceIcon} title={<Text className={stats.assessmentProfile.assessmentClass}>{isNaN(stats.assessmentProfile.rate) ? "--" : `${stats.assessmentProfile.rate}%`}</Text>} />
            </StatCol>

        </Row>


    </PerformanceCard>

}
