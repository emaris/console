import { Button } from '#app/components/Button'
import { DrawerProps, useRoutableDrawer } from '#app/components/Drawer'
import { Label } from '#app/components/Label'
import { Paragraph } from '#app/components/Typography'
import { Form } from '#app/form/Form'
import { SelectBox } from '#app/form/SelectBox'
import { icns } from '#app/icons'
import { useT } from '#app/intl/api'
import { useCompareMultiLang, useL } from '#app/model/multilang'
import { useToggleBusy } from '#app/system/api'
import { tenantIcon } from '#app/tenant/constants'
import { useTenantStore } from '#app/tenant/store'
import { useStable } from '#app/utils/function'
import { useWorkbookWriter } from '#app/utils/workbookWriter'
import { useCurrentCampaign } from '#campaign/hooks'
import { AssetInstance } from '#campaign/model'
import { PartyInstanceLabel } from '#campaign/party/Label'
import { PartyInstance, PartyInstanceDto, usePartyInstances } from '#campaign/party/api'
import { ProductInstanceLabel } from '#campaign/product/Label'
import { useProductInstances } from '#campaign/product/api'
import { RequirementInstanceLabel } from '#campaign/requirement/Label'
import { useRequirementInstances } from '#campaign/requirement/api'
import { Product } from '#product/model'
import { useProductStore } from '#product/store'
import { requirementIcon, requirementType } from '#requirement/constants'
import { Requirement } from '#requirement/model'
import { useRequirementStore } from '#requirement/store'
import { Icon } from 'antd'
import { utils } from 'apprise-frontend-core/utils/common'
import { convert } from 'html-to-text'
import moment from 'moment-timezone'
import { useState } from 'react'
import { MdDownloadForOffline } from 'react-icons/md'
import { useCurrentDashboard } from './hooks'
import { Tenant } from '#app/tenant/model'
import { useTagStore } from '#app/tag/store'



export const useExportDrawer = () => {

    const t = useT()

    const title = t("dashboard.export.title")
    const icon = icns.download

    const { Drawer, route } = useRoutableDrawer({ id: 'export', title, icon })

    const ExportDrawer = useStable((props: Partial<DrawerProps>) => <Drawer width={600} icon={icon} title={title} {...props} />)

    const openBtn = <Button enabledOnReadOnly icn={icon} linkTo={route()}>{t("dashboard.export.open_btn")}</Button> as JSX.Element

    return { ExportDrawer, openBtn }

}


export const ExportPanel = () => {

    const t = useT()
    const toggleBusy = useToggleBusy()
    const xport = useExportStatistics()

    const campaign = useCurrentCampaign()
    const partyinst = usePartyInstances().on(campaign)
    const reqinst = useRequirementInstances().on(campaign)
    const prodinst = useProductInstances().on(campaign)

    const [parties, setParties] = useState<PartyInstance[]>(undefined!)
    const [assets, setAssets] = useState<AssetInstance[]>(undefined!)

    const allAssets = [...reqinst.allSorted(), ...prodinst.allSorted()].filter(asset => asset.properties.assessed)
    const allParties = partyinst.allSorted()

    const exportNow = async () => {

        toggleBusy(`exportstats`, t("dashboard.export.generate_msg"))

        try {

            xport.compliance(parties, assets)
            await utils().waitNow(200)
        }
        finally {

            toggleBusy(`exportstats`)

        }
    }

    const getLabel = (assetId: string) => {
        const asset = allAssets.find(a => a.id === assetId)

        return asset?.instanceType === requirementType ?
            <RequirementInstanceLabel noDecorations noLink noOptions instance={asset} />
            :
            <ProductInstanceLabel noDecorations noLink noOptions instance={asset} />
    }

    return <div style={{ alignItems: 'inherit' }} className="submission-dialog-panel">

        <Paragraph className="submission-dialog-intro">{t("dashboard.export.introduction")}</Paragraph>

        <div className="submission-dialog-section" style={{ background: 'inherit' }}>

            <Icon style={{ color: 'cadetblue' }} className='submission-dialog-icon' component={MdDownloadForOffline} />

            <br />

            <Button type='primary' enabledOnReadOnly onClick={exportNow}>
                {t('dashboard.export.btn')}
            </Button>

        </div>

        <div style={{width: '100%', textAlign: 'left', padding: '0px 20px'}}>
            <Form>
                <SelectBox
                    label={t("dashboard.export.party_selector_lbl")}
                    validation={{msg: t("dashboard.export.party_selector_msg")}}
                    onChange={v => setParties(v.length === 0 ? undefined! : v.map(sel => allParties.find(pi => pi.id === sel)))}
                    allowClear
                    undefinedOption={<Label icon={tenantIcon} title={t("dashboard.export.party_selector_placeholder")} />}
                    selectedKey={(parties ?? []).map(p => p.id)}
                    includeSelected={false}
                    getlbl={p => <PartyInstanceLabel noOptions noDecorations noLink instance={allParties.find(pi => pi.id === p)} />}
                >
                    {allParties.map(p => p.id)}
                </SelectBox>


                <SelectBox
                    label={t("dashboard.export.asset_selector_lbl")}
                    validation={{msg: t("dashboard.export.asset_selector_msg")}}
                    onChange={v => setAssets(v.length === 0 ? undefined! : v.map(sel => allAssets.find(pi => pi.id === sel)))}
                    allowClear
                    undefinedOption={<Label icon={requirementIcon} title={t("dashboard.export.asset_selector_placeholder")} />}
                    selectedKey={(assets ?? []).map(p => p.id)}
                    includeSelected={false}
                    getlbl={getLabel}
                >
                    {allAssets.map(p => p.id)}
                </SelectBox>
            </Form>
        </div>

    </div>

}

// For the first sheet collect for each party a Record of instance->compliance
type FirstSheetRow = {
    party: string
    compliance: Record<string, string | undefined>
}

// For the second sheet collect all information needed by each row
type SecondSheetRow = {
    party: string
    requirement: string
    infoRequired: string
    compliance: string
    observation: string
}


const useExportStatistics = () => {


    const dashboard = useCurrentDashboard()

    const campaign = useCurrentCampaign()
    const partyinst = usePartyInstances().on(campaign)
    const reqinst = useRequirementInstances().on(campaign)
    const prodinst = useProductInstances().on(campaign)

    const products = useProductStore()
    const requirements = useRequirementStore()
    const tenants = useTenantStore()
    const write = useWorkbookWriter()
    const multilangComparator = useCompareMultiLang()

    const tags = useTagStore()

    const l = useL()
    const t = useT()

    return {

        compliance: (parties: PartyInstance[] = partyinst.all(), assets: AssetInstance[] = [...reqinst.all(), ...prodinst.all()]) => {

            console.log(`exporting compliance data for #${parties.length} parties and #${assets.length} assets...`)

            const allTags = tags.allTags()

            const reqNumTag = allTags.filter(t => t.code === 'reqnum')
            const shortSourceTag = allTags.filter(t => t.code === 'shortsource')
            const informationRequiredTag = allTags.filter(t => t.code === 'inforequired')

            // Warn that some of the caps cannot be uniquely resolved
            if (reqNumTag.length !== 1 || shortSourceTag.length !== 1 || informationRequiredTag.length !== 1) {
                console.warn(`Error, found reqnum=${reqNumTag.length} shortcode=${shortSourceTag.length} inforequired=${informationRequiredTag.length} instances of cap tags`)
                return
            }

            // Resolve cap tag ids
            const reqNumTagId = reqNumTag[0].id
            const shortSourceTagId = shortSourceTag[0].id
            const informationRequiredTagId = informationRequiredTag[0].id

            const excludedPartiesFromStats = campaign.properties.statExcludeList ?? []

            const summaryOf = (party: PartyInstance, instance: AssetInstance) => dashboard.summaries.trails[party.source]?.find(s => s.trail.key.asset === instance?.source)
            const getSource = (instance: AssetInstance): Requirement | Product => instance.instanceType === requirementType ? requirements.safeLookup(instance.source) : products.safeLookup(instance.source)

            const assetSources = assets.reduce((acc, cur) => ({ ...acc, [cur.id]: getSource(cur) }), {})
            const partySources = parties.reduce((acc, cur) => ({ ...acc, [cur.id]: tenants.safeLookup(cur.source) }), {} as Record<string, Tenant>)

            // Comparators
            const partyInstanceComparator = (a: PartyInstanceDto, b: PartyInstanceDto) => multilangComparator(partySources[a.id].name, partySources[b.id].name)
            const assetInstanceComparator = (a: AssetInstance, b: AssetInstance) => {
                const aReqNumCap = `${l(assetSources[a.id].properties?.cap?.[reqNumTagId]) ?? ''}`.trim()
                const bReqNumCap = `${l(assetSources[b.id].properties?.cap?.[reqNumTagId]) ?? ''}`.trim()

                if (aReqNumCap === bReqNumCap)
                    return multilangComparator(assetSources[a.id].name, assetSources[b.id].name)

                return aReqNumCap.localeCompare(bReqNumCap)
            }

            // Assets that can be assessed and sorted
            const filteredSortedInstances = assets.filter(asset => asset.properties.assessed).sort(assetInstanceComparator)

            // Parties sorted divided in included and excluded from stats
            const partiesSortedNotExcludedFromStats = parties.filter(p => !excludedPartiesFromStats.includes(partySources[p.id].id)).sort(partyInstanceComparator)
            const partiesSortedExcludedFromStats = parties.filter(p => excludedPartiesFromStats.includes(partySources[p.id].id)).sort(partyInstanceComparator)

            // Mock empty rows for first and second sheets
            const emptyFirstSheetRow = {party: undefined!, compliance: filteredSortedInstances.reduce((acc, cur) => ({...acc, [cur.id]: undefined}), {})} as FirstSheetRow
            const emptySecondSheetRow = {party: undefined!, requirement: undefined!, infoRequired: undefined!, compliance: undefined!, observation: undefined!} as SecondSheetRow

            // Initial model for first and second sheets rows
            let firstSheetRows = [] as FirstSheetRow[]
            let secondSheetRows = [] as SecondSheetRow[]
            let firstSheetSubHeaderRow1: FirstSheetRow = { party: undefined!, compliance: {} }
            let firstSheetSubHeaderRow2: FirstSheetRow = { party: undefined!, compliance: {} }

            // Sub-Headers for first sheet
            filteredSortedInstances.forEach(instance => {
                firstSheetSubHeaderRow1 = { ...firstSheetSubHeaderRow1, compliance: { ...firstSheetSubHeaderRow1.compliance, [instance.id]: l(assetSources[instance.id].properties.cap?.[shortSourceTagId]) ?? '' } }
                firstSheetSubHeaderRow2 = { ...firstSheetSubHeaderRow2, compliance: { ...firstSheetSubHeaderRow2.compliance, [instance.id]: l(assetSources[instance.id].properties.cap?.[reqNumTagId]) ?? '' } }
            })

            firstSheetRows.push(firstSheetSubHeaderRow1)
            firstSheetRows.push(firstSheetSubHeaderRow2)

            // Collect stats for both sheets by party first and then assets for each party
            const collectStatsFor = (parties: PartyInstanceDto[]) => {
                parties.forEach(party => {

                    let firstSheetRow: FirstSheetRow = { party: l(partySources[party.id].name) ?? '', compliance: {} }
    
                    filteredSortedInstances.forEach(instance => {

                        const assetinst = instance.instanceType === requirementType ? reqinst : prodinst
    
                        const summary = summaryOf(party, instance)
    
                        const compliance = assetinst.isForParty(party, instance) ? summary?.compliance?.code ?? '' : t('submission.status.nostatus')
                        const observation = assetinst.isForParty(party, instance) ? l(summary?.compliance?.complianceObservation) : t('submission.status.nostatus')
    
                        firstSheetRow = { ...firstSheetRow, compliance: { ...firstSheetRow.compliance, [instance.id]: compliance } }
                        secondSheetRows.push({party: l(partySources[party.id].name), requirement: l(assetSources[instance.id].properties.cap?.[reqNumTagId]) ?? '', infoRequired: l(assetSources[instance.id].properties.cap?.[informationRequiredTagId]) ?? '', compliance, observation })
    
                    })
    
                    firstSheetRows.push(firstSheetRow)
    
                })
            }

            // Collect stats for parties included in statistics (they should be on top of the sheets)
            collectStatsFor(partiesSortedNotExcludedFromStats)
            
            // Add empty rows to first and second sheets to put a gap between included and excluded parties from statistics
            if (partiesSortedExcludedFromStats.length > 0) {
                firstSheetRows.push(emptyFirstSheetRow)
                firstSheetRows.push(emptyFirstSheetRow)
                secondSheetRows.push(emptySecondSheetRow)
                secondSheetRows.push(emptySecondSheetRow)
            }

            // Collect stats for parties excluded from statistics (they should be on bottom of the sheets)
            collectStatsFor(partiesSortedExcludedFromStats)
            

            // Write sheets to file and push it to the client

            console.log({firstSheetRows})

            // Render first sheet headers
            const firstSheet = write.sheetOf(firstSheetRows)
                .text('').render(row => row.party)

            // For each instance write a column
            filteredSortedInstances.forEach(instance => firstSheet.text(l(assetSources[instance.id].properties.cap?.[informationRequiredTagId]) ?? l(assetSources[instance.id].name)).render(row => row.compliance[instance.id]))

            // Render second sheet headers
            const secondSheet = firstSheet.as(t('campaign.compliance_module.name_singular'))
            secondSheet.sheetOf(secondSheetRows)
                .text(t('dashboard.export.cpc')).render(r => r.party)
                .text(t('dashboard.export.requirement')).render(r => r.requirement)
                .text(t('dashboard.export.info_required')).render(r => r.infoRequired)
                .text(t('dashboard.export.info_compliance')).render(r => r.compliance)
                .text(t('dashboard.export.observation_singular')).render(r => convert(r.observation))
                .as(t('dashboard.export.observation_plural'))
            
            secondSheet.save(`${campaign.name.en ?? 'campaign'}-${t('campaign.compliance_module.name_singular').toLowerCase()}-${moment().format('YYYYMMDD')}.xlsx`)

        }
    }
}