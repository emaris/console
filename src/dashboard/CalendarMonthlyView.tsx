import { Button } from '#app/components/Button'
import { TableProps } from '#app/components/VirtualTable'
import { icns } from '#app/icons'
import { useT } from '#app/intl/api'
import { TransitionContext } from '#app/scaffold/Page'
import { paramsInQuery, updateQuery } from '#app/utils/routes'
import Carousel, { Dots } from '@brainhubeu/react-carousel'
import { Empty, Icon, Tooltip } from 'antd'
import moment from 'moment-timezone'
import * as React from 'react'
import { AutoResizer } from 'react-base-table'
import { AiOutlineLeft, AiOutlineRight } from 'react-icons/ai'
import { useHistory, useLocation } from 'react-router-dom'
import { CalendarTable, Group } from './CalendarTable'
import { useCalendar } from './hooks'



const monhtParam = "calendar-month"



export type CalendarMonthlyViewProps = Partial<TableProps<any>> & { groups: Group[] }


export const CalendarMonthlyView = (props: CalendarMonthlyViewProps) => {

    const { groups, ...rest } = props

    const history = useHistory()

    // const { closestMonthFor } = useCalendar()
    const { currentMonthFor } = useCalendar()
    const { search } = useLocation()

    const { [monhtParam]: month } = paramsInQuery(search)

    // const [selectedMonth, setSelectedMonth] = React.useState(() => parseInt(month as string) || closestMonthFor(groups))
    const [selectedMonth, setSelectedMonth] = React.useState(() => parseInt(month as string) || currentMonthFor(groups))


    const onMonthChange = month => {

        setSelectedMonth(month);

        history.push(`${history.location.pathname}?${updateQuery(search).with(p => { p[monhtParam] = month })}`)
    }

    return <div className='monthly-view'>

        {groups.length > 0 ?

            <React.Fragment>
                <div className="months">
                    <AutoResizer>{
                        ({ width, height }) =>

                            <div style={{ width, height }}>

                                <Carousel
                                    centered
                                    itemWidth={Math.round(width * 0.80)}

                                    value={selectedMonth} onChange={onMonthChange}
                                    draggable={false}
                                    slides={groups.map((group, i) => {

                                        const active = selectedMonth === i

                                        return <div className={`month-container ${active ? 'active' : 'inactive'}`} key={i}>
                                            <div style={{ height, width: Math.round(width * 0.75) }}>
                                                <MonthCard active={active} onMonthChange={() => onMonthChange(i < selectedMonth ? selectedMonth - 1 : selectedMonth + 1)} group={group} arrowDirection={i < selectedMonth ? 'left' : 'right'} {...rest} />
                                            </div>
                                        </div>

                                    })}
                                />
                            </div>
                    }
                    </AutoResizer>
                </div>

                <div className="month-dots">

                    <Dots value={selectedMonth} onChange={onMonthChange}
                        thumbnails={groups.map((group, i) => {

                            return <Tooltip key={i} title={group.key.name}>
                                {icns.dot}
                            </Tooltip>
                        })} />
                </div>

            </React.Fragment>

            :

            <Empty image={Empty.PRESENTED_IMAGE_SIMPLE} style={{ marginTop: 100, width: '100%' }} />}

    </div >
}


export type MonthCardProps = Partial<TableProps<any>> & {

    active: boolean
    group: Group
    onMonthChange: () => any
    arrowDirection?: 'left' | 'right'

}

const MonthCard = (props: MonthCardProps) => {
    const t = useT()

    const { active, group, arrowDirection, onMonthChange, data, total, ...rest } = props

    const { transitionOngoing } = React.useContext(TransitionContext)

    const extraStyles = !active ? { display: "flex", alignItems: "center" } : {}

    const monthCardTitle = moment().year(group.key.year).month(group.key.month).format(t("time.default_format_month_year"))

    return <div style={{ height: "100%", ...extraStyles }}>{

        (active && !transitionOngoing) ?

            <div className="month-card">
                <div className="month-title">{monthCardTitle}</div>
                <CalendarTable className="month-table" data={group.group} total={group.group.length} showHeader={false} dateFormat="D" {...rest}> </CalendarTable>
            </div>

            :

            <Button noborder type="ghost" tooltip={group.key.name} className={`${arrowDirection}-arrow`} onClick={onMonthChange}>
                <Icon component={arrowDirection === 'left' ? AiOutlineLeft : AiOutlineRight}></Icon>
            </Button>

    }</div>
}