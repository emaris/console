import { Button } from '#app/components/Button'
import { Label } from '#app/components/Label'
import { OptionMenu } from '#app/components/OptionMenu'
import { Placeholder } from '#app/components/Placeholder'
import { Column, VirtualTable } from '#app/components/VirtualTable'
import { VSelectBox } from '#app/form/VSelectBox'
import { icns } from '#app/icons'
import { useCurrentLanguage, useT } from '#app/intl/api'
import { Language } from '#app/intl/model'
import { useLocale } from '#app/model/hooks'
import { Multilang } from '#app/model/multilang'
import { useBytestreams } from '#app/stream/api'
import { tenantIcon } from '#app/tenant/constants'
import { TenantDto, useNoTenant } from '#app/tenant/model'
import { TimeLabel } from '#app/time/Label'
import { useLogged } from '#app/user/store'
import { compareDates } from '#app/utils/common'
import { useFilterState } from '#app/utils/filter'
import { useAsyncRender } from '#app/utils/hooks'
import { productDeadlineEvent, requirementDeadlineEvent } from '#campaign/constants'
import { useEventInstanceModel } from '#campaign/event/model'
import { useEventInstanceDates, useEventInstanceStore } from '#campaign/event/store'
import { useCurrentCampaign } from '#campaign/hooks'
import { TimelinessLabel } from '#campaign/submission/TimelinessLabel'
import { useSubmissionCalls } from '#campaign/submission/calls'
import { useSubmissionClient } from '#campaign/submission/client'
import { submissionIcon } from '#campaign/submission/constants'
import { SubmissionStub, Trail, TrailDto, TrailKey } from '#campaign/submission/model'
import { ProductLabel } from '#product/Label'
import { productIcon, productType } from '#product/constants'
import { ProductDto } from '#product/model'
import { useProductStore } from '#product/store'
import { RequirementLabel } from '#requirement/Label'
import { requirementType } from '#requirement/constants'
import { RequirementDto } from '#requirement/model'
import { useRequirementStore } from '#requirement/store'
import { Tooltip } from 'antd'
import moment from 'moment-timezone'
import * as React from 'react'
import { DashboardPage } from './DashboardPage'
import { useCurrentAsset, useCurrentDashboard, useCurrentParty } from './hooks'
import { useTagModel } from '#app/tag/model'
import { TagList } from '#app/tag/Label'

const filterGroup = 'shared-documents-filter-group'

type Data = {
    trails: Trail[]
    tenants: { id: string, name: Multilang }[]
}

export const SharedDocuments = () => {
    const campaign = useCurrentCampaign()

    const calls = useSubmissionCalls().on(campaign)

    const [fetched, setFetched] = React.useState<Data>(undefined!)

    const forceReload = () => calls.fetchAllPublished().then(setFetched)

    console.log({ fetched })

    const [render] = useAsyncRender({
        id: "fetchshared",
        when: !!fetched,
        task: () => calls.fetchAllPublished().then(setFetched),
        content: () => <InnerSharedDocuments data={fetched} forceReload={forceReload} />,
        placeholder: Placeholder.none

    })

    return render

}

type SubmissionWithTrail = SubmissionStub & {
    trail: TrailDto
    timeliness: 'ontime' | 'late' | 'notdue'
}

type AccentFrame = "none" | "months" | "weeks" | "days" | undefined

const InnerSharedDocuments = (props: { data: Data, forceReload: () => void }) => {

    const { data: remote, forceReload } = props

    // console.log({ remote })

    const { trails, tenants } = remote

    const campaign = useCurrentCampaign()
    const campaignTimeZone = campaign.properties.timeZone ? { 'original': campaign.properties.timeZone } : undefined
    const events = { ...useEventInstanceStore().on(campaign), ...useEventInstanceModel().on(campaign), ...useEventInstanceDates().on(campaign) }
    const stream = useBytestreams()
    const requirements = useRequirementStore()
    const products = useProductStore()
    const currentLang = useCurrentLanguage()
    const parties = useParties(tenants)

    const submissionsClient = useSubmissionClient()

    const t = useT()
    const { l } = useLocale()

    const { timelinessComparator } = useTimelinessComparator()

    const profiles = {

        [requirementType]: {
            deadlineOf: (s: SubmissionWithTrail) => events.allAbout(s.trail.key.asset).find(ei => ei.source === requirementDeadlineEvent),
            dueDateLabel: (s: SubmissionWithTrail, accentFrame: AccentFrame, className: string) => {
                const date = events.absolute(profiles[requirementType].deadlineOf(s)?.date)
                return <TimeLabel accentFrame={accentFrame} displayMode={'relative'} render={date => t('campaign.date.due', { date })} value={date} className={className} timezones={campaignTimeZone} />
            }
        },
        [productType]: {
            deadlineOf: (s: SubmissionWithTrail) => events.allAbout(s.trail.key.asset).find(ei => ei.source === productDeadlineEvent),
            dueDateLabel: (s: SubmissionWithTrail, accentFrame: AccentFrame, className: string) => {
                const date = events.absolute(profiles[productType].deadlineOf(s)?.date)
                return <TimeLabel accentFrame={accentFrame} displayMode={'relative'} render={date => t('campaign.date.due', { date })} value={date} className={className} timezones={campaignTimeZone} />
            }
        }
    }

    // const p = (s: SubmissionWithTrail) => profiles[s.trail.key.assetType]

    const data = trails.flatMap(t => t.submissions).filter(s => s.lifecycle.publication !== undefined).map(s => {
        const trail = trails.find(t => t.id === s.trail)!
        const assetId = trail.key.asset
        const assetType = trail.key.assetType
        const eventDate = events.dateOf(assetType === requirementType ? requirementDeadlineEvent : productDeadlineEvent, assetId)
        const submittedDate = s.lifecycle.lastSubmitted
        const timeliness = (eventDate && submittedDate) ? moment(new Date(eventDate)).isAfter(moment(new Date(submittedDate))) ? 'ontime' : 'late' : 'notdue'
        return { ...s, trail: { ...trail, submissions: [] as SubmissionStub[] }, timeliness }
    }) as SubmissionWithTrail[]

    const { PartyFilter, filteredData: partyFilteredData } = usePartyFilter({
        data,
        parties: tenants
    })
    const { AssetFilter, filteredData: assetFilterData } = useAssetFilter({
        filtered: partyFilteredData,
        data
    })

    const { RevisionFilter, filteredData } = useRevisionFilter({
        filtered: assetFilterData,
        trails
    })

    const logged = useLogged()
    const party = useCurrentParty()
    const asset = useCurrentAsset()
    const title = party && logged.hasNoTenant() ? l(party.tenant.name) : asset.asset ? asset.nameOf(asset.asset!) : undefined

    const removeFor = async (submission: SubmissionWithTrail, lang: Language) => await submissionsClient.on(campaign).deleteSharedSubmission(submission, submission.trail, lang, forceReload)

    const documentFor = (lang, idx, submission) => {

        const currentStream = submission.lifecycle.publication?.[lang]!

        const removeBtn = <Button className='remove-shared-document-btn' light onClick={() => removeFor(submission, lang as Language)}>{t('dashboard.shared_documents.columns.publications.remove')}</Button>

        const taggedDocument = <a key={idx} download href={stream.linkOf(currentStream)}>
            <Label className='share-in-lang' title={lang.toUpperCase()} mode='tag' icon={icns.pdf} />
        </a>

        return <div key={idx}>{logged.isAdmin() ? <Tooltip overlayClassName="error-tooltip" mouseEnterDelay={1.5} title={removeBtn}>{taggedDocument}</Tooltip> : taggedDocument}</div>
    }

    return <DashboardPage tab='shared' title={title}>

        <VirtualTable
            data={filteredData}
            selectable={false}
            filters={[PartyFilter, AssetFilter, RevisionFilter]}
            sortBy={[['party', 'asc']]}
            clearFilters
            filterGroup={filterGroup}
            filterWith={(f, s) => {
                const asset = s.trail.key.asset
                const assetType = s.trail.key.assetType
                const party = s.trail.key.party
                const reference = (s.lifecycle.reference ? s.lifecycle.reference[currentLang] ? s.lifecycle.reference[currentLang] : '' : '') as string

                const partyObj = parties.lookup(party)
                const assetObj = assetType === requirementType ? requirements.lookup(asset) : products.lookup(asset)

                const assetName = assetObj ? l(assetObj.name) : ''
                const partyName = partyObj ? l(partyObj.name) : ''

                return partyName.toLowerCase().includes(f.toLowerCase()) || assetName.toLowerCase().includes(f.toLowerCase()) || reference.toLowerCase().includes(f.toLowerCase())
            }}

        >

            <Column<SubmissionWithTrail>
                title={t("dashboard.shared_documents.columns.party.name")}
                headerTooltip={t("dashboard.shared_documents.columns.party.tip")}
                key={'party'}
                dataKey="party"
                sortable
                comparator={(p1, p2) => l(parties.safeLookup(p1).name).localeCompare(l(parties.safeLookup(p2).name))}
                dataGetter={s => s.trail.key.party}
                cellRenderer={(s) => <Label icon={tenantIcon} title={l(parties.safeLookup(s.cellData).name)} />}
            ></Column>

            <Column<SubmissionWithTrail>
                title={t("dashboard.shared_documents.columns.name.name")}
                headerTooltip={t("dashboard.shared_documents.columns.name.tip")}
                key={'name'}
                flexGrow={1}
                minWidth={350}
                dataKey="trail"
                sortable
                comparator={(p1, p2) => {
                    const name1 = p1.trail.key.assetType === requirementType ? l(requirements.safeLookup(p1.trail.key.asset).name) : l(products.safeLookup(p1.trail.key.asset).name)
                    const name2 = p2.trail.key.assetType === requirementType ? l(requirements.safeLookup(p2.trail.key.asset).name) : l(products.safeLookup(p2.trail.key.asset).name)
                    return name1.localeCompare(name2)
                }}
                dataGetter={s => s}
                cellRenderer={({ cellData: s }) => s.trail.key.assetType === requirementType ? <RequirementLabel requirement={s.trail.key.asset} noDecorations noLink /> : <ProductLabel product={s.trail.key.asset} noDecorations noLink />}
            ></Column>

            <Column<SubmissionStub> title={t("submission.tags.col")} dataKey="tags" minWidth={50}
                dataGetter={s => s.lifecycle.tags}
                cellRenderer={({ rowData: s }) => <TagList light={false} renderProps={{ noIcon: true, className: 'submission-tag' }} taglist={s.lifecycle.tags} /> } />

            <Column<SubmissionWithTrail> key='reference'
                minWidth={350}
                flexGrow={1}
                dataKey="reference"
                title={t("dashboard.shared_documents.columns.reference.name")}
                headerTooltip={t("dashboard.shared_documents.columns.reference.tip")}
                sortable={true}
                comparator={(p1, p2) => {
                    const name1 = (p1.lifecycle.reference?.[currentLang] ?? '') as string
                    const name2 = (p2.lifecycle.reference?.[currentLang] ?? '') as string
                    return name1.localeCompare(name2)
                }}
                cellRenderer={({ rowData: s }) => {
                    const hasLinkInCurrentLang = s.lifecycle.publication?.[currentLang] !== undefined ? s.lifecycle.publication[currentLang]!.trim() === "" ? false : true : false
                    const label = s.lifecycle.reference && s.lifecycle.reference[currentLang] ? <Label icon={icns.pdf} noDecorations noLink title={s.lifecycle.reference[currentLang]} /> : <></>

                    return hasLinkInCurrentLang ? <a download href={stream.linkOf(s.lifecycle.publication?.[currentLang]!)}>{label}</a> : label
                }
                } />

            {/* <Column<SubmissionWithTrail> key='duedate'
                minWidth={150}
                dataKey="duedate"
                title={t("dashboard.shared_documents.columns.due_date.name")}
                headerTooltip={t("dashboard.shared_documents.columns.due_date.tip")}
                sortable
                comparator={events.duedateComparator}
                cellRenderer={({ rowData: r }) => {
                    const accentLate = r.timeliness === 'late'
                    return p(r).dueDateLabel(r, accentLate ? "none" : "weeks", accentLate ? 'warn' : '')
                }} /> */}

            {/* <Column<SubmissionWithTrail> key='timeliness'
                        minWidth={110} 
                        title={t("dashboard.shared_documents.columns.timeliness.name")}
                        headerTooltip={t("dashboard.shared_documents.columns.timeliness.tip")}
                        className='trail-timeliness' 
                        dataKey="timeliness" 
                        sortable
                        dataGetter={s=>s}
                        cellRenderer={({cellData:summary}) => summary.timeliness && summary.last?.lifecycle.state !== 'missing' &&
                            <Label mode='tag' className={`timeliness-${summary.timeliness}`}
                                icon={eventIcon} 
                                title={t(`dashboard.labels.timeliness.${summary.timeliness}`)}  />}  
            /> */}

            <Column<SubmissionWithTrail> key='timeliness'
                minWidth={110}
                title={t("dashboard.shared_documents.columns.timeliness.name")}
                headerTooltip={t("dashboard.shared_documents.columns.timeliness.tip")}
                className='trail-timeliness'
                dataKey="timeliness"
                sortable
                comparator={(t1, t2) => timelinessComparator(t1?.trail?.key, t2?.trail?.key)}
                dataGetter={s => s}
                cellRenderer={({ cellData: summary }) => summary.timeliness && summary.last?.lifecycle.state !== 'missing' &&
                    <TimelinessLabel trailKey={summary.trail.key} />}
            />

            <Column<SubmissionWithTrail> width={150} key="latestsubmission"

                dataKey="latestsubmission"

                flexGrow={1}
                title={t("dashboard.shared_documents.columns.last_submission.name")}
                headerTooltip={t("dashboard.shared_documents.columns.last_submission.tip")}
                sortable

                comparator={[(d1, d2) => compareDates(d1, d2, false), (d1, d2) => compareDates(d1, d2, true)]}

                dataGetter={s => s.lifecycle.lastSubmitted}
                cellRenderer={({ rowData: s }) => <TimeLabel displayMode="relative" accentFrame="days" accentMode="record" icon={submissionIcon} noLink value={moment(s.lifecycle.lastSubmitted)} timezones={campaignTimeZone} />}
            />

            <Column<SubmissionWithTrail> width={150} key="shared"

                dataKey="shared"

                flexGrow={1}
                title={t("dashboard.shared_documents.columns.shared.name")}
                headerTooltip={t("dashboard.shared_documents.columns.shared.tip")}
                sortable

                comparator={[(d1, d2) => compareDates(d1, d2, false), (d1, d2) => compareDates(d1, d2, true)]}

                dataGetter={s => s.lifecycle.lastSubmitted}
                cellRenderer={({ rowData: s }) => <TimeLabel displayMode="relative" accentFrame="days" accentMode="record" icon={submissionIcon} noLink value={moment(s.lifecycle.lastModified)} timezones={campaignTimeZone} />}
            />


            {/*<Column<SubmissionWithTrail> width={150} key="latestpublication"

                dataKey="latestpublication"

                title={t("dashboard.shared_documents.columns.last_published.name")}
                headerTooltip={t("dashboard.shared_documents.columns.last_published.tip")}
                sortable

                comparator={[(d1, d2) => compareDates(d1, d2, false), (d1, d2) => compareDates(d1, d2, true)]}

                dataGetter={s => s.lifecycle.lastPublished}
                cellRenderer={({ rowData: s }) => <TimeLabel displayMode="relative" accentFrame="days" accentMode="record" icon={icns.publish} noLink value={moment(s.lifecycle.lastPublished)} timezones={campaignTimeZone} />}
            /> */}



            <Column<SubmissionWithTrail> key='publications'
                minWidth={150}
                dataKey="publications"
                title={t("dashboard.shared_documents.columns.publications.name")}
                headerTooltip={t("dashboard.shared_documents.columns.publications.tip")}
                sortable={false}
                cellRenderer={({ rowData: s }) => {
                    const submission = s as SubmissionWithTrail
                    return <div className="shares-in-lang">
                        {
                            Object.keys(submission.lifecycle.publication!).map((lang, idx) => documentFor(lang, idx, submission))
                        }
                    </div>
                }} />

        </VirtualTable>

    </DashboardPage>

}

const useTimelinessComparator = () => {

    const { summaries } = useCurrentDashboard()

    const tagModel = useTagModel()

    const timelinessComparator = (key1: TrailKey | undefined, key2: TrailKey | undefined): number => {
        if (key1 === undefined && key2 === undefined) return 0
        if (key1 === undefined) return 1
        if (key2 === undefined) return -1
        const timeliness1 = summaries.trailSummaryOf(key1).timeliness
        const timeliness2 = summaries.trailSummaryOf(key2).timeliness

        const tag1 = timeliness1 ? timeliness1.tag : timeliness1
        const tag2 = timeliness2 ? timeliness2.tag : timeliness2

        return (tag1 && tag2) ? tagModel.comparator(tag1, tag2) : 0
    }

    return { timelinessComparator }

}

const usePartyFilter = (props: { data: SubmissionWithTrail[], parties: { id: string, name: Multilang }[] }) => {

    const { data, parties: dataParties } = props

    const t = useT()
    const { l } = useLocale()

    //eslint-disable-next-line
    const key = React.useMemo(() => 'dashboard-shared-documents-party-key', [])

    const ctx = useFilterState(filterGroup)

    const parties = useParties(dataParties)

    const distinctParties = React.useMemo(() =>
        data.map(d => (d.trail as TrailDto).key.party).filter((v, i, s) => s.indexOf(v) === i).map(p => parties.safeLookup(p))
        //eslint-disable-next-line
        , [data])

    const selected = (ctx.get(key) ?? distinctParties) as TenantDto[]

    const PartyFilter = (

        <OptionMenu id={t => t.id} selected={selected} setSelected={(a) => ctx.set(key)(a)} placeholderIcon={tenantIcon} placeholder={t("dashboard.shared_documents.filters.select_party")}>
            {distinctParties.map(party => <OptionMenu.Option key={party.id} value={party} label={<Label icon={tenantIcon} title={l(parties.safeLookup(party.id).name)} />} />)}
        </OptionMenu>
    )

    const partyFilter = React.useCallback(

        (p: SubmissionWithTrail) => selected.some(s => p.trail.key.party === s.id)
        //eslint-disable-next-line
        , [selected])

    const filteredData = React.useMemo(
        () => data.filter(partyFilter)
        , [data, partyFilter])

    return { PartyFilter, filteredData }

}


const useAssetFilter = (props: { data: SubmissionWithTrail[], filtered: SubmissionWithTrail[] }) => {

    const { data, filtered } = props

    const t = useT()

    //eslint-disable-next-line
    const key = React.useMemo(() => 'dashboard-shared-documents-asset-key', [])

    const ctx = useFilterState(filterGroup)

    const requirements = useRequirementStore()
    const products = useProductStore()


    //eslint-disable-next-line
    const distinctAssets = React.useMemo(() => data.map(d => d.trail.key).filter((v, i, s) => s.findIndex(k => k.asset === v.asset) === i).map(k => k.assetType === requirementType ? requirements.safeLookup(k.asset) : products.safeLookup(k.asset)), [data])


    const selected = (ctx.get(key) ?? distinctAssets) as (ProductDto | RequirementDto)[]

    const AssetFilter = (

        <OptionMenu id={t => t.id} selected={selected} setSelected={(a) => ctx.set(key)(a)} placeholderIcon={productIcon} placeholder={t("dashboard.shared_documents.filters.select_asset")}>
            {distinctAssets.map(asset => {
                const isRequirement = requirements.lookup(asset.id) !== undefined
                const label = isRequirement ? <RequirementLabel noLink requirement={asset.id} /> : <ProductLabel noLink product={asset.id} />
                return <OptionMenu.Option key={asset.id} value={asset} label={label} />
            })}
        </OptionMenu>
    )

    const assetFilter = React.useCallback(

        (s: SubmissionWithTrail) => selected.some(s_ => s.trail.key.asset === s_.id)
        //eslint-disable-next-line
        , [selected])

    const filteredData = React.useMemo(
        () => filtered.filter(assetFilter)
        , [filtered, assetFilter])

    return { AssetFilter, filteredData }

}


const useRevisionFilter = (props: { filtered: SubmissionWithTrail[], trails: TrailDto[] }) => {

    const { filtered, trails } = props

    const t = useT()

    //eslint-disable-next-line
    const key = React.useMemo(() => 'dashboard-shared-documents-revision-key', [])

    const ctx = useFilterState(filterGroup)

    const defaultOption = { id: 'all', name: "dashboard.shared_documents.filters.select_revision.older_revisions" }

    //eslint-disable-next-line
    const options = React.useMemo(() => [defaultOption, { id: 'latest', name: "dashboard.shared_documents.filters.select_revision.latest_revisions" }], [])

    const selected = ctx.get(key) ?? defaultOption

    const RevisionFilter = <VSelectBox
        placeholder={t("dashboard.shared_documents.filters.select_revision.name")}
        onChange={v => ctx.set(key)(v)}
        options={options}
        light

        style={{ width: 250 }}
        renderOption={o => <Label icon={icns.pdf} title={t(o.name)}></Label>}
        //lblTxt={o=>t(o.name)}
        optionId={o => o.id}
    >
        {[selected]}
    </VSelectBox>

    const filter = React.useCallback((s: SubmissionWithTrail) => {

        const allFilteredSubmissions = trails.map(trail => ({
            ...trail, submissions: trail.submissions.reduce((acc, cur) => {
                if (acc.length === 0) return [cur]
                const latest = acc[0] as SubmissionStub
                return moment(new Date(latest.lifecycle.lastPublished)).isBefore(moment(new Date(cur.lifecycle.lastPublished))) ? [cur] : acc
            }, [] as SubmissionStub[])
        })).flatMap(t => t.submissions)


        return allFilteredSubmissions.some(f => f.id === s.id)

    }

        //eslint-disable-next-line
        , [selected])


    const filteredData = (selected === undefined || selected.id === 'all') ? filtered : filtered.filter(filter)

    return { RevisionFilter, filteredData }

}

const useParties = (tenants: { id: string, name: Multilang }[]) => {

    const noTenant = useNoTenant()

    const self = {
        lookup: (id: string) => tenants.find(t => t.id === id),
        safeLookup: (id: string) => self.lookup(id) ?? noTenant.get(),
    }

    return self

}