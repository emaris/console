import { Label } from "#app/components/Label"
import { OptionMenu } from '#app/components/OptionMenu'
import { useT } from '#app/intl/api'
import { useLocale } from "#app/model/hooks"
import { useToggleBusy } from '#app/system/api'
import { tenantIcon, tenantSingular, tenantType } from "#app/tenant/constants"
import { useTenantStore } from '#app/tenant/store'
import { clock, wait } from "#app/utils/common"
import { showAndThrow } from '#app/utils/feedback'
import { useFilterState } from '#app/utils/filter'
import { campaignSingular, campaignType } from "#campaign/constants"
import { useEventInstanceStore } from '#campaign/event/store'
import { useCurrentCampaign } from "#campaign/hooks"
import { AssetInstance } from '#campaign/model'
import { PartyInstance, PartyInstanceDto, usePartyInstances } from '#campaign/party/api'
import { ProductInstance, ProductInstanceDto, useProductInstances } from '#campaign/product/api'
import { RequirementInstance, useRequirementInstances } from '#campaign/requirement/api'
import { useRevisions } from '#campaign/submission/revision'
import { useSubmissionStore } from '#campaign/submission/store'
import { eventIcon, eventType } from "#event/constants"
import { productIcon, productSingular, productType } from "#product/constants"
import { useProductStore } from '#product/store'
import { requirementIcon, requirementSingular, requirementType } from "#requirement/constants"
import { useRequirementStore } from '#requirement/store'
import { TFunction } from 'i18next'
import * as React from "react"
import { CalendarRow } from './CalendarTable'
import { CalendarContextType, useCalendarContext } from "./calendar"
import { useCalendar } from "./hooks"
import { FilterSelection } from './store'


const typeMap = (t: TFunction) => ({
    'other': t('event.types.reminder_singular'),
    [requirementType]: t(requirementSingular),
    [productType]: t(productSingular),
    [campaignType]: t(campaignSingular),
    'submitted': t("dashboard.calendar.filters.submission"),
    'assessed': t("dashboard.calendar.filters.assessment")
})

type ComputedCalendarType = {
    calendarData: CalendarRow[]
    computeCalendar: () => Promise<void>
    currentlySelected: FilterSelection
    initiallySelected: FilterSelection
    filters: JSX.Element[]
}


export const useCampaignCalendar = (props: { campaignCalendarGroup: string }): ComputedCalendarType => {

    const { campaignCalendarGroup } = props

    const t = useT()
    const { l } = useLocale()

    const { eventInstanceToCalendarRow } = useCalendar()

    const ctx = useFilterState(campaignCalendarGroup)
    
    const campaign = useCurrentCampaign()

    const eventinst = useEventInstanceStore().on(campaign)
    const partyinst = usePartyInstances().on(campaign)
    const reqinst = useRequirementInstances().on(campaign)
    const prodinst = useProductInstances().on(campaign)
    
    const allpartyinst = partyinst.all()
    const allreqinst = reqinst.all()
    const allprodinst = prodinst.all()
    const alleventinst = eventinst.all()
    

    const { calendars = {} as CalendarContextType, setCalendars } = useCalendarContext()

    const regenerate = calendars.regenerate

    React.useEffect(() => {

        if (regenerate)
            computeAndSetCalendarData()

        //eslint-disable-next-line
    }, [regenerate])

    const [calendarData, calendarDataSet] = React.useState<CalendarRow[]>(calendars.general!)

    const eventOptions = [campaignType, productType, requirementType]
    const actionOptions = ["assessed", "submitted"]

    const computeCalendarRows = async (): Promise<CalendarRow[]> => {
        const noTemporalEvents = alleventinst.filter(t => t.type !== eventType)

        // const submissionRows: CalendarRow[] = clock('building calendar submission rows', () => buildCalendarRows(trails, 'all'))

        const eventRows: CalendarRow[] = clock('building calendar event rows', () => [...noTemporalEvents.filter(e => e.date !== undefined).map(event => eventInstanceToCalendarRow(event, noTemporalEvents))].filter(row => !!row.date))

        // const calendarRows: CalendarRow[] = clock('merging submissions and events calendar rows', () => [...eventRows/*, ...submissionRows!*/])

        setCalendars({ ...calendars, general: eventRows })

        return eventRows
    }

    const initiallySelected = {
        [tenantType]: partyinst.all().map(p => p.source),
        [requirementType]: allreqinst.map(r => r.source),
        [productType]: allprodinst.map(p => p.source),
        [eventType]: eventOptions,
        'action': actionOptions
    }

    const currentlySelected = ctx.get('toggles') ?? initiallySelected

    const EventFilter = <OptionMenu placeholderIcon={eventIcon} placeholder={t("campaign.labels.filter_event_types")}
        setSelected={types => {
            const { [eventType]: events, ...rest } = ctx.get('toggles') ?? {}
            ctx.set('toggles')(types ? { ...rest, [eventType]: types } : rest)
        }}
        selected={currentlySelected[eventType] ?? initiallySelected[eventType]}>
        {[campaignType, requirementType, productType].map(o => <OptionMenu.Option key={o} value={o} label={<Label icon={eventIcon} title={typeMap(t)[o]} />} />)}
    </OptionMenu>

    // const ActionFilter = <OptionMenu placeholderIcon={eventIcon} placeholder={t("campaign.labels.filter_action_types")}
    //     setSelected={types => {
    //         const { 'action': actions, ...rest } = ctx.get('toggles') ?? {}
    //         ctx.set('toggles')(types ? { ...rest, 'action': types } : rest)
    //     }}
    //     selected={currentlySelected['action'] ?? initiallySelected['action']}>
    //     {['assessed', 'submitted'].map(o => <OptionMenu.Option key={o} value={o} label={<Label icon={eventIcon} title={typeMap(t)[o]} />} />)}
    // </OptionMenu>


    // let filters = [EventFilter, ActionFilter]
    let filters = [EventFilter]

    if (allreqinst.length > 0)
        filters.push(
            <OptionMenu placeholderIcon={requirementIcon} placeholder={t(requirementSingular)}
                setSelected={selected => {
                    const { [requirementType]: requirements, ...rest } = ctx.get('toggles') ?? {}
                    ctx.set(`toggles`)(selected ? { ...rest, [requirementType]: selected } : rest)
                }}
                selected={currentlySelected[requirementType] ?? initiallySelected[requirementType]}>
                {allreqinst.map(o => <OptionMenu.Option key={o.source} value={o.source} label={<Label icon={requirementIcon} title={l(reqinst.lookupSource(o).name)} />} />)}
            </OptionMenu>)

    if (allprodinst.length > 0)
        filters.push(
            <OptionMenu placeholderIcon={productIcon} placeholder={t(productSingular)}
                setSelected={(selected) => {
                    const { [productType]: products, ...rest } = ctx.get('toggles') ?? {}
                    ctx.set(`toggles`)(selected ? { ...rest, [productType]: selected } : rest)
                }}
                selected={currentlySelected[productType] ?? initiallySelected[productType]}>
                {allprodinst.map(o => <OptionMenu.Option key={o.source} value={o.source} label={<Label icon={productIcon} title={l(prodinst.lookupSource(o).name)} />} />)}
            </OptionMenu>)

    if (allpartyinst.length > 0)
        filters.push(
            <OptionMenu placeholderIcon={tenantIcon} placeholder={t(tenantSingular)}
                setSelected={(selected) => {
                    const { [tenantType]: parties, ...rest } = ctx.get('toggles') ?? {}
                    ctx.set(`toggles`)(selected ? { ...rest, [tenantType]: selected } : rest)
                }}
                selected={currentlySelected[tenantType] ?? initiallySelected[tenantType]}>
                {allpartyinst.map(o => <OptionMenu.Option key={o.source} value={o.source} label={<Label icon={tenantIcon} title={l(partyinst.lookupSource(o).name)} />} />)}
            </OptionMenu>)

    const computeAndSetCalendarData = () => Promise.resolve().then(wait(150)).then(computeCalendarRows).then(calendarDataSet)

    const toggleBusy = useToggleBusy()

    const computeCalendar = () => toggleBusy(`computecampaigncalendar`, t("dashboard.calendar.generating"))

        .then(computeAndSetCalendarData)
        .catch(e => showAndThrow(e, t("dashboard.calendar.generating_error", { campaign: l(campaign.name) })))
        .finally(() => toggleBusy(`computecampaigncalendar`))

    return { calendarData, computeCalendar, initiallySelected, currentlySelected, filters } as const
}


export const usePartyCalendar = (props: { party: PartyInstance, partyCalendarGroup: string }): ComputedCalendarType => {

    const { party, partyCalendarGroup } = props

    const partyId = party.source

    const t = useT()
    const { l } = useLocale()

    const requirements = useRequirementStore()
    const products = useProductStore()
    const campaign = useCurrentCampaign()
    const requirementInstances = useRequirementInstances().on(campaign)
    const productInstances = useProductInstances().on(campaign)
    const eventInstances = useEventInstanceStore().on(campaign)
    const substore = useSubmissionStore().on(campaign)
    const revisions = useRevisions()

    const allRequirementInstances = requirementInstances.all()
    const allProductInstances = productInstances.all()

    const ctx = useFilterState(partyCalendarGroup)

    const { calendars = {} as CalendarContextType, setCalendars } = useCalendarContext()

    const regenerate = calendars.regenerate

    React.useEffect(() => {
        if (regenerate)
            computeAndSetCalendarData()

        //eslint-disable-next-line
    }, [regenerate])


    const [calendarData, calendarDataSet] = React.useState<CalendarRow[]>(calendars.party?.[partyId]!)

    const { buildCalendarRows, eventInstanceToCalendarRow } = useCalendar()

    const eventOptions = [campaignType, productType, requirementType]
    const actionOptions = ["assessed", "submitted"]

    const [partyRelatedRequirements, partyRelatedRequirementsSet] = React.useState<AssetInstance[]>(requirementInstances.allForParty(party))
    const [partyRelatedProducts, partyRelatedProductsSet] = React.useState<ProductInstanceDto[]>(productInstances.allForParty(party))

    //eslint-disable-next-line
    React.useEffect(() => partyRelatedRequirementsSet(requirementInstances.allForParty(party)), [allRequirementInstances])

    //eslint-disable-next-line
    React.useEffect(() => partyRelatedProductsSet(productInstances.allForParty(party)), [allProductInstances])

    const computeCalendarRows = async (): Promise<CalendarRow[]> => {

        /* All assets visible to party (audiance) */
        const partyRelatedAssets = [...partyRelatedRequirements, ...partyRelatedProducts]

        /* All events about campaign and visible assets */
        const partyRelatedEvents = [...eventInstances.allAbout(...partyRelatedAssets.map(a => a.source)), ...eventInstances.allOf(campaignType)]

        /* All trails for this party */
        const allPartyTrails = substore.allTrailsForParty(party)

        const eventTrailsMap = partyRelatedEvents.reduce((acc, cur) => {

            const trail = allPartyTrails.find(pt => pt.key.asset === cur.target)
            
            return { ...acc, [cur.id]: trail ?  revisions.on(trail).latest() : undefined }
        
        
        }, {})

        const submissionRows: CalendarRow[] = clock('building calendar submission rows', () => buildCalendarRows(allPartyTrails, 'party'))

        const eventRows: CalendarRow[] = clock('building calendar event rows', () => [...partyRelatedEvents.filter(e => e.date !== undefined).map(event => eventInstanceToCalendarRow(event, partyRelatedEvents, eventTrailsMap[event.id], party)).filter(row => !!row.date)])

        const calendarRows: CalendarRow[] = clock('merging submission and events calendar rows', () => [...eventRows, ...submissionRows!])

        const calendarParty = calendars.party ?? {}

        setCalendars({ ...calendars, party: { ...calendarParty, [partyId]: calendarRows } })

        return calendarRows

    }

    const initiallySelected = {
        [tenantType]: [party.source],
        [requirementType]: partyRelatedRequirements.map(r => r.source),
        [productType]: partyRelatedProducts.map(r => r.source),
        [eventType]: eventOptions,
        'action': []
        //eslint-disable-next-line
    }

    const currentlySelected = ctx.get('toggles') ?? initiallySelected

    const EventFilter = <OptionMenu placeholderIcon={eventIcon} placeholder={t("campaign.labels.filter_event_types")}
        setSelected={types => {
            const { [eventType]: events, ...rest } = ctx.get('toggles') ?? {}
            ctx.set('toggles')(types ? { ...rest, [eventType]: types } : rest)
        }}
        selected={currentlySelected[eventType] ?? initiallySelected[eventType]}>
        {[campaignType, requirementType, productType].map(o => <OptionMenu.Option key={o} value={o} label={<Label icon={eventIcon} title={typeMap(t)[o]} />} />)}
    </OptionMenu>

    const ActionFilter = <OptionMenu placeholderIcon={eventIcon} placeholder={t("campaign.labels.filter_action_types")}
        allOptionsAsUndef={false}
        setSelected={types => {
            const { 'action': actions, ...rest } = ctx.get('toggles') ?? {}
            ctx.set('toggles')(types ? { ...rest, 'action': types } : rest)
        }}
        selected={currentlySelected['action'] ?? initiallySelected['action']}>
        {actionOptions.map(o => <OptionMenu.Option key={o} value={o} label={<Label icon={eventIcon} title={typeMap(t)[o]} />} />)}
    </OptionMenu>


    let filters = [EventFilter, ActionFilter]


    if (partyRelatedRequirements.length > 0)
        filters.push(
            <OptionMenu placeholderIcon={requirementIcon} placeholder={t(requirementSingular)}
                setSelected={selected => {
                    const { [requirementType]: requirements, ...rest } = ctx.get('toggles') ?? {}
                    ctx.set(`toggles`)(selected ? { ...rest, [requirementType]: selected } : rest)
                }}
                selected={currentlySelected[requirementType] ?? initiallySelected[requirementType]}>
                {partyRelatedRequirements.map(o => <OptionMenu.Option key={o.source} value={o.source} label={<Label icon={requirementIcon} title={l(requirements.lookup(o.source)?.name)} />} />)}
            </OptionMenu>)

    if (partyRelatedProducts.length > 0)
        filters.push(
            <OptionMenu placeholderIcon={productIcon} placeholder={t(productSingular)}
                setSelected={(selected) => {
                    const { [productType]: products, ...rest } = ctx.get('toggles') ?? {}
                    ctx.set(`toggles`)(selected ? { ...rest, [productType]: selected } : rest)
                }}
                selected={currentlySelected[productType] ?? initiallySelected[productType]}>
                {partyRelatedProducts.map(o => <OptionMenu.Option key={o.source} value={o.source} label={<Label icon={productIcon} title={l(products.lookup(o.source)?.name)} />} />)}
            </OptionMenu>)

    const computeAndSetCalendarData = () => Promise.resolve().then(wait(150)).then(computeCalendarRows).then(calendarDataSet)

    const toggleBusy = useToggleBusy()


    const computeCalendar = () => toggleBusy(`computepartycalendar`, t("dashboard.calendar.generating"))

        .then(computeAndSetCalendarData)
        .catch(e => showAndThrow(e, t("dashboard.calendar.generating_error", { campaign: l(campaign.name) })))
        .finally(() => toggleBusy(`computepartycalendar`))

    return { calendarData, computeCalendar, currentlySelected, initiallySelected, filters } as const

}

export const useAssetCalendar = (props: { asset: RequirementInstance | ProductInstance, assetCalendarGroup: string }): ComputedCalendarType => {

    const { asset, assetCalendarGroup } = props

    const assetId = asset.source

    const t = useT()
    const { l } = useLocale()

    const tenants = useTenantStore()
    const campaign = useCurrentCampaign()

    const eventInstances = useEventInstanceStore().on(campaign)
    const substore = useSubmissionStore().on(campaign)
    const partyinststore = usePartyInstances().on(campaign)

    const ctx = useFilterState(assetCalendarGroup)

    const { calendars = {} as CalendarContextType, setCalendars } = useCalendarContext()

    const regenerate = calendars.regenerate

    React.useEffect(() => {
        if (regenerate)
            computeAndSetCalendarData()

        //eslint-disable-next-line
    }, [regenerate])

    const [calendarData, calendarDataSet] = React.useState<CalendarRow[]>(calendars.asset?.[assetId]!)

    const { buildCalendarRows, eventInstanceToCalendarRow } = useCalendar()

    const eventOptions = [campaignType]
    const actionOptions = ["assessed", "submitted"]

    const allPartyInstances = partyinststore.all()

    const [realatedPartyInstances, realatedPartyInstancesSet] = React.useState<PartyInstanceDto[]>(allPartyInstances)

    //eslint-disable-next-line
    React.useEffect(() => realatedPartyInstancesSet(allPartyInstances), [allPartyInstances])

    const computeCalendarRows = async (): Promise<CalendarRow[]> => {

        const assetRelatedEvents = [...eventInstances.allAbout(asset.source), ...eventInstances.allOf(campaignType)]

        /* All trails for this asset */
        const allAssetTrails = substore.allTrailsForAsset(asset)

        const submissionRows: CalendarRow[] = clock('building calendar submission rows', () => buildCalendarRows(allAssetTrails, 'asset'))

        const eventRows: CalendarRow[] = clock('building calendar event rows', () => [...assetRelatedEvents.filter(e => e.date !== undefined).map(event => eventInstanceToCalendarRow(event, assetRelatedEvents)).filter(row => row.date)])

        const calendarRows: CalendarRow[] = clock('merging submissions and events calendar rows', () => [...eventRows, ...submissionRows!])

        const assetCalendar = { ...calendars.asset ?? {}, [assetId]: calendarRows }

        setCalendars({ ...calendars, asset: assetCalendar })

        return calendarRows

    }

    const initiallySelected = {
        [tenantType]: realatedPartyInstances.map(p => p.source),
        [requirementType]: asset.instanceType === requirementType ? [asset.source] : [],
        [productType]: asset.instanceType === productType ? [asset.source] : [],
        [eventType]: eventOptions,
        'action': actionOptions
    }

    const currentlySelected = ctx.get('toggles') ?? initiallySelected

    const EventFilter = <OptionMenu placeholderIcon={eventIcon} placeholder={t("campaign.labels.filter_event_types")}
        setSelected={types => {
            const { [eventType]: events, ...rest } = ctx.get('toggles') ?? {}
            ctx.set('toggles')(types ? { ...rest, [eventType]: types } : rest)
        }}
        selected={currentlySelected[eventType] ?? initiallySelected[eventType]}>
        {eventOptions.map(o => <OptionMenu.Option key={o} value={o} label={<Label icon={eventIcon} title={typeMap(t)[o]} />} />)}
    </OptionMenu>

    const ActionFilter = <OptionMenu placeholderIcon={eventIcon} placeholder={t("campaign.labels.filter_action_types")}
        setSelected={types => {
            const { 'action': actions, ...rest } = ctx.get('toggles') ?? {}
            ctx.set('toggles')(types ? { ...rest, 'action': types } : rest)
        }}
        selected={currentlySelected['action'] ?? initiallySelected['action']}>
        {actionOptions.map(o => <OptionMenu.Option key={o} value={o} label={<Label icon={eventIcon} title={typeMap(t)[o]} />} />)}
    </OptionMenu>

    let filters = [EventFilter, ActionFilter]

    if (realatedPartyInstances.length > 0)
        filters.push(
            <OptionMenu placeholderIcon={tenantIcon} placeholder={t(tenantSingular)}
                setSelected={(selected) => {
                    const { [tenantType]: parties, ...rest } = ctx.get('toggles') ?? {}
                    ctx.set('toggles')(selected ? { ...rest, [tenantType]: selected } : rest)
                }}
                selected={currentlySelected[tenantType] ?? initiallySelected[tenantType]}>
                {realatedPartyInstances.map(o => <OptionMenu.Option key={o.source} value={o.source} label={<Label icon={tenantIcon} title={l(tenants.lookup(o.source)?.name)} />} />)}
            </OptionMenu>)

    const computeAndSetCalendarData = () => Promise.resolve().then(wait(150)).then(computeCalendarRows).then(calendarDataSet)

    const toggleBusy = useToggleBusy()


    const computeCalendar = () => toggleBusy(`computeassetcalendar`, t("dashboard.calendar.generating"))

        .then(computeAndSetCalendarData)
        .catch(e => showAndThrow(e, t("dashboard.calendar.generating_error", { campaign: l(campaign.name) })))
        .finally(() => toggleBusy(`computeassetcalendar`))

    return { calendarData, computeCalendar, currentlySelected, initiallySelected, filters } as const

}