import { Divider, Row } from 'antd'
import { Label } from '#app/components/Label'
import { tenantIcon, tenantSingular } from '#app/tenant/constants'
import { useCurrentCampaign } from '#campaign/hooks'
import { submissionIcon } from '#campaign/submission/constants'
import { SubmissionLabel } from '#campaign/submission/Label'
import { useSubmissionStore } from '#campaign/submission/store'
import { AssetSummary } from '#campaign/submission/statistics/api'
import { PartySummary } from '#campaign/submission/statistics/api'
import { productIcon, productSingular, productType } from '#product/constants'
import { requirementIcon, requirementSingular, requirementType } from '#requirement/constants'
import * as React from 'react'
import { useT } from '#app/intl/api'
import { useCurrentAsset, useCurrentParty, useCurrentDashboard } from './hooks'
import { StatCol } from './SummaryCard'



export const LatestSubmissions = () => {

	const t = useT()

	const dashboard = useCurrentDashboard()

	const { party } = useCurrentParty()
	const { asset } = useCurrentAsset()

	const partySummaries = dashboard.summaries.staticparties

	const content = party ?

		<LatestPartySubmissions party={partySummaries[party.source]} />

		: asset ?

			<LatestAssetSubmissions asset={dashboard.summaries[asset.instanceType][asset.source]} />

			: <AllLatestSubmissions />

	return <React.Fragment>

		<Divider orientation='left'>
			<Label icon={submissionIcon} title={asset? t("dashboard.summary.latest_subs.name_singular") : t("dashboard.summary.latest_subs.name")} />
		</Divider>

		{content}

	</React.Fragment>

}


const AllLatestSubmissions = () => {

	const t = useT()

	const dashboard = useCurrentDashboard()

	const substore= useSubmissionStore().on(useCurrentCampaign())

	const requirement = dashboard.summaries[requirementType].lastSubmittedRequirement
	const requirementRoute = () => dashboard.assetRouteToSubmission(requirement!)
	const product = dashboard.summaries[productType].lastSubmittedProduct
	const productRoute = () => dashboard.assetRouteToSubmission(product!)
	
	return <React.Fragment>

		<Row >

			<StatCol className='small-stat' span={16} title={t(requirementSingular)}>
				{
					requirement ?
						<SubmissionLabel icon={requirementIcon} linkTo={requirementRoute} displayMode='asset' submission={requirement} />
						: <Label icon={requirementIcon} title={t("dashboard.summary.latest_subs.none")} />

				}
			</StatCol>

			<StatCol className='small-stat' title={t(tenantSingular)}>
				{
					requirement ? <SubmissionLabel icon={tenantIcon} linkTo={() => dashboard.routeToParty(substore.lookupTrail(requirement.trail)?.key.party!)} displayMode='party' submission={requirement} />
						: <Label icon={tenantIcon} title={t("dashboard.summary.latest_subs.none")} />
				}
			</StatCol>


		</Row>

		<Row >

			<StatCol className='small-stat' span={16} title={t(productSingular)}>
				{
					product ?
						<SubmissionLabel icon={productIcon} linkTo={productRoute} displayMode='asset' submission={product} />
						: <Label icon={productIcon} title={t("dashboard.summary.latest_subs.none")} />

				}
			</StatCol>

			<StatCol className='small-stat' title={t(tenantSingular)}>
				{
					product ?
						<SubmissionLabel icon={tenantIcon} linkTo={() => dashboard.routeToParty(substore.lookupTrail(product.trail)?.key.party!)} displayMode='party' submission={product!} />
						: <Label icon={tenantIcon} title={t("dashboard.summary.latest_subs.none")} />

				}
			</StatCol>


		</Row>

	</React.Fragment>
}



type PartyProps = {

	party: PartySummary
}

const LatestPartySubmissions = (props: PartyProps) => {

	const t = useT()

	const { party } = props

	const dashboard = useCurrentDashboard()

	const requirement = party.lastSubmittedRequirement
	const product = party.lastSubmittedProduct

	return <React.Fragment>

		<Row >

			<StatCol className='small-stat' span={16} title={t(requirementSingular)}>
				{
					requirement ?
						<SubmissionLabel icon={requirementIcon} linkTo={dashboard.assetRouteToSubmission(requirement)} displayMode='asset' submission={requirement} />
						: <Label icon={requirementIcon} title={t("dashboard.summary.latest_subs.none")} />

				}
			</StatCol>


			{
				requirement &&

				<StatCol className='small-stat' title={t("dashboard.summary.latest_subs.date")}>
					<SubmissionLabel noPseudoNames linkTo={dashboard.assetRouteToSubmission(requirement)} submission={requirement} />
				</StatCol>

			}


		</Row>

		<Row >

			<StatCol className='small-stat' span={16} title={t(productSingular)}>
				{
					product ?
						<SubmissionLabel icon={productIcon} linkTo={dashboard.assetRouteToSubmission(product)} displayMode='asset' submission={product} />
						: <Label icon={productIcon} title={t("dashboard.summary.latest_subs.none")} />

				}

			</StatCol>

			{
				product &&

				<StatCol className='small-stat' title={t("dashboard.summary.latest_subs.date")}>
					<SubmissionLabel noPseudoNames linkTo={dashboard.assetRouteToSubmission(product)} displayMode='date' submission={product} />
				</StatCol>

			}


		</Row>

	</React.Fragment>

}


type AssetProps = {

	asset: AssetSummary
}


const LatestAssetSubmissions = (props: AssetProps) => {

	const t = useT()

	const { asset } = props

	const dashboard = useCurrentDashboard()

	const latest = asset.lastSubmitted

	return <React.Fragment>

		<Row >

			<StatCol span={12} className='small-stat' title={t(tenantSingular)}>
				{
					latest ? <SubmissionLabel icon={tenantIcon} linkTo={() => dashboard.assetRouteToSubmission(latest)} displayMode='party' submission={latest} />
						: <Label icon={tenantIcon} title={t("dashboard.summary.latest_subs.none")} />
				}
			</StatCol>

			{
				latest &&

				<StatCol span={12}  className='small-stat' title={t("dashboard.summary.latest_subs.date")}>
					<SubmissionLabel noPseudoNames linkTo={dashboard.assetRouteToSubmission(latest)} displayMode='date' submission={latest} />
				</StatCol>

			}

		</Row>

	</React.Fragment>

}