

import { Divider, Progress, Row, Tooltip } from 'antd'
import { Label } from '#app/components/Label'
import { Text } from '#app/components/Typography'
import { tenantIcon, tenantPlural, tenantType } from "#app/tenant/constants"
import { CampaignInstance } from '#campaign/model'
import { PartyInstance } from '#campaign/party/api'
import { performanceGroups, submissionIcon } from "#campaign/submission/constants"
import { AggregateSummaries } from '#campaign/submission/statistics/aggregateMany'
import { productIcon, productPlural, productSingular, productType } from "#product/constants"
import { requirementIcon, requirementPlural, requirementSingular, requirementType } from "#requirement/constants"
import { useT } from '#app/intl/api'
import { useCurrentAsset, useCurrentParty, useCurrentDashboard } from "./hooks"
import { HorizonRow } from './SubmissionHorizon'
import { PerformanceCard, StatCol } from "./SummaryCard"
import { LatestSubmissions } from './SummaryLatestSubmissions'
import { AssetSummary, PartySummary } from '#campaign/submission/statistics/api'



type Props = {

    active: boolean
}

export const SubmissionCard = (props: Props) => {

    const { asset } = useCurrentAsset()
    const { party } = useCurrentParty()

    const { active } = props

    if (!active)
        return null;

    return asset ? <AssetCard asset={asset} /> : party ? <PartyCard party={party} /> : <AllCard />

}


export const AllCard = () => {

    const t = useT()

    const dashboard = useCurrentDashboard()

    const selector = (type: string) => {

        const summaries: AggregateSummaries = dashboard.summaries[type]

        return {
            
            tips: {
                1: t(`dashboard.summary.performance.tips.submission_other.${type}`, { range: performanceGroups.submission[1].label }),
                2: t(`dashboard.summary.performance.tips.submission_other.${type}`, { range: performanceGroups.submission[2].label }),
                3: t(`dashboard.summary.performance.tips.submission_other.${type}`, { range: performanceGroups.submission[3].label }),
                4: t(`dashboard.summary.performance.tips.submission_best.${type}`),

                overall: t(`dashboard.summary.performance.tips.overall.submission.${type}`)
            },

            statstype: 'submission' as const,
            groups: summaries.submissionGroups,
            performanceRate: summaries.singleSubmissionRate,
            performanceClass: summaries.singleSubmissionClass,
        }
    }


    // at this top-evel of aggregation we needs to combine somehow stats about requirements and reports.
    // averaging missing and late rates hovever seems skewed, as reports are much less than requirements.
    // so we recompute them here at the level of individual submissions.

    const reqstats = dashboard.summaries[requirementType]
    const prodstats = dashboard.summaries[productType]

    const missingTotal = reqstats.submittedAsMissing + prodstats.submittedAsMissing
    const missingRate = Math.round( missingTotal /  Math.max(1, reqstats.submitted + prodstats.submitted + missingTotal) *100 )

    const lateRate = Math.round((reqstats.late().length + prodstats.late().length) / (reqstats.submitted + prodstats.submitted) *100)
  
    return <PerformanceCard selector={selector}>

        <Divider orientation='left'><Label icon={submissionIcon} title={t('dashboard.summary.stats')} /></Divider>

        <Row >

            <StatCol span={6} title={t("dashboard.summary.performance.submission.missing")} tip={t('dashboard.summary.performance.submission.parties_missing_rate_tip')}>
                <Label icon={submissionIcon} title={<Text emphasis='error'>{isNaN(missingRate) ? '0%' : `${missingRate}%`}</Text>} />
            </StatCol>

            <StatCol span={6} title={t("dashboard.summary.performance.submission.late")} tip={t('dashboard.summary.performance.submission.late_rate_submitted_tip')}>
                <Label icon={submissionIcon} title={<Text emphasis='warning'>{isNaN(lateRate) ? '0%' : `${lateRate}%`}</Text>} />
            </StatCol>

            <StatCol span={6} title={t('dashboard.summary.performance.submission.pending')} tip={t('dashboard.summary.performance.submission.pending_submitted_tip')}>
                <Label icon={submissionIcon} title={<Text>{reqstats.pending + prodstats.pending}</Text>} />
            </StatCol>

            <StatCol span={6} title={t('dashboard.summary.performance.submission.draft')} tip={t('dashboard.summary.performance.submission.draft_submitted_tip')}>
                <Label icon={submissionIcon} title={<Text>{reqstats.drafts + prodstats.drafts}</Text>} />
            </StatCol>

        </Row>

    </PerformanceCard>

}



export const PartyCard = (props: { party: PartyInstance }) => {

    const t = useT()

    const { party } = props

    const dashboard = useCurrentDashboard()

    const stats = dashboard.summaries[tenantType][party.source] as PartySummary

    const lateRate = stats.submissionProfile.lateRate()

    return <div className="progress-card">

        <div className="part-graph">

            <Progress className={stats.submissionProfile.submissionClass} width={180} strokeWidth={5} type="circle" percent={stats.submissionProfile.rate}
                format={v => <Tooltip title={

                    t(`dashboard.summary.performance.submission.party_rate_tip`)

                }><span className={stats.submissionProfile.submissionClass}>{isNaN(v ?? NaN) ? '--' : `${v}%`}</span></Tooltip>} />

            <div className="graph-stats">

                <Row>
                    <StatCol title={t("dashboard.summary.performance.submission.asset_due")} tip={t('dashboard.summary.performance.submission.party_asset_due_tip')}>
                        <Label icon={submissionIcon} title={<Text emphasis='primary'>{stats.dueAssets.length}</Text>} />
                    </StatCol>
                </Row>

                <Row>
                    <StatCol title={t(requirementPlural)} tip={t('dashboard.summary.performance.submission.party_asset_submitted_tip', { plural: t(requirementPlural).toLowerCase() })}>
                        <Label icon={requirementIcon} title={<Text emphasis='primary'>{stats.submittedRequirements.length}/<Text secondary>{stats.dueRequirements.length}</Text></Text>} />
                    </StatCol>
                </Row>

                <Row>
                    <StatCol title={t(productPlural)} tip={t('dashboard.summary.performance.submission.party_asset_submitted_tip', { plural: t(productPlural).toLowerCase() })}>
                        <Label icon={productIcon} title={<Text emphasis='primary'>{stats.submittedProducts.length}/<Text secondary>{stats.dueProducts.length}</Text></Text>} />
                    </StatCol>
                </Row>

                <Row>
                    <StatCol span={8} title={t("dashboard.summary.performance.submission.late")} tip={t('dashboard.summary.performance.submission.party_late_rate_submitted_tip')}>
                        <Label icon={submissionIcon} title={<Text emphasis='warning'>{isNaN(lateRate) ? '0%' : `${lateRate}%`}</Text>} />
                    </StatCol>
                </Row>

            </div>
        </div>

        <HorizonRow />

        <div className="part-numbers">

            <Divider orientation='left'><Label icon='#' title={t("dashboard.summary.stats")} /></Divider>

            <Row >

                <StatCol span={8} title={t("dashboard.summary.performance.submission.late")} tip={t('dashboard.summary.performance.submission.party_late_submitted_tip')} >
                    <Label icon={submissionIcon} title={<Text emphasis='warning'>{stats.late().length}</Text>} />
                </StatCol>


                <StatCol span={8} title={t('dashboard.summary.performance.submission.pending')} tip={t('dashboard.summary.performance.submission.party_pending_submitted_tip')}>
                    <Label icon={submissionIcon} title={<Text>{stats.pending.length}</Text>} />
                </StatCol>

                <StatCol span={8} title={t("dashboard.summary.performance.submission.draft")} tip={t('dashboard.summary.performance.submission.party_draft_submitted_tip')}>
                    <Label icon={submissionIcon} title={<Text>{stats.draftProducts.length}</Text>} />
                </StatCol>

            </Row>

            <LatestSubmissions />

        </div>

    </div>

}


export const AssetCard = (props: { asset: CampaignInstance }) => {

    const t = useT()

    const { asset } = props

    const dashboard = useCurrentDashboard()

    const stats = dashboard.summaries[asset.instanceType][asset.source] as AssetSummary

    const singular = t(asset.instanceType === requirementType ? requirementSingular : productSingular).toLowerCase()

    const lateRate = stats.submissionProfile.lateRate()

    return <div className="progress-card">

        <div className="part-graph" style={{ marginTop: 20 }}>

            <Progress className={stats.submissionProfile.submissionClass} width={180} strokeWidth={5} type="circle" percent={stats.submissionProfile.rate}
                format={v => <Tooltip title={t('dashboard.summary.performance.submission.asset_rate_tip', { singular })}>
                    <span className={stats.submissionProfile.submissionClass}>{isNaN(v ?? NaN) ? '--' : `${v}%`}</span>
                </Tooltip>} />

            <div className="graph-stats">

                <Row>
                    <StatCol title={t('dashboard.summary.performance.submission.due', { plural: t(tenantPlural) })} tip={t('dashboard.summary.performance.submission.party_count_tip', { singular })}>
                        <Label icon={tenantIcon} title={stats.dueParties.length} />
                    </StatCol>
                </Row>

                <Row>
                    <StatCol title={t('dashboard.summary.performance.submission.submitted')} tip={t('dashboard.summary.performance.submission.asset_submitted_tip', { singular })}>
                        <Label icon={tenantIcon} title={stats.submitted.length} />
                    </StatCol>
                </Row>

                <Row>
                    <StatCol title={t('dashboard.summary.performance.submission.missing')} tip={t('dashboard.summary.performance.submission.party_missing_rate_tip', { singular })}>
                        <Label icon={tenantIcon} title={<Text emphasis='error'>{isNaN(stats.submissionProfile.missingRate) ? '0%' : `${stats.submissionProfile.missingRate}%`}</Text>} />
                    </StatCol>
                </Row>

                <Row>
                    <StatCol title={t("dashboard.summary.performance.submission.late")} tip={t('dashboard.summary.performance.submission.asset_laterate_tip', { singular })}>
                        <Label icon={submissionIcon} title={<Text emphasis='warning'>{isNaN(lateRate) ? '0%' : `${lateRate}%`}</Text>} />
                    </StatCol>
                </Row>

            </div>
        </div>

        <div className="part-numbers">

            <Divider style={{ marginTop: 40 }} orientation='left'><Label icon='#' title={t("dashboard.summary.stats")} /></Divider>

            <Row >

                <StatCol span={6} title={t('dashboard.summary.performance.submission.missing')} tip={t('dashboard.summary.performance.submission.party_missing_tip', { singular })}>
                    <Label icon={tenantIcon} title={<Text emphasis='error'>{stats.submittedAsMissing.length}</Text>} />
                </StatCol>

                <StatCol span={6} title={t("dashboard.summary.performance.submission.late")} tip={t('dashboard.summary.performance.submission.asset_late_submitted_tip', { singular })}>
                    <Label icon={submissionIcon} title={<Text emphasis='warning'>{stats.late().length}</Text>} />
                </StatCol>

                <StatCol span={6} title={t('dashboard.summary.performance.submission.pending')} tip={t('dashboard.summary.performance.submission.asset_pending_submitted_tip', { singular })}>
                    <Label icon={tenantIcon} title={<Text>{stats.pending.length}</Text>} />
                </StatCol>

                <StatCol span={6} title={t("dashboard.summary.performance.submission.draft")} tip={t('dashboard.summary.performance.submission.asset_draft_submitted_tip', { singular })}>
                    <Label icon={tenantIcon} title={<Text>{stats.drafts.length}</Text>} />
                </StatCol>

            </Row>

            <LatestSubmissions />

        </div>

    </div>

}
