import { useT } from '#app/intl/api'
import { useLocale } from '#app/model/hooks'
import { noTenant, tenantType } from '#app/tenant/constants'
import { useTenantStore } from '#app/tenant/store'
import { useLogged } from '#app/user/store'
import { anyPartyTopic } from '#campaign/constants'
import { useCurrentCampaign } from '#campaign/hooks'
import { useMessageHelper } from '#campaign/messagehelper'
import { CampaignInstance, useCampaignModel } from '#campaign/model'
import { PartyInstance } from '#campaign/party/api'
import { useProductInstances } from '#campaign/product/api'
import { useRequirementInstances } from '#campaign/requirement/api'
import { useSubmissionProfile } from '#campaign/submission/profile'
import { FlowViewer } from '#messages/VirtualFlow'
import { messageType } from '#messages/constants'
import { Flow, Message } from '#messages/model'
import { productType } from '#product/constants'
import { useProductStore } from '#product/store'
import { requirementType } from '#requirement/constants'
import { useRequirementStore } from '#requirement/store'
import { DashboardPage } from './DashboardPage'
import { useCurrentAsset, useCurrentParty } from './hooks'
import { useMessageMailProfile } from './mailprofile'
import { MessagePartySelector, useAssetPartySelector } from './messagepartyselector'

export const MessageViewer = () => {

    const { party } = useCurrentParty()
    const { asset } = useCurrentAsset()

    const component = party ? <PartyMessageViewer party={party} /> : asset ? <AssetMessageViewer asset={asset} /> : <CampaignMessageViewer />

    return component
}




// ----------------------------------   campaign flow

const CampaignMessageViewer = () => {

    return <DashboardPage tab={messageType}  >
        <CampaignMessageFlow />
    </DashboardPage>
}

type CampaignMessageFlowProps = {


    summary?: boolean
}

export const useCampaignMessages = (summary?: boolean) => {

    const campaign = useCurrentCampaign()

    const messages = useMessageHelper()

    const profileOfMessage = useMessageMailProfile()


    const flowId = campaign.id

    const flow: Flow = {

        scope: campaign.id,
        
        mailProfile: profileOfMessage

        ,

        channels: [{
            name: "main",
            channel: {
                read: messages.campaignTopics(campaign.id),
                noPost: !!summary,
                noReply: !!summary
            }
        }]
    }

    return { id: flowId, flow }
}

export const CampaignMessageFlow = (props: CampaignMessageFlowProps) => {

    const t = useT()

    const { summary } = props

    const { id, flow } = useCampaignMessages(summary)

    const logged = useLogged()
    const model = useCampaignModel()
    const campaign = useCurrentCampaign()


    const readonly = !logged.isAdmin() || model.isArchived(campaign) || model.isMuted(campaign)

    return <FlowViewer id={id} flow={flow} multiTenant collapsed={!summary}
                     collapsedMessage={t('campaign.labels.broadcast_message')} readOnly={readonly} 
                     dynamicsEditor={MessagePartySelector} />

}





// ----------------------------------   party flow

export const usePartyMessages = (party: PartyInstance, summary?: boolean) => {

    const campaign = useCurrentCampaign()
    const campaignTopics = useCampaignModel().topics(campaign)

    const messages = useMessageHelper()

    const requirements = useRequirementStore()
    const products = useProductStore()
    const tenants = useTenantStore()

    const profileOfMessage = useMessageMailProfile()

    let flowId
    let flow: Flow | undefined


    if (party) {

        const source = tenants.safeLookup(party.source)

        flowId = party.id
        const partyTopics = messages.partyTopics(campaign.id, party.source)

        flow = {

            scope: campaign.id,
            recipient: party.source,

            mailProfile: profileOfMessage,

            channels: [

                {
                    name: "party",
                    channel: {
                        read: [...campaignTopics, ...partyTopics],
                        noPost: !!summary,
                        noReply: !!summary
                    }
                }

                ,

                {
                    name: "broadcast-asset",
                    channel: {
                        read: [...campaignTopics, anyPartyTopic],
                        // on replies, replaces anyparty topic with specific party
                        onReply: m => ({ ...m, topics: m.topics.flatMap(t => t.type === tenantType && t.name === anyPartyTopic.name ? partyTopics : [t]) }),
                        noPost: true,
                        noReply: !!summary,

                        // must discover from topics and add the asset of this broadcast as an email parameter.
                        // so it can be referenced in the target mail template.
                        mailProfile: (message: Message) => {


                            const assetTopic = message.topics.find(t => t.type === requirementType || t.type === productType)

                            if (!assetTopic)
                                throw new Error(`no asset topic in ${message}`)


                            const asset = (assetTopic.type === requirementType ? requirements : products).lookup(assetTopic.name.split(':')?.[1])

                            if (!asset)
                                throw new Error(`can't resolve asset for topic ${assetTopic}`)

                            return {
                                targets: [noTenant, party.source],
                                parameters: {
                                    campaign: campaign.name,
                                    campaignId: campaign.id,
                                    tenant: source.name,
                                    tenantId: source.id,
                                    aassetType: assetTopic.type,
                                    asset: asset.name,
                                    assetId: asset.id,
                                    assetTitle: asset.description
                                }
                            }
                        }

                    }
                }

                ,

                {
                    name: "broadcast",
                    channel: {
                        read: campaignTopics,
                        write: [...campaignTopics, ...partyTopics],
                        mode: 'strict',
                        noPost: true,
                        noReply: !!summary
                    }
                }
            ]
        } as Flow

    }


    return { id: flowId, flow: flow! }
}

type PartyProps = {
    party: PartyInstance
}

type PartyMessageFlowProps = PartyProps & {

    summary?: boolean
}


export const PartyMessageFlow = (props: PartyMessageFlowProps) => {

    const { summary } = props

    const { party } = props

    const { id, flow } = usePartyMessages(party, summary)

    const logged = useLogged()

    const active = logged.isTenantManager() || logged.isManagerOf(party.id) || logged.isManagerOf(party.id, 'direct') || logged.isAdmin() || logged.isMultiManager() || logged.hasNoTenant()

    const campaignmodel = useCampaignModel()
    const campaign = useCurrentCampaign()

    const reqinst = useRequirementInstances().on(campaign)
    const prodinst = useProductInstances().on(campaign)

    const readonly = !active || campaignmodel.isArchived(campaign) || campaignmodel.isMuted(campaign)

    return <FlowViewer id={id} key={party.source} flow={flow} readOnly={readonly}
        filterTopicBy={t => t.type !== tenantType || t.name.includes(party.source)}
        filterMessageBy={m =>
             m.topics.every(t => 
                
                (t.type !== requirementType || reqinst.isForParty(party,reqinst.safeLookupBySource(t?.name.split(':')?.[1]))) &&
                (t.type !== productType || prodinst.isForParty(party,prodinst.safeLookupBySource(t?.name.split(':')?.[1])))
            
            
            )  
        }
    // splitWith={summary ? undefined : party.source} 
    />
}

const PartyMessageViewer = (props: PartyProps) => {

    const { l } = useLocale()

    const party = useCurrentParty()

    const logged = useLogged()

    const title = logged.hasNoTenant() ? l(party?.tenant.name) : undefined

    return <DashboardPage title={title} tab={messageType}  >
        <PartyMessageFlow {...props} />
    </DashboardPage>
}







// ----------------------------------   asset flow

export const useAssetMessages = (asset: CampaignInstance, summary?: boolean) => {

    const campaign = useCurrentCampaign()

    const messages = useMessageHelper()

    const campaignTopics = useCampaignModel().topics(campaign)

    const profileOfMessage = useMessageMailProfile()

    let flowId
    let flow: Flow

    if (asset) {


        flowId = asset.id
        const assettopics = messages.assetTopics(asset.instanceType, campaign.id, asset.source)

        flow = {

            scope: campaign.id,

            mailProfile: profileOfMessage,

            channels: [
                {
                    name: asset.instanceType,
                    channel: {
                        read: [...campaignTopics, ...assettopics],
                        noPost: true,
                        noReply: !!summary
                    }
                }
                ,
                {
                    name: "broadcast",
                    channel: {
                        read: [...campaignTopics, ...assettopics],
                        noPost: !!summary,
                        noReply: !!summary,
                        write: [...campaignTopics, ...assettopics, anyPartyTopic]
                    }
                }
            ]
        } as Flow

    }

    return { id: flowId, flow: flow! }
}

type AssetProps = {
    asset: CampaignInstance
}

const AssetMessageViewer = (props: AssetProps) => {

    const asset = useCurrentAsset()

    const logged = useLogged()

    const title = logged.hasNoTenant() ? asset.nameOf(asset.asset!) : undefined

    return <DashboardPage title={title} tab={messageType}  >
        <AssetMessageFlow {...props} />
    </DashboardPage>

}

type AssetMessageFlowProps = AssetProps & {

    summary?: boolean
}


export const AssetMessageFlow = (props: AssetMessageFlowProps) => {


    const t = useT()
    const { l } = useLocale()

    const campaign = useCurrentCampaign()
    const subprofile = useSubmissionProfile().on(campaign)
    const campaignproperties = useCampaignModel()

    const { asset, summary } = props

    const MessagePartySelector = useAssetPartySelector(asset)

    const { id, flow } = useAssetMessages(asset, summary)

    const logged = useLogged()

    const active = logged.isAdmin() || logged.isMultiManager()

    const readonly = !active || campaignproperties.isArchived(campaign) || campaignproperties.isMuted(campaign)

    return <FlowViewer id={id} key={asset.source} flow={flow} multiTenant 
    
                    collapsed={!summary} readOnly={readonly}
                    collapsedMessage={t('campaign.labels.broadcast_message_about', { topic: l(subprofile.profileOf(asset.instanceType).source(asset.source).name) })} 
                    dynamicsEditor={MessagePartySelector} />
}