import { ResponsivePie } from '@nivo/pie'
import { Col, Empty, Progress, Radio, Row, Statistic, Tooltip } from 'antd'
import { Label } from '#app/components/Label'
import { RadioBox } from '#app/form/RadioBox'
import { icns } from '#app/icons'
import { tenantIcon, tenantSingular, tenantType } from '#app/tenant/constants'
import { complianceIcon, complianceType } from '#campaign/constants'
import { performanceGroups, StatsType } from '#campaign/submission/constants'
import { productIcon, productSingular, productType } from '#product/constants'
import { requirementIcon, requirementSingular, requirementType } from '#requirement/constants'
import * as React from 'react'
import { useT } from '#app/intl/api'
import { useCurrentAsset, useCurrentDashboard } from './hooks'
import { HorizonRow } from './SubmissionHorizon'
import { LatestSubmissions } from './SummaryLatestSubmissions'

export type SelectFunction = (type: string) => {


    statstype: StatsType
    groups: Record<1 | 2 | 3 | 4, any[]>
    tips: Record<string, React.ReactNode>
    performanceRate: number
    performanceClass: string

}

export type Props = React.PropsWithChildren<{

    noHorizon?: boolean
    types?: string[]
    selector: SelectFunction

}>

export const tipDelay = 0.5

const allTypes = [tenantType, requirementType, productType]

export const PerformanceCard = (props: Props) => {

    const t = useT()

    const { selector, noHorizon, types = allTypes } = props

    const { asset } = useCurrentAsset()

    const dashboard = useCurrentDashboard()

    const lastView =  dashboard.summaryRateView()
    const view =  lastView && types.includes(lastView) ? lastView : types[0]

    let views = [] as JSX.Element[]

    if (types.includes(complianceType))
        views.push(<Radio.Button key={complianceType} value={complianceType}><Tooltip mouseEnterDelay={0.5} title={t("dashboard.summary.performance.by_tip", { singular: t('dashboard.summary.performance.asset_singular') })}>{complianceIcon}</Tooltip></Radio.Button>)

    if (types.includes(tenantType))
        views.push(<Radio.Button key={tenantType} value={tenantType}><Tooltip mouseEnterDelay={0.5} title={t("dashboard.summary.performance.by_tip", { singular: t(tenantSingular) })}>{tenantIcon}</Tooltip></Radio.Button>)

    if (types.includes(requirementType))
        views.push(<Radio.Button key={requirementType} value={requirementType}><Tooltip mouseEnterDelay={0.5} title={t("dashboard.summary.performance.by_tip", { singular: t(requirementSingular) })}>{requirementIcon}</Tooltip></Radio.Button>)

    if (types.includes(productType))
        views.push(<Radio.Button key={productType} value={productType}><Tooltip mouseEnterDelay={0.5} title={t("dashboard.summary.performance.by_tip", { singular: t(productSingular) })}>{productIcon}</Tooltip></Radio.Button>)

    return <div className="performance-summary-card">

        <div className='graph-contents'>

            <div className="graph-controls">


                {types.length > 1 &&
                    <RadioBox className="pie-switch" size="large" value={view} onChange={dashboard.setSummaryRateView}>
                        {views}
                    </RadioBox>
                }

            </div>

            {

                view === complianceType ?

                    <PerformanceView targetIcon={complianceIcon} {...selector(complianceType)} /> :

                    view === productType ?

                        <PerformanceView targetIcon={productIcon} {...selector(productType)} /> :

                        view === requirementType ?

                            <PerformanceView targetIcon={requirementIcon} {...selector(requirementType)} /> :

                            view === tenantType ?

                                <PerformanceView targetIcon={tenantIcon} {...selector(tenantType)} />

                                : null
            }



        </div>

        {!!asset || noHorizon || <HorizonRow disabled={view !== tenantType} />}

        <div className="part-numbers">
            {props.children}
        </div>

        <LatestSubmissions />

    </div>

}



type ViewProps = ReturnType<SelectFunction> & {

    targetIcon: React.ReactNode
}

export const PerformanceView = (props: ViewProps) => {

    const t = useT()

    const { statstype, groups, targetIcon, tips } = props

    const groupCounts = Object.keys(groups).reduce((acc, k) => ({ ...acc, [k]: groups[k]?.length }), {} as Record<1 | 2 | 3 | 4, number>)

    const [chart, flipChart] = React.useState<boolean>(false)

    return <div className="part-graph">

        <div className="graph-chart" onClick={() => flipChart(c => !c)}>

            <div style={{ height: 180 }}>{chart ? <PieChart  {...props} groupCounts={groupCounts} /> : <ProgressChart {...props} />}</div>

            <div className='chart-selector'>
                <Tooltip placement='bottom' title={t('dashboard.summary.performance.progress_bar')} className={`selector-dot dot-${chart ? 'inactive' : 'active'}`}>{icns.dot}</Tooltip>
                <Tooltip placement='bottom' title={t('dashboard.summary.performance.pie_chart')} className={`selector-dot dot-${chart ? 'active' : 'inactive'}`}>{icns.dot}</Tooltip>
            </div>

        </div>

        <div className="graph-stats"> {Object.entries(groups).map(([ordinal]) =>

            <Row key={ordinal}>
                <StatCol span={24} title={t(`dashboard.summary.performance.groups.${statstype}.${ordinal}`)}>
                    <Label tipClassName={"summary-tooltip"} tipDelay={tipDelay} tip={tips[ordinal] ? () => tips[ordinal] : undefined} iconStyle={{ color: performanceGroups[statstype][ordinal].color }} icon={targetIcon} title={groupCounts[ordinal] ?? 0} />
                </StatCol>
            </Row>

        )}
        </div>

    </div>

}


type StatProps = {

    span?: number
    title: React.ReactNode
    tip?: React.ReactNode

    children: React.ReactNode | JSX.Element

    className?: string

}

export const StatCol = (props: StatProps) => {

    const { span = 8, tip, title, className, children: value } = props

    const plainvalue = typeof 'value' in ['number', 'string']

    const col = <Col className="stat-col" span={span}>
        <Statistic className={className} title={title} value={plainvalue ? value as any : -1} formatter={() => value} />
    </Col>

    return tip ?

        <Tooltip mouseEnterDelay={tipDelay} title={tip}>
            {col}
        </Tooltip>

        : col
}


type ProgressProps = ViewProps & {


}



const ProgressChart = (props: ProgressProps) => {

    const { performanceRate, performanceClass, tips } = props

    const rate = v => <div className={performanceClass}>{isNaN(performanceRate) ? '--' : `${v}%`}</div>

    const bullseye = v => tips?.overall ?

        <Tooltip overlayClassName="summary-tooltip" mouseEnterDelay={tipDelay} title={tips.overall}>

            {rate(v)}

        </Tooltip>

        :

        rate(v)


    return <Progress className={performanceClass} width={180} strokeWidth={5} type="circle" percent={performanceRate} format={bullseye} />


}




type PieProps = ViewProps & {

    groupCounts: Record<3 | 1 | 2 | 4, number>
}




const PieChart = (props: PieProps) => {

    const t = useT()

    // const { targetIcon, performanceClass, performanceRate, groupCounts, tips } = props
    const { statstype, targetIcon, groupCounts } = props

    const piedata = Object.keys(groupCounts)
        .filter(k => groupCounts[k])        // avoid empty slices.
        .map(k => (

            {
                id: k,
                label: t(performanceGroups[statstype][k]?.label),  //defensive, in case of stats bugs doesn't crash ungracefully.
                value: groupCounts[k]
            }
        )

        )

    return piedata.length === 0 ?

        <Empty image={Empty.PRESENTED_IMAGE_SIMPLE} style={{ marginTop: 50, width: 180 }} description={t('dashboard.summary.cards.compliance_nodata')} />

        : <div className="pie-chart">

            <ResponsivePie data={piedata} enableArcLinkLabels={false} enableArcLabels arcLabelsTextColor='white'

                tooltip={group =>

                    <Label style={{ width: 120, background: 'white', padding: "5px 5px", borderRadius: 6 }} icon={targetIcon} iconStyle={{ color: performanceGroups[statstype][group.datum.id].color }}
                        title={<span>{performanceGroups[statstype][group.datum.id].label}: <strong>{group.datum.value}</strong></span>}

                    />}

                colors={group => performanceGroups[statstype][group.id].color}

            />
        </div>

}