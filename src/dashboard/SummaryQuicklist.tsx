

import { AccordionChildProps, AccordionLabel } from "#app/components/Accordion"
import { useT } from '#app/intl/api'
import { tenantIcon, tenantPlural, tenantType } from "#app/tenant/constants"
import { paramsInQuery, parentIn, updateQuery } from "#app/utils/routes"
import { useCurrentCampaign } from "#campaign/hooks"
import { CampaignInstance } from "#campaign/model"
import { PartyInstance, usePartyInstances } from '#campaign/party/api'
import { useSubmissionProfile } from '#campaign/submission/profile'
import { productType } from "#product/constants"
import { requirementType } from "#requirement/constants"
import { Icon, Tooltip } from "antd"
import * as React from "react"
import { RiCheckboxMultipleBlankLine } from "react-icons/ri"
// import { GrCheckbox as todoicon, GrCheckboxSelected as doneicon } from "react-icons/gr"
import { useHistory, useLocation } from "react-router-dom"
import { AssetStatsTable } from "./AssetStatsTable"
import { AssetTrailTable } from "./AssetTrailTable"
import { useCurrentAsset, useCurrentDashboard, useCurrentParty } from "./hooks"
import { PartyTrailTable } from "./PartyTrailTable"

    
type ViewType = "first" | "second"

type QuickListHeaderProps = {

    selectView: (_: ViewType) => () => void
    view: ViewType
    forParty?: boolean
}

const QuickListHeader = React.memo((props: QuickListHeaderProps & AccordionChildProps) => {

    const t = useT()
    const { selectView, view, forParty = false } = props
    const { onCollapseToggle, collapsed } = props;

    const contents = forParty ?
        {
            first: t('common.labels.todo'),
            second: t('common.labels.done'),
           
        }
        :
        {
            first: t('dashboard.labels.due_next'),
            second: t('dashboard.labels.past_due')
        }

    return <div className='section-header'>

        <div className='summary-stats-title summary-tabs'>
            <div className='tabs-icon'>
                <Tooltip  mouseEnterDelay={1} title={collapsed ? t("dashboard.section_expand_tip") : t("dashboard.section_collapse_tip")}>
                    <Icon onClick={onCollapseToggle}component={RiCheckboxMultipleBlankLine} />
                </Tooltip>
            </div>
            <div className={`${view === "first" ? 'tab-active' : 'tab-inactive'}`} onClick={selectView("first")}>{contents.first}</div>
            <div className='tab-separator'>|</div>
            <div className={`${view === "second" ? 'tab-active' : 'tab-inactive'}`} onClick={selectView("second")}>{contents.second}</div>
        </div>

    </div>

})


export const QuickList = (props: AccordionChildProps = {}) => {

    const { asset } = useCurrentAsset()
    const { onCollapseToggle, collapsed } = props;

    return asset ? <AssetQuickList onCollapseToggle={onCollapseToggle} collapsed={collapsed} asset={asset} />
                 : <AllOrPartyQuickList onCollapseToggle={onCollapseToggle} collapsed={collapsed}/>

}


const AllOrPartyQuickList = (props: AccordionChildProps = {}) => {

    const { onCollapseToggle, collapsed } = props;

    const { search, pathname } = useLocation()

    const { party } = useCurrentParty()

    const history = useHistory()

    const { aview = "first" } = paramsInQuery(history.location.search)

    const dashboard = useCurrentDashboard()

    const campaign = useCurrentCampaign()
    const subprofile = useSubmissionProfile().on(campaign)

    const requirements = subprofile.profileOf(requirementType)
    const products = subprofile.profileOf(productType)

    const profiles = { 
        
        [requirementType] : subprofile.profileOf(requirementType),
        [productType] : subprofile.profileOf(productType)
    
    }

    const assets = [
        ...party ? requirements.allForParty(party) : requirements.allSorted(),
        ...party ? products.allForParty(party) : products.allSorted()
    ]

    // eslint-disable-next-line
    const selectView = React.useCallback((view: ViewType) => () => history.push(`${pathname}?${updateQuery(search).with(ps => ps.aview = view)}`), [])

    const isRowEnabled = (ci: CampaignInstance) => ci.instanceType === requirementType ? profiles[requirementType].isForParty(party!, ci) : profiles[productType].isForParty(party!, ci)

    return <div className="summary-section list-assets-section">

        <QuickListHeader view={aview as ViewType} selectView={selectView} forParty={!!party} onCollapseToggle={onCollapseToggle} collapsed={collapsed}/>

        <div className="section-content">
            
            {party ?

                <AssetTrailTable  mode='summary' group='quick-list'
                    key={aview as string}
                    scope={aview === 'first' ? 'todo' : 'done'}
                    route={a => `${parentIn(pathname)}/${a.instanceType}/${dashboard.assetParam(a.instanceType, a.source)}`}
                    data={assets}
                    isRowEnabled={isRowEnabled}
                    sortBy={[['duedate', 'asc']]} />

                :

                <AssetStatsTable
                    mode='summary' className="asset-trail-quicklist"
                    key={aview as string}
                    scope={aview === 'first' ? 'future' : 'past'}
                    route={dashboard.routeToAsset}
                    data={assets}
                    sortBy={[['duedate', aview === 'first' ? 'asc' : 'desc']]} />

            }
        </div>

    </div>

}



const AssetQuickList = (props: { asset: CampaignInstance } & AccordionChildProps) => {

    const t = useT()

    const dashboard = useCurrentDashboard()

    const { pathname } = useLocation()

    const { asset, onCollapseToggle, collapsed } = props

    const campaign = useCurrentCampaign()

    const parties = usePartyInstances().on(campaign)

    const profile = useSubmissionProfile().on(campaign).profileOf(asset.instanceType)

    const data = parties.all()//.filter(p =>  profile.isForParty(p,asset))

    const isRowEnabled = (p: PartyInstance) => profile.isForParty(p, asset!)


    return <div className="summary-section list-assets-section">

        <div className='section-header'>
            <AccordionLabel
				className='summary-stats-title'
				title={t(tenantPlural)}
				icon={ tenantIcon }
				onCollapseToggle={ onCollapseToggle }
				collapsed={ collapsed }
			/>
            {/* <Label className='summary-stats-title' icon={<Icon className="fi" component={FiBox} />} title={t(tenantPlural)} /> */}

        </div>

        <div className="section-content">
            <PartyTrailTable mode="summary" route={a => `${parentIn(pathname)}/${tenantType}/${dashboard.partyParam(a.source)}`} data={data} isRowEnabled={isRowEnabled} />
        </div>

    </div>

}




