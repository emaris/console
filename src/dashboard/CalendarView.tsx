

import { Label } from "#app/components/Label"
import { Placeholder } from '#app/components/Placeholder'
import { TableProps } from '#app/components/VirtualTable'
import { SelectBox } from '#app/form/SelectBox'
import { icns } from "#app/icons"
import { useT } from '#app/intl/api'
import { useLocale } from "#app/model/hooks"
import { useTenantStore } from '#app/tenant/store'
import { useLogged } from '#app/user/store'
import { group } from "#app/utils/common"
import { paramsInQuery, updateQuery } from "#app/utils/routes"
import { useCurrentCampaign } from "#campaign/hooks"
import { PartyInstance } from '#campaign/party/api'
import { useCampaignUserPreferences } from '#campaign/preferences/api'
import { calendarOptions } from '#campaign/preferences/model'
import { ProductInstance } from '#campaign/product/api'
import { RequirementInstance } from '#campaign/requirement/api'
import { eventType } from "#event/constants"
import '#event/styles.scss'
import { useProductStore } from '#product/store'
import { requirementType } from "#requirement/constants"
import { useRequirementStore } from '#requirement/store'
import '@brainhubeu/react-carousel/lib/style.css'
import { Icon } from "antd"
import { useRenderGuard } from 'apprise-frontend-core/utils/renderguard'
import moment from "moment-timezone"
import * as React from "react"
import { AiOutlineCalendar, AiOutlineUnorderedList } from 'react-icons/ai'
import { useHistory, useLocation } from "react-router-dom"
import { useAssetCalendar, useCampaignCalendar, usePartyCalendar } from './calendarHooks'
import { CalendarListView } from './CalendarListView'
import { CalendarMonthlyView } from './CalendarMonthlyView'
import { CalendarRow } from './CalendarTable'
import { DashboardPage } from "./DashboardPage"
import { useCurrentAsset, useCurrentParty } from "./hooks"
import { FilterSelection } from './store'

const calendarModeParam = "calendar-mode"
const listMode = "list"
const monthMode = "month"

const defaultComparator = (i1, i2) => i1.name.localeCompare(i2.name)


export const CalendarView = () => {

    const { party } = useCurrentParty()
    const { asset } = useCurrentAsset()

    return party ? <PartyCalendarView party={party} /> :
        asset ? <AssetCalendarView asset={asset} /> :
            <CampaignCalendarView />


}


const CampaignCalendarView = () => {

    const campaign = useCurrentCampaign()

    const campaignCalendarGroup = `${campaign.id}-calendar`

    const { calendarData, computeCalendar, currentlySelected, initiallySelected, filters } = useCampaignCalendar({ campaignCalendarGroup })

    const {content} = useRenderGuard({

        when: !!calendarData,

        render: () => <InnerView data={calendarData} filterSelections={currentlySelected} initialSelection={initiallySelected} filterGroup={campaignCalendarGroup} filters={filters} />,

        orRun: computeCalendar,

        andRender: Placeholder.page

    })

    return content

}

const PartyCalendarView = (props: { party: PartyInstance }) => {

    const { party } = props

    const { l } = useLocale()

    const logged = useLogged()
    const tenants = useTenantStore()
    const campaign = useCurrentCampaign()

    const partyCalendarGroup = `${campaign.id}-${party.source}-calendar`

    const { calendarData, computeCalendar, currentlySelected, initiallySelected, filters } = usePartyCalendar({ party, partyCalendarGroup })

    const pageTitle = logged.hasNoTenant() ? l(tenants.safeLookup(party.source)?.name) : undefined

    const {content} = useRenderGuard({

        when: !!calendarData,

        render: () => <InnerView data={calendarData} filterSelections={currentlySelected} initialSelection={initiallySelected} filterGroup={partyCalendarGroup} filters={filters} pageTitle={pageTitle} />,

        orRun: computeCalendar,

        andRender: Placeholder.page

    })


    return content

}

const AssetCalendarView = (props: { asset: RequirementInstance | ProductInstance }) => {
    const { asset } = props

    const { l } = useLocale()

    const requirements = useRequirementStore()
    const products = useProductStore()
    const campaign = useCurrentCampaign()

    const assetCalendarGroup = `${campaign.id}-${asset.source}-calendar`
    const { calendarData, computeCalendar, currentlySelected, initiallySelected, filters } = useAssetCalendar({ asset, assetCalendarGroup })

    const pageTitle = asset.instanceType === requirementType ? l(requirements.safeLookup(asset.source).name) : l(products.safeLookup(asset.source).name)

    const {content} = useRenderGuard({

        when: !!calendarData,

        render: () => <InnerView data={calendarData} filterSelections={currentlySelected} initialSelection={initiallySelected as any} filterGroup={assetCalendarGroup} filters={filters} pageTitle={pageTitle} />,

        orRun: computeCalendar,

        andRender: Placeholder.page

    })

    return content
}



type InnerProps = Partial<TableProps<CalendarRow>> & {
    pageTitle?: React.ReactNode
    filterSelections: FilterSelection
    initialSelection?: FilterSelection
}

const InnerView = (props: InnerProps) => {

    const { data = [], pageTitle, filterSelections, initialSelection = {}, filters, ...rest } = props

    const history = useHistory()
    const t = useT()

    const { search } = useLocation()

    const defaultCalendarMode = useCampaignUserPreferences().calendarView()


    const { [calendarModeParam]: mode = defaultCalendarMode } = paramsInQuery(search)
    const directives = views[mode as string]


    const sortedRows = React.useMemo(() => data.sort((o1, o2) => o1.date.diff(o2.date)), [data])

    const minDate = sortedRows[0]?.date
    const maxDate = sortedRows[sortedRows.length - 1]?.date


    //eslint-disable-next-line
    const beforeMonths = React.useMemo(() => moment().diff(minDate, 'months') >= 0 ? moment().diff(minDate, 'months') + 1 : 0, [minDate])

    //eslint-disable-next-line
    const afterMonths = React.useMemo(() => moment(maxDate).diff(moment(), 'months') + 1 >= 0 ? moment(maxDate).diff(moment(), 'months') + 1 : 0, [maxDate])


    const filteredCalendarRows = React.useMemo(() => sortedRows.filter(i => i.filter({ ...initialSelection, ...filterSelections })), [sortedRows, filterSelections, initialSelection])


    //eslint-disable-next-line
    const groupsForSelection = React.useMemo(() => group(filteredCalendarRows).by(i => directives.groupBy(i, t), directives.orderBy ?? defaultComparator), [filteredCalendarRows])

    //eslint-disable-next-line
    const selectionTotal = React.useMemo(() => groupsForSelection.reduce((acc, cur) => acc = acc + cur.group.length, 0), [])

    //In the case of monthly view we want all the groups from start of end of campaign, even if the groups are empty
    const emptyGroup = React.useMemo(() => {

        const monthDates = [
            // now for the months before.
            ...Array.from(Array(beforeMonths).keys()).map(i => ({ date: moment().subtract(i + 1, 'months') })),

            // now
            { date: moment() },

            // now for the months after.
            ...Array.from(Array(afterMonths).keys()).map(i => ({ date: moment().add(i + 1, 'months') }))

        ]

        // const groups = group(monthDates).by(
        return group(monthDates).by(

            date => directives.groupBy(date, t),  // key

            directives.orderBy ?? ((i1, i2) => i1.name.localeCompare(i2.name)) // sort

        )

        //eslint-disable-next-line
    }, [])


    const groupsForMonthly = React.useMemo(() => emptyGroup.map(g => {
        const match = groupsForSelection.findIndex(gs => gs.key.name === g.key.name)
        return { key: g.key, group: match >= 0 ? [...groupsForSelection[match].group] : [] }
        //eslint-disable-next-line
    }), [groupsForSelection])

    const modePicker = <SelectBox
        fieldStyle={{ minWidth: 150 }}
        standalone
        onChange={m => history.push(`${history.location.pathname}?${updateQuery(history.location.search).with(p => { p[calendarModeParam] = m })}`)}
        getlbl={m => <Label icon={m === monthMode ? <Icon component={AiOutlineCalendar} /> : <Icon component={AiOutlineUnorderedList} />} title={t(`dashboard.calendar.${m}`)} />}
        selectedKey={mode as string}
        getkey={o => o} >
        {Object.keys(calendarOptions)}
    </SelectBox>

    return <DashboardPage title={pageTitle} tab={eventType} decorations={[modePicker]} className={`calendar-${mode}`} >
        {
            mode === listMode ?
                <CalendarListView groups={groupsForSelection} total={selectionTotal + groupsForSelection.length} filters={filters} {...rest} />
                :
                <CalendarMonthlyView groups={groupsForMonthly} total={data.length + groupsForMonthly.length} filters={filters} {...rest} />
        }
    </DashboardPage>
}




const orderBy = (t1, t2) => t1.year - t2.year || t1.month - t2.month

const groupBy = (i: CalendarRow, t) => ({ month: i.date.month(), year: i.date.year(), name: `${t(`time.months.${i.date.month() + 1}`)} ${i.date.year()}`, icon: icns.calendar })


const views = {

    [listMode]: {
        name: "campaign.labels.event_view.flat",
        orderBy,
        groupBy
    },

    [monthMode]: {
        name: "campaign.labels.event_view.calendar",
        orderBy,
        groupBy
    }
}
