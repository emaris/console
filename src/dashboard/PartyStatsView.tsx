
import { useCurrentCampaign } from "#campaign/hooks"
import { PartyInstance } from "#campaign/party/api"
import { usePartyInstances } from '#campaign/party/api'
import { useLocale } from "#app/model/hooks"
import { tenantType } from "#app/tenant/constants"
import { useTenantStore } from '#app/tenant/store'
import { useHistory } from "react-router-dom"
import { DashboardPage } from "./DashboardPage"
import { useCurrentAsset, useCurrentDashboard } from "./hooks"
import { PartyStatsTable } from "./PartyStatsTable"
import { HorizonDecoration } from './SubmissionHorizon'


export const PartyStatsView = () => {

        const { l } = useLocale()

        const history = useHistory()
        const tenants = useTenantStore()

        const dashboard = useCurrentDashboard()

        const campaign = useCurrentCampaign()

        const parties = usePartyInstances().on(campaign)

        const horizon = dashboard.submissionHorizon(campaign)

        const { asset, source } = useCurrentAsset()

        const partyRoute = (p: PartyInstance) => `${history.location.pathname}/${dashboard.partyParam(p.source)}`

        const summary = dashboard.summaries[tenantType]

        const data = parties.allSorted().map(p => ({ ...p, summary: summary[p.source] }))

        return <DashboardPage title={!!asset && l(source(asset.source)?.name)} tab={tenantType}>
                <PartyStatsTable route={partyRoute} data={data} horizon={horizon}
                        filterBy={r => l(tenants.safeLookup(r.instance.source)?.name)}
                        decorations={[<HorizonDecoration />]} />

        </DashboardPage>
}