
import { campaignRoute, campaignType } from "#campaign/constants"
import { useCurrentCampaign } from "#campaign/hooks"
import { Campaign, CampaignInstance } from "#campaign/model"
import { PartyInstance } from "#campaign/party/api"
import { TrailKey, useSubmissionScales, useSubmissionTopics } from "#campaign/submission/model"
import { useSubmissionProfile } from '#campaign/submission/profile'
import { AllAssetsSummary, AllPartiesSummary, useSubmissionStatistics } from '#campaign/submission/statistics/api'
import { TrailSummary } from '#campaign/submission/statistics/trail'
import { productType } from "#product/constants"
import { requirementType } from "#requirement/constants"
import { tenantType } from "#app/tenant/constants"
import { useTenantStore } from '#app/tenant/store'
import * as React from "react"
import { useLocation, useParams } from "react-router-dom"
import { useCampaignStatistics, useDashboard } from './api'
import { useDashboardCalendar } from "./calendar"
import { dashboardRoute } from "./constants"
import { useSubmissionStore } from '#campaign/submission/store'
import { StatisticsContext } from './context'




export const useCurrentDashboard = () => {

    const campaign = useCurrentCampaign()
    const dashboard = useDashboard().on(campaign)
   
    const statistics = React.useContext (StatisticsContext) ?? {}

    return {...dashboard,summaries:statistics}
}

export type CampaignMode = 'design' | 'dashboard' | 'off'

export const useCampaignMode = () : CampaignMode => {

    const {pathname} = useLocation()
    
    if (pathname.startsWith(campaignRoute) )
        return 'design'
    
    if (pathname.startsWith(dashboardRoute) )
        return "dashboard"

    return "off"

}

export const useCalendar = () => {

    const campaign = useCurrentCampaign()
  
    const api = useDashboardCalendar().on(campaign)
    
    return api
}

export const useCurrentParty = () => {

    const tenants = useTenantStore()
    const dashboard = useCurrentDashboard()
    const {party:partyId} = useParams<{party: string}>()

    const party = dashboard.resolveParty(partyId)
    const tenant = tenants.safeLookup(partyId)
   
    return { party, partyParam:partyId, tenant}


}

export const useCurrentAsset = () => {


    const dashboard = useCurrentDashboard()
    const params = useParams<{ type: string, asset: string}>()
    
    const asset = params.asset ? dashboard.resolveAsset(params.type,params.asset) : undefined
    const type = params.type

    const campaign = useCurrentCampaign()
    const subprofile= useSubmissionProfile().on(campaign)

    return { asset,type,assetParam:params.asset, ...subprofile.profileOf(type) }


}


export const useTrail = () => {

    const {party} = useCurrentParty()
    const {asset} = useCurrentAsset()
    const campaign = useCurrentCampaign()
    const submissions = {...useSubmissionStore().on(campaign), ...useSubmissionScales().on(campaign),  ...useSubmissionTopics().on(campaign), ...useSubmissionProfile().on(campaign), ...useSubmissionStatistics().on(campaign)}

    const key = {
        campaign: campaign.id
    }

    const keyWithParty = (p:PartyInstance) : TrailKey => ({...key,party:p.source,assetType:asset?.instanceType!,asset:asset?.source!})
    const keyWithAsset = (a:CampaignInstance) : TrailKey => ({...key,party:party?.source!,assetType:a.instanceType!,asset:a.source!})
    

    return {keyWithParty,keyWithAsset, submissions}
}


export type Summaries = {

    startedCampaigns: Campaign[]
    dueDates: Record<string,string>
    trails: Record<string, TrailSummary[]>
    trailSummaryMap: Record<string, TrailSummary>
    trailSummaryOf: (key:TrailKey) => TrailSummary
    [campaignType]:  ReturnType<ReturnType<typeof useCampaignStatistics>['get']>,
    [requirementType]: AllAssetsSummary,
    [productType]: AllAssetsSummary,
    [tenantType] : AllPartiesSummary
    staticparties: AllPartiesSummary

}

