import { Label } from '#app/components/Label'
import { VSelectBox } from '#app/form/VSelectBox'
import { useT } from '#app/intl/api'
import { TenantLabel } from '#app/tenant/Label'
import { tenantPlural, tenantType } from '#app/tenant/constants'
import { useTenantStore } from '#app/tenant/store'
import { useStable } from '#app/utils/function'
import { useCurrentCampaign } from '#campaign/hooks'
import { AssetInstance } from '#campaign/model'
import { usePartyInstances } from '#campaign/party/api'
import { useProductInstances } from '#campaign/product/api'
import { useRequirementInstances } from '#campaign/requirement/api'
import { DynamicEditorProps, DynamicsEditor } from '#messages/Editor'
import { requirementType } from '#requirement/constants'
import { utils } from 'apprise-frontend-core/utils/common'
import { useEffect, useMemo } from 'react'


export const AllPartySelector: DynamicsEditor = props => {

    return <MessagePartySelector {...props} />
}

export const useAssetPartySelector = (asset:AssetInstance)  : DynamicsEditor => useStable( (props: DynamicEditorProps) => {

    return <MessagePartySelector {...props} asset={asset} />
})

export const MessagePartySelector = (props: DynamicEditorProps & {

    asset?: AssetInstance

}) => {

    const t = useT()
   
    const campaign = useCurrentCampaign()

    const tenantmodel = useTenantStore()
    const partyinst = usePartyInstances().on(campaign)
    const reqinst = useRequirementInstances().on(campaign)
    const prodinst = useProductInstances().on(campaign)

    const { dynamics, onChange, asset, replies } = props

    // aligns empty dynamics with recipients of reply targer, if any.
    // works at start, but also if selection is being emptied.
    useEffect(()=> {

        if (replies?.recipient /* ENABLE IF WE WANT TO GUARD AGAINST PROMOTING TO UNRESTRICTED BROADCASTS && !dynamics.recipients.length */)
            onChange({recipients: replies.recipient.split(":"), topics: replies.recipient.split(":").map(t => ({ type: tenantType, name: `${campaign.id}:${t}` }))})

        // eslint-disable-next-line
    }, [replies,/* dynamics */])

    const instances = partyinst.all()
    const assetInstances = asset ? (asset.instanceType === requirementType ? reqinst : prodinst ) : undefined

   
    const options = useMemo(() => utils().dedup(partyinst.allSorted()
                .filter( p => asset ? assetInstances?.isForParty(p,asset) : true )
                // ENABLE IF WE WANT TO GUARD AGAINST OPENING UP TO MORE PARTIES .filter( t => replies?.recipient ? replies.recipient.includes(t.source) : t)
                .map(t => t.source))
                
                 // eslint-disable-next-line
                , [replies,instances, asset])

    return <div className='party-selector'>
        <Label className='party-selector-label' noIcon mode='tag' title={t(tenantPlural)} />
        <VSelectBox menuPlacement='top' className='party-selector-dropdown' mode='multiple' light
            placeholder={`<${t('common.labels.all_many', { plural: t(tenantPlural) })}>`}
            options={options}
            onChange={tenants => onChange({ recipients: tenants, topics: tenants.map(t => ({ type: tenantType, name: `${campaign.id}:${t}` })) })}
            renderOption={s => <TenantLabel noIcon noLink noDecorations tenant={s} />}
            lblTxt={tenantmodel.stringifyRef}
            optionId={s => s}
            >
            {dynamics.recipients}
        </VSelectBox>
    </div>



}
