import { Label } from '#app/components/Label'
import { icns } from '#app/icons'
import { useT } from '#app/intl/api'
import { tenantIcon, tenantPlural } from '#app/tenant/constants'
import { TimeLabel } from '#app/time/Label'
import { useCurrentCampaign } from '#campaign/hooks'
import { usePartyInstances } from '#campaign/party/api'
import { eventIcon } from '#event/constants'
import { productIcon, productPlural } from '#product/constants'
import { requirementIcon, requirementPlural } from '#requirement/constants'
import { Divider, Progress, Row, Tooltip } from 'antd'
import * as React from 'react'
import { useCampaignStatistics } from './api'
import { useCurrentParty } from './hooks'
import { StatCol } from './SummaryCard'
import { LatestSubmissions } from './SummaryLatestSubmissions'


type Props = {

    active:boolean 
}
export const ProgressCard = (props:Props) => {

    const t = useT()

    const campaign = useCurrentCampaign()
    const stats = useCampaignStatistics()
    const campaignTimeZone = campaign.properties.timeZone ? {'original' : campaign.properties.timeZone} : undefined

    const parties = usePartyInstances().on(campaign)

    const {active} = props;

    if (!active)
        return null;

    const { start, started, days, end, ended, progress, progressRate } = stats.get(campaign)

    return <div className="progress-card">

        <div className="part-graph">

            <Progress width={180} strokeWidth={5} type="circle" percent={progressRate}
                format={v => <Tooltip title={t("dashboard.summary.progress.days_elapsed", { number: progress })}><span className={v === 100 ? '' : 'progress'}>{v}%</span></Tooltip>} />

            <div className="graph-stats">

                <Row>
                    <StatCol span={24} title={t(started ? t("dashboard.summary.progress.started") : t("dashboard.summary.progress.starts"))} >
                        <TimeLabel icon={icns.calendar} format='short' value={start} timezones={campaignTimeZone} />
                    </StatCol>
                </Row>


                {end &&

                    <>

                    <Row>
                        <StatCol span={24} title={t(ended ? t("dashboard.summary.progress.ended") : t("dashboard.summary.progress.ends"))} >
                            <TimeLabel icon={icns.calendar} accentFrame='months' accentMode='alert' format='short' value={end} timezones={campaignTimeZone} />
                        </StatCol>
                    </Row>
                


                 {ended ||

                    <Row>
                        <StatCol span={24} title={t("dashboard.summary.progress.in")} >
                            <Label icon={eventIcon} title={t("dashboard.summary.progress.days", { number: days - progress })} />
                        </StatCol>
                    </Row>

                 }
                
                </>}


                <Row>
                    <StatCol span={24} title={t(tenantPlural)} >
                        <Label icon={tenantIcon} title={parties.allForStats().length} />
                    </StatCol>
                </Row>


            </div>
        </div>

        <div className="part-numbers">

            <Divider orientation='left'><Label icon='#' title={t("dashboard.summary.stats")} /></Divider>

            <ProgressStats />

            <LatestSubmissions />

        </div>

    </div>
}


const ProgressStats = () => {

    const t = useT()

    const { party } = useCurrentParty()
    const campaign = useCurrentCampaign()

    const stats = useCampaignStatistics()

    const { totalProducts, totalRequirements, requirementPastDue, productsPastDue } = stats.get(campaign,party)

    return <React.Fragment>

        <Row >

            <StatCol title={t(requirementPlural)} >
                <Label icon={requirementIcon} title={totalRequirements} />
            </StatCol>

            <StatCol title={t("dashboard.summary.progress.past_due")} >
                <Label icon={requirementIcon} title={requirementPastDue} />
            </StatCol>

            <StatCol title={t("dashboard.summary.progress.remaining")} >
                <Label icon={requirementIcon} title={totalRequirements - requirementPastDue} />
            </StatCol>

        </Row>

        <Row style={{ marginTop: 20 }}>

            <StatCol title={t(productPlural)} >
                <Label icon={productIcon} title={totalProducts} />
            </StatCol>

            <StatCol title={t("dashboard.summary.progress.past_due")} >
                <Label icon={productIcon} title={productsPastDue} />
            </StatCol>

            <StatCol title={t("dashboard.summary.progress.remaining")} >
                <Label icon={productIcon} title={totalProducts - productsPastDue} />
            </StatCol>

        </Row>

    </React.Fragment>

}