
import { useLocale } from "#app/model/hooks"
import { tenantType } from "#app/tenant/constants"
import { useTenantStore } from '#app/tenant/store'
import { useCurrentCampaign } from "#campaign/hooks"
import { PartyInstance, usePartyInstances } from "#campaign/party/api"
import { useSubmissionProfile } from '#campaign/submission/profile'
import { useHistory } from "react-router-dom"
import { DashboardPage } from "./DashboardPage"
import { useCurrentAsset, useCurrentDashboard } from "./hooks"
import { PartyTrailTable } from "./PartyTrailTable"


export const PartyTrailView = () => {

        const { l } = useLocale()

        const history = useHistory()

        const tenants = useTenantStore()

        const dashboard = useCurrentDashboard()

        const campaign = useCurrentCampaign()

        const partyinst = usePartyInstances().on(campaign)

        const { asset, source } = useCurrentAsset()
       
        const profile = useSubmissionProfile().on(campaign).profileOf(asset?.instanceType)
       
       
        const partyRoute = (p: PartyInstance) => `${history.location.pathname}/${dashboard.partyParam(p.source)}`
        
        const rows = partyinst.allSorted()//.filter(p=> profile.isForParty(p,asset!))

        const isRowEnabled = (p: PartyInstance) => profile.isForParty(p, asset!)

        return <DashboardPage title={l(source(asset!.source)?.name)} tab={tenantType}>

                <PartyTrailTable data={rows}
                        route={partyRoute}
                        filterBy={r => l(tenants.safeLookup(r.source)?.name)}
                        isRowEnabled={isRowEnabled}/>

        </DashboardPage>
}