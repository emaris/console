import { tenantType } from "#app/tenant/constants"
import { CampaignContext } from '#campaign/context'
import { Campaign, useCampaignModel } from '#campaign/model'
import { eventType } from "#event/constants"
import { productType } from "#product/constants"
import { requirementType } from "#requirement/constants"
import { useContext } from 'react'


export type TargetType = typeof tenantType | typeof productType | typeof requirementType

export type FilterSelection = {[requirementType]:string[], [productType]:string[], [tenantType]:string[], [eventType]:string[], 'action': string[]}

export type DashboardState = {

    dashboard : {

        summaryCard?: number,
        summaryRateView?: TargetType
        partySummaryCard?: number,
        campaignFilter?: string
        requirementFilter?: string,
        productFilter?: string
        partyFilter?:string
        submissionHorizon?: number

        calendarFilters?: {[key:string]:FilterSelection | undefined}

        
    }
}

export const useDashboardStore = () => {
   
    const s = useContext(CampaignContext)

    const campaignmodel = useCampaignModel()

    const self = {


        summaryCard: () =>  s.get().campaigns.dashboard.summaryCard

        ,
    
        setSummaryCard: (card:number) => s.set(s => s.campaigns.dashboard.summaryCard=card)

        ,

        summaryRateView: ()  =>  s.get().campaigns.dashboard.summaryRateView

        ,
    
        setSummaryRateView: (view:TargetType) => s.set(s => s.campaigns.dashboard.summaryRateView=view)

        ,

        partySummaryCard: () =>  s.get().campaigns.dashboard.partySummaryCard

        ,
    
        setPartySummaryCard: (card:number) => s.set(s => s.campaigns.dashboard.partySummaryCard=card)

        ,

        submissionHorizon: (campaign:Campaign) =>  s.get().campaigns.dashboard.submissionHorizon ?? campaignmodel.statisticalHorizon(campaign)

        ,
    
        setSubmissionHorizon: (horizon:number) => s.set(s => s.campaigns.dashboard.submissionHorizon=horizon)

    
    }

    return self;

}