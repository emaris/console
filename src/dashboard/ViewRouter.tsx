

import { useCurrentCampaign } from '#campaign/hooks'
import { useCampaignUserPreferences } from '#campaign/preferences/api'
import { SubmissionRouter } from "#dashboard/SubmissionRouter"
import { eventType } from "#event/constants"
import { messageType } from "#messages/constants"
import { productType } from "#product/constants"
import { requirementType } from "#requirement/constants"
import { NoSuchRoute } from "#app/components/NoSuchRoute"
import { tenantType } from "#app/tenant/constants"
import { useTenantStore } from '#app/tenant/store'
import { useLogged } from '#app/user/store'
import { Redirect, Route, Switch } from "react-router-dom"
import { useRoutableCampaigns } from './api'
import { AssetStatsView } from "./AssetStatsView"
import { AssetTrailView } from "./AssetTrailView"
import { CalendarView } from "./CalendarView"
import { dashboardRoute } from "./constants"
import { useCurrentDashboard } from "./hooks"
import { MessageViewer } from "./MessageView"
import { PartyStatsView } from "./PartyStatsView"
import { PartyTrailView } from "./PartyTrailView"
import { SharedDocuments } from "./SharedDocuments"
import "./styles.scss"
import { SummaryView } from "./SummaryView"

export const summaryRoute = "summary"
export const summaryName = "dashboard.labels.summary"

export const sharedDocumentsRoute = 'shared'
export const sharedDocumentsName = "dashboard.labels.shared_documents"

// const campaignRoute = logged.hasNoTenant() ? `${dashboardRoute}/:c` : `${dashboardRoute}/${tenantType}/${tenants.lookup(logged.tenant)}/:c`
const campaignRoute = `${dashboardRoute}/:c`
const partyRoute = `${campaignRoute}/${tenantType}/:party`
const assetRoute = `${campaignRoute}/:type/:asset`

export const ViewRouter = () => {

    const logged = useLogged()
    const tenantstore = useTenantStore()
    const campaigns = useCampaignUserPreferences()

    const userParty = tenantstore.safeLookup(logged.tenant)

    const defaultDashboardView = campaigns.dashboardView()

    const getUrlParent = (url: string): string => {
        let split = url.split('/')
        split.pop()
        return split.join('/')
    }

    return <Switch>


        {/* default view. */}

        <Route exact path={campaignRoute}
            render={({ match }) => logged.hasNoTenant() ? <Redirect to={`${match.url}/${defaultDashboardView}`} /> : <Redirect to={`${match.url}/${tenantType}/${userParty.id}/${defaultDashboardView}`} />}
        />
        {/* default view guard */}
        {!logged.hasNoTenant() && <Route exact path={`${campaignRoute}/:type`}
            render={({ match }) => <Redirect to={getUrlParent(match.url)} />}
        />}


        {/* submission routes, via parties and assets in any order. */}

        <Route exact path={[
            `${partyRoute}/:type/:asset/:id`,
            `${assetRoute}/${tenantType}/:party/:id`
        ]}
            render={props => {

                const { match: { params: p } } = props

                return <RouteGuard {...props}>
                    <SubmissionRouter submission={p.id} />
                </RouteGuard>

            }} />


        <Route exact path={[`${partyRoute}/:type/:asset`, `${assetRoute}/${tenantType}/:party`]} render={({ match, location }) => <Redirect to={`${match.url}/latest${location.search ? `${location.search}` : ''}`} />} />

        {/* summary routes. */}

        <Route exact path={`${campaignRoute}/${summaryRoute}`} render={props => <RouteGuard {...props}><SummaryView /></RouteGuard>} />
        <Route exact path={`${campaignRoute}/${sharedDocumentsRoute}`} render={props => <RouteGuard {...props}><SharedDocuments /></RouteGuard>} />
        <Route exact path={`${partyRoute}/${summaryRoute}`} render={props => <RouteGuard {...props}><SummaryView /></RouteGuard>} />

        <Route exact path={`${assetRoute}/${summaryRoute}`} render={props => <RouteGuard {...props}><SummaryView /></RouteGuard>} />

        {/* calendar routes. */}

        <Route exact path={[
            `${campaignRoute}/${eventType}`,
            `${partyRoute}/${eventType}`,
            `${assetRoute}/${eventType}`

        ]} component={CalendarView} />

        {/* Messages  routes. */}

        <Route exact path={[
            `${campaignRoute}/${messageType}`,
            `${partyRoute}/${messageType}`,
            `${assetRoute}/${messageType}`

        ]} component={MessageViewer} />

        {/* 
                    party routes. MUST come before asset route rules, which are more generic and would otherwise 'greedly capture' them.
                
                */}
        <Route exact path={`${campaignRoute}/${tenantType}`} render={props => <RouteGuard noEntry={!logged.isMultiManager()} {...props}><PartyStatsView /></RouteGuard>} />
        <Route exact path={`${partyRoute}/${sharedDocumentsRoute}`} render={props => <RouteGuard {...props}><SharedDocuments /></RouteGuard>} />
        <Route exact path={`${partyRoute}/:type`} render={props => <RouteGuard {...props}><AssetTrailView /></RouteGuard>} />
        <Route exact path={partyRoute} render={({ match }) => <Redirect to={`${match.url}/${summaryRoute}`} />} />


        {/* asset routes. */}
        <Route exact path={`${dashboardRoute}/:c/:type`} render={props => <RouteGuard {...props}><AssetStatsView /></RouteGuard>} />
        <Route exact path={assetRoute} render={({ match }) => <Redirect to={`${match.url}/${summaryRoute}`} />} />

        {/* why this one? to move from calendar to parties.*/}
        <Route exact path={`${assetRoute}/${tenantType}`} render={props => <RouteGuard noEntry={!logged.isMultiManager()} {...props}><PartyTrailView /></RouteGuard>} />

        <NoSuchRoute />

    </Switch>
}

const d = (s: string) => decodeURIComponent(s)

type GuardProps = {

    children: any
    match: { params: any }
    noEntry?: boolean

}
// resolves|validated route parameters to handles invalid cases wuth a <NoSuchRoute>
// better than blowing up in component code.
export const RouteGuard = (props: GuardProps) => {

    // if (!!props.noEntry) 
    //     return <NoSuchRoute />

    const { resolveParty, resolveAsset } = useCurrentDashboard()
    // const campaigns = useCampaigns()

    const { match: { params }, children } = props

    // const canDesignCampaign = useConfig().get().routedTypes?.includes(campaignType)

    const routable = useRoutableCampaigns()


    const campaign = useCurrentCampaign()
    const type = params.type && d(params.type)
    const party = params.party && d(params.party)
    const asset = params.asset && d(params.asset)

    // if (!canDesignCampaign && !campaigns.isRunning(campaign))
    //     return <NoSuchRoute msg={campaign.name?.en} />
    if (routable.all().findIndex(c => c.id === campaign.id) < 0)
        return <NoSuchRoute msg={campaign.name?.en} />

    if (type && type !== requirementType && type !== productType && type !== eventType && type !== tenantType)
        return <NoSuchRoute msg={type} />

    if (party && !resolveParty(party))
        return <NoSuchRoute msg={party} />

    if (asset && !resolveAsset(type, asset))
        return <NoSuchRoute msg={asset} />



    return children

}

