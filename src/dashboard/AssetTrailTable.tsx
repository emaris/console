import { Column, TableProps, VirtualTable } from '#app/components/VirtualTable'
import { useT } from '#app/intl/api'
import { useLocale } from '#app/model/hooks'
import { TagLabel } from '#app/tag/Label'
import { useTagModel } from '#app/tag/model'
import { TimeLabel } from '#app/time/Label'
import { compareDates } from '#app/utils/common'
import { complianceIcon } from '#campaign/constants'
import { useEventInstanceStore } from '#campaign/event/store'
import { useCurrentCampaign } from '#campaign/hooks'
import { AssetInstance, CampaignInstance, useCampaignModel } from '#campaign/model'
import { latestSubmission, submissionIcon } from '#campaign/submission/constants'
import { SubmissionLabel } from '#campaign/submission/Label'
import { TrailSummary } from '#campaign/submission/statistics/trail'
import { TimelinessLabel } from '#campaign/submission/TimelinessLabel'
import { eventIcon } from '#event/constants'
import { productType } from '#product/constants'
import { requirementType } from '#requirement/constants'
import { NotAssessableLabel } from '#requirement/Label'
import { Tooltip } from 'antd'
import * as React from 'react'
import { Link, useHistory } from 'react-router-dom'
import { useCurrentDashboard, useCurrentParty, useTrail } from './hooks'
import { useSubmissionFilter } from './SubmissionFilter'


type ViewType = 'todo' | 'done'

export type AssetRow = AssetInstance & { summary: TrailSummary }

type AssetTrailTableProps = Partial<Omit<TableProps<AssetRow>, 'data'>> & {
    data: AssetInstance[]
    mode?: 'full' | 'summary'
    route: (_: AssetInstance) => string
    scope?: ViewType
    isRowEnabled?: (_: AssetInstance) => boolean
    group?: string
}


export const AssetTrailTable = (props: AssetTrailTableProps) => {

    const t = useT()
    const { l } = useLocale()
    const tagModel = useTagModel()

    const { data, mode = 'full', route, scope, isRowEnabled = () => true, group = 'asset-trail-table', ...rest } = props

    // const time = useTime()
    const campaignmodel = useCampaignModel()
    const { summaries } = useCurrentDashboard()

    const { keyWithAsset, submissions } = useTrail()
    const campaign = useCurrentCampaign()

    const campaignTimeZone = campaign.properties.timeZone ? { 'original': campaign.properties.timeZone } : undefined

    const profiles = {
        [requirementType]: submissions.profileOf(requirementType),
        [productType]: submissions.profileOf(productType)
    }

    const p = (a: CampaignInstance) => profiles[a.instanceType]

    const { party } = useCurrentParty()
    const history = useHistory()

    const events = useEventInstanceStore().on(campaign)

    const routeToSubmitted = (a: AssetRow) => `${route(a)}/${a.summary.official?.id ?? 'unknown'}`
    const routeToHistory = (a: AssetRow) => `${route(a)}/${latestSubmission}?tab=history`

    const filterTodo = (a: CampaignInstance) => {

        const summary = submissions.trailSummary(keyWithAsset(a),summaries.dueDates[a.source])

        const isDue = p(a).isDue(a)

        return (summary.missing || summary.official === undefined) && isDue
    }

    const filterDone = (a: CampaignInstance) => !filterTodo(a)

    const filterScope = React.useCallback((a: CampaignInstance) => {

        if (!scope)
            return true

        return scope === 'todo' ? filterTodo(a) : filterDone(a)

        // eslint-disable-next-line
    }, [scope])

    const filteredScopeData = React.useMemo(() => data.filter(filterScope), [data, filterScope])

    const assetTrailGroup = `${campaign.id}-${party?.source}-${group}-trail`


    const unfilteredrows: AssetRow[] = React.useMemo(() =>

        filteredScopeData.map(asset => ({ ...asset, summary: summaries.trailSummaryOf(keyWithAsset(asset)) } as AssetRow))

        //eslint-disable-next-line
        , [filteredScopeData, summaries.trails])


    const { Filters: SubmissionFilters, data: rows } = useSubmissionFilter({
        rows: unfilteredrows,
        key: 'submission-filter',
        group: assetTrailGroup,
        complianceProfiles: submissions.allComplianceProfiles(),
        timelinessProfiles: submissions.allTimelinessProfiles(),
        view: 'asset'
    })


    const assetColumn = <Column<AssetRow> key='partysource'
        refreshWithParty={party?.source}
        minWidth={250}
        flexGrow={1}
        title={t("common.fields.name_multi.name")}
        dataKey="asset" dataGetter={r => l(p(r).source(r.source)?.name)}
        cellRenderer={({ rowData: a }) => p(a).label(a, { noDecorations: true, className: !isRowEnabled(a) ? 'notapplicable' : '', linkTo: () => route(a) })} />

    const dueDateColumn = <Column<AssetRow> key='duedate'
        minWidth={150}
        dataKey="duedate"
        title={t("dashboard.labels.due_date.title")}
        headerTooltip={t("dashboard.labels.due_date.asset_tip")}
        comparator={[(d1, d2) => compareDates(d1, d2, false), (d1, d2) => compareDates(d1, d2, true)]}
        dataGetter={r => events.absolute(p(r).deadlineOf(r)?.date)}
        cellRenderer={({ rowData: r }) => {
            const accentLate = r.summary.computedTimeliness === 'late' && scope === 'todo'
            return p(r).label(r, { deadlineOnly: true, accentFrame: accentLate ? "none" : "weeks", className: accentLate ? 'warn' : '', timezone: campaignTimeZone })
        }
        } />

    const currentStatusColumn = <Column<AssetRow> key='currrentstatus'
        minWidth={130}
        dataKey="status"
        title={t("dashboard.labels.status.title")}
        headerTooltip={t("dashboard.labels.status.asset_tip")}
        dataGetter={r => r.summary.lastRevision?.lifecycle.state}
        cellRenderer={({ rowData: r }) => 
            isRowEnabled(r) ? 
                r.summary.lastRevision && <SubmissionLabel displayMode='state-only' noTip submission={r.summary.lastRevision} /> 
            : <TagLabel className='notapplicable lbl' tag={t("submission.status.nostatus")} icon={submissionIcon} noTip />
        }
    />

    // const timelinessColumn = <Column<AssetRow> key='timeliness'
    //     minWidth={110}
    //     className='trail-timeliness'
    //     dataKey="timeliness"
    //     title={t("dashboard.labels.timeliness.title")}
    //     headerTooltip={t("dashboard.labels.timeliness.asset_tip")}
    //     dataGetter={r => r.summary}
    //     cellRenderer={({ cellData: summary }) => summary.computedTimeliness && summary.last?.lifecycle.state !== 'missing' &&
    //         <Label mode='tag' className={`timeliness-${summary.computedTimeliness}`}
    //             icon={eventIcon}
    //             title={t(`dashboard.labels.timeliness.${summary.computedTimeliness}`)} />}
        
    // />
    const timelinessColumn = <Column<AssetRow> key='timeliness'
        minWidth={110}
        className='trail-timeliness'
        dataKey="timeliness"
        title={t("dashboard.labels.timeliness.title")}
        headerTooltip={t("dashboard.labels.timeliness.asset_tip")}
        dataGetter={r => r}
        comparator={(t1, t2) => tagModel.comparator(t1?.summary?.timeliness?.tag, t2.summary?.timeliness?.tag)}
        cellRenderer={({ cellData   }) => isRowEnabled(cellData) ? <TimelinessLabel asset={cellData} /> : <TagLabel className='notapplicable lbl' tag={t('submission.labels.not_applicable_badge')} icon={eventIcon} noTip />}
        
    />

    const submittedColumns = <Column<AssetRow> key='filter'
        minWidth={150}
        dataKey="submitted"
        title={t("dashboard.labels.submitted.title")}
        headerTooltip={t("dashboard.labels.submitted.asset_tip")}
        comparator={[(d1, d2) => compareDates(d1, d2, false), (d1, d2) => compareDates(d1, d2, true)]}
        dataGetter={r => r.summary.official?.lifecycle.lastSubmitted}
        cellRenderer={({ rowData: r }) => r.summary.official && r.summary.official.lifecycle.state !== 'missing' &&
            <SubmissionLabel tipMode='author' noPseudoNames
                linkTo={routeToSubmitted(r)}   // if submission is last revision, this is the same as row's link.
                submission={r.summary.official} />}
    />

    const complianceColumn = <Column<AssetRow> key='compliance'
        minWidth={150}
        dataKey="compliance"
        title={t("dashboard.labels.compliance.title")}
        headerTooltip={t("dashboard.labels.compliance.asset_tip")}
        dataGetter={r => r.summary.assessable ? l(r.summary.compliance?.name) ?? "zz1": "zz2"}
        cellRenderer={({ rowData: r }) => {

            if (!isRowEnabled(r)) return <TagLabel className='notapplicable lbl' tag={t("submission.labels.not_applicable_badge")} icon={complianceIcon} noTip />

            if (r.summary.assessable===false) return <NotAssessableLabel emphasis={false} />

            const submission = r.summary.official

            return submission && <SubmissionLabel displayMode='compliance-rating' submission={submission} />

        }} />

    const lastUpdatedColumn = <Column<AssetRow> key='lastupdate'
        minWidth={150}
        dataKey="lastUpdated"
        title={t("dashboard.labels.last_update.title")}
        headerTooltip={t("dashboard.labels.last_update.asset_tip")}
        comparator={[(d1, d2) => compareDates(d1, d2, false), (d1, d2) => compareDates(d1, d2, true)]}
        dataGetter={r => r.summary.lastUpdate}
        cellRenderer={({ rowData: r }) => r.summary.lastUpdate && <TimeLabel icon={eventIcon} displayMode='relative' value={r.summary.lastUpdate} timezones={campaignTimeZone} />}
    />


    const revisionCountColumn = <Column<AssetRow> key='revisionCount'
        align='center'
        width={100}
        minWidth={80}
        dataKey="revisionCount"
        title={t("dashboard.labels.revisions.title")}
        headerTooltip={t("dashboard.labels.revisions.asset_tip")}
        dataGetter={r => r.summary.revisions}
        
        cellRenderer={({ rowData: r }) =>
            (r.summary.lastRevision?.lifecycle.state !== 'missing') &&
            <Link to={routeToHistory(r)}>
                <Tooltip mouseEnterDelay={0.3} title={t("dashboard.labels.revisions.count_tip")}>
                    {r.summary.revisions && r.summary.revisions}
                </Tooltip>
            </Link>}
    />
    
    const columns = mode === 'full' ?
        campaignmodel.currentComplianceScale(campaign) ?
            [assetColumn, dueDateColumn, timelinessColumn, submittedColumns, complianceColumn, currentStatusColumn, lastUpdatedColumn, revisionCountColumn] :
            [assetColumn, dueDateColumn, timelinessColumn, submittedColumns, currentStatusColumn, lastUpdatedColumn, revisionCountColumn]
        :
        scope === 'done' ?
            campaignmodel.currentComplianceScale(campaign) ?
                [assetColumn, dueDateColumn, timelinessColumn, submittedColumns, complianceColumn, currentStatusColumn] :
                [assetColumn, dueDateColumn, timelinessColumn, submittedColumns, currentStatusColumn]
            :
            [assetColumn, dueDateColumn, currentStatusColumn, lastUpdatedColumn]

    // const filters = mode==='full' ? [submissionFilter,StatusFilter,UserProfileFilter, AudienceFilter, TagFilter,...rest.filters??[]]: rest.filters
    // const filters = mode==='full' ? [...SubmissionFilters,UserProfileFilter, AudienceFilter, ...rest.filters??[]]: rest.filters
    const filters = mode === 'full' ? [...SubmissionFilters, ...rest.filters ?? []] : rest.filters


    return <VirtualTable<AssetRow> rowKey='source' selectable={false} filtered={mode === 'full'}
        {...rest}
        data={rows}
        filterGroup={assetTrailGroup}
        total={filteredScopeData.length}
        filterWith={(f, i) => p(i).stringify(i).toLowerCase().includes(f.toLowerCase())}
        filters={filters}
        onDoubleClick={a => history.push(route(a))}
        sortBy={rest.sortBy ?? [['duedate', 'asc']]}>
        {columns}
    </VirtualTable>
}