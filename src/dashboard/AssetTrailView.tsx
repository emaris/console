
import { useCurrentCampaign } from "#campaign/hooks"
import { CampaignInstance } from "#campaign/model"
import { useSubmissionProfile } from '#campaign/submission/profile'
import { productType } from "#product/constants"
import { requirementType } from "#requirement/constants"
import { useLocale } from "#app/model/hooks"
import { useLogged } from '#app/user/store'
import * as React from "react"
import { useHistory } from "react-router-dom"
import { AssetTrailTable } from "./AssetTrailTable"
import { DashboardPage } from "./DashboardPage"
import { useCurrentAsset, useCurrentDashboard, useCurrentParty } from "./hooks"


export const AssetTrailView = () => {

        const {l} = useLocale()

        const history = useHistory()

        const logged = useLogged()
        const dashboard = useCurrentDashboard()

        const campaign = useCurrentCampaign()
        const subprofile = useSubmissionProfile().on(campaign)

        const profiles = { 
        
                [requirementType] : subprofile.profileOf(requirementType),
                [productType] : subprofile.profileOf(productType)
            
        }

        const {party,tenant} = useCurrentParty()
        const {type=requirementType,allForParty} = useCurrentAsset()
 
       
        const title = logged.isSingleTenantUser() ? l(campaign.name) : party && l(tenant.name)


        const filterId = `${campaign.id}-${type}`

        const isRowEnabled = (ci: CampaignInstance) => profiles[type].isForParty(party!, ci)


        return  <DashboardPage title={title} tab={type} >
                        <AssetTrailTable key={type} group='asset-list'
                                filterKey = {filterId}
                                filterGroup = {filterId}
                                filterBy = {r=>l(profiles[r.instanceType].source(r.source)?.name)}
                                data={allForParty(party!)} 
                                route={a=> `${history.location.pathname}/${dashboard.assetParam(a.instanceType,a.source)}`}
                                isRowEnabled = {isRowEnabled}
                        />

                </DashboardPage>
        }


