
import { Column, TableProps, VirtualTable } from "#app/components/VirtualTable"
import { useT } from '#app/intl/api'
import { useLocale } from "#app/model/hooks"
import { useTagModel } from "#app/tag/model"
import { useTenantStore } from '#app/tenant/store'
import { TimeLabel } from "#app/time/Label"
import { compareDates } from "#app/utils/common"
import { useCurrentCampaign } from "#campaign/hooks"
import { useCampaignModel } from '#campaign/model'
import { PartyInstance } from "#campaign/party/api"
import { PartyInstanceLabel } from "#campaign/party/Label"
import { latestSubmission, submissionIcon } from "#campaign/submission/constants"
import { SubmissionLabel } from "#campaign/submission/Label"
import { TrailSummary, useTrailSummary } from '#campaign/submission/statistics/trail'
import { TimelinessLabel } from "#campaign/submission/TimelinessLabel"
import { eventIcon } from "#event/constants"
import { NotAssessableLabel } from '#requirement/Label'
import { Tooltip } from "antd"
import React from 'react'
import { Link, useHistory } from "react-router-dom"
import { submissionType } from '../campaign/submission/constants'
import { useCurrentAsset, useCurrentDashboard, useTrail } from "./hooks"
import { useSubmissionFilter } from "./SubmissionFilter"

import { changeicon } from '#campaign/submission/changeprovider'
import { TagLabel } from "#app/tag/Label"
import { complianceIcon } from "#campaign/constants"

export type PartyRow = PartyInstance & { summary: TrailSummary }


export type Props = Partial<Omit<TableProps<PartyRow>, 'data'>> & {

    mode?: 'full' | 'summary'
    route: (_: PartyInstance) => string
    data: PartyInstance[]
    isRowEnabled?: (_: PartyInstance) => boolean
}

export const PartyTrailTable = (props: Props) => {

    const t = useT()
    const { l } = useLocale()

    const tagModel = useTagModel()

    const { mode = 'full', data, route, isRowEnabled = () => true, ...rest } = props

    const history = useHistory()

    const trailsummary = useTrailSummary()

    const tenants = useTenantStore()
    const campaignmodel = useCampaignModel()

    const campaign = useCurrentCampaign()
    const { summaries } = useCurrentDashboard()

    const campaignTimeZone = campaign.properties.timeZone ? { 'original': campaign.properties.timeZone } : undefined

    const { asset } = useCurrentAsset()
    const { submissions } = useTrail()

    const routeToSubmitted = (a: PartyRow) => `${route(a)}/${a.summary.official?.id ?? 'unknown'}`
    const routeToHistory = (a: PartyRow) => `${route(a)}/${latestSubmission}?tab=history`

    const partyTrailGroup = `${campaign.id}-${asset?.source}-trail`

    const summaryOf = (party: PartyInstance) => summaries.trails[party.source]?.find(s => s.trail.key.asset === asset?.source) ?? trailsummary.on(undefined!)

    const unfilteredrows: PartyRow[] = React.useMemo(() =>

        data.map(party => ({ ...party, summary: summaryOf(party) } as PartyRow))

        //eslint-disable-next-line
        , [data, summaries.trails])


    const { Filters: SubmissionFilters, data: rows } = useSubmissionFilter({
        rows: unfilteredrows,
        key: 'submission-filter',
        group: partyTrailGroup,
        complianceProfiles: submissions.allComplianceProfiles(),
        timelinessProfiles: submissions.allTimelinessProfiles(),
        view: 'party'
    })

    return <VirtualTable<PartyRow> rowKey='source'
        selectable={false}
        filtered={mode === 'full'}
        {...rest}
        data={rows}
        total={data.length}
        filterGroup={partyTrailGroup}
        filters={mode === 'full' ? [...props.filters ?? [], ...SubmissionFilters] : props.filters}
        onDoubleClick={p => history.push(route(p))}>

        <Column<PartyRow> min-width={200} refreshOn={asset?.source} flexGrow={1} title={t("common.fields.name_multi.name")} decorations={[]}
            dataKey="party"
            dataGetter={r => l(tenants.safeLookup(r.source)?.name)}
            cellRenderer={({ rowData: p }) => <PartyInstanceLabel className={!isRowEnabled(p) ? 'notapplicable' : ''} noDecorations linkTo={() => route(p)} instance={p} linkTarget={submissionType} />} />



        {/* <Column<PartyRow> minWidth={110} className='trail-timeliness' dataKey="timeliness"

            title={t("dashboard.labels.timeliness.title")}
            headerTooltip={t("dashboard.labels.timeliness.party_tip")}

            dataGetter={r => r.summary.computedTimeliness}
            cellRenderer={({ rowData: r }) => r.summary.computedTimeliness &&

                <Label mode='tag' className={`timeliness-${r.summary.computedTimeliness}`}
                    icon={eventIcon}
                    title={t(`dashboard.labels.timeliness.${r.summary.computedTimeliness}`)} />} /> */}

        <Column<PartyRow> minWidth={110} className='trail-timeliness' dataKey="timeliness"

            title={t("dashboard.labels.timeliness.title")}
            headerTooltip={t("dashboard.labels.timeliness.party_tip")}

            dataGetter={r => r.summary.timeliness}
            comparator={(t1,t2) => tagModel.comparator(t1?.tag, t2?.tag)}
            cellRenderer={({ rowData: r }) => isRowEnabled(r) ? r.summary.timeliness && <TimelinessLabel party={r} /> : <TagLabel className='notapplicable lbl' tag={t('submission.labels.not_applicable_badge')} icon={eventIcon} noTip />} />


        <Column<PartyRow> minWidth={150} dataKey="submitted"

            title={t("dashboard.labels.submitted.title")}
            headerTooltip={t("dashboard.labels.submitted.party_tip")}

            dataGetter={r => r.summary.official?.lifecycle.lastSubmitted}
            comparator={[(d1, d2) => compareDates(d1, d2, false), (d1, d2) => compareDates(d1, d2, true)]}
            cellRenderer={({ rowData: r }) => r.summary.official && r.summary.official.lifecycle.state !== 'missing' &&
                <SubmissionLabel tipMode='author' noPseudoNames
                    linkTo={routeToSubmitted(r)}   // if submission is last revision, this is the same as row's link.
                    submission={r.summary.official} />} />


        {(asset?.properties.assessed ?? true) && campaignmodel.currentComplianceScale(campaign) &&

            <Column<PartyRow> minWidth={150} dataKey="compliance"

                title={t("dashboard.labels.compliance.title")}
                headerTooltip={t("dashboard.labels.compliance.party_tip")}
                dataGetter={r => r.summary.assessable ? l(r.summary.compliance?.name) ?? "zz1": "zz2"}
                cellRenderer={({ rowData: r }) => {

                    if (!isRowEnabled(r)) return <TagLabel className='notapplicable lbl' tag={t("submission.labels.not_applicable_badge")} icon={complianceIcon} noTip />

                    if (r.summary.assessable === false) return <NotAssessableLabel emphasis={false} />

                    const submission = r.summary.official

                    return submission && <SubmissionLabel displayMode='compliance-rating' submission={submission} />

                }} />

        }


        <Column<PartyRow> minWidth={130} dataKey="status"

            title={t("dashboard.labels.status.title")}
            headerTooltip={t("dashboard.labels.status.party_tip")}

            dataGetter={r => r.summary.lastRevision?.lifecycle.state}
            cellRenderer={({ rowData: r }) => isRowEnabled(r) ? r.summary.lastRevision && <SubmissionLabel displayMode='state-only' noTip submission={r.summary.lastRevision} />
                                            : <TagLabel className='notapplicable lbl' tag={t("submission.status.nostatus")} icon={submissionIcon} noTip />
                                            } />



        {/* {

            mode === 'summary' ||

            <Column<Row> minWidth={150} dataKey="lastEdited"

                title={t("dashboard.labels.last_edit.title")}
                headerTooltip={t("dashboard.labels.last_edit.party_tip")}

                dataGetter={r => r.summary.last?.lifecycle.lastModified}
                cellRenderer={({ rowData: r }) => r.summary.last && <SubmissionLabel noPseudoNames tipMode='author' submission={r.summary.last} />} />

        } */}

        {

            mode === 'summary' ||

            <Column<PartyRow> minWidth={150} dataKey="lastUpdated"

                title={t("dashboard.labels.last_update.title")}
                headerTooltip={t("dashboard.labels.last_update.party_tip")}
                comparator={[(d1, d2) => compareDates(d1, d2, false), (d1, d2) => compareDates(d1, d2, true)]}

                
                dataGetter={r => r.summary.lastUpdate}
                cellRenderer={({ rowData: r }) =>{

                    const rev = r.summary.lastRevision
                    const highlight = r.summary.revisions>1 && rev?.lifecycle.firstEdited
                    const icon = highlight ? <Tooltip title={t('submission.changelog.change_alert_tip')}>{changeicon}</Tooltip>: eventIcon 
                    return  r.summary.lastUpdate && <TimeLabel className={highlight  ? 'last-update-alert' : ''} icon={icon} displayMode='relative' value={r.summary.lastUpdate} timezones={campaignTimeZone} />}
                    
                } />

        }

        {

            mode === 'summary' ||


            <Column<PartyRow> align='center' minWidth={100} dataKey="revisionCount"

                title={t("dashboard.labels.revisions.title")}
                headerTooltip={t("dashboard.labels.revisions.party_tip")}

                dataGetter={r => r.summary.revisions}
                cellRenderer={({ rowData: r }) => r.summary.lastRevision?.lifecycle.state !== 'missing' &&
                    <Link to={routeToHistory(r)}>
                        <Tooltip mouseEnterDelay={0.3} title={t("dashboard.labels.revisions.count_tip")}>
                            {r.summary.revisions && r.summary.revisions}
                        </Tooltip>
                    </Link>} />

        }

    </VirtualTable>
}