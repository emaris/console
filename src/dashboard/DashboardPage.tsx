
import { Button, Buttons } from "#app/components/Button"
import { Label } from '#app/components/Label'
import { VSelectBox } from "#app/form/VSelectBox"
import { specialise } from "#app/iam/model"
import { icns } from "#app/icons"
import { useT } from '#app/intl/api'
import { useLocale } from "#app/model/hooks"
import { PushGuard } from '#app/push/PushGuard'
import { Page } from "#app/scaffold/Page"
import { Titlebar } from "#app/scaffold/PageHeader"
import { Tab } from "#app/scaffold/Tab"
import { Topbar } from "#app/scaffold/Topbar"
import { useSettingsDrawer } from "#app/settings/hooks"
import { ContextAwareSelectBox } from "#app/system/ContextAwareSelectBox"
import { useFacets } from '#app/system/facets'
import { TagList } from "#app/tag/Label"
import { tenantIcon, tenantPlural, tenantSingular, tenantType } from "#app/tenant/constants"
import { useTenantStore } from '#app/tenant/store'
import { useLogged } from '#app/user/store'
import { ArchiveLabel, CampaignLabel, MutedLabel, SuspendedLabel } from "#campaign/Label"
import { campaignActions } from '#campaign/actions'
import { runningIcon } from "#campaign/constants"
import { useCurrentCampaign } from "#campaign/hooks"
import { PartyInstanceLabel } from "#campaign/party/Label"
import { LivePartySettings } from "#campaign/party/Settings"
import { usePartyInstances } from '#campaign/party/api'
import { useProductInstanceClient } from '#campaign/product/client'
import { useRequirementInstanceClient } from '#campaign/requirement/client'
import { useCampaignRouting, useCampaignStore } from '#campaign/store'
import { eventType } from "#event/constants"
import { useFlowCount } from "#messages/VirtualFlow"
import { messageIcon, messagePlural, messageType } from "#messages/constants"
import { productIcon, productPlural, productType } from "#product/constants"
import { requirementIcon, requirementPlural, requirementType } from "#requirement/constants"
import { Badge, Tooltip } from "antd"
import * as React from "react"
import { useHistory, useLocation } from "react-router-dom"
import { useAssetMessages, useCampaignMessages, usePartyMessages } from "./MessageView"
import { sharedDocumentsName, sharedDocumentsRoute, summaryRoute } from "./ViewRouter"
import { StatisticsContext } from './context'
import { ExportPanel, useExportDrawer } from './export'
import { useCurrentAsset, useCurrentDashboard, useCurrentParty } from "./hooks"


type Props = React.PropsWithChildren<{

    className?: string
    title?: React.ReactNode
    decorations?: JSX.Element[]
    tab: string

}>

export const DashboardPage = (props: Props) => {

    const t = useT()
    const { l } = useLocale()

    const history = useHistory()
    const { search } = useLocation()

    const campaignstore = useCampaignStore()
    const campaignrouting = useCampaignRouting()

    const logged = useLogged()

    const dashboard = useCurrentDashboard()
    const campaign = useCurrentCampaign()

    const { type, asset, route, stringify, label, allSorted } = useCurrentAsset()
    const { party, tenant } = useCurrentParty()

    const { startedCampaigns } = React.useContext(StatisticsContext)

    const tenants = useTenantStore()

    const reqinststore = useRequirementInstanceClient().on(campaign)
    const prodinststore = useProductInstanceClient().on(campaign)

    const instancestore = type === requirementType ? reqinststore : prodinststore

    const parties = usePartyInstances().on(campaign)

    const { ExportDrawer, ...xport } = useExportDrawer()
    const { SettingsDrawer, settingsButton, closeSettings } = useSettingsDrawer()

    const { title, tab, decorations, children, className } = props

    const context = useFacets().contextOf(campaign)

    const routeToDesignPage = () => party ? parties.route(party) :
        asset ? route(asset) :
            campaignrouting.routeTo(campaign)

    const designBtn = logged.hasNoTenant() && <Button iconLeft icn={icns.edit} onClick={() => history.push(routeToDesignPage())}>{t("campaign.labels.campaign_view.design")}</Button>
    const editableBtn = asset && logged.hasNoTenant() && <Button iconLeft icn={icns.edit} onClick={() => instancestore.save({...asset, properties: {...asset.properties, editable : !asset.properties.editable}}) }>{t(t("campaign.labels.asset_view.editable"))}</Button>

    const settingsTitle = t("settings.preferences_one", { one: t(tenantSingular) })
    const customSettingsBtn = React.cloneElement(settingsButton, { type: 'primary', title: settingsTitle })

    const campaignPicker = <ContextAwareSelectBox key={startedCampaigns.length} className='campaign-picker' light
        style={{ width: 250 }}
        currentContext={context}
        searchOption={c => l(campaignstore.lookup(c.id)?.name)}
        options={startedCampaigns}
        onChange={c => history.push(dashboard.preserveRouteTo(c))}
        renderOption={c => <CampaignLabel mode='option' campaign={c} />}
        lblTxt={c => l(c.name)}
        optionId={c => c.id}>

        {[campaign]}

    </ContextAwareSelectBox>

    const allParties = party ? parties.allSorted() : []

    ///// Real logic
    // const partyPicker =  (party && !logged.isSingleTenantUser() && allParties.length > 1) && <VSelectBox className='party-picker' light 
    //                 style={{width:170}}
    //                 searchOption={parties.stringify}
    //                 options={allParties}  onChange={pi=>history.push(`${dashboard.routeToParty(pi)}/${type}`)} 
    //                 renderOption={p=><PartyInstanceLabel mode='option' instance={p} />}
    //                 optionId={p=>p.source }>

    //                 {[party]}

    //                 </VSelectBox>

    ///// For demo 25/06/2020
    const partyPicker = party && logged.hasNoTenant() && <VSelectBox className='party-picker' light
        style={{ width: 170 }}
        searchOption={parties.stringify}
        options={allParties} onChange={pi => history.push(`${dashboard.routeToParty(pi)}/${history.location.pathname.substring(history.location.pathname.lastIndexOf('/') + 1)}`)}
        renderOption={p => <PartyInstanceLabel mode='option' instance={p} />}
        optionId={p => p.source}
        lblTxt={p => l(tenants.lookup(p.source)?.name)}>

        {[party]}

    </VSelectBox>

    const assetPicker = type && asset && <VSelectBox className='asset-picker' light
        style={{ width: 250 }}
        searchOption={a => stringify(a)}
        options={allSorted()} onChange={a => history.push(`${dashboard.routeToAsset(a)}/${history.location.pathname.substring(history.location.pathname.lastIndexOf('/') + 1)}`)}
        renderOption={a => label(a, { mode: 'option' })}
        optionId={p => p.source}>

        {[asset]}

    </VSelectBox>


    const routeType = (type: string) => {

        history.push(

            // navigate at party level
            party ? `${dashboard.routeToParty(party)}/${type}${search}` :
                // navigate at asset level
                asset ? `${dashboard.routeToAsset(asset)}/${type}${search}` :

                    // navigate at top level
                    `${dashboard.routeToCampaign()}/${type}${search}`

        )

    }


    const campaignMessages = useCampaignMessages()
    const partyMessages = usePartyMessages(party!)
    const assetMessages = useAssetMessages(asset!)

    const messages = party ? partyMessages : asset ? assetMessages : campaignMessages

    const manage = party ? logged.managesSomeTenant() : logged.can(specialise(campaignActions.manage, campaign.id))

    const unreadMessages = useFlowCount(messages.id, messages.flow)
    const unreadBadge = <Badge dot={unreadMessages > 0} className='unread-message-badge'>{t(messagePlural)}</Badge>

    const campaignPills = <React.Fragment>
        <CampaignLabel mode='tag' statusOnly campaign={campaign} />
        <ArchiveLabel campaign={campaign} />
        <SuspendedLabel campaign={campaign} />
        <MutedLabel campaign={campaign} />
        <TagList taglist={campaign.tags} />
    </React.Fragment>

    const partyPills = <React.Fragment>
        <ArchiveLabel campaign={campaign} />
        <SuspendedLabel campaign={campaign} />
        <MutedLabel campaign={campaign} />
        <TagList taglist={tenant.tags} />
    </React.Fragment>

    return <PushGuard>

        <Page className={className}>

            <Topbar activeTab={tab} onTabChange={routeType}>


                <Titlebar title={title || l(campaign.name)}>
                    <div style={{ display: "flex", alignItems: "center" }}>
                        {asset ?

                            <React.Fragment>
                                <ArchiveLabel campaign={campaign} />
                                <SuspendedLabel campaign={campaign} />
                                <MutedLabel campaign={campaign} />
                                {label(asset, { mode: 'tag', deadlineOnly: true, accentScheme: "weeks" })}
                                {asset.properties.editable === false && <Label iconStyle={{ color: 'white' }} style={{ background: 'orange', color: 'white' }} mode='tag' title={t('submission.labels.not_editable')} />}
                                {asset.properties.assessed === false && <Label iconStyle={{ color: 'white' }} style={{ background: 'orange', color: 'white' }} mode='tag' title={t('submission.labels.not_assessable')} />}
                                <TagList taglist={asset.tags} />
                            </React.Fragment>

                            :

                            party ?

                                logged.hasNoTenant() ? partyPills : campaignPills
                                :
                                campaignPills
                        }

                    </div>
                </Titlebar>


                <Tab id={summaryRoute} icon={runningIcon} name={t("dashboard.labels.summary")} />
                <Tab id={eventType} icon={icns.calendar} name={t("dashboard.calendar.name")} />

                {!!party ||
                    <Tab id={tenantType} icon={tenantIcon} name={t(tenantPlural)} />
                }

                {!!asset || <Tab id={requirementType} icon={requirementIcon} name={t(requirementPlural)} />}
                {!!asset || <Tab id={productType} icon={productIcon} name={t(productPlural)} />}


                <Tab id={messageType} icon={messageIcon} name={unreadMessages > 0 ? <Tooltip title={t('message.unread_count', { count: unreadMessages })}>{unreadBadge}</Tooltip> : unreadBadge} />


                {!asset && <Tab id={sharedDocumentsRoute} icon={icns.share} name={t(sharedDocumentsName)} />}

                <div className="dashboard-pickers">
                    {decorations && decorations.map((d, i) => <div key={`decoration_${i}`}>{d}</div>)}
                    {assetPicker}
                    {partyPicker}
                    {campaignPicker}
                    <Buttons>
                        {designBtn}
                        {editableBtn}
                        {party && customSettingsBtn}
                        {/* only global for now.. */}
                        {!!party || !!asset || xport.openBtn}       
                    </Buttons>
                </div>

            </Topbar>

            {children}

            {party &&

                <SettingsDrawer readonly={!manage} title={settingsTitle}>
                    <LivePartySettings party={party} onSave={closeSettings} />
                </SettingsDrawer>

            }

            <ExportDrawer>
                    {(!!party && null) || (!!asset && null) || <ExportPanel />}
            </ExportDrawer>

        </Page>
    </PushGuard>

}
