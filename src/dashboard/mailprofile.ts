import { TParams } from '#app/mail/model'
import { noTenant } from '#app/tenant/constants'
import { useTenantStore } from '#app/tenant/store'
import { useCurrentCampaign } from '#campaign/hooks'
import { useMessageHelper } from '#campaign/messagehelper'
import { usePartyInstances } from '#campaign/party/api'
import { useProductInstances } from '#campaign/product/api'
import { useRequirementInstances } from '#campaign/requirement/api'
import { FlowMailProfile, MessageDto } from '#messages/model'
import { productType } from '#product/constants'
import { useProductStore } from '#product/store'
import { requirementType } from '#requirement/constants'
import { useRequirementStore } from '#requirement/store'

export const useMessageMailProfile = () => {

    const campaign = useCurrentCampaign()

    const messages = useMessageHelper()

    const tenants = useTenantStore()
    const requirements = useRequirementStore()
    const products = useProductStore()
    const partyinst = usePartyInstances()
    const reqinst = useRequirementInstances()
    const productinst = useProductInstances()


    return (message: MessageDto): FlowMailProfile => {

        const parties = partyinst.on(campaign)

        const { requirements: rids, products: pids, tenants: tids } = messages.message(message).extract()

        const aid = rids[0] ?? pids[0]

        const type = rids[0] ? requirementType : productType

        const assets = type === requirementType ? requirements : products
        const instances = (type === requirementType ? reqinst : productinst).on(campaign)

        // full asset and instance.
        const asset = assets.lookup(aid)
        const assetInstance = instances.lookupBySource(aid)

        // mail targets: any extracted, or audience (asset braodcast), or all in campaign (campaign broadcast)
        const partyTargets = tids.length ? tids :  parties.all().filter(p => !assetInstance || instances.isForParty(p, assetInstance)).map(p => p.source)

        const parameters: TParams = { campaign: campaign.name, campaignId: campaign.id }

        // if single target, we add params to better customise mail.
        if (partyTargets.length === 1) {

            const party = tenants.lookup(partyTargets[0])

            if (party) {
                parameters.tenantId = party.id
                parameters.tenant = party.name
            }
        }

        // add parameters for asset, if we have one. 
        if (asset) {
            parameters.assetType = type
            parameters.asset = asset?.name
            parameters.assetId = asset?.id
            parameters.assetTitle = asset?.description
        }

        //console.log({rids, pids, aid, assetInstance, parameters, tids, partyTargets, topics: message.topics})

        return { targets: [noTenant, ...partyTargets], parameters }
    }

}