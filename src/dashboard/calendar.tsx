import { Label } from "#app/components/Label";
import { useT } from '#app/intl/api';
import { useL } from "#app/model/multilang";
import { TenantLabel } from "#app/tenant/Label";
import { tenantType } from "#app/tenant/constants";
import { TenantDto } from "#app/tenant/model";
import { useTenantStore } from "#app/tenant/store";
import { UserLabel } from "#app/user/Label";
import { useLogged } from '#app/user/store';
import { clock } from "#app/utils/common";
import { campaignSingular, campaignType } from "#campaign/constants";
import { EventInstanceLabel } from "#campaign/event/Label";
import { EventInstance, EventInstanceDto } from "#campaign/event/model";
import { useEventInstanceProfile } from '#campaign/event/profile';
import { useEventInstanceStore } from '#campaign/event/store';
import { useCurrentCampaign } from "#campaign/hooks";
import { Campaign } from "#campaign/model";
import { PartyInstanceDto, usePartyInstances } from "#campaign/party/api";
import { useCampaignUserPreferences } from "#campaign/preferences/api";
import { ProductInstanceLabel } from "#campaign/product/Label";
import { useProductInstances } from '#campaign/product/api';
import { RequirementInstanceLabel } from "#campaign/requirement/Label";
import { useRequirementInstances } from '#campaign/requirement/api';
import { SubmissionLabel } from "#campaign/submission/Label";
import { SubmissionStub, TrailDto } from "#campaign/submission/model";
import { useSubmissionProfile } from '#campaign/submission/profile';
import { useSubmissionStore, useTrailLookups } from '#campaign/submission/store';
import { eventType } from '#event/constants';
import { productSingular, productType } from "#product/constants";
import { ProductDto } from "#product/model";
import { useProductStore } from '#product/store';
import { requirementSingular, requirementType } from "#requirement/constants";
import { RequirementDto } from "#requirement/model";
import { useRequirementStore } from '#requirement/store';
import moment, { Moment } from "moment-timezone";
import React from "react";
import { Trans } from "react-i18next";
import shortid from "shortid";
import { CalendarRow, Group } from "./CalendarTable";
import { useDashboard } from './api';

const labelMargin = 4

const marginLeft = { marginLeft: labelMargin }
const marginRight = { marginRight: labelMargin }
const margin = { ...marginLeft, ...marginRight }

export const useCalendarContext = () => React.useContext(CalendarContextProvider)

export const useDashboardCalendar = () => {

    const t = useT()
    const l = useL()


    const logged = useLogged()

    const tenants = useTenantStore()

    const reqstore = useRequirementStore()
    const prodstore = useProductStore() 
    const reqinstances = useRequirementInstances()
    const prodinstances = useProductInstances()

    const dash = useDashboard()
    const substore = useTrailLookups()
    const subprofile = useSubmissionProfile()
    const eventinststore = useEventInstanceStore()
    const eventinstprofile = useEventInstanceProfile()

    const userCampaignLocation = useCampaignUserPreferences().dateLocation(logged)
    const userLocation = logged.details.preferences?.location
    const defaultTz = moment.tz.guess()
   

    

    return {
        on: (c: Campaign) => {

            const dashboard = dash.on(c)
            const events = { ...eventinststore.on(c), ...eventinstprofile.on(c) }
   
            const eventsTranslations = {
                'other': t('event.types.reminder_singular'),
                [requirementType]: t(requirementSingular),
                [productType]: t(productSingular),
                [campaignType]: t(campaignSingular)
            }

            const profile = subprofile.on(c)

            const profiles = {

                [requirementType]: profile.profileOf(requirementType),
                [productType]: profile.profileOf(productType)

            }

            const self = {

                eventInstanceToCalendarRow: (e: EventInstance, allEvents: EventInstanceDto[], submission?: SubmissionStub, party?: PartyInstanceDto): CalendarRow => {

                    const target = (e.type === productType || e.type === requirementType) ? e.type === productType ? prodinstances.on(c).lookupSource(e.target) : reqinstances.on(c).lookupSource(e.target) : undefined
                    const targetType = (e.type === productType || e.type === requirementType) ? e.type : undefined

                    const getTimezonedDateFor = (date: string | undefined): Moment => {

                        let tz

                        switch (userCampaignLocation) {
                            case 'home' : tz = userLocation ?? defaultTz; break;
                            case 'local' : tz = defaultTz; break;
                            case 'original' : tz = c.properties.timeZone ?? userLocation ?? defaultTz; break;
                            default: tz = defaultTz
                        }

                        return date ? moment(date).tz(tz) : undefined!

                    }


                    const absoluteDate = events.absolute(e.date)
                    const date = getTimezonedDateFor(absoluteDate)

                    return {
                        id: `COL-${shortid()}`
                        ,
                        label: self.eventCalendarLabel(e, submission, party)
                        ,
                        date
                        ,
                        targetType
                        ,
                        type: eventsTranslations[e.type]
                        ,
                        target
                        ,
                        stringify: (f, i) => {
                            const eventString = events.stringify(e)
                            const targetString = i.targetType === requirementType ? reqinstances.on(c).stringifySource(i.target as RequirementDto) : i.targetType === productType ? prodinstances.on(c).stringifySource(i.target as ProductDto) : ''
                            return `${eventString} ${targetString}`.toLowerCase().includes(f.toLocaleLowerCase())
                        }
                        ,

                        filter: (selections): boolean => {
                            // driven by 1) type menu and 2) asset menu, if they are about assets and those assets are selected

                            const typeSelected = selections[eventType]?.includes(e.type)

                            const aboutAsset = [requirementType, productType].includes(e.type) && e.target
                            const assetSelected = selections[e.type]?.includes(e.target!)

                            return typeSelected && (!aboutAsset || assetSelected)
                        }
                    }
                }

                ,

                buildCalendarRows: (trails: TrailDto[], labelFor: 'all' | 'asset' | 'party') => {


                    const submissionToCalendarRow = (trail: TrailDto, submission: SubmissionStub, type: 'submitted' | 'assessed'): CalendarRow => {

                        const target = trail.key.assetType === requirementType ? reqinstances.on(c).lookupSource(trail.key.asset) : prodinstances.on(c).lookupSource(trail.key.asset)
                        const party = tenants.safeLookup(trail.key.party)
                        const partyname = l(party.name)
                        const date = submission.lifecycle.lastSubmitted

                        // const rowLabelType = type === 'submitted' ? 'submission' : 'assessment'

                        // const resolveLabel = ():JSX.Element => {
                        //     if (labelFor === 'all') return self.fullCalendarLabel(type, submission, trail, party)
                        //     else if (labelFor === 'asset') return self.partyCalendarLabel(type, submission, trail, party)
                        //     else return self.assetCalendarLabel(trail.key.assetType, type, target, submission, trail )
                        // }

                        // const label = resolveLabel()

                        let label
                        if (labelFor === 'all') label = self.fullCalendarLabel(type, submission, trail, party)
                        else if (labelFor === 'asset') label = self.partyCalendarLabel(type, submission, trail, party)
                        else label = self.assetCalendarLabel(trail.key.assetType, type, target, submission, trail)

                        return {
                            id: `COL-${shortid()}`
                            ,
                            type
                            ,
                            date: date ? moment(date) : undefined!
                            ,
                            party
                            ,
                            targetType: trail.key.assetType === requirementType ? requirementType : productType
                            ,
                            target
                            ,
                            label
                            ,
                            stringify: (f) => labelFor === 'all' ?
                                `${l(target.name)} ${t("submission.status.submitted")} ${partyname}`.toLocaleLowerCase().includes(f.toLocaleLowerCase()) :
                                labelFor === 'asset' ?
                                    `${partyname} ${t("submission.status.submitted")}`.toLocaleLowerCase().includes(f.toLocaleLowerCase()) :
                                    `${l(target.name)} ${t("submission.status.submitted")}`.toLocaleLowerCase().includes(f.toLocaleLowerCase())

                            ,
                            filter: (selections): boolean => {

                                const allAssets = [...selections[requirementType] ?? {}, ...selections[productType] ?? {}]

                                const partySelected = selections[tenantType]?.includes(trail.key.party)
                                const assetSelected = allAssets.includes(trail.key.asset)
                                const typeSelected = selections['action']?.includes(type)


                                return partySelected && assetSelected && typeSelected

                            }
                        }
                    }

                    const trailsMap = clock('computing trailMap', () => trails.map(trail => {
                        const setDataFor = (s: SubmissionStub, type: 'submission' | 'assessment'): moment.Moment => {
                            return type === 'submission' ? moment(s.lifecycle.lastSubmitted) :
                                s.lifecycle.compliance?.lastAssessed === undefined ? moment(s.lifecycle.lastSubmitted) : moment(s.lifecycle.compliance.lastAssessed)
                        }

                        const submissions = trail.submissions.filter(s => s.lifecycle.lastSubmitted !== undefined).sort((a, b) => moment(a.lifecycle.lastSubmitted).isAfter(b.lifecycle.lastSubmitted) ? 1 : 0)
                        const assessments = trail.submissions.filter(s => s.lifecycle.compliance?.lastAssessed).sort((a, b) => moment(a.lifecycle.compliance?.lastAssessed).isAfter(b.lifecycle.compliance?.lastAssessed) ? 1 : 0)

                        const allSubmissions = submissions.map(submission => (submissionToCalendarRow(trail, submission, 'submitted'))) as CalendarRow[]
                        const allAssessments = assessments.map(assessment => ({ ...submissionToCalendarRow(trail, assessment, 'assessed'), date: setDataFor(assessment, 'assessment') })) as CalendarRow[]

                        return [...allAssessments, ...allSubmissions]

                    }))

                    return trailsMap.flat().filter(e => e.date !== undefined)
                }

                ,

                //Find distinct values in an array or array[object] with max 2 levels depth
                getDistinct: <T extends {}>(array: T[], prop: string, subprop: string | undefined, sort: (a: any, b: any) => number, filter?: (_: T) => boolean) => {

                    const sorted = filter === undefined ? array.filter(a => a[prop] !== undefined).map(t => t[prop]).sort(sort) : array.filter(a => a[prop] !== undefined).filter(filter).map(t => t[prop]).sort(sort)
                    return sorted.reduce((acc, curr) => !acc.includes(subprop === undefined ? curr! : curr[subprop]!) ? [...acc, subprop === undefined ? curr! : curr[subprop]!] : [...acc], [] as any[])

                }

                ,

                dateToInt: (year: number, month: number) => parseInt(`${year}${month}`)

                ,

                closestMonthFor: (groups: Group[]) => {

                    if (!groups || groups.length === 0) return 0

                    const currentDate = moment(new Date(), 'YYYY/MM/DD')

                    const currentMonth = self.dateToInt(parseInt(currentDate.format('YYYY')), (parseInt(currentDate.format('M')) - 1))

                    const closest = groups.filter(g => g.group.length > 0).reduce((prev, curr) => Math.abs(self.dateToInt(curr.key.year, curr.key.month) - currentMonth) < Math.abs(self.dateToInt(prev.key.year, prev.key.month) - currentMonth) ? curr : prev)

                    const closestMonth = self.dateToInt(closest.key.year, closest.key.month)

                    return groups.findIndex(group => self.dateToInt(group.key.year, group.key.month) === closestMonth)
                }

                ,

                currentMonthFor: (groups: Group[]) => {
                    const currentDate = moment(new Date(), 'YYYY/MM/DD')
                    const currentMonth = self.dateToInt(parseInt(currentDate.format('YYYY')), (parseInt(currentDate.format('M')) - 1))

                    const idx = groups.findIndex(group => self.dateToInt(group.key.year, group.key.month) === currentMonth)

                    return idx >= 0 ? idx : self.closestMonthFor(groups)
                }

                ,

                dateToNumber: (y: string | number, m: number | number) => parseInt(`${typeof y === 'string' ? y : y.toString()}${typeof m === 'string' ? m : m.toString()}`)

                ,

                closestGroupToNow: (grouped: (CalendarRow | { id: any, title: any })[]) => {

                    const currentDate = moment(new Date(), 'YYYY/MM/DD')
                    const currentYearMonth = self.dateToNumber(currentDate.format('YYYY'), parseInt(currentDate.format('M')) - 1)

                    const headers =  grouped.filter(g => 'title' in g )

                    const closest = headers.length ? headers.reduce((prev, cur) =>    {
                        const numberizedDateCur = cur ? self.dateToNumber(cur['title'].year, cur['title'].month) : 0
                        const numberizedDatePrev = prev ? self.dateToNumber(prev['title'].year, prev['title'].month) : 0

                        return Math.abs(numberizedDateCur - currentYearMonth) < Math.abs(numberizedDatePrev - currentYearMonth) ? cur : prev

                    }) : undefined

                    return closest

                }

                ,

                eventCalendarLabel: (event: EventInstance, submission?: SubmissionStub, party?: PartyInstanceDto) => {

                    const tenant = logged.isTenantUser() ? tenants.lookup(logged.tenant) : undefined

                    const sourceEvent = events.lookup(event.target)

                    let asset = reqstore.lookup(event.target) || prodstore.lookup(event.target)
                    if (asset === undefined) {
                        asset = sourceEvent ? (reqstore.lookup(sourceEvent!.target) || prodstore.lookup(sourceEvent!.target)) : undefined
                    }
                   
                    const assetType = (event.type === requirementType || event.type === productType) ? event.type : sourceEvent ? (sourceEvent.type === requirementType || sourceEvent.type === productType) ? sourceEvent.type : undefined : undefined


                    const linkTo = asset ?
                        tenant ?
                            `${dashboard.routeTo(c)}/${tenantType}/${tenant.id}/${assetType}/${asset.id}`
                            :
                            `${dashboard.routeTo(c)}/${assetType}/${asset.id}`
                        :
                        undefined


                    const assetInstance = assetType ? assetType === 'requirement' ? reqinstances.on(c).lookupBySource(event.target) : prodinstances.on(c).lookupBySource(event.target) : undefined

                    const isEnabled = (assetInstance && party) ?
                        profiles[assetType!].isForParty(party, assetInstance)
                        : true

                    const notApplicableclass = !isEnabled ? 'notapplicable' : ''

                    return <div style={{ display: 'flex' }}>
                        <EventInstanceLabel className={`event-type ${notApplicableclass}`} noDate instance={event} noLineage />
                        {
                            event.target !== c.id &&
                            <>
                                <Label style={{ ...margin, paddingTop: 1.4 }} className={`label-main ${notApplicableclass}`} title={t("common.labels.for")} noIcon />
                                {events.given(event).labelOf(event.target, { noDate: true, noIcon: true, linkTo, className: notApplicableclass, noLineage: true })!}
                                {submission && <SubmissionLabel className={notApplicableclass} submission={submission} displayMode="state-only" />}
                            </>
                        }
                    </div>
                }

                ,

                fullCalendarLabel: (type: "submitted" | "assessed", submission: SubmissionStub, trail?: TrailDto, party?: TenantDto) => {


                    const resolvedTrail = trail ?? substore.on(c).lookupTrail(submission.trail)!

                    const campaignRoute = dashboard.routeTo(c)
                    const linkToParty = `${campaignRoute}/${tenantType}/${party?.id}`
                    const assetType = resolvedTrail?.key.assetType ?? undefined

                    const assetInstance = resolvedTrail ? resolvedTrail.key.assetType === 'requirement' ? reqinstances.on(c).lookupBySource(resolvedTrail.key.asset) : prodinstances.on(c).lookupBySource(resolvedTrail.key.asset) : undefined

                    const linkToSubmission = submission && `${linkToParty}/${resolvedTrail.key.assetType}/${resolvedTrail.key.asset}/${submission.id}`

                    const isEnabled = (assetInstance && party) ?
                        profiles[assetType!].isForParty(party, assetInstance)
                        : true


                    const notApplicableclass = !isEnabled ? 'notapplicable' : ''

                    const tenantLabel = (noIcon: boolean) => <TenantLabel className={`party-label ${notApplicableclass}`} tenant={party} linkTo={linkToParty} noIcon={noIcon} style={noIcon ? marginLeft : marginRight} linkTarget={undefined} />
                    const assetLabel = (noIcon: boolean) => <SubmissionLabel className={notApplicableclass} mode='light' submission={submission} linkTo={linkToSubmission} displayMode='asset' noIcon={noIcon} style={noIcon ? margin : marginRight} />

                    return <div style={{ display: 'flex' }}>
                        {
                            type === 'submitted' ?
                                <Trans i18nKey="dashboard.calendar.submission_full">
                                    {tenantLabel(false)}
                                    <Label className="event-label" title={t("dashboard.labels.submitted.title")} tip={() => <UserLabel user={submission.lifecycle.lastModifiedBy} />} noLink noIcon style={marginLeft} />
                                    {assetLabel(true)}
                                </Trans>
                                :
                                <Trans i18nKey="dashboard.calendar.assessed_full">
                                    {tenantLabel(false)}
                                    <Label className="event-label" title={t("dashboard.labels.assessed.title")} tip={() => <UserLabel user={submission.lifecycle.compliance?.lastAssessedBy} />} noLink noIcon style={margin} />
                                    {assetLabel(true)}
                                </Trans>
                        }

                    </div>
                }

                ,

                assetCalendarLabel: (assetType: string, type: "submitted" | "assessed", asset: ProductDto | RequirementDto, submission: SubmissionStub, trail?: TrailDto) => {

                    const resolvedTrail = trail ?? substore.on(c).lookupTrail(submission.trail)!

                    const linkToSubmission = dashboard.partyRouteToSubmissionWithTrail(submission, resolvedTrail)

                    const prodinst = prodinstances.on(c)
                    const reqinst = reqinstances.on(c)

                    const linkToAsset = assetType === requirementType ? reqinst.route(reqinst.all().find(i => i.source === asset.id)) : prodinst.route(prodinst.all().find(i => i.source === asset.id))
                   
                    const assetLabel = (noIcon: boolean) => assetType === requirementType ?
                        <RequirementInstanceLabel style={{ marginRight: 4 }} noLineage instance={reqinst.all().find(i => i.source === asset.id)} noIcon={noIcon} linkTo={linkToAsset} noOptions />
                        :
                        <ProductInstanceLabel style={{ marginRight: 4 }} noLineage instance={prodinst.all().find(i => i.source === asset.id)} noIcon={noIcon} linkTo={linkToAsset} noOptions />

                    return <div style={{ display: 'flex' }}>{
                        type === "submitted" ?
                            <Trans i18nKey="dashboard.calendar.submission_party" >
                                {assetLabel(false)}
                                <Label style={{ marginLeft: 4 }} className="asset-label" title={t("dashboard.labels.submitted.name")} tip={() => <UserLabel user={submission.lifecycle.lastModifiedBy} />} linkTo={linkToSubmission} noIcon />
                            </Trans>
                            :
                            <Trans i18nKey="dashboard.calendar.assessed_party">
                                {assetLabel(false)}
                                <Label style={{ marginLeft: 4 }} title={t("dashboard.labels.assessed.title")} tip={() => <UserLabel user={submission.lifecycle.compliance?.lastAssessedBy} />} linkTo={linkToSubmission} noIcon />
                            </Trans>
                    }
                    </div>

                }

                ,

                partyCalendarLabel: (type: "submitted" | "assessed", submission: SubmissionStub, trail?: TrailDto, party?: TenantDto) => {

                    const linkToSubmission = dashboard.partyRouteToSubmission(submission)
                    const linkToParty = `${dashboard.routeTo(c)}/${tenantType}/${party?.id}`

                    const resolvedTrail = trail ?? substore.on(c).lookupTrail(submission.trail)
                    const assetType = resolvedTrail?.key.assetType ?? undefined
                    const assetInstance = resolvedTrail ? resolvedTrail.key.assetType === 'requirement' ? reqinstances.on(c).lookupBySource(resolvedTrail.key.asset) : prodinstances.on(c).lookupBySource(resolvedTrail.key.asset) : undefined

                    const isEnabled = (assetInstance && party) ?
                        profiles[assetType!].isForParty(party, assetInstance)
                        : true

                    const notApplicableclass = !isEnabled ? 'notapplicable' : ''

                    const tenantLabel = <TenantLabel tenant={party} linkTo={linkToParty} className={notApplicableclass} style={{ marginRight: 4 }} />

                    return <div style={{ display: 'flex' }}>{
                        type === "submitted" ?
                            <Trans i18nKey="dashboard.calendar.submission_asset" >
                                {tenantLabel}
                                <Label className={`asset-label ${notApplicableclass}`} title={t("dashboard.labels.submitted.name")} tip={() => <UserLabel user={submission.lifecycle.lastModifiedBy} />} linkTo={linkToSubmission} noIcon style={{ marginLeft: 4 }} />
                            </Trans>
                            :
                            <Trans i18nKey="dashboard.calendar.assessed_asset">
                                {tenantLabel}
                                <Label className={notApplicableclass} title={t("dashboard.labels.assessed.title")} tip={() => <UserLabel user={submission.lifecycle.compliance?.lastAssessedBy} />} linkTo={linkToSubmission} noIcon style={{ marginLeft: 4 }} />
                            </Trans>
                    }
                    </div>

                }

            }

            return self
        }


    }
}

export type CalendarContextType = React.PropsWithChildren<{
    general: CalendarRow[] | undefined
    party: Record<string, CalendarRow[]> | undefined
    asset: Record<string, CalendarRow[]> | undefined
    regenerate: boolean
}>

type CalendarContextProviderType = {
    calendars: CalendarContextType
    setCalendars: (_: CalendarContextType) => void
}

const CalendarContextProvider = React.createContext<CalendarContextProviderType>(undefined!)

export const CalendarContext = (props: React.PropsWithChildren<{}>) => {

    const campaign = useCurrentCampaign()
    const eventInstances = useEventInstanceStore().on(campaign)
    const events = eventInstances.all()
    const trails = useSubmissionStore().on(campaign).allTrails()
    const partyinststore = usePartyInstances().on(campaign)
    const reqinststore = useRequirementInstances().on(campaign)
    const prodinststore = useProductInstances().on(campaign)
    const allPartyInstances = partyinststore.all()
    const allRequirementInstances = reqinststore.all()
    const allProductInstances = prodinststore.all()

    const deps = React.useMemo(() => [events, trails, allPartyInstances, allRequirementInstances, allProductInstances] as const, [events, trails, allPartyInstances, allRequirementInstances, allProductInstances])

    const [calendars, setCalendars] = React.useState<CalendarContextType>(undefined!)

    //eslint-disable-next-line
    const setCalendarAndReset = React.useCallback((value: CalendarContextType) => setCalendars({ ...value, regenerate: false }), [deps])
    //eslint-disable-next-line
    const value = React.useMemo(() => ({ calendars, setCalendars: setCalendarAndReset }), [calendars])

    React.useEffect(() => {
        if (calendars)
            setCalendars({ ...calendars, regenerate: true })
        //eslint-disable-next-line
    }, [deps])

    return <CalendarContextProvider.Provider value={value}>
        {props.children}
    </CalendarContextProvider.Provider>
}


