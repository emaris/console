import { iamType } from "#app/iam/constants";
import { IamSlot } from "#app/iam/module";
import { Module } from "#app/module/model";
import { pushEventType } from '#app/push/constants';
import { tenantType } from '#app/tenant/constants';
import { TenantSlot } from '#app/tenant/module';
import { eventType } from "#event/constants";
import { EventSlot } from "#event/module";
import { productActions } from "./actions";
import { productIcon, productPlural, productSingular, productType } from "./constants";
import { usePushProductSlot } from './pushevents';


export const useProductModule = (): Module => {

    const pushProductSlot = usePushProductSlot()

    return {

        icon: productIcon,
        type: productType,

        nameSingular: productSingular,
        namePlural: productPlural,

        [tenantType]: {


            tenantResource: true

        } as TenantSlot

        ,

        [pushEventType]: pushProductSlot

        ,


        [iamType]: {

            actions: productActions,

        } as IamSlot,


        [eventType]: {

            enable: true

        } as EventSlot

    }

}