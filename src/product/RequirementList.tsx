
import { Product, useProductModel } from '#product/model';
import { requirementRoute } from "#requirement/constants";
import { RequirementLabel } from "#requirement/Label";
import { Requirement } from "#requirement/model";
import { useRequirementStore } from '#requirement/store';
import { Column, VirtualTable } from "#app/components/VirtualTable";
import { icns } from "#app/icons";
import { useLocale } from "#app/model/hooks";
import { TagList } from "#app/tag/Label";
import { ReportMethods } from "#app/utils/validation";
import * as React from "react";
import { useT } from '#app/intl/api';
import { useHistory } from "react-router";
import { useProductStore } from './store';


type Props = {

    product: Product
    report?: ReportMethods
    height?: number
}


export const ProductRequirementList = (props: Props) => {

    const history = useHistory()
    const t = useT()
    const { l } = useLocale()

    const reqstore = useRequirementStore()

    const { height, product, report = {} } = props

    const invalid = ({ rowData: r }) => report[r.id]?.status === "error" && icns.error(report[r.id].msg)

    const store = useProductStore()
    const model = useProductModel()
   
    const assets : (Product | Requirement)[] = [...store.all(), ...reqstore.all()]
    
    // eslint-disable-next-line
    const data = React.useMemo(() => model.dependenciesOf(product).map( ({asset}) => assets.find( a => a.id === asset)! ).filter( asset => !!asset), [product])

    return <VirtualTable<Requirement | Product> height={height} selectable={false} filtered={data.length > 10}
        data={data} rowKey="id"
        onDoubleClick={t => history.push(`${requirementRoute}/${t.id}`)} >

        <Column title={t("common.fields.name_multi.name")} dataKey="name" decorations={[invalid]}
            dataGetter={(r: Requirement) => l(r.name)} cellRenderer={r => <RequirementLabel lineage requirement={r.rowData} />} />

        <Column flexGrow={1} title={t("common.fields.tags.name")} dataKey="tags" dataGetter={(r: Requirement) => <TagList taglist={r.tags} />} />

    </VirtualTable>

}