
import { useT } from '#app/intl/api';
import { useToggleBusy } from "#app/system/api";
import { through } from "#app/utils/common";
import { notify, showAndThrow, useFeedback } from "#app/utils/feedback";
import { usePreload } from 'apprise-frontend-core/client/preload';
import { productApi, useProductCalls } from "./calls";
import { productPlural, productSingular, productType } from "./constants";
import { Product, ProductDto } from "./model";
import { useProductStore } from './store';
import { Paragraph } from '#app/components/Typography';
import { Trans } from 'react-i18next';
import { useL } from 'apprise-frontend-core/intl/multilang';


export const useProductClient = () => {

    const t = useT()
    const l = useL()
    const toggleBusy = useToggleBusy()
    const fb = useFeedback()

    const store = useProductStore()
    const call = useProductCalls()

    const singular = t(productSingular).toLowerCase()
    const plural = t(productPlural).toLowerCase()

    const preload = usePreload()

    const self = {

        areReady: () => !!store.all()

        ,

        isLoaded: (prod: Product | undefined) => !!prod?.properties?.layout


        ,

        fetchAll: (forceRefresh = false): Promise<Product[]> =>

            self.areReady() && !forceRefresh ? Promise.resolve(store.all())

                :

                toggleBusy(`${productType}.fetchAll`, t("common.feedback.load", { plural }))

                    .then(_ => console.log(`fetching products...`))
                    .then(_ => preload.get<Product[]>(productApi) ?? call.fetchAll())
                    .then(through($ => store.setAll($, { noOverwrite: true })))


                    .catch(e => showAndThrow(e, t("common.calls.fetch_all_error", { plural })))
                    .finally(() => toggleBusy(`${productType}.fetchAll`))

        ,


        fetchOne: (id: string, forceRefresh?: boolean): Promise<Product> => {

            const current = store.lookup(id)

            return current && self.isLoaded(current) && !forceRefresh ?

                Promise.resolve(current) :

                toggleBusy(`${productType}.fetchOne`, t("common.feedback.load_one", { singular }))

                    .then(_ => call.fetchOne(id))
                    .then(through(fetched => { if (!fetched) throw Error(`Invalid resource lookup for ${id}, returned undefined.`) }))
                    .then(through(fetched => store.setAll(store.all().map(u => u.id === id ? fetched : u))
                    ))

                    .catch(e => showAndThrow(e, t("common.calls.fetch_one_error", { singular })))
                    .finally(() => toggleBusy(`${productType}.fetchOne`))

        }

        ,

        fullFetch: (product: Product, force?: boolean): Promise<Product> =>

            self.isLoaded(product) && !force ? Promise.resolve(product) : self.fetchOne(product.id)



        ,




        fetchManySilently: (ids: string[], force?: boolean): Promise<Product[]> => {

            const filteredIds = ids.filter(id => force || !self.isLoaded(store.lookup(id)))

            const fetchFiltered = filteredIds.map(id =>

                call.fetchOne(id)

                    .catch(e => {

                        console.warn(`warning: couldn't fetch product ${id}:`, e)
                        return Promise.resolve(undefined)

                    }))

            return Promise.all(fetchFiltered)
                .then(fetched => fetched.filter(p => p) as ProductDto[])
                .then(
                    through(fetched => store.setAll(store.all().map(u => fetched.find(uu => uu.id === u.id) ?? u)))
                )
        }
     
        ,

        save: (product: Product): Promise<Product> => {

            return toggleBusy(`${productType}.save`, t("common.feedback.save_changes"))

                .then(() => product.lifecycle.created ?

                    call.update(product)
                        .then(through(saved => store.setAll(store.all().map(c => c.id === product.id ? saved : c))))

                    :

                    call.add(product)
                        .then(through(saved => store.setAll([saved, ...store.all()])))

                )
                .then(through(() => notify(t('common.feedback.saved'))))

                .catch(e => showAndThrow(e, t("common.calls.save_one_error", { singular })))
                .finally(() => toggleBusy(`${productType}.save`))
        }
        ,


        remove: (id: string, onConfirm: () => void, challenge?: boolean) => {

            const prod = store.lookup(id)

            const content = prod ? <div>
                    <Paragraph style={{marginBottom: 10}}>
                        <Trans i18nKey={'common.consent.remove_one_msg_head'} values={{singular, name: l(prod.name)}} components={{ bold: <strong />}}/>
                    </Paragraph>
                    <Paragraph>{t('common.consent.remove_one_msg', { singular })}</Paragraph>
                </div>                 
            : t('common.consent.remove_one_msg', { singular })

            fb.askConsent({

                title: t('common.consent.remove_one_title', { singular }),
                content,
                okText: t('common.consent.remove_one_confirm', { singular }),

                okChallenge: challenge ? t('common.consent.remove_challenge', { singular }) : undefined,

                onOk: () => {


                    toggleBusy(`${productType}.removeOne`, t("common.feedback.save_changes"))

                        .then(_ => call.delete(id))
                        .then(_ => store.all().filter(u => id !== u.id))
                        .then(store.setAll)
                        .then(_ => notify(t('common.feedback.saved')))
                        .then(_ => onConfirm && onConfirm())

                        .catch(e => showAndThrow(e, t("common.calls.remove_one_error", { singular })))
                        .finally(() => toggleBusy(`${productType}.removeOne`))


                }
            })
            
        },

        removeAll: (list: string[], onConfirm?: () => void) =>

            fb.askConsent({


                title: t('common.consent.remove_many_title', { count: list.length, plural }),
                content: t('common.consent.remove_many_msg', { plural }),
                okText: t('common.consent.remove_many_confirm', { count: list.length, plural }),

                onOk: () => {


                    toggleBusy(`${productType}.removeAll`, t("common.feedback.save_changes"))

                        .then(_ => list.forEach(id => call.delete(id)))
                        .then(_ => store.all().filter(u => !list.includes(u.id)))
                        .then(store.setAll)
                        .then(_ => notify(t('common.feedback.saved')))
                        .then(_ => onConfirm && onConfirm())

                        .catch(e => showAndThrow(e, t("common.calls.remove_one_error", { singular })))
                        .finally(() => toggleBusy(`${productType}.removeAll`))


                }
            })


    }

    return self

}