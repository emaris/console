import { Placeholder } from '#app/components/Placeholder'
import { useCampaignUsage } from '#campaign/Usage'
import { AssetSection, assetSectionid, sectionAssetOf } from '#layout/components/assetsection'
import { Component, isContainer } from '#layout/components/model'
import { RequirementSection, requirementSectionId } from '#layout/components/requirementsection'
import { useRequirementClient } from '#requirement/client'
import { requirementType } from '#requirement/constants'
import { useRenderGuard } from 'apprise-frontend-core/utils/renderguard'
import * as React from "react"
import { useProductClient } from './client'
import { productType } from './constants'
import { Product, ProductDto } from './model'
import { useProductStore } from './store'


export type Props = {

    id: string,
    placeholder?: JSX.Element
    children: (product: Product) => React.ReactElement
}


export const ProductDetailLoader = (props: Props) => {

    const { id, placeholder=Placeholder.none,  children } = props


    const usage = useCampaignUsage()
    const store = useProductStore()
    const client = useProductClient()

    const current = store.lookup(id)

    const initialValue = () => client.isLoaded(current) ? current : undefined

    const [fetched, setFetched] = React.useState<Product | undefined>(initialValue())

    React.useEffect(() => {
        usage.fetchFor(productType,id) 

     // eslint-disable-next-line
    },[id])


    const {content} = useRenderGuard({
        when: !!fetched && id === fetched.id && client.isLoaded(current),
        render: ()=><ProductDepLoader product={fetched!} placeholder={placeholder}>{children(fetched!)}</ProductDepLoader>,
        orRun: () => client.fetchOne(id).then(setFetched),
        andRender: placeholder

    })

    return content

}

type ProductDepLoaderProps = React.PropsWithChildren<{
    product: ProductDto,
    placeholder?: JSX.Element
}>

export const ProductDepLoader = (props: ProductDepLoaderProps) => {

    const { product, placeholder, children } = props

    const prods = useProductClient()
    const reqs = useRequirementClient()

    const fetchAllDependencies = (fetched: Product) => {

        const flattencontainers = (c: Component): Component[] => isContainer(c) ? [c, ...c.children.flatMap(flattencontainers)] : []

        const referencedReqs = flattencontainers(fetched.properties.layout.components).filter(c => c.spec === requirementSectionId).flatMap(c => (c as RequirementSection).requirements)
        const referencedAssets = flattencontainers(fetched.properties.layout.components).filter(c => c.spec === assetSectionid).flatMap(c => (c as AssetSection).assets)

        const assetReq = [...referencedReqs, ...referencedAssets.filter(a=>a.type===requirementType)].map(sectionAssetOf).reduce( (acc, cur) => (acc.includes(cur) ? acc : [...acc, cur]), [] as string[])
        const assetProd = referencedAssets.filter(a=>a.type===productType).map(sectionAssetOf).reduce( (acc, cur) => (acc.includes(cur) ? acc : [...acc, cur]), [] as string[])

        // fetch those never fetched before

        // NOTE: (fallback to id for retro compatibility: parameter.id used to be `requirement id` before becoming a stable, independent id)
        return Promise.all([reqs.fetchManySilently(assetReq), prods.fetchManySilently(assetProd)]) //.then(r => r.flatMap(r=>r))
        

    }

    const {content} = useRenderGuard({
        render: children,
        orRun: () => fetchAllDependencies(product),
        andRender:placeholder

    })

    return content

}
