
import { useCampaignUsage } from "#campaign/Usage"
import { productPlural, productSingular, productType } from "#product/constants"
import { ProductLabel } from "#product/Label"
import { Product, useProductModel } from "#product/model"
import { useProductPermissions } from "#product/ProductPermissions"
import { buildinfo } from "#app/buildinfo"
import { Button } from "#app/components/Button"
import { useListState } from "#app/components/hooks"
import { Label } from "#app/components/Label"
import { Column, VirtualTable } from "#app/components/VirtualTable"
import { specialise } from "#app/iam/model"
import { icns } from "#app/icons"
import { useLocale } from "#app/model/hooks"
import { Page } from "#app/scaffold/Page"
import { Titlebar } from "#app/scaffold/PageHeader"
import { Sidebar } from "#app/scaffold/Sidebar"
import { Topbar } from "#app/scaffold/Topbar"
import { contextCategory } from '#app/system/constants'
import { useContextFilter } from '#app/system/filter'
import { useTagFilter, useTagHolderFilter } from '#app/tag/filter'
import { TagList } from "#app/tag/Label"
import { tagRefsIn } from "#app/tag/model"
import { tenantPlural } from "#app/tenant/constants"
import { userPlural } from '#app/user/constants'
import { useLogged } from '#app/user/store'
import { useBaseFilter } from '#app/utils/filter'
import * as React from "react"
import { useT } from '#app/intl/api'
import { useHistory, useLocation } from "react-router"
import { productActions } from './actions'
import { useProductStore } from './store'
import { useTagStore } from '#app/tag/store'
import { useProductClient } from './client'


export const productGroup = productType

export const ProductList = () => {

    const { l } = useLocale()
    const t = useT()
    const history = useHistory()
    const { pathname } = useLocation()

    const tags = useTagStore()
    const logged = useLogged()


    const model = useProductModel()
    const store = useProductStore()
    const client = useProductClient()

    const [singular, plural] = [t(productSingular), t(productPlural)]

    const [Permissions, showPermissions] = useProductPermissions()

    const usage = useCampaignUsage()

    const liststate = useListState<Product>();

    // -------------- authz privileges
    const canManage = React.useCallback((p: Product) => logged.can(specialise(productActions.manage, p.id)), [logged])


    // -------------- actions
    const onRemove = ({ id }: Product) => client.remove(
        id,
        () => {

            liststate.setSelected(liststate.selected.filter(l => l.id !== id))
        }
        , usage.isInUse(id)
    )

    const onClone = (product: Product) => {

        client.fetchOne(product.id).then(loaded => {

            store.setNext({ model: model.clone(loaded) })
            history.push(`${pathname}/new`)
        })

    }

    const onBranch = (product: Product) => {

        client.fetchOne(product.id).then(loaded => {

            store.setNext({ model: model.branch(loaded) })
            history.push(`${pathname}/new`)
        })
    }

    const onRemoveAll = () => client.removeAll(liststate.selected.map(l => l.id), () => liststate.setSelected([]))

    const onAddProduct = () => Promise.resolve(store.resetNext())


    // -------------- buttons

    const addBtn = <Button
        type="primary"
        icn={icns.add}
        enabled={logged.can(productActions.edit)}
        onClick={onAddProduct}
        linkTo={`${pathname}/new`} >
        {t("common.buttons.add_one", { singular })}
    </Button>

    const openBtn = (p: Product) => <Button
        key={1}
        icn={icns.open}
        linkTo={store.routeTo(p)}>
        {t("common.buttons.open")}
    </Button>

    const cloneBtn = (product: Product) =>
        <Button
            key={3}
            icn={icns.clone}
            onClick={() => onClone(product)}>
            {t("common.buttons.clone")}
        </Button>

    const branchBtn = (product: Product) =>
        <Button
            key={4}
            icn={icns.branch}
            onClick={() => onBranch(product)} >
            {t("campaign.buttons.branch")}
        </Button>

    const rightsBtn = <Button
        enabled={liststate.selected.length > 0}
        icn={icns.permissions}
        style={{ marginTop: 30 }}
        onClick={showPermissions}>
        {t("common.buttons.permissions")}
    </Button>

    const removeBtn = (product: Product) =>
        <Button
            key={2}
            icn={icns.remove}
            enabled={logged.can(productActions.edit) && product.lifecycle.state === 'inactive'}
            disabled={product.predefined}
            onClick={() => onRemove(product)}>
            {t("common.buttons.remove")}
        </Button>


    const removeAllBtn = <Button
        type="danger"
        disabled={liststate.selected.length < 1}
        enabled={logged.can(productActions.edit)}
        onClick={onRemoveAll}>
        <span style={{ fontVariantCaps: "all-small-caps" }}>(DEV)</span> {t("common.labels.remove_all", { count: liststate.selected.length })}
    </Button>




    // filters

    const unfilteredData = store.all()

    // eslint-disable-next-line
    const sortedData = React.useMemo(() => [...unfilteredData].sort(model.comparator), [unfilteredData])

    const { ContextSelector, contextFilteredData } = useContextFilter({
        data: sortedData,
        group: productGroup
    })

    const { BaseFilter, baseFilteredData } = useBaseFilter({
        data: contextFilteredData,
        readonlyUnless: canManage,
        group: productGroup
    })

    const { TagFilter, tagFilteredData } = useTagFilter({
        tagged: unfilteredData,
        filtered: baseFilteredData,
        excludeCategories: [contextCategory],
        key: `tags`,
        group: productGroup
    })

    const { TagFilter: AudienceFilter, tagFilteredData: audienceFilteredData } = useTagHolderFilter({
        tagged: unfilteredData,
        filtered: tagFilteredData,
        tagsOf: t => t.audience.terms.flatMap(t => t.tags),
        placeholder: t("common.fields.audience.name"),
        key: `audience`,
        group: productGroup
    })

    const { TagFilter: UserProfileFilter, tagFilteredData: userProfileFilteredData } = useTagHolderFilter({
        filtered: audienceFilteredData,
        tagged: unfilteredData,
        tagsOf: t => t.userProfile?.terms.flatMap(t => t.tags),
        placeholder: t("common.fields.user_profile.name"),
        key: `userprofile`,
        group: productGroup
    })


    const data = userProfileFilteredData

    return <Page>

        <Sidebar>
            {addBtn}
            {rightsBtn}

            {buildinfo.development &&
                <>
                    <br />
                    {removeAllBtn}
                </>
            }

        </Sidebar>

        <Topbar>

            <Titlebar title={plural} />

            {addBtn}
            {rightsBtn}

        </Topbar>

        <VirtualTable data={data} total={sortedData.length} state={liststate}

            filterGroup={productGroup} filterBy={model.stringify} filterPlaceholder={t("common.components.table.filter_placeholder", { plural: plural.toLowerCase() })}
            filters={[UserProfileFilter, AudienceFilter, TagFilter, BaseFilter, ContextSelector]}

            actions={p => [openBtn(p), removeBtn(p), cloneBtn(p), branchBtn(p), ContextSelector]}
            onDoubleClick={t => history.push(store.routeTo(t))} >

            <Column flexGrow={2} title={t("common.fields.name_multi.name")} dataKey="name"
                dataGetter={(t: Product) => l(t.name)}
                cellRenderer={t => <ProductLabel tipTitle product={t.rowData} readonly={!canManage(t.rowData)} />} />

            <Column sortable={false} title={t("common.fields.audience.name")} dataKey="audience" dataGetter={(p: Product) => p.audience?.terms?.length > 0 ? <TagList taglist={tagRefsIn(p.audience)} /> : <Label title={t(tenantPlural)} />} />


            <Column sortable={false} title={t("common.fields.user_profile.name")} dataKey="userprofile" dataGetter={(r: Product) => r.userProfile?.terms?.length > 0 ? <TagList taglist={tagRefsIn(r.userProfile)} /> : <Label title={t(userPlural)} />} />

            {tags.allTagsOf(productType).length > 0 &&
                <Column sortable={false} flexGrow={3} title={t("common.fields.tags.name")} dataKey="tags" dataGetter={(p: Product) => <TagList truncateEllipsisLink={store.routeTo(p)} taglist={p.tags} />} />
            }

        </VirtualTable>

        <Permissions resourceRange={liststate.selected} />

    </Page>

}

