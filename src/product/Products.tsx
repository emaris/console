
import { productRoute } from "#product/constants"
import { ProductLoader } from "#product/Loader"
import * as React from "react"
import { Route, Switch } from "react-router"
import { NewProductDetail, ProductDetail } from "./Detail"
import { ProductList } from "./List"
import { Placeholder } from "#app/components/Placeholder"


export const Products =  () =>
     
          <ProductLoader placeholder={Placeholder.page}>
                <Switch>
                    <Route exact path={productRoute} component={ProductList} />
                    <Route path={`${productRoute}/new`} component={NewProductDetail} />
                    <Route path={`${productRoute}/:id`} component={ProductDetail} />
                </Switch>
            </ProductLoader>
