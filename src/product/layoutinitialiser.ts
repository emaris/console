
import { useRavSectionComponent } from '#integration/rav/client/section'
import { useAnchorComponent } from '#layout/components/anchor'
import { useAssetSectionComponent } from '#layout/components/assetsection'
import { useConditionalSection } from '#layout/components/conditionalsection'
import { useConditionContext } from '#layout/components/conditioncontext'
import { useDateBoxComponent } from '#layout/components/datebox'
import { useDocumentComponent } from '#layout/components/document'
import { useEmbeddingComponent } from '#layout/components/embedding'
import { useFileDropComponent } from '#layout/components/filedrop'
import { useImageComponent } from '#layout/components/image'
import { useInputBoxComponent } from '#layout/components/inputbox'
import { useMultiChoiceComponent } from '#layout/components/multichoice'
import { useNumberBoxComponent } from '#layout/components/numberbox'
import { useParagraphComponent } from '#layout/components/paragraph'
import { useCellComponent, useRowComponent } from '#layout/components/row'
import { useRowSectionComponent } from '#layout/components/rowsection'
import { useSectionComponent } from '#layout/components/section'
import { useSeparatorComponent } from '#layout/components/separator'
import { useSubmissionFileDropComponent } from '#layout/components/submissionfiledrop'
import { useTitleComponent } from '#layout/components/title'
import { useAssetParameter } from '#layout/parameters/asset'
import { useCampaignParameter } from '#layout/parameters/campaign'
import { useDateYearParam } from '#layout/parameters/dateyear'
import { useNumberParameter } from '#layout/parameters/number'
import { usePartyParameter } from '#layout/parameters/party'
import { useProductParameter } from '#layout/parameters/product'
import { useReferenceParameter } from '#layout/parameters/reference'
import { useRequirementParameter } from '#layout/parameters/requirement'
import { useTenantParameter } from '#layout/parameters/tenant'
import { useTextParameter } from '#layout/parameters/text'
import { useLayoutRegistry } from '#layout/registry'


export const useProductLayoutInitialiser = () => {

    const registry = useLayoutRegistry()

    const numberparam = useNumberParameter()
    const textparam = useTextParameter()
    const dateyearparam = useDateYearParam()

    const tenantparam = useTenantParameter()
    const partyparam = usePartyParameter()
    const campaignparam = useCampaignParameter()
    const referenceparam = useReferenceParameter()
    const requirementparam = useRequirementParameter()
    const productparam = useProductParameter()
    const assetparam = useAssetParameter()

    const paragraph = useParagraphComponent()
    const title = useTitleComponent()
    const inputbox = useInputBoxComponent()
    const numberbox = useNumberBoxComponent()
    const datebox = useDateBoxComponent()
    const multichoice = useMultiChoiceComponent()

    const conditioncontext = useConditionContext()
    const conditionalsection = useConditionalSection()
    

    const rowsection = useRowSectionComponent()
    const row = useRowComponent()
    const cell = useCellComponent()

    const assetsection = useAssetSectionComponent()

    const filedrop =useFileDropComponent()
    const submissionfiledrop = useSubmissionFileDropComponent()
    const image = useImageComponent()
    const document = useDocumentComponent()
    const separator=  useSeparatorComponent()

    const section = useSectionComponent()
 
    const ravSection = useRavSectionComponent()

    const anchor = useAnchorComponent()

    const embedding = useEmbeddingComponent()


    return () => {

        registry.addComponents([
            
            section,
            
            title,
            paragraph,
            
            rowsection,
            row,
            separator,
            image,
            document,
           
            inputbox,
            numberbox,
            datebox,
            multichoice,
            filedrop,
           
            assetsection,
            embedding,

            conditionalsection,
            conditioncontext,
            
            anchor,

            {...ravSection, active: false },
          
            // these we do not show.
            cell,

            // these is used in requirements only but may be referenced from reports.
            {...submissionfiledrop, active:false},

 
            

        ])
        .addParameters([
            textparam,
            numberparam,
            dateyearparam,
            tenantparam,
            partyparam,
            campaignparam,
            referenceparam,
            requirementparam,
            productparam,
            assetparam,
        ])

    }


}