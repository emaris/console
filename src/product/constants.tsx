import { Icon } from "antd";
import * as React from "react";
import { IoMdToday } from "react-icons/io";



export const productSingular = "product.module.name_singular"
export const productPlural = "product.module.name_plural"

export const productType="product"
export const productIcon=<Icon className="io" component={IoMdToday} />
export const productRoute="/product" 


export const prodImportanceCategory = "TGC-product-importancescale"