
import { Placeholder } from "#app/components/Placeholder"
import { RequirementLoader } from "#requirement/Loader"
import { useRenderGuard } from 'apprise-frontend-core/utils/renderguard'
import * as React from "react"
import { useProductClient } from './client'

type Props = React.PropsWithChildren<{

  placeholder?: React.ReactNode

}>


export const ProductLoader = (props: Props) => {

  const client = useProductClient()

  const {content} = useRenderGuard({
    when: client.areReady(),
    render: props.children,
    orRun: client.fetchAll,
    andRender: props.placeholder

  })

  return <RequirementLoader placeholder={Placeholder.none}>
    {content}
  </RequirementLoader>

}