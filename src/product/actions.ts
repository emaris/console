
import { Action, any } from "#app/iam/model"
import { productIcon, productType } from "./constants"

const baseAction = { icon:productIcon, type: productType, resource :any, actionType: 'admin' } as Action

export const productActions = {  
    
    manage: {...baseAction, labels:["manage"], shortName:"product.actions.manage.short",name:"product.actions.manage.name", description: "product.actions.manage.desc"}
    ,
    edit:  {...baseAction, labels:["manage","edit"], shortName:"product.actions.edit.short",name:"product.actions.edit.name", description: "product.actions.edit.desc"}
    ,
    assess:  {...baseAction, labels:["manage","assess"], shortName:"product.actions.assess.short",name:"product.actions.assess.name", description: "product.actions.assess.desc"}
    
}