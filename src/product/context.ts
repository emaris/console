import { fallbackStateOver, State } from 'apprise-frontend-core/state/api'
import { createContext } from 'react'
import { Product } from './model'

export type ProductNext = {
    model: Product
}

export type ProductState = {

    products: {
        all: Product[]
        map: Record<string, Product>
        next?: ProductNext
    }
}

export const initialProducts: ProductState = {

    products: {
        all: undefined!,
        map: {}
    }

}


export const ProductContext = createContext<State<ProductState>>(fallbackStateOver(initialProducts))