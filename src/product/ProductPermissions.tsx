
import { any } from '#app/iam/model'
import { ChangesProps } from '#app/iam/permission'
import { usePermissionDrawer } from '#app/iam/PermissionForm'
import { ResourceProps, StateProps, SubjectProps } from '#app/iam/PermissionTable'
import { useLocale } from '#app/model/hooks'
import { noTenant } from '#app/tenant/constants'
import { User } from '#app/user/model'
import { UserPermissions } from '#app/user/UserPermissionTable'
import * as React from 'react'
import { useT } from '#app/intl/api'
import { productActions } from './actions'
import { productPlural, productSingular, productType } from './constants'
import { ProductLabel } from './Label'
import { Product } from './model'
import { useProductStore } from './store'


type PermissionsProps = ChangesProps & Partial<ResourceProps<Product>> & Partial<SubjectProps<User>> & Partial<StateProps<User,Product>> & {
    edited? : User
}


//  wraps UserPermissions for Requirements, so as to inject appropriate defaults.

export const ProductPermissions =  (props:PermissionsProps) => {

    const t = useT()
    const {l} = useLocale()
    const products = useProductStore()

    return  <UserPermissions {...props}
                id="product-permissions" 
                subjectRange={props.subjectRange?.filter(s=>s.tenant===noTenant)}
                resourceSingular={props.resourceSingular || t(productSingular)}
                resourcePlural={props.resourcePlural || t(productPlural)}
                renderResource={props.renderResource || ((t:Product) => <ProductLabel product={t} /> )}
                resourceText={t=>l(t.name)}
                resourceId={props.resourceId || ((r:Product) => r.id) }
                renderResourceOption={props.renderResourceOption || ((r:Product) => <ProductLabel noLink product={r} />)} 
                resourceRange={props.resourceRange || products.all() } 
                resourceType={productType}

                actions={Object.values(productActions)} />
                
}


export const useProductPermissions = ()=>usePermissionDrawer (ProductPermissions, { types:[productType, any] }  )