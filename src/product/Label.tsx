

import { Tooltip } from 'antd'
import { Label, LabelProps, sameLabel, UnknownLabel } from "#app/components/Label"
import { icns } from '#app/icons'
import { useLocale } from "#app/model/hooks"
import * as React from "react"
import { useT } from '#app/intl/api'
import { Link, useLocation } from "react-router-dom"
import { productIcon, productRoute, productType } from "./constants"
import { Product } from "./model"
import { useProductStore } from './store'



type Props = LabelProps & {

    product: string | Product | undefined
    lineage?: boolean
    showTitle?: boolean
    tipTitle?: boolean
    noVersion?: boolean
}

const innerrouteRegexp = new RegExp(`${productRoute}\\/(.+?)(\\?|\\/|$)`)

export const ProductLabel = (props: Props) => {

    const store = useProductStore()
    const location = useLocation()

    const { product } = props

    const resolved = typeof product === 'string' ? store.lookup(product) : product

    if (!resolved)
        return <UnknownLabel tip={() => product as string} />

    return <PureProductLabel {...props} product={resolved} products={store} location={location} />

}

type PureProps = Omit<Props,'product'> & {

    product: Product
    products: ReturnType<typeof useProductStore>,
    location: ReturnType<typeof useLocation>
}



export const PureProductLabel = React.memo((props: PureProps) => {

    const { l } = useLocale()
    const t = useT()

    const { location, products, product, linkTo, lineage, linkTarget, icon, inactive, showTitle=false, noVersion = false, tipTitle=false, decorations = [], tipPlacement="topLeft", ...rest } = props

    const { pathname, search } = location

    const { safeLookup, routeTo } = products

    const isProtected = product ? product.predefined : false

    const versioned = product.properties.version 

    const version = (!noVersion && versioned) ? `(${product.properties.version})` : ''

    const lineagelist = product?.lineage?.map(r=>l(products.lookup(r)!.name)).join(', ')
    const lineageContent = product?.lineage?.[0] && lineage ?

            [<Tooltip title={t("requirement.misc.supersedes", { lineage: lineagelist })}>{
                product?.lineage?.length === 1 ?
                    <Link to={ routeTo(safeLookup(product!.lineage?.[0]))}>{icns.branch}</Link>
                    : icns.branch}
            </Tooltip>]

            : []

    // like routeTo() if the current route is not a campaign route, otherwise replaces the current campaign with the target one.
    // undefined, if the two campaigns coincide.
    const innerRouteTo = (target: Product) => {

        const regexpmatch = pathname.match(innerrouteRegexp)?.[1]
        const linkname = target?.id

        return regexpmatch ?
                    regexpmatch === linkname ? undefined : `${pathname.replace(regexpmatch, linkname!)}${search}`
                : products.routeTo(target)


    }

    const route = linkTo ?? innerRouteTo(product) 

    const getVersionedInfo = (lbl: string):string => {
        if (product.properties.version) {
            return `(${t("common.fields.version.name")} ${product.properties.version}) ${lbl ?? ''}`.trim()
        }

        return lbl
    }

    const noTip = props.noTip ?? (props.tip ? false : showTitle ? l(product.description) ? false : true : tipTitle ? l(product.description) ? false : versioned ? false : true : true)
    const tip = props.tip ?? ( () => showTitle ? getVersionedInfo(l(product.name)) : (tipTitle || versioned) ? getVersionedInfo(l(product.description)) : undefined)

    const decorationContent = [version, ...lineageContent, ...decorations]

    const inactiveState = product.lifecycle.state === 'inactive'

    const label = showTitle ? l(product.description) ? l(product.description) : l(product.name) : l(product.name)

    return <Label {...rest} title={label} inactive={inactive ?? inactiveState} tip={tip} noTip={noTip} tipPlacement={tipPlacement} isProtected={isProtected} linkTarget={linkTarget ?? productType} decorations={decorationContent} icon={icon ?? productIcon} linkTo={route} />

},($, $$) => $.product === $$.product && sameLabel($, $$))