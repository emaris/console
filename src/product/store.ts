
import { indexMap } from "#app/utils/common";
import { useContext } from 'react';
import { productRoute } from "./constants";
import { ProductContext, ProductNext } from './context';
import { Product, useNewProduct, useNoProduct } from "./model";



export const useProductStore = () => {

    const state = useContext(ProductContext)


    const newproduct = useNewProduct()
    const noProduct = useNoProduct()



    const self = {

        all: () => state.get().products.all

        ,

        lookup: (id: string | undefined) => id ? state.get().products.map?.[id] : undefined

        ,

        safeLookup: (id: string | undefined) => self.lookup(id) ?? noProduct.get()

        ,

        setAll: (products: Product[], props?: { noOverwrite: boolean }) => state.set(s => {

            if (s.products.all && props?.noOverwrite)
                return

            s.products.all = products;
            s.products.map = indexMap(products).by(r => r.id)
        }

        )

        ,


        routeTo: (p: Product) => p ? `${productRoute}/${p.id}` : productRoute

        ,

        next: () => {
            const next = state.get().products.next || { model: newproduct.get() }
            return next;
        },

        resetNext: () => {
            self.setNext(undefined)
        }
        ,

        setNext: (t: ProductNext | Product | undefined): ProductNext | undefined => {
            const model = t ? t.hasOwnProperty('model') ? t as ProductNext : { model: t as Product } : undefined

            state.set(s => s.products.next = model)
            return model

        }


    }

    return self

}