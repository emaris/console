


import { useCapProperties } from '#app/components/CapList';
import { useConfig } from '#app/config/state';
import { useT } from '#app/intl/api';
import { Multilang } from '#app/model/multilang';
import { contextCategory } from "#app/system/constants";
import { useTagStore } from '#app/tag/store';
import { tenantPlural } from "#app/tenant/constants";
import { userPlural } from '#app/user/constants';
import { useLogged } from '#app/user/store';
import { check, checkIt, noDupsInArray, notdefined, requireLanguages, reservedKeyword, stringLength, uniqueLanguages, withReport } from "#app/utils/validation";
import { productPlural, productSingular, productType } from "#product/constants";
import { Product } from "#product/model";
import { useProductStore } from './store';


export type ProductValidation = ReturnType<typeof useProductValidation>

export const useProductValidation = () => {

    const t = useT()

    const logged = useLogged()
    const config = useConfig()
    const tags = useTagStore()
    const store = useProductStore()
    const caps = useCapProperties()

    return {

        validateProduct: (edited: Product) => {

            const { validateCategories } = tags

            const products = store.all();
            const requiredLangs = config.get().intl.required || [];

            const singular = t(productSingular).toLowerCase()
            const plural = t(productPlural).toLowerCase()
            const partyAudiencePlural = t(tenantPlural).toLowerCase()
            const userProfilePlural = t(userPlural).toLowerCase()

            const mapName = (p: Product) => {
                const version = (p.properties.version ?? '').trim()
                const name = requiredLangs.reduce((acc, cur) => ({...acc, [cur] : `${(p.name?.[cur] ?? '')} ${version}`.trim()}), {})
                return name as Multilang
            }

            const productNames = products.filter(t => t.id !== edited.id).map(mapName)

            return withReport({

                active: checkIt().nowOr(t("common.fields.active.msg"), t("common.fields.active.help", { plural })),

                name: check(mapName(edited)).with(notdefined(t))
                    .with(requireLanguages(requiredLangs, t))
                    .with(stringLength(t))
                    .with(reservedKeyword(t))
                    .with(uniqueLanguages(productNames, t)).nowOr(
                        t("common.fields.name_multi.msg"),
                        t("common.fields.name_multi.help", { plural, requiredLangs })),

                tags: checkIt().nowOr(
                    t("common.fields.tags.msg"),
                    t("common.fields.tags.help", { singular, plural })
                ),

                title: checkIt().nowOr(
                    t("common.fields.title.msg", { singular }),
                    t("common.fields.title.help", { plural, requiredLangs }))

                ,

                version: check(mapName(edited)).with(uniqueLanguages(productNames, t)).nowOr(
                    t("common.fields.version.msg"),
                    t("common.fields.version.help", { plural: t("common.version.plural") }))

                ,

                // title: check(edited.description).with(notdefined(t))
                // .with(requireLanguages(requiredLangs,t))
                // .with(uniqueLanguages(products.filter(t=>t.id!==edited.id).map(t=>t.description ?? {}),t)).nowOr(
                //         t("common.fields.title.msg",{singular}),
                //         t("common.fields.title.help",{plural,requiredLangs}))

                // ,


                audience: checkIt().nowOr(
                    t("common.fields.audience.msg"),
                    t("common.fields.audience.help", { singular, plural: partyAudiencePlural })
                ),

                audienceList: checkIt().nowOr(
                    t("common.fields.audience_list.msg"),
                    t("common.fields.audience_list.help", { singular, plural: partyAudiencePlural })
                )

                ,

                userProfile: checkIt().nowOr(
                    t("common.fields.user_profile.msg"),
                    t("common.fields.user_profile.help", { singular, plural: userProfilePlural })
                )

                ,

                tagCategories: checkIt().nowOr(
                    t("common.fields.stage_scale.msg"),
                    t("common.fields.stage_scale.help")
                )

                ,

                lineage: checkIt().nowOr(
                    t("common.fields.lineage.msg", { singular }),
                    t("product.fields.lineage.help"),
                )

                ,

                note: check(edited.properties.note ? edited.properties.note[logged.tenant] : '')
                    .with(reservedKeyword(t)).nowOr(t("common.fields.note.msg"), t("common.fields.note.help", { singular }))

                ,

                editable: checkIt().nowOr(
                    t("common.fields.editable.msg"), t("common.fields.editable.help", { plural, parties: partyAudiencePlural })
                )

                ,

                versionable: checkIt().nowOr(
                    t("common.fields.versionable.msg"), t("common.fields.versionable.help", { plural, parties: partyAudiencePlural })
                )

                ,

                assessed: checkIt().nowOr(
                    t("common.fields.assessed.msg"), t("common.fields.assessed.help", { plural })
                )

                ,

                tagMap: check((edited.properties.submissionTagMap ?? []).map(tagMap => tagMap.category))
                    .with(noDupsInArray(t)).nowOr(undefined, t("product.fields.tagmap.help", { asset: t(productSingular).toLowerCase() }))

                ,

                ...validateCategories(edited.tags).include(contextCategory)

                ,

                ...validateCategories(edited.tags).for(productType)

                ,

                ...validateCategories(edited.properties.submissionTagMap?.flatMap(tagmap => tagmap.tags)).include(...(edited.properties.submissionTagMap?.map(tagmap => tagmap.category) ?? []))

                ,

                ...caps.validationFor(edited.properties.cap)
            })

        }

    }
}


