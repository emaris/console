import { FormState } from "#app/form/hooks"
import { useT } from '#app/intl/api'
import { useLocale } from "#app/model/hooks"
import { ContentHeader } from "#app/scaffold/ContentHeader"
import { campaignIcon, campaignPlural, campaignSingular } from "#campaign/constants"
import { useCampaignUsage } from "#campaign/UsageList"
import { productType } from "#product/constants"
import { Product, useProductModel } from "#product/model"
import { requirementIcon, requirementPlural, requirementSingular } from "#requirement/constants"
import * as React from "react"
import { ProductRequirementList } from "./RequirementList"

export type Props = FormState<Product>

export const ProductUsage = (props: Props) => {

        const t = useT()
        const { l } = useLocale()

        const model = useProductModel()

        const { edited } = props

        const [campaignUsageList, campaignList] = useCampaignUsage({ type: productType, target: edited.id, height: 250 })

        const name = l(edited.name)
        const reqcount = model.requirementsFor(edited).length  //edited.requirements?.length ?? 0
        const requirements = t(reqcount !== 1 ? requirementPlural : requirementSingular)
        const campcount = campaignList?.length ?? -1
        const campaigns = t(campcount !== 1 ? campaignPlural : campaignSingular)

        return <>

                <ContentHeader title={<span>{requirementIcon}&nbsp;&nbsp; <span>{reqcount}</span> {requirements}</span>}
                        subtitle={t("product.feedback.based_on", { name, reqcount, requirements: requirements.toLowerCase() })} />

                <ProductRequirementList height={250} product={edited} />

                <ContentHeader title={<span>{campaignIcon}&nbsp;&nbsp; <span>{campcount === -1 ? '...' : campcount}</span> {campaigns}</span>}
                        subtitle={t("campaign.feedback.used_in", { name, campcount: campcount === -1 ? '...' : campcount })} />

                {campaignUsageList}


        </>
}