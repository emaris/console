
import { useCalls } from "#app/call/call"
import { domainService } from '#constants'
import { Product } from "./model"

export const productApi = "/product"


export const useProductCalls = () => {

    
    const {at} = useCalls()

    return {


        fetchAll: () => at(`${productApi}/raw`,domainService).get<Product[]>()
        
        , 

        fetchOne: (id:String) =>  at(`${productApi}/${id}`,domainService).get<Product>()
        
        ,
        
        add: (product:Product) =>  at(productApi,domainService).post<Product>(product)
        
        ,
    
        update: (product:Product) => at(`${productApi}/${product.id}`,domainService).put<Product>(product)
        
        , 

        delete: (id:string) => at(`${productApi}/${id}`,domainService).delete()
  

    }

}