import { useAskReloadOnPushChange, useAskReloadOnPushRemove } from '#app/push/constants';
import { PushEventSlot } from '#app/push/module';
import { tenantRoute } from '#app/tenant/constants';
import { useCampaignStore } from '#campaign/store';
import { dashboardRoute } from '#dashboard/constants';
import shortid from 'shortid';
import { productRoute, productSingular, productType } from './constants';
import { Product } from './model';
import { useProductStore } from './store';

export type ProductChangeEvent = {

    product: Product
    type: 'add' | 'remove' | 'change'

}


export const usePushProductSlot = (): PushEventSlot<ProductChangeEvent> => {

    const askReloadOnPushChange = useAskReloadOnPushChange()
    const askReloadOnPushRemove = useAskReloadOnPushRemove()

    const products = useProductStore()
    const campaisgnstore = useCampaignStore()
    
    return {

        onSubcribe: () => {

            console.log("subscribing for product changes...")

            return [{

                topics: [productType]

                ,

                onEvent: (event: ProductChangeEvent, history, location) => {

                    const { product, type } = event


                    console.log(`received ${type} event for product ${product.name.en}...`, { event })



                    const currentUrl = `${location.pathname}${location.search}`

                    const detailOrDashBordOrTrailOnScreen = currentUrl.includes(product.id)
                    const trailOnScreen = detailOrDashBordOrTrailOnScreen && currentUrl.includes(tenantRoute)
                    const detailOnScreen = detailOrDashBordOrTrailOnScreen && !currentUrl.includes(dashboardRoute)

                    let campaignArchived = false

                    if (trailOnScreen) {
                        const campaignId = location.pathname.split("/")[2]
                        const campaign = campaisgnstore.lookup(campaignId)
                        campaignArchived = campaignArchived || campaign?.lifecycle.state === 'archived'
                    }

                    switch (type) {

                        case 'add': {

                            products.setAll([...products.all(), product])

                            break
                        }

                        case 'change': {

                            const change = () => products.setAll(products.all().map(c => c.id === product.id ? product : c))
                            const changeAndRefresh = () => { change(); history.push(currentUrl, shortid()) } // uses location state to send down a signal for remount.

                            if (detailOnScreen || (trailOnScreen && !campaignArchived))
                                askReloadOnPushChange({ singular: productSingular, onOk: changeAndRefresh })

                            else change()

                            break
                        }

                        case 'remove': {

                            // when designing, we go back to list. in dshboard we go back to summary current campaign.
                            const fallbackRoute = detailOnScreen ? productRoute : dashboardRoute

                            const remove = () => products.setAll(products.all().filter(r => r.id !== product.id))
                            const leaveAndRemove = () => { history.push(fallbackRoute); remove() }

                            if (detailOrDashBordOrTrailOnScreen && !campaignArchived)
                                askReloadOnPushRemove({ singular: productSingular, onOk: leaveAndRemove })

                            else remove()

                            break
                        }
                    }

                }

            },


            ]
        }
    }
}