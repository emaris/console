
import { CapProperties, useCapProperties } from '#app/components/CapList'
import { Lifecycle, Lifecycled, newLifecycle } from "#app/model/lifecycle"
import { MultilangDto, useCompareMultiLang, useL } from "#app/model/multilang"
import { useSettings } from '#app/settings/store'
import { Document } from "#app/stream/model"
import { systemType } from "#app/system/constants"
import { SystemAppSettings } from "#app/system/model"
import { newTagged, Tagged, TagMap } from "#app/tag/model"
import { TenantAudience } from "#app/tenant/AudienceList"
import { deepclone, shortid } from "#app/utils/common"
import { AssetSection, NamedAsset } from '#layout/components/assetsection'
import { Component, isContainer } from '#layout/components/model'
import { Layout, newLayout } from "#layout/model"
import { predefinedProductParameterId, usePredefinedParameters, usePredefinedProductParameters } from "#layout/parameters/constants"
import { requirementType } from "#requirement/constants"
import { productType } from './constants'
import { useProductStore } from './store'


export type ProductDto = Tagged & Lifecycled<ProductState> & {

    id: string

    predefined?: boolean

    name: MultilangDto
    description: MultilangDto
    lifecycle: Lifecycle<ProductState>
    audience: { terms: any[] },
    audienceList?: TenantAudience,
    userProfile: { terms: any[] },

    lineage?: string[]

    documents?: Record<string, Document[]>
    properties: ProductProperties

}


export type ProductProperties = {

    layout: Layout
     cap:CapProperties
    dependencies: NamedAsset[]
    note?: Record<string, string>
    versionable: boolean | undefined
    editable: boolean | undefined
    assessed: boolean | undefined
    submissionTagMap?: TagMap[]
    version: string | undefined

} & { [key: string]: any }

export type ProductState = "active" | "inactive"

export type Product = ProductDto



export const useNewProduct = () => {

    const settings = useSettings().get<SystemAppSettings>()[systemType] ?? {}

    const predefinedParams = usePredefinedParameters()
    const productPredefinedParams = usePredefinedProductParameters()

    const cap  = useCapProperties()

    return {
        get: () => {


            const product = {


                ...newTagged,

                id: undefined!,
                name: { en: undefined },
                description: { en: undefined },
                lifecycle: newLifecycle('inactive'),
                audience: { terms: [] },
                userProfile: { terms: [] },
                audienceList: undefined!,
                tags: settings.defaultProductImportance ? ["TG-system-defaultcontext", ...settings.defaultProductImportance] : ["TG-system-defaultcontext"], //Default context set


                properties: {
                    layout: { ...newLayout() },
                    cap:cap.defaultsFor(productType),
                    dependencies: [],
                    versionable: true,
                    editable: true,
                    assessed: true,
                    version: undefined
                },

            } as Product

            product.properties.layout.parameters = [...predefinedParams.get(), ...productPredefinedParams.get()]

            return product

        }
    }
}

export const completeOnAdd = (product: Product, id?: string) => {

    const newreq = { ...product, id: id ?? newProductId() }

    const hostparam = newreq.properties.layout.parameters.find(p => p.id === predefinedProductParameterId)

    if (hostparam)
        hostparam.value = { id: newreq.id }

    return newreq

}

export const useNoProduct = () => {

    const noProduct = useNewProduct()

    return {
        get: (): Product => ({ ...noProduct.get(), id: 'unknown' })
    }
}

export const newProductId = () => `P-${shortid()}`


export const useProductModel = () => {


    const l = useL()
    const compare = useCompareMultiLang()

    const store = useProductStore()

    const self = {

        stringify: (p: Product | undefined) => p ? `${l(p.name)} ${l(p.description) ?? ''}` : ''

        ,

        stringifyRef: (r: string | undefined) => self.stringify(store.safeLookup(r))

        ,

        nameOf: (p: Product | undefined) => p ? `${l(p.name)}` : ''

        ,

        nameOfRef: (r: string | undefined) => self.nameOf(store.safeLookup(r))


        ,

        clone: (p: Product): Product => ({ ...deepclone(p), id: undefined!, predefined: false, lifecycle: newLifecycle(`inactive`)/* , documents: {} */ })

        ,

        branch: (p: Product): Product => ({ ...deepclone(p), id: undefined!, predefined: false, lifecycle: newLifecycle(`inactive`), lineage: [p.id]/* , documents: {} */ })

        ,

        comparator: (o1: Product, o2: Product) => compare(o1.name, o2.name)

        ,

        dependenciesOf: (product: Product) => {

            const flatten = (c: Component): Component[] => isContainer(c) ? [c, ...c.children.flatMap(flatten)] : [c]

            return flatten(product.properties.layout?.components ?? []).flatMap(c => (c as AssetSection)?.assets ?? [])
        }

        ,

        requirementsFor: (p: Product) => p.properties.layout.parameters.filter(param => param.spec === requirementType && param.value !== undefined).map(r => r.value.id)

    }

    return self


}
