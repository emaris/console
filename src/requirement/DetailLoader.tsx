import { Placeholder } from '#app/components/Placeholder'
import { useRenderGuard } from 'apprise-frontend-core/utils/renderguard'
import * as React from "react"
import { useRequirementClient } from './client'
import { requirementType } from './constants'
import { Requirement } from './model'
import { useRequirementStore } from './store'
import { useCampaignUsage } from '#campaign/Usage'


export type Props = {

    id: string, 
    placeholder?: JSX.Element
    children: (loaded:Requirement)=>React.ReactElement
}


export const RequirementDetailLoader = (props: Props) => {

    const { id, placeholder=Placeholder.none, children } = props

    const usage = useCampaignUsage()
    const store = useRequirementStore()
    const client = useRequirementClient()

    const current = store.lookup(id)

    const initialValue = () => client.isLoaded(current) ? current : undefined

    React.useEffect(() => {
        
        usage.fetchFor(requirementType,id) 

     // eslint-disable-next-line
    },[id])


    const [fetched, setFetched] = React.useState<Requirement | undefined>(initialValue())

    const {content} = useRenderGuard({
        when: !!fetched && id===fetched.id && client.isLoaded(current),
        render: ()=>children(fetched!),
        orRun: () => client.fetchOne(id).then(setFetched),
        andRender: placeholder

    })

    return content

}
