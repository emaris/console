
import { useT } from '#app/intl/api';
import { useToggleBusy } from "#app/system/api";
import { through } from "#app/utils/common";
import { notify, showAndThrow, useFeedback } from "#app/utils/feedback";
import { requirementApi, useRequirementCalls } from "#requirement/calls";
import { Requirement, RequirementDto } from "#requirement/model";
import { usePreload } from 'apprise-frontend-core/client/preload';
import { requirementPlural, requirementSingular, requirementType } from "./constants";
import { useRequirementStore } from './store';
import { useL } from 'apprise-frontend-core/intl/multilang';
import { Paragraph } from '#app/components/Typography';
import { Trans } from 'react-i18next';


export const useRequirementClient = () => {

    const t = useT()
    const l = useL()
    const toggleBusy = useToggleBusy()
    const fb = useFeedback()

    const store = useRequirementStore()
    const call = useRequirementCalls()

    const singular = t(requirementSingular).toLowerCase()
    const plural = t(requirementPlural).toLowerCase()

    const preload = usePreload()

    const self = {

        areReady: () => !!store.all()

        ,

        isLoaded: (req: Requirement | undefined) => !!req?.properties?.layout

        ,

        fetchAll: (forceRefresh = false): Promise<Requirement[]> =>

            self.areReady() && !forceRefresh ? Promise.resolve(store.all())

                :

                toggleBusy(`${requirementType}.fetchAll`, t("common.feedback.load", { plural }))

                    .then(_ => console.log(`fetching requirements...`))
                    .then(_ => preload.get<Requirement[]>(requirementApi) ?? call.fetchAll())
                    .then(through($ => store.setAll($, { noOverwrite: true })))


                    .catch(e => showAndThrow(e, t("common.calls.fetch_all_error", { plural })))
                    .finally(() => toggleBusy(`${requirementType}.fetchAll`))


        ,


        fetchOne: (id: string, forceRefresh?: boolean): Promise<Requirement> => {

            const current = store.lookup(id)

            return current && self.isLoaded(current) && !forceRefresh ?

                Promise.resolve(current) :


                toggleBusy(`${requirementType}.fetchOne`, t("common.feedback.load_one", { singular }))


                    .then(_ => call.fetchOne(id))
                    .then(through(fetched => { if (!fetched) throw Error(`Invalid resource lookup for ${id}, returned undefined.`) }))
                    .then(through(fetched => store.setAll(store.all().map(u => u.id === id ? fetched : u))))

                    .catch(e => showAndThrow(e, t("common.calls.fetch_one_error", { singular })))
                    .finally(() => toggleBusy(`${requirementType}.fetchOne`))
        }

        ,

        fullFetch: (requirement: Requirement, force?: boolean): Promise<Requirement> =>

            self.isLoaded(requirement) && !force ? Promise.resolve(requirement) : self.fetchOne(requirement.id)



        ,

        fetchMany: (ids: string[], force?: boolean): Promise<Requirement[]> =>

            toggleBusy(`${requirementType}.fetchMany`, t("common.feedback.load_many", { plural }))

                .then(() => self.fetchManySilently(ids, force))

                .catch(e => showAndThrow(e, t("common.calls.fetch_many_error", { plural })))
                .finally(() => toggleBusy(`${requirementType}.fetchMany`))

        ,


        fetchManySilently: async (ids: string[], force?: boolean): Promise<Requirement[]> => {

            const filteredIds = ids.filter(id => force || !self.isLoaded(store.lookup(id)))

            const parallelism = 4

            const groupSize = Math.min(30, Math.ceil(filteredIds.length / parallelism))

            // Prepare tasks that fetch requirements 10 at the time
            // (splice mutates the original array and returns the deleted entries.)
            let fetchtasks = [] as (() => Promise<RequirementDto[]>)[]
            while (filteredIds.length) {
                const group = filteredIds.splice(0, groupSize)
                fetchtasks = [...fetchtasks, () => call.fetchMany(group)]
            }
            // Execute tasks 5 at the time in parallel.
            let taskgroups = [] as Promise<RequirementDto[]>[]
            while (fetchtasks.length) taskgroups = [...taskgroups, Promise.all(fetchtasks.splice(0, parallelism).map(task => task())).then(reqs => reqs.flat()).then(fetched => fetched.filter(r => r))]

            const fetched = await taskgroups.reduce((prev, cur) => prev.then(reqs => cur.then(reqs2 => [...reqs, ...reqs2])), Promise.resolve([] as RequirementDto[]));

            store.setAll(store.all().map(u => fetched.find(uu => uu.id === u.id) ?? u))

            return fetched
        }

        ,


        save: (requirement: Requirement): Promise<Requirement> =>

            toggleBusy(`${requirementType}.save`, t("common.feedback.save_changes"))


                .then(() => requirement.lifecycle.created ?

                    call.update(requirement)
                        .then(through(saved => store.setAll(store.all().map(r => r.id === requirement.id ? saved : r))))

                    :

                    call.add(requirement)
                        .then(through(saved => store.setAll([saved, ...store.all()])))

                )
                .then(through(() => notify(t('common.feedback.saved'))))

                .catch(e => showAndThrow(e, t("common.calls.save_one_error", { singular })))
                .finally(() => toggleBusy(`${requirementType}.save`))

        ,

        remove: (id: string, onConfirm: () => void, challenge?: boolean) => {

            const req = store.lookup(id)

            const content = req ? <div>
                    <Paragraph style={{marginBottom: 10}}>
                        <Trans i18nKey={'common.consent.remove_one_msg_head'} values={{singular, name: l(req.name)}} components={{ bold: <strong />}}/>
                    </Paragraph>
                    <Paragraph>{t('common.consent.remove_one_msg', { singular })}</Paragraph>
                </div>                 
            : t('common.consent.remove_one_msg', { singular })

            fb.askConsent({

                title: t('common.consent.remove_one_title', { singular }),
                content,
                okText: t('common.consent.remove_one_confirm', { singular }),

                okChallenge: challenge ? t('common.consent.remove_challenge', { singular }) : undefined,

                onOk: () => {


                    toggleBusy(`${requirementType}.removeOne`, t("common.feedback.save_changes"))

                        .then(_ => call.delete(id))
                        .then(_ => store.all().filter(u => id !== u.id))
                        .then(store.setAll)
                        .then(_ => notify(t('common.feedback.saved')))
                        .then(_ => onConfirm && onConfirm())

                        .catch(e => showAndThrow(e, t("common.calls.remove_one_error", { singular })))
                        .finally(() => toggleBusy(`${requirementType}.removeOne`))


                }
            })
        },

        removeAll: (list: string[], onConfirm?: () => void) =>

            fb.askConsent({


                title: t('common.consent.remove_many_title', { count: list.length, plural }),
                content: t('common.consent.remove_many_msg', { plural }),
                okText: t('common.consent.remove_many_confirm', { count: list.length, plural }),

                onOk: () => {


                    toggleBusy(`${requirementType}.removeAll`, t("common.feedback.save_changes"))

                        .then(_ => list.forEach(id => call.delete(id)))
                        .then(_ => store.all().filter(u => !list.includes(u.id)))
                        .then(store.setAll)
                        .then(_ => notify(t('common.feedback.saved')))
                        .then(_ => onConfirm && onConfirm())

                        .catch(e => showAndThrow(e, t("common.calls.remove_one_error", { singular })))
                        .finally(() => toggleBusy(`${requirementType}.removeAll`))


                }
            })


    }

    return self

}