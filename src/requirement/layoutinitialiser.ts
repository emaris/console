
import { useIntegrationParameter } from '#integration/parameter'
import { useRavSectionComponent } from '#integration/rav/client/section'
import { useAnchorComponent } from '#layout/components/anchor'
import { useConditionalSection } from '#layout/components/conditionalsection'
import { useConditionContext } from '#layout/components/conditioncontext'
import { useDateBoxComponent } from '#layout/components/datebox'
import { useDocumentComponent } from '#layout/components/document'
import { useImageComponent } from '#layout/components/image'
import { useInputBoxComponent } from '#layout/components/inputbox'
import { useMultiChoiceComponent } from '#layout/components/multichoice'
import { useNumberBoxComponent } from '#layout/components/numberbox'
import { useParagraphComponent } from '#layout/components/paragraph'
import { useCellComponent, useRowComponent } from '#layout/components/row'
import { useRowSectionComponent } from '#layout/components/rowsection'
import { useSectionComponent } from '#layout/components/section'
import { useSeparatorComponent } from '#layout/components/separator'
import { useSubmissionFileDropComponent } from '#layout/components/submissionfiledrop'
import { useTitleComponent } from '#layout/components/title'
import { useCampaignParameter } from '#layout/parameters/campaign'
import { useDateYearParam } from '#layout/parameters/dateyear'
import { useNumberParameter } from '#layout/parameters/number'
import { usePartyParameter } from '#layout/parameters/party'
import { useRequirementParameter } from '#layout/parameters/requirement'
import { useTextParameter } from '#layout/parameters/text'
import { useLayoutRegistry } from '#layout/registry'


export const useRequirementLayoutInitialiser = () => {

    const registry = useLayoutRegistry()
    const textparam = useTextParameter()
    const numberparam = useNumberParameter()
    const dateyearparam = useDateYearParam()
    const integrationParam = useIntegrationParameter()

    const partyparam = usePartyParameter()
    const campaignparam = useCampaignParameter()
    const requirementParam = useRequirementParameter()

    const paragraph = useParagraphComponent()
    const title = useTitleComponent()
    const inputbox = useInputBoxComponent()
    const numberbox = useNumberBoxComponent()
    const datebox = useDateBoxComponent()
    const multichoice = useMultiChoiceComponent()
    const submissionFileDrop = useSubmissionFileDropComponent()

    const conditioncontext = useConditionContext()
    const conditionalsection = useConditionalSection()

    const ravSection = useRavSectionComponent()

    const rowsection = useRowSectionComponent()
    const row = useRowComponent()

    const cell = useCellComponent()

    const document = useDocumentComponent()
    const image = useImageComponent()
    const separator = useSeparatorComponent()

    const section = useSectionComponent()

    const anchor = useAnchorComponent()


    return () => {

        registry.addComponents([
            
            section,

            title,
            paragraph,

            rowsection,
            row,
            separator,
            image,
            document,
          
            inputbox,
            numberbox,
            datebox,
            multichoice,
            submissionFileDrop,

            conditionalsection,
            conditioncontext,

            ravSection,

            anchor,

            
            cell,
            
            
        ])
        .addParameters([
            textparam,
            numberparam,
            dateyearparam,
            partyparam,
            campaignparam,
            requirementParam,
            integrationParam
        ])
        
    }


}