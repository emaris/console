
import { useRenderGuard } from 'apprise-frontend-core/utils/renderguard'
import * as React from "react"
import { useRequirementClient } from './client'

type Props = React.PropsWithChildren<{

  placeholder?: React.ReactNode

}>


export const RequirementLoader = (props: Props) => {

  const client = useRequirementClient()

  const {content} = useRenderGuard({
    
    when: client.areReady(),
    render: props.children,
    orRun: client.fetchAll,
    andRender: props.placeholder

  })

  return content


}