
import { CapProperties, useCapProperties } from '#app/components/CapList'
import { useT } from '#app/intl/api'
import { Lifecycle, newLifecycle } from "#app/model/lifecycle"
import { MultilangDto, newMultiLang, noMultilang, useCompareMultiLang, useL } from "#app/model/multilang"
import { useSettings } from '#app/settings/store'
import { Document } from "#app/stream/model"
import { systemType } from "#app/system/constants"
import { SystemAppSettings } from "#app/system/model"
import { TagExpression, TagMap, Tagged, useTagModel } from "#app/tag/model"
import { TenantAudience } from "#app/tenant/AudienceList"
import { noTenant } from '#app/tenant/constants'
import { User } from "#app/user/model"
import { deepclone, shortid } from "#app/utils/common"
import { Layout, newLayout } from "#layout/model"
import { predefinedRequirementParameterId, usePredefinedParameters, usePredefinedRequirementParameters } from "#layout/parameters/constants"
import { requirementSingular, requirementType } from "./constants"
import { useRequirementStore } from './store'


export type RequirementDto = Tagged & {

    id: string

    predefined?: boolean

    lifecycle: Lifecycle<RequirementState>

    name: MultilangDto,
    description: MultilangDto

    audience: TagExpression
    audienceList?: TenantAudience
    userProfile: TagExpression

    lineage?: string[]

    documents?: Record<string, Document[]>

    properties: RequirementProperties
}

export type RequirementProperties = {

    source: RequirementSource,
    cap: CapProperties,
    layout: Layout
    note?: Record<string, string>
    versionable: boolean | undefined
    editable: boolean | undefined
    assessed: boolean | undefined
    submissionTagMap?: TagMap[]
    version: string | undefined

} //& {[key:string]:any}

export type RequirementSource = {

    title: MultilangDto

}

export type RequirementState = "active" | "inactive"

export type Requirement = RequirementDto


export const useNewRequirement = () => {

    const settings = useSettings().get<SystemAppSettings>()[systemType] ?? {}
    const predefinedParams = usePredefinedParameters()
    const predefinedRequirementParams = usePredefinedRequirementParameters()

    const cap = useCapProperties()

    return {
        get: () => {

            return {

                tags: settings.defaultRequirementImportance ? ["TG-system-defaultcontext", ...settings.defaultRequirementImportance] : ["TG-system-defaultcontext"], //Default context set

                id: undefined!,
                name: newMultiLang(),
                description: newMultiLang(),
                lineage: [],
                audience: { terms: [] },
                userProfile: { terms: [] },
                audienceList: undefined!,
                lifecycle: newLifecycle('inactive'),

                properties: {

                    title: newMultiLang(),
                    source: { type: undefined!, title: newMultiLang() },
                    layout: { ...newLayout(), parameters: [...predefinedParams.get(), ...predefinedRequirementParams.get()] },
                    cap: cap.defaultsFor(requirementType),
                    versionable: true,
                    editable: true,
                    assessed: true,
                    version: undefined

                }

            } as Requirement

        }
    }
}



export const useNoRequirement = () => {

    const t = useT()

    const newRequirement = useNewRequirement()

    return {

        get: (): Requirement => {

            return {
                ...newRequirement.get(),
                id: `${newRequirementId()}-unknown`,
                name: noMultilang(t('common.labels.unknown_one', { singular: t(requirementSingular) }))
            }
        }
    }
}


export const completeOnAdd = (requirement: Requirement, id?: string) => {

    const newreq = { ...requirement, id: id ?? newRequirementId() }

    const hostparam = newreq.properties.layout.parameters.find(p => p.id === predefinedRequirementParameterId)

    if (hostparam)
        hostparam.value = { id: newreq.id }

    return newreq

}



export const newRequirementId = () => `R-${shortid()}`



export const useRequirementModel = () => {


    const l = useL()
    const compare = useCompareMultiLang()

    const tags = useTagModel()

    const store = useRequirementStore()


    const self = {

        stringify: (r: Requirement | undefined) => r ? `${l(r.name)} ${l(r.description) ?? ''}` : ``

        ,

        stringifyRef: (r: string | undefined) => self.stringify(store.safeLookup(r))

        ,

        nameOf: (r: Requirement | undefined) => r ? `${l(r.name)}` : ''

        ,

        nameOfRef: (r: string | undefined) => self.nameOf(store.safeLookup(r))


        ,

        clone: (r: Requirement): Requirement => ({ ...deepclone(r), id: undefined!, predefined: false, lifecycle: newLifecycle(`inactive`)/* , documents: {} */ })

        ,

        branch: (r: Requirement): Requirement => ({ ...deepclone(r), id: undefined!, predefined: false, lifecycle: newLifecycle(`inactive`), lineage: [r.id]/* , documents: {} */ })

        ,

        comparator: (o1: Requirement, o2: Requirement) => compare(o1.name, o2.name)

        ,


        comparatorRef: (o1: string, o2: string) => self.comparator(store.safeLookup(o1), store.safeLookup(o2))

        ,

        matches: (r: Requirement, u: User) => {

            return u.tenant === noTenant || tags.given(u).matches(r.userProfile)

        },

        explicitLayoutParameters: (r: Requirement) => r.properties.layout.parameters

        ,

        implicitLayoutParameters: (_: Requirement) => []


    }

    return self
}