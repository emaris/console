
import { useCalls } from "#app/call/call"
import { domainService } from "#constants"
import { Requirement } from "./model"

export const requirementApi = "/requirement"


export const useRequirementCalls = () => {

    const call = useCalls()

    return {

        
        fetchAll: () => call.at(`${requirementApi}/raw`, domainService).get<Requirement[]>()

        
        ,

        fetchOne: (id: String) => call.at(`${requirementApi}/${id}`, domainService).get<Requirement>()

        ,

        fetchMany: (ids: string[]) => call.at(`${requirementApi}/search`, domainService).post<Requirement[]>({ ids })

        ,

        add: (requirement: Requirement) => call.at(requirementApi, domainService).post<Requirement>(requirement)

        ,

        update: (requirement: Requirement) => call.at(`${requirementApi}/${requirement.id}`, domainService).put<Requirement>(requirement)

        ,

        delete: (id: string) => call.at(`${requirementApi}/${id}`, domainService).delete()
    }

}