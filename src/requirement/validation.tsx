

import { useCapProperties } from '#app/components/CapList';
import { useConfig } from '#app/config/state';
import { useT } from '#app/intl/api';
import { Multilang } from "#app/model/multilang";
import { contextCategory } from "#app/system/constants";
import { useTagStore } from '#app/tag/store';
import { tenantPlural } from "#app/tenant/constants";
import { userPlural } from "#app/user/constants";
import { useLogged } from '#app/user/store';
import { check, checkIt, noDupsInArray, notdefined, requireLanguages, stringLength, uniqueLanguages, withReport } from "#app/utils/validation";
import { requirementPlural, requirementSingular, requirementType } from "#requirement/constants";
import { Requirement } from "#requirement/model";
import { useRequirementStore } from './store';

export type RequirementValidation = ReturnType<typeof useRequirementValidation>

export const useRequirementValidation = () => {

    const t = useT()

    const { validateCategories } = useTagStore()

    const store = useRequirementStore()

    const logged = useLogged()

    const config = useConfig()

    const caps = useCapProperties()

    return {

        validateRequirement: (edited: Requirement) => {

            const requirements = store.all();
            const requiredLangs = config.get().intl.required || [];

            const singular = t(requirementSingular).toLowerCase()
            const plural = t(requirementPlural).toLowerCase()

            const partyAudiencePlural = t(tenantPlural).toLowerCase()
            const userProfilePlural = t(userPlural).toLowerCase()

            const mapName = (r: Requirement) => {
                const version = (r.properties.version ?? '').trim()
                const name = requiredLangs.reduce((acc, cur) => ({...acc, [cur] : `${(r.name?.[cur] ?? '')} ${version}`.trim()}), {})
                return name as Multilang
            }

            const requirementNames = requirements.filter(t => t.id !== edited.id).map(mapName)

            return withReport({

                active: checkIt().nowOr(t("common.fields.active.msg"), t("common.fields.active.help", { plural })),



                name: check(mapName(edited)).with(notdefined(t))
                    .with(requireLanguages(requiredLangs, t))
                    .with(stringLength(t))
                    .with(uniqueLanguages(requirementNames, t)).nowOr(
                        t("common.fields.name_multi.msg"),
                        t("common.fields.name_multi.help", { plural, requiredLangs })),


                title: checkIt().nowOr(
                    t("common.fields.title.msg", { singular }),
                    t("common.fields.title.help", { plural, requiredLangs })),

                version: check(mapName(edited)).with(uniqueLanguages(requirementNames, t)).nowOr(
                    t("common.fields.version.msg"),
                    t("common.fields.version.help", {plural: t("common.version.plural")})),

                // title: check(edited.description).with(notdefined(t))
                //             .with(requireLanguages(requiredLangs,t))
                //             .with(uniqueLanguages(requirements.filter(t=>t.id!==edited.id).map(t=>t.description ?? {}),t)).nowOr(
                //                     t("common.fields.title.msg",{singular}),
                //                     t("common.fields.title.help",{plural,requiredLangs})),


                sourcetitle: check(edited.properties.source?.title).nowOr(
                    t("common.fields.description_multi_rich.msg"),
                    t("requirement.fields.source_title.help", { plural }),
                ),


                lineage: checkIt().nowOr(
                    t("common.fields.lineage.msg", { singular }),
                    t("requirement.fields.lineage.help"),
                )

                ,

                note: check(edited.properties.note ? edited.properties.note[logged.tenant] : '').nowOr(t("common.fields.note.msg"), t("common.fields.note.help", { singular }))

                ,

                audience: checkIt().nowOr(
                    t("common.fields.audience.msg"),
                    t("common.fields.audience.help", { singular, plural: partyAudiencePlural })
                ),

                audienceList: checkIt().nowOr(
                    t("common.fields.audience_list.msg"),
                    t("common.fields.audience_list.help", { singular, plural: partyAudiencePlural })
                ),

                userProfile: checkIt().nowOr(
                    t("common.fields.user_profile.msg"),
                    t("common.fields.user_profile.help", { singular, plural: t(userProfilePlural) })


                )

                ,

                editable: checkIt().nowOr(
                    t("common.fields.editable.msg"), t("common.fields.editable.help", { plural, parties: partyAudiencePlural })
                )

                ,

                versionable: checkIt().nowOr(
                    t("common.fields.versionable.msg"), t("common.fields.versionable.help", { plural, parties: partyAudiencePlural })
                )

                ,

                assessed: checkIt().nowOr(
                    t("common.fields.assessed.msg"), t("common.fields.assessed.help", { plural })
                )

                ,

                tagMap: check((edited.properties.submissionTagMap ?? []).map(tagMap => tagMap.category))
                    .with(noDupsInArray(t)).nowOr(undefined, t("requirement.fields.tagmap.help", { asset: t(requirementSingular).toLowerCase() }))

                ,

                ...validateCategories(edited.tags).include(contextCategory),

                ...validateCategories(edited.tags).for(requirementType),

                ...validateCategories(edited.properties.submissionTagMap?.flatMap(tagmap => tagmap.tags)).include(...(edited.properties.submissionTagMap?.map(tagmap => tagmap.category) ?? [])),

                ...caps.validationFor(edited.properties.cap)
            })


        }
    }
}

