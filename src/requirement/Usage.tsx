import { FormState } from "#app/form/hooks"
import { useT } from '#app/intl/api'
import { useLocale } from "#app/model/hooks"
import { ContentHeader } from "#app/scaffold/ContentHeader"
import { campaignIcon, campaignPlural, campaignSingular } from "#campaign/constants"
import { useCampaignUsage } from "#campaign/UsageList"
import { requirementType } from "#requirement/constants"
import { Requirement } from "#requirement/model"
import * as React from "react"

export type Props = FormState<Requirement>

export const RequirementUsage = (props:Props) => {

    const t = useT()
    const {l} = useLocale()
   

    const {edited} = props

    const [campaignUsageList,campaignList] = useCampaignUsage({type:requirementType,target:edited.id})

    const name = l(edited.name)
    const campcount = campaignList?.length ?? -1
    const campaigns = t( campcount!==1 ? campaignPlural: campaignSingular)

    return <> 

            <ContentHeader  title={<span>{campaignIcon}&nbsp;&nbsp; <span>{campcount===-1? '...' : campcount}</span> {campaigns}</span>} 
                            subtitle={t("campaign.feedback.used_in",{name,campcount:campcount===-1? '...' : campcount})} />

            {campaignUsageList}

          </>


}