import { CapList } from '#app/components/CapList'
import { Form } from '#app/form/Form'
import { MultiBox } from '#app/form/MultiBox'
import { NoteBox } from '#app/form/NoteBox'
import { Switch } from '#app/form/Switch'
import { FormState } from '#app/form/hooks'
import { useT } from '#app/intl/api'
import { useLocale } from '#app/model/hooks'
import { ContextAwareSelectBox } from '#app/system/ContextAwareSelectBox'
import { contextCategory, systemType } from '#app/system/constants'
import { useFacets } from '#app/system/facets'
import { TagBoxset } from '#app/tag/TagBoxset'
import { TagMapBox } from '#app/tag/TagMapBox'
import { TagRefBox } from '#app/tag/TagRefBox'
import { useTagStore } from '#app/tag/store'
import { AudienceList } from '#app/tenant/AudienceList'
import { tenantType } from '#app/tenant/constants'
import { userType } from '#app/user/constants'
import { useLogged } from '#app/user/store'
import { submissionType } from '#campaign/submission/constants'
import { RequirementLabel } from '#requirement/Label'
import { requirementSingular, requirementType, sourceTypeCategory } from '#requirement/constants'
import { Requirement } from '#requirement/model'
import { RequirementValidation } from '#requirement/validation'
import { partition } from "lodash"
import { useRequirementStore } from './store'
import { TextBox } from '#app/form/TextBox'

type Props = FormState<Requirement> & {

    isNew: boolean
    report: ReturnType<RequirementValidation['validateRequirement']>
}


export const RequirementGeneralForm = (props: Props) => {

    const t = useT()
    const { l } = useLocale()
    const { edited, change, report } = props

    const { contextOf } = useFacets()
    const { lookupTag, lookupCategory, allCategoriesOf } = useTagStore()
    const requirements = useRequirementStore()

    const logged = useLogged()



    const sourcetype = lookupCategory(sourceTypeCategory)

    // separetes sourcetype category from others, as it gets ad-hoc placement
    const restOfCategories = allCategoriesOf(requirementType).filter(c => c.id !== sourceTypeCategory)

    // separate system tags from requirement tags
    const [facets, other] = partition(edited.tags, t => lookupTag(t).type === systemType)

    const context = lookupCategory(contextCategory)

    const otherRequirements = requirements.all().filter(r => r.id !== edited.id)

    return <Form state={props} sidebar>

        <Switch label={t("common.fields.active.name")} onChange={change((u, v) => u.lifecycle.state = v ? "active" : "inactive")} validation={report.active}>
            {edited.lifecycle.state === 'active'}
        </Switch>

        <MultiBox id="requirement-name" label={t("common.fields.name_multi.name")} validation={report.name} onChange={change((t, v) => t.name = v)}>
            {edited.name}
        </MultiBox>

        <TextBox id='requirement.version' label={t("common.fields.version.name")} validation={report.version} onChange={change((t, v) => t.properties.version = v)}>
            {edited.properties.version}
        </TextBox>

        <MultiBox id="requirement-title" label={t("common.fields.title.name")} validation={report.title} onChange={change((t, v) => t.description = v)}>
            {edited.description}
        </MultiBox>

        <TagBoxset edited={other} categories={[sourcetype]} validation={report} onChange={change((t, v) => t.tags = [...facets, ...v])} />

        <MultiBox autoSize id="requirement-source-title" label={t("requirement.fields.source_title.name")} validation={report.sourcetitle} onChange={change((t, v) => t.properties.source = { ...t.properties.source, title: v })}>
            {edited.properties.source?.title}
        </MultiBox>

        <CapList type={requirementType} singular={requirementSingular} edited={edited.properties.cap ?? {}} onChange={change((u, v) => u.properties = { ...u.properties, cap: v })} />

        <ContextAwareSelectBox mode="multiple" label={t("common.fields.lineage.name")} validation={report.lineage}
            currentContext={contextOf(edited)}
            options={otherRequirements}
            onChange={change((t, vs) => t.lineage = vs ? vs.map(v => v.id) : vs)}
            renderOption={r => <RequirementLabel noLink requirement={r} />}
            lblTxt={r => l(r.name)}
            optionId={r => r.id}>

            {requirements.all().filter(r => edited.lineage?.includes(r.id))}

        </ContextAwareSelectBox>

        <TagRefBox mode="multi" label={t("common.fields.audience.name")} validation={report.audience} expression={edited.audience} type={tenantType} onChange={change((t, v) => t.audience = v)} />

        <AudienceList label={t("common.fields.audience_list.name")} validation={report.audienceList} onChange={change((t, v) => t.audienceList = v)}>{edited.audienceList}</AudienceList>

        <TagRefBox mode="multi" label={t("common.fields.user_profile.name")} validation={report.userProfile} expression={edited.userProfile} type={userType} onChange={change((t, v) => t.userProfile = v)} />

        <TagBoxset edited={other} categories={restOfCategories} type={requirementType} validation={report} onChange={change((t, v) => t.tags = [...facets, ...v])} />

        <TagBoxset edited={facets} categories={[context]} validation={report} onChange={change((t, v) => t.tags = [...v, ...other])} />

        <TagMapBox type={submissionType} validationReport={report} validation={report.tagMap} label={t("requirement.fields.tagmap.label")} singleLabel={t("requirement.fields.tagmap.add")} onChange={change((t, v) => t.properties.submissionTagMap = v)}>{edited.properties.submissionTagMap}</TagMapBox>

        <Switch label={t("common.fields.editable.name")} onChange={change((u, v) => u.properties.editable = v)} validation={report.editable}>
            {edited.properties.editable !== undefined ? edited.properties.editable : true}
        </Switch>

        <Switch label={t("common.fields.versionable.name")} onChange={change((u, v) => u.properties.versionable = v)} validation={report.versionable}>
            {edited.properties.versionable !== undefined ? edited.properties.versionable : true}
        </Switch>

        <Switch label={t("common.fields.assessed.name")} onChange={change((u, v) => u.properties.assessed = v)} validation={report.assessed}>
            {edited.properties.assessed !== undefined ? edited.properties.assessed : true}
        </Switch>

        <NoteBox label={t("common.fields.note.name")} validation={report.note} autoSize={{ minRows: 4, maxRows: 6 }} onChange={change((t, v) => t.properties.note = t.properties.note ? { ...t.properties.note, [logged.tenant]: v } : { [logged.tenant]: v })} >
            {edited.properties.note}
        </NoteBox>


    </Form>

}
