import { fallbackStateOver, State } from 'apprise-frontend-core/state/api'
import { createContext } from 'react'
import { Requirement } from './model'

export type RequirementNext = {
    model: Requirement
}

export type RequirementState = {

    requirements: {
        all: Requirement[],
        map: Record<string, Requirement>
        next?: RequirementNext
    }
}

export const initialRequirements: RequirementState = {

    requirements: {
        all: undefined!,
        map: undefined!
    }

}

export const RequirementContext = createContext<State<RequirementState>>(fallbackStateOver(initialRequirements))