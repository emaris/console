
import { any } from '#app/iam/model'
import { ChangesProps } from '#app/iam/permission'
import { usePermissionDrawer } from '#app/iam/PermissionForm'
import { ResourceProps, StateProps, SubjectProps } from '#app/iam/PermissionTable'
import { useLocale } from '#app/model/hooks'
import { noTenant } from '#app/tenant/constants'
import { User } from '#app/user/model'
import { UserPermissions } from '#app/user/UserPermissionTable'
import * as React from 'react'
import { useT } from '#app/intl/api'
import { requirementActions } from './actions'
import { requirementPlural, requirementSingular, requirementType } from './constants'
import { RequirementLabel } from './Label'
import { Requirement } from './model'
import { useRequirementStore } from './store'


type PermissionsProps = ChangesProps & Partial<ResourceProps<Requirement>> & Partial<SubjectProps<User>> & Partial<StateProps<User,Requirement>> & {
    edited? : User
}


//  wraps UserPermissions for Requirements, so as to inject appropriate defaults.

export const RequirementPermissions =  (props:PermissionsProps) => {

    const t = useT()
    const {l} = useLocale()
    const requirements = useRequirementStore()

    return  <UserPermissions {...props}
                id="requirement-permissions" 
                subjectRange={props.subjectRange?.filter(s=>s.tenant===noTenant)}
                resourceSingular={props.resourceSingular || t(requirementSingular)}
                resourcePlural={props.resourcePlural || t(requirementPlural)}
                resourceText={t=>l(t.name)}
                renderResource={props.renderResource || ((t:Requirement) => <RequirementLabel requirement={t} /> )}
                resourceId={props.resourceId || ((r:Requirement) => r.id) }
                renderResourceOption={props.renderResourceOption || ((r:Requirement) => <RequirementLabel noLink requirement={r} />)} 
                resourceRange={props.resourceRange || requirements.all() } 
                resourceType={requirementType}

                actions={Object.values(requirementActions)} />
                
}


//  wraps the table in a drawer-based-form
export const useRequirementPermissions = ()=>usePermissionDrawer ( RequirementPermissions, { types:[requirementType, any] } )