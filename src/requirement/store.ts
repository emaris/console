
import { indexMap } from "#app/utils/common";
import { Requirement, useNewRequirement, useNoRequirement } from "#requirement/model";
import { useContext } from 'react';
import { requirementRoute } from "./constants";
import { RequirementContext, RequirementNext } from './context';



export const useRequirementStore = () => {

    const state = useContext(RequirementContext)

    const newRequirement = useNewRequirement()
    const noRequirement = useNoRequirement()

    const self = {

        all: () => state.get().requirements.all

        ,

        lookup: (id: string | undefined) => id ? state.get().requirements.map?.[id] : undefined

        ,

        safeLookup: (id: string | undefined) => self.lookup(id) ?? noRequirement.get()

        ,

        setAll: (requirements: Requirement[], props?: { noOverwrite: boolean }) => state.set(s => {

            if (s.requirements.all && props?.noOverwrite)
                return

            s.requirements.all = requirements; s.requirements.map = indexMap(requirements).by(r => r.id)


        })

        ,

        routeTo: (r: Requirement) => r ? `${requirementRoute}/${r.id}` : requirementRoute

        
        ,

        next: () => {
            const next = state.get().requirements.next || { model: newRequirement.get() }
            return next;
        }

        ,

        resetNext: () => {
            self.setNext(undefined)
        }

        ,

        setNext: (t: RequirementNext | Requirement | undefined): RequirementNext | undefined => {
            const model = t ? t.hasOwnProperty('model') ? t as RequirementNext : { model: t as Requirement } : undefined

            state.set(s => s.requirements.next = model)
            return model

        }
    }

    return self

}