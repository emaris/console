
import { Placeholder } from "#app/components/Placeholder"
import { requirementRoute } from "#requirement/constants"
import { RequirementLoader } from "#requirement/Loader"
import { Route, Switch } from "react-router"
import { NewRequirementDetail, RequirementDetail } from "./Detail"
import { RequirementList } from "./List"


export const Requirements = () => {

    return <RequirementLoader placeholder={Placeholder.page}>
        <Switch>
            <Route exact path={requirementRoute} component={RequirementList} />
            <Route path={`${requirementRoute}/new`} component={NewRequirementDetail} />
            <Route path={`${requirementRoute}/:id`} component={RequirementDetail} />
        </Switch>
    </RequirementLoader>

}