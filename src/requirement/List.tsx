

import { buildinfo } from "#app/buildinfo"
import { Button } from "#app/components/Button"
import { Label } from "#app/components/Label"
import { Column, VirtualTable } from "#app/components/VirtualTable"
import { useListState } from "#app/components/hooks"
import { iamPlural } from "#app/iam/constants"
import { specialise } from "#app/iam/model"
import { icns } from "#app/icons"
import { useT } from '#app/intl/api'
import { useLocale } from "#app/model/hooks"
import { Page } from "#app/scaffold/Page"
import { Titlebar } from "#app/scaffold/PageHeader"
import { Sidebar } from "#app/scaffold/Sidebar"
import { Topbar } from "#app/scaffold/Topbar"
import { contextCategory } from '#app/system/constants'
import { useContextFilter } from '#app/system/filter'
import { TagList } from "#app/tag/Label"
import { useTagFilter, useTagHolderFilter } from '#app/tag/filter'
import { tagRefsIn } from "#app/tag/model"
import { useTagStore } from '#app/tag/store'
import { tenantPlural } from "#app/tenant/constants"
import { userPlural } from '#app/user/constants'
import { useLogged } from '#app/user/store'
import { useBaseFilter } from '#app/utils/filter'
import { useCampaignUsage } from '#campaign/Usage'
import { RequirementLabel } from "#requirement/Label"
import { useRequirementPermissions } from "#requirement/RequirementPermissions"
import { requirementPlural, requirementSingular, requirementType } from "#requirement/constants"
import { Requirement, useRequirementModel } from "#requirement/model"
import * as React from "react"
import { useHistory, useLocation } from "react-router"
import { requirementActions } from './actions'
import { useRequirementClient } from './client'
import { useRequirementStore } from './store'


export const requirementGroup = requirementType

export const RequirementList = () => {

    const { l } = useLocale()
    const t = useT()
    const history = useHistory()
    const { pathname } = useLocation()

    const tags = useTagStore()
    const logged = useLogged()


    const store = useRequirementStore()
    const client = useRequirementClient()
    const model = useRequirementModel()


    const [singular, plural] = [t(requirementSingular), t(requirementPlural)]


    const [Permissions, showPermissions] = useRequirementPermissions()

    const usage = useCampaignUsage()

    const liststate = useListState<Requirement>()


    // -------------- authz privileges
    const canManage = (r: Requirement) => logged.can(specialise(requirementActions.manage, r.id))


    // -------------- actions
    const onRemove = ({ id }: Requirement) => client.remove(
        id,
        () => {

            liststate.setSelected(liststate.selected.filter(l => l.id !== id))
        }
        , usage.isInUse(id)
    )

    const onClone = (requirement: Requirement) => {

        client.fetchOne(requirement.id).then(loaded => {

            store.setNext({ model: model.clone(loaded) })
            history.push(`${pathname}/new`)
        })
    }

    const onBranch = (requirement: Requirement) => {

        client.fetchOne(requirement.id).then(loaded => {

            store.setNext({ model: model.branch(loaded) })
            history.push(`${pathname}/new`)
        })
    }

    const onRemoveAll = () => client.removeAll(liststate.selected.map(l => l.id), () => liststate.setSelected([]))

    const onAddRequirement = () => Promise.resolve(store.resetNext())

    // -------------- buttons

    const addBtn = <Button
        type="primary"
        icn={icns.add}
        enabled={logged.can(requirementActions.edit)}
        onClick={onAddRequirement}
        linkTo={`${pathname}/new`}>
        {t("common.buttons.add_one", { singular })}
    </Button>

    const openBtn = (r: Requirement) => <Button
        key={1}
        icn={icns.open}
        linkTo={store.routeTo(r)}>
        {t("common.buttons.open")}
    </Button>


    const cloneBtn = (requirement: Requirement) =>
        <Button
            key={3}
            icn={icns.clone}
            onClick={() => onClone(requirement)} >
            {t("common.buttons.clone")}
        </Button>

    const branchBtn = (requirement: Requirement) =>
        <Button
            key={4}
            icn={icns.branch}
            onClick={() => onBranch(requirement)} >
            {t("campaign.buttons.branch")}
        </Button>

    const rightsBtn = <Button
        enabled={liststate.selected.length > 0}
        icn={icns.permissions}
        style={{ marginTop: 30 }}
        onClick={showPermissions}>
        {t(iamPlural)}
    </Button>

    const removeBtn = (requirement: Requirement) =>
        <Button
            key={2}
            icn={icns.remove}
            enabled={logged.can(requirementActions.edit) && requirement.lifecycle.state === 'inactive'}
            disabled={requirement.predefined || usage.isInUse(requirement)}
            onClick={() => onRemove(requirement)} >
            {t("common.buttons.remove")}
        </Button>

    const removeAllBtn = <Button
        type="danger"
        disabled={liststate.selected.length < 1}
        enabled={logged.can(requirementActions.edit)}
        onClick={onRemoveAll} >
        <span style={{ fontVariantCaps: "all-small-caps" }}>(DEV)</span> {t("common.labels.remove_all", { count: liststate.selected.length })}
    </Button>


    // filters

    const unfilteredData = store.all()



    // eslint-disable-next-line
    const sortedData = React.useMemo(() => [...unfilteredData].sort(model.comparator), [unfilteredData])

    const { ContextSelector, contextFilteredData } = useContextFilter({
        data: sortedData,
        group: requirementGroup
    })

    const { BaseFilter, baseFilteredData } = useBaseFilter({
        data: contextFilteredData,
        readonlyUnless: canManage,
        group: requirementGroup
    })


    const { TagFilter, tagFilteredData } = useTagFilter({
        filtered: baseFilteredData,
        tagged: unfilteredData,
        excludeCategories: [contextCategory],
        key: `tags`,
        group: requirementGroup
    })

    const { TagFilter: AudienceFilter, tagFilteredData: audienceFilteredData } = useTagHolderFilter({
        filtered: tagFilteredData,
        tagged: unfilteredData,
        tagsOf: t => t.audience.terms.flatMap(t => t.tags),
        placeholder: t("common.fields.audience.name"),
        key: `audience`,
        group: requirementGroup
    })

    const { TagFilter: UserProfileFilter, tagFilteredData: userProfileFilteredData } = useTagHolderFilter({
        filtered: audienceFilteredData,
        tagged: unfilteredData,
        tagsOf: t => t.userProfile?.terms.flatMap(t => t.tags),
        placeholder: t("common.fields.user_profile.name"),
        key: `profile`,
        group: requirementGroup
    })


    const data = userProfileFilteredData




    return <Page>

        <Sidebar>
            {addBtn}
            {rightsBtn}

            {buildinfo.development &&
                <>
                    <br />
                    {removeAllBtn}
                </>
            }

        </Sidebar>

        <Topbar>

            <Titlebar title={plural} />

            {addBtn}
            {rightsBtn}

        </Topbar>


        <VirtualTable state={liststate} data={data} total={unfilteredData.length}
            filterGroup={requirementGroup}
            filterBy={model.stringify} filterPlaceholder={t("common.components.table.filter_placeholder", { plural: plural.toLowerCase() })}
            filters={[UserProfileFilter, AudienceFilter, TagFilter, BaseFilter, ContextSelector]}
            rowKey="id"
            actions={r => [openBtn(r), removeBtn(r), cloneBtn(r), branchBtn(r)]}
            onDoubleClick={r => history.push(store.routeTo(r))} >


            <Column flexGrow={2} title={t("common.fields.name_multi.name")} dataKey="name"
                dataGetter={(r: Requirement) => l(r.name)} cellRenderer={r => <RequirementLabel tipTitle lineage requirement={r.rowData} readonly={!canManage(r.rowData)} />} />

            <Column sortable={false} title={t("common.fields.audience.name")} dataKey="audience" dataGetter={(r: Requirement) => r.audience?.terms?.length > 0 ? <TagList taglist={tagRefsIn(r.audience)} /> : <Label title={t(tenantPlural)} />} />

            <Column sortable={false} title={t("common.fields.user_profile.name")} dataKey="userprofile" dataGetter={(r: Requirement) => r.userProfile?.terms?.length > 0 ? <TagList taglist={tagRefsIn(r.userProfile)} /> : <Label title={t(userPlural)} />} />

            {tags.allTagsOf(requirementType).length > 0 &&
                <Column sortable={false} flexGrow={2} title={t("common.fields.tags.name")} dataKey="tags" dataGetter={(r: Requirement) => <TagList truncateEllipsisLink={store.routeTo(r)} taglist={r.tags} />} />
            }

        </VirtualTable>

        <Permissions resourceRange={liststate.selected} />

    </Page>

}