
import { Button } from "#app/components/Button";
import { IdProperty } from "#app/components/IdProperty";
import { NoSuchRoute } from "#app/components/NoSuchRoute";
import { RouteGuard } from "#app/components/RouteGuard";
import { SideList } from "#app/components/SiderList";
import { Paragraph, Text } from "#app/components/Typography";
import { useFormState } from "#app/form/hooks";
import { iamPlural } from "#app/iam/constants";
import { any, specialise } from "#app/iam/model";
import { icns } from "#app/icons";
import { useT } from '#app/intl/api';
import { PredefinedLabel } from '#app/model/constants';
import { useLocale } from "#app/model/hooks";
import { LifecycleSummary } from "#app/model/lifecycle";
import { PushGuard } from '#app/push/PushGuard';
import { Page } from "#app/scaffold/Page";
import { Subtitle, Titlebar } from "#app/scaffold/PageHeader";
import { Sidebar } from "#app/scaffold/Sidebar";
import { Tab } from "#app/scaffold/Tab";
import { Topbar } from "#app/scaffold/Topbar";
import { BytestreamedContext, useBytestreamedHelper } from "#app/stream/BytestreamedHelper";
import { useBytestreams } from '#app/stream/api';
import { localisedIdOf, localisedStreamsOf } from "#app/stream/model";
import { TagList } from "#app/tag/Label";
import { useLogged } from '#app/user/store';
import { paramsInQuery, parentIn } from "#app/utils/routes";
import { useLiveAssetGuard } from "#campaign/LiveGuard";
import { useTocDrawer } from '#campaign/contenttoc';
import { LayoutAssetContext, LayoutConfig } from '#layout/context';
import { LayoutDesigner } from "#layout/designer";
import { Layout } from "#layout/model";
import { RequirementDetailLoader } from '#requirement/DetailLoader';
import { RequirementLabel } from "#requirement/Label";
import { useRequirementPermissions } from "#requirement/RequirementPermissions";
import { requirementPlural, requirementRoute, requirementSingular, requirementType } from "#requirement/constants";
import { Requirement, completeOnAdd, useRequirementModel } from "#requirement/model";
import * as React from "react";
import { useHistory, useLocation, useParams } from "react-router";
import { RequirementGeneralForm } from "./GeneralForm";
import { requirementGroup } from './List';
import { RequirementUsage } from "./Usage";
import { requirementActions } from './actions';
import { useRequirementClient } from './client';
import { useRequirementLayoutInitialiser } from './layoutinitialiser';
import { useRequirementStore } from './store';
import { useRequirementValidation } from './validation';



export const RequirementDetail = () => {

    const { id } = useParams<{ id: string }>()
    const { pathname } = useLocation()

    const store = useRequirementStore()

    const current = store.lookup(id)

    if (!current)
        return <NoSuchRoute backTo={parentIn(pathname)} />

    return <PushGuard>
        <RequirementDetailLoader key={current.id} id={current.id} >{

            loaded =>

                <BytestreamedContext target={loaded.id}>
                    <InnerRequirementDetail isNew={false} detail={loaded} />
                </BytestreamedContext>

        }</RequirementDetailLoader>
    </PushGuard>

}

export const NewRequirementDetail = () => {

    const store = useRequirementStore()

    const detail = store.next()

    return <BytestreamedContext><InnerRequirementDetail isNew={true} detail={detail.model} /></BytestreamedContext>

}


const InnerRequirementDetail = (props: { isNew: boolean, detail: Requirement }) => {

    const t = useT()
    const { l } = useLocale()
    const history = useHistory()
    const { pathname, search } = useLocation()

    const logged = useLogged()

    const streams = useBytestreams()

    const store = useRequirementStore()
    const client = useRequirementClient()
    const model = useRequirementModel()
    const validation = useRequirementValidation()
    const helper = useBytestreamedHelper()
    const [Permissions, showPermissions] = useRequirementPermissions()


    const singular = t(requirementSingular)

    const { isNew, detail } = props

    const formstate = useFormState<Requirement>(detail);

    const { edited, initial, change, reset, dirty } = formstate

    const { tocBtn, TocDrawer } = useTocDrawer()


    const { liveGuarded, liveGuard, usage } = useLiveAssetGuard({
        type: requirementType,
        id: edited.id,
        disabled: dirty,
        singular
    })

    const { tab } = paramsInQuery(search)

    const plural = t(requirementPlural).toLowerCase()
    const name = edited.properties.version ? <span>{l(edited.name)} <span className="label-decoration">({edited.properties.version})</span></span> : l(edited.name)

    const editIt = specialise(requirementActions.edit, detail.id)
    const canEditIt = logged.can(editIt)

    const isProtected = edited.predefined || liveGuarded



    // -------------- effetcs
    React.useEffect(() =>
        store.resetNext(),
        //eslint-disable-next-line
        [])

    // -------------- error reporting

    const report = {
        profile: validation.validateRequirement(edited)
    }

    const totalErrors = Object.keys(report).reduce((total, key) => total += report[key].errors(), 0)


    // -------------- actions

    const onSave = async () => {

        // we save first any document associated with this asset, then the asset itself.
        // for a new asset, the documents can come from cloning an existing asset, so their streams need to be cloned too at the backend.
        // for an existing asset, the documents have been added to the layout and their stream need to be uploaded at the backend.
        // in both cases new streams return from the backend and need to be reset on the respective documents.
        // extracting and resetting stremans from a on documents is a fiddly business.
        // 1. we use an intermediary structure that associates streams with doc and languages, 
        // 2. pass them apis that talk to the backend and return them with new streams in the same order,
        // 3. hen user the extra data to locate the right document to update.

        const requirement = isNew ? completeOnAdd(edited) : edited

        const docs = Object.values(requirement.documents ?? {}).flat()

        const langstreams = docs.flatMap(localisedStreamsOf)
      
        const uploaded = isNew ? await streams.clone(langstreams, requirement.id) : await helper.upload(...langstreams)

        uploaded.forEach(({id,stream,lang}) => {

            const match = docs.find(d =>  localisedIdOf(d,lang) === id)

            if (match)
                match.streams[lang] = stream

        })

        const saved = await client.save(requirement)

        reset(saved, false)

        helper.reset()

        store.resetNext();

        history.push(`${store.routeTo(saved)}${search}`)


    }

    const onRevert = () => reset(initial, helper.reset)

    const onRemove = () => client.remove(detail.id, () => {

        history.push(parentIn(pathname))

    }, usage.isInUse(edited.id))

    const onClone = () => {
        store.setNext({ model: model.clone(edited) })
        history.push(`${parentIn(pathname)}/new`)
    }

    const onBranch = () => {
        store.setNext({ model: model.branch(edited) })
        history.push(`${parentIn(pathname)}/new`)
    }

    const onAddRequirement = () => Promise.resolve(store.resetNext())

    //console.log("documents",edited.documents,"descriptors",helper.all())

    // -------------- action buttons

    const removeBtn = <Button
        icn={icns.remove}
        enabled={canEditIt && edited.lifecycle.state === 'inactive'}
        disabled={dirty || isNew || edited.predefined}
        onClick={onRemove}>
        {t("common.buttons.remove")}
    </ Button>

    const saveBtn = <Button
        type="primary"
        icn={icns.save}
        enabled={dirty}
        disabled={totalErrors > 0}
        dot={totalErrors > 0}
        onClick={onSave}>
        {t("common.buttons.save")}
    </Button>

    const revertBtn = <Button
        icn={icns.revert}
        enabled={dirty}
        type="danger"
        onClick={onRevert}>
        {t("common.buttons.revert")}
    </Button>

    const cloneBtn = <Button
        enabled={logged.can(requirementActions.edit)}
        enabledOnReadOnly
        disabled={isNew || totalErrors > 0 || dirty}
        icn={icns.clone}
        onClick={onClone}>
        {t("common.buttons.clone")}
    </Button>

    const branchBtn = <Button
        enabled={logged.can(requirementActions.edit)}
        enabledOnReadOnly
        disabled={isNew || totalErrors > 0 || dirty}
        icn={icns.branch}
        onClick={onBranch}>
        {t("campaign.buttons.branch")}
    </Button>

    const rightsBtn = <Button
        icn={icns.permissions}
        enabledOnReadOnly
        disabled={isNew || dirty}
        onClick={showPermissions}>
        {t(iamPlural)}
    </Button>

    const addRequirementBtn = <Button
        enabledOnReadOnly
        type="primary"
        icn={icns.add}
        enabled={logged.can(requirementActions.edit)}
        onClick={onAddRequirement}
        linkTo={`${requirementRoute}/new`} >
        {t("common.buttons.add_one", { singular })}
    </Button>


    const layoutConfig: LayoutConfig = {
        lazyRender: false,
        mode: 'design',
        exportsData: true,
        canUnlock: logged.hasNoTenant()
    }

    const tabcompo = (() => {

        switch (tab) {

            case "usage": return <RequirementUsage {...formstate} />
            case "layout": return <React.Fragment>
                <LayoutAssetContext.Provider value={edited}>
                    <LayoutDesigner
                        useInitialiser={useRequirementLayoutInitialiser}
                        config={layoutConfig}
                        layout={{ ...edited.properties.layout ?? {}, documents: edited.documents ?? {} }}
                        onChange={change((t, layout: Layout) => {

                            const { documents = {}, ...rest } = layout

                            t.documents = documents

                            t.properties.layout = { ...rest }
                        })} />
                </LayoutAssetContext.Provider>
                <TocDrawer enabledOnReadOnly />
            </React.Fragment>

            default: return <RequirementGeneralForm isNew={isNew} report={report.profile} {...formstate} />
        }
    })()


    const readonly = isProtected || !logged.can(editIt)


    return <Page className={`detail-${tab}`} readOnly={readonly}>

        <Sidebar>

            {saveBtn}
            {revertBtn}
            {cloneBtn}
            {branchBtn}
            {removeBtn}
            {rightsBtn}

            <br />

            <IdProperty id={detail.id} />

            <br />

            <LifecycleSummary {...edited.lifecycle} />

            <br />

            <div className="sidebar-property">
                <Paragraph>
                    {totalErrors > 0 ?

                        <Text type="danger">{t("common.validation.total_error_count", { totalErrors })}</Text>
                        :
                        <Text smaller className="emphasis">{t("common.validation.zero_error_count")}</Text>

                    }
                </Paragraph>
            </div>

            {(logged.can(requirementActions.edit) && !isNew) &&
                <>
                    <div style={{ marginTop: "25px" }}>{addRequirementBtn}</div>
                </>
            }
            <SideList<Requirement> data={store.all()}
                filterGroup={requirementGroup} filterBy={model.stringify} filterPlaceholder={t("common.components.table.filter_placeholder", { plural })}
                renderData={r => l(r.name)}
                render={r => <RequirementLabel noMemo selected={r.id === edited.id} lineage requirement={r} />} />


        </Sidebar>

        <Topbar>

            <Titlebar title={name || (name === undefined ? `<${t('common.labels.new')}>` : "")}>
                <Subtitle>{l(edited.description)}</Subtitle>
                {edited.predefined && <PredefinedLabel />}
                <TagList taglist={edited.tags} />
            </Titlebar>

            <Tab default id="info" icon={icns.form} name={t("common.labels.general")} badge={report.profile.errors() > 0} />
            <Tab disabled={isNew} id="layout" icon={icns.layout} name={t("common.labels.layout")} />
            {/* <Tab disabled={isNew} default id="usage" icon={icns.usage} name={t("common.labels.usage")} /> */}

            {tab === 'layout' && React.cloneElement(<div>{tocBtn}</div>, { style: { marginRight: 10 } })}


            {liveGuard}

            {saveBtn}
            {revertBtn}
            {cloneBtn}
            {branchBtn}
            {removeBtn}
            {rightsBtn}

        </Topbar>

        {tabcompo}

        <RouteGuard when={dirty} onOk={() => store.resetNext()} />

        <Permissions resourceCentric resourceRange={[edited]} filter={{ resources: [edited.id, any] }} />

    </Page>

}