import { Placeholder } from "#app/components/Placeholder";
import { iamType } from "#app/iam/constants";
import { IamSlot } from "#app/iam/module";
import { Module } from "#app/module/model";
import { pushEventType } from '#app/push/constants';
import { noTenant, tenantType } from "#app/tenant/constants";
import { TenantSlot } from '#app/tenant/module';
import { userType } from "#app/user/constants";
import { UserSlot } from "#app/user/module";
import { eventType } from "#event/constants";
import { EventSlot } from "#event/module";
import { requirementActions } from "./actions";
import { requirementIcon, requirementPlural, requirementSingular, requirementType } from "./constants";
import { RequirementLoader } from "./Loader";
import { usePushRequirementSlot } from './pushevents';
import { RequirementPermissions } from "./RequirementPermissions";


export const useRequirementModule = (): Module => {

    const pushRequirementSlot = usePushRequirementSlot()

    return {

        icon: requirementIcon,
        type: requirementType,

        nameSingular: requirementSingular,
        namePlural: requirementPlural,


        [tenantType]: {

            tenantResource: true


        } as TenantSlot

        ,

        [pushEventType]: pushRequirementSlot

        ,


        [iamType]: {

            actions: requirementActions,

        } as IamSlot,


        [userType]: {

            renderIf: ({ subjectRange }) => subjectRange?.some(s => s.tenant === noTenant)
            ,
            permissionComponent: props => <RequirementLoader placeholder={Placeholder.list}>
                <RequirementPermissions {...props} />
            </RequirementLoader>

        } as UserSlot

        ,

        [eventType]: {

            enable: true

        } as EventSlot

    
    }}