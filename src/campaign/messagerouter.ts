import { tenantType } from "#app/tenant/constants";
import { useLogged } from '#app/user/store';
import { dashboardRoute } from "#dashboard/constants";
import { recipientSeparator } from '#messages/VirtualFlow';
import { messageType } from "#messages/constants";
import { Message } from "#messages/model";
import { productType } from "#product/constants";
import { requirementType } from "#requirement/constants";
import { useCampaignCalls } from "./calls";
import { useMessageHelper } from './messagehelper';
import { usePartyInstanceCalls } from './party/calls';
import { useProductInstances } from './product/api';
import { useProductInstanceCalls } from './product/calls';
import { useRequirementInstances } from './requirement/api';
import { useRequirementInstanceCalls } from './requirement/calls';
import { submissionType } from './submission/constants';



export const useMessageRouter = () => {

    const messagehelper = useMessageHelper()

    const campaigncalls = useCampaignCalls()

    const reqinst = useRequirementInstances()
    const prodinst = useProductInstances()

    const reqinstcalls = useRequirementInstanceCalls()
    const prodinstcalls = useProductInstanceCalls()
    const partyinstcalls = usePartyInstanceCalls()


    const logged = useLogged()

    return {

        route: async (message: Message) => {

            // CPC users shouldn't see broadcasts narrowed to other CPCs v(cf.) message dynamics.)
            // the backend can't tell at the point of sending, so we filter them out at the point of receiving.
            const recipients = message.recipient?.split(recipientSeparator) ?? []

            const multirecipient = recipients.length > 1 

            if (multirecipient && logged.isTenantUser() && !recipients.some(t=>t===logged.tenant)){
                console.log(`suppresssing message because recipients narrowed to exclude ${logged.tenant}`)
                return undefined
            }


            const helper = messagehelper.message(message)

            const { campaign: cid, requirements: rids, products: pids, tenants: tids } = helper.extract()

            const tid = logged.isTenantUser() ? logged.tenant : tids[0]
            const aid = rids[0] ?? pids[0]
            const type = rids[0] ? requirementType : productType

            const instances = type == requirementType ? reqinst : prodinst

            const fetchCampaign = () => campaigncalls.fetchOne(cid).then(c => c.campaign)
            const fetchPartyInstance = () => partyinstcalls.on(cid).fetchOne(`${tid}@${cid}`)
            const fetchAssetInstance = () => (type === requirementType ? reqinstcalls : prodinstcalls).on(cid).fetchOne(`${aid}@${cid}`)

            const partyInAudience = async () => {

                const partyinstance = await fetchPartyInstance()

                const assetinstance = await fetchAssetInstance()

                const campaign = await fetchCampaign()

                return instances.on(campaign).isForParty(partyinstance, assetinstance)

            }

            const campaignroute = { route: `${dashboardRoute}/${cid}/${messageType}` }
            const assetroute = { route: `${dashboardRoute}/${cid}/${type}/${aid}/${messageType}` }
            const partyroute = { route: `${dashboardRoute}/${cid}/${tenantType}/${tid}/${messageType}` }
            const submissionroute = { route: `${dashboardRoute}/${cid}/${tenantType}/${tid}/${type}/${aid}/latest?tab=${messageType}` }

            // we analyse topics to figure out the board we should send the user to.
            // we move from the most specific topic to the least specific.


            // if it's about submissions, go that board.
            if (helper.includesTopic(submissionType)) {

                if (logged.hasNoTenant() || await partyInAudience())
                    return submissionroute

                console.log(`message notification is off-audience for ${logged.tenant}`)

            }

            // otherwise if it's about an asset,go to the asset board.
            else if (helper.includesTopic(productType) || helper.includesTopic(requirementType)) {

                if (logged.hasNoTenant())
                    return assetroute

                if (await partyInAudience())
                    return submissionroute

            }

            // otherwise if it's about a party, go to the party board.
            else if (helper.includesTopic(tenantType) && !multirecipient)
                return partyroute

            // otherwise sent to campaign board,.
            else {

                console.log('here')

                // makes sure party is in campaign, errors otherwise.
                if (logged.isTenantUser())
                    await fetchPartyInstance()

                return campaignroute

            }

        }
    }
}