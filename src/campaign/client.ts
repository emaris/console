import { useT } from '#app/intl/api'
import { defaultLanguage } from "#app/intl/model"
import { useSettings } from '#app/settings/store'
import { useToggleBusy } from "#app/system/api"
import { through } from "#app/utils/common"
import { notify, showAndThrow, showError, useFeedback } from "#app/utils/feedback"
import { useEventInstanceStore } from "#campaign/event/store"
import { useContext } from 'react'
import { useCampaignCalls } from "./calls"
import { campaignPlural, campaignRoute, campaignSingular, campaignType } from "./constants"
import { CampaignContext, CampaignNext } from './context'
import { Campaign, CampaignAppSettings, newCampaign, newCampaignId, unresolvedCampaign } from "./model"
import { usePartyInstanceClient } from './party/client'
import { useProductInstanceClient } from './product/client'
import { useRequirementInstanceClient } from './requirement/client'
import { useCampaignStore } from './store'
import { useSubmissionClient } from './submission/client'


export type PruneInstanceCheck = {
    predicate: (c: Campaign) => boolean
}




export const useCampaignClient = () => {

    const store = useCampaignStore()

    const t = useT()
    const toggleBusy = useToggleBusy()
    const fb = useFeedback()

    const calls = useCampaignCalls()
    
    const plural = t(campaignPlural).toLowerCase()
    const singular = t(campaignSingular).toLowerCase()

    const self = {
       
        areReady: () => !!store.all()


        ,


        fetchAll: (forceRefresh = false): Promise<Campaign[]> => {


            return self.areReady() && !forceRefresh ? Promise.resolve(store.all())

                :

                toggleBusy(`${campaignType}.fetchAll`, t("common.feedback.load", { plural }))

                    .then(_ => console.log(`fetching campaigns...`))
                    .then(_ => calls.fetchAll())
                    .then(through($ => store.setAll($, { noOverwrite: true })))


                    .catch(e => showAndThrow(e, t("common.calls.fetch_all_error", { plural })))
                    .finally(() => toggleBusy(`${campaignType}.fetchAll`))

        }

        ,

        fetchAllWith: (type: string, instanceOf: string) =>

            Promise.all([
                self.fetchAll(),
                calls.fetchWith({ type: type, id: instanceOf })
            ])
                .then(([campaigns, matches]) => matches.map(m => campaigns.find(c => c.id === m) || unresolvedCampaign(m)))
        ,

        removeAll: (list: string[], onConfirm?: () => void) =>

            fb.askConsent({


                title: t('common.consent.remove_many_title', { count: list.length, plural }),
                content: t('common.consent.remove_many_msg', { plural }),
                okText: t('common.consent.remove_many_confirm', { count: list.length, plural }),

                onOk: () => {


                    toggleBusy(`${campaignType}.removeAll`, t("common.feedback.save_changes"))

                        .then(_ => list.forEach(id => calls.delete(id)))
                        .then(_ => store.all().filter(u => !list.includes(u.id)))
                        .then(store.setAll)
                        .then(_ => notify(t('common.feedback.saved')))
                        .then(_ => onConfirm && onConfirm())

                        .catch(e => showAndThrow(e, t("common.calls.remove_one_error", { singular })))
                        .finally(() => toggleBusy(`${campaignType}.removeAll`))


                }
            })



    }



    return self;

}




export const useCampaignRouting = () => {

    const state = useContext(CampaignContext)

    const settings = useSettings().get<CampaignAppSettings>().campaign

    const self = {


        routeTo: (campaign: Campaign) => campaign ? `${campaignRoute}/${campaign.id}` : undefined!


        ,

        next: (): CampaignNext => state.get().campaigns.next ?? { source: newCampaign(settings), type: 'new' }

        ,

        resetNext: () => self.setNext(undefined)

        ,

        setNext: (context: CampaignNext | undefined) => state.set(s => s.campaigns.next = context)


    }

    return self
}

export const useCampaignDetail = () => {

    const t = useT()
    const toggleBusy = useToggleBusy()
    const fb = useFeedback()

    const calls = useCampaignCalls()

    const store = useCampaignStore()
    const partyinstclient = usePartyInstanceClient()
    const reqinstclient = useRequirementInstanceClient()
    const prodinstclient = useProductInstanceClient()
    const eventinststore = useEventInstanceStore()
    const subclient = useSubmissionClient()

    const singular = t(campaignSingular).toLowerCase()

    const self = {


        fetchOne: (campaign: Campaign, forceRefresh?: boolean): Promise<Campaign> => {

            const archived = campaign.lifecycle.state === 'archived'
            const t0 = performance.now()

            return toggleBusy(`${campaignType}.fetchOne`, archived ? t("campaign.feedback.load_archive") : t("common.feedback.load_one", { singular }))

                .then(_ => calls.fetchOne(campaign.id))
                .then(({ campaign, archive }) => {

                    if (archived)
                        store.setArchive(campaign, archive)

                    return campaign

                })
                .then(through(fetched =>

                    // if no instance is mocked, then we can afford a more parallel approach.
                    Promise.all([
                        partyinstclient.on(fetched).fetchAll(forceRefresh),
                        reqinstclient.on(fetched).fetchAll(forceRefresh),
                        prodinstclient.on(fetched).fetchAll(forceRefresh),
                        eventinststore.on(fetched).fetchAll(forceRefresh),
                        subclient.on(fetched).fetchAllTrails(forceRefresh)
                    ])

                ))

                .then(through(fetched => { if (!fetched) throw Error(`Invalid resource lookup for ${campaign.id}, returned undefined.`) }))
                .then(fetched => {
                    const time = performance.now() - t0
                    console.log(`loading campaign took ${time}`)
                    return { ...fetched, loaded: true }
                })   // interning campaign
                .then(through(fetched => store.setAll(store.all().map(u => u.id === campaign.id ? fetched : u))))

                .catch(e => showAndThrow(e, t("common.calls.fetch_one_error", { singular })))
                .finally(() => toggleBusy(`${campaignType}.fetchOne`))


        }


        ,


        save: (campaign: Campaign, context: CampaignNext): Promise<Campaign> => {


            const updateCampaign = () => calls.update(campaign)
                .then(({ campaign, archive }) => {

                    if (campaign.lifecycle.state === 'archived')
                        store.setArchive(campaign, archive)

                    return campaign

                })
                .then(saved => ({ ...saved, loaded: true }))
                .then(through(saved => store.setAll(store.all().map(c => c.id === campaign.id ? saved : c))))



            const addCampaign = () => calls.add({ ...campaign, id: newCampaignId() })
                .then(through(saved => store.setAll([saved, ...store.all()])))
                .then(through(saved => context.type === 'new' ?

                    eventinststore.on(saved).addManagedForCampaign()
                    :

                    self.cloneInstances(saved, context)))




            return toggleBusy(`${campaignType}.save`, t("common.feedback.save_changes"))


                .then(() => campaign.id ? updateCampaign() : addCampaign())

                .then(through(() => notify(t('common.feedback.saved'))))
                .catch(e => showAndThrow(e, t("common.calls.save_one_error", { singular })))
                .finally(() => toggleBusy(`${campaignType}.save`))
        }

        ,


        cloneInstances: (to: Campaign, context: CampaignNext) => {

            const { lineage } = context

            const from = lineage!

            console.log(`cloning instances from ${from.name[defaultLanguage]} into ${to.name[defaultLanguage]}`)


            return toggleBusy(`clone.campaign.${to.id}`, t("campaign.feedback.clone_resources"))

                .then(_ => Promise.all([

                    partyinstclient.on(from).fetchAll().then(all => partyinstclient.on(to).cloneAll(all, context)),
                    reqinstclient.on(from).fetchAll().then(all => reqinstclient.on(to).cloneAll(all, context)),
                    prodinstclient.on(from).fetchAll().then(all => prodinstclient.on(to).cloneAll(all, context)),
                    eventinststore.on(from).fetchAll().then(all => eventinststore.on(to).cloneAll(all, context))



                ]))
                .catch(e => showError(e, { title: t("campaign.feedback.clone_error", { singular }) }))
                .finally(() => toggleBusy(`clone.campaign.${to.id}`))


        }

        ,

        remove: (id: string, onConfirm: () => void, challenge?: boolean) =>

            fb.askConsent({

                title: t('common.consent.remove_one_title', { singular }),
                content: t('common.consent.remove_one_msg', { singular }),
                okText: t('common.consent.remove_one_confirm', { singular }),

                okChallenge: challenge ? t('common.consent.remove_challenge', { singular }) : undefined,

                onOk: () => {


                    toggleBusy(`${campaignType}.removeOne`, t("common.feedback.save_changes"))

                        .then(_ => calls.delete(id))
                        .then(_ => store.all().filter(u => id !== u.id))
                        .then(store.setAll)
                        .then(_ => notify(t('common.feedback.saved')))
                        .then(_ => onConfirm && onConfirm())

                        .catch(e => showAndThrow(e, t("common.calls.remove_one_error", { singular })))
                        .finally(() => toggleBusy(`${campaignType}.removeOne`))


                }
            }),


    }

    return self

}