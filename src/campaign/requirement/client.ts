
import { useT } from '#app/intl/api';
import { defaultLanguage } from "#app/intl/model";
import { useToggleBusy } from "#app/system/api";
import { through } from "#app/utils/common";
import { notify, showAndThrow, useFeedback } from "#app/utils/feedback";
import { CampaignNext } from '#campaign/context';
import { useRequirementClient } from '#requirement/client';
import { requirementPlural, requirementSingular } from "#requirement/constants";
import { useEventInstanceStore } from '../event/store';
import { Campaign } from "../model";
import { useRequirementInstanceCalls } from "./calls";
import { RequirementInstance } from "./api";
import { useRequirementInstances } from './api';



export type RequirementInstanceState = {

    requirements: {
        all: RequirementInstance[]
        map: Record<string, RequirementInstance>
        sourceMap: Record<string, RequirementInstance>
    }

}

export const initialRequirementInstances = (): RequirementInstanceState => ({

    requirements: {
        all: undefined!,
        map: undefined!,
        sourceMap: undefined!
    }
})


export const useRequirementInstanceClient = () => {


    const store = useRequirementInstances()

    const t = useT()
    const fb = useFeedback()
    const toggleBusy = useToggleBusy()
        
    const singular = t(requirementSingular).toLowerCase()
    const plural = t(requirementPlural).toLowerCase()

    const type = "reqinstance"

    const reqclient = useRequirementClient()   

    const reqinstcalls = useRequirementInstanceCalls()

    const eventinststore = useEventInstanceStore()

    
    return  {

        on: (c: Campaign) => {

            const cstore = store.on(c)
        
            const self = {

                addAll: (instances: RequirementInstance[]) =>

                    toggleBusy(`${type}.addall`, t("common.feedback.save_changes"))

                        .then(_ => reqinstcalls.on(c).addAll(instances))
                        .then(through(added => cstore.setAll([...cstore.all(), ...added])))
                        // notifies the event system to generate automanaged events.
                        .then(through(added => eventinststore.on(c).addManagedForInstances(added)))
                        .then(through(() => notify(t('common.feedback.saved'))))

                        .catch(e => showAndThrow(e, t("common.calls.add_many_error", { plural })))
                        .finally(() => toggleBusy(`${type}.addall`))

                ,

                cloneAll: (instances: RequirementInstance[], context: CampaignNext) => reqinstcalls.on(c).addAll(instances.map(i => cstore.clone(i, context))).then(cstore.setAll)

                ,

                areReady: () => !!cstore.all()

                ,

                fetchAll: (forceRefresh = false) =>

                    Promise.all([

                        reqclient.fetchAll(),

                        self.areReady() && !forceRefresh ? Promise.resolve(cstore.all())

                            :

                            toggleBusy(`${type}.fetchAll.${c.id}`, t("common.feedback.load", { plural }))

                                .then(_ => console.log(`fetching requirements for ${c.name[defaultLanguage]}...`))
                                .then(reqinstcalls.on(c).fetchAll) // load template dependency in parallel

                                .then(through($ => cstore.setAll($, { noOverwrite: !forceRefresh })))


                                .catch(e => showAndThrow(e, t("common.calls.fetch_all_error", { plural })))
                                .finally(() => toggleBusy(`${type}.fetchAll.${c.id}`))

                    ])
                        .then(([, instances]) => instances)  // continue with instances only

                ,

                save: (instance: RequirementInstance, replacementOf: RequirementInstance = instance) =>

                    toggleBusy(`${type}.updateOne`, t("common.feedback.save_changes"))

                        .then(() => reqinstcalls.on(c).updateOne(instance))
                        .then(() => cstore.all().map(p => p.id === replacementOf.id ? instance : p))
                        .then(cstore.setAll)
                        .then(() => notify(t('common.feedback.saved')))

                        .catch(e => showAndThrow(e, t("common.calls.update_one_error", { singular })))
                        .finally(() => toggleBusy(`${type}.updateOne`))
                ,

                remove: (instance: RequirementInstance, onConfirm?: (...args) => void) =>

                    fb.askConsent({

                        title: t('common.consent.remove_one_title', { singular }),
                        content: t("common.consent.remove_one_msg", { singular }),
                        okText: t("common.consent.remove_one_confirm", { singular }),

                        onOk: () => {


                            toggleBusy(`${type}.removeOne`, t("common.feedback.save_changes"))

                                .then(_ => reqinstcalls.on(c).removeOne(instance))
                                .then(() => cstore.all().filter(p => instance.id !== p.id))
                                .then(cstore.setAll)
                                .then(() => notify(t('common.feedback.saved')))
                                .then(() => onConfirm && onConfirm(instance))

                                .catch(e => showAndThrow(e, t("common.calls.remove_one_error", { singular })))
                                .finally(() => toggleBusy(`${type}.removeOne`))


                        }
                    })

                ,


                removeMany: (instances: RequirementInstance[], onConfirm?: () => void) =>

                    fb.askConsent({

                        title: t('common.consent.remove_many_title', { count: instances.length, plural }),
                        content: t("common.consent.remove_many_msg", { count: instances.length, plural }),
                        okText: t("common.consent.remove_many_confirm", { count: instances.length, plural }),

                        onOk: () => {

                            const ids = instances.map(p => p.id)

                            toggleBusy(`${type}.removeMany`, t("common.feedback.save_changes"))

                                .then(_ => reqinstcalls.on(c).removeMany(instances))
                                .then(() => cstore.all().filter(p => !ids.includes(p.id)))
                                .then(cstore.setAll)
                                .then(() => notify(t('common.feedback.saved')))
                                .then(through(() => onConfirm && onConfirm()))

                                .catch(e => showAndThrow(e, t("common.calls.remove_many_error", { plural })))
                                .finally(() => toggleBusy(`${type}.removeMany`))

                        }
                    })


            }

            return self
        }
    }
}


