
import { requirementPlural, requirementSingular, requirementType } from "#requirement/constants"
import { useRequirementStore } from '#requirement/store'
import { useT } from '#app/intl/api'
import { useTagStore } from '#app/tag/store'
import { tenantPlural } from "#app/tenant/constants"
import { userPlural } from '#app/user/constants'
import { check, checkIt, empty, ValidationCheck, withReport } from "#app/utils/validation"
import { Campaign } from "../model"
import { RequirementInstance } from "./api"


export const useRequirementInstanceValidation = () => {

    const reqstore = useRequirementStore()
    const t = useT()

    const tags = useTagStore()

    return {
        on: (c: Campaign) => ({

            validateInstance: (edited: RequirementInstance) => {

                const { validateCategories } = tags
                const { all } = reqstore

                const singular = t(requirementSingular).toLowerCase()
                const plural = t(requirementPlural).toLowerCase()

                const targetExists: ValidationCheck<any> = {

                    predicate: target => !all().find(i => i.id === target),
                    msg: () => "campaign.feedback.missing"
                }

                const partyPlural = t(tenantPlural).toLowerCase()

                return withReport({

                    note: checkIt().nowOr(t("campaign.fields.note.msg"))

                    ,

                    source_editable: check(edited.source!)
                        .with(empty(t))
                        .with(targetExists)
                        .nowOr(t("campaign.fields.source.msg"))

                    ,


                    source_readonly: checkIt().nowOr(t("campaign.fields.source.msg", { singular }))
                    ,

                    lineage: checkIt().nowOr(t("common.fields.lineage.msg_nochoice", { singular }))

                    ,

                    audience: checkIt().nowOr(
                        t("common.fields.audience.msg"),
                        t("common.fields.audience.help", { singular, plural: partyPlural })
                    ),

                    userProfile: checkIt().nowOr(
                        t("common.fields.user_profile.msg"),
                        t("common.fields.user_profile.help", { singular, plural: userPlural })
                    )

                    ,

                    editable: checkIt().nowOr(
                        t("common.fields.editable.help", { plural, parties: partyPlural })
                    )

                    ,

                    versionable: checkIt().nowOr(
                        t("common.fields.versionable.help", { plural, parties: partyPlural })
                    )

                    ,

                    assessed: checkIt().nowOr(
                        t("common.fields.assessed.help", { plural })
                    )

                    ,

                    ...validateCategories(edited.tags).for(requirementType),

                    ...validateCategories(edited.properties.submissionTagMap?.flatMap(tagmap => tagmap.tags)).include(...(edited.properties.submissionTagMap?.map(tagmap => tagmap.category) ?? []))

                })

            }
        })
    }
}