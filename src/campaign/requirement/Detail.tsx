
import { Button } from "#app/components/Button"
import { Drawer } from "#app/components/Drawer"
import { Form } from "#app/form/Form"
import { NoteBox } from "#app/form/NoteBox"
import { SimpleBox } from "#app/form/SimpleBox"
import { Switch } from "#app/form/Switch"
import { VSelectBox } from "#app/form/VSelectBox"
import { FormState, useFormState } from "#app/form/hooks"
import { icns } from "#app/icons"
import { useT } from '#app/intl/api'
import { instanceFrom } from '#layout/model'
import { paramIcon } from '#layout/parameters/model'
import { LayoutProvider } from '#layout/provider'
import { useLocale } from "#app/model/hooks"
import { Tab } from '#app/scaffold/Tab'
import { Topbar } from "#app/scaffold/Topbar"
import { BytestreamedContext } from '#app/stream/BytestreamedHelper'
import { TagBoxset } from "#app/tag/TagBoxset"
import { TagMapBox } from '#app/tag/TagMapBox'
import { TagRefBox } from "#app/tag/TagRefBox"
import { AudienceList } from "#app/tenant/AudienceList"
import { tenantIcon, tenantType } from "#app/tenant/constants"
import { userType } from '#app/user/constants'
import { useLogged } from '#app/user/store'
import { through } from "#app/utils/common"
import { paramsInQuery } from '#app/utils/routes'
import { AssetOverlayForm } from '#campaign/AssetOverlayForm'
import { EventList } from "#campaign/event/EventList"
import { EventInstance } from '#campaign/event/model'
import { useEventInstanceDates, useEventInstanceStore } from '#campaign/event/store'
import { useCurrentCampaign } from "#campaign/hooks"
import { RequirementInstanceLabel } from "#campaign/requirement/Label"
import { RequirementInstance, useRequirementInstances } from "#campaign/requirement/api"
import { useRequirementInstanceValidation } from '#campaign/requirement/validation'
import { submissionType } from "#campaign/submission/constants"
import { useDashboard } from '#dashboard/api'
import { predefinedRequirementParameterId } from '#layout/parameters/constants'
import { RequirementDetailLoader } from '#requirement/DetailLoader'
import { RequirementLabel } from "#requirement/Label"
import { requirementType } from "#requirement/constants"
import { Requirement } from "#requirement/model"
import { useRequirementStore } from '#requirement/store'
import * as React from "react"
import { useHistory, useLocation } from "react-router-dom"
import { useRequirementInstanceClient } from './client'

import { useRoutedTypes } from '#config/model'
import { useRequirementLayoutInitialiser } from '#requirement/layoutinitialiser'
import './styles.scss'


type Props = {

    detail: RequirementInstance
    onClose: () => void

    width?: number
}


export const RequirementInstanceDetail = (props: Props) => {

    const { detail } = props

    return <RequirementDetailLoader id={detail.source}>{

        () => <InnerRequirementInstanceDetail {...props} />

    }</RequirementDetailLoader>
}

const InnerRequirementInstanceDetail = (props: Props) => {

    const t = useT()
    const { l } = useLocale()
    const history = useHistory()
    const { search } = useLocation()

    const campaign = useCurrentCampaign()


    const routedTypes = useRoutedTypes()
    const dashboard = useDashboard()

    const { width = 650, detail, onClose } = props

    const reqinststore = useRequirementInstances().on(campaign)
    const reqinstclient = useRequirementInstanceClient().on(campaign)
    const reqinstvalidation = useRequirementInstanceValidation().on(campaign)

    const eventinststore = useEventInstanceStore().on(campaign)
    const eventinsthelper = useEventInstanceDates().on(campaign)

    const sources = useRequirementStore()
    // existing events about this detail, including temporals.
    const initialRelatedEvents = eventinststore.allAboutWithTemporals(detail.source)

    type FormData = { instance: RequirementInstance, relatedEvents: EventInstance[] }

    const formstate = useFormState<FormData>({ instance: detail, relatedEvents: initialRelatedEvents })

    const { edited, initial, dirty, change, reset, set, softReset } = formstate

    // resyncs on external changes (push events)
    // eslint-disable-next-line
    React.useEffect(() => set({ ...edited, relatedEvents: initialRelatedEvents }), [eventinststore.all()])

    const { instance, relatedEvents } = formstate.edited

    const onScreenEvents = dirty ? relatedEvents : [...relatedEvents].sort(eventinsthelper.temporalAwareDateComparatorOver(relatedEvents))

    const source = sources.safeLookup(edited.instance.source)!

    const liveBtn = <Button enabledOnReadOnly iconLeft icn={icns.edit} onClick={() => history.push(dashboard.on(campaign).routeToAsset(edited.instance))}>{t("campaign.labels.campaign_view.live")}</Button>

    const onSave = () => eventinststore.replaceAllAbout(onScreenEvents, detail.source)
        .then(through(() => reqinstclient.save(instance, initial.instance)))
        .then(relatedEvents => softReset({ ...edited, relatedEvents }))

    const revert = <Button icn={icns.revert} enabled={dirty} onClick={() => reset()}>{t("common.buttons.revert")}</Button>
    const saveact = <Button icn={icns.save} enabled={dirty} type="primary" onClick={onSave}>{t("common.buttons.save")}</Button>

    const report = reqinstvalidation.validateInstance(instance)

    const { tab } = paramsInQuery(search)

    const tabcompo = (() => {

        switch (tab) {

            case 'overlay': return (

                <BytestreamedContext>
                    <LayoutProvider useInitialiser={useRequirementLayoutInitialiser} layout={instanceFrom(source.properties.layout)} onChange={() => {/* no real change intended here, only overlays. */ }}>
                        <AssetOverlayForm
                            instance={edited.instance}
                            excludes={p => predefinedRequirementParameterId.includes(p.id)}
                            onChange={change((t, v) => t.instance = v)} />
                    </LayoutProvider>
                </BytestreamedContext>

            )

            default: return <GeneralForm report={report} {...formstate} />
        }
    })()

    return <Drawer readonly={!edited.instance.source} renderAfterTransition routeId={reqinststore.detailParam()} visible width={width} warnOnClose={dirty} title={t("common.buttons.edit_one", { singular: l(source?.name) })} icon={tenantIcon} onClose={onClose}>

        <Topbar offset={63}>

            <Tab default id={"info"} icon={icns.form} name={t("common.labels.general")} badge={report.errors() > 0} />
            <Tab id={'overlay'} icon={paramIcon} name={t('layout.parameters.plural')} />

            {saveact}
            {revert}
            {routedTypes.includes(submissionType) && liveBtn}

        </Topbar>


        {tabcompo}

    </Drawer>
}

type FormProps = FormState<{ instance: RequirementInstance, relatedEvents: EventInstance[] }> & {

    report: any
}

const GeneralForm = (props: FormProps) => {


    const { edited, change, report, dirty, initial } = props

    const { instance, relatedEvents } = edited

    const t = useT()

    const { l } = useLocale()

    const logged = useLogged()

    const campaign = useCurrentCampaign()

    const sources = useRequirementStore()

    const source = sources.safeLookup(edited.instance.source)!

    const reqinststore = useRequirementInstances().on(campaign)

    const events = useEventInstanceDates().on(campaign)

    const onScreenEvents = dirty ? relatedEvents : [...relatedEvents].sort(events.temporalAwareDateComparatorOver(relatedEvents))


    // bizrule: an instance can be "re-targeted", associated with a later version of its source,
    // provided that version is selected for this campaign. 
    // this is typically required when the campaign is cloned from an older one whilst requirements have been superseeded.

    const isViableDescendant = (r: Requirement) => r.lineage?.includes(initial.instance.source) && !reqinststore.all().map(i => i.source).includes(r.id)

    const descendants = sources.all().filter(r => r.id === initial.instance.source || isViableDescendant(r))


    return <Form state={props} >

        <EventList instance={instance} events={onScreenEvents} onChange={change((e, newevents) => { e.relatedEvents = newevents })} />

        <TagRefBox mode="multi" label={t("common.fields.audience.name")} validation={report.audience} expression={edited.instance.audience} type={tenantType} onChange={change((t, v) => t.instance.audience = v)} />

        <AudienceList label={t("common.fields.audience_list.name")} validation={report.audienceList} onChange={change((t, v) => t.instance.audienceList = v)}>{edited.instance.audienceList}</AudienceList>

        <TagRefBox mode="multi" label={t("common.fields.user_profile.name")} validation={report.userProfile} expression={edited.instance.userProfile} type={userType} onChange={change((t, v) => t.instance.userProfile = v)} />

        <TagBoxset edited={instance.tags} type={requirementType} validation={report} onChange={change((t, v) => t.instance.tags = v)} />

        <TagMapBox type={submissionType} className="instance-tag-boxes" label={t("requirement.fields.tagmap.label")} singleLabel={t("requirement.fields.tagmap.add")} validationReport={report} onChange={change((t, v) => t.instance.properties.submissionTagMap = v)}>{edited.instance.properties.submissionTagMap}</TagMapBox>

        <NoteBox label={t("common.fields.note.name")} validation={report.note} autoSize={{ minRows: 4, maxRows: 6 }} onChange={change((t, v) => t.instance.properties.note = t.instance.properties.note ? { ...t.instance.properties.note, [logged.tenant]: v } : { [logged.tenant]: v })} >
            {instance.properties.note}
        </NoteBox>


        {descendants.length > 1 ?

            <VSelectBox label={t("campaign.fields.source.name")}
                validation={report.source_editable}
                lblTxt={r => l(r.name)}
                options={descendants}
                onChange={change((t, v) => t.instance.source = v.id)}
                renderOption={r => <RequirementLabel noLink requirement={r} />}
                optionId={r => r.id}>

                {[source]}

            </VSelectBox>

            :

            <SimpleBox label={t("campaign.fields.source.name")} validation={report.source_readonly} >
                <RequirementLabel requirement={instance.source} />
            </SimpleBox>

        }

        {instance.lineage &&

            <SimpleBox label={t("common.fields.lineage.name")} validation={report.lineage} >
                <RequirementInstanceLabel showCampaign instance={instance.lineage} />
            </SimpleBox>

        }

        <Switch label={t("common.fields.editable.name")} onChange={change((u, v) => u.instance.properties.editable = v)} validation={report.editable}>
            {edited.instance.properties.editable !== undefined ? edited.instance.properties.editable : true}
        </Switch>


        <Switch label={t("common.fields.versionable.name")} onChange={change((u, v) => u.instance.properties.versionable = v)} validation={report.versionable}>
            {edited.instance.properties.versionable !== undefined ? edited.instance.properties.versionable : true}
        </Switch>

        {
            campaign.properties.complianceScale &&
            <Switch label={t("common.fields.assessed.name")} onChange={change((u, v) => u.instance.properties.assessed = v)} validation={report.assessed}>
                {edited.instance.properties.assessed !== undefined ? edited.instance.properties.assessed : true}
            </Switch>
        }


    </Form>


}