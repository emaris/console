import { useCalls } from "#app/call/call";
import { domainService } from "../../constants";
import { Campaign } from "../model";
import { RequirementInstance } from "./api";

const requirements = `/requirementinstance`


export const useRequirementInstanceCalls = () => {

    const { at } = useCalls()

    return {

        on: (c: Campaign | string) => {

            const cid = typeof c === 'string' ? c : c.id

            return {


                fetchAll: (): Promise<RequirementInstance[]> => at(`${requirements}/search`, domainService).post({ campaign: cid })

                ,

                fetchOne: (id: string): Promise<RequirementInstance> => at(`${requirements}/${id}?byref=true`, domainService).get()

                ,

                addAll: (instances: RequirementInstance[]): Promise<RequirementInstance[]> => at(requirements, domainService).post(instances)
                ,

                updateOne: (instance: RequirementInstance): Promise<void> => at(`${requirements}/${instance.id}`, domainService).put(instance)

                ,

                removeOne: (instance: RequirementInstance): Promise<void> => at(`${requirements}/${instance.id}`, domainService).delete()

                ,

                removeMany: (instances: RequirementInstance[]): Promise<void> => at(`${requirements}/remove`, domainService).post(instances.map(i => i.id))

            }
        }
    }
}

