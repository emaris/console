
import { Button, ButtonGroup } from '#app/components/Button';
import { useListState } from "#app/components/hooks";
import { Label } from "#app/components/Label";
import { NoSuchRoute } from "#app/components/NoSuchRoute";
import { Column, VirtualTable } from "#app/components/VirtualTable";
import { icns } from '#app/icons';
import { useT } from '#app/intl/api';
import { useLocale } from "#app/model/hooks";
import { useTagFilter, useTagHolderFilter } from '#app/tag/filter';
import { TagList } from "#app/tag/Label";
import { tagRefsIn } from "#app/tag/model";
import { useTagStore } from '#app/tag/store';
import { tenantPlural } from "#app/tenant/constants";
import { userPlural } from '#app/user/constants';
import { useEventInstanceStore } from '#campaign/event/store';
import { useCurrentCampaign } from '#campaign/hooks';
import { RequirementInstanceLabel } from "#campaign/requirement/Label";
import { RequirementInstance } from "#campaign/requirement/api";
import { useRequirementInstances } from '#campaign/requirement/api';
import { CampaignValidation } from '#campaign/validation';
import { eventIcon, eventPlural } from "#event/constants";
import { requirementIcon, requirementPlural, requirementType } from "#requirement/constants";
import { RequirementLabel } from "#requirement/Label";
import { Requirement, useRequirementModel } from "#requirement/model";
import { useRequirementStore } from '#requirement/store';
import * as React from "react";
import { useHistory } from "react-router-dom";
import { usePicker } from "../Picker";
import '../styles.scss';
import { useRequirementInstanceClient } from './client';
import { RequirementInstanceDetail } from "./Detail";

type Props = {

    report: ReturnType<CampaignValidation['validateRequirements']>
}

export const RequirementInstanceList = (props: Props) => {

    const campaign = useCurrentCampaign()

    const reqinststore = useRequirementInstances().on(campaign)

    const instanceId = reqinststore.detailInRoute()

    const detail = reqinststore.lookup(instanceId)

    return instanceId && !detail ? <NoSuchRoute /> : <InnerList {...props} detail={detail!} />
}

const InnerList = (props: Props & { detail: RequirementInstance }) => {


    const t = useT()
    const { l } = useLocale()

    const history = useHistory()

    const tags = useTagStore()
    const store = useRequirementStore()
    const model = useRequirementModel()

    const campaign = useCurrentCampaign()
    const eventInstances = useEventInstanceStore().on(campaign)

    const readOnly = campaign.guarded

    const { report, detail } = props

    const { Picker, pickerVisible, setPickerVisible } = usePicker<Requirement>()

    const reqinst = useRequirementInstances().on(campaign)
    const reqinstclient = useRequirementInstanceClient().on(campaign)
    
    const liststate = useListState<RequirementInstance>()

    const { selected, resetSelected, removeOne } = liststate

    const plural = t(requirementPlural)

    const editBtn = (p: RequirementInstance) => <Button
        key="edit"
        onClick={() => history.push(reqinst.route(p))} >
        {t("common.buttons.open")}
    </Button>
    
    const removeBtn = (p: RequirementInstance) => <Button
        key="remove"
        disabled={readOnly}
        onClick={() => reqinstclient.remove(p, removeOne)}>
        {t("common.buttons.remove")}
    </Button>

    const addBtn = <Button
        icn={icns.add}
        type="primary"
        disabled={readOnly}
        onClick={() => setPickerVisible(true)}>
        {t("common.buttons.add_many", { plural })}
    </Button>

    const removeSelectedBtn = <Button
        type="danger"
        disabled={readOnly}
        enabled={selected.length >= 1}
        onClick={() => reqinstclient.removeMany(selected, resetSelected)}>
        {t("common.buttons.remove_many", { count: selected.length })}
    </Button>

    const unfilteredData = reqinst.all()

   // eslint-disable-next-line
    const sortedData = React.useMemo(() => reqinst.allSorted(), [unfilteredData])

    const requirementinstanceGroup = `${campaign.id}-${requirementType}`

    const { TagFilter, tagFilteredData } = useTagFilter({
        filtered: sortedData,
        group: requirementinstanceGroup,
        key: `tags`
    })

    const { TagFilter: AudienceFilter, tagFilteredData: audienceFilteredData } = useTagHolderFilter({
        filtered: tagFilteredData,
        tagged: unfilteredData,
        tagsOf: t => t.audience?.terms.flatMap(t => t.tags),
        placeholder: t("common.fields.audience.name"),
        group: requirementinstanceGroup,
        key: `audience`
    })

    const {TagFilter:UserProfileFilter,tagFilteredData: userProfileFilteredData} = useTagHolderFilter ({
        filtered:audienceFilteredData, 
        tagged: unfilteredData,
        tagsOf: t=>t.userProfile?.terms.flatMap(t => t.tags),
        placeholder:t("common.fields.user_profile.name"),
        group: requirementinstanceGroup,
        key: `profile`
    })

    const data = userProfileFilteredData

    const rowValidation = ({ rowData: requirement }) => report[requirement.id].status === "error" && icns.error(report[requirement.id].msg)

    const unfilteredTemplates = store.all()

    // eslint-disable-next-line
    const activeTemplates = React.useMemo(() => store.all().sort(model.comparator).filter(r => r.lifecycle.state !== 'inactive'), [unfilteredTemplates])


    const eventCountLabel = (ri: RequirementInstance) => {

        const thisEventInstances = eventInstances.allAboutWithTemporals(ri.source)
        const someMissing = thisEventInstances.some(ei => !ei.date)

        return <Label
            icon={eventIcon}
            title={thisEventInstances.length}
            className={someMissing ? 'event-warn' : ''}
            tipPlacement='topRight'
            tip={() => <span>{t("event.no_date_set")}</span>}
            noTip={!someMissing} />
    }

    return (<>
        <VirtualTable id="reqinst" debug data={data} total={sortedData.length} state={liststate} selectAll
            filters={[UserProfileFilter, AudienceFilter, TagFilter]}
            decorations={[<ButtonGroup>{addBtn}{removeSelectedBtn}</ButtonGroup>]}
            filterGroup={requirementinstanceGroup} filterBy={reqinst.stringify} filterPlaceholder={t("common.components.table.filter_placeholder", { plural: plural.toLowerCase() })}
            onDoubleClick={p => history.push(reqinst.route(p))}
            refreshFlag={selected}
            actions={p => [editBtn(p), removeBtn(p)]}>

            <Column<RequirementInstance> flexGrow={1} title={t("common.fields.name_multi.name")} dataKey="name" decorations={[rowValidation]}
                dataGetter={r => l(store.safeLookup(r.source)!.name)}
                cellRenderer={({ rowData: r }) => <RequirementInstanceLabel tipTitle noLineage noMemo lineage instance={r} />} />

            <Column<RequirementInstance> sortable={false} title={t("common.fields.audience.name")} dataKey="audience" dataGetter={(p: RequirementInstance) =>{
                const audienceTerms = (p.audience?.terms.length ?? 0) === 0
                const audienceListIncludes = (p.audienceList?.includes.length ?? 0) === 0
                const audienceListExcludes = (p.audienceList?.excludes.length ?? 0) === 0
                return (audienceTerms && audienceListIncludes && audienceListExcludes) ? <Label title={ t(tenantPlural) } /> : <Label title={t("common.labels.restricted")} />

            }
            } />

            <Column<RequirementInstance> sortable={false} title={t("common.fields.user_profile.name")} dataKey="userprofile" dataGetter={(p: RequirementInstance) =>

                (p.userProfile?.terms.length ?? 0) > 0 ? <TagList taglist={tagRefsIn(p.userProfile)} /> : <Label noIcon title={t(userPlural)} />
            } />

            {tags.allTagsOf(requirementType).length > 0 &&
                <Column<RequirementInstance> flexGrow={2} sortable={false} title={t("common.fields.tags.name")} dataKey="tags"
                    dataGetter={i => i.tags} cellRenderer={({ rowData: ri }) => <TagList truncateEllipsisLink={reqinst.route(ri)} taglist={ri.tags} />}
                />
            }

            <Column<RequirementInstance> width={60} sortable={false} title={t(eventPlural)} dataKey="source"
                dataGetter={i => i.source} cellRenderer={({ rowData: ri }) => eventCountLabel(ri)} />

        </VirtualTable>


        {
            pickerVisible &&

            <Picker id='reqinst' title={plural} icon={requirementIcon}
                filterGroup={`${requirementinstanceGroup}-picker`}
                resources={activeTemplates}
                selected={sortedData.map(i => i.source)}
                onSelection={added => Promise.resolve(reqinstclient.addAll(added.map(reqinst.generate as any)))}
                filterWith={(filter, r) => l(r.name).toLocaleLowerCase().includes(filter.toLocaleLowerCase())}>

                <Column title={t("common.fields.name_multi.name")} flexGrow={1} dataKey="name" dataGetter={(r: Requirement) => l(r.name)} cellRenderer={cell => <RequirementLabel noLink tipTitle lineage requirement={cell.rowData} />} />
                <Column title={t("common.fields.tags.name")} dataKey="tags" dataGetter={(r: Requirement) => <TagList truncateEllipsisLink={store.routeTo(r)} taglist={r.tags} />} flexGrow={1} />

            </Picker>

        }

        { detail &&

            <RequirementInstanceDetail detail={detail} onClose={() => history.push(reqinst.route())} />}


    </>
    )
}