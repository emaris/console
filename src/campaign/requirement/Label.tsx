import { Label, sameLabel, UnknownLabel } from "#app/components/Label"
import { icns } from "#app/icons"
import { useT } from '#app/intl/api'
import { TimeLabel } from "#app/time/Label"
import { useEventInstanceStore } from '#campaign/event/store'
import { useCampaignStore } from '#campaign/store'
import { useRoutedTypes } from '#config/model'
import { useDashboard } from '#dashboard/api'
import { useCampaignMode } from "#dashboard/hooks"
import { requirementRoute } from "#requirement/constants"
import { RequirementLabel } from "#requirement/Label"
import { Tooltip } from "antd"
import React from "react"
import { Link } from "react-router-dom"
import { campaignSingular, campaignType, runningIcon } from "../constants"
import { InstanceLabel, InstanceLabelProps } from "../InstanceLabel"
import { CampaignLabel } from "../Label"
import { InstanceRef, isInstanceRef } from "../model"
import { submissionType } from "../submission/constants"
import { RequirementInstance, useRequirementInstances } from "./api"
import { isAbsolute } from "#campaign/event/model"

type Props = InstanceLabelProps & {

    instance: RequirementInstance | InstanceRef | undefined
    lineage?: boolean
    deadlineOnly?: boolean
    deadlineMode?: 'default' | 'relative'
    showTitle?: boolean
    tipTitle?: boolean
}

const emptySpace = <div style={{ width: 13 }} />

export const RequirementInstanceLabel = React.memo((props: Props) => {

    const { instance, options = [], deadlineOnly, deadlineMode, linkTo, linkTarget, showTitle = false, tipTitle, ...rest } = props

    let noVersion = false

    const t = useT()
    const routedTypes = useRoutedTypes()

    const dashboard = useDashboard()
    const reqinst = useRequirementInstances()
    const eventinst = useEventInstanceStore()
    const campaignstore = useCampaignStore()
    const campaignMode = useCampaignMode()

    const campaign = campaignstore.lookup(instance?.campaign)

    if (!campaign)
        return <UnknownLabel title={t("common.labels.unknown_one", { singular: t(campaignSingular).toLowerCase() })} />

    const requirements = reqinst.on(campaign)


    const campaignTimeZone = campaign.properties.timeZone ? { 'original': campaign.properties.timeZone } : undefined

    var requirementinstance = instance as RequirementInstance;

    if (isInstanceRef(instance))

        if (campaignstore.isLoaded(instance.campaign))
            requirementinstance = requirements.lookupLineage(instance)!
        else
            return <CampaignLabel campaign={instance.campaign} />

    if (!requirementinstance)
        return <UnknownLabel {...props} />

    if (deadlineOnly) {

        const events = eventinst.on(campaign)

        const deadline = events.deadlineOfRequirement(requirementinstance)

        if (deadline) {

            if (isAbsolute(deadline.date) && deadline.date.none) {
                return <Label {...props} noIcon noLink title={t("common.labels.no_due_date")} disabled />
            }
            
            const date = events.absolute(deadline.date)
            return date ? <TimeLabel accentFrame='weeks' format='numbers' displayMode={deadlineMode} render={props.mode === 'tag' ? date => t('campaign.date.due', { date }) : undefined} value={date} timezones={campaignTimeZone} {...rest} />
                : <Label {...props} noIcon noLink title={t("common.labels.unknown_due_date")} disabled />
        }
        else

            return <Label {...props} noIcon noLink title={t("common.labels.no_due_date")} disabled />

    }

    if (campaignMode === 'dashboard') {
        noVersion = true
        if (routedTypes.includes(campaignType))
            options.push(<Tooltip title={t("campaign.labels.campaign_view.design")}><Link to={() => reqinst.on(campaign).route(requirementinstance)}>{icns.edit}</Link></Tooltip>)
    }        
    else
        options.push(emptySpace)

    if (campaignMode === 'design') 
        options.push(<Tooltip title={t("campaign.labels.campaign_view.live")}><Link to={() => dashboard.on(campaign).routeToAsset(requirementinstance)}>{runningIcon}</Link></Tooltip>)
    

    return <InstanceLabel instance={requirementinstance} noMemo={props.noMemo} tooltipForLineage={ref => <RequirementInstanceLabel showCampaign instance={ref} />}

        baseRoute={requirementRoute}
        {...rest}
        sourceLabel={props => <RequirementLabel {...props} noMemo={props.noMemo} showTitle={showTitle} tipTitle={tipTitle} options={options} noVersion={noVersion} linkTo={linkTo || (() => reqinst.on(campaign).route(requirementinstance))} linkTarget={submissionType} {...rest} requirement={instance?.source} />} />


}, ($, $$) => $.instance === $$.instance && sameLabel($, $$))