
import { useTagModel } from '#app/tag/model';
import { TenantDto } from "#app/tenant/model";
import { User } from '#app/user/model';
import { useLogged } from '#app/user/store';
import { deepclone, indexMap } from "#app/utils/common";
import { paramsInQuery, updateQuery } from "#app/utils/routes";
import { withReport } from '#app/utils/validation';
import { CampaignContext, CampaignNext } from '#campaign/context';
import { useCampaignRouting } from '#campaign/store';
import { ParameterRef } from '#layout/parameters/model';
import { useLayoutRegistry } from '#layout/registry';
import { Topic } from '#messages/model';
import { requirementType } from "#requirement/constants";
import { Requirement, useRequirementModel } from '#requirement/model';
import { useRequirementStore } from '#requirement/store';
import moment from 'moment-timezone';
import { useContext } from 'react';
import { requirementDeadlineEvent } from '../constants';
import { useEventInstanceStore } from '../event/store';
import { AssetInstance, Campaign, CampaignInstance, InstanceRef, noCampaignInstance } from "../model";
import { PartyInstance, usePartyInstances } from "../party/api";


// future-proofs custom fields, starts.generic. 
export type RequirementInstanceDto = AssetInstance

export type RequirementInstance = RequirementInstanceDto    // future-proofs divergence from exchange model, starts, aligned.


export type RequirementInstanceState = {

    requirements: {
        all: RequirementInstance[]
        map: Record<string, RequirementInstance>
        sourceMap: Record<string, RequirementInstance>
    }

}

export const initialRequirementInstances = (): RequirementInstanceState => ({

    requirements: {
        all: undefined!,
        map: undefined!,
        sourceMap: undefined!
    }
})


export const useRequirementInstanceStore = () => {

    const state = useContext(CampaignContext)

    return {
        on: (c: Campaign) => {


            const self = {


                noInstance: () => ({

                    id: undefined!,
                    instanceType: requirementType,
                    source: "unknown",
                    campaign: c.id,
                    tags: [],
                    properties: {
                        editable: true,
                        versionable: true,
                        assessed: true,
                        version: undefined
                    }

                }) as RequirementInstance

                ,


                all: () => state.get().campaigns.instances[c.id]?.requirements?.all


                ,

                setAll: (instances: RequirementInstance[], props?: { noOverwrite: boolean }) => state.set(s => {

                    const campaign = s.campaigns.instances[c.id] ?? {}

                    const requirements = campaign.requirements ?? initialRequirementInstances()

                    if (requirements.all && props?.noOverwrite)
                        return

                    s.campaigns.instances[c.id] = { ...campaign, requirements: { ...requirements, all: instances, map: indexMap(instances).by(i => i.id), sourceMap: indexMap(instances).by(i => i.source) } }

                })

                ,

                livePush: (instances: RequirementInstance[], type: 'change' | 'remove') => {


                    const map = state.get().campaigns.instances[c.id]?.requirements?.map ?? {}
                    let current = state.get().campaigns.instances[c.id]?.requirements?.all ?? []

                    instances.forEach(instance => {

                        switch (type) {

                            case "change":
                                current = map[instance.id] ? current.map(i => i.id === instance.id ? instance : i) : [...current, instance]
                                break;

                            case "remove": current = current.filter(i => i.id !== instance.id); break

                        }

                    })

                    self.setAll(current)


                }

                ,



                lookup: (id: string | undefined) => id ? state.get().campaigns.instances[c.id]?.requirements?.map[id] : undefined

                ,

                safeLookup: (id: string | undefined) => self.lookup(id) ?? self.noInstance() as RequirementInstance

                ,

                lookupLineage: (ref: InstanceRef | undefined) => ref ? state.get().campaigns.instances[ref.campaign]?.requirements?.all.find(p => p.source === ref.source) : undefined

                ,

                lookupBySource: (source: string | undefined) => source ? state.get().campaigns.instances[c.id]?.requirements?.sourceMap[source] : undefined

                ,

                safeLookupBySource: (source: string | undefined) => self.lookupBySource(source) ?? noCampaignInstance(requirementType, c.id) as RequirementInstance


                ,

                generate: (requirement: Requirement): RequirementInstance => ({

                    id: undefined!,
                    instanceType: requirementType,
                    source: requirement.id,
                    campaign: c.id,
                    tags: deepclone(requirement.tags),
                    audience: requirement.audience,
                    audienceList: requirement.audienceList,
                    userProfile: requirement.userProfile,
                    properties: {
                        note: requirement.properties.note,
                        editable: requirement.properties.editable,
                        versionable: requirement.properties.versionable,
                        assessed: requirement.properties.assessed,
                        submissionTagMap: requirement.properties.submissionTagMap,
                        version: requirement.properties.version
                    }
                })


                ,

                audienceOf: (r: RequirementInstance) => self.safeLookup(r.source).audience

                ,

                validateInstance: (r: RequirementInstance) => withReport({})  // nothing to vaidate for now.

                ,

                topics: (r: CampaignInstance): Topic[] => [{ type: requirementType, name: `${c.id}:${r.source}` }]


            }

            return self
        }
    }
}

export const useRequirementInstances = () => {

    const store = useRequirementInstanceStore()

    const tags = useTagModel()

    const reqstore = useRequirementStore()
    const reqmodel = useRequirementModel()
    const eventinststore = useEventInstanceStore()
    const partyinst = usePartyInstances()

    const campaigns = useCampaignRouting()

    const layoutRegistry = useLayoutRegistry()

    const logged = useLogged()

    return {
        on: (c: Campaign) => {


            const self = {

                ...store.on(c)

                ,

                allSource: () => reqstore.all()

                ,

                lookupSource: (instance: string | RequirementInstance | undefined) => reqstore.safeLookup(typeof instance === 'string' ? instance : instance?.source)

                ,

                detailParam: () => 'ri-drawer'

                ,

                // eslint-disable-next-line
                detailInRoute: () => paramsInQuery(location.search)[self.detailParam()] as string

                ,

                route: (p?: RequirementInstance) =>

                    // eslint-disable-next-line
                    `${campaigns.routeTo(c)}/${requirementType}?${updateQuery(location.search).with(params => {

                        params[self.detailParam()] = p ? p.id : null
                        params['source'] = p?.source ?? null

                    })}`

                ,



                allSorted: () => [...self.all() ?? []].sort(self.comparator)

                ,

                allForParty: (party: PartyInstance, includeNotApplicable = partyinst.on(c).canSeeNotApplicable(party)) =>

                    (logged.hasNoTenant() || includeNotApplicable) ? self.allSorted() : self.allSorted().filter(ci => self.isForParty(party, ci))


                ,

                isForParty: (party: PartyInstance | TenantDto, ci: CampaignInstance): boolean => {

                    const partyId = (party as PartyInstance).source ?? (party as TenantDto).id

                    const audienceListCheckInclude = ci.audienceList ? ci.audienceList.includes.includes(partyId) || ci.audienceList.includes.length === 0 : true
                    const audienceListCheckExclude = ci.audienceList ? !ci.audienceList.excludes.includes(partyId) || ci.audienceList.excludes.length === 0 : true

                    const audienceListCheck = audienceListCheckExclude && audienceListCheckInclude

                    return audienceListCheck && (ci.audience ? tags.expression(ci.audience).matches(party) : true)

                }

                ,


                pastDue: (party?: PartyInstance) => {

                    const now = Date.now()

                    const targets = (party ? self.allForParty(party) : self.all()).map(r => r.source)

                    const events = eventinststore.on(c)

                    return events.all().filter(ei => ei.source === requirementDeadlineEvent &&
                        targets.includes(ei.target!)
                        && ei.date
                        && moment(new Date(events.absolute(ei.date)!)).isBefore(now))
                }


                ,

                stringify: (i: RequirementInstance) => self.stringifySourceRef(i.source)

                ,

                stringifySourceRef: reqmodel.stringifyRef

                ,

                stringifySource: reqmodel.stringify

                ,

                clone: (instance: RequirementInstance, campaign: CampaignNext) => {
                    const { type } = campaign
                    const clone = deepclone(instance)

                    const parameterOverlay = clone.properties.parameterOverlay
                    const overlays = parameterOverlay &&
                        Object.keys(parameterOverlay).reduce(
                            (acc, cur) => {
                                const param = parameterOverlay![cur] as ParameterRef
                                const clonedParam = type === 'branch' ?
                                    layoutRegistry.lookupParameter(param.original.spec).clone({ ...param.original, value: param.value }, campaign)
                                    :
                                    { ...param.original, value: param.value }
                                return clonedParam ?
                                    { ...acc, [cur]: { original: param.original, value: clonedParam ? clonedParam.value : clonedParam } }
                                    : acc
                            }
                            , {})


                    return {
                        ...clone,
                        id: undefined!,
                        campaign: c.id,
                        tags: type === 'branch' ? self.lookup(instance.source)?.tags ?? clone.tags : clone.tags,
                        lineage: type === 'branch' ? { source: instance.source, campaign: instance.campaign } : undefined,
                        properties: { ...clone.properties, parameterOverlay: overlays }

                    }

                }

                ,


                nameOf: (i: RequirementInstance) => `${reqmodel.nameOfRef(i?.source)}`


                ,

                comparator: (o1: RequirementInstance, o2: RequirementInstance) => reqmodel.comparatorRef(o1.source, o2.source)


                ,


                matches: (p: RequirementInstance, u: User) => {

                    const { given } = tags

                    return logged.isAdmin() || logged.isTenantManager() || given(u).matches(p.userProfile)

                }

            }

            return self
        }
    }
}


