import { NoSuchRoute } from "#app/components/NoSuchRoute";
import { campaignRoute } from "#campaign/constants";
import { CampaignValidation } from "#campaign/validation";
import { requirementType } from "#requirement/constants";
import * as React from "react";
import { Route, Switch } from "react-router-dom";
import { RequirementInstanceList } from "./List";

type Props = {

    report: ReturnType<CampaignValidation['validateRequirements']>
}


export const RequirementInstances = (props:Props)=> {

    return  <Switch>
                <Route exact path={`${campaignRoute}/:name/${requirementType}`} render={()=><RequirementInstanceList {...props} />} />
                <NoSuchRoute /> 
            </Switch>
           

}