

import { Paragraph } from "#app/components/Typography";
import { useConfig } from '#app/config/state';
import { useT } from '#app/intl/api';
import { contextCategory } from "#app/system/constants";
import { useTagStore } from '#app/tag/store';
import { useLogged } from '#app/user/store';
import { check, checkIt, notdefined, requireLanguages, reservedKeyword, stringLength, uniqueLanguages, withReport } from "#app/utils/validation";
import { usePartyInstanceValidation } from '#campaign//party/validation';
import { campaignPlural, campaignSingular, campaignType } from "#campaign/constants";
import { useEventInstanceProfile } from '#campaign/event/profile';
import { useEventInstanceStore } from '#campaign/event/store';
import { useEventInstanceValidation } from '#campaign/event/validation';
import { usePartyInstances } from '#campaign/party/api';
import * as React from "react";
import { Campaign, CampaignSettings } from "./model";
import { useProductInstances } from './product/api';
import { useProductInstanceValidation } from './product/validation';
import { useRequirementInstances } from './requirement/api';
import { useRequirementInstanceValidation } from './requirement/validation';
import { useCampaignStore } from './store';

export type CampaignValidation = ReturnType<typeof useCampaignValidation>

export const useCampaignValidation = () => {

   const logged = useLogged()

    const t = useT()

    const store = useCampaignStore()

    const partyinstvalidation = usePartyInstanceValidation()
    const partyinststore = usePartyInstances()

    const prodinstvalidation = useProductInstanceValidation()
    const prodinststore = useProductInstances()

    const reqinstvalidation = useRequirementInstanceValidation()
    const reqinstancestore = useRequirementInstances()

    const eventinstvalidation = useEventInstanceValidation()
    const eventinststore = useEventInstanceStore()
    const eventinstprofile = useEventInstanceProfile()

    const campaigns = useCampaignStore()

    const config = useConfig()
    
    const tags = useTagStore()
    
    const self = {


        validationErrors: (c: Campaign) => {


            return store.isLoaded(c.id) ?
                self.validateParties(c).errors()
                + self.validateProducts(c).errors()
                + self.validateRequirements(c).errors()
                + self.validateEvents(c).errors()
                : 0

        }

        ,

        validateProfile: (edited: Campaign, initial: Campaign) => {


            const { validateCategories } = tags

            const requiredLangs = config.get().intl.required || [];

            const singular = t(campaignSingular).toLowerCase()
            const plural = t(campaignPlural).toLowerCase()


            return withReport({

                active: checkIt().nowOr(t("common.fields.active.msg"), t("common.fields.active.help", { plural })),

                name: check(edited.name).with(notdefined(t))
                    .with(requireLanguages(requiredLangs, t))
                    .with(reservedKeyword(t))
                    .with(stringLength(t))
                    .with(uniqueLanguages(campaigns.all().filter(t => t.id !== edited.id).map(t => t.name), t)).nowOr(
                        t("common.fields.name_multi.msg"),
                        t("common.fields.name_multi.help", { plural, requiredLangs })),


                description: check(edited.description).nowOr(
                    t("common.fields.description_multi.msg"),
                    t("common.fields.description_multi.help", { plural }),
                )

                ,

                lineage: checkIt().nowOr(
                    t("common.fields.lineage.msg", { singular }),
                    t("campaign.fields.lineage.help"),
                )

                ,

                note: check(edited.properties.note ? edited.properties.note[logged.tenant] : '')
                    .nowOr(t("common.fields.note.msg"), t("common.fields.note.help", { singular }))

                ,

                muted: checkIt().nowOr(
                    t("campaign.fields.muted.msg"),
                    t("campaign.fields.muted.help"),
                )

                ,

                ...validateCategories(edited.tags).include(contextCategory)

                ,

                ...validateCategories(edited.tags).for(campaignType)

                ,

                approveCycle: {
                    msg: t("campaign.fields.approval.msg"),
                    help: <React.Fragment>
                        <Paragraph>{t("campaign.fields.approval.help1")}</Paragraph>
                        <Paragraph spaced >{t("campaign.fields.approval.help2_campaigns")}</Paragraph>
                    </React.Fragment>
                }

                ,

                defaultRelativeDate: checkIt().nowOr(
                    t("campaign.fields.default_relative_date.msg"),
                    t("campaign.fields.default_relative_date.help", { plural }),
                )

                ,

                complianceScale: {
                    msg: t("campaign.fields.compliance_scale.msg"),
                    help: <React.Fragment>
                        <Paragraph>{t("campaign.fields.compliance_scale.help1")}</Paragraph>
                        <Paragraph spaced>{t("campaign.fields.compliance_scale.help2")}</Paragraph>
                    </React.Fragment>
                }

                ,

                timelinessScale: {
                    msg: t("campaign.fields.timeliness_scale.msg"),
                    help: <React.Fragment>
                        <Paragraph>{t("campaign.fields.timeliness_scale.help1")}</Paragraph>
                        <Paragraph spaced>{t("campaign.fields.timeliness_scale.help2")}</Paragraph>
                    </React.Fragment>
                }

                ,

                statisticalHorizon: {
                    msg: t("campaign.fields.statistical_horizon.msg"),
                    help: <React.Fragment>
                        <Paragraph>{t("campaign.fields.statistical_horizon.help1")}</Paragraph>
                    </React.Fragment>
                }

                ,

                suspendOnEnd: {

                    msg: t("campaign.fields.suspend_on_end.msg"),
                    help: <Paragraph>{t("campaign.fields.suspend_on_end.help")}</Paragraph>

                }

                ,

                suspendSubmissions: {

                    msg: t("campaign.fields.suspend_submissions.msg"),
                    help: <Paragraph>{t("campaign.fields.suspend_submissions.help")}</Paragraph>

                }

                ,

                partyCanSeeNotApplicable: {
                    msg: t("campaign.fields.party_can_see_not_applicable.msg"),
                    help: <React.Fragment>
                        <Paragraph>{t("campaign.fields.party_can_see_not_applicable.help1")}</Paragraph>
                        <Paragraph spaced>{t("campaign.fields.party_can_see_not_applicable.help2")}</Paragraph>
                    </React.Fragment>
                }

                ,

                statExcludeList: {
                    msg: t("campaign.fields.stat_exclude_list.msg"),
                    help: <Paragraph>{t("campaign.fields.stat_exclude_list.help")}</Paragraph>
                }



            })


        }

        ,

        validateApplicationSettings: (_: Partial<CampaignSettings>) => {

            return {


                liveguard: {

                    msg: t("campaign.fields.liveguard.msg"),
                    help: <Paragraph>{t("campaign.fields.liveguard.help")}</Paragraph>

                }
                ,

                approveCycle: {
                    msg: t("campaign.fields.approval.msg"),
                    help: <React.Fragment>
                        <Paragraph>{t("campaign.fields.approval.help1")}</Paragraph>
                    </React.Fragment>
                }

                ,

                adminCanEdit: {

                    msg: t("campaign.fields.admin_can_edit.msg"),
                    help: <React.Fragment>
                        <Paragraph>{t("campaign.fields.admin_can_edit.help1")}</Paragraph>
                    </React.Fragment>
                }

                ,


                adminCanSubmit: {

                    msg: t("campaign.fields.admin_can_submit.msg"),
                    help: <React.Fragment>
                        <Paragraph>{t("campaign.fields.admin_can_submit.help1")}</Paragraph>
                    </React.Fragment>
                }

                ,

                complianceScale: {
                    msg: t("campaign.fields.compliance_scale.msg"),
                    help: <React.Fragment>
                        <Paragraph>{t("campaign.fields.compliance_scale.help1")}</Paragraph>
                    </React.Fragment>
                }

                ,

                timelinessScale: {
                    msg: t("campaign.fields.timeliness_scale.msg"),
                    help: <React.Fragment>
                        <Paragraph>{t("campaign.fields.timeliness_scale.help1")}</Paragraph>
                    </React.Fragment>
                }

                ,

                statisticalHorizon: {
                    msg: t("campaign.fields.statistical_horizon.msg"),
                    help: <React.Fragment>
                        <Paragraph>{t("campaign.fields.statistical_horizon.help1")}</Paragraph>
                    </React.Fragment>
                }

                ,
                suspendOnEnd: {

                    msg: t("campaign.fields.suspend_on_end.msg"),
                    help: <Paragraph>{t("campaign.fields.suspend_on_end.help")}</Paragraph>

                }
                ,

                defaultTimeZone: {
                    msg: t("common.fields.timezone.msg"),
                    help: <React.Fragment>
                        <Paragraph>{t("common.fields.timezone.help2")}</Paragraph>
                    </React.Fragment>
                }

            }
        }

        ,

        validateParties: (edited: Campaign) => {


            const { validateInstance } = partyinstvalidation.on(edited)
            const { all } = partyinststore.on(edited)

            const report = all().map(p => ({ p, e: validateInstance(p).errors() })).reduce((acc, { p, e }) => ({ [p.id]: { status: e > 0 ? "error" : "success", msg: t("common.feedback.issues", { count: e }) }, ...acc }), {})

            return withReport(report)

        }

        ,


        validateRequirements: (edited: Campaign) => {

            const { validateInstance } = reqinstvalidation.on(edited)
            const { all } = reqinstancestore.on(edited)

            return withReport({


                ...all().map(p => ({ p, e: validateInstance(p).errors() })).reduce((acc, { p, e }) => ({ [p.id]: { status: e > 0 ? "error" : "success", msg: t("common.feedback.issues", { count: e }) }, ...acc }), {})
            })


        }

        ,

        validateProducts: (edited: Campaign) => {


            const { validateInstance } = prodinstvalidation.on(edited)
            const { all } = prodinststore.on(edited)

            return withReport({

                ...all().map(p => ({ p, e: validateInstance(p).errors() })).reduce((acc, { p, e }) => ({ [p.id]: { status: e > 0 ? "error" : "success", msg: t("common.feedback.issues", { count: e }) }, ...acc }), {})
            })


        }

        ,

        validateEvents: (edited: Campaign) => {

            const { validateInstance } = eventinstvalidation.on(edited)
            const { all } = eventinststore.on(edited)
            const { validationProfile } = eventinstprofile.on(edited)

            const profile = validationProfile()

            return withReport({

                ...all().map(i => ({ i, e: validateInstance(i, profile as any).errors() }))
                    .reduce((acc, { i, e }) => ({ [i.id]: { status: e > 0 ? "error" : "success", msg: t("common.feedback.issues", { count: e }) }, ...acc }), {})
            })


        }


    }

    return self;

}