# How we enforce constraints in submission workflow

The submission workflow is subjected to a rich set constraints which vary based on:

- **campaign state**: is it suspended, is it ended, is it archied?
- **asset instance configuration**: is it editable pre- is or post-publish, is it assessable, is it versionable ?
- **party preferences**: is it in the audience of the asset.
- **trail state**: is it superseded, is it active, is it official?
- **revision state**: is it saved, is it in any of these states, is it active, has it been assessed, is it superseded?
- **user permissions**: do they have at leastthis permission?
- **user tenancy**: are they CPC users or tenant users ? 

As a result, some actions may be available only for some users, or at some specific time, or in some specic state overall. Otherwise UI elements such as buttons and fields may appear disabled, or even disappear altogether.

Implementing this multiplicity of requirements is challenging. 

We have these basic tools:

- hide fields/buttons if they’re fundamentally incompatible with the current context.
- mark them`disabled`/`enabled` if they’re compatible and yet can/can’t be used in the current context.
- mark the page `readonly` to disable all buttons/fields at once.
- mark fields/buttons `enabledOnReadonly` to override page settings.

Our strategy is as follows.

In `useSubmission()`, we:

- define a `canShowX()` guard for each action `X`.
  - holds true if the action is compatible with the current context.
  
- define a `canX()` guard for each action `X`.
  - holds true if the action can be performed in the current context.
  - `canX()` uses `canShowX()` to “fail-fast”.
  
- define a single `canEdit()` guard.
  - holds true when *content* can be edited in fields.
  
In `<SubmissionDetail>` we:

- mark the page `readonly` if `canEdit()` holds false. 

- define a `<XBtn>` for each action `X`. 
  - renders if `canShowX()` holds true.
  - is `enabled` if `canX()` holds true.
  - is `enabledOnReadonly` also if `canX()` holds true.


> why set both `enabled` and `enabledOnReadonly`?  Because `canX()`is more specific that `canEdit()`, so we must override the page-wide `read-only` marker when the action can be performed.

Note that some actions can be performed whenever they’re compatible with the current context. We can tell in our strategy because `canX()` delegates to `canShowX()`  but adds no checks of its own. In these cases, we could use just one guard and specify just `enabledOnReadonly` on buttons and fields. 

However,  the cost of sticking with the general pattern is negligible, and it’s safer to do so if later the logic evolves, `canX` needs to add extra checks, and `enabled` becomes required. Then, we don’t need to recall the fine details of the strategy, or worse forget to apply it properly.