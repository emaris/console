import { createContext } from 'react';
import { SubmissionContext } from './model';


// propagates a submission and its context.
export const SubmissionReactContext = createContext<SubmissionContext>(undefined!)
