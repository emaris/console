
import { productType } from '#product/constants'
import { requirementType } from '#requirement/constants'
import { useTime } from '#app/time/api'
import moment from "moment-timezone"
import { ComplianceProfile, SubmissionStub } from "../model"
import { TrailSummary } from './trail'
import { useTimelinessScale } from '../profile'
import { roundPercent } from './api'



// collects statistics over groups of trails.
// expects homogenous groups, ie. about same party (fixed party) or same asset (fixed asset).

export type AggregateSummary = {

    dueSubmissions: number                                      // expected submissions (due assets for a fixed party. due parties, for a fixed asset )

    submitted: TrailSummary[]                                   // actually submitted by parties so far.
    submittedRequirements: TrailSummary[]
    submittedProducts: TrailSummary[]

    submittedAsMissing: TrailSummary[]                          // submitted as placeholders by campaign managers so as to attach assessments.
    submittedAsMissingRequirements: TrailSummary[]
    submittedAsMissingProducts: TrailSummary[]

    late: () => TrailSummary[]                                  // submitted late (includes placeholders, always late).

    assessable: TrailSummary[],                                 // submitted by parties and eligible for assesment.
    assessableRequirements: TrailSummary[]
    assessableProducts: TrailSummary[]

    assessed: TrailSummary[]                                    // submitted (actuall or as placeholders), and actually assessed.
    assessedRequirements: TrailSummary[]
    assessedProducts: TrailSummary[]

    pending: TrailSummary[]                                     // not submitted and waiting internal approval.
    pendingRequirements: TrailSummary[]
    pendingProducts: TrailSummary[]

    drafts: TrailSummary[]                                      // not submited and in-progress work.
    draftRequirements: TrailSummary[]
    draftProducts: TrailSummary[]

    submissionProfile: AggregatedSubmissionProfile              // rates and profiles.
    assessmentProfile: AggregatedAssessmentProfile
    complianceProfile: AggregatedComplianceProfile


    last: SubmissionStub | undefined                            // most recently edited.
    lastEditedRequirement: SubmissionStub | undefined
    lastEditedProduct: SubmissionStub | undefined

    lastSubmitted: SubmissionStub | undefined                   // most recently submitted.
    lastSubmittedRequirement: SubmissionStub | undefined
    lastSubmittedProduct: SubmissionStub | undefined


}


// collects reporting rates across many submissions.
export type AggregatedSubmissionProfile = {

    rate: number
    lateRate: () => number                              // wrt to submissions (real or placholders)
    absoluteLateRate: () => number                      // wrt to expectations.
    missingRate: number,
    submissionGroup: 1 | 2 | 3 | 4
    submissionClass: string

}

export type GroupOrdinal = 1 | 2 | 3 | 4

export type Groups<T extends Object> = Record<GroupOrdinal, T[]>

export const emptyGroups = <T extends Object>(): Groups<T> => ({ 1: [] as T[], 2: [] as T[], 3: [] as T[], 4: [] as T[] })

// collects compliance rates across many submissions. 
export type AggregatedComplianceProfile = {

    aggregateRate: number,
    aggregateWeightSum: number,

    rate: number                                            // compliance rate across all summaries.
    group: GroupOrdinal                                    // matching performance group.
    complianceClass: string                                          // matching CSS class.

    groups: Groups<TrailSummary>

    aggregateRequirementRate: number,
    aggregateRequirementWeightSum: number,

    requirementRate: number
    requirementGroup: GroupOrdinal
    requirementGroups: Groups<TrailSummary>
    requirementComplianceClass: string

    aggregateProductRate: number,
    aggregateProductWeightSum: number,


    productRate: number
    productGroup: GroupOrdinal
    productGroups: Groups<TrailSummary>
    productComplianceClass: string


    profiles: (ComplianceProfile & { submission: SubmissionStub })[]        // all compliance profiles, enriched with their submission.
}

// collects assessment rates across many submissions. 
export type AggregatedAssessmentProfile = {

    rate: number
    assessmentGroup: 1 | 2 | 3 | 4
    assessmentClass: string

}

const initialSummary = (): AggregateSummary => ({

    dueSubmissions: 0,

    complianceProfile: {

        groups: emptyGroups<TrailSummary>(),
        requirementGroups: emptyGroups<TrailSummary>(),
        productGroups: emptyGroups<TrailSummary>(),
        profiles: [] as ComplianceProfile[]

    } as AggregatedComplianceProfile,

    submissionProfile: {} as AggregatedSubmissionProfile,
    assessmentProfile: {} as AggregatedAssessmentProfile,

    last: undefined,
    lastEditedRequirement: undefined,
    lastEditedProduct: undefined,

    lastSubmitted: undefined,
    lastSubmittedRequirement: undefined,
    lastSubmittedProduct: undefined,

    drafts: [],
    draftRequirements: [],
    draftProducts: [],

    pending: [],
    pendingRequirements: [],
    pendingProducts: [],

    submitted: [],
    submittedRequirements: [],
    submittedProducts: [],

    submittedAsMissing: [],
    submittedAsMissingRequirements: [],
    submittedAsMissingProducts: [],

    assessable: [],
    assessableRequirements: [],
    assessableProducts: [],

    assessed: [],
    assessedRequirements: [],
    assessedProducts: [],

    late: undefined!

})


export const useAggregate = () => {


    const time = useTime()
    const timelinessScale = useTimelinessScale()


    return (

        trailSummaries: TrailSummary[],
        dueSubmissions: number,

    ) => {


        const summary = initialSummary()

        summary.dueSubmissions = dueSubmissions

        // when we aggregate some submissions are already late now, others are missing but not due yet: so maybe late maybe not.
        // then we cache this and time passes. later we consult the statistics and some that maybe's have become provably late.

        let lateNow = [] as TrailSummary[]                  // submissions that are already late now.
        let maybeLate = [] as [string, TrailSummary][]      // submissions that are missing but 'notdue' yet, so may be late when we'll ask.


        summary.late = () => {  // keeps maybelate that are provably late when this is invoked.

            const now = time.current()

            return [...lateNow, ...maybeLate.filter(([date,]) => now.isAfter(new Date(date))).map(([, trailsummary]) => trailsummary)]
        }


        trailSummaries.forEach(trailsummary => {

            const type = trailsummary.trail.key.assetType

            // MISSING PLACEHOLDERS.
            if (trailsummary.missing) {

                summary.submittedAsMissing.push(trailsummary)       // separate from actual submissions. 
                summary.assessable.push(trailsummary)               // necessarily assessable. 

                switch (type) {
                    case requirementType: summary.assessedRequirements.push(trailsummary); break
                    case productType: summary.assessedProducts.push(trailsummary); break
                }


                if (trailsummary.compliance) {

                    summary.assessed.push(trailsummary)             // and assessed.      

                    // puts the summary in a compliance groups 2-4.
                    const summarygroup = trailsummary.compliance.rate >= 0 ? Math.floor(trailsummary.compliance.rate / 49.5 + 2) : 1
                    summary.complianceProfile.groups[summarygroup].push(trailsummary)

                    switch (type) {

                        case requirementType: summary.complianceProfile.requirementGroups[summarygroup].push(trailsummary); break
                        case productType: summary.complianceProfile.productGroups[summarygroup].push(trailsummary); break
                    }

                }

                // always considered late, regardless of time.
                // note: commenting this to make late counts/rates transparent to users.
                // party users don't need to explain 'late' with statistics. 
                //lateNow.push(trailsummary)       

                return

            }

            // PARTY SUBMISSIONS.
            if (trailsummary.official) {

                summary.submitted.push(trailsummary)

                switch (type) {

                    case requirementType: summary.submittedRequirements.push(trailsummary); break
                    case productType: summary.submittedProducts.push(trailsummary); break
                }

                // LATEST REFERENCES.  
                const submittedDate = trailsummary.official?.lifecycle.lastSubmitted

                if (!trailsummary.missing && submittedDate) {

                    if (!summary.lastSubmitted || moment(submittedDate).isAfter(summary.lastSubmitted.lifecycle.lastSubmitted)) {

                        summary.lastSubmitted = trailsummary.official
                    }

                    switch (type) {

                        case requirementType:

                            if (!summary.lastSubmittedRequirement || moment(submittedDate).isAfter(summary.lastSubmittedRequirement.lifecycle.lastSubmitted)) {
                                summary.lastSubmittedRequirement = trailsummary.official
                            }

                            break;


                        case productType:

                            if (!summary.lastSubmittedProduct || moment(submittedDate).isAfter(summary.lastSubmittedProduct.lifecycle.lastSubmitted)) {
                                summary.lastSubmittedProduct = trailsummary.official
                            }

                            break

                    }
                }


                // TIMELINESS.
                // if (trailsummary.computedTimeliness === 'late')         // already late now.
                //     lateNow.push(trailsummary)
                // else
                //     if (trailsummary.computedTimeliness === 'notdue' && trailsummary.dueDate)   // submisisons are never late if the asset wasn't due.
                //         maybeLate.push([trailsummary.dueDate, trailsummary])

                const timelinessScaleValue = timelinessScale.for(trailsummary.timeliness?.tag)

                if (trailsummary.timeliness?.tag && timelinessScaleValue && timelinessScaleValue < 0)
                    lateNow.push(trailsummary)
                else
                    if (trailsummary.computedTimeliness === 'notdue' && trailsummary.dueDate)   // submisisons are never late if the asset wasn't due.
                        maybeLate.push([trailsummary.dueDate, trailsummary])



                // ASSESSABLE: is it eligible for assessment (so does it impact on assessment rate)?
                if (trailsummary.assessable) {

                    summary.assessable.push(trailsummary)

                    switch (type) {
                        case requirementType: summary.assessableRequirements.push(trailsummary); break
                        case productType: summary.assessableProducts.push(trailsummary); break
                    }

                }

                // ASSESSMENT
                if (trailsummary.compliance) {    // has it been assessed already?

                    summary.assessed.push(trailsummary)

                    switch (type) {
                        case requirementType: summary.assessedRequirements.push(trailsummary); break
                        case productType: summary.assessedProducts.push(trailsummary); break
                    }

                    // slots summary in a group
                    const profile = { ...trailsummary.compliance, submission: trailsummary.official ?? trailsummary.lastRevision! }

                    if (trailsummary.compliance.rate >= 0) {

                        summary.complianceProfile.profiles.push(profile)

                        // puts the summary in a compliance groups 2-4.
                        const summarygroup = Math.floor(trailsummary.compliance.rate / 49.6 + 2)
                        summary.complianceProfile.groups[summarygroup].push(trailsummary)

                        switch (type) {

                            case requirementType: summary.complianceProfile.requirementGroups[summarygroup].push(trailsummary); break
                            case productType: summary.complianceProfile.productGroups[summarygroup].push(trailsummary); break
                        }
                    }

                    // puts not applicable assessments into first group. 
                    else {

                        summary.complianceProfile.groups[1].push(trailsummary)

                        switch (type) {

                            case requirementType: summary.complianceProfile.requirementGroups[1].push(trailsummary); break
                            case productType: summary.complianceProfile.productGroups[1].push(trailsummary); break
                        }
                    }
                }


            }

            // looks at the last revision to count draft and pending.
            if (trailsummary.lastRevision) {

                if (trailsummary.lastRevision.lifecycle.state === 'draft') {

                    summary.drafts.push(trailsummary)

                    switch (type) {
                        case requirementType: summary.draftRequirements.push(trailsummary); break
                        case productType: summary.draftProducts.push(trailsummary); break
                    }

                }

                else if (trailsummary.lastRevision.lifecycle.state === 'pending') {

                    summary.pending.push(trailsummary)

                    switch (type) {
                        case requirementType: summary.pendingRequirements.push(trailsummary); break
                        case productType: summary.pendingProducts.push(trailsummary); break
                    }
                }


                // is this the most recent version ?    

                const modifiedDate = trailsummary.lastRevision.lifecycle.lastModified

                if (!summary.last || moment(modifiedDate).isAfter(summary.last.lifecycle.lastModified))
                    summary.last = trailsummary.lastRevision

                switch (type) {

                    case requirementType:

                        if (!summary.lastEditedRequirement || moment(modifiedDate).isAfter(summary.lastEditedRequirement.lifecycle.lastSubmitted))
                            summary.lastEditedRequirement = trailsummary.official

                        break;


                    case productType:

                        if (!summary.lastEditedProduct || moment(modifiedDate).isAfter(summary.lastEditedProduct.lifecycle.lastSubmitted))
                            summary.lastEditedProduct = trailsummary.official

                        break

                }

            }

        })

        // note: in the stats below we:
        // 1. first compute a rate, rounding some average. 
        // 2. then classify the rate in a performance group from 1 to 4.
        // 3. then use the group to build a css class that implements a coloring scheme.
        // if there's no data yet, rates are NaNs and we default to the best group possible. this makes colouring optimistic: "green until proved otherwise".
        // components that present the rate will render them as they see fit, '0', or '--' or 'N/A' or ....

        // submission stats: actual party submissions (no missing placeholders) over submissions expected.
        const rate = roundPercent (summary.submitted.length * 100, summary.dueSubmissions)

        // for stability, we cap the rate to a sensical value, or assumptions will break down the line (app crash)
        // but we log the anomaly so that bugs can be chased.
        if (rate > 100)
            console.warn("incorrect submission rate: more submissions than due", { summary })

        summary.submissionProfile.rate = rate

        summary.submissionProfile.submissionGroup = isNaN(summary.submissionProfile.rate) ? 4 : Math.floor(summary.submissionProfile.rate / 33.1 + 1) as GroupOrdinal
        summary.submissionProfile.submissionClass = `submission-${summary.submissionProfile.submissionGroup}`

        // missing rate: placeholder submissions over expected submissions
        summary.submissionProfile.missingRate = Math.round((summary.submittedAsMissing.length * 100) / summary.dueSubmissions)


        // late rate: late submissions over submissions, both including actual party submissions and placeholders.
        summary.submissionProfile.lateRate = () => Math.round((summary.late().length * 100) / summary.submitted.length)
        summary.submissionProfile.absoluteLateRate = () => Math.round((summary.late().length * 100) / summary.dueSubmissions)

        //compliance stats: weighted average rate across all assessed submissions (any asset type, actual and placeholder).
        const requirementProfiles = Object.values(summary.complianceProfile.requirementGroups).flat().filter(s => s.compliance?.rate !== undefined).map(s => s.compliance!)
       
        summary.complianceProfile.aggregateRequirementRate = requirementProfiles.reduce((sum: number, profile) => sum + (profile.rate * (profile.weight || 1)), 0)
        summary.complianceProfile.aggregateRequirementWeightSum =requirementProfiles.reduce((sum: number, profile) => sum + (profile.weight || 1), 0)
        
        summary.complianceProfile.requirementRate = roundPercent(summary.complianceProfile.aggregateRequirementRate, summary.complianceProfile.aggregateRequirementWeightSum)
        summary.complianceProfile.requirementGroup = isNaN(summary.complianceProfile.requirementRate) ? 4 : Math.floor(summary.complianceProfile.requirementRate / 33.1 + 1) as GroupOrdinal
        summary.complianceProfile.requirementComplianceClass = `compliance-${summary.complianceProfile.requirementGroup}`


        const productProfiles = Object.values(summary.complianceProfile.productGroups).flat().filter(s => s.compliance?.rate !== undefined).map(s => s.compliance!)
        
        summary.complianceProfile.aggregateProductRate = productProfiles.reduce((sum: number, profile) => sum + (profile.rate * (profile.weight || 1)), 0)
        summary.complianceProfile.aggregateProductWeightSum = productProfiles.reduce((sum: number, profile) => sum + (profile.weight || 1), 0)
        
        summary.complianceProfile.productRate = roundPercent(summary.complianceProfile.aggregateProductRate, summary.complianceProfile.aggregateProductWeightSum)
        summary.complianceProfile.productGroup = isNaN(summary.complianceProfile.productRate) ? 4 : Math.floor(summary.complianceProfile.productRate / 33.1 + 1) as GroupOrdinal
        summary.complianceProfile.productComplianceClass = `compliance-${summary.complianceProfile.productGroup}`

        
        summary.complianceProfile.aggregateRate = summary.complianceProfile.aggregateRequirementRate + summary.complianceProfile.aggregateProductRate
        summary.complianceProfile.aggregateWeightSum = summary.complianceProfile.aggregateRequirementWeightSum +  summary.complianceProfile.aggregateProductWeightSum
        
        summary.complianceProfile.rate = roundPercent(summary.complianceProfile.aggregateRate, (summary.complianceProfile.aggregateWeightSum))
        summary.complianceProfile.group = isNaN(summary.complianceProfile.rate) ? 4 : Math.floor(summary.complianceProfile.rate / 33.1 + 1) as GroupOrdinal
        summary.complianceProfile.complianceClass = `compliance-${summary.complianceProfile.group}`
        

        // assessment stats: assessed submissions (actual or placeholders) over assessable submissions.
        summary.assessmentProfile.rate = roundPercent(summary.assessed.length * 100, summary.assessable.length)
        summary.assessmentProfile.assessmentGroup = isNaN(summary.assessmentProfile.rate) ? 4 : Math.floor(summary.assessmentProfile.rate / 33.1 + 1) as GroupOrdinal
        summary.assessmentProfile.assessmentClass = `assessment-${summary.assessmentProfile.assessmentGroup}`

        return summary

    }
}