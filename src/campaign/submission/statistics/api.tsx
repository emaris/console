import { useEventInstanceStore } from '#campaign/event/store'
import { PartyInstance, usePartyInstances } from '#campaign/party/api'
import { useProductInstances } from '#campaign/product/api'
import { useRequirementInstances } from '#campaign/requirement/api'
import { productType } from '#product/constants'
import { requirementType } from '#requirement/constants'
import { partition } from 'lodash'
import { AssetInstance, Campaign, CampaignInstance, useCampaignModel } from "../../model"
import { PartyInstanceDto } from "../../party/api"
import { AggregateSummaries, aggregateMany } from './aggregateMany'
import { AggregateSummary, useAggregate } from './aggregateOne'
import { TrailSummary, useTrailSummary, useTrailSummaryNoDeps } from './trail'



export const roundPercent = (aggregation: number, population: number) => Math.min(Math.round( ( aggregation / population) * 10 ) / 10, 100)


export type AssetSummary = AggregateSummary & {

    dueParties: PartyInstance[]
    dueDate: string | undefined
}

// collects and aggreagtes reporting, assessment, and compliance statistics about parties.
export type AllAssetsSummary = AggregateSummaries<AssetSummary>


// statistics over a party have specific aspects:
//
//  1. we need to split derived data and counts across asset types.
//  2. we may want to measure them in real-time, not necessarily at the end of the campaign
//     (how's the performance today, how are they doing for the upcoming submissions). so we need split counts at a given horizon.
//

export type PartySummary = AggregateSummary & {

    dueAssets: AssetInstance[]                                         // expected at this point.
    dueRequirements: AssetInstance[]
    dueProducts: AssetInstance[]

    next: PartySummary

}



// collects and aggregates reporting, assessment, and compliance statistics about and across all parties.
export type AllPartiesSummary = AggregateSummaries<PartySummary>


export const useSubmissionStatistics = () => {

    const prodinstore = useProductInstances()
    const reqinststore = useRequirementInstances()
    const partyinststore = usePartyInstances()

    const eventinstancestore = useEventInstanceStore()
    const campaignprops = useCampaignModel()

    const trailsummary = useTrailSummary()
    const trailsummaryND = useTrailSummaryNoDeps()

    const aggregate = useAggregate()

    return {

        on: (c: Campaign) => {


            const allAssetsSummary =  (assets: CampaignInstance[], trailSummaries: Record<string, TrailSummary[]>, dueDates: Record<string, string>): AllAssetsSummary => {

                const summaries = aggregateMany<AssetSummary>()

                const products = prodinstore.on(c)
                const requirements = reqinststore.on(c)
                const parties = partyinststore.on(c)

                const isForParty = {

                    [requirementType]: requirements.isForParty,
                    [productType]: products.isForParty,
                }

                const partiesForLogged = parties.allForStats()

                assets.forEach(asset => {

                    const dueDate = dueDates[asset.source]

                    const dueParties = partiesForLogged.filter(p => isForParty[asset.instanceType](p, asset))

                    const duePartiesId = dueParties.map(p => p.source)

                    // computes summaries of all trails of party submission for this asset wrt to its due date.
                    const filteredAssetSummaries = (trailSummaries[asset.source] ?? []).filter(ts =>  duePartiesId.includes(ts.trail.key.party))

                    
                    // computes common statistics for this asset. 
                    const summary: AssetSummary = {

                        ...aggregate(filteredAssetSummaries, dueParties.length),

                        dueParties,
                        dueDate
                    }
   
                    summaries.add(asset.source, summary)
   
                })

            

                  return summaries.done()
            }

            const allPartiesSummary = (summaryMap: [PartyInstance, TrailSummary[]][], horizon?: moment.Moment): AllPartiesSummary => {

                const summaries = aggregateMany<PartySummary>()
                const excludeList = c.properties.statExcludeList ?? []
                const requirements = reqinststore.on(c)
                const products = prodinstore.on(c)
                const currentHorizon = horizon ?? campaignprops.currentHorizon(c)

                const events = eventinstancestore.on(c)

                // we need to know what how many assets of which type are due at the horizon
                // for this we need due dates, but we can't just look at those inside summaries, as a parties might have not submitted for all assets yet.
                // so we recompute them here. but then we end up including assets that are not due for some parties.
                // so we need to filter those out as we iterate across parties.
                const dueDates = { ...events.allDueDatesByRequirementId(), ...events.allDueDatesByProductId() }

                const assetsInCampaign = [...requirements.all(), ...products.all()]

                const [assetsDueAtHorizon, assetsDueBeyondHorizon] = partition(assetsInCampaign, a => !dueDates[a.source] || currentHorizon.isAfter(new Date(dueDates[a.source])))

                const isForParty = {

                    [requirementType]: requirements.isForParty,
                    [productType]: products.isForParty

                }
                const assetFor = (party: PartyInstance) => (a: AssetInstance) => isForParty[a.instanceType](party, a)


                summaryMap.forEach(([party, trailSummaries]) => {

                    // filter sunmmaries by those that are currently applicable to party.
                    // or a submisison made before a restriction came into force will break statistics.
                    const dueAssetsMap = {} as Record<string, boolean>
                    const assetsDueAtHorizonForParty = [] as AssetInstance[]
                    const requirementsDueAtHorizon = [] as AssetInstance[]
                    const productsDueAtHorizon = [] as AssetInstance[]

                    assetsDueAtHorizon.forEach(asset => {

                        if (assetFor(party)(asset)) {
                            assetsDueAtHorizonForParty.push(asset)
                            dueAssetsMap[asset.source] = true
                            if (asset.instanceType === requirementType)
                                requirementsDueAtHorizon.push(asset)
                            else
                                productsDueAtHorizon.push(asset)

                        }

                    })

                    const requirementsDueBeyondHorizon = [] as AssetInstance[]
                    const productsDueBeyondtHorizon = [] as AssetInstance[]

                    assetsDueBeyondHorizon.forEach(asset => {

                        if (assetFor(party)(asset)) {

                            if (asset.instanceType === requirementType)
                                requirementsDueBeyondHorizon.push(asset)
                            else
                                productsDueBeyondtHorizon.push(asset)
                        }
                    })

                    const applicableTrailSummaries = trailSummaries.filter(summary => dueAssetsMap[summary.trail.key.asset])

                    // counts requirement, product, and total expectations for this party.
                    summaries.get().dueSubmissions += assetsDueAtHorizonForParty.length

                    const assetsDueBeyondHorizonForParty = assetsDueBeyondHorizon.filter(assetFor(party))

                    const [dueTrailSummaries, nextTrailSummaries] = partition(applicableTrailSummaries, s => !s.dueDate || currentHorizon.isAfter(new Date(s.dueDate)))

                    //console.log({party:party.source,dueTrailSummaries,assetsDueAtHorizonForParty,applicableTrailSummaries, dueAssetsMap})

                    const dueSummary = aggregate(dueTrailSummaries, assetsDueAtHorizonForParty.length) as PartySummary
                    const nextSummary = aggregate(nextTrailSummaries, assetsDueBeyondHorizonForParty.length) as PartySummary

                    const summary: PartySummary = {

                        ...dueSummary,

                        dueAssets: assetsDueAtHorizonForParty,
                        dueRequirements: requirementsDueAtHorizon,
                        dueProducts: productsDueAtHorizon,

                        next: {

                            ...nextSummary,

                            dueAssets: assetsDueBeyondHorizon,
                            dueRequirements: requirementsDueBeyondHorizon,
                            dueProducts: productsDueBeyondtHorizon

                        }
                    }



                    if (excludeList.includes(party.source))
                        summaries.get()[party.source] = summary
                    else
                        summaries.add(party.source, summary)


                })

                return summaries.done()

            }
               


            const self = {

               
                trailSummary: trailsummary.on(c),


                trailSummaryNoDeps: trailsummaryND.on(c),


                // the trail summaries required to calculate statistics across all parties.
                // includes all parties in the campaign, whether they have some trails or not.
                // intended for caching, it can be passed to allPartiesSummary below.
                partiesWithTrailSummaries: (trailSummaries: Record<string, TrailSummary[]>) => {

                    // pairs each party with the summaries of its trails.
                    return partyinststore.on(c).all().map(party => [party, trailSummaries[party.source] ?? []] as [PartyInstanceDto, TrailSummary[]])

                }

                ,

                // calculate stats across all parties based on a pre-computed list of trail summaries.
                allPartiesSummary


                ,

                //  builds summaries for all requirements by iterating and aggregating over trail summaries.
                //  pre-computes as much as possible.

                allRequirementsSummary: (trailSummaries: Record<string, TrailSummary[]>, dueDates: Record<string, string>) => {

                    const assets = reqinststore.on(c).all()

                    return allAssetsSummary(assets, trailSummaries, dueDates)

                }

                ,

                allProductsSummary: (trailSummaries: Record<string, TrailSummary[]>, dueDates: Record<string, string>) => {

                    return allAssetsSummary(prodinstore.on(c).all(), trailSummaries, dueDates)

                }


            }

            return self
        }
    }
}