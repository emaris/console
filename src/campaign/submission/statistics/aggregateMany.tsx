import moment from 'moment-timezone'
import { SubmissionStub } from '../model'
import { AggregateSummary, emptyGroups, Groups } from './aggregateOne'
import { TrailSummary } from './trail'
import { roundPercent } from './api'

//  a collection of aggregate summary, indexed by groups submission and compliance groups.
// use case: presentation of statistics (pie chart, groups)
export type AggregateSummaries<T extends AggregateSummary = AggregateSummary> = Record<string, T> & {


    // totals across all summaries
    dueSubmissions: number                                     // expected submissions.

    submitted: number                                         // actual submissions.  
    submittedRequirements: number
    submittedProducts: number

    submittedAsMissing: number                                // submission placeholders.

    assessable: number                                         // actual submissions eligible for assessment. 
    assessed: number                                          // assessed submissions, actual or placeholders.

    pending: number                                           // party work still waiting for internal approval. 
    drafts: number                                            // poarty work still in progress.

    // combined late functions
    late: () => TrailSummary[]                                // submissions that are late at the point of asking.


    submissionGroups: Groups<T>                               // all summaries indexed by their submission group.                
    complianceGroups: Groups<T>                               // all summaries indexed byt their compliance group



    singleSubmissionRate: number
    singleSubmissionClass: string

    singleMissingRate: number

    singleAssessmentRate: number
    singleAssessmentClass: string

    singleComplianceRate: number
    singleComplianceClass: string

    singleLateRate: () => number

    lastSubmitted: SubmissionStub | undefined                   // most recently submitted.
    lastSubmittedRequirement: SubmissionStub | undefined
    lastSubmittedProduct: SubmissionStub | undefined

}

export const initialSummaries = <T extends AggregateSummary>() => ({

    submissionGroups: emptyGroups<T>(),
    complianceGroups: emptyGroups<T>(),

    dueSubmissions: 0,
    submitted: 0,
    submittedAsMissing: 0,
    submittedRequirements: 0,
    submittedProducts: 0,
    assessed: 0,
    assessable: 0,
    pending: 0,
    drafts: 0,

    singleSubmissionRate: 0,
    singleAssessmentRate: 0,
    singleComplianceRate: 0

}) as AggregateSummaries<T>



export const aggregateMany = <T extends AggregateSummary>() => {

    const summaries = initialSummaries<T>()

    var withComplianceRate = 0
    var withSubmissionRate = 0

    const lateFunctions = [] as (() => TrailSummary[])[]

    return {

        add: (id: string, summary: T) => {

            summaries.dueSubmissions += summary.dueSubmissions

            // "adds up" all stats.
            summaries.submitted += summary.submitted.length
            summaries.submittedRequirements += summary.submittedRequirements.length
            summaries.submittedProducts += summary.submittedProducts.length

            summaries.submittedAsMissing += summary.submittedAsMissing.length

            summaries.drafts += summary.drafts.length
            summaries.pending += summary.pending.length
            summaries.assessable += summary.assessable.length
            summaries.assessed += summary.assessed.length

            // SUBMISSION
            // acumulates rates, will normalize later.
            if (!isNaN(summary.submissionProfile.rate)) {

                withSubmissionRate += summary.dueSubmissions
                summaries.singleSubmissionRate += summary.submitted.length

                //  collects into groups
                summaries.submissionGroups[summary.submissionProfile.submissionGroup].push(summary)
            }


            // COMPLIANCE
            // acumulates rates, will normalize later.
            if (!isNaN(summary.complianceProfile.rate)) {

                withComplianceRate += summary.complianceProfile?.aggregateWeightSum
                summaries.singleComplianceRate += summary.complianceProfile?.aggregateRate

                //  collects into groups
                summaries.complianceGroups[summary.complianceProfile.group].push(summary)
            }

            lateFunctions.push(summary.late)


            const lastSubmittedDate = summary.lastSubmitted?.lifecycle.lastSubmitted
            const lastSubmittedRequirementDate = summary.lastSubmittedRequirement?.lifecycle.lastSubmitted
            const lastSubmittedProductDate = summary.lastSubmittedProduct?.lifecycle.lastSubmitted

            if (lastSubmittedDate && (!summaries.lastSubmitted || moment(lastSubmittedDate).isAfter(summaries.lastSubmitted.lifecycle.lastSubmitted)))
                summaries.lastSubmitted = summary.lastSubmitted

            if (lastSubmittedRequirementDate && (!summaries.lastSubmittedRequirement || moment(lastSubmittedRequirementDate).isAfter(summaries.lastSubmittedRequirement.lifecycle.lastSubmitted)))
                summaries.lastSubmittedRequirement = summary.lastSubmittedRequirement

            if (lastSubmittedProductDate && (!summaries.lastSubmittedProduct || moment(lastSubmittedProductDate).isAfter(summaries.lastSubmittedProduct.lifecycle.lastSubmitted)))
                summaries.lastSubmittedProduct = summary.lastSubmittedProduct

            summaries[id] = summary

        }

        ,

        get: () => summaries

        ,

        done: () => {

            summaries.singleSubmissionRate = roundPercent(summaries.singleSubmissionRate * 100 ,withSubmissionRate)

            const singleSubmissionGroup = isNaN(summaries.singleSubmissionRate) ? 4 : Math.floor(summaries.singleSubmissionRate / 33.1 + 1)
            summaries.singleSubmissionClass = `submission-${singleSubmissionGroup}`

            summaries.singleMissingRate = Math.round(summaries.submittedAsMissing / summaries.dueSubmissions)

            // recompute for assessment, more accurate than averaging. 
            summaries.singleAssessmentRate = Math.round(summaries.assessed * 100 / summaries.assessable)
            const singleAssessmentGroup = isNaN(summaries.singleAssessmentRate) ? 4 : Math.floor(summaries.singleAssessmentRate / 33.1 + 1)
            summaries.singleAssessmentClass = `assessment-${singleAssessmentGroup}`

            summaries.singleComplianceRate = roundPercent(summaries.singleComplianceRate,withComplianceRate)
            const singleComplianceGroup = isNaN(summaries.singleComplianceRate) ? 4 : Math.floor(summaries.singleComplianceRate / 49.1 + 2)
            summaries.singleComplianceClass = `compliance-${singleComplianceGroup}`

            summaries.late = () => lateFunctions.flatMap(fun => fun())
            summaries.singleLateRate = () => summaries.late().length / summaries.submitted

            return summaries

        }
    }
}