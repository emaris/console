import { Button } from '#app/components/Button'
import { Paragraph, Text } from '#app/components/Typography'
import { icns } from '#app/icons'
import { useT } from '#app/intl/api'
import { useLayoutSettings } from '#layout/settings/api'
import { LifecycleSummary } from '#app/model/lifecycle'
import { useTime } from '#app/time/api'
import { UserLabel } from '#app/user/Label'
import { Tooltip } from 'antd'
import moment from 'moment-timezone'
import * as React from 'react'
import { SubmissionLabel } from './Label'
import { useSubmissionContext } from './hooks'
import { SubmissionStub } from './model'

export type Props = {

    submission: SubmissionStub
    onDateChange?: () => void
}

export const SubmissionLifecycle = ($: Props) => {

    const t = useT()

    const { ll } = useLayoutSettings()

    const time = useTime()

    const { submission, onDateChange } = $

    const lc = submission.lifecycle

    const ctx = useSubmissionContext()

    return <LifecycleSummary {...submission.lifecycle}>

        <br />

        {(lc.lastEdited ?? lc.lastModified) && <>
            <Paragraph ><Text smaller>{t("submission.labels.last_edited")}</Text></Paragraph>
            <Paragraph>
                <Text smaller className="emphasis">
                    <Tooltip overlayStyle={{ fontSize: 'small' }} title={time.format(moment(lc.lastEdited ?? lc.lastModified))}>{time.format(moment(lc.lastEdited ?? lc.lastModified), 'short')}</Tooltip>
                </Text>
            </Paragraph>
        </>
        }

        {(lc.lastEditedBy ?? lc.lastModifiedBy) &&

            <>

                <Paragraph ><Text smaller>{t("submission.labels.last_edited_by")}</Text></Paragraph>
                <Paragraph>
                    <Text style={{ display: "flex", justifyContent: "flex-end" }} smaller className="emphasis">
                        <UserLabel noIcon user={lc.lastEditedBy ?? lc.lastModifiedBy} />
                    </Text>
                </Paragraph>

            </>}

        {(lc.lastSubmitted || lc.lastApprovedBy) &&

            <br />

        }

        {lc.lastSubmitted &&

            <React.Fragment>

                <Paragraph ><Text smaller>{t("submission.labels.last_submitted")}</Text></Paragraph>
                <Paragraph>
                    <Button disabled={!onDateChange} enabledOnReadOnly tooltip={t('submission.datechange.open_lbl')} className='submissiondate-edit' type='ghost' icn={icns.edit} onClick={onDateChange} />
                    <Text smaller className="emphasis">
                        <Tooltip overlayStyle={{ fontSize: 'small' }} title={time.format(moment(lc.lastSubmitted))}>{time.format(lc.lastSubmitted, 'short')}</Tooltip>
                    </Text>
                </Paragraph>

            </React.Fragment>
        }

        {lc.lastApprovedBy &&

            <React.Fragment>

                <Paragraph ><Text smaller>{t("submission.labels.approved_by")}</Text></Paragraph>
                <Paragraph>
                    <Text style={{ display: "flex", justifyContent: "flex-end" }} smaller className="emphasis">
                        <UserLabel noIcon user={lc.lastApprovedBy} />
                    </Text>
                </Paragraph>
            </React.Fragment>
        }

        {lc.reference &&
            <>
                <Paragraph ><Text smaller>{t("submission.reference.label")}</Text></Paragraph>
                <Paragraph>
                    <Text style={{ display: "flex", justifyContent: "flex-end" }} smaller className="emphasis">
                        {ll(lc.reference ?? {})}
                    </Text>
                </Paragraph>

            </>
        }

        {(lc.lastPublished || lc.lastPublishedBy) &&

            <br />

        }

        {lc.lastPublished &&

            <React.Fragment>

                <Paragraph ><Text smaller>{t("submission.labels.last_published")}</Text></Paragraph>
                <Paragraph>
                    <Text smaller className="emphasis">
                        <Tooltip overlayStyle={{ fontSize: 'small' }} title={time.format(moment(lc.lastPublished))}>{time.format(lc.lastPublished, 'short')}</Tooltip>
                    </Text>
                </Paragraph>

            </React.Fragment>
        }

        {lc.lastPublishedBy &&

            <React.Fragment>

                <Paragraph ><Text smaller>{t("submission.labels.published_by")}</Text></Paragraph>
                <Paragraph>
                    <Text style={{ display: "flex", justifyContent: "flex-end" }} smaller className="emphasis">
                        <UserLabel noIcon user={lc.lastPublishedBy} />
                    </Text>
                </Paragraph>
            </React.Fragment>
        }

        {lc.state && ctx.isSaved() &&

            <React.Fragment>
                <br />
                <Paragraph ><Text smaller>{t("submission.labels.submission_state")}</Text></Paragraph>
                <Paragraph>
                    <Text style={{ display: "flex", justifyContent: "flex-end" }} smaller className="emphasis">
                        <SubmissionLabel noIcon displayMode="state-only" mode='light' submission={submission} />
                    </Text>
                </Paragraph>
                <br />


            </React.Fragment>

        }

        {lc.compliance?.state &&
            <React.Fragment>

                <Paragraph ><Text smaller>{t("submission.labels.compliance_state")}</Text></Paragraph>
                <Paragraph>
                    <Text style={{ display: "flex", justifyContent: "flex-end" }} smaller className="emphasis">
                        <SubmissionLabel displayMode='compliance' noIcon mode='light' submission={submission} />
                    </Text>
                </Paragraph>
            </React.Fragment>
        }

        {lc.compliance?.lastAssessedBy &&
            <React.Fragment>
                <Paragraph ><Text smaller>{t("submission.labels.assessed_by")}</Text></Paragraph>
                <Paragraph>
                    <Text style={{ display: "flex", justifyContent: "flex-end" }} smaller className="emphasis">
                        <UserLabel noIcon user={lc.compliance.lastAssessedBy} />
                    </Text>
                </Paragraph>
            </React.Fragment>
        }

        {lc.compliance?.lastAssessed &&
            <React.Fragment>
                <Paragraph ><Text smaller>{t("submission.labels.last_assessed")}</Text></Paragraph>
                <Paragraph>
                    <Text smaller className="emphasis">
                        <Tooltip overlayStyle={{ fontSize: 'small' }} title={time.format(moment(lc.compliance.lastAssessed))}>{time.format(lc.compliance.lastAssessed, 'short')}</Tooltip>
                    </Text>
                </Paragraph>
            </React.Fragment>
        }

    </LifecycleSummary>

}
