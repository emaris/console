
import { Label, LabelProps } from "#app/components/Label"
import { icns } from "#app/icons"
import { useT } from '#app/intl/api'
import { useIntl } from '#app/intl/store'
import { useLocale } from "#app/model/hooks"
import { TagList } from "#app/tag/Label"
import { useTenantStore } from '#app/tenant/store'
import { TimeLabel } from "#app/time/Label"
import { useTime } from "#app/time/api"
import { UserLabel } from "#app/user/Label"
import { PartyInstanceLabel } from '#campaign/party/Label'
import { usePartyInstances } from '#campaign/party/api'
import { eventIcon, missingIcon } from "#event/constants"
import { NotAssessableLabel } from '#requirement/Label'
import { requirementType } from "#requirement/constants"
import moment from "moment-timezone"
import * as React from "react"
import { complianceIcon, timelinessIcon } from "../constants"
import { useCurrentCampaign } from "../hooks"
import { useProductInstances } from '../product/api'
import { useRequirementInstances } from '../requirement/api'
import { manageIcon, submissionIcon, submissionSingular } from "./constants"
import { ComplianceProfile, SubmissionStub, TimelinessProfile } from "./model"
import { useSubmissionProfile } from './profile'
import { useSubmissionStore } from './store'


type Props = LabelProps & {

    submission: SubmissionStub

    tipMode?: 'party' | 'asset' | 'author'
    displayMode?: 'submission' | 'party' | 'asset' | 'asset-tags' | 'date' | 'date-pill' | 'date-only' | 'date-change' | 'compliance' | 'timeliness' | 'compliance-rating' | 'state-only'

    noDateAccent?: boolean

    showDateWithTime?: boolean

    computedTimeliness?: 'ontime' | 'late' | 'notdue'

    noPseudoNames?: boolean

}

// reprents a submission in terms of its status or its submission/editing date.

export const SubmissionLabel = (props: Props) => {

    const t = useT()
    const { l } = useLocale()

    const { currentLanguage } = useIntl()

    const time = useTime()
    const tenants = useTenantStore()

    const campaign = useCurrentCampaign()
    const campaignTimeZone = campaign.properties.timeZone ? { 'original': campaign.properties.timeZone } : undefined

    const parties = usePartyInstances().on(campaign)
    const requirement = useRequirementInstances().on(campaign)
    const product = useProductInstances().on(campaign)

    const { submission, tip, tipMode, displayMode = 'date', mode, className = '', noPseudoNames, noDateAccent = false, computedTimeliness, showDateWithTime=false, ...rest } = props

    const substore = useSubmissionStore().on(campaign)
    const subprofile = useSubmissionProfile().on(campaign)

    return React.useMemo(() => {


        const state = submission.lifecycle.state
        const classNames = `submission-label ${displayMode} ${state} ${className}`

        const now = time.current()

        const submissionDate = submission.lifecycle.lastSubmitted

        // if submitted, then submission icon (date view) or event icon (status view). otherwise, icon matches state.
        const stateAwareIcon = submissionDate ?

           ( state === 'missing' ? missingIcon :
                state === 'managed' ? manageIcon
                    : state === 'published' ? icns.publish
                        : displayMode === 'state-only' ? submissionIcon : eventIcon)

            : 
            
            (state === 'draft' ? icns.edit
                : state === 'pending' ? icns.pause
                    : state === 'published' ? icns.publish

                        : submissionIcon)

        if (displayMode === 'submission')

            return <Label className={classNames} icon={submissionIcon} title={t(submissionSingular)} {...rest} />


        if (displayMode === 'state-only') {

            const sotip = tip ?? (() =>


                tipMode === 'author' ? <UserLabel user={submission.lifecycle.state === 'published' ? submission.lifecycle.lastPublishedBy : submission.lifecycle.lastApprovedBy ?? submission.lifecycle.lastModifiedBy} />

                    :

                    submissionDate ? t("submission.labels.submitted_since", { since: moment(submissionDate).from(now) })

                        :

                        t("submission.labels.last_edited", { since: moment(submission.lifecycle.lastEdited ?? submission.lifecycle.lastModified  ?? now).from(now) })

            )

            const sotitle = t(`submission.status.${state}`)

            return <Label className={classNames} tip={sotip} icon={stateAwareIcon} title={sotitle} mode={mode ?? 'tag'} {...rest} />

        }

        const trail = substore.lookupTrail(submission.trail)

        if (displayMode === 'compliance' || displayMode === 'compliance-rating' || displayMode === 'timeliness') {

            const profile = (displayMode === 'timeliness' ? substore.timelinessProfile(submission, computedTimeliness) : substore.complianceProfile(submission)) ?? {} as ComplianceProfile | TimelinessProfile

            const icon = displayMode === 'timeliness' ? timelinessIcon : complianceIcon
            const judgement = displayMode === 'timeliness' ? profile.name : profile.name

            const profileClass = displayMode === 'timeliness' ? (profile as TimelinessProfile).timelinessClass : (profile as ComplianceProfile).complianceClass

            //console.log(submission, submissions.lookupTrail(submission.trail), requirement.all())
            // const trail = submissions.lookupTrail(submission.trail)
            const instance = trail?.key.assetType === requirementType ? requirement.lookupBySource(trail.key.asset) : product.lookupBySource(trail?.key.asset)

            const canBeAssessed = instance?.properties.assessed ?? true

            // const index = allNames?.findIndex(n => n.en === judgement?.en)

            const revoked = submission.lifecycle.revokedCompliance?.state

            const revokedSubmission = { ...submission, lifecycle: { ...submission.lifecycle, compliance: submission.lifecycle.revokedCompliance, revokedCompliance: undefined! } }
            const revokedProfile = revoked ? displayMode === 'timeliness' ? substore.timelinessProfile(revokedSubmission, computedTimeliness) : substore.complianceProfile(revokedSubmission) : undefined

            const cotip = revoked ? 
            
                () => t('submission.labels.revoked_from', { from: l(revokedProfile?.name) }) :

                (judgement && submission.lifecycle.compliance?.lastAssessed) ?
                    () => t('submission.labels.last_assessed_since', { since: moment(submission.lifecycle.compliance?.lastAssessed).from(now) }) :

                undefined

            const cotitle = revoked ? t("submission.labels.revoked") : l(judgement) ?? t("submission.labels.not_assessed")

            return canBeAssessed ?

                <Label className={`${classNames} ${profileClass ?? ''}`} tip={cotip} icon={icon} title={cotitle} mode={mode ?? 'tag'} {...rest} />

                :

                <NotAssessableLabel />

            // retracted:  doesn't cover scales where multiple levels have same values (eg unweighed schemes).
            //         :
            //         // compliance-rating
            //         <Tooltip title={l(tag?.name)}>
            //             {/* <Rate> seems to override mouse event listeners for its own tooltips? So we interject a <div>. */}
            //             <span>
            //                 <Rate className={`${classNames} ${complianceClass ?? ''}`} disabled
            //                     count={allNames?.length ?? 0}
            //                     character={icns.dot}
            //                     defaultValue={(index ?? 0) + 1} />
            //             </span>
            //         </Tooltip>
            // 

        }

        if (displayMode === 'asset-tags') {
            return <React.Fragment>{trail && <TagList taglist={subprofile.profileOf(trail.key.assetType).source(trail.key.party).tags}></TagList>}</React.Fragment>
        }

        let computedtip = tip;

        if (trail && !computedtip) {

            switch (tipMode) {

                case 'asset': {

                    const profile = subprofile.profileOf(trail.key.assetType)
                    computedtip = () => profile.label(profile.instance(trail.key.asset), { linkTo: props.linkTo })
                    break
                }

                case 'party': {
                    const party = parties.safeLookupBySource(trail.key.party)
                    computedtip = () => <PartyInstanceLabel linkTo={props.linkTo} instance={party} />
                    break
                }

                case 'author':
                    computedtip = () => <UserLabel user={submission.lifecycle.lastApprovedBy ?? submission.lifecycle.lastModifiedBy} />


            }

        }

        if (!computedtip) {

            switch (state) {

                case 'published': computedtip = () => <TimeLabel className={classNames} displayMode="relative" accentFrame="days" accentMode="record" noGreyAccent={noDateAccent} icon={stateAwareIcon} value={moment(submission.lifecycle.lastPublished)} mode={mode} timezones={campaignTimeZone} {...rest} />
                    break
                case 'managed': computedtip = () => <TimeLabel className={classNames} displayMode="relative" accentFrame="days" accentMode="record" noGreyAccent={noDateAccent} icon={stateAwareIcon} value={moment(submission.lifecycle.lastModified)} mode={mode} timezones={campaignTimeZone} {...rest} />
                    break
                case 'submitted': computedtip = () => <TimeLabel className={classNames} displayMode="relative" accentFrame="days" accentMode="record" noGreyAccent={noDateAccent} icon={stateAwareIcon} value={moment(submission.lifecycle.lastModified)} mode={mode} timezones={campaignTimeZone} {...rest} />
            }
        }

        const getTimeLabel = (submission: SubmissionStub, rest: any) => {

            const value = submission.lifecycle.lastPublished ?? submissionDate ?? moment(submission.lifecycle.lastModified)

            return <TimeLabel className={classNames} format='numbers' accentFrame="days" accentMode="record" withTime={showDateWithTime} noGreyAccent={noDateAccent} icon={stateAwareIcon} value={value} mode={mode} timezones={campaignTimeZone} {...rest} />
        }

        if (displayMode === 'date-only' && (submissionDate || noPseudoNames)) {

            return getTimeLabel(submission, rest)
        }

        if (displayMode === 'date-change') {

            const value = submission.lifecycle.lastPublished ?? submission.lifecycle.lastEdited ?? moment(submission.lifecycle.lastModified)

            return  <TimeLabel className={classNames} format='numbers' accentFrame="days" accentMode="record" noGreyAccent={noDateAccent} icon={stateAwareIcon} value={value} mode={mode} timezones={campaignTimeZone} {...rest} />
        
        }


        if ((displayMode === 'date' || displayMode === 'date-pill') && (!substore.isManagedState(submission) || noPseudoNames) && (submissionDate || noPseudoNames)) {

            let label = getTimeLabel(submission, rest)

            if ( displayMode === 'date-pill' && (state === 'published' || state === 'managed')) {
                label = React.cloneElement(label, {icon: eventIcon, prefix: `${t(`submission.status.submitted_on`)}`})
            }

            return label

        }

        let title

        if (trail) {

            switch (displayMode) {

                case 'asset': {

                    const profile = subprofile.profileOf(trail.key.assetType)
                    title = l(profile.source(trail.key.asset).name)
                    break;
                }
                case 'party': {

                    title = l(tenants.safeLookup(trail.key.party).name)
                    break;
                }

                default: {

                    title = submission.lifecycle.reference ?

                        <div className='submission-title'>{t(`submission.status.${state}`)} <span className={state}>({l(submission.lifecycle.reference)})</span></div>
                        :

                        t(`submission.status.${state}`)
                }
            }

        }

        return <Label className={classNames} tip={computedtip} icon={stateAwareIcon} title={title} mode={mode} {...rest} />


        // eslint-disable-next-line
    }, [submission, currentLanguage])

}