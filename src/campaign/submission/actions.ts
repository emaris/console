


import { any } from "#app/iam/model"

import { productSubmissionType, requirementSubmissionType, submissionIcon } from "./constants"

const baseRequirementAction = { icon:submissionIcon, type: requirementSubmissionType, resource :any }

export const requirementSubmissionActions  = {  
    
    manage:  {...baseRequirementAction, labels:["manage"], shortName:"submission.actions.requirements.manage.short",name:"submission.actions.requirements.manage.name", description: "submission.actions.requirements.manage.desc"}
    ,
    edit:  {...baseRequirementAction, labels:["manage","edit"], shortName:"submission.actions.requirements.edit.short",name:"submission.actions.requirements.edit.name", description: "submission.actions.requirements.edit.desc"}
    ,
    submit:  {...baseRequirementAction, labels:["manage","submit"], shortName:"submission.actions.requirements.submit.short",name:"submission.actions.requirements.submit.name", description: "submission.actions.requirements.submit.desc"}
    ,
    message:  {...baseRequirementAction, labels:["manage","message"], shortName:"submission.actions.requirements.message.short",name:"submission.actions.requirements.message.name", description: "submission.actions.requirements.message.desc"}
   
}

const baseProductAction = { icon:submissionIcon, type: productSubmissionType, resource :any }

export const productSubmissionActions = {  
    
    manage:  {...baseProductAction, labels:["manage"], shortName:"submission.actions.products.manage.short",name:"submission.actions.products.manage.name", description: "submission.actions.products.manage.desc"}
    ,
    edit:  {...baseProductAction, labels:["manage","edit"], shortName:"submission.actions.products.edit.short",name:"submission.actions.products.edit.name", description: "submission.actions.products.edit.desc"}
    ,
    submit:  {...baseProductAction, labels:["manage","submit"], shortName:"submission.actions.products.submit.short",name:"submission.actions.products.submit.name", description: "submission.actions.products.submit.desc"}
    ,
    message:  {...baseProductAction, labels:["manage","message"], shortName:"submission.actions.products.message.short",name:"submission.actions.products.message.name", description: "rsubmission.actions.products.message.desc"}
   
}