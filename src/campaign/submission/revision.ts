import { useLogged } from '#app/user/store'
import { compareDates } from '../../app/utils/common'
import { SubmissionStatus, SubmissionStub, Trail } from './model'


export const useRevisions = () => {

    const logged = useLogged()

    return {

        on: (trail: Trail | undefined) => {

            const self = {

                // null-last => draft-last.
                ascendingOrder: (o1: SubmissionStub, o2: SubmissionStub) => compareDates(o1.lifecycle.lastSubmitted, o2.lifecycle.lastSubmitted, false) || compareDates(o1.lifecycle.lastModified, o2.lifecycle.lastModified, false),

                // invert comparisons => draft-first.
                descendingOrder: (o1: SubmissionStub, o2: SubmissionStub) => self.ascendingOrder(o2, o1),


                lastChanged: (visibility: 'tenancy' | 'notenancy' = 'tenancy') =>{
                    
                    const bylastchange = [...trail?.submissions ?? []]
                                        .filter(s => visibility === 'tenancy' && logged.isTenantUser() ? s.lifecycle.state !== 'managed' : true)
                                        .sort((o1,o2) => compareDates(o2.lifecycle.lastModified, o1.lifecycle.lastModified))

                    console.log(bylastchange)
                    
                    return bylastchange[0]
}
                ,

                all: (order: 'asc' | 'desc' = 'desc', visibility: 'tenancy' | 'notenancy' = 'tenancy'): SubmissionStub[] =>

                    [...trail?.submissions ?? []]

                        .filter(s => visibility === 'tenancy' && logged.isTenantUser() ? s.lifecycle.state !== 'managed' : true)
                        .sort(order === 'asc' ? self.ascendingOrder : self.descendingOrder)

                ,

                submittedGroupOf: (sub: SubmissionStub) => self.submittedGroups().find(g => g.some(s => s.id === sub.id))

                ,

                submittedGroups: (order: 'asc' | 'desc' = 'desc') => {

                    return self.all(order).filter(r => r.lifecycle.lastSubmitted).reduce((acc, next) => {

                        const group = acc[acc.length - 1] ?? []

                        if (next.lifecycle.lastSubmitted === group[0]?.lifecycle.lastSubmitted) {
                            group.push(next)
                            return acc
                        }

                        else return [...acc, [next]]


                    }, [] as SubmissionStub[][])

                }

                ,

                latest: (state?: SubmissionStatus) => self.all().find(s => state ? s.lifecycle.state === state : true)

                ,

                // most recent in the most recent submitted group.
                active: () => self.submittedGroups()[0]?.[0]

                ,

                isActive: (sub: SubmissionStub) => self.active()?.id === sub.id


                ,

                isSuperseded: (sub: SubmissionStub) => !self.submittedGroups()?.[0]?.some(s => s.id === sub.id)

                ,

                official: () => self.allOfficial()[0]

                ,

                allOfficial: () => self.all().filter(r => ['published', 'submitted', 'missing'].includes(r.lifecycle.state!))


                ,

                isOfficial: (sub: SubmissionStub) => self.official()?.id === sub.id



                ,

                isLatestInGroup: (sub: SubmissionStub) => !!self.submittedGroups().find(g => g[0]?.id === sub.id)


                ,

                previous: (sub: SubmissionStub) => {

                    const all = self.all()

                    return all[all.findIndex(s => s.id === sub.id) + 1]

                }

                ,


                next: (sub: SubmissionStub) => {

                    const all = self.all()

                    return all[all.findIndex(s => s.id === sub.id) - 1]

                }


            }

            return self
        }
    }
}