import { UnknownLabel } from '#app/components/Label';
import { specialise } from '#app/iam/model';
import { Layout } from '#layout/model';
import { TenantDto } from "#app/tenant/model";
import { useLogged } from '#app/user/store';
import { useEventInstanceStore } from '#campaign/event/store';
import { productActions } from '#product/actions';
import { productType } from "#product/constants";
import { ProductDto } from '#product/model';
import { useProductModule } from "#product/module";
import { requirementActions } from '#requirement/actions';
import { requirementType } from "#requirement/constants";
import { RequirementDto } from '#requirement/model';
import { useRequirementModule } from "#requirement/module";
import { InstanceLabelProps } from "../InstanceLabel";
import { AssetInstance, Campaign, CampaignInstance } from "../model";
import { PartyInstance } from "../party/api";
import { ProductInstance, useProductInstances } from "../product/api";
import { ProductInstanceLabel } from "../product/Label";
import { RequirementInstance, useRequirementInstances } from "../requirement/api";
import { RequirementInstanceLabel } from "../requirement/Label";
import { productSubmissionActions, requirementSubmissionActions } from './actions';
import { submissionType } from './constants';
import { Tag } from '#app/tag/model';
import { complianceValueCustomProp, timelinessValueCustomProp } from '#campaign/constants';


export const useSubmissionProfile = () => {

    const prodinst = useProductInstances()
    const reqinst = useRequirementInstances()

    const eventinst = useEventInstanceStore()

    const logged = useLogged()

    return {

        on: (c: Campaign) => {

            const reqinststore = reqinst.on(c)
            const prodinststore = prodinst.on(c)
            const eventinststore = eventinst.on(c)

            const self = {

                profileOf: (type: string | undefined) => {

                    switch (type) {

                        case requirementType: return {

                            module: () => useRequirementModule,
                            allSorted: reqinststore.allSorted,
                            allSources: reqinststore.allSource,
                            sourceOf: reqinststore.lookupSource,
                            layoutOf: (source: string) => reqinststore.lookupSource(source).properties.layout,
                            layoutParameters: (ci: CampaignInstance) => reqinststore.lookupSource(ci).properties.layout.parameters,
                            allForParty: (party: PartyInstance) => reqinststore.allForParty(party),
                            isForParty: (party: PartyInstance | TenantDto, ci: CampaignInstance) => reqinststore.isForParty(party, ci),
                            stringify: (ci: CampaignInstance) => reqinststore.stringify(ci as RequirementInstance),
                            stringifySource: (source: string) => reqinststore.stringifySourceRef(source),
                            nameOf: (ci: CampaignInstance) => reqinststore.nameOf(ci as ProductInstance),
                            instance: (source: string) => reqinststore.safeLookupBySource(source),
                            source: reqinststore.lookupSource,
                            deadlineOf: (ci: CampaignInstance) => eventinststore.deadlineOfRequirement(ci as RequirementInstance),
                            dueDateOf: (ci: CampaignInstance) => eventinststore.deadlineDateOfRequirement(ci as RequirementInstance),
                            isDue: (ci: CampaignInstance) => eventinststore.isRequirementDue(ci as RequirementInstance),
                            allDueDates: eventinststore.allDueDatesByRequirementId,
                            label: (ci: CampaignInstance, props?: InstanceLabelProps & { [key: string]: any }) => <RequirementInstanceLabel tipTitle instance={ci as RequirementInstance} linkTarget={submissionType} {...props} />,
                            route: (ci: CampaignInstance) => reqinststore.route(ci as RequirementInstance),
                            topics: (ci: AssetInstance) => reqinststore.topics(ci) ,
                           
                            isSubmissionManager: (ci: CampaignInstance) => {

                                return logged.can(specialise(requirementActions.edit, ci.source)) || logged.can(specialise(requirementSubmissionActions.manage, ci.source))
                            }

                            ,

                            isEditor: (ci: CampaignInstance) => {

                                return logged.can(specialise(requirementActions.edit, ci.source)) ||  logged.can(specialise(requirementSubmissionActions.edit, ci.source))
                            }

                            ,


                            isSubmitter: (ci: CampaignInstance) => {

                                return logged.can(specialise(requirementActions.edit, ci.source)) || logged.can(specialise(requirementSubmissionActions.submit, ci.source))
                            }

                            ,

                            isAssessor: (ci: CampaignInstance) => {

                                return logged.can(specialise(requirementActions.edit, ci.source)) || logged.can(specialise(requirementActions.assess, ci.source))
                            }

                            ,

                            isInUserProfile: (ci: AssetInstance) => {

                                return reqinststore.matches(ci as ProductInstance, logged)
                             
                            }

                            ,

                            canMessage: (ci: AssetInstance) => {

                                return logged.can(specialise(requirementActions.edit, ci.source)) || logged.can(specialise(requirementActions.assess, ci.source)) || logged.can(specialise(requirementSubmissionActions.message, ci.source)) || logged.hasNoTenant()
                            }


                        }

                        case productType: return {

                            module: () => useProductModule,
                            allSorted: prodinststore.allSorted,
                            allSources: prodinststore.allSources,
                            sourceOf: prodinststore.lookupSource,
                            layoutOf: (source: string) => prodinststore.lookupSource(source).properties.layout,
                            layoutParameters: (ci: CampaignInstance) => prodinststore.lookupSource(ci.source).properties.layout.parameters,
                            allForParty: (party: PartyInstance) => prodinst.on(c).allForParty(party),
                            isForParty: (party: PartyInstance | TenantDto, ci: CampaignInstance) => prodinststore.isForParty(party, ci),
                            stringify: (ci: CampaignInstance) => prodinststore.stringify(ci as ProductInstance),
                            stringifySource: (source: string) => prodinststore.stringifySourceRef(source),
                            nameOf: (ci: CampaignInstance) => prodinststore.nameOf(ci as ProductInstance),
                            instance: (source: string) => prodinststore.safeLookupBySource(source),
                            source: prodinststore.lookupSource,
                            label: (ci: CampaignInstance, props?: InstanceLabelProps & { [key: string]: any }) => <ProductInstanceLabel tipTitle instance={ci as ProductInstance} {...props} />,
                            dueDateOf: (ci: CampaignInstance) => eventinststore.deadlineDateOfProduct(ci as RequirementInstance),
                            deadlineOf: (ci: CampaignInstance) => eventinststore.deadlineOfProduct(ci as ProductInstance),
                            isDue: (ci: CampaignInstance) => eventinststore.isProductDue(ci as ProductInstance),
                            allDueDates: eventinststore.allDueDatesByProductId,
                            route: (ci: CampaignInstance) => prodinststore.route(ci as ProductInstance),
                            topics: (ci: AssetInstance) => prodinststore.topics(ci) ,

                            isSubmissionManager: (ci: CampaignInstance) => {

                                return logged.can(specialise(productActions.manage, ci.source)) || logged.can(specialise(productSubmissionActions.manage, ci.source))
                            }
                            ,

                            isEditor: (ci: CampaignInstance) => {

                                return logged.can(specialise(productActions.edit, ci.source)) || logged.can(specialise(productSubmissionActions.edit, ci.source))
                            }

                            ,

                            isSubmitter: (ci: CampaignInstance) => {

                                return logged.can(specialise(productActions.edit, ci.source)) || logged.can(specialise(productSubmissionActions.submit, ci.source))
                            }

                            ,

                            isAssessor: (ci: CampaignInstance) => {

                                return logged.can(specialise(productActions.edit, ci.source)) || logged.can(specialise(productActions.assess, ci.source))
                            }

                            ,


                            isInUserProfile: (ci: AssetInstance) => {

                                return prodinststore.matches(ci as ProductInstance, logged)

                            }

                            ,

                            canMessage: (ci: AssetInstance) => {

                                return logged.can(specialise(productActions.edit, ci.source)) || logged.can(specialise(productActions.assess, ci.source)) || logged.can(specialise(productSubmissionActions.message, ci.source)) || logged.hasNoTenant()

                            }

                        }

        
                        default: return {

                            module: () => useRequirementModule,
                            allSorted: () => [],
                            stringify: () => '',
                            stringifySource: (_: string) => '',
                            nameOf: () => '',
                            allSources: () => [],
                            sourceOf: (_: string) => { },
                            layoutOf: () => undefined! as Layout,
                            layoutParameters: (ci: CampaignInstance) => [],
                            allForParty: (_: PartyInstance) => [],
                            isForParty: (_: PartyInstance | TenantDto, __: CampaignInstance) => false,
                            instance: (_: string) => undefined as unknown as AssetInstance,
                            source: (_: string) => undefined as unknown as ProductDto | RequirementDto,
                            label: (ci: CampaignInstance, props?: InstanceLabelProps & { [key: string]: any }) => <UnknownLabel tip={() => ci.id} {...props} />,
                            deadlineOf: (ci: CampaignInstance) => undefined,
                            dueDateOf: (ci: CampaignInstance) => undefined,
                            isDue: (ci: CampaignInstance) => false,
                            allDueDates: () => [],
                            route: (_: CampaignInstance) => "/",
                            
                            isSubmissionManager: () => false,
                            isSubmitter: () => false,
                            isEditor: () => false,
                            isAssessor: () => false,
                            isInUserProfile: () => false,
                            canMessage: () => false,
                            topics: () => []
                        }
                    }
                }


            }

            return self
        }
    }
}

export const useComplianceScale = () => {

    // fallback to 'properties.value' for retro-compatibility

    const self = {
        for: (tag: Tag | undefined) => tag ? (tag.properties?.['custom']?.[complianceValueCustomProp]  ?? tag.properties?.value) : undefined
    }

    return self

}

export const useTimelinessScale = () => {

    // fallback to 'properties.value' for retro-compatibility

    const self = {
        
        for: (tag: Tag | undefined) => tag ? (tag.properties['custom']?.[timelinessValueCustomProp] ?? tag.properties?.value) : undefined
    }

    return self

}