import { Button } from '#app/components/Button'
import { Label } from '#app/components/Label'
import { useT } from '#app/intl/api'
import { utils } from 'apprise-frontend-core/utils/common'
import isEqualWith from 'lodash/isEqualWith'
import moment from 'moment-timezone'
import { useContext } from 'react'
import { changedateformat, changeicon, ChangelogContext } from './changeprovider'
import { useSubmissionContext } from './hooks'
import { useRevisions } from './revision'


export type Changelog = {

    // when the changelog is first created.
    created: string

    // the changes since creation, indexed by component id.
    changes: Record<string, ChangeEntry>

    // the changes since creation, indexed by component id.
    resourceChanges: Record<string, ChangeEntry>
}

type ChangeEntry = {

    // when the component last changed.
    lastChanged: string

    // the content before the first change.
    original: any
}


const undo = Symbol()

export const useChangelog = () => {

    const t = useT()

    const ctx = useContext(ChangelogContext)

    const revs = useRevisions()

    const { submission, currentSubmission: previousSubmission, trail, } = useSubmissionContext() ?? {}


    const changelog = submission.content.changelog

    const now = new Date().toISOString()

    const { created = now, changes = {}, resourceChanges = {} } = changelog ?? {}

    // these are equiavalent for comparisons.
    const equivalents = [undefined, null, '']

    // like deepequals, but uses  treats null/undefined/empty
    const equals = (o1: any, o2: any) => isEqualWith(o1, o2, (o1, o2) => equivalents.includes(o1) && equivalents.includes(o2) ? true : undefined)

    // creates or updates a log of the changes since this revision was created, provided it's not the very first revision.
    // the log tracks changed components, the time of change, and the original content.
    // - if there are no changes returns the previous changelog, if any.
    // - if there are changes and no previous changelog, one is created and timestamped.
    // - if there are changes as well as a previous changelog, the changelog is updated with the new changes.   
    const compute = (): Changelog | undefined => {

        // no changelog for the very first revision.
        if (!revs.on(trail).previous(submission))
            return

        const previous = previousSubmission?.content.data ?? {}
        const current = submission.content.data
        const previousResources = previousSubmission?.content.resources ?? {}
        const currentResources = submission.content.resources

        // we look for changes @ both current and previous keys, because filling/unfilling fields adds/removes keys.
        const allkeys = utils().dedup([...Object.keys(current), ...Object.keys(previous)])

        let updatedChanges: Record<string, ChangeEntry> = {
            ...changes,

            ...utils().index(allkeys.filter(k => !equals(current[k], previous[k]))).mappingBy(k => k, k => {

                return {

                    // updates timestamp
                    lastChanged: now,

                    // we preserve original content if it's the first time we change it.
                    // but if we have already preserved it, we make sure this latest change doesn't "revert" to the original.
                    // in the case, we mark the entry for removal from the changelog to undo the first change.

                    original: changes[k] ? equals(changes[k].original, current[k]) ? undo : changes[k].original : previous[k]


                }
            })
        }

        // remove all entries marked for undo
        Object.keys(updatedChanges).forEach(k => {

            if (updatedChanges[k]?.original === undo) {
                delete updatedChanges[k]
            }
        })

        const updatedResourceChanges: Record<string, ChangeEntry> = {
            ...resourceChanges,

            ...utils().index(allkeys.filter(k => !equals(currentResources[k], previousResources[k]))).mappingBy(k => k, k => ({

                // updates timestamp
                lastChanged: now,

                // keeps original content if it exists alread, otherwise adds it now.
                original: resourceChanges[k] ? equals(resourceChanges[k].original, currentResources[k]) ? undo : resourceChanges[k].original : previousResources[k]

            }))
        }

        const noCurrentChanges = Object.entries(updatedChanges).length === 0 && Object.entries(updatedResourceChanges).length === 0

        // return a new changelog only if there are changes left to track.
        return noCurrentChanges ? undefined : { created, changes: updatedChanges, resourceChanges: updatedResourceChanges }



    }

    const changecount = Object.entries(changes).length + Object.entries(resourceChanges).length

    const pill = changelog &&

        <Label key='changelog' className='changelog-pill' icon={changeicon} title={t("submission.changelog.changes_pill", { count: changecount })}
            mode='tag'
            tipPlacement='bottom'
            tipClassName='tooltip-auto'
            tip={t("submission.changelog.changes_tip", { count: changecount, since: moment(created).format(changedateformat) })}

        />


    const highlighted = ctx.get()

    const btn = changelog &&
        <Button enabledOnReadOnly key='changelog' className={`changelog-btn mode_${highlighted ? 'on' : 'off'}`} icn={changeicon} iconLeft onClick={() => ctx.reset(!highlighted)}>
            {t(highlighted ? "submission.changelog.changes_btn_on" : "submission.changelog.changes_btn_off", { count: changecount })}
        </Button>

    return { changeHighlightBtn: btn, changePill: pill, computeChangelog: compute }

}