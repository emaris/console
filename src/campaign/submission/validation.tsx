

import { Paragraph } from "#app/components/Typography";
import { withReport } from "#app/utils/validation";
import * as React from "react";
import { useT } from '#app/intl/api';
import { SubmissionDto } from "./model";


export const useSubmissionValidation = () => {

    const t = useT()

    return {


        validateSubmission: (_: SubmissionDto) => {


            return withReport({

                approveCycle: {
                    msg: t("submission.fields.approval.msg"), help:

                        <React.Fragment>
                            <Paragraph>{t("submission.fields.approval.help1")}</Paragraph>
                            <Paragraph spaced>{t("submission.fields.approval.help2")}</Paragraph>

                        </React.Fragment>

                }

                ,

                bypassSelfApproval: {
                    msg: t("submission.fields.bypassapproval.msg"), 
                    help:t("submission.fields.bypassapproval.help")

                }

                ,


                submitAfterNA: {
                    msg: t("submission.fields.submitafterNA.msg"), 
                    help:t("submission.fields.submitafterNA.help")

                }

                ,

                adminCanEdit: {

                    msg: t("submission.fields.admin_can_edit.msg"),
                    help: <React.Fragment>
                        <Paragraph>{t("submission.fields.admin_can_edit.help1")}</Paragraph>
                        <Paragraph spaced >{t("submission.fields.admin_can_edit.help2")}</Paragraph>
                    </React.Fragment>
                }

                ,

                adminCanSubmit: {

                    msg: t("submission.fields.admin_can_submit.msg"),
                    help: <React.Fragment>
                        <Paragraph>{t("submission.fields.admin_can_submit.help1")}</Paragraph>
                        <Paragraph spaced >{t("campaign.fields.admin_can_submit.help2")}</Paragraph>
                    </React.Fragment>
                }


            })

        }


    }
}
