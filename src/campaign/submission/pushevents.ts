import { PushEventSlot } from '#app/push/module';
import { noTenant } from '#app/tenant/constants';
import { User } from '#app/user/model';
import { useCampaignStore } from '#campaign/store';
import { useEffect } from 'react';
import { unstable_batchedUpdates } from 'react-dom';
import { useHistory } from 'react-router-dom';
import { submissionType } from './constants';
import { Trail } from './model';
import { useSubmissionStore } from './store';
import { useDashboard } from '#dashboard/api';

export type TrailEvent = {

   trail: Trail
}




export const useSubmissionChangeEvent = (): PushEventSlot<TrailEvent> => {

   const substore = useSubmissionStore()
   const cmpstore = useCampaignStore()
   const dashboard = useDashboard()

   const history = useHistory()

   return {

      onSubcribe: (logged: User) => {

         console.log("subscribing for submission changes")

         return [{

            topics: logged.tenant === noTenant ? [`*.${submissionType}.>`] : [`${logged.tenant}.${submissionType}.>`]

            ,

            onEvent: (event: TrailEvent) => {

               console.log("processing external change about trail", event.trail)

               const campaign = cmpstore.safeLookup(event.trail.key.campaign)
   
               substore.on(campaign).replaceTrail(event.trail)

               // send a signal with trail id at component mounted at current URL (cf. submission's Detail).
               // but the router's data is stale in this closure, the URL is from the time THIS component was mounted.
               // we want the URL that shows in the bar right now instead.
               // apparently the router can't give us it lazily, via a function or other closure-friendly structure.
               // so we fallback to the browser APIs, with the added complication that we need to discard the prefix.

               const currentUrl = `/${location.pathname.split("/").slice(2).join("/")}${location.search}`

               const dashboardOnScreen = currentUrl.includes(dashboard.routeTo(campaign))

               if (dashboardOnScreen)
                  history.push(currentUrl, event.trail.id)
            }

         }]
      }

   }
}

// ---------------------------------------------------------------------------

const statcompDebounceDelayInSeconds = 0

// acumulates events that can be constly to process too often.
// because cause re-computation of stats that interrupts text editing in dashboard scope
// real problem for SEC users mostly at peak timne. 
let eventQueue: TrailEvent[] = undefined!

// currently disabled.
export const useThrottledSubmissionChangeEvent = (): PushEventSlot<TrailEvent> => {

   const substore = useSubmissionStore()
   const cmpstore = useCampaignStore()

   const history = useHistory()

   useEffect(() => {


      // we make extra sure we don't setup intervals more than once.
      // we're caled only once right now, but this is a safety latch.
      if (eventQueue)
         return

      eventQueue = []

    
      setInterval(() => {

         // flush all accumulated events at once.
         unstable_batchedUpdates(()=> {

            while (eventQueue.length) {

               const event = eventQueue.shift()!

               console.log("processing external change about trail", event.trail)

               const campaign = cmpstore.safeLookup(event.trail.key.campaign)
   
               substore.on(campaign).replaceTrail(event.trail)


               // send a signal with trail id at component mounted at current URL (cf. submission's Detail).
               // but the router's data is stale in this closure, the URL is from the time THIS component was mounted.
               // we want the URL that shows in the bar right now instead.
               // apparently the router can't give us it lazily, via a function or other closure-friendly structure.
               // so we fallback to the browser APIs, with the added complication that we need to discard the prefix.

               const currentUrl = `/${location.pathname.split("/").slice(2).join("/")}${location.search}`

               history.push(currentUrl, event.trail.id)

            }

         })

    

      }, statcompDebounceDelayInSeconds * 1000)

   // eslint-disable-next-line
   }, [])

   return {

      onSubcribe: (logged: User) => {

         console.log("subscribing for submission changes")

         return [{

            topics: logged.tenant === noTenant ? [`*.${submissionType}.>`] : [`${logged.tenant}.${submissionType}.>`]

            ,

            onEvent: (event: TrailEvent) => eventQueue.push(event)

         }]
      }

   }
}