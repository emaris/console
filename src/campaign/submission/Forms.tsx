
import { Form } from "#app/form/Form"
import { FormState } from "#app/form/hooks"
import { useCurrentAsset } from "#dashboard/hooks"
import { useLayoutApis } from '#layout/apicontext'
import { LayoutScrollSubscriber } from '#layout/components/scroll'
import { LayoutInstance } from "#layout/model"
import { LazyPdfViewer } from "#layout/pdf/lazyviewer"
import { useSubmissionDownload } from "#layout/pdf/submissionlayout"
import { RenderStrategy } from '#layout/renderstrategy'
import { Submission } from "./model"
import "./styles.scss"






export type Props = {

    state: FormState<Submission>

}

// puts up a blank screen first, to make sure we're not blocking tab transitions.
// we also start a spinner for the same reasons.  then we puts up a real UI immediately after, which may block.
export const SubmissionForms = (props: Props) => {

    const { components, pageComponent, } = useLayoutApis()

    const { state } = props

    const { Render } = pageComponent

    const asset = useCurrentAsset()

    return <RenderStrategy>
        <div className='submission-layout'>
            <div className={`submission-pages ${asset.type}`}>
                <Form state={state} >
                    {components.allPages().map(p =>
                        <div className="page-card" key={p.id}>
                            <LayoutScrollSubscriber key={p.id} component={p}>
                                <Render style={{ display: "flex", flexDirection: "column" }} component={p} />
                            </LayoutScrollSubscriber>
                        </div>)}

                </Form>
            </div>
        </div>
    </RenderStrategy>




}


export const SubmissionPDFForms = (props: Props & { layout: LayoutInstance }) => {

    const { document } = useSubmissionDownload(props.layout)

    return <LazyPdfViewer style={{ height: "100%" }}>{document}</LazyPdfViewer>

}