import { iamType } from '#app/iam/constants';
import { IamSlot } from '#app/iam/module';
import { useT } from '#app/intl/api';
import { Module } from "#app/module/model";
import { pushEventType } from '#app/push/constants';
import { tenantType } from '#app/tenant/constants';
import { TenantSlot } from '#app/tenant/module';
import { useCampaignStore } from '#campaign/store';
import { useDashboard } from '#dashboard/api';
import { messageType } from "#messages/constants";
import { Topic } from "#messages/model";
import { MessageSlot } from "#messages/module";
import { TopicResolver } from "#messages/registry";
import { productSubmissionActions, requirementSubmissionActions } from './actions';
import { productSubmissionPlural, productSubmissionSingular, productSubmissionType, requirementSubmissionPlural, requirementSubmissionSingular, requirementSubmissionType, submissionIcon, submissionPlural, submissionSingular, submissionType } from "./constants";
import { SubmissionLabel } from "./Label";
import { useSubmissionChangeEvent } from './pushevents';
import { useRevisions } from './revision';
import { useSubmissionStore } from './store';

export const useRequirementSubmissionModule = (): Module => {

    return {

        icon: submissionIcon,
        type: requirementSubmissionType,

        nameSingular: requirementSubmissionSingular,
        namePlural: requirementSubmissionPlural

        ,

        [iamType]: {
            actions: requirementSubmissionActions

        } as IamSlot


        ,

        [tenantType]: {

            tenantResource: true


        } as TenantSlot

    }
}


export const useProductSubmissionModule = (): Module => {

    return {

        icon: submissionIcon,
        type: productSubmissionType,

        nameSingular: productSubmissionSingular,
        namePlural: productSubmissionPlural

        ,

        [tenantType]: {

            tenantResource: true


        } as TenantSlot

        ,

        [iamType]: {
            actions: productSubmissionActions

        } as IamSlot
    }
}

export const useSubmissionModule = (): Module => {

    const t = useT()

    const cmpstore = useCampaignStore()
    const dashboard = useDashboard()
    const substore = useSubmissionStore()

    const revisions = useRevisions()

    const subchangeevent = useSubmissionChangeEvent()

    
   
    return {

        icon: submissionIcon,
        type: submissionType,


        nameSingular: submissionSingular,
        namePlural: submissionPlural

        ,


        [pushEventType]: subchangeevent

        ,

        // registers pre-defined modules
        [messageType]: {


            resolvers: (): TopicResolver[] => [

                {
                    type: submissionType,
                    resolve: (topic: Topic) => {


                        const [cid, tid] = topic.name.split(":")
                        const campaign = cmpstore.lookup(cid);

                        if (!campaign)
                            return undefined

                        const store = substore.on(campaign)

                        const trail = store.lookupTrail(tid)

                        if (!trail)
                            return undefined

                        const revs = revisions.on(trail)

                        const submission = revs.latest()

                        if (!submission)
                            return undefined


                        const linkTo = dashboard.on(campaign).partyRouteToSubmission(submission)
                        const linkToMessage = `${linkTo}?tab=${messageType}`

                        return {

                            label: <SubmissionLabel noTip className='topic-icon' linkTo={linkTo} displayMode='submission' submission={submission} />,
                            tip: <SubmissionLabel displayMode='submission' title={t(submissionSingular).toLowerCase()} submission={submission} noLink noIcon noOptions />,
                            link: linkToMessage

                        }

                    }
                }

            ]

        } as MessageSlot
    }

}