

// trail of submissions for a source by party in a campaign.

import { Lifecycle } from "#app/model/lifecycle"
import { MultilangDto } from "#app/model/multilang"
import { Tag, Tagged } from "#app/tag/model"
import { useTagStore } from '#app/tag/store'
import { noTenant } from '#app/tenant/constants'
import { Tenant } from '#app/tenant/model'
import { useTenantStore } from '#app/tenant/store'
import { useLogged } from '#app/user/store'
import { useMessageHelper } from '#campaign/messagehelper'
import { LayoutInstanceState } from "#layout/model"
import { Parameter, ParameterSpec, isLiveParameter } from "#layout/parameters/model"
import { Flow } from "#messages/model"
import { useProductStore } from '#product/store'
import { requirementType } from '#requirement/constants'
import { useRequirementStore } from '#requirement/store'
import shortid from "shortid"
import { anyPartyTopic } from "../constants"
import { AssetInstance, Campaign, useCampaignModel, useCampaignStatus } from "../model"
import { PartyInstance, usePartyInstances } from "../party/api"
import { useComplianceScale, useSubmissionProfile, useTimelinessScale } from './profile'
import { useRevisions } from './revision'
import { useSubmissionStore } from "./store"



export type TrailKey = {

    campaign: string
    party: string
    asset: string
    assetType: string
}

export type TrailDto = {

    // opaque id for references.
    id: string

    // business key for queries.
    key: TrailKey

    submissions: SubmissionStub[]

    properties: Partial<TrailSettings>

}

export const newTrail = (key: TrailKey): Trail => ({ id: newTrailId(), key, submissions: [], properties: {} })

export type TrailSettings = {

    approveCycle: boolean
    bypassSelfApproval: boolean,
    submitAfterNA: boolean,
    adminCanEdit: boolean
    adminCanSubmit: boolean
}

//  may diverge in the future
export type Trail = TrailDto


export const newTrailId = () => `TR-${shortid()}`


// 'cheap' part of the model.
export type SubmissionStub = {

    id: string
    trail: string

    lifecycle: SubmissionLifecycle
}

export type SubmissionLifecycle = Tagged & Lifecycle<SubmissionStatus> & {

    firstEdited? : string

    lastEdited?: string

    lastEditedBy?: string

    lastSubmitted?: string

    lastApprovedBy?: string

    lastPublished: string

    lastPublishedBy?: string

    compliance?: ComplianceState

    revokedCompliance?: ComplianceState

    changeRequest?: string

    reference?: MultilangDto

    publication?: MultilangDto

}


export type ManagementData = Partial<Tagged & {

    reference: MultilangDto

}>

export type PublicationData = {

    revision: string

}


export type SubmissionWithTrail = {

    submission: Submission
    trail: Trail
}

export type ComplianceState = {
    state: string | undefined
    timeliness: string | undefined
    message: string | undefined
    officialObservation: MultilangDto | undefined
    lastAssessed: string | undefined
    lastAssessedBy: string | undefined
}

export type ComplianceProfile = {

    name: MultilangDto
    rate: number
    allNames: MultilangDto[]
    complianceClass: string
    complianceObservation: MultilangDto | undefined
    weight?: number
    tag?: Tag
    code?: string

}

export type TimelinessProfile = {

    name: MultilangDto
    allNames: MultilangDto[]
    tag?: Tag
    code?: string
    timelinessClass: string

}

export type ComplianceScale = {

    names: MultilangDto[],
    min: number,
    max: number,
    tags: Tag[]

}

export type TimelinessScale = ComplianceScale

export type ComplianceProfiles = (Tag & { complianceClass: string })[]
export type TimelinessProfiles = (Tag & { timelinessClass: string })[]

export type SubmissionStatus = "draft" | "pending" | "submitted" | "managed" | "published" | "missing"


export const managedStates: SubmissionStatus[] = ["managed", "published"]
export const submissionStates: SubmissionStatus[] = ["draft", "pending", "submitted", "missing", ...managedStates]

export type SubmissionDto = SubmissionStub & {

    content: LayoutInstanceState

}

export type Submission = SubmissionDto

export const newSubmissionId = () => `SUB-${shortid()}`

export const newSubmission = (trail?: string, asset?: AssetInstance): Submission => ({

    id: newSubmissionId(),
    trail: trail!,
    lifecycle: {
        state: 'draft',
        created: undefined,
        firstEdited: undefined,
        tags: asset?.properties?.submissionTagMap?.flatMap(v => v.tags) ?? [],
    } as SubmissionLifecycle,
    content: {  data: {}, resources: {}}

})

export const newMissingSubmission = (submission: Submission): Submission => ({ ...submission, lifecycle: { ...submission.lifecycle, state: 'missing' } })


// a new revision that takes over from a given one.
// today is: a new CPC revision, a managed revision. 
export const nextRevision = (submission: Submission) => {

    const next = newSubmission(submission.trail)

    // carries over content, always.
    next.content = submission.content

    next.content.changelog = undefined

    next.lifecycle.firstEdited = undefined

    // carries over tags (as may be driven by content).
    next.lifecycle.tags = submission.lifecycle.tags

    // why not set current state as previous state? 
    // may or may not "continue" the workflow: managed revisions do, new cpc drafts don't.

    return next
}




// bundles a submission and the context obtained from its trail.
export type SubmissionContext = {

    currentSubmission?: Submission
    submission: Submission
    relatedSubmissions: SubmissionWithTrail[]

    trail: Trail

    campaign: Campaign
    party: PartyInstance
    asset: AssetInstance

    tenant: Tenant

}


export const useSubmissionTopics = () => {

    const subprofile = useSubmissionProfile()

    const partyinststore = usePartyInstances()
    const tenants = useTenantStore()


    const messages = useMessageHelper()

    return {

        on: (c: Campaign) => {

            const cprofile = subprofile.on(c)

            const self = {


                topicsAndFlow: (trail: Trail, sub: Submission) => {

                    const profile = cprofile.profileOf(trail.key.assetType)

                    const campaignTopics = messages.campaignTopics(c.id)
                    const party = partyinststore.on(c).safeLookupBySource(trail.key.party)
                    const partytopics = messages.partyTopics(c.id, party.source)
                    const asset = profile.instance(trail.key.asset)
                    const assetTopics = messages.assetTopics(trail.key.assetType, c.id, trail.key.asset)
                    const submissionTopics = messages.submissionTopics(c.id, trail.id)

                    const trailTopics = [...campaignTopics, ...partytopics, ...assetTopics]
                    const topics = [...trailTopics, ...submissionTopics]

                    const partySource = tenants.safeLookup(party.source)
                    const assetSource = profile.source(asset.source)

                    const submissionExists = trail.submissions.length > 0

                    const flow: Flow = {
                        scope: c.id,
                        recipient: party.source,

                        mailProfile: () => ({

                            targets: [noTenant, party.source],
                            parameters: {
                                campaign: c.name,
                                campaignId: c.id,
                                tenant: partySource.name,
                                tenantId: partySource.id,
                                asset: assetSource.name,
                                assetId: assetSource.id,
                                assetType: asset.instanceType,
                                assetTitle: assetSource.description
                            }
                        })
                        ,
                        channels: [

                            // msgs sent from/to sub detail or sent from asset detail and addressed to specific party (restricted broadcasts).
                            // in posts/replyes from sub detail trail is a topic if id is stable (one draft has been saved).
                            {
                                name: "main",
                                channel: {
                                    read: trailTopics,
                                    write: submissionExists ? topics : trailTopics,

                                }
                            },

                            // msgs sent from asset detail and addressed to all parties.
                            // in replies from sub detail trail is a topic if id is stable (one draft has been saved).
                            {
                                name: "broadcast",
                                channel: {
                                    read: [...campaignTopics, anyPartyTopic, ...assetTopics],
                                    write: submissionExists ? topics : trailTopics
                                }
                            }

                        ]
                    }

                    return { topics: submissionExists ? topics : trailTopics, flow }
                }

            }

            return self
        }
    }
}

export const useSubmissionScales = () => {

    const tags = useTagStore()
    const campaignmodel = useCampaignModel()

    const complianceScaleResolver = useComplianceScale()
    const timelinessScaleResolver = useTimelinessScale()

    return {
        on: (c: Campaign) => {

            const self = {

                allComplianceProfiles: (): ComplianceProfiles => {

                    const complianceScale = campaignmodel.currentComplianceScale(c)

                    if (!complianceScale)
                        return []

                    const orderedtags = tags.allTagsOfCategory(complianceScale.id).filter(t => t.lifecycle.state === 'active').sort((t1, t2) => complianceScaleResolver.for(t1) - complianceScaleResolver.for(t2))
                    const orderedValues = orderedtags.map(complianceScaleResolver.for)

                    const floor = orderedValues.shift()
                    const ceiling = orderedValues.pop()
                    const range = ceiling - floor

                    return orderedtags.map(tag => {
                        const normalized = (complianceScaleResolver.for(tag) ?? floor) - floor
                        const rate = normalized * 100 / range
                        const complianceClassNumber = Math.floor(rate / 33 + 1)

                        return { ...tag, complianceClass: `compliance-${complianceClassNumber}` }
                    })
                }

                ,

                allTimelinessProfiles: (): TimelinessProfiles => {

                    const timelinessScale = campaignmodel.currentTimelinessScale(c)

                    if (!timelinessScale)
                        return []

                    const orderedtags = tags.allTagsOfCategory(timelinessScale.id).filter(t => t.lifecycle.state === 'active').sort((t1, t2) => timelinessScaleResolver.for(t1) - timelinessScaleResolver.for(t2))

                    return orderedtags.map(tag => {
                        const value = timelinessScaleResolver.for(tag)
                        const timelinessClass = !isNaN(value) ? value < 0 ? 'timeliness-late' : 'timeliness-ontime' : 'timeliness-plain'
                        return { ...tag, timelinessClass }
                    })
                }

                ,

                complianceScale: () => {


                    const complianceScale = campaignmodel.currentComplianceScale(c)

                    if (!complianceScale)
                        return undefined

                    const cachedOrTags = tags.allTagsOfCategory(complianceScale.id).filter(tag => tag.lifecycle.state === 'active')
                    const sorted = cachedOrTags.filter(t => t.lifecycle.state === 'active').filter(t => !isNaN(complianceScaleResolver.for(t))).sort((t1, t2) => complianceScaleResolver.for(t1) - complianceScaleResolver.for(t2))
                    const values = sorted.map(complianceScaleResolver.for)

                    return {
                        names: sorted.map(v => v.name),
                        min: values.shift(),
                        max: values.pop(),
                        tags: cachedOrTags
                    }


                }

                ,

                timelinessScale: (cache?: Tag[]) => {


                    const timelinessScale = campaignmodel.currentTimelinessScale(c)

                    if (!timelinessScale)
                        return undefined

                    const cachedOrTags = cache ?? tags.allTagsOfCategory(timelinessScale.id)
                    const sorted = cachedOrTags.filter(t => t.lifecycle.state === 'active').filter(t => !isNaN(timelinessScaleResolver.for(t))).sort((t1, t2) => timelinessScaleResolver.for(t1) - timelinessScaleResolver.for(t2))
                    const values = sorted.map(timelinessScaleResolver.for)

                    return {
                        names: sorted.map(v => v.name),
                        min: values.shift(),
                        max: values.pop(),
                        tags: cachedOrTags
                    }


                }

                ,

                defaultTimeliness: (timeliness: string | undefined, cache?: Tag[]): Tag | undefined => {
                    if (timeliness === undefined) return undefined

                    const timelinessScale = self.timelinessScale(cache)

                    if (!timelinessScale)
                        return undefined

                    let scaleValue
                    if (timeliness === "ontime") scaleValue = timelinessScale?.max
                    if (timeliness === "late") scaleValue = timelinessScale?.min

                    if (scaleValue === undefined)
                        return undefined



                    return timelinessScale.tags.find(t => timelinessScaleResolver.for(t) === scaleValue)
                }

            }

            return self

        }
    }
}




export const useSubmission = () => {

    const reqstore = useRequirementStore()
    const prodstore = useProductStore()

    const substore = useSubmissionStore()
    const subprofile = useSubmissionProfile()

    const campaignmodel = useCampaignModel()
    const campaigndates = useCampaignStatus()

    const logged = useLogged()

    const tagstore = useTagStore()

    const complianceHelper = useComplianceScale()

    const revisions = useRevisions()

    const checkOrThrow = () => {

        type Pred = boolean | (() => boolean)

        const evaluate = (p: Pred) => typeof p === 'boolean' ? p : p()

        const throwCause = (_: string) => { throw _ }

        const self = {

            if: (p: Pred, cause: string) => (evaluate(p) && self) || throwCause(cause),
            not: (p: Pred, cause: string) => (evaluate(p) && throwCause(cause)) || self,
            done: () => true

        }

        return self

    }

    return {

        in: (ctx: SubmissionContext) => {


            const { submission, currentSubmission, trail, campaign, party, asset } = ctx

            const store = substore.on(campaign)

            const P = subprofile.on(campaign).profileOf(asset.instanceType)

            const isEditor = () => P.isEditor(asset)
            const isSubmitter = () => P.isSubmitter(asset)
            const isValidator = () => P.isSubmissionManager(asset)
            const isAssessor = () => P.isAssessor(asset)
            const isPublisher = () => P.isSubmissionManager(asset)
           
            const isPartyManager = () => logged.isManagerOf(party.source)

            // this applies to both CPCs and SEC, both must have a matching profile tag.
            // party managers are exempted to match or even have a profile, again both CPCs and SEC.
            const isUserProfileValid = () => P.isInUserProfile(asset)

            // can be suspended manually or because it's ended and the settings say to suspend on end.
            const isCampaignSuspended = () => logged.isTenantUser() && campaigndates.isSuspended(campaign)
            // can be suspended because it's ended, based on app settings. Here we check that's the reason.
            const isCampaignEnded = () => isCampaignSuspended() && campaigndates.isEnded(campaign)
            const isCampaignArchived = () => campaignmodel.isArchived(campaign)


            const revs = revisions.on(trail)


            const isOfficialNotApplicable = () => {

                if (logged.hasNoTenant())
                    return false

                const official = revs.official()

                if (store.canSubmitAfterNA(trail) || !official)
                    return false

                const state = tagstore.lookupTag(official.lifecycle.compliance?.state)

                if (!state.id || state.type === 'unknown')
                    return false

                return complianceHelper.for(state) === undefined
            }

            const self = {

                ...ctx

                ,

                isSaved: () => !!submission.lifecycle.created,
                isDraft: () => submission.lifecycle.state === 'draft',
                isPending: () => submission.lifecycle.state === 'pending',
                isSubmitted: () => submission.lifecycle.state === 'submitted',
                isLikeSubmitted: () => self.isSubmitted() || self.isMissing(),
                isManaged: () => submission.lifecycle.state === 'managed',
                isPublished: () => submission.lifecycle.state === 'published',
                isManagedState: () => self.isManaged() || self.isPublished(),
                isMissing: () => submission.lifecycle.state === 'missing',
                isEditable: () => isUserProfileValid() &&  (logged.hasNoTenant() || (asset.properties.editable ?? true)),
                isVersionable: () => logged.hasNoTenant() || (asset.properties.versionable ?? true),
                isAssessable: () => asset.properties.assessed ?? true,
                isAssesssableInCampaign: () => self.isAssessable() && !!self.complianceScale(),
                isAssessed: () => !!currentSubmission?.lifecycle.compliance?.state,


                isInAudience: () => P.isForParty(party, asset)


                ,
                isSubmittedOrAbout: () => {

                    return !!submission.lifecycle.lastSubmitted
                }
                ,

                isOfficial: () => revs.isOfficial(submission)

                ,

                isActive: () => revs.isActive(submission)

                ,

                isSuperseded: () => revs.isSuperseded(submission)

                ,


                isApproveCycleActive: () => !!store.hasApproveCycle(trail)

                ,

                hasApproveCycle: () => self.isApproveCycleActive() && !self.canBypassApproval()


                ,

                canBypassApproval: () => (logged.hasNoTenant() && isEditor()) || (isValidator() && store.canBypassSelfApproval(trail))

                ,

                getAssetSource: (asset: AssetInstance) => asset.instanceType === requirementType ? reqstore.safeLookup(asset.source) : prodstore.safeLookup(asset.source)

                ,

                complianceScale: () => campaignmodel.currentComplianceScale(campaign)

                ,

                timelinessScale: () => campaignmodel.currentTimelinessScale(campaign)

                ,

                canChangeSettings: isPartyManager

                ,

                canShowSaveAndRevert: () => {

                    const revisionstate = self.isDraft() || self.canEditUnlocked() || self.canEditPostPublish()

                    return check.preconditions && revisionstate

                }

                ,

                canSaveAndRevert: () => {

                    if (!self.canShowSaveAndRevert())
                        return false

                    const userstate = isEditor()

                    return userstate

                }

                ,

                // this is like `canSaveAndRevert()` but broken down to capture the diverse causes.
                canEdit: (report?: (_: string) => void) => {

                    try {


                        return self.areSubmissionsAllowed(report) &&

                            checkOrThrow()
                                .if(check.partystate, "submission.labels.readonly_not_in_audience")
                                .if(isUserProfileValid, "submission.labels.readonly_profile_invalid")
                                .if(check.assetstate, "submission.labels.readonly_not_editable")
                                .if(self.isDraft, "submission.labels.readonly_not_a_draft")
                                .if(isEditor, "submission.labels.readonly_not_privileged")
                                .done()

                    }
                    catch (cause) {

                        report?.(cause as string)

                        return false
                    }

                }



                ,

                canEditUnlocked: () => self.isManaged() && logged.hasNoTenant()

                ,

                canEditPostPublish: () => self.isPublished() && !self.isSuperseded()
                ,


                canShowSubmit: () => {

                    const revisionstate = (self.isDraft() && self.isSaved()) || self.isPending()

                    return check.preconditions && revisionstate

                }

                ,

                canSubmit: () => {

                    if (!self.canShowSubmit())
                        return false

                    const trailstate = !isOfficialNotApplicable()
                    const userstate = self.isPending() ? isValidator() : self.isApproveCycleActive() ? isEditor() : isSubmitter()

                    return trailstate && userstate

                }

                ,


                canShowAssessMissing: () => {

                    const assessmentstate = self.isAssesssableInCampaign()

                    const trailstate = !revs.active()

                    const userstate = logged.hasNoTenant()

                    return check.preconditions && assessmentstate && trailstate && userstate

                }

                ,


                // can assess as missing only as the first submission (not necessarily the first revision or the last revision).
                canAssessMissing: () => {

                    if (!self.canShowAssessMissing())
                        return false

                    const userstate = isAssessor()

                    return userstate

                }

                ,


                canSubmitManaged: () => logged.hasNoTenant() && isAssessor() && self.canSubmit()

                ,

                canShowReject: () => {

                    const revisionstate = self.isPending()

                    return check.preconditions && revisionstate

                }

                ,

                canReject: () => {

                    if (!self.canShowReject())
                        return false

                    const userstate = isValidator()

                    return userstate

                }

                ,

                canShowRequestChange: () => ['draft', 'pending'].includes(submission.lifecycle.state!) && submission.lifecycle.changeRequest


                ,

                canShowCreate: () => {

                    const trailstate = !['draft', 'pending'].includes(revs.latest()?.lifecycle.state ?? '') && !isOfficialNotApplicable()

                    const revisionstate = self.isSaved()

                    return check.preconditions && trailstate && revisionstate
                }
                ,

                canCreate: () => {

                    if (!self.canShowCreate())
                        return false

                    const userstate = isEditor()

                    return userstate

                }

                ,


                // can remove drafts, submitted under admin privileges, published, or managed without published, conditionally to privileges and settings.
                canShowRemove: () => {

                    const draftcase = self.isDraft() && self.isSaved() && isEditor()

                    //note: a managed can be removed even if it's superseded
                    const managedcase = self.isManaged() && isValidator()       // even if superseded

                    // admins can exceptionally remove top of group
                    const latestCase = revs.isLatestInGroup(submission) && logged.isAdmin()

                    return check.campaignstate && (draftcase || managedcase || latestCase)

                }

                ,
                // can remove drafts, submitted under admin privileges, published, or managed without published, conditionally to privileges and settings.
                canRemove: () => self.canShowRemove()


                ,


                canShowAssess: () => {

                    const assessmentstate = self.isAssesssableInCampaign()

                    const revisionstate = self.isActive() && ((self.isLikeSubmitted() && (!self.isAssessed() || self.isRevoked())) || self.isManaged())

                    const userstate = logged.hasNoTenant()

                    return check.preconditions && assessmentstate && revisionstate && userstate

                }

                ,

                canAssess: () => {

                    if (!self.canShowAssess())
                        return false;

                    const userstate = isAssessor()

                    return userstate

                }

                ,

                canRevokeAssessment: () => {

                    const substate = self.isAssessed()

                    const privilege = isAssessor()

                    return substate && privilege

                }

                ,

                isRevoked: () => !self.isAssessed() && !!submission.lifecycle.revokedCompliance?.state

                ,

                canShowViewAssessment: () => {

                    const substate = self.isAssessed() && !self.canAssess()

                    return substate

                }

                ,

                canViewAssessment: () => self.canShowViewAssessment()

                ,

                areSubmissionsAllowed: (report?: (_: string) => void) => {

                    try {

                        return checkOrThrow()
                            .not(isCampaignSuspended() && isCampaignEnded(), "submission.labels.readonly_campaign_ended")
                            .not(isCampaignSuspended, "submission.labels.readonly_campaign_suspended")
                            .not(isCampaignArchived, "submission.labels.readonly_campaign_archived")
                            .done()

                    }
                    catch (cause) {

                        report?.(cause as string)

                        return false
                    }

                }

                ,


                canShowManage: () => {


                    const trailstate = revs.isActive(submission)

                    const assessmentstate = !self.isAssesssableInCampaign() || self.isAssessed() || self.isRevoked()  // if it can be assessed, must have been at least once.

                    const revisionstate = (self.isSubmitted() || self.isMissing() || self.isPublished()  )&& assessmentstate

                    const userstate = logged.hasNoTenant()

                    return check.preconditions && revisionstate && trailstate && userstate

                }
                ,


                canManage: () => {

                    if (!self.canShowManage())
                        return false

                    const userstate = isAssessor()

                    return userstate

                }

                ,

                canShowEditManagementData: () => {

                    const assetstate = !!Object.values(asset.properties.submissionTagMap ?? {}).length

                    const trailstate = revs.isActive(submission)

                    const revisionstate = self.isManaged()

                    const userstate = logged.hasNoTenant()

                    return check.preconditions && assetstate && revisionstate && trailstate && userstate


                }

                ,

                canEditManagementData: () => {

                    if (!self.canShowEditManagementData())
                        return false

                    const userstate = isAssessor()

                    return userstate

                }


                ,

                canShowPublish: () => {

                    const revisionstate = self.isManaged()

                    const trailstate = self.isActive()

                    const privilege = logged.hasNoTenant()

                    return check.preconditions && revisionstate && trailstate && privilege

                }

                ,

                canPublish: () => {

                    if (!self.canShowPublish())
                        return false

                    const userstate = isAssessor()

                    return userstate

                }

                ,

                canShowShare: () => {

                    const revisionstate = self.isPublished()

                    const userstate = logged.hasNoTenant()

                    return check.preconditions && revisionstate && userstate


                }

                ,

                canShare: () => {

                    if (!self.canShowShare())
                        return false

                    const userstate = isPublisher()

                    return userstate

                }


                ,

                canShowChangeDate: () => {

                    const revisionstate = self.isManaged()
                    const trailstate = self.isActive()
                    const userstate = logged.hasNoTenant()

                    return check.preconditions && trailstate && revisionstate && userstate

                }

                ,

                canChangeDate: () => {

                    if (!self.canShowChangeDate())
                        return false;

                    const userstate = isEditor() && logged.hasNoTenant()

                    return userstate

                }

                ,

                canBeTagged: () => asset.properties.submissionTagMap?.length


                ,

                initialiseParameters: (params: [Parameter, ParameterSpec][]): Parameter[] => {

                    // computes value for live parameters
                    const parameters = params.map(([param, spec]) => isLiveParameter(spec) ? { ...param, value: spec.valueFor(ctx, param) } : param)

                    var overlay = asset?.properties.parameterOverlay ?? {}

                    overlay = { ...overlay, ...party?.properties.parameterOverlays.requirements[ctx.asset.source] ?? {} }

                    // applies per-party overrides.
                    return parameters.map(p => ({ ...p, value: overlay[p.name] ? overlay[p.name].value : p.value }))
                }

            }

            // common checks
            const check = {


                campaignstate: self.areSubmissionsAllowed(),
                assetstate: self.isEditable(),
                partystate: self.isInAudience(),
                preconditions: true

            }

            check.preconditions = check.campaignstate && check.assetstate && check.partystate

            return self;
        }
    }
}
