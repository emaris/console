
import { RadioBox } from "#app/form/RadioBox"
import { Switch } from '#app/form/Switch'
import { FormState } from "#app/form/hooks"
import { useT } from '#app/intl/api'
import { SettingsForm } from "#app/settings/SettingsForm"
import { useLogged } from '#app/user/store'
import { through } from "#app/utils/common"
import { Validation } from "#app/utils/validation"
import { Radio } from "antd"
import { approveCycleOptionDefaults } from "../model"
import { useSubmissionClient } from './client'
import { useSubmissionContext } from "./hooks"
import { Trail, TrailSettings } from "./model"
import { useSubmissionStore } from './store'


type Props = {

    state: FormState<Trail>
    onSave: (_:Trail)=>void
    report: Partial<Record<keyof TrailSettings ,Validation>>
}

export const TrailSettingsFields = (props:Props) => {

    const t = useT()

    const {state,onSave,report} = props

    const logged = useLogged()

    const ctx = useSubmissionContext()
    const subclient = useSubmissionClient().on(ctx.campaign)
    const substore = useSubmissionStore().on(ctx.campaign)

    const {edited,change,reset} = state

    const isCoordinator = logged.hasNoTenant()


    return  <SettingsForm state={state} onSave={()=>subclient.saveTrail(edited).then(through(saved=>reset(saved,false))).then(onSave)}>

                <RadioBox label={t("submission.fields.approval.name")} validation={report.approveCycle}
                    value={ substore.hasApproveCycle(edited, 'nodelegation') ?? 'default' }
                    onChange={change( (t,v) => {  substore.setApproveCycle(t,v==='default' ? undefined : v)})} >
                        <Radio value={false}>{t("submission.fields.approval.options.not_required")}.</Radio>
                        <Radio value={true}>{t("submission.fields.approval.options.required")}.</Radio>
                        <Radio value={'default'}>
                            {t("submission.fields.approval.options.default",{value:t(approveCycleOptionDefaults[substore.approveCycleDefault(edited) ])})}.
                        </Radio>
                </RadioBox>

                <RadioBox label={t("submission.fields.bypassapproval.name")} validation={report.bypassSelfApproval}
                    value={ substore.canBypassSelfApproval(edited, 'nodelegation') ?? 'default' }
                    onChange={change( (t,v) => {  substore.setCanBypassSelfApproval(t,v==='default' ? undefined : v)})} >
                        <Radio value={true}>{t("submission.fields.bypassapproval.options.can")}.</Radio>
                        <Radio value={false}>{t("submission.fields.bypassapproval.options.cannot")}.</Radio>
                        <Radio value={'default'}>{t("submission.fields.bypassapproval.options.default",
                         {value:t(substore.canBypassSelfApprovalDefault(edited)?
                                                "submission.fields.bypassapproval.options.can_short"
                                                : "submission.fields.bypassapproval.options.cannot_short" )})}.
                        </Radio>
                </RadioBox>

                { isCoordinator &&

                    <Switch label={t("submission.fields.submitafterNA.name")} validation={report.submitAfterNA} onChange={change(substore.setCanSubmitAfterNA)}>
                        {substore.canSubmitAfterNA(edited)}
                    </Switch>
                
                }

           </SettingsForm>
}