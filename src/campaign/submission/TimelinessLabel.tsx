import { Label, LabelProps, UnknownLabel } from "#app/components/Label"
import { TagLabel } from "#app/tag/Label"
import { useCurrentDashboard, useTrail } from "#dashboard/hooks"
import { eventIcon } from "#event/constants"
import { useT } from '#app/intl/api'
import { AssetInstance } from "../model"
import { PartyInstance } from "../party/api"
import { TrailKey } from "./model"

export type Props = LabelProps & {

    asset?: AssetInstance
    party? : PartyInstance
    trailKey?: TrailKey

}


export const TimelinessLabel = (props: Props) => {
    const {asset, party, trailKey, ...rest} = props

    const t = useT()

    const { summaries } = useCurrentDashboard()
    const { keyWithAsset, keyWithParty } = useTrail()

    const key = trailKey ? trailKey : party ? keyWithParty(party) : asset ? keyWithAsset(asset) : undefined

    if (!key) return <UnknownLabel />

    const summary = summaries.trailSummaryOf(key)
    const timeliness = summary.timeliness


    const tag = timeliness ? timeliness.tag : timeliness

    if (!tag) return <Label mode='tag' className="timeliness-notdue" icon={eventIcon} title={t("dashboard.labels.timeliness.notdue")} />

    const className = timeliness ? timeliness.timelinessClass : ''

    
    return <TagLabel className={className} tag={tag} {...rest} icon={eventIcon} noDecorations  />
}