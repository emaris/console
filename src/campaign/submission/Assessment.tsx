import { Button } from "#app/components/Button"
import { DrawerProps, RoutableDrawerProps, useRoutableDrawer } from "#app/components/Drawer"
import { HtmlSnippet } from "#app/components/HtmlSnippet"
import { Form } from "#app/form/Form"
import { MultiBox } from "#app/form/MultiBox"
import { RichBox } from "#app/form/RichBox"
import { FormState } from "#app/form/hooks"
import { icns } from "#app/icons"
import { useT } from '#app/intl/api'
import { useLocale } from '#app/model/hooks'
import { Topbar } from "#app/scaffold/Topbar"
import { TagBoxset } from "#app/tag/TagBoxset"
import { TagCategory } from "#app/tag/model"
import { useTagStore } from '#app/tag/store'
import { useTime } from '#app/time/api'
import { defaultTimeFormat } from '#app/time/constants'
import { useLogged } from '#app/user/store'
import { useStable } from '#app/utils/function'
import { paramsInQuery } from '#app/utils/routes'
import { useCampaignModel } from '#campaign/model'
import { useDashboard } from '#dashboard/api'
import { useCurrentDashboard } from "#dashboard/hooks"
import { ProductLabel } from '#product/Label'
import { Product } from '#product/model'
import { RequirementLabel } from '#requirement/Label'
import { requirementType } from '#requirement/constants'
import { Requirement } from '#requirement/model'
import moment from "moment-timezone"
import * as React from "react"
import { useLocation } from 'react-router-dom'
import { complianceIcon, complianceType, timelinessType } from "../constants"
import { useCurrentCampaign } from "../hooks"
import { useSubmissionContext } from './hooks'
import { Submission, SubmissionDto, useSubmissionScales } from "./model"
import { useComplianceScale, useTimelinessScale } from './profile'
import { useTrailSummary } from './statistics/trail'
import { LineageData, useSubmissionLineage, useSubmissionStore } from './store'


type Props = {
    onSave: () => void
    scale: TagCategory
    timeliness: TagCategory
    state: FormState<Submission>
}

export const AssessmentForm = (props: Props) => {

    const t = useT()
    const dashboard = useCurrentDashboard()
    const campaign = useCurrentCampaign()
    const { trail } = useSubmissionContext()
    const subscales = useSubmissionScales().on(campaign)
    const substore = useSubmissionStore().on(campaign)

    const sublineage = useSubmissionLineage().on(campaign)

    const { LineageDrawer, lineageBtn } = useLineageAssessmentDrawer()


    const { onSave, state, scale, timeliness } = props
    // const { onSave, onRevokeCompliance = () => { }, state, scale } = props

    const logged = useLogged()

    const tagstore = useTagStore()

    const compliancescale = useComplianceScale()
    const timelinessscale = useTimelinessScale()

    const { initial, dirty, reset, change, edited } = state

    const lineage = sublineage.given({ trail: edited.trail })

    const deadline = dashboard.summaries.dueDates[trail.key.asset]

    const firstSubmissionDate = dashboard.summaries.trailSummaryMap[trail.id]?.firstSubmissionDate

    const lateness = deadline && firstSubmissionDate ? moment(new Date(firstSubmissionDate)).diff(new Date(deadline), 'days') : 0

    const trailKey = substore.allTrails().find(t => t.id === edited.trail)?.key

    const summary = trailKey ? dashboard.summaries.trailSummaryOf(trailKey) : undefined

    const defaultTimeliness = summary ? subscales.defaultTimeliness(summary.computedTimeliness) : undefined

    const revertBtn = <Button icn={icns.revert} enabled={dirty} onClick={() => reset(initial, true)}>{t("common.buttons.revert")}</Button>

    const saveBtn = <Button
        type="primary"
        icn={icns.save}
        enabled={dirty && !!edited.lifecycle.compliance?.state}
        onClick={onSave}>
        {t("common.buttons.save")}
    </Button>

    return <div>

        <Topbar offset={63} autoGroupButtons={false}>

            {React.cloneElement(lineageBtn, { disabled: !lineage?.assets[0]?.submission?.lifecycle.compliance?.state })}
            {saveBtn}
            {revertBtn}
        </Topbar>

        <Form className="settings-form" state={state} sidebar>

            <TagBoxset edited={edited.lifecycle.compliance?.state ? [edited.lifecycle.compliance.state] : []}
                type={complianceType}
                categories={[scale]}
                onChange={change((t, v) => {

                    const now = moment().format()

                    const state = typeof v === 'string' ? v : v[0]! as string

                    const weight = compliancescale.for(tagstore.lookupTag(state))

                    const timelinessValue = weight === undefined ? tagstore.allTagsOfCategory(timeliness.id).find(t => timelinessscale.for(t) === undefined)?.id : t.lifecycle.compliance?.timeliness

                    t.lifecycle.compliance = { ...t.lifecycle.compliance!, timeliness: timelinessValue, state, lastAssessed: now, lastAssessedBy: logged.username }
                    delete t.lifecycle.revokedCompliance

                })}
            />

            <TagBoxset edited={edited.lifecycle.compliance?.timeliness ? [edited.lifecycle.compliance.timeliness] : defaultTimeliness ? [defaultTimeliness.id] : []}
                type={timelinessType}
                categories={[timeliness]}
                onChange={change((t, v) => t.lifecycle.compliance = { ...t.lifecycle.compliance!, timeliness: typeof v === 'string' ? v : v[0]! })}
            />

            {lateness > 0 &&

                <div className='lateness-reminder'>{t('submission.assessment.lateness_reminder', { days: lateness })}</div>
            }

            <MultiBox
                rte
                id="compliance_official_observation"
                label={t("submission.fields.compliance_official_observation.name")}
                onChange={change((t, v) => t.lifecycle.compliance = { ...t.lifecycle.compliance!, officialObservation: v })}
                validation={{ msg: t("submission.fields.compliance_official_observation.msg") }}
            >{edited.lifecycle.compliance?.officialObservation}
            </MultiBox>

            <RichBox
                // id="compliance_msg"
                label={t("submission.fields.compliance_message.name")}
                height="mid"
                onChange={change((t, v) => t.lifecycle.compliance = { ...t.lifecycle.compliance!, message: v })}
                validation={{ msg: t("submission.fields.compliance_message.msg") }}
            >{edited.lifecycle.compliance?.message}</RichBox>



        </Form>


        <LineageDrawer mask={false} width={585}>
            <LineageAssessmentForm lineage={lineage} />
        </LineageDrawer>

    </div>

}

type LinegeProps = {

    lineage: Required<LineageData>
}


export const LineageAssessmentForm = (props: LinegeProps) => {

    const t = useT()

    const { lineage } = props

    const catstore = useTagStore()
    const cmpmodel = useCampaignModel()
    const subscales = useSubmissionScales()
    const summary = useTrailSummary()



    const scale = catstore.lookupCategory(cmpmodel.complianceScale(lineage.campaign!))
    const timeliness = catstore.lookupCategory(cmpmodel.timelinessScale(lineage.campaign!))

    const assetdata = lineage.assets[0] // todo: generalise to many assets


    const asset = assetdata.asset

    const submission = assetdata?.submission

    const dashboard = useDashboard().on(lineage.campaign!)

    const type = assetdata.trail!.key.assetType

    const assetlink = dashboard.assetRouteToSubmission(submission!)

    const labelprops = { noOptions: true, noDecorations: true, linkTo: assetlink }

    const label = type === requirementType ?
        <RequirementLabel requirement={asset as Requirement} {...labelprops} />
        : <ProductLabel product={asset as Product} {...labelprops} />



    const defaultTimeliness = () => {

        const computedTimeliness = summary.on(lineage.campaign!)(assetdata.trail?.key!)?.computedTimeliness
        const timeliness = subscales.on(lineage.campaign!).defaultTimeliness(computedTimeliness)?.id

        return timeliness ? [timeliness] : undefined
    }

    return <React.Fragment>

        <div className='lineage-panel-title'>
            {label}
            {/* <CampaignLabel noIcon style={{fontSize:12}} noOptions linkTo={dashboard.routeTo(lineage.campaign!)} campaign={lineage.campaign} /> */}
        </div>


        <Form className="settings-form" sidebar>

            <TagBoxset edited={submission?.lifecycle.compliance?.state ? [submission.lifecycle.compliance.state] : []}
                type={complianceType}
                categories={[scale]}
                onChange={() => { }}
            />

            <TagBoxset edited={submission?.lifecycle.compliance?.timeliness ? [submission?.lifecycle.compliance?.timeliness] : defaultTimeliness()}
                type={timelinessType}
                categories={[timeliness]}
                onChange={() => { }}
            />

            <MultiBox
                rte
                readonly
                id="compliance_official_observation"
                label={t("submission.fields.compliance_official_observation.name")}
                onChange={() => { }}
                validation={{ msg: t("submission.fields.compliance_official_observation.msg") }}
            >{submission?.lifecycle.compliance?.officialObservation}</MultiBox>

            <RichBox readonly
                // id="compliance_msg"
                label={t("submission.fields.compliance_message.name")}
                height="mid"
                onChange={() => { }}
                validation={{ msg: t("submission.fields.compliance_message.msg") }}
            >{submission?.lifecycle.compliance?.message}</RichBox>



        </Form>


    </React.Fragment>

}



export const AssessmentViewer = (props: {

    submission: SubmissionDto,
    onRevokeCompliance: () => void
    onManage: () => void

}) => {

    const t = useT()
    const { l } = useLocale()
    const time = useTime()

    const { submission, onRevokeCompliance, onManage } = props

    const ctx = useSubmissionContext()
    const substore = useSubmissionStore().on(ctx.campaign)
    const summary = useCurrentDashboard().summaries.trailSummaryOf(ctx.trail.key)


    const { name, complianceClass } = substore.complianceProfile(submission) ?? {}
    const { name: timeliness, timelinessClass } = substore.timelinessProfile(submission, summary.computedTimeliness) ?? {}

    const msg = submission.lifecycle.compliance?.message
    const officialObservation = submission.lifecycle.compliance?.officialObservation

    return <div style={{ marginTop: 10, padding: 30 }} >

        <div className="assessment-label">
            <div className="label-name">{t("submission.assessment.level")}</div>
            <div style={{ marginTop: -5 }} className={`assessment-level compliance ${complianceClass}`}>{l(name)}</div>
        </div>

        <div className="assessment-label">
            <div className="label-name">{t("submission.assessment.timeliness")}</div>
            <div style={{ marginTop: -5 }} className={`timeliness-level timeliness ${timelinessClass}`}>{l(timeliness)}</div>
        </div>

        <div className="assessment-label">
            <div className="label-name">{t("submission.assessment.date")}</div>
            <div>{moment(submission.lifecycle.compliance?.lastAssessed).format(t(defaultTimeFormat))} ({moment(submission.lifecycle.compliance?.lastAssessed).from(time.current())})</div>
        </div>

        <div className="assessment-label">
            <div className="label-name">{t("submission.assessment.official_observation")}</div>
            <div className="assessment-comment">
                {

                    officialObservation ?

                        <HtmlSnippet snippet={l(officialObservation)} />


                        :

                        t("submission.assessment.no_official_observation")

                }
            </div>
        </div>

        <div className="assessment-label">
            <div className="label-name">{t("submission.assessment.comment")}</div>
            <div className="assessment-comment">
                {

                    msg ?

                        <HtmlSnippet snippet={msg} />


                        :

                        t("submission.assessment.no_comment")

                }
            </div>
        </div>

        <div style={{ marginTop: 30, display: 'flex' }}>

            {ctx.canManage() &&

                <Button style={{ marginRight: 10 }} type='primary' onClick={onManage}>
                    {t('submission.assessment.manage_btn')}
                </Button>
            }

            {ctx.canRevokeAssessment() &&

                <Button style={{ marginRight: 10 }} type="danger"
                    onClick={onRevokeCompliance}>
                    {t("common.labels.revoke")}
                </Button>


            }

        </div>



    </div>

}




export const useAssessmentDrawer = (opts: Partial<RoutableDrawerProps> = {}) => {

    const t = useT()

    const { id = 'assessment', title = t("submission.assessment.title"), icon = complianceIcon } = opts

    const { closeLineage, lineageassessParam } = useLineageAssessmentDrawer()

    const { Drawer, open, close, visible, route, param } = useRoutableDrawer({ id, title, icon })

    const { search } = useLocation()

    const lineageAssessmentVisible = !!paramsInQuery(search)[lineageassessParam]

    const DrawerProxy = useStable((props: Partial<DrawerProps>) =>

        <Drawer width={600} icon={icon} title={title} {...props}
            className={lineageAssessmentVisible ? 'translate-assessment-panel' : ''}
            afterVisibleChange={visible => visible || closeLineage()} />

    )

    const assessBtn = <Button icn={icon} linkTo={route()}>{t("submission.assessment.link")}</Button> as JSX.Element

    return { ComplianceDrawer: DrawerProxy, openAssessment: open, complianceRoute: route, closeCompliance: close, visibleCompliance: visible, assessBtn, assessParam: param }

}

export const useLineageAssessmentDrawer = (opts: Partial<RoutableDrawerProps> = {}) => {

    const t = useT()

    const { id = 'lineage-assessment', title = t("submission.assessment.lineage_title"), icon = complianceIcon } = opts

    const { Drawer, open, close, visible, route, param } = useRoutableDrawer({ id, title, icon })

    const DrawerProxy = useStable((props: Partial<DrawerProps>) => <Drawer icon={icon} title={title} {...props}>{props.children}</Drawer>)

    const lineageBtn = <Button type={visible ? 'primary' : 'default'} icn={icon} onClick={visible ? close : open}>{visible ? t("submission.assessment.lineage_hide") : t("submission.assessment.lineage_show")}</Button> as JSX.Element

    return { LineageDrawer: DrawerProxy, openLineage: open, lineageRoute: route, closeLineage: close, visibleLineage: visible, lineageBtn, lineageassessParam: param }

}

export const useMissingAssessmentDrawer = (opts: Partial<RoutableDrawerProps & { onClick: () => void }> = {}) => {

    const t = useT()

    const { ComplianceDrawer, openAssessment, complianceRoute, closeCompliance,assessParam, visibleCompliance, assessBtn } = useAssessmentDrawer(opts)

    const assessMissingBtn = React.cloneElement(assessBtn, { onClick: opts.onClick, linkTo: undefined, children: t("submission.assessment.assess_missing") })

    return { ComplianceDrawer, openAssessment, complianceRoute, closeCompliance, assessParam, visibleCompliance, assessMissingBtn }
}

export const useAssessmentViewerDrawer = (opts: Partial<RoutableDrawerProps & { title: string }> = {}) => {

    const t = useT()

    const { id = 'assessmentviewer', title = t("submission.assessment.viewer_title"), icon = complianceIcon } = opts

    const { Drawer, open, close, visible, route } = useRoutableDrawer({ id, title, icon })

    //eslint-disable-next-line
    const DrawerProxy = (props: Partial<DrawerProps>) => <Drawer width={700} icon={icon} title={title} {...props}>{props.children}</Drawer>

    //eslint-disable-next-line
    const StableProxy = React.useCallback(DrawerProxy, [visible])

    const complianceViewerButton = <Button enabledOnReadOnly type="primary" icn={icon} linkTo={route()}>{t("submission.assessment.viewer_link")}</Button>

    return {
        ComplianceViewerDrawer: StableProxy,
        openAssessmentViewer: open,
        complianceViewerRoute: route,
        closeComplianceViewer: close,
        visibleComplianceViewer: visible,
        complianceViewerButton
    }
}