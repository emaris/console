

import { useCampaignStore } from '#campaign/store'
import * as React from "react"
import { usePartyInstances } from '../party/api'
import { Submission, SubmissionContext, Trail, useSubmission } from "./model"
import { useSubmissionProfile } from './profile'
import { useTenantStore } from '#app/tenant/store'
import { SubmissionReactContext } from './context'





// custom hook: return the curent submission api and the current submission.
export const useSubmissionContext = () => useSubmission().in( React.useContext(SubmissionReactContext) )


// like above, but for a non-contextual submission versions. 
export const useContextFactory = (trail:Trail) => {

    const campaign = useCampaignStore().safeLookup(trail.key.campaign)

    const P = useSubmissionProfile().on(campaign).profileOf(trail.key.assetType)
    const asset = P.instance(trail.key.asset)
    const party = usePartyInstances().on(campaign).safeLookup(trail.key.party)
    const tenant = useTenantStore().safeLookup(trail.key.party)

    const sub = useSubmission()
    
    return { contextOf: (submission:Submission, currentSubmission?: Submission) => {

        const context : SubmissionContext = {submission,currentSubmission,trail, tenant, campaign,party,asset,relatedSubmissions:[]}
        return sub.in(context)
    
    }}

}