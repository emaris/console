
import { Component, isContainer } from '#layout/components/model'
import { Layout } from '#layout/model'
import { Parameter } from '#layout/parameters/model'
import { useLayoutRegistry } from '#layout/registry'
import { Submission } from './model'



// passes over the layout and asks named components to manipulate their data.
// as to 11/24 this is used only to materialise defaults - both configred or resolved from field references -
// so that they can be referenced from other layouts, primarily reports.

export const useContentPostProcess = () => {

    const registry = useLayoutRegistry()

    return (submission: Submission, layout: Layout) => {

        const data = submission.content.data
        const resources = submission.content.resources

        const postprocess = (component:Component, params:Parameter[]) => {

            let parameters = params

            // first, process current component: may change data or add scoped params.
            if (component.shortname) {

                const spec = registry.lookupComponent(component.spec)

                const { data: d, resources: r, parameters: ps } = spec.submittedDataOf?.(component, 
                    
                    { data: data[component.id], resources: resources[component.id] },

                    parameters
                
                ) ?? { data: undefined, resources: undefined, parameters: undefined }
    
                if (d !== undefined)
                    data[component.id] = d
    
                if (r !== undefined)
                    resources[component.id] = r

                // adds scoped params
                if (ps)
                    parameters = [...parameters, ...ps]
            }

            // then process children with the params in scope of current component.
            if (isContainer(component)) 
                component.children.forEach( c => postprocess(c,parameters))


        }
            
        // post-process root
        postprocess(layout.components,  [...layout.parameters])

    }

}