import { Icon } from "antd";
import { icns } from '#app/icons';
import * as React from "react";
import { AiOutlineRollback } from "react-icons/ai";
import { BsCloudUploadFill } from "react-icons/bs";
import { GiBackwardTime } from 'react-icons/gi';
import { RiCheckboxMultipleBlankLine } from "react-icons/ri";
import { GroupOrdinal } from './statistics/aggregateOne';


export const submissionIcon = <Icon component={RiCheckboxMultipleBlankLine} />
export const submissionType = "submission"
export const requirementSubmissionType = "requirement-submission"
export const productSubmissionType = "product-submission"
export const dateChangeIcon =   <Icon style={{ color: 'orange' }} component={GiBackwardTime} />
export const manageSubmitIcon =   <Icon component={BsCloudUploadFill} />

          
export const  submissionSingular = "submission.module.name_singular"
export const  requirementSubmissionSingular = "submission.module.requirement_name_singular"
export const  productSubmissionSingular = "submission.module.product_name_singular"
export const  submissionPlural = "submission.module.name_plural"
export const  requirementSubmissionPlural = "submission.module.requirement_name_plural"
export const  productSubmissionPlural = "submission.module.product_name_plural"

export const latestSubmission = "latest"

export const rejectIcon = <Icon component={AiOutlineRollback} /> 
export const manageIcon = icns.admin


export type StatsType = 'submission' | 'singlecompliance' | 'multicompliance'

export const standardPerformanceGroups =  {

    1 : {label:"[0%-33%]",color:"red"},
    2 : {label:"[33%-66%]",color:"orange"},
    3 : {label:"[66%-99%]",color:"#1890ff"},
    4 : {label:"100%",color:"#4fcc12"}

}

export const performanceGroups: Record<StatsType,Record<GroupOrdinal,{label:string,color:string}>> = {

    "submission" : standardPerformanceGroups

    ,

    "singlecompliance": {

        1 : {label:"N/A",color:'lightgray'},
        2 : {label:"[0%-50%]",color:'red'},
        3 : {label:"[50%-99%]",color:'orange'},
        4 : {label:"100%",color:'#4fcc12'}

    }

    ,

    "multicompliance" : standardPerformanceGroups

}