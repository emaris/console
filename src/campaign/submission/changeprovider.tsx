import { Icon } from 'antd'
import { fallbackStateOver, State } from 'apprise-frontend-core/state/api'
import { StateProvider } from 'apprise-frontend-core/state/provider'
import { createContext, PropsWithChildren } from 'react'
import { RiNotificationBadgeFill } from 'react-icons/ri'


export const changeicon = <Icon component={RiNotificationBadgeFill} />
export const changedateformat = 'll HH:mm'

export const ChangelogContext = createContext<State<boolean>>(fallbackStateOver(false))

export const ChangelogCompareContext = createContext<State<JSX.Element | undefined>>(undefined!)

export const ChangeProvider = (props: PropsWithChildren<{ value?: boolean }>) => {

    const { value } = props

    return <StateProvider initialState={!!value} context={ChangelogContext}>
        {props.children}
    </StateProvider>
}