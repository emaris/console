
import { Button } from '#app/components/Button'
import { DrawerProps, RoutableDrawerProps, useRoutableDrawer } from '#app/components/Drawer'
import { RouteGuard } from '#app/components/RouteGuard'
import { Paragraph } from '#app/components/Typography'
import { Form } from '#app/form/Form'
import { useFormState } from '#app/form/hooks'
import moment from 'moment-timezone'
import * as React from 'react'
import { useT } from '#app/intl/api'
import { manageSubmitIcon, submissionIcon } from './constants'
import { useDateChangePicker } from './DateChange'
import { SubmissionStub } from './model'



export type Props = {

    submission: SubmissionStub
    onSubmit: (date: string) => any

}


export const useManagedSubmissionDrawer = (opts: Partial<RoutableDrawerProps> = {}) => {

    const t = useT()

    const { id = 'managedsubmit', title = t("submission.managesubmit.title"), icon = submissionIcon } = opts

    const { Drawer, open, close, visible, route, param } = useRoutableDrawer({ id, title, icon })

    const DrawerProxy = (props: Partial<DrawerProps>) => <Drawer width={500} icon={icon} title={title} {...props} />

    //eslint-disable-next-line
    const StableProxy = React.useCallback(DrawerProxy, [visible])


    return { ManagedSubmissionDrawer: StableProxy, openManagedSubmission: open, managedSubmissionRoute: route, closeManagedSubmission: close, managedSubmissionParam: param }

}


export const ManagedSubmission = (props: Props) => {

    const t = useT()
    const { submission, onSubmit } = props

    const initialDate = moment().format()

    const { edited, dirty, change, softReset } = useFormState({ date: initialDate })

    const submit = () => {

        onSubmit(edited.date!).then(()=>softReset({ date: initialDate }))
    }

    const { timepicker, linearHistory } = useDateChangePicker({

        submission,
        date: edited.date,
        onChange: change((m, v) => m.date = v)

    })

    return <div className="submission-dialog-panel">

        <Paragraph className="submission-dialog-intro">{t("submission.managesubmit.introduction")}</Paragraph>

        <div className="submission-dialog-section" style={{ background: 'inherit' }}>

            {React.cloneElement(manageSubmitIcon, { className: 'submission-dialog-icon' })}

            <Paragraph className='submission-dialog-explainer'>{t("submission.managesubmit.explainer")}</Paragraph>

            <Button style={{ marginTop: 30 }} type={linearHistory ? 'primary' : 'danger'} onClick={submit} >
                {t("submission.managesubmit.submit_btn")}
            </Button>

            <Form style={{marginTop:50}}>
                {timepicker}
            </Form>



        </div>

        <RouteGuard when={dirty} />

    </div>
}
