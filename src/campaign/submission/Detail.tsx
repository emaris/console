
import { buildinfo } from '#app/buildinfo'
import { Button } from "#app/components/Button"
import { IdProperty } from "#app/components/IdProperty"
import { Label } from '#app/components/Label'
import { AdvancedRouteGuard } from "#app/components/RouteGuard"
import { Paragraph } from '#app/components/Typography'
import { useRememberScroll } from '#app/components/hooks'
import { FormState, useFormState } from "#app/form/hooks"
import { icns } from "#app/icons"
import { LanguageBox } from '#app/intl/LanguageBox'
import { useCurrentLanguage, useT } from "#app/intl/api"
import { defaultLanguage } from "#app/intl/model"
import { useLocale } from "#app/model/hooks"
import { Page } from "#app/scaffold/Page"
import { Subtitle, Titlebar } from "#app/scaffold/PageHeader"
import { ReadOnly, ReadOnlyContext, ReadOnlyLabel } from '#app/scaffold/ReadOnly'
import { Sidebar } from "#app/scaffold/Sidebar"
import { Tab } from "#app/scaffold/Tab"
import { Topbar } from "#app/scaffold/Topbar"
import { useSettingsDrawer } from "#app/settings/hooks"
import { BytestreamedContext } from '#app/stream/BytestreamedHelper'
import { useBytestreams } from "#app/stream/api"
import { TagLabel, TagList } from '#app/tag/Label'
import { Tenant } from '#app/tenant/model'
import { useTenantStore } from '#app/tenant/store'
import { TimeZoneContext } from "#app/time/hooks"
import { useLogged } from '#app/user/store'
import { through } from "#app/utils/common"
import { useFeedback } from '#app/utils/feedback'
import { paramsInQuery, parentIn } from "#app/utils/routes"
import { complianceIcon } from '#campaign/constants'
import { useCurrentDashboard, useCurrentParty } from "#dashboard/hooks"
import { useComponentData, useLayoutComponents } from '#layout/components/api'
import { AssetSection, NamedAsset, assetSectionid, isNamedAsset, sectionAssetOf } from "#layout/components/assetsection"
import { NamedRequirement, RequirementSection, requirementSectionId, sectionRequirementOf } from "#layout/components/requirementsection"
import { LayoutAssetContext, LayoutConfig } from '#layout/context'
import { Layout, LayoutInstance } from "#layout/model"
import { usePublicName, useSubmissionDownload } from "#layout/pdf/submissionlayout"
import { LayoutProvider } from '#layout/provider'
import { useLayoutSettings } from '#layout/settings/api'
import { FlowViewer, useFlowCount } from "#messages/VirtualFlow"
import { messageIcon, messagePlural, messageType } from "#messages/constants"
import { useProductLayoutInitialiser } from '#product/layoutinitialiser'
import { Product } from "#product/model"
import { useProductStore } from '#product/store'
import { NotAssessableLabel } from '#requirement/Label'
import { reqImportanceCategory, requirementType } from "#requirement/constants"
import { useRequirementLayoutInitialiser } from '#requirement/layoutinitialiser'
import { Requirement } from "#requirement/model"
import { useRequirementStore } from '#requirement/store'
import { Badge, Tooltip } from "antd"
import { saveAs } from 'file-saver'
import moment from 'moment-timezone'
import * as React from "react"
import { useHistory, useLocation } from "react-router-dom"
import { ArchiveLabel, MutedLabel, SuspendedLabel } from '../Label'
import { useTocDrawer } from '../contenttoc'
import { useCurrentCampaign } from "../hooks"
import { AssetInstance, Campaign, useCampaignModel } from "../model"
import { PartyInstance } from "../party/api"
import { ProductInstance, useProductInstances } from "../product/api"
import { useRequirementInstances } from '../requirement/api'
import { AssessmentForm, AssessmentViewer, useAssessmentDrawer, useAssessmentViewerDrawer, useLineageAssessmentDrawer, useMissingAssessmentDrawer } from "./Assessment"
import { useChangeRequestdDrawer } from './ChangeRequest'
import { DateChangeForm, useDateChangeDrawer } from './DateChange'
import { SubmissionForms } from "./Forms"
import { SubmissionHistory } from "./History"
import { SubmissionLabel } from "./Label"
import { SubmissionLifecycle } from "./Lifecycle"
import { ManagedSubmission, useManagedSubmissionDrawer } from './ManagedSubmission'
import { EditManagementFields, ManagementFields, useManagementDrawer } from './Management'
import { TrailSettingsFields } from "./Settings"
import { useTagDrawer } from './TagDrawer'
import { useChangelog } from './changelog'
import { ChangeProvider } from './changeprovider'
import { useSubmissionClient } from './client'
import { dateChangeIcon, latestSubmission, manageIcon, rejectIcon, submissionIcon, submissionSingular, submissionType } from "./constants"
import { SubmissionReactContext } from './context'
import { useSubmissionContext } from "./hooks"
import { Submission, SubmissionContext, SubmissionDto, SubmissionWithTrail, Trail, newMissingSubmission, nextRevision, useSubmission, useSubmissionTopics } from "./model"
import { useSubmissionProfile } from './profile'
import { useRevisions } from './revision'
import "./styles.scss"
import { useSubmissionValidation } from './validation'
import { useTagStore } from '#app/tag/store'
import { prodImportanceCategory } from '#product/constants'
import { contextCategory } from '#app/system/constants'



type ContextProps = {

    trail: Trail

    campaign: Campaign
    asset: AssetInstance
    party: PartyInstance
    tenant: Tenant


}

type Props = ContextProps & {

    submission: Submission,
    relatedSubmissions: SubmissionWithTrail[]
    onReload: (_?: 'includeRelated' | 'onlySubmission') => void
}

type Accent = 'active' | 'official' | 'superseded' | 'missing' | 'na' | 'other'

// puts the submission in form and pushes it down below a context of per-submission data and utilities. 
export const SubmissionDetail = (props: Props) => {

    const { trail, submission, relatedSubmissions, onReload } = props

    const reqstore = useRequirementStore()
    const prodstore = useProductStore()

    const type = trail.key.assetType

    const store = type === requirementType ? reqstore : prodstore

    const formstate = useFormState(submission)

    const { edited, initial, change } = formstate

    const submissionContext: SubmissionContext = { ...props, submission: edited, currentSubmission: initial, relatedSubmissions }

    const source = store.safeLookup(trail.key.asset)

    const explicitParameters = source.properties.layout.parameters

    const layout: LayoutInstance = React.useMemo(() => ({

        ...source.properties.layout,
        parameters: explicitParameters,
        state: edited.content,
        documents: source.documents

        // eslint-disable-next-line
    }), [edited])

    const onChange = change((sub, layout) => { sub.content = layout.state })


    return <LayoutAssetContext.Provider value={source} >

        <SubmissionReactContext.Provider value={submissionContext}  >
            <SubmissionLayoutProvider layout={layout} onChange={onChange}>
                <TrailChangeGuard state={formstate} {...props}>
                    {/* Use to collect readonly causes */}
                    <ReadOnly value={false} delegate>
                        <ChangeProvider>
                            <InnerDetail state={formstate} layout={layout} reload={onReload} />
                        </ChangeProvider>
                    </ReadOnly>
                </TrailChangeGuard>
            </SubmissionLayoutProvider>
        </SubmissionReactContext.Provider>
    </LayoutAssetContext.Provider>


}

export const TrailChangeGuard = (props: React.PropsWithChildren<Props & { state: FormState<Submission> }>) => {

    const { trail, state, onReload, children } = props

    const { edited } = state

    const t = useT()

    const { askConsent } = useFeedback()

    const revisions = useRevisions()

    const history = useHistory()
    const location = useLocation()

    const [ignored, ignoredSet] = React.useState(false)

    const logged = useLogged()

    // register for state changes to location, which work as signals from a global listener of push change notifications.
    // if the location state is the id of the current trail, then we put up a dialog to alert the user and load the latest revision.
    // if the form is dirty though, we give a chance to users to postpone reoad and copy away some of the content theey'd otherwise lose.
    React.useEffect(() => {

        const trailid = location.state

        // clears state or will stay attached to this route and show even on reload.
        location.state = undefined
        window.history.replaceState({}, '')


        // first render or re-renders on "internal changes"
        if (!trailid || trailid !== trail.id)
            return

        const revs = revisions.on(trail)

        // for a cpc, we try to infer notif is about "invisible" managed hence should be silenced.
        // we lift the tenancy visibility rule and see if the first submitted revision is managed, notif should be about it.
        if (logged.isTenantUser() && revs.lastChanged('notenancy')?.lifecycle.state === 'managed')
            return


        const isLatest = revs.latest()?.id === edited.id

        askConsent({


            title: t('push.consent.change_detection_title'),
            content: <>
                <p>{t(isLatest ? "push.consent.change_detection_content_main_latest" : "push.consent.change_detection_content_main", { singular: t(submissionSingular).toLowerCase() })}</p>
                {isLatest && <p>{t("push.consent.change_detection_content_latest")}</p>}
            </>,
            okText: t(isLatest ? 'push.consent.change_detection_reload' : "push.consent.change_detection_redirect"),
            cancelText: isLatest ? t(t('push.consent.change_detection_ignore')) : undefined,

            noValediction: true,

            onOk: isLatest ? onReload : () => history.push(`${parentIn(location.pathname)}/${latestSubmission}`), // don't keep search and go to content.
            onCancel: () => isLatest && ignoredSet(true)

        })


        //eslint-disable-next-line
    }, [location.state])

    return <ReadOnly value={ignored} >
        {children}
    </ReadOnly>


}

export type SubmissionLayoutConfig = LayoutConfig & {
    dashboard: ReturnType<typeof useCurrentDashboard>,
    campaign: Campaign,
    party: PartyInstance,
    tenant: Tenant
}


// takes the layout and builds a context to work with it.
const SubmissionLayoutProvider = (props: React.PropsWithChildren<{

    layout: LayoutInstance
    onChange: (_: Layout) => void

}>) => {

    const { layout, onChange } = props

    const ctx = useSubmissionContext()

    const dashboard = useCurrentDashboard()

    const { campaign, party, asset, submission, tenant } = ctx

    const layoutConfig: SubmissionLayoutConfig = {

        mode: 'live',
        lazyRender: false,
        canUnlock: ctx.canEditUnlocked(),

        campaign,
        party,
        submission,
        tenant

        ,

        dashboard
    }

    const initialiser = asset.instanceType === requirementType ? useRequirementLayoutInitialiser : useProductLayoutInitialiser


    return <BytestreamedContext tenant={party.id} target={submission.id}  >
        <LayoutProvider useInitialiser={initialiser} initialiseParameters={ctx.initialiseParameters} config={layoutConfig} layout={layout} onChange={onChange}>
            {props.children}
        </LayoutProvider>
    </BytestreamedContext>


}

const InnerDetail = (props: { state: FormState<Submission>, layout: LayoutInstance, reload: () => any }) => {

    const { tocBtn, languageSelector, btns, showChangeRequestBtn, Titlebar, TocDialog, ComplianceDialog, AssessmentViewer, ManagementDialog, EditManagementDialog, ChangeRequestDialog, DateChangeDialog, ManagedSubmissionDialog, openDateChange, Settings, changeHighlightBtn } = useDetailComponents(props)

    const t = useT()
    const { search } = useLocation()

    //    const toggles = useAppToggles()

    const ctx = useSubmissionContext()
    const subtopics = useSubmissionTopics().on(ctx.campaign)
    const subprofile = useSubmissionProfile().on(ctx.campaign)

    const currentCampaign = useCurrentCampaign()

    const campaignmodel = useCampaignModel()

    const { tab = 'edit' } = paramsInQuery(search)

    const { state } = props

    const { edited, dirty, softReset, initial } = state


    // const {managementParam } = useManagementDrawer()
    const { datechangeParam } = useDateChangeDrawer()
    const { tagchangeParam } = useTagDrawer()
    const { tocParam } = useTocDrawer()
    const { assessParam } = useAssessmentDrawer()
    const { lineageassessParam } = useLineageAssessmentDrawer()
    const { changeRequestParam } = useChangeRequestdDrawer()

    const canMessage = subprofile.profileOf(ctx.asset.instanceType).canMessage(ctx.asset)

    const pageref = React.useRef<HTMLDivElement>(undefined!)

    //  note: if we navigate from 'latest' to 'latest' (eg from product to linked requirement), browser (or router) forces scroll restoration, seemingly because parametric root is the same.
    // react-ruter v5 shouldn-t be resonsible, but setting window.history.scrollRestoration = 'manual' doesn't stop this either).
    // this fights it explicitly.
    React.useEffect(() => {

        window.scrollTo(0, 0)

    }, [])

    // we do want to remember the scroll when saving first draft though.
    useRememberScroll("new-detail", {
        id: edited.id,          // this keeps the state specific to current draft even if we move to other instance of the same parametric route.
        when: !ctx.isSaved()
    })


    // readonly management.

    const parentReadonly = React.useContext(ReadOnlyContext)

    let readonlyCause = React.useRef<string>(undefined!)

    const setReadOnlyCause = (cause: string) => readonlyCause.current = cause

    // all buttons and fields are readonly if the main content of the page cannot be edited
    // by the logged user in the current context. individual fields and buttons will need to
    // override this for said user and context.
    const readonly = !!parentReadonly || !ctx.canEdit(setReadOnlyCause)

    const readonlyLbl = readonlyCause.current ? <ReadOnlyLabel tip={() => t(readonlyCause.current)} /> : undefined

    const AugmentedTitleBar = React.cloneElement(Titlebar, { readonlyLbl })

    const { flow } = subtopics.topicsAndFlow(ctx.trail, edited)

    const flowId = ctx.submission.id

    const unreadMessages = useFlowCount(flowId, flow)
    const unreadBadge = <Badge dot={unreadMessages > 0} className='unread-message-badge'>{t(messagePlural)}</Badge>

    const accent: Accent = !ctx.isInAudience() ? 'na' : ctx.isOfficial() ? 'official' : ctx.isActive() ? 'active' : ctx.isSuperseded() ? 'superseded' : 'other'

    const badge = accent === 'superseded' ? t('submission.labels.superseded_revision_badge') : accent === 'official' ? t('submission.labels.official_revision_badge') : accent === 'active' ? t('submission.labels.active_revision_badge') : accent === 'na' ? t('submission.labels.not_applicable_badge') : undefined

    const accentBadge = (!ctx.isInAudience() || (ctx.isSubmittedOrAbout() && accent !== 'other')) && <Label className={`accent-badge accent-${accent}`} mode='tag' icon={submissionIcon} title={badge} />

    return <TimeZoneContext.Provider value={currentCampaign.properties.timeZone}>
        <Page ref={pageref} className={`submission-${tab} accent-${accent}`} readOnly={readonly}>


            <Sidebar defaultOpen>

                {btns}

                <br />

                <IdProperty id={edited.id} />

                <br />

                <SubmissionLifecycle submission={edited} onDateChange={ctx.canChangeDate() ? openDateChange : undefined} />

            </Sidebar>


            <Topbar tabBarAnimated={false}>

                {AugmentedTitleBar}

                <Tab default id='edit' icon={icns.edit} name={t("submission.labels.edit")} />
                {/* {<Tab default id='pdf' icon={icns.edit} name={"PDF"} />} */}
                <Tab id='history' disabled={dirty} icon={icns.history} name={t("submission.labels.history")} />
                <Tab id={messageType} disabled={dirty} icon={messageIcon} name={unreadMessages > 0 ? <Tooltip title={t('message.unread_count', { count: unreadMessages })}>{unreadBadge}</Tooltip> : unreadBadge} />

                {changeHighlightBtn && React.cloneElement(<div>{changeHighlightBtn}</div>, { style: { marginRight: 10 } })}

                {accentBadge}

                {ctx.canShowRequestChange() && React.cloneElement(<div>{showChangeRequestBtn}</div>, { style: { marginRight: 10 } })}

                {tocBtn && React.cloneElement(<div>{tocBtn}</div>, { style: { marginRight: 10 } })}

                {React.cloneElement(languageSelector, { style: { marginRight: 10 } })}

                {btns}

            </Topbar>


            {
                tab === `edit` ? <SubmissionForms state={state} />

                    //  : tab === `pdf` ? <SubmissionPDFForms state={state} layout={props.layout} />

                    : tab === 'history' ? <SubmissionHistory />

                        : <FlowViewer id={flowId} flow={flow}
                            multiTenant
                            readOnly={!canMessage || campaignmodel.isArchived(ctx.campaign) || campaignmodel.isMuted(ctx.campaign)}
                            filterTopicBy={t => !t.name.includes(ctx.trail.id)}

                        />}


            {TocDialog}
            {Settings}
            {ComplianceDialog}
            {AssessmentViewer}
            {ManagementDialog}
            {EditManagementDialog}
            {DateChangeDialog}
            {ManagedSubmissionDialog}
            {ChangeRequestDialog}
            {/*             {<ChangelogDialog />} */}


            <AdvancedRouteGuard when={dirty} escapePattern={submissionType} ignoreQueryParams={['drawer-management', assessParam, lineageassessParam, datechangeParam, tagchangeParam, changeRequestParam, tocParam]} onOk={() => softReset(initial)} />


        </Page>
    </TimeZoneContext.Provider>
}

const useDetailComponents = (props: { state: FormState<Submission>, layout: LayoutInstance, reload: () => any }) => {

    const t = useT()
    const { l } = useLocale()
    const history = useHistory()
    const { search } = useLocation()

    const logged = useLogged()
    const currentLang = useCurrentLanguage()
    const { ask } = useFeedback()

    const { tocBtn, TocDrawer } = useTocDrawer()

    const ctx = useSubmissionContext()

    const submission = useSubmission().in(ctx)

    const currentParty = ctx.trail.key.party

    const revisions = useRevisions().on(ctx.trail)
    const subclient = useSubmissionClient().on(ctx.campaign)
    const tenantstore = useTenantStore()
    const subvalidation = useSubmissionValidation()
    const subprofile = useSubmissionProfile().on(ctx.campaign).profileOf(ctx.trail.key.assetType)

    const components = useLayoutComponents()
    const componentData = useComponentData()

    const settings = useLayoutSettings()

    const layoutLang = settings.layoutLanguage ?? currentLang

    const model = useCampaignModel()
    const currentCampaign = useCurrentCampaign()

    const isArchivedCampaign = model.isArchived(currentCampaign)

    //const layoutReport = layout.validate()

    // this has to go...
    const reqstore = useRequirementStore()
    const prodstore = useProductStore()

    const reqinstances = useRequirementInstances().on(currentCampaign)
    const prodinstances = useProductInstances().on(currentCampaign)

    const { party } = useCurrentParty()

    const assessEmpty = () => ask({
        title: t('submission.consent.assess_missing.title'),
        content: t('submission.consent.assess_missing.msg'),
        okText: t('submission.consent.assess_missing.confirm')
    }).thenRun(() => subclient.save(newMissingSubmission(edited), ctx.trail)
        .then(through(() => { if (ctx.isSaved()) componentData.removeDescriptors() }))
        .then(saved => { if (ctx.isSaved()) softReset(saved) })
        .then(_ => openAssessment())
    )

    const { SettingsDrawer, settingsButton, closeSettings } = useSettingsDrawer()
    const { ComplianceDrawer, assessBtn, closeCompliance } = useAssessmentDrawer()
    const { assessMissingBtn, openAssessment, assessParam } = useMissingAssessmentDrawer({ onClick: assessEmpty })
    const { ComplianceViewerDrawer, complianceViewerButton, closeComplianceViewer } = useAssessmentViewerDrawer()
    const { ManagementDrawer, openManagement } = useManagementDrawer()
    const { DateChangeDrawer, openDateChange, closeDataChange } = useDateChangeDrawer()
    const { ManagedSubmissionDrawer, openManagedSubmission, closeManagedSubmission } = useManagedSubmissionDrawer()
    const { ChangeRequestDrawer, openChangeRequest, showChangeRequestBtn } = useChangeRequestdDrawer()
    const { changeHighlightBtn, changePill, computeChangelog } = useChangelog()

    const { state, reload } = props

    const { edited, dirty, reset, softReset, initial, change } = state

    //const initialCtx = useContextFactory(ctx.trail).contextOf(initial);

    //  tasks

    const save = async () => {


        await componentData.uploadResources()

        const submission = edited

        const changelog = computeChangelog()

        // we store the changelog if it's defined,
        // or if already exists and must be retracted.
        if (changelog || initial.content.changelog) {

            submission.content.changelog = changelog
            submission.lifecycle.firstEdited = changelog?.created
        }

        const saved = await subclient.save(submission, ctx.trail)

        // detail is remounted on first save
        if (ctx.isSaved()) {
            componentData.removeDescriptors()
            softReset(saved)
        }
    }
    const changeDate = async (date: string) => {

        const saved = await subclient.changeDate(edited, date, ctx.trail)

        if (saved)
            softReset(saved)

        closeDataChange()

    }


    const saveCompliance = async () => {

        const saved = await subclient.assess(edited, ctx.trail)

        softReset(saved)

        closeCompliance()
    }

    const revokeCompliance = () => ask({
        title: t('submission.consent.revoke.title'),
        content: t('submission.consent.revoke.msg'),
        okText: t('submission.consent.revoke.confirm')
    })
        .thenRun(async () => {

            const revoked = {
                ...edited, lifecycle: { ...edited.lifecycle, compliance: undefined, revokedCompliance: edited.lifecycle.compliance }
            }
            const saved = await subclient.revokeAssessment(revoked, ctx.trail)

            softReset(saved)

            closeComplianceViewer()
        })


    const manageAndChangeAssessment = async () => {

        closeComplianceViewer()

        setTimeout(async () => {

            const managed = await subclient.manage(edited, ctx.trail, {})

            history.push(`${parentIn(history.location.pathname)}/${managed.id}?${assessParam}='true`)


        }, 200)


    }

    const routeTo = (id: string) => `${parentIn(history.location.pathname)}/${id}${search}`

    const revert = () => reset(initial)
    const remove = () => ask({

        title: t('submission.consent.remove.title'),
        content: t('submission.consent.remove.msg'),
        okText: t('submission.consent.remove.confirm'),
        okChallenge: buildinfo.development ? undefined : t('submission.consent.remove.challenge')

    }).thenRun(async () => {

        await subclient.remove(edited)

        reload()

        setTimeout(() => history.push(routeTo(latestSubmission), 50))

    })

    const clone = () => ask({

        title: t('submission.consent.clone.title'),
        content: t('submission.consent.clone.msg'),
        okText: t('submission.consent.clone.confirm'),

    })
        .if(ctx.isSuperseded())

        .thenRun(async () => {

            const officialStub = (revisions.latest('submitted') ?? revisions.latest('missing')!) as SubmissionDto

            const official = officialStub.content ? officialStub : await subclient.fetchOneSubmission(officialStub.id)

            const clone = nextRevision(official)

            subclient.save(clone, ctx.trail).then(sub => history.push(routeTo(sub.id)))

        })


    const valueFor = (type: string, asset: Requirement | Product) => {

        // lookup the requirement this parameter is configured with.
        const instance = type === requirementType ?
            reqinstances.safeLookupBySource(asset.id)
            :
            prodinstances.safeLookupBySource(asset.id)

        // if we're rendering a requirement, take live data. 
        // if we're rendering a report, seek a match among related submissions.
        const trailAndSubmission = ctx.asset.instanceType === requirementType ? { submission: ctx.submission, trail: ctx.trail } :

            ctx.relatedSubmissions.find(sub => sub.trail.key.asset === instance?.source) ?? {} as SubmissionWithTrail

        return {

            id: asset.id,

            liveData: {

                asset,
                instance,
                ...trailAndSubmission
            }
            ,
            type
        }
    }

    const namedAssetToInstance = (namedAsset: NamedAsset): AssetInstance | ProductInstance | undefined => {
        const { asset, type } = namedAsset
        const resolved = type === requirementType ? reqstore.safeLookup(asset) : prodstore.safeLookup(asset)
        const value = valueFor(type, resolved)
        return value.liveData?.instance
    }

    const namedRequirementToInstance = (namedRequirement: NamedRequirement): AssetInstance | ProductInstance | undefined => {
        const { requirement } = namedRequirement
        const resolved = reqstore.safeLookup(requirement)
        const value = valueFor(requirementType, resolved)
        return value.liveData?.instance
    }

    const isForParty = (asset: NamedAsset | NamedRequirement): boolean => {
        const instance = isNamedAsset(asset) ? namedAssetToInstance(asset) : namedRequirementToInstance(asset)
        const type = isNamedAsset(asset) ? asset.type : requirementType
        return (party && instance) ? type === requirementType ? reqinstances.isForParty(party, instance) : prodinstances.isForParty(party, instance) : true
    }

    const submissionState = () => edited.lifecycle.state === 'draft' ? (ctx.hasApproveCycle() ? 'pending' : 'submitted') : 'submitted'


    const submit = () => {

        // requirement references that have been resolved into requirement submissions.
        const relatedRequirements = ctx.relatedSubmissions.flatMap(s => s.trail.key.asset)

        const unresolved = [
            ...components.allOf<RequirementSection>(requirementSectionId).flatMap(c => c.requirements).filter(isForParty).filter(r => sectionRequirementOf(r) && !relatedRequirements.includes(sectionRequirementOf(r)!)),
            ...components.allOf<AssetSection>(assetSectionid).flatMap(c => c.assets).filter(isForParty).filter(r => sectionAssetOf(r) && !relatedRequirements.includes(sectionAssetOf(r)!))
        ]

        // const errors = layoutReport.errors()

        const confirm = ask({

            title: <span style={{ color: 'red' }}>{t("submission.actions.products.warns.no_req_submissions.title")}</span>,
            content: <div>{t("submission.actions.products.warns.no_req_submissions.contents")} <br /> {
                unresolved.map((r, i) => <span key={i}><b>{l(reqstore.safeLookup(sectionAssetOf(r)).name)}</b><br /></span>)}
            </div>

        }).if(unresolved.length > 0)
        // .thenAsk(
        //     {
        //         title: <span style={{ color: 'red' }}>{t('submission.consent.errors.title')}</span>,
        //         content: t('submission.consent.errors.msg', { errors }),
        //         okText: t('submission.consent.errors.confirm'),
        //     }
        // ).if(errors > 0)


        // SEC users with privileges bypass cycle.
        if (ctx.canSubmitManaged() && (!ctx.hasApproveCycle() || ctx.isPending() || ctx.canBypassApproval()))
            confirm.thenRun(openManagedSubmission)

        else

            // CPC users submit drafts for approval if the cycle is on (bypass not on or user is not privileges enough).
            confirm.thenAsk({
                title: t('submission.consent.pending.title'),
                content: t('submission.consent.pending.msg'),
                okText: t('submission.consent.pending.confirm'),

            })
                .if(ctx.isDraft() && ctx.hasApproveCycle())

                // used  for CPCs as an alternative dialog, not in chain, in two cases:
                // there's no cycle (eg. bypass is on and user is privileged) or there's a cycle but we're pending alrady.       
                .thenAsk({

                    title: t('submission.consent.submit.title'),
                    content: t('submission.consent.submit.msg'),
                    okText: t('submission.consent.submit.confirm'),

                }).if((!ctx.hasApproveCycle() || ctx.isPending()) && logged.isTenantUser())

                .thenRun(() => {

                    subclient.submit({ ...edited, lifecycle: { ...edited.lifecycle, state: submissionState(), lastSubmitted: moment().format() } }, ctx.trail).then(softReset)

                })

    }

    const submitManaged = (date: string) => subclient.submit({ ...edited, lifecycle: { ...edited.lifecycle, state: submissionState(), lastSubmitted: date } }, ctx.trail)
        .then(softReset)
        .then(closeManagedSubmission)

    const reject = (changeRequest: string) => subclient.reject({ ...edited, lifecycle: { ...edited.lifecycle, changeRequest } }, ctx.trail).then(softReset)


    const { downloadSubmissionPdf, generateSubmissionPdf } = useSubmissionDownload(props.layout)

    const publish = () =>

        ask({

            title: t('submission.consent.publish.title'),
            content: t('submission.consent.publish.msg'),
            okText: t('submission.consent.publish.confirm')

        })
            .thenRun(() => subclient.publish(ctx.submission, ctx.trail).then(s => history.push(routeTo(s.id))))

    const publicName = usePublicName()

    const share = () => {

        const excludedParties = currentCampaign.properties.statExcludeList ?? []

        const tenantName = l(tenantstore.safeLookup(currentParty).name)

        if (excludedParties.includes(currentParty)) {
            ask({

                title: t('submission.consent.share.title'),
                content: <div className='share-excluded-modal'>
                    <Paragraph className='modal-warn'>{t('submission.consent.share.msg')}</Paragraph>
                    <Paragraph className='modal-warn modal-warn-red'>{t('submission.consent.share.msg_excluded', { party: tenantName.toLocaleUpperCase() })}</Paragraph>
                    <Paragraph className='modal-warn'>{t('submission.consent.share.sure')}</Paragraph></div>,
                okText: t('submission.consent.share.confirm'),
                okChallenge: 'SHARE'

            }).thenRun(() =>

                // generateSubmissionPdf().then(blob => blob && subclient.shareSubmission(edited, ctx.trail, blob, publicName, layoutLang).then(softReset)).then(_ => downloadSubmissionPdf(publicName))
                generateSubmissionPdf().then(blob => {
                    blob && subclient.share(edited, ctx.trail, blob, publicName, layoutLang).then(softReset)
                    return blob
                }).then(blob => blob && saveAs(blob, publicName))

            )
        } else {
            ask({

                title: t('submission.consent.share.title'),
                content: t('submission.consent.share.msg'),
                okText: t('submission.consent.share.confirm'),

            }).thenRun(() =>

                // generateSubmissionPdf().then(blob => blob && subclient.shareSubmission(edited, ctx.trail, blob, publicName, layoutLang).then(softReset)).then(_ => downloadSubmissionPdf(publicName))
                generateSubmissionPdf().then(blob => {
                    blob && subclient.share(edited, ctx.trail, blob, publicName, layoutLang).then(softReset)
                    return blob
                }).then(blob => blob && saveAs(blob, publicName))

            )
        }
    }


    //  buttons
    const saveBtn = ctx.canShowSaveAndRevert() && <Button key='save' icn={icns.save} enabled={(dirty || !ctx.isSaved()) && ctx.canSaveAndRevert()} enabledOnReadOnly={ctx.canSaveAndRevert()} onClick={save}>
        {t("common.buttons.save")}
    </Button>


    const revertBtn = ctx.canShowSaveAndRevert() && <Button key='revert' enabledOnReadOnly={ctx.areSubmissionsAllowed()} icn={icns.revert} enabled={dirty} onClick={revert}>
        {t("common.buttons.revert")
        }</Button>

    const addNewBtn = ctx.canShowCreate() && <Button key='add' enabledOnReadOnly={ctx.areSubmissionsAllowed()} enabled={ctx.canCreate() && ctx.isVersionable()} icn={icns.add} onClick={clone}>
        {t("common.buttons.new_one", { singular: t(submissionSingular) })}
    </Button>

    const assessmentMissingBtn = ctx.canShowAssessMissing() && React.cloneElement(assessMissingBtn, { key: 'assess-missing', disabled: dirty, enabled: ctx.canAssessMissing(), enabledOnReadOnly: ctx.canAssessMissing() })

    const submitTitle = ctx.isPending() ? "submission.labels.approve_and_submit"
        : ctx.isDraft() && ctx.isApproveCycleActive() ?
            ctx.canBypassApproval() ?
                "submission.labels.approve_and_submit"
                : "submission.labels.submit_for_approval"

            : "submission.labels.submit"

    const submitBtn = ctx.canShowSubmit() && <Button enabledOnReadOnly={ctx.canSubmit()} key='submit' icn={icns.upload} enabled={ctx.canSubmit()} onClick={submit}>
        {t(submitTitle)}
    </Button>


    const removeBtn = ctx.canShowRemove() && <Button key='remove' type="danger" icn={icns.remove} enabledOnReadOnly={ctx.canRemove() || buildinfo.development} enabled={ctx.canRemove() || buildinfo.development} onClick={remove}>
        {`${t("common.buttons.remove")} ${(buildinfo.development && !ctx.canRemove()) ? '[DEV]' : ''}`}
    </Button>

    const viewAssessmentBtn = ctx.canShowViewAssessment() && React.cloneElement(complianceViewerButton, { key: 'view-assessment', enabledOnReadOnly: ctx.canViewAssessment(), enabled: ctx.canViewAssessment() })

    const assessmentBtn = ctx.canShowAssess() && React.cloneElement(assessBtn, { key: 'assess', enabledOnReadOnly: ctx.canAssess(), enabled: ctx.canAssess() })

    const rejectBtn = ctx.canShowReject() && <Button key='reject' icn={rejectIcon} enabledOnReadOnly={ctx.canReject()} enabled={ctx.canReject()} onClick={openChangeRequest}>
        {t("submission.labels.reject")}
    </Button>


    const manageBtn = ctx.canShowManage() && <Button key="manage" icn={manageIcon} onClick={openManagement} enabledOnReadOnly={ctx.canManage()} enabled={ctx.canManage()}>
        {t("submission.labels.manage")}
    </Button>


    const editManagementBtn = ctx.canShowEditManagementData() && <Button key="editMgmt" icn={manageIcon} onClick={openManagement} enabledOnReadOnly={ctx.canEditManagementData()} enabled={ctx.canEditManagementData()}>
        {t("submission.labels.manage_edit")}
    </Button>

    const changeDateBtn = ctx.canShowChangeDate() && <Button key='changedate' enabledOnReadOnly={ctx.canChangeDate()} icn={dateChangeIcon} enabled={ctx.canChangeDate()} onClick={openDateChange}>
        {t("submission.datechange.open_lbl")}
    </Button>


    const publishBtn = ctx.canShowPublish() && <Button key="publish" icn={icns.publish} onClick={publish} enabledOnReadOnly={ctx.canPublish()} enabled={ctx.canPublish()}>
        {t("submission.labels.publish")}
    </Button>

    const shareBtn = ctx.canShowShare() && <Button key="share" icn={icns.share} onClick={share} enabledOnReadOnly={ctx.canShare()} enabled={ctx.canShare()}>
        {t("submission.labels.share")}
    </Button>


    const previousBtn = <Button key='previous' enabledOnReadOnly icn={icns.left} onClick={() => history.push(routeTo(revisions.previous(edited)?.id))} disabled={!revisions.previous(edited)}>
        {t("submission.labels.previous_rev")}
    </Button>

    const nextBtn = <Button key='next' enabledOnReadOnly icn={icns.right} onClick={() => history.push(routeTo(revisions.next(edited)?.id))} disabled={!revisions.next(edited)}>
        {t("submission.labels.next_rev")}
    </Button>

    const downloadBtn = /* ctx.isSaved() &&  */<Button icn={icns.download} key="download" enabledOnReadOnly onClick={() => downloadSubmissionPdf(publicName)}>{t("submission.labels.download")}</Button>

    const fallbackBtn = ctx.isSaved() ? ctx.canCreate() ? addNewBtn : downloadBtn : saveBtn

    const primary =

        ctx.isDraft() ?

            dirty ? saveBtn : ctx.canSubmit() ? submitBtn : ctx.canAssessMissing() ? assessmentMissingBtn : fallbackBtn

            : ctx.isPending() ? ctx.canSubmit() ? submitBtn : fallbackBtn

                : ctx.isLikeSubmitted() ?

                    ctx.isAssessed() ?

                        viewAssessmentBtn

                        : ctx.canAssess() ?

                            assessmentBtn

                            :

                            ctx.canManage() ? manageBtn : fallbackBtn

                    : ctx.isManaged() ?

                        ctx.canPublish() ?

                            dirty ? saveBtn : publishBtn

                            : assessmentBtn


                        : ctx.isPublished() ?

                            // post-publish data can dirty a published revision.
                            dirty ? saveBtn :

                                ctx.canShare() ? shareBtn

                                    : ctx.isAssessed() ? viewAssessmentBtn

                                        : ctx.canManage() ? manageBtn

                                            : fallbackBtn

                            : fallbackBtn       // this should never happen, if we've covered all cases above.



    let primaryBtn = primary && React.cloneElement(primary, { type: 'primary' })

    const btnSpacer = <br />

    let secondaryBtns = [

        saveBtn,
        revertBtn,
        assessmentMissingBtn,
        submitBtn,
        assessmentBtn,
        viewAssessmentBtn,
        publishBtn,
        rejectBtn,
        manageBtn,
        editManagementBtn,
        changeDateBtn,
        shareBtn,
        removeBtn,
        btnSpacer,
        downloadBtn,
        addNewBtn,
        btnSpacer,
        previousBtn,
        nextBtn,
        React.cloneElement(settingsButton, { key: 'settings', enabledOnReadOnly: true })

    ]
        // removes primary button 
        .filter(btn => btn && btn !== primary)
        // remove falsey and unncessary spacers. 
        .filter(btn => !!btn).filter((btn, i, array) => btn !== btnSpacer || (i > 0 && array[i - 1] !== btnSpacer))

    //
    const btns = [primaryBtn, ...secondaryBtns].filter(b => b).map((b, i) => React.cloneElement(b as JSX.Element, { key: (b as JSX.Element).key ?? i }))


    //  -------  title



    const asset = subprofile.instance(ctx.trail.key.asset)

    const stream = useBytestreams()

    const assetSource = subprofile.source(asset.source)
    const partyName = l(tenantstore.safeLookup(ctx.party.source).name)
    const assetName = l(assetSource.name)

    const assetTitle = l(assetSource.description) ?? assetName
    const assetSourceName = l(assetSource.properties?.source?.title)

    const title = logged.managesMultipleTenants() ? `${partyName}: ${assetTitle}` : assetTitle

    const complianceTag = ctx.isAssessable() ?
        (ctx.isSubmitted() || ctx.isAssessed() || ctx.isRevoked()) && ctx.complianceScale() && <SubmissionLabel displayMode='compliance' submission={edited} />

        :

        <NotAssessableLabel />

    let submittedTag = <>{ctx.isSaved() && <SubmissionLabel displayMode='state-only' tipMode='author' submission={edited} />}{ctx.isSubmittedOrAbout() && <SubmissionLabel mode='tag' displayMode='date-pill' noPseudoNames locale={layoutLang} submission={edited} />}</>

    const official = revisions.official()

    submittedTag = official?.id !== edited.id && official?.lifecycle.state === 'missing' ? <><SubmissionLabel displayMode='state-only' tipMode='author' submission={official} />{submittedTag}</> : submittedTag


    const getPublicationPill = (submission: Submission) => {
        const language = settings.layoutLanguage ?? defaultLanguage
        const sharedBytestream = submission.lifecycle.publication?.[language] ?? submission.lifecycle.publication?.[defaultLanguage]
        return sharedBytestream ? <a download href={stream.linkOf(sharedBytestream)}>
            <TagLabel tag={t("dashboard.labels.shared_documents")} icon={icns.share} noTip className={'share-tag'} />
        </a> : <></>
    }
    const publicationPill = <>{edited.lifecycle.publication && getPublicationPill(edited)}</>

    const cause = React.useRef<string>(undefined!)

    const setCause = (v: string) => cause.current = v

    ctx.canEdit(setCause)

    const notInAudiancePill = !submission.isInAudience() ? <><TagLabel className='notapplicable lbl' tag={t("submission.labels.not_applicable_badge")} icon={complianceIcon} tip={t(cause.current)} /></> : undefined

    const pills = (edited.lifecycle.state === 'published' || edited.lifecycle.state === 'managed') ?
        <>{notInAudiancePill}{complianceTag}{submittedTag}{publicationPill}</> :
        <>{notInAudiancePill}{submittedTag}{complianceTag}</>



    const tags = useTagStore()

    const filteredAssetTags = asset.tags.filter(tag => ![contextCategory, reqImportanceCategory, prodImportanceCategory].includes(tags.lookupTag(tag).category ?? ''))

    const Title = <Titlebar title={title}>
        <Subtitle className='submission-subtitle'>
            {assetSourceName &&
                <div>
                    <span className='subtitle-label'>{t("common.labels.source")}:</span>&nbsp;{assetSourceName}
                </div>
            }
        </Subtitle>
        {changePill}
        <ArchiveLabel campaign={ctx.campaign} />
        <SuspendedLabel campaign={ctx.campaign} />
        <MutedLabel campaign={ctx.campaign} />
        {subprofile.label(asset, { mode: 'tag', deadlineOnly: true, deadlineMode: 'default', format: 'numbers', accentScheme: "weeks", locale: layoutLang })}
        {pills}
        <TagList noStretch light={false} renderProps={{ noIcon: true, noDecorations: true }} taglist={filteredAssetTags} />
        <span style={{ marginLeft: 8 }} />
        <TagList light={false} renderProps={{ noIcon: true, noDecorations: true, className: 'submission-tag' }} taglist={edited.lifecycle.tags} />
    </Titlebar>

    const languageSelector = <LanguageBox className='layout-language-selector' light enabledOnReadOnly onChange={l => settings.changeLayoutLanguage(l)} selectedKey={settings.resolveLanguage()} />

    const ComplianceDialog = ctx.complianceScale() &&

        <ComplianceDrawer enabledOnReadOnly={!isArchivedCampaign} readonly={!ctx.canAssess()}>
            <AssessmentForm state={state} scale={ctx.complianceScale()!} timeliness={ctx.timelinessScale()!} onSave={saveCompliance} />
        </ComplianceDrawer>

    const AssessmentViewerDrawer = ctx.isAssessed() &&

        <ComplianceViewerDrawer enabledOnReadOnly={!isArchivedCampaign}>
            <AssessmentViewer submission={edited} onRevokeCompliance={revokeCompliance} onManage={manageAndChangeAssessment} />
        </ComplianceViewerDrawer>


    const ManagementDialog = ctx.canManage() &&

        <ManagementDrawer enabledOnReadOnly={!isArchivedCampaign}>
            <ManagementFields />
        </ManagementDrawer>


    const ChangeRequestDialog = (ctx.canReject() || ctx.canShowRequestChange()) &&

        <ChangeRequestDrawer enabledOnReadOnly onReject={reject} />


    const EditManagementDialog = ctx.isManaged() && ctx.canEditManagementData() &&

        <ManagementDrawer enabledOnReadOnly={!isArchivedCampaign}>
            <EditManagementFields data={edited.lifecycle} onChange={change((t, data) => t.lifecycle = { ...t.lifecycle, ...data })} />
        </ManagementDrawer>

    const DateChangeDialog = ctx.canChangeDate() &&

        <DateChangeDrawer enabledOnReadOnly={!isArchivedCampaign} >
            <DateChangeForm submission={edited} onChange={changeDate} />
        </DateChangeDrawer>


    const ManagedSubmissionDialog = ctx.canSubmitManaged() &&

        <ManagedSubmissionDrawer enabledOnReadOnly={!isArchivedCampaign}>
            <ManagedSubmission submission={edited} onSubmit={submitManaged} />
        </ManagedSubmissionDrawer>


    const TocDialog = <TocDrawer enabledOnReadOnly />

    const report = subvalidation.validateSubmission(edited)

    const trailstate = useFormState(ctx.trail)

    const Settings = <SettingsDrawer enabledOnReadOnly={!isArchivedCampaign} readonly={!ctx.canChangeSettings() || isArchivedCampaign} warnOnClose={trailstate.dirty}>
        <TrailSettingsFields state={trailstate} onSave={closeSettings} report={report} />
    </SettingsDrawer>

    return {
        tocBtn, languageSelector,

        btns,
        changeHighlightBtn,
        showChangeRequestBtn,
        Titlebar: Title,
        Settings,
        AssessmentViewer: AssessmentViewerDrawer,
        ManagementDialog,
        DateChangeDialog, openDateChange, closeDataChange,
        EditManagementDialog,
        ManagedSubmissionDialog,
        ComplianceDialog,
        ChangeRequestDialog,
        TocDialog
    }


}

