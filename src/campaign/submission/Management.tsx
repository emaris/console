
import { Button } from '#app/components/Button'
import { DrawerProps, RoutableDrawerProps, useRoutableDrawer } from '#app/components/Drawer'
import { Paragraph } from '#app/components/Typography'
import { Form } from '#app/form/Form'
import { useT } from '#app/intl/api'
import { TagBoxset } from '#app/tag/TagBoxset'
import { useTagStore } from '#app/tag/store'
import { stripMultiHtmlTags } from '#app/utils/common'
import { useStable } from '#app/utils/function'
import { parentIn } from '#app/utils/routes'
import { useCurrentCampaign } from '#campaign/hooks'
import { useLayoutParameters } from '#layout/parameters/api'
import { ParametricMultiTextBox } from '#layout/parameters/parametricboxes'
import { useReferenceParam } from '#layout/parameters/reference'
import { useLayoutSettings } from '#layout/settings/api'
import { productType } from '#product/constants'
import { Icon } from 'antd'
import { useState } from 'react'
import { RiGroup2Fill } from 'react-icons/ri'
import { useHistory } from 'react-router-dom'
import { useSubmissionClient } from './client'
import { manageIcon } from './constants'
import { useSubmissionContext } from './hooks'
import { ManagementData } from './model'
import { useRevisions } from './revision'








export const ManagementFields = () => {

    const t = useT()

    const { multir } = useLayoutParameters()

    const history = useHistory()

    const campaign = useCurrentCampaign()

    const subclient = useSubmissionClient().on(campaign)

    const { closeManagement } = useManagementDrawer()

    const { submission, trail } = useSubmissionContext()

    const type = trail.key.assetType

    const ref = useReferenceParam()

    // collects management data for when it's the time to create a new revision.
    const [data, setData] = useState<ManagementData>(() => ({ reference: ref.value() ? stripMultiHtmlTags(multir(ref.value())) : undefined, tags: submission.lifecycle.tags ?? [] }))

    const routeTo = (id: string) => `${parentIn(history.location.pathname)}/${id}`

    const manage = async () => {

        closeManagement()

        // this creates a new revision and a new revision will cause an unmount, so let's give the close animation time to finish.
        setTimeout(() => subclient.manage(submission, trail, data).then(s => history.push(routeTo(s.id))), 200)


    }

    return <div style={{ alignItems: 'inherit' }} className="submission-dialog-panel">

        <Paragraph className="submission-dialog-intro">{t("submission.management.introduction")}</Paragraph>

        <div className="submission-dialog-section" style={{ background: 'inherit' }}>

            <Icon style={{ color: 'cadetblue' }} className='submission-dialog-icon' component={RiGroup2Fill} />

            <Paragraph className='submission-dialog-explainer'>{t(`submission.management.description_${type}`)}</Paragraph>

            <Button style={{ marginTop: 30 }} type="primary" onClick={manage} >
                {t("submission.management.confirm_btn")}
            </Button>

        </div>

        <EditableFields data={data} onChange={setData} mode='manage' />


    </div>

}


export const EditManagementFields = (props: Omit<EditableFieldProps, 'mode'>) => {

    const t = useT()


    return <div style={{ alignItems: 'inherit' }} className="submission-dialog-panel">

        <Paragraph className="submission-dialog-intro">{t("submission.editmanagement.introduction")}</Paragraph>

        <div className="submission-dialog-section" style={{ background: 'inherit' }}>

            <Icon style={{ color: 'cadetblue' }} className='submission-dialog-icon' component={RiGroup2Fill} />

        </div>

        <EditableFields {...props} mode='edit' />


    </div>

}


export type EditableFieldProps = {

    data: ManagementData,
    onChange: (_: ManagementData) => any
    mode: 'manage' | 'edit'
}

// used to first gather management data, and to edit it afterwards
export const EditableFields = (props: EditableFieldProps) => {

    const t = useT()
    const { data, onChange, mode } = props

    const type = useSubmissionContext().trail.key.assetType

    const ctx = useSubmissionContext()

    const tagstore = useTagStore()

    const revisions = useRevisions().on(ctx.trail)

    const { multir } = useLayoutParameters()

    const last = revisions.all().find(s => (mode==='manage' || s.id !== ctx.submission.id) && s.lifecycle.reference)?.lifecycle.reference
    
    const categories = (ctx.asset.properties.submissionTagMap?.map(p => p.category) ?? []).map(tagstore.lookupCategory)

    const { ll } = useLayoutSettings()

    const referenceMsg = last ? t('submission.reference.last_reference', { last: ll(last) }) : t('submission.reference.first_reference')

    return <div style={{ alignItems: 'inherit', textAlign: 'left' }} className="submission-dialog-panel">

        <Form >
            
            { type === productType &&

                <ParametricMultiTextBox label={t("submission.reference.title")} id='reference' validation={{ msg: referenceMsg }} noToolbar onChange={reference => onChange({ ...data, reference: stripMultiHtmlTags(multir(reference)) })}>
                    {data.reference}
                </ParametricMultiTextBox>
            }

            <TagBoxset categories={categories} edited={data.tags} onChange={tags => onChange({ ...data, tags })} />

        </Form>
    </div>

}



export const useManagementDrawer = (opts: Partial<RoutableDrawerProps> = {}) => {

    const t = useT()

    const { id = 'management', title = t("submission.management.title"), icon = manageIcon } = opts


    const { Drawer, open, close, route, param } = useRoutableDrawer({ id, title, icon })

    const ManagementDrawer = useStable((props: Partial<DrawerProps>) => <Drawer width={600} icon={icon} title={title} {...props} />)

    return { ManagementDrawer, openManagement: open, managementRoute: route, closeManagement: close, managementParam: param }

}

