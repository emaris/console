import { Button } from "#app/components/Button"
import { Column, VirtualTable } from "#app/components/VirtualTable"
import { icns } from "#app/icons"
import { useT } from '#app/intl/api'
import { TagList } from '#app/tag/Label'
import { useTime } from "#app/time/api"
import { UserLabel } from "#app/user/Label"
import { useLogged } from '#app/user/store'
import { useFeedback } from '#app/utils/feedback'
import { parentIn } from "#app/utils/routes"
import { useCurrentDashboard } from '#dashboard/hooks'
import moment from "moment-timezone"
import { useLocation } from 'react-router'
import { useHistory } from "react-router-dom"
import { SubmissionLabel } from "./Label"
import { useSubmissionClient } from './client'
import { latestSubmission } from "./constants"
import { useContextFactory, useSubmissionContext } from './hooks'
import { Submission, SubmissionStub } from "./model"
import { useRevisions } from './revision'
import { buildinfo } from '#app/buildinfo'



export const SubmissionHistory = () => {

    const t = useT()

    const { search } = useLocation()

    const logged = useLogged()

    const history = useHistory()

    const ctx = useSubmissionContext()
    const subclient = useSubmissionClient().on(ctx.campaign)
    const ctxfactory = useContextFactory(ctx.trail)

    const time = useTime()
    const { ask } = useFeedback()

    const dashboard = useCurrentDashboard()

    const summary = dashboard.summaries.trailSummaryOf(ctx.trail.key)

    const now = time.current()

    const routeTo = (id: string) => `${parentIn(history.location.pathname)}/${id}`

    const revisions = useRevisions().on(ctx.trail)

    const data = revisions.all()

    const submissionGroups = revisions.submittedGroups()
    const submissionMultiGroups = submissionGroups.filter(g => g.length > 1)

    const openbtn = (s: SubmissionStub) => <Button enabledOnReadOnly key={'opem'} icn={icns.open} linkTo={routeTo(s.id)}>{t("common.buttons.open")}</Button>

    const remove = (s: SubmissionStub) => {

        const nextRoute = ctx.submission.id !== s.id ? ctx.submission.id : revisions.previous(s)?.id ?? revisions.next(s)?.id ?? latestSubmission

        return () => ask({

            title: t('submission.consent.remove.title'),
            content: t('submission.consent.remove.msg'),
            okText: t('submission.consent.remove.confirm'),
            okChallenge:  buildinfo.development ? undefined : t('submission.consent.remove.challenge')

        }).thenRun(() => subclient.remove(s).then(() => history.push(`${routeTo(nextRoute)}${search}`)))

    }

    const removebtn = (s: SubmissionStub) => <Button enabledOnReadOnly key={'remove'} enabled={ctxfactory.contextOf(s as Submission).canRemove()} icn={icns.remove} onClick={remove(s)}>{t("common.buttons.remove")}</Button>

    const latest = revisions.latest()
    
    const mostRelevant = latest?.lifecycle.state === 'draft' ? latest  : revisions.official() ?? latest

    // const calculateClassFor = (submission: Submission):string => {

    //     const groupForSubmission = (sub: Submission):SubmissionStub[] => submissionGroups.reduce((acc, cur) => acc = cur.find(s => s.id === sub.id) ? cur : acc, [])
    //     const indexOfGroupBySubmission = (sub: Submission): number => submissionGroups.findIndex(g => g.find(s => s.id === sub.id) !== undefined)

    //     const isSubmissionOnFirstGroup = (sub: Submission): boolean => indexOfGroupBySubmission(sub) === submissionGroups.length-1
    //     const isSubmissionTheFirstOnItsGroup = (sub: Submission): boolean => {
    //         const group = groupForSubmission(sub)
    //         return group[group.length-1].id === sub.id
    //     }
    //     const isSubmissionOnMultiSubGroup = (sub: Submission): boolean => groupForSubmission(sub).length > 1
    //     const isPreviousGroupMulti = (sub: Submission): boolean => {
    //         const currentGroupIndex = indexOfGroupBySubmission(sub)
    //         if (currentGroupIndex === submissionGroups.length-1) return false
    //         return submissionGroups[currentGroupIndex+1].length > 1
    //     }

    //     if (isSubmissionOnFirstGroup(submission)) return ''

    //     if ((isSubmissionTheFirstOnItsGroup(submission) && isSubmissionOnMultiSubGroup(submission)) || isPreviousGroupMulti(submission)) return 'row-bottom-green'

    //     return ''
    // }

    const getClassForRow = (stub: SubmissionStub) => {
        
        const idx = submissionMultiGroups.findIndex(g => g.find(s => s.id === stub.id) !== undefined)    

        if (idx < 0) return ''

        const currSelected = submissionMultiGroups[idx].find(s => s.id === ctx.submission.id)

        return currSelected ? 'current-group' : ''
    }
    
    const RowRenderer = (props) => {
        const { submission, ...rest } = props
        const class1 = submission.id === ctx.submission.id ? 'current-row' : getClassForRow(submission)
        const class2 = `${rest.className} ${submission === mostRelevant ? '' : 'accent-grey'}`
        const classes = `${class1} ${class2}`
        return <div {...rest} className={classes} />
    }

    return <VirtualTable<SubmissionStub> data={data} rowKey='id' selectable={false} filtered={false}
        onDoubleClick={s => history.push(routeTo(s.id))}
        sortBy={[['date', 'desc']]}
        // rowClassName={s => s.rowData.id === ctx.submission.id ? 'current-row' : getClassForRow(s.rowData)}
        actions={a => [openbtn(a), removebtn(a)]}
        customProperties={({ rowData }) => ({ submission: rowData })}
        customRowRenderer={RowRenderer}
    >

        <Column<SubmissionStub> flexGrow={2} title={t("submission.revision.col")} dataKey="date" minWidth={300} 
            comparator={revisions.ascendingOrder}
            cellRenderer={({ rowData: s }) => <SubmissionLabel noDateAccent linkTo={routeTo(s.id)} showDateWithTime submission={s} />} />


        {
            ctx.canBeTagged() &&

            <Column<SubmissionStub> title={t("submission.tags.col")} dataKey="tags" minWidth={130} maxWidth={150}
                sortable={false}
                cellRenderer={({ rowData: s }) => <TagList light={false} renderProps={{ noIcon: true, className: 'submission-tag' }} taglist={s.lifecycle.tags} />} />
        }

        <Column<SubmissionStub> title={t("submission.labels.last_changed")} dataKey="lastchanged" minWidth={120} 
            sortable={false}
            cellRenderer={({ rowData: s }) => <SubmissionLabel noIcon noPseudoNames noDateAccent displayMode='date-change' submission={s} />} />

        <Column<SubmissionStub> title={t("submission.labels.changed_by")} dataKey="changedBy" minWidth={160}
            sortable={false}
            cellRenderer={({ rowData: s }) => <UserLabel tip={s.lifecycle.lastEdited ?? s.lifecycle.lastModified ? () => moment(s.lifecycle.lastEdited ?? s.lifecycle.lastModified).from(now) : undefined} user={s.lifecycle.lastEditedBy ?? s.lifecycle.lastModifiedBy} />} />


        {logged.hasNoTenant() &&

            < Column < SubmissionStub > title={t("submission.labels.assessed_by")} dataKey="assessedBy"  minWidth={160} 
                sortable={false}
                cellRenderer={({ rowData: s }) => <UserLabel tip={s.lifecycle.compliance?.lastAssessed ? (() => moment(s.lifecycle.compliance?.lastAssessed).from(now)) : undefined} user={s.lifecycle.compliance?.lastAssessedBy} />} />

        }

        {logged.hasNoTenant() &&

            <Column<SubmissionStub> title={t("submission.labels.published_by")} dataKey="publishedBy" minWidth={160}
                sortable={false}
                cellRenderer={({ rowData: s }) => <UserLabel tip={s.lifecycle.lastPublished ? (() => moment(s.lifecycle.lastPublished).from(now)) : undefined} user={s.lifecycle.lastPublishedBy} />} />
        }

        <Column<SubmissionStub> title={t("submission.labels.compliance_state")} dataKey="assessment" minWidth={200}
            sortable={false}
            cellRenderer={({ rowData: s }) => <SubmissionLabel displayMode='compliance-rating' submission={s} />} />

        <Column<SubmissionStub> title={t("submission.labels.timeliness_state")} dataKey="timeliness" minWidth={200}
            sortable={false}
            cellRenderer={({ rowData: s }) => <SubmissionLabel displayMode='timeliness' submission={s} computedTimeliness={summary.computedTimeliness} />} />


    </VirtualTable>
}