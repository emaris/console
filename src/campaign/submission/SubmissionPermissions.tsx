
import { productPlural, productSingular } from '#product/constants'
import { ProductLabel } from '#product/Label'
import { Product } from '#product/model'
import { useProductStore } from '#product/store'
import { requirementPlural, requirementSingular } from '#requirement/constants'
import { RequirementLabel } from '#requirement/Label'
import { Requirement } from '#requirement/model'
import { useRequirementStore } from '#requirement/store'
import { ChangesProps } from '#app/iam/permission'
import { ResourceProps, StateProps, SubjectProps } from '#app/iam/PermissionTable'
import { useLocale } from '#app/model/hooks'
import { noTenant } from '#app/tenant/constants'
import { User } from '#app/user/model'
import { UserPermissions } from '#app/user/UserPermissionTable'
import * as React from 'react'
import { useT } from '#app/intl/api'
import { productSubmissionActions, requirementSubmissionActions } from './actions'
import { productSubmissionType, requirementSubmissionType } from './constants'

type RequirementPermissionsProps = ChangesProps & Partial<ResourceProps<Requirement>> & Partial<SubjectProps<User>> & Partial<StateProps<User,Requirement>> & {
    edited? : User
}


//  wraps UserPermissions for Requirements, so as to inject appropriate defaults.

export const RequirementSubmissionPermissions =  (props:RequirementPermissionsProps) => {

    const t = useT()
    const {l} = useLocale()

    const requirements = useRequirementStore()


    return  <UserPermissions {...props}
                id="requirement-submission-permissions" 
                subjectRange={props.subjectRange?.filter(s=>s.tenant!==noTenant)}
                resourceSingular={props.resourceSingular || t(requirementSingular)}
                resourcePlural={props.resourcePlural || t(requirementPlural)}
                renderResource={props.renderResource || ((t:Requirement) => <RequirementLabel requirement={t} /> )}
                resourceText={t=>l(t.name)}
                resourceId={props.resourceId || ((r:Requirement) => r.id) }
                renderResourceOption={props.renderResourceOption || ((r:Requirement) => <RequirementLabel noLink requirement={r} />)} 
                resourceRange={props.resourceRange || requirements.all() } 
                resourceType={requirementSubmissionType}

                actions={ Object.values(requirementSubmissionActions)} />
                
 }


 type ProductPermissionsProps = ChangesProps & Partial<ResourceProps<Product>> & Partial<SubjectProps<User>> & Partial<StateProps<User,Product>> & {
    edited? : User
}


 export const ProductSubmissionPermissions =   (props:ProductPermissionsProps) => {

    const t = useT()
    const {l} = useLocale()
    const products = useProductStore()

    return  <UserPermissions {...props}
                id="product-submission-permissions" 
                subjectRange={props.subjectRange?.filter(s=>s.tenant!==noTenant)}
                resourceSingular={props.resourceSingular || t(productSingular)}
                resourcePlural={props.resourcePlural || t(productPlural)}
                resourceText={t=>l(t.name)}
                renderResource={props.renderResource || ((t:Product) => <ProductLabel product={t} /> )}
                resourceId={props.resourceId || ((r:Product) => r.id) }
                renderResourceOption={props.renderResourceOption || ((r:Product) => <ProductLabel noLink product={r} />)} 
                resourceRange={props.resourceRange || products.all() } 
                resourceType={productSubmissionType}

                actions={Object.values(productSubmissionActions)} />
                
}