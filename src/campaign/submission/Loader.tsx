import { Placeholder } from '#app/components/Placeholder'
import { useT } from '#app/intl/api'
import { useFeedback } from '#app/utils/feedback'
import { useAsyncRender } from '#app/utils/hooks'
import * as React from 'react'
import { Campaign } from '../model'
import { useSubmissionClient } from './client'
import { Submission, SubmissionStub, SubmissionWithTrail, Trail } from './model'


//  takes a submission, loads its data if it's not brand new, and pushes it down.

type LoaderProps = {

    trail: Trail
    submission: SubmissionStub
    campaign: Campaign

    // render props
    children: (_: Submission, related: SubmissionWithTrail[], onReload: () =>void) => JSX.Element

}

export type ChangeGuardProps = React.PropsWithChildren<{

    when: boolean
    onReload: () => void

}>

export const ChangeGuard = (props: ChangeGuardProps) => {

    const { when, onReload, children } = props

    const t = useT()

    const { askConsent } = useFeedback()

    React.useEffect(() => {

        if (when)
            askConsent({


                title: t('common.consent.change_detection_title'),
                content: t('common.consent.change_detection_content'),
                okText: t('common.consent.change_detection_ok'),

                onOk: onReload

            })

    })


    return <>{children}</>
}



export const SubmissionLoader = (props: LoaderProps) => {
    

    const { trail, submission, campaign, children } = props

    const isNew = submission.lifecycle?.created === undefined

    // the latest version of the incoming submission.
    const [loaded, loadedSet] = React.useState<Submission|undefined>(undefined)

    //  all the other submissions that may be needed to work with the submissions.
    //  e.g. the requirement submissons that may be referred to in a report.
    const [related, relatedSet] = React.useState<SubmissionWithTrail[]>(undefined!)

    const subclient = useSubmissionClient().on(campaign)

    // we load related submissions before we load the target submission.
    React.useEffect(()=>{

        if (!related)
        subclient.fetchSubmissionsRelatedTo(submission, trail).then(relatedSet)
    },
    
    //eslint-disable-next-line
    [related])

    const reload = () => loadedSet(undefined)
   
    // load target submission, the render with it and the related submissions
    const [render] = useAsyncRender({

        // if we have related submissions, render brand new submissions or freshly updated one.
        when: !!related && (isNew || !!loaded ),

        // but don't try to fetch submission until related have been fetched
        runWhen: !!related,

        content: () => children(isNew ? submission as Submission : loaded!, related ?? [], reload)
           
        ,    // we know it's loaded by now.

        // else fetch non-new submission.
        task: () => subclient.fetchOneSubmission(submission.id).then(loadedSet),

        placeholder: Placeholder.page

    })

    return render

}