
import { useT } from '#app/intl/api';
import { Tag, noTag } from '#app/tag/model';
import { useTagStore } from '#app/tag/store';
import { Tenant } from '#app/tenant/model';
import { useTenantStore } from '#app/tenant/store';
import { index, indexMap } from "#app/utils/common";
import { CampaignContext } from '#campaign/context';
import { useCampaignStore } from '#campaign/store';
import { Component, isContainer } from '#layout/components/model';
import { UploadTimestamp, submissionFileDropId } from '#layout/components/submissionfiledrop';
import { prodImportanceCategory, productType } from '#product/constants';
import { Product } from '#product/model';
import { useProductStore } from '#product/store';
import { reqImportanceCategory, requirementType } from '#requirement/constants';
import { Requirement } from '#requirement/model';
import { useRequirementStore } from '#requirement/store';
import { useContext } from 'react';
import { AssetInstance, Campaign, CampaignInstance } from "../model";
import { PartyInstance, usePartyInstances } from "../party/api";
import { ComplianceProfile, ComplianceScale, Submission, SubmissionStub, TimelinessProfile, TimelinessScale, Trail, TrailDto, TrailKey, managedStates, useSubmissionScales } from "./model";
import { useComplianceScale, useSubmissionProfile, useTimelinessScale } from './profile';
import { useRevisions } from './revision';


export type SubmissionState = {

    submissions: {

        all: Trail[]
        mapByKey: Record<string,Trail>
        compliance: ComplianceCache
        timeliness: TimelinessCache
        map: Record<string, Trail>

    }

}

export type ComplianceCache = {

    scale: ComplianceScale | undefined
}
    & Record<string, {
        weights: Tag[]
    }>

export type TimelinessCache = {

    scale: TimelinessScale | undefined
}


export const initialSubmissions = () => ({

    all: undefined!,
    compliance: {
        [requirementType]: {
            weights: []

        },
        [productType]: {
            weights: []

        }
    }
})

const keyToString = (key: TrailKey) => `${key.asset}${key.party}`


export const useTrailLookups = () => {

    const state = useContext(CampaignContext)

    return {
        on: (c: Campaign) => {

            const self = {

                allTrails: () => state.get().campaigns.instances[c.id]?.submissions?.all

                ,


                areTrailsReady: () => !!self.allTrails()

                ,

                lookupTrailKey: (key: TrailKey | undefined) => {

                    return key ? state.get().campaigns.instances[c.id]?.submissions?.mapByKey[keyToString(key)] : undefined
                }

                ,

                lookupTrail: (id: string) => id ? state.get().campaigns.instances[c.id]?.submissions?.map[id] : undefined

                ,

                allTrailsForParty: (party: PartyInstance) => self.allTrails().filter(t => t.key.party === party.source)

                ,

                allTrailsForAsset: (asset: CampaignInstance) => self.allTrails().filter(t => t.key.asset === asset.source)



            }

            return self
        }
    }
}




export const useSubmissionStore = () => {

    const t = useT()

    const state = useContext(CampaignContext)

    const tags = useTagStore()

    const lookups = useTrailLookups()

    const partyinststore = usePartyInstances()
    const subprofile = useSubmissionProfile()
    const subscales = useSubmissionScales()

    const complianceScale = useComplianceScale()
    const timelinessScale = useTimelinessScale()

    return {

        on: (c: Campaign) => {


            const self = {

                ...lookups.on(c)

                ,


                complianceCache: () => state.get().campaigns.instances[c.id]?.submissions?.compliance

                ,

                timelinessCache: () => state.get().campaigns.instances[c.id]?.submissions?.timeliness

                ,

                livePush: (trails: Trail[], type: 'change' | 'remove') => {


                    let current = state.get().campaigns.instances[c.id]?.submissions?.all ?? []

                    trails.forEach(trail => {

                        switch (type) {

                            case "change":

                                console.log("received unexpected change event on trails: this should have a different per-submission channel")
                                break;

                            case "remove": current = current.filter(i => i.id !== trail.id); break

                        }

                    })

                    self.setAllTrails(current)

                }


                ,

                setAllTrails: (trails: Trail[], props?: { noOverwrite: boolean }) => state.set(s => {

                    const campaign = s.campaigns.instances[c.id] ?? {}
                    const submissions = campaign.submissions ?? initialSubmissions()

                    if (submissions.all && props?.noOverwrite)
                        return

                    submissions.all = trails
                    submissions.mapByKey = indexMap(trails).by(t=> keyToString(t.key))

                    submissions.map = indexMap(trails).by(i => i.id)

                    const scales = subscales.on(c)

                    submissions.compliance.scale = scales.complianceScale()
                    submissions.compliance[requirementType].weights = tags.allTagsOfCategory(reqImportanceCategory).filter(tag => tag.lifecycle.state === 'active')
                    submissions.compliance[productType].weights = tags.allTagsOfCategory(prodImportanceCategory).filter(tag => tag.lifecycle.state === 'active')
                    submissions.timeliness = { scale: scales.timelinessScale() }


                    s.campaigns.instances[c.id] = { ...campaign, submissions }
                })


                ,

                replaceTrail: (trail: TrailDto) => self.setAllTrails(

                    (self.allTrails() ?? []).some(t => t.id === trail.id) ?
                        self.allTrails().map(t => t.id === trail.id ? trail : t)
                        : [...self.allTrails(), trail]
                )

                ,


                allActiveTrails: () => {
                    const currentParties = partyinststore.on(c).all().map(p => p.source)
                    return self.allTrails().filter(t => currentParties.some(p => p === t.key.party))
                }

                ,


                allTrailsByAsset: (type: string): { [_: string]: Trail[] } => index(self.allActiveTrails().filter(t => t.key.assetType === type)).by(t => t.key.asset)

                ,

                allTrailsByParty: (): { [_: string]: Trail[] } => index(self.allTrails()).by(t => t.key.party)


                ,

                // updates the timestamp on all uploads.
                timestamp: (submission: Submission, trail: Trail) => {

                    const now = Date.now()

                    const flatten = (c: Component): Component[] => isContainer(c) ? c.children.flatMap(flatten) : [c]

                    const layout = subprofile.on(c).profileOf(trail.key.assetType).layoutOf(trail.key.asset)

                    // upload components.
                    const uploads = flatten(layout.components).filter(c => c.spec === submissionFileDropId)

                    // timestamps uploads.
                    uploads.flatMap(c => (submission.content.data[c.id] ?? []) as UploadTimestamp[])
                        .filter(ts => ts.submission === submission.id)
                        .forEach(ts => {
                            ts.timestamp = now.valueOf()
                        })

                    return submission;

                }

                ,


                complianceProfile: (submission: SubmissionStub, instance?: AssetInstance, complianceCache?: ComplianceCache): ComplianceProfile | undefined => {

                    const cache = complianceCache ?? self.complianceCache()

                    const asset = instance ?? (() => {

                        const trail = self.allTrails().find(t => t.id === submission.trail)
                        return subprofile.on(c).profileOf(trail?.key.assetType).instance(trail?.key.asset!)

                    })()

                    if (!asset)
                        return undefined

                    const scale = cache.scale

                    if (!scale)
                        return undefined

                    const { tags, min, max, names } = scale

                    const weights = cache[asset.instanceType].weights

                    if (!submission.lifecycle.compliance)
                        return undefined

                    const state = submission.lifecycle.compliance.state
                    const complianceObservation = submission.lifecycle.compliance.officialObservation

                    if (!state)
                        return undefined

                    const tag = tags.find(t => t.id === state)

                    if (!tag)
                        return undefined

                    const value = complianceScale.for(tag)

                    let rate, complianceClass

                    if (!isNaN(value)) {

                        const normalized = value - min
                        rate = normalized * 100 / (max - min)
                        const group = Math.floor(rate / 33.1 + 1)
                        complianceClass = `compliance-${group}`
                    }

                    const weight = complianceScale.for(weights.find(tag => asset.tags.includes(tag.id))) || 1

                    const profile = { allNames: names, tag, name: tag.name, code: tag.code, weight, rate, complianceClass, complianceObservation }

                    return profile
                }

                ,


                timelinessProfile: (submission: SubmissionStub, computedTimeliness?: 'ontime' | 'late' | 'notdue', instance?: AssetInstance, timelinessCache?: TimelinessCache): TimelinessProfile | undefined => {

                    const noTimeliness = { ...noTag('no-timeliness'), name: { en: t('campaign.fields.timeliness_scale.no_scale'), fr: t('campaign.fields.timeliness_scale.no_scale') } }

                    const cache = timelinessCache ?? self.timelinessCache()

                    const asset = instance ?? (() => {

                        const trail = self.allTrails().find(t => t.id === submission.trail)
                        return subprofile.on(c).profileOf(trail?.key.assetType).instance(trail?.key.asset!)

                    })()

                    if (!asset)
                        return undefined

                    const scale = cache.scale

                    if (!scale)
                        return undefined

                    const { tags, names } = scale

                    const state = submission.lifecycle.compliance?.timeliness ?? subscales.on(c).defaultTimeliness(computedTimeliness, cache.scale?.tags)?.id

                    const tag = state ? tags.find(t => t.id === state) ?? noTimeliness : noTimeliness

                    const value = timelinessScale.for(tag) ?? 0

                    const timelinessClass = value === undefined || !isNaN(value) ? 'timeliness-plain' : value < 0 ? 'timeliness-late' : 'timeliness-ontime'

                    const profile = { allNames: names, tag, name: tag.name, code: tag.code, timelinessClass }

                    return profile
                }

                ,


                isManagedState: (submission: SubmissionStub) => submission?.lifecycle.state && managedStates.includes(submission?.lifecycle.state)

                ,


              
                // optionally defaults to party instance setting.
                hasApproveCycle: (trail: Trail, delegation: 'delegate' | 'nodelegation' = 'delegate') => {

                    const setting = trail.properties?.approveCycle

                    if (setting!==undefined)
                        return setting

                    if (delegation !== 'delegate')
                            return undefined

                    const defaultsetting = partyinststore.on(c).approveCycleFor(trail.key.party)

                    return defaultsetting === 'all' || defaultsetting === trail.key.assetType


                }
            
                ,


                // defaults to party instance setting.
                approveCycleDefault: (trail: Trail) => partyinststore.on(c).approveCycleFor(trail.key.party)!


                ,

                setApproveCycle: (trail: Trail, value: boolean | undefined) => {

                    console.log({value})

                    if (value === undefined)
                        delete trail.properties.approveCycle
                    else trail.properties.approveCycle = value

                }

                ,

                // optionally defaults to party instance setting.
                canBypassSelfApproval: (trail: Trail, delegation: 'delegate' | 'nodelegation' = 'delegate') => trail.properties?.bypassSelfApproval ?? (delegation === 'delegate' ? self.canBypassSelfApprovalDefault(trail) : undefined)


                ,

                // defaults to party instance setting.
                canBypassSelfApprovalDefault: (trail: Trail) => partyinststore.on(c).canBypassSelfApprovalFor(trail.key.party)


                ,

                setCanBypassSelfApproval: (trail: Trail, value: boolean | undefined) => {

                    if (value === undefined)
                        delete trail.properties.bypassSelfApproval
                    else trail.properties.bypassSelfApproval = value

                }

                ,

                canSubmitAfterNA: (trail: Trail) => trail.properties?.submitAfterNA ?? false

                ,

                setCanSubmitAfterNA: (trail: Trail, value: boolean | undefined) => {

                    if (value === undefined)
                        delete trail.properties.submitAfterNA
                    else trail.properties.submitAfterNA = value

                }

                // ,

                // // most recent last.
                // dateComparator: (o1: SubmissionStub, o2: SubmissionStub) => {

                //     const isMissing = (o1: SubmissionStub, o2: SubmissionStub) => o1.lifecycle.state === 'missing' || o2.lifecycle.state === 'missing'

                //     const comparison = isMissing(o1, o2) ? 0 : compareDates(o1.lifecycle.lastSubmitted, o2.lifecycle.lastSubmitted, false)

                //     return comparison === 0 ? self.stateComparator(o1, o2) : comparison

                // }


                // ,

                // // compare by state in workflow order, missing state always down, and by latest 
                // stateComparator: (o1: SubmissionStub, o2: SubmissionStub) => {

                //     const comparison = o1.lifecycle.state === 'missing' ? -1 : o2.lifecycle.state === 'missing' ? 1 : (o1.lifecycle.state ? submissionStates.indexOf(o1.lifecycle.state!) : 0) - (o2.lifecycle.state ? submissionStates.indexOf(o2.lifecycle.state) : 0)

                //     return comparison === 0 ? compareDates(o1.lifecycle.lastModified, o2.lifecycle.lastModified) : comparison
                // }

                // ,

                // // most recent first, including drafts.
                // dateComparatorDesc: (o1: SubmissionStub, o2: SubmissionStub) => self.dateComparator(o2, o1)


            }

            return self;
        }
    }
}

export type LineageData = {

    campaign: Campaign | undefined,
    party: Tenant | undefined,
    assets: {

        asset: Product | Requirement
        trail?: Trail
        submission?: SubmissionStub

    }[]

}

export const useSubmissionLineage = () => {


    const cmpstore = useCampaignStore()
    const substore = useSubmissionStore()
    const tenantstore = useTenantStore()

    const reqstore = useRequirementStore()
    const prodstore = useProductStore()

    const revisions = useRevisions()

    return {

        on: (c: Campaign) => {


            const self = {


                given: (source: Partial<{

                    asset: string | Requirement | Product
                    party: string | Tenant
                    trail: string | Trail


                }>, useFallback?: 'useFallback'): LineageData => {

                    const fallback = {

                        asset: undefined!,
                        trail: { id: 'notrail', key: undefined!, submissions: [], properties: {} } as Trail,
                        submission: { id: 'nosub', trail: 'notrail', lifecycle: { state: 'draft', tags: [] as string[] } } as SubmissionStub

                    }

                    const lineage = {

                        campaign: undefined! as Campaign | undefined,
                        party: undefined! as Tenant | undefined,
                        assets: (useFallback ? [fallback] : []) as {

                            asset: Product | Requirement
                            trail?: Trail
                            submission?: SubmissionStub

                        }[]
                    }


                    const store = substore.on(c)

                    let sourcetrail = typeof source.trail === 'string' ? store.lookupTrail(source.trail) : source.trail

                    lineage.party = (!source.party || typeof source.party === 'string') ?

                        tenantstore.lookup(source.party ?? sourcetrail?.key.party)

                        : source.party


                    if (!lineage.party)
                        return lineage

                    // now we have a party.

                    if (!c.lineage)
                        return lineage

                    lineage.campaign = cmpstore.lookup(c.lineage)

                    if (!lineage.campaign)
                        return lineage

                    // now we have campaign.


                    if (!sourcetrail) {

                        if (!source.asset)
                            return lineage

                        // try to compose from other data
                        const sourcetrailkey = {
                            campaign: lineage.campaign.id,
                            asset: typeof source.asset === 'string' ? source.asset : source.asset.id,
                            party: lineage.party?.id
                        } as TrailKey

                        sourcetrail = store.lookupTrailKey(sourcetrailkey)

                    }

                    if (!sourcetrail)
                        return lineage

                    // now we have the source trail.


                    const assetstore = sourcetrail.key.assetType === requirementType ? reqstore : prodstore

                    const sourceasset = (!source.asset || typeof source.asset === 'string') ? assetstore.lookup(source.asset ?? sourcetrail.key.asset) : source.asset

                    if (!sourceasset)
                        return lineage


                    const hasLineage = !!sourceasset.lineage?.length

                    // fallback to same asset in case it's been re-used from old campaign.
                    const sourcelineage = sourceasset.lineage?.length ? sourceasset.lineage : [sourceasset.id]

                    // now we have the source asset.

                    lineage.assets = sourcelineage.map(lineageId => {

                        const trailkey = { ...sourcetrail?.key, campaign: lineage.campaign!.id, asset: lineageId } as TrailKey

                        const trail = substore.on(lineage.campaign!).lookupTrailKey(trailkey)

                        if (!trail && !hasLineage)
                            return lineage.assets[0]

                        const asset = assetstore.lookup(lineageId)!

                        if (lineage.assets[0]) {
                            lineage.assets[0].asset = asset
                            if (lineage.assets[0].trail)
                                lineage.assets[0].trail.key = trailkey
                        }

                        const trailOrDefault = trail ?? lineage.assets[0]?.trail

                        const submission = revisions.on(trailOrDefault).official() ?? lineage.assets[0]?.submission

                        return { asset, trail: trailOrDefault, submission: submission }

                    }) ?? []


                    if (!lineage.assets[0])
                        lineage.assets = []

                    return lineage


                }

            }

            return self
        }
    }
}
