
import { Button } from '#app/components/Button'
import { DrawerProps, RoutableDrawerProps, useRoutableDrawer } from '#app/components/Drawer'
import { RouteGuard } from '#app/components/RouteGuard'
import { Paragraph } from '#app/components/Typography'
import { Form } from '#app/form/Form'
import { useFormState } from '#app/form/hooks'
import { useT } from '#app/intl/api'
import { useTimePicker } from '#app/time/TimePickerBox'
import { eventIcon } from '#event/constants'
import moment from 'moment-timezone'
import * as React from 'react'
import { dateChangeIcon } from './constants'
import { useSubmissionContext } from './hooks'
import { SubmissionStub } from './model'
import { useRevisions } from './revision'



export type Props = {

    submission: SubmissionStub
    onChange: (_: string) => Promise<void>

}


export const useDateChangeDrawer = (opts: Partial<RoutableDrawerProps> = {}) => {

    const t = useT()

    const { id = 'datechange', title = t("submission.datechange.title"), icon = eventIcon } = opts

    const { Drawer, open, close, visible, route, param } = useRoutableDrawer({ id, title, icon })

    const DrawerProxy = (props: Partial<DrawerProps>) => <Drawer width={500} icon={icon} title={title} {...props} />

    //eslint-disable-next-line
    const StableProxy = React.useCallback(DrawerProxy, [visible])


    return { DateChangeDrawer: StableProxy, openDateChange: open, datechangeRoute: route, closeDataChange: close, datechangeParam: param }

}


export const DateChangeForm = (props: Props) => {

    const t = useT()
    const { submission, onChange } = props

    const initialDate = submission.lifecycle.lastSubmitted

    const { edited, dirty, change, softReset } = useFormState({ date: initialDate })

    const changeDate = () => {

        onChange(edited.date!).then(() => softReset({ date: initialDate }))
    }

    const { timepicker, linearHistory } = useDateChangePicker({

        submission,
        date: edited.date,
        onChange: change((m, v) => m.date = v)

    })

    return <div className="submission-dialog-panel">

        <Paragraph className="submission-dialog-intro">{t("submission.datechange.introduction")}</Paragraph>

        <div className="submission-dialog-section" style={{ background: 'inherit' }}>

            {React.cloneElement(dateChangeIcon, { className: 'submission-dialog-icon' })}

            <Paragraph className='submission-dialog-explainer'>{t("submission.datechange.explainer")}</Paragraph>

            <Form style={{ marginTop: 30 }}>
                {timepicker}
            </Form>

            <Button style={{ marginTop: 30 }} enabled={dirty && linearHistory} type={'primary'} onClick={changeDate} >
                {t("submission.datechange.confirm_btn")}
            </Button>
        </div>

        <RouteGuard when={dirty} />

    </div>
}


type DateChangePickerProps = {

    submission: SubmissionStub
    onChange: (_: string) => void
    date: string | undefined
    enabledOnReadOnly?: boolean
}

export const useDateChangePicker = (props: DateChangePickerProps) => {

    const t = useT()

    const { submission, date, enabledOnReadOnly=false, onChange } = props

    const { Picker: TimePicker } = useTimePicker()

    const {trail} = useSubmissionContext()

    const revisions = useRevisions()

    const groups = revisions.on(trail).submittedGroups()
    const groupIndex = groups.findIndex(g => g.some(s => s.id === submission.id ))
    const previousDate = groups[groupIndex +1]?.[0].lifecycle.lastSubmitted

    const linearHistory = !previousDate || moment(date).isAfter(moment(previousDate))

    const now = moment.now()

    const timepicker = <>

        <TimePicker label={t('submission.datechange.label')} style={{ minWidth: 250 }} showToday allowClear={false}
            onChange={date => onChange(date.format())}
            disabledDate={date => (date?.isAfter(now) ?? true) || (!!previousDate && (date?.isBefore(moment(previousDate).subtract(1,'days')) ?? false)) }
            enabledOnReadOnly={enabledOnReadOnly}
            validation={linearHistory ? undefined : {status:'error', msg: t('submission.datechange.linear_warning',{date:moment(previousDate).format('lll')})} }
            showTime
            value={date} />

    </>

    return { timepicker, linearHistory }
}