import { useCalls } from "#app/call/call";
import { Multilang } from "#app/model/multilang";
import { domainService } from "#constants";
import { Campaign } from "../model";
import { Submission, SubmissionStub, Trail, TrailKey } from "./model";


const trails = `/trail`
const submissions = `/submission`


export const useSubmissionCalls = () => {

    const { at } = useCalls()

    return {
        on: (c: Campaign) => {


            const self = {



                fetchAllTrails: (): Promise<Trail[]> => at(`${trails}/search`, domainService).post({ campaign: c.id })

                ,

                fetchAllPublished: (): Promise<{ trails: Trail[], tenants: { id: string, name: Multilang }[] }> => at(`${trails}/published`, domainService).post({ campaign: c.id })

                ,

                fetchOneTrail: (id: string): Promise<Trail> => at(`${trails}/${id}`, domainService).get()

                ,

                saveOneTrail: (trail: Trail): Promise<Trail> => at(`${trails}/${trail.id}`, domainService).put(trail)

                ,

                // adds a submission to a new or existing trail, and return both.
                // if the trail is new, the backend creates it along with its first submission.
                addSubmission: (submission: Submission, key: TrailKey): Promise<[Trail, Submission]> => at(`${trails}`, domainService).post<{ trail: Trail, submission: Submission }>({ submission, key }).then(({ trail, submission }) => [trail, submission])


                ,


                fetchOneSubmission: (id: string): Promise<Submission> => at(`${submissions}/${id}`, domainService).get()

                ,


                fetchManySubmissions: (subs: SubmissionStub[]): Promise<Submission[]> => at(`${submissions}/search`, domainService).post({ submissions: subs.map(s => s.id) })


                ,

                updateSubmission: (submission: Submission): Promise<Submission> => at(`${submissions}/${submission.id}`, domainService).put(submission)

                ,

                shallowUpdateSubmissions: (subs: SubmissionStub[]): Promise<Submission[]> => at(`${submissions}`, domainService).put(subs)

                ,

                removeSubmission: (id: string) => at(`${submissions}/${id}`, domainService).delete()



            }

            return self;
        }
    }
}
