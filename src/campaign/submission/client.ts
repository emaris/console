
import { useT } from '#app/intl/api';
import { Language } from "#app/intl/model";
import { useMail } from "#app/mail/api";
import { TemplatedMailContent } from '#app/mail/model';
import { Multilang, MultilangDto } from '#app/model/multilang';
import { useBytestreams } from "#app/stream/api";
import { Bytestream, bytestreamForBlob } from '#app/stream/model';
import { useToggleBusy } from "#app/system/api";
import { noTenant } from '#app/tenant/constants';
import { useTenantStore } from '#app/tenant/store';
import { useTime } from '#app/time/api';
import { useLogged } from '#app/user/store';
import { clock, through } from "#app/utils/common";
import { notify, showAndThrow, showError, useFeedback } from "#app/utils/feedback";
import { AssetSection, assetSectionid, sectionAssetOf } from "#layout/components/assetsection";
import { useReferencedSubmissionsLoader } from '#layout/components/fieldref';
import { Component, isContainer } from '#layout/components/model';
import { RequirementSection, requirementSectionId } from '#layout/components/requirementsection';
import { Message, MessageContent } from '#messages/model';
import { useMessageStore } from '#messages/store';
import { productType } from '#product/constants';
import { Product } from '#product/model';
import { useProductStore } from '#product/store';
import { requirementType } from '#requirement/constants';
import { Requirement } from '#requirement/model';
import { useRequirementStore } from '#requirement/store';
import { utils } from 'apprise-frontend-core/utils/common';
import moment from 'moment';
import { subAssessedMailTopic, subAssessementRevokedMailTopic, subPendingApprovalMailTopic, subRequestForChangeMailTopic, subSubmittedMailTopic, subchangeMailTemplate } from '../constants';
import { AssetInstance, Campaign, useCampaignModel } from "../model";
import { useSubmissionCalls } from "./calls";
import { submissionPlural, submissionSingular } from './constants';
import { ComplianceProfile, ManagementData, Submission, SubmissionDto, SubmissionStub, SubmissionWithTrail, TimelinessProfile, Trail, TrailDto, nextRevision, useSubmissionScales, useSubmissionTopics } from "./model";
import { useContentPostProcess } from './postprocess';
import { useComplianceScale, useSubmissionProfile, useTimelinessScale } from './profile';
import { useRevisions } from './revision';
import { ComplianceCache, TimelinessCache, useSubmissionStore, useTrailLookups } from './store';


export const useSubmissionClient = () => {

    const store = useSubmissionStore()

    const t = useT()

    const toggleBusy = useToggleBusy()

    const time = useTime()

    const logged = useLogged()

    const mail = useMail()

    const lookups = useTrailLookups()

    const tenants = useTenantStore()

    const requirements = useRequirementStore()

    const products = useProductStore()

    const subprofile = useSubmissionProfile()

    const subcalls = useSubmissionCalls()
    const subscales = useSubmissionScales()
    const subtopics = useSubmissionTopics()

    const campaignmodel = useCampaignModel()

    const messages = useMessageStore()

    const fb = useFeedback()

    const bs = useBytestreams()

    const [singular, plural] = [t(submissionSingular), t(submissionPlural)].map(n => n.toLowerCase())

    const complianceScale = useComplianceScale()
    const timelinessScale = useTimelinessScale()

    const revisions = useRevisions()

    const subreferences = useReferencedSubmissionsLoader()

    const postprocess = useContentPostProcess()

    const api = {

        on: (c: Campaign) => {

            const cstore = store.on(c)


            const mailtemplate = (submission: Submission, trail: Trail): TemplatedMailContent => {

                const compliance = self.complianceProfile(submission)

                const tenant = tenants.safeLookup(trail.key.party)

                let asset: Requirement | Product = undefined!
                let assetTitle: MultilangDto = undefined!

                if (trail.key.assetType === requirementType)

                    asset = requirements.safeLookup(trail.key.asset)

                else
                    asset = products.safeLookup(trail.key.asset)


                assetTitle = asset.description

                const parameters = {
                    tenant: tenant.name as Multilang,
                    tenantId: tenant.id,
                    campaign: c.name as Multilang,
                    campaignId: c.id,
                    asset: asset.name as Multilang,
                    assetTitle,
                    assetId: asset.id,
                    assetType: trail.key.assetType,
                    status: submission.lifecycle.state as string,
                    assessment: compliance?.name as Multilang,
                    observation: compliance?.complianceObservation as Multilang
                }

                return { template: subchangeMailTemplate, parameters }
            }

            const self = {

                ...lookups.on(c)

                ,


                fetchAllTrails: (forceRefresh = false) =>

                    self.areTrailsReady() && !forceRefresh ? Promise.resolve(self.allTrails())

                        :

                        toggleBusy(`trails.fetchAll`, t("common.feedback.load", { plural }))

                            .then(_ => console.log(`fetching trails...`))
                            .then(_ => subcalls.on(c).fetchAllTrails())
                            .then(through($ => cstore.setAllTrails($, { noOverwrite: !forceRefresh })))


                            .catch(e => showAndThrow(e, t("common.calls.fetch_all_error", { plural })))
                            .finally(() => toggleBusy(`trails.fetchAll`))



                ,


                revokeAssessment: async (submission: Submission, trail: Trail) => {

                    const saved = await self.save(submission, trail)

                    const changeTemplate = mailtemplate(saved, trail)

                    const messageContent = { keys: ["submission.message.revoked"], parameters: {} }

                    self.postMessage(trail, submission, messageContent)

                    changeTemplate.parameters.status = 'revokedassessment'

                    self.scheduleMail(() => mail.sendTo([noTenant, trail.key.party]).content(changeTemplate).onTopic(subAssessementRevokedMailTopic))

                    return saved

                }

                ,

                fetchOneSubmission: (id: string): Promise<Submission> =>

                    toggleBusy(`submissions.fetchOne`, t("common.feedback.load", { singular }))

                        //.then( _ =>console.log(`fetching submission ${id}...`))
                        .then(_ => subcalls.on(c).fetchOneSubmission(id))


                        .catch(e => showAndThrow(e, t("common.calls.fetch_one_error", { singular })))
                        .finally(() => toggleBusy(`submissions.fetchOne`))

                ,

                fetchSubmissionsRelatedTo: (submission: SubmissionStub, trail: Trail): Promise<SubmissionWithTrail[]> =>

                    clock("fetching submissions related to submission of " + trail.key.asset, () =>

                        toggleBusy(`submissions.fetchRelated`, t("dashboard.labels.load_related"))

                            .then(_ => trail.key.assetType === productType ?

                                self.fetchSubmissionsRelatedToProduct(submission, trail)
                                :

                                self.fetchSubmissionsRelatedToRequirement(submission, trail)
                            )

                            .catch(e => {
                                // show the error, but allow progress with unresolvable references.
                                showError(e, t("dashboard.labels.load_related_error", { plural }));
                                return []
                            })
                            .finally(() => toggleBusy(`submissions.fetchRelated`))

                    )

                ,

                // returns the full requirement submissions whose data may be referenced from within this product.
                fetchSubmissionsRelatedToRequirement: async (submission: SubmissionStub, trail: Trail): Promise<SubmissionWithTrail[]> => {

                    return subreferences.given(submission, trail)

                }
                ,

                // returns the full requirement submissions whose data may be referenced from within this product.
                fetchSubmissionsRelatedToProduct: (submission: SubmissionStub, trail: Trail): Promise<SubmissionWithTrail[]> => {



                    // find all requirements referenced from this product.
                    const flattencontainers = (c: Component): Component[] => isContainer(c) ? [c, ...c.children.flatMap(flattencontainers)] : []
                    const product = products.safeLookup(trail.key.asset)
                    const flattenComponent = flattencontainers(product.properties.layout.components)
                    const referenceAssets = [
                        ...flattenComponent.filter(c => c.spec === requirementSectionId).flatMap(c => (c as RequirementSection).requirements),
                        ...flattenComponent.filter(c => c.spec === assetSectionid).flatMap(c => (c as AssetSection).assets)
                    ]


                    const temporallyCompatible = (s: SubmissionStub) => {

                        // we take published date, or submission date if it doesn't exist
                        const submissionCompatible = moment(s.lifecycle.lastSubmitted).isSameOrBefore(submission.lifecycle.lastSubmitted)

                        const changeDate = submission.lifecycle.state === 'managed' ? undefined : submission.lifecycle.state === 'published' ? submission.lifecycle.lastPublished : submission.lifecycle.lastSubmitted ?? undefined

                        const changeCompatible = moment(s.lifecycle.lastPublished ?? s.lifecycle.lastSubmitted).isSameOrBefore(changeDate)

                        return submissionCompatible && changeCompatible
                    }

                    // identify related submissions.
                    const related = self.allTrails()

                        .filter(t => referenceAssets.find(r => sectionAssetOf(r) === t.key.asset && t.key.party === trail.key.party))
                        .map(t => revisions.on(t).allOfficial().find(temporallyCompatible)!)
                        .filter(s => s !== undefined)

                    return Promise.all([

                        subreferences.given(submission, trail),
                        related.length > 0 ? subcalls.on(c).fetchManySubmissions(related).then(submissions => submissions.map(s => ({ submission: s, trail: self.lookupTrail(s.trail)! }))) : Promise.resolve([])

                    ]).then(([a, b]) => [...a, ...b])

                }
                ,

                assess: async (submission: Submission, trail: Trail) => {

                    const saved = await self.save(submission, trail)

                    if (submission.lifecycle.state === 'submitted' || submission.lifecycle.state === 'published')
                        self.notifyAssessment(saved, trail)

                    return saved
                }

                ,

                notifyAssessment: (submission: Submission, trail: Trail) => {

                    const changeTemplate = mailtemplate(submission, trail)

                    changeTemplate.parameters.status = 'assessed'

                    self.scheduleMail(() => mail.sendTo([noTenant, trail.key.party]).content(changeTemplate).onTopic(subAssessedMailTopic))

                    const compliance = self.complianceProfile(submission)?.name!

                    const messageContent = { keys: ["submission.message.assessed"], parameters: { compliance } }

                    self.postMessage(trail, submission, messageContent)

                }


                ,

                postMessage: (trail: Trail, submission: Submission, content: MessageContent) => {


                    const { topics, flow } = subtopics.on(c).topicsAndFlow(trail, submission)

                    const message = { scope: c.id, topics, recipient: flow.recipient, readBy: [noTenant, trail.key.party], content } as Message

                    if (campaignmodel.isMuted(c)) return message

                    return messages.postOne(message)

                }

                ,


                scheduleMail: (sender: () => void) => {
                    if (!campaignmodel.isMuted(c)) sender()
                }

                ,

                save: (submission: Submission, trail: Trail) =>

                    submission.lifecycle?.created ? self.update(submission) : self.add(submission, trail)


                ,


                changeDate: async (submission: Submission, date: string, trail: Trail) => {

                    // submitted group must be redated at once on lastSubmitted.
                    // but lastModify must preserve sequence.

                    const group = revisions.on(trail).submittedGroupOf(submission)?.reverse().map((s, i) => ({

                        ...s,

                        lifecycle: {

                            ...s.lifecycle,

                            lastModified: Date.now() + i,       // preserves sequence.
                            lastModifiedBy: logged.username,    // client fully in charge for shallow update.
                            lastSubmitted: date

                        }
                    }))

                    // sanity check
                    if (!group?.length) {
                        console.error("can't change submission date, not in a group", submission)
                        return
                    }

                    toggleBusy(`submissions.save`, t("common.feedback.save_changes"))

                    try {

                        const savedSubmissions = await subcalls.on(c).shallowUpdateSubmissions(group)

                        // replace submission in trail already in state.
                        await cstore.replaceTrail({ ...trail, submissions: trail.submissions.map(s => savedSubmissions.find(ss => ss.id === s.id) ?? s) })

                        notify(t('common.feedback.saved'))

                        // notify only the change to a single submission.
                        const savedSubmission = savedSubmissions.find(s => s.id === submission.id)!

                        const parameters: Record<string, MultilangDto> = { date: time.formatAsMultilang(savedSubmission.lifecycle.lastSubmitted, 'short') }

                        const messageContent = { keys: [`submission.message.datechange`], parameters }

                        self.postMessage(trail, savedSubmission, messageContent)

                        return savedSubmission
                    }
                    catch (e: any) {

                        showAndThrow(e, t("common.calls.save_one_error", { singular }))

                    }
                    finally {
                        toggleBusy(`submissions.save`)
                    }

                }



                ,


                add: (submission: Submission, trail: Trail) => {

                    return toggleBusy(`submissions.save`, t("common.feedback.save_changes"))
                        .then(_ => subcalls.on(c).addSubmission(submission, trail.key))

                        // process the pair: intern the trail and return submission.
                        .then(([trail, submission]) => {

                            cstore.replaceTrail(trail)
                            return submission

                        })


                        .then(through(() => notify(t('common.feedback.saved'))))

                        .catch(e => showAndThrow(e, t("common.calls.save_one_error", { singular })))
                        .finally(() => toggleBusy(`submissions.save`))

                }

                ,

                update: (submission: Submission) => {

                    return toggleBusy(`submissions.save`, t("common.feedback.save_changes"))
                        .then(_ => subcalls.on(c).updateSubmission(submission)
                            .then(updated =>
                                subcalls.on(c).fetchOneTrail(submission.trail)
                                    .then(trail => [trail, updated] as [TrailDto, SubmissionDto])

                            ))

                        // process the pair: intern the trail and return submission.
                        .then(([trail, submission]) => {

                            cstore.replaceTrail(trail)
                            return submission

                        })


                        .then(through(() => notify(t('common.feedback.saved'))))

                        .catch(e => showAndThrow(e, t("common.calls.save_one_error", { singular })))
                        .finally(() => toggleBusy(`submissions.save`))

                }

                ,

                saveTrail: (trail: Trail) =>

                    toggleBusy(`trail.save`, t("common.feedback.save_changes"))

                        .then(() => subcalls.on(c).saveOneTrail(trail))
                        .then(through(cstore.replaceTrail))
                        .then(through(() => notify(t('common.feedback.saved'))))

                        .catch(e => showAndThrow(e, t("common.calls.save_one_error", { singular })))
                        .finally(() => toggleBusy(`trail.save`))

                ,

                remove: async (submission: SubmissionStub) => {


                    toggleBusy(`submissions.remove`, t("common.feedback.save_changes"))

                    try {

                        await subcalls.on(c).removeSubmission(submission.id)

                        const trail = await subcalls.on(c).fetchOneTrail(submission.trail)     // may now be empty.

                        cstore.replaceTrail(trail)

                    }
                    catch (e: any) {

                        showAndThrow(e, t("common.calls.remove_one_error", { singular }))
                    }
                    finally {
                        toggleBusy(`submissions.remove`)
                    }



                }
                ,


                submit: async (submission: Submission, trail: Trail) => {

                    let saved = submission

                    const substate = saved.lifecycle.state

                    if (substate === 'submitted')
                        saved.lifecycle.changeRequest = undefined!

                    if (substate === 'submitted')
                        saved = cstore.timestamp(saved, trail)

    

                    let asset = (trail.key.assetType === requirementType ? requirements : products).safeLookup(trail.key.asset)

                    postprocess(saved, asset.properties.layout)

                    const savedSubmission = await self.save(saved, trail)

                    const changeTemplate = mailtemplate(savedSubmission, trail)

                    const messageState = substate === 'submitted' && logged.hasNoTenant() ? 'managed_submitted' : substate
                    const parameters: Record<string, MultilangDto> = messageState === 'managed_submitted' ? { date: time.formatAsMultilang(submission.lifecycle.lastSubmitted, 'short') } : {}

                    const messageContent = { keys: [`submission.message.${messageState}`], parameters }

                    self.postMessage(trail, savedSubmission, messageContent)

                    const mailTopic = substate === 'pending' ? subPendingApprovalMailTopic : subSubmittedMailTopic
                    const targets = substate === 'pending' ? [trail.key.party] : [noTenant, trail.key.party]
                    self.scheduleMail(() => mail.sendTo(targets).content(changeTemplate).onTopic(mailTopic))

                    return savedSubmission

                }

                ,


                reject: async (submission: Submission, trail: Trail) => {

                    const saved = {

                        ...submission,
                        lifecycle: {
                            ...submission.lifecycle,
                            state: 'draft'
                        }

                    } as Submission

                    const savedSubmission = await self.save(saved, trail)



                    const request = submission.lifecycle.changeRequest

                    const messageContent = request ?

                        { keys: ["submission.message.rejected_request"], parameters: { request } }

                        :

                        { keys: ["submission.message.rejected"], parameters: {} as Record<string, string> }


                    self.postMessage(trail, saved, messageContent)

                    const changeTemplate = mailtemplate(savedSubmission, trail)

                    changeTemplate.parameters.status = 'requestforchange'

                    if (request)
                        changeTemplate.parameters.changerequest = request

                    self.scheduleMail(() => mail.sendTo(trail.key.party).content(changeTemplate).onTopic(subRequestForChangeMailTopic))

                    return savedSubmission


                },

                manage: (submission: Submission, trail: Trail, initialManagementData: ManagementData) => {

                    // new revision, same content.
                    const managed = nextRevision(submission)

                    managed.lifecycle = {

                        // carries over submission and compliance data.
                        ...submission.lifecycle,

                        firstEdited: undefined,

                        // carries over workflow data.
                        state: 'managed' as const,
                        previousState: submission.lifecycle.state,

                        // adds initial management data
                        ...initialManagementData,

                        // resets edit data.
                        created: undefined!,
                        lastModified: undefined!,
                        lastModifiedBy: undefined,

                        // resets  publication (if it exists).
                        lastPublished: undefined!,
                        lastPublishedBy: undefined!,
                        publication: undefined,


                    }

                    return self.add(managed, trail)

                }

                ,

                publish: async (submission: Submission, trail: Trail) => {

                    const now = time.current()

                    const published = {

                        ...submission,


                        content: {

                            ...submission.content,

                            changelog: undefined


                        }

                        ,

                        lifecycle: {

                            ...submission.lifecycle,

                            state: 'published' as const,
                            previousState: submission.lifecycle.state,
                            firstEdited: undefined,
                            lastPublished: now.format(),
                            lastPublishedBy: logged.username
                        },
                    }

                    const saved = await self.save(published, trail)

                    const previous = revisions.on(trail).previous(saved)

                    if (!utils().deepequals(saved.lifecycle.compliance, previous.lifecycle.compliance))
                        self.notifyAssessment(saved, trail)

                    return saved

                }

                ,

                share: async (submission: Submission, trail: Trail, blob: Blob, name: string, lang: Language) => {


                    const share = async () => {

                        const publication = submission.lifecycle.publication ?? {}
                        const publicationInLang = publication[lang] !== undefined ? publication[lang]!.trim() === "" ? undefined : publication[lang] : undefined
                        const bytestream = (publicationInLang ? { ...bytestreamForBlob(name, blob), id: publicationInLang, ref: submission.id } : { ...bytestreamForBlob(name, blob), ref: submission.id }) as Bytestream
                        const streams = await bs.upload([[{ id: bytestream.id, stream: bytestream }, blob]], !!publicationInLang)

                        if (streams.length > 0) {
                            const stream = streams[0]
                            const newSubmission: Submission = { ...submission, content: { ...submission.content, changelog: undefined }, lifecycle: { ...submission.lifecycle, firstEdited: undefined, publication: { ...publication, [lang]: stream.id } } }
                            self.save(newSubmission, trail)
                            return newSubmission
                        }

                        return submission

                    }

                    try {

                        await toggleBusy(`submissions.share`, t("submission.labels.share_wait"))

                        const shared = await share()

                        const changeTemplate = mailtemplate(shared, trail)

                        changeTemplate.parameters.status = 'shared'

                        const messageContent = { keys: ["submission.message.shared"], parameters: {} }

                        self.postMessage(trail, shared, messageContent)

                        self.scheduleMail(() => mail.sendTo([noTenant, trail.key.party]).content(changeTemplate).onTopic(subRequestForChangeMailTopic))

                        toggleBusy(`submissions.share`)

                        return shared

                    }
                    catch (e) {
                        return showAndThrow(e as Error, t("common.calls.share_error"))
                    }


                }

                ,

                deleteSharedSubmission: async (submission: SubmissionStub, trail: Trail, lang: Language, afterDeleteTrigger?: () => void) => {

                    fb.askConsent({
                        onOk: async () => {
                            const currentSubmission = await self.fetchOneSubmission(submission.id)

                            const bytestreamId = submission.lifecycle.publication?.[lang]

                            const deletePublicationForSubmission = (submission: Submission | SubmissionStub, lang: Language) => {
                                const publication = submission.lifecycle.publication
                                if (!publication) return undefined

                                if (publication[lang] != undefined) delete (publication[lang])

                                if (Object.keys(publication).length === 0) return undefined

                                return publication
                            }

                            const newSubmission = bytestreamId ? { ...currentSubmission, lifecycle: { ...currentSubmission.lifecycle, publication: deletePublicationForSubmission(currentSubmission, lang) } } : currentSubmission
                            const newSubmissionStub = bytestreamId ? { ...submission, lifecycle: { ...submission.lifecycle, publication: deletePublicationForSubmission(submission, lang) } } : submission

                            if (bytestreamId) {
                                await self.save(newSubmission, trail)
                                await self.saveTrail({ ...trail, submissions: trail.submissions.map(s => s.id === submission.id ? newSubmissionStub : s) })
                                if (afterDeleteTrigger) {
                                    await afterDeleteTrigger()
                                }
                            }
                        },
                        onCancel: () => null,
                        title: t('dashboard.shared_documents.columns.publications.remove'),
                        content: t('dashboard.shared_documents.columns.publications.remove_msg'),
                        okText: t('common.consent.remove_one_confirm', { singular: t('dashboard.shared_documents.columns.publications.file_singular') })

                    })



                }

                ,


                complianceProfile: (submission: SubmissionStub, instance?: AssetInstance, complianceCache?: ComplianceCache): ComplianceProfile | undefined => {

                    const cache = complianceCache ?? cstore.complianceCache()

                    const asset = instance ?? (() => {

                        const trail = self.allTrails().find(t => t.id === submission.trail)
                        return subprofile.on(c).profileOf(trail?.key.assetType).instance(trail?.key.asset!)

                    })()

                    if (!asset)
                        return undefined

                    const scale = cache.scale

                    if (!scale)
                        return undefined

                    const { tags, min, max, names } = scale

                    const weights = cache[asset.instanceType].weights

                    if (!submission.lifecycle.compliance)
                        return undefined

                    const state = submission.lifecycle.compliance.state
                    const complianceObservation = submission.lifecycle.compliance.officialObservation

                    if (!state)
                        return undefined

                    const tag = tags.find(t => t.id === state)

                    if (!tag)
                        return undefined

                    const value = complianceScale.for(tag)

                    let rate, complianceClass

                    if (!isNaN(value)) {

                        const normalized = value - min
                        rate = normalized * 100 / (max - min)
                        const group = Math.floor(rate / 33.1 + 1)
                        complianceClass = `compliance-${group}`
                    }

                    const weight = complianceScale.for(weights.find(tag => asset.tags.includes(tag.id))) || 1

                    const profile = { allNames: names, tag, name: tag.name, code: tag.code, weight, rate, complianceClass, complianceObservation }

                    return profile
                }

                ,


                timelinessProfile: (submission: SubmissionStub, instance?: AssetInstance, timelinessCache?: TimelinessCache, computedTimeliness?: 'ontime' | 'late' | 'notdue'): TimelinessProfile | undefined => {

                    const cache = timelinessCache ?? cstore.timelinessCache()

                    const asset = instance ?? (() => {

                        const trail = self.allTrails().find(t => t.id === submission.trail)
                        return subprofile.on(c).profileOf(trail?.key.assetType).instance(trail?.key.asset!)

                    })()

                    if (!asset)
                        return undefined

                    const scale = cache.scale

                    if (!scale)
                        return undefined

                    const { tags, names } = scale

                    const state = submission.lifecycle.compliance?.timeliness ?? subscales.on(c).defaultTimeliness(computedTimeliness, cache.scale?.tags)?.id

                    if (!state)
                        return undefined

                    const tag = tags.find(t => t.id === state)

                    if (!tag)
                        return undefined

                    const value = timelinessScale.for(tag)

                    const timelinessClass = !isNaN(value) ? value < 0 ? 'timeliness-late' : 'timeliness-ontime' : 'timeliness-plain'

                    const profile = { allNames: names, tag, name: tag.name, code: tag.code, timelinessClass }

                    return profile
                }

            }

            return self;
        }
    }

    return api
}

