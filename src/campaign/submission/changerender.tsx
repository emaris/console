import { Drawer } from '#app/components/Drawer'
import { Form } from '#app/form/Form'
import { useT } from '#app/intl/api'
import { useComponentData, useLayoutComponents } from '#layout/components/api'
import { FieldProps } from '#layout/components/fielddetail'
import { Component, RenderProps } from '#layout/components/model'
import { LayoutContext } from '#layout/context'
import { LayoutInstance, whenInstance } from '#layout/model'
import { Button, Tooltip } from 'antd'
import moment from 'moment-timezone'
import { PropsWithChildren, useContext, useState } from 'react'
import { changedateformat, changeicon, ChangelogContext, ChangeProvider } from './changeprovider'




export const ChangelogRender = (props: PropsWithChildren<RenderProps<Component & FieldProps>>) => {

    const t = useT()

    const mode = useContext(ChangelogContext)

    const { children, component } = props

    const layoutctx = useContext(LayoutContext)

    const { layout } = layoutctx

    const { componentId } = useComponentData()

    const components = useLayoutComponents()

    const { Render } = components.componentSpecOf(component)

    const { changes, resourceChanges } = (layout as LayoutInstance).state?.changelog ?? {}

    const cid = componentId(component)

    const change = changes?.[cid]
    const resourceChange = resourceChanges?.[cid]

    const [form, setForm] = useState<JSX.Element | undefined>(undefined)

    if (!mode.get() || !changes)
        return <>
            {children}
        </>

    if (!change)
        return <div className='changelog-unchanged'>
            {children}
        </div>

    const transformContext = () => whenInstance(layout).do(l => (

        {
            ...layoutctx,

            layout: {
                ...l, state: {
                    ...l.state,

                    resources: { ...l.state.resources, [cid]: resourceChange?.original },
                    data: { ...l.state.data, [cid]: change?.original }

                }
            }
        }))!

    const openDrawer = () => setForm(

        <Form >

            <div className='changelog-compare-section section-current'>

                <div className='section-title  section-current'>{t('submission.changelog.change_compare_current')}</div>

                <div className='section-field'>
                    <Render {...props} />
                </div>

            </div>

            <div className='changelog-compare-section section-original'>

                <div className='section-title'>{t('submission.changelog.change_compare_original')}</div>

                <LayoutContext.Provider value={transformContext()} >

                    <div className='section-field'>
                        <Render {...props} />
                    </div>

                </LayoutContext.Provider>

            </div>

        </Form>
    )

    const tooltip = <>

        {t('submission.changelog.highlight_tip', { date: moment(change.lastChanged).format(changedateformat) })}

        <Button className='changelog-compare' type='link' onClick={openDrawer}>
            {t('submission.changelog.change_compare_btn').toLowerCase()}
        </Button>

    </>


    return <Tooltip placement='topLeft' overlayClassName='tooltip-auto' trigger='hover' title={tooltip}>

        <div onDoubleClick={openDrawer}>

            <Drawer width={800} className='changelog-drawer' visible={!!form} title={t('submission.changelog.change_compare_title')} icon={changeicon} onClose={() => setForm(undefined!)}>

                {/* no change highlight mode inside the drawer or we 'recur': show tooltips that open drawers to show tooltips that open...*/}
                <ChangeProvider value={false}>
                    {form}
                </ChangeProvider>

            </Drawer>

            {children}

        </div>
    </Tooltip>

}