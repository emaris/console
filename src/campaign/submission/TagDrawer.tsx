import { Button } from '#app/components/Button'
import { DrawerProps, RoutableDrawerProps, useRoutableDrawer } from '#app/components/Drawer'
import { Paragraph } from '#app/components/Typography'
import { Form } from '#app/form/Form'
import { useFormState } from '#app/form/hooks'
import { useT } from '#app/intl/api'
import { TagBoxset } from '#app/tag/TagBoxset'
import { tagIcon } from '#app/tag/constants'
import { useTagStore } from '#app/tag/store'
import React from 'react'


export const useTagDrawer = (opts: Partial<RoutableDrawerProps> = {}) => {

    const t = useT()

    const { id = 'tags', title = t("submission.tags.title"), icon = tagIcon } = opts

    const { Drawer, open, close, visible, route, param } = useRoutableDrawer({ id, title, icon })

    const DrawerProxy = (props: Partial<DrawerProps>) => <Drawer width={500} icon={icon} title={title} {...props} />

    //eslint-disable-next-line
    const StableProxy = React.useCallback(DrawerProxy, [visible])


    return { TagDrawer: StableProxy, openTags: open, tagsRoute: route, closeTags: close, tagchangeParam: param }

}

export type TagFieldProps = {

    categories: string[]
    tags: string[],
    onSave: (_: string[]) => any
}

export const TagFields = (props: TagFieldProps) => {

    const t = useT()
    const { categories, tags:initialTags, onSave } = props

    const tagstore = useTagStore()
    
    const cats = categories.map(tagstore.lookupCategory)

    const {edited,set, dirty} = useFormState(initialTags)

    return <div style={{alignItems:'inherit'}} className="submission-dialog-panel">

        <Paragraph className='submission-dialog-explainer'>{t('submission.tags.explainer')}</Paragraph>

        <Form>
            <TagBoxset categories={cats} edited={edited} onChange={set} />
        </Form>


        <div style={{marginTop:40}}>
            <Button type='primary' enabled={dirty} onClick={() => onSave(edited)}>{t('submission.tags.change_btn')}</Button>
        </div>
    </div>


}