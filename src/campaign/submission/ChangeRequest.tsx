
import { Button } from '#app/components/Button'
import { DrawerProps, useRoutableDrawer } from '#app/components/Drawer'
import { Paragraph } from '#app/components/Typography'
import { Form } from '#app/form/Form'
import { TextBox } from '#app/form/TextBox'
import { useT } from '#app/intl/api'
import { useStable } from '#app/utils/function'
import { Icon } from 'antd'
import { useState } from 'react'
import { FaChevronCircleLeft } from 'react-icons/fa'
import { rejectIcon } from './constants'
import { useSubmissionContext } from './hooks'




export const useChangeRequestdDrawer = () => {

    const t = useT()

    const ctx = useSubmissionContext()


    const { Drawer, open, close, route, param } = useRoutableDrawer({ id: 'changerequest' })

    const mode = ctx.canReject() ? 'edit' : 'view' 

    const [request, setRequest] = useState<string>(undefined!)

    const showChangeRequestBtn =  <Button enabledOnReadOnly type="danger" onClick={open} >
        {t("submission.changerequest.showrequest_btn")}
    </Button>

    const currentRequest = ctx.submission.lifecycle.changeRequest

    const ChangeRequestDrawer = useStable((props: Partial<DrawerProps> & {

        onReject: (request: string) => Promise<void>

    }) => {

        const { onReject, ...rest } = props

        const reject = async () => {

            await onReject(request)

            resetAndClose()
        }

        const resetAndClose = () => {

            setRequest(undefined!)

            close()
        }

        return <Drawer onClose={resetAndClose} readonly={!ctx.canReject()} width={500} icon={rejectIcon} title={t("submission.changerequest.title")} {...rest}>

            <div style={{ alignItems: 'inherit' }} className="submission-dialog-panel">

                <Paragraph className="submission-dialog-intro">{t(`submission.changerequest.introduction_${mode}`)}</Paragraph>

                <div className="submission-dialog-section" style={{ background: 'inherit' }}>

                    <Icon style={{ marginTop: 10, color: 'cadetblue', fontSize: 130 }} className='submission-dialog-icon' component={FaChevronCircleLeft} />


                </div>

                <Form>

                    <TextBox placeholder={currentRequest}  autoSize={{ minRows: 5, maxRows: 8 }} id='comment' label={t(`submission.changerequest.request_lbl_${mode}`)} onChange={setRequest}>
                        {mode == 'edit' ? request : currentRequest}
                    </TextBox>

                    {ctx.canReject() &&

                        <Button disabled={!request} style={{ marginTop: 30 }} type="primary" onClick={reject} >
                            {t("submission.changerequest.confirm_btn")}
                        </Button>
                    }

                </Form>

            </div>

        </Drawer>

    }

    )

    return { ChangeRequestDrawer, openChangeRequest: open, changerequestRoute: route, closeChangeRequest: close, showChangeRequestBtn, changeRequestParam: param }

}

