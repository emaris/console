import { Label, LabelProps, sameLabel, UnknownLabel } from "#app/components/Label"
import { specialise } from "#app/iam/model"
import { icns } from "#app/icons"
import { useT } from '#app/intl/api'
import { useL } from '#app/model/multilang'
import { useLogged } from '#app/user/store'
import { useRoutedTypes } from '#config/model'
import { dashboardRoute } from '#dashboard/constants'
import { useCampaignMode } from "#dashboard/hooks"
import { Tooltip } from "antd"
import moment from "moment-timezone"
import * as React from "react"
import { Link, useLocation } from "react-router-dom"
import { campaignActions } from './actions'
import { campaignIcon, campaignRoute, campaignType, runningIcon } from "./constants"
import { Campaign, useCampaignModel, useCampaignStatus } from "./model"
import { useCampaignRouting, useCampaignStore } from './store'
import { submissionType } from "./submission/constants"

type Props = LabelProps & {

    campaign: string | Campaign | undefined

    statusOnly?: boolean

    noLiveView?: boolean

    targetMode?: 'default' | 'lineage'
}


const innerrouteRegexp = new RegExp(`${campaignRoute}/(.+?)(/|$)`.replace("/", "\\/"))


export const CampaignLabel = (props: Props) => {

    const t = useT()
    const l = useL()
    const routedTypes = useRoutedTypes()
    const campaignstore = useCampaignStore()
    const model = useCampaignModel()
    const status = useCampaignStatus()
    const campaignrouting = useCampaignRouting()
    const campaignMode = useCampaignMode()
    const { pathname, search } = useLocation()

    const logged = useLogged()

    const { campaign, decorations = [], options = [], noLiveView = false, linkTo, className = '', statusOnly, targetMode = 'default', ...rest } = props


    const resolved = typeof campaign === 'string' ? campaignstore.lookup(campaign) : campaign

    if (!campaign)
        return <UnknownLabel />


    if (!resolved)
        return <UnknownLabel tip={() => campaign as string} />

    const manageIt = specialise(campaignActions.manage, resolved.id)

    const isProtected = resolved ? resolved.predefined || resolved.guarded : false
    const readOnly = !logged.can(manageIt)

    const { label, start, end, started, ended } = status.status(resolved)

    const statusClass = started ? ended ? 'campaign-ended' : 'campaign-started' : ''

    const duration = ended ? t("campaign.lifecycle.ended_since", { since: moment(end).fromNow() })
        : started ? t("campaign.lifecycle.started_since", { since: moment(start).fromNow() })
            : start ? t("campaign.lifecycle.start_in", { in: moment(start).fromNow() })
                : undefined

    const iconWithStatus = (icon = campaignIcon) => started ? <span className={`campaign-icon ${statusClass}`}>{icon}</span> : icon

    const rdashboardRute = `${dashboardRoute}/${resolved?.id}`

    if (statusOnly)
        return <Label className={`status-only ${statusClass}`}
            linkTo={started ? rdashboardRute : undefined}
            tip={() =>duration}
            linkTarget={campaignType} title={label}
            icon={iconWithStatus(started ? runningIcon : campaignIcon)}
            {...rest} />

    const icon = started ? <Tooltip title={duration}>{iconWithStatus(campaignIcon)}</Tooltip> : iconWithStatus()

    if (targetMode === 'lineage') {
      
        return resolved.lineage ?
            <CampaignLabel campaign={resolved.lineage} decorations={decorations} noLiveView options={options} className={`${className}`} {...rest} />

            :

            <Label mode='tag' title={t("common.labels.none")} />
    }


    if (started && !noLiveView) {
        const icon = iconWithStatus(runningIcon)
        const contents = routedTypes.includes(campaignType) ? <Link to={rdashboardRute}>{icon}</Link> : icon
        if (routedTypes.includes(submissionType))
            options.push(<Tooltip title={t("campaign.labels.campaign_view.live")}>{contents}</Tooltip>)

    }

    if (model.isMuted(resolved))
        decorations.push(<Tooltip title={t("campaign.mute.muted")}>{icns.muted}</Tooltip>)

    if (campaignMode === 'dashboard' && routedTypes.includes(campaignType)){
        const route = campaignrouting.routeTo(resolved)
        options.push(<Tooltip title={t("campaign.labels.campaign_live.design")}><Link to={route}>{icns.edit}</Link></Tooltip>)
    }
    // like routeTo() if the current route is not a campaign route, otherwise replaces the current campaign with the target one.
    // undefined, if the two campaigns coincide.
    const innerRouteTo = (target: Campaign) => {

        const regexpmatch = pathname.match(innerrouteRegexp)?.[1]
        const linkname = target.id

        return regexpmatch ?
                    regexpmatch === linkname ? undefined : `${pathname.replace(regexpmatch, linkname!)}${search}`
                : campaignrouting.routeTo(target)


    }

    const route = linkTo ?? innerRouteTo(resolved) 

    // if (loadTrigger)
    //     decorations.push(<Button tooltip={t("common.feedback.load_details")} className="load-campaign" icn={icns.download} type="link" noborder onClick={()=>loadTrigger(resolved)} />)

    if (resolved.lineage){
        const lineage = campaignstore.lookup(resolved.lineage)
        decorations.push(<Tooltip title={<CampaignLabel mode="light" campaign={lineage} />}>{icns.branch}</Tooltip>)
    }

    return <Label className={`${statusClass} ${className}`} linkTarget={campaignType} readonly={readOnly} isProtected={isProtected} title={l(resolved.name)} icon={icon} linkTo={route} decorations={decorations} options={options} {...rest} />

}



type SuspendedLabelProps = LabelProps & {

    campaign: string | Campaign
}

export const SuspendedLabel = React.memo((props: SuspendedLabelProps) => {

    const {campaign, className='', ...rest} = props

    const t = useT()
    const store = useCampaignStore()
    const model = useCampaignModel()

    const currentCampaign = typeof campaign === 'string' ? store.safeLookup(campaign) : campaign

    const suspended = model.isSuspendSubmissions(currentCampaign)
    const archived = model.isArchived(currentCampaign)

    if (!suspended || archived)
        return <></>

    return <Label mode='tag' className={`campaign-suspended ${className}`} icon={icns.pause} title={t("campaign.suspend.suspended")} {...rest} />


}, ($, $$) => $.campaign === $$.campaign && sameLabel($, $$))

type MutedLabelProps = LabelProps & {

    campaign: string | Campaign
}


export const MutedLabel = React.memo((props: MutedLabelProps) => {

    const {campaign, className='', ...rest} = props

    const t = useT()
    const store = useCampaignStore()
    const model = useCampaignModel()

    const resolved = typeof campaign === 'string' ? store.safeLookup(campaign) : campaign

    if (!model.isMuted(resolved))
        return <></>

    return <Label mode='tag' className={`campaign-muted ${className}`} icon={icns.muted} title={t("campaign.mute.muted")} {...rest} />


}, ($, $$) => $.campaign === $$.campaign && sameLabel($, $$))

type ArchiveLabelProps = SuspendedLabelProps

export const ArchiveLabel = (props: ArchiveLabelProps) => {

    const {campaign, className='', ...rest} = props

    const t = useT()

    const campaignstore = useCampaignStore()
    const campaignmodel = useCampaignModel()

    const currentCampaign = typeof campaign === 'string' ? campaignstore.safeLookup(campaign) : campaign

    const archived = campaignmodel.isArchived(currentCampaign)

    if (!archived)
        return <></>

    return <Label mode='tag' className={`campaign-archived ${className}`} icon={icns.archive} title={t("campaign.archive.archived")} {...rest} />


}