import { Tenant } from "#app/tenant/model"
import { useTenantStore } from "#app/tenant/store"
import { User } from "#app/user/model"
import { useLogged, useUserStore } from '#app/user/store'
import { ApproveCycle, defaultApprovalCycleOption } from "#campaign/model"
import { summaryRoute } from "#dashboard/ViewRouter"
import { CalendarView, CampaignTenantPreferences, DashboardView, DateLocation } from "./model"



export const useCampaignTenantPreferences = () => {


    const tenants = useTenantStore()

    const prefs = (t: Tenant) => t.preferences as any as CampaignTenantPreferences

    const tenantFrom = (id: string) => tenants.safeLookup(id)

    const self = {


        location: (t: Tenant) => prefs(t).location

        ,


        locationFor: (id: string) => self.location(tenantFrom(id))

        ,

        setLocation: (t: Tenant, value: string) => prefs(t).location = value

        ,

        dateLocation: (t: Tenant) => prefs(t).dateLocation || 'home'

        ,

        dateLocationFor: (id: string) => self.dateLocation(tenantFrom(id))

        ,

        setDateLocation: (t: Tenant, value: DateLocation) => {

            if (value === 'home' || !value)
                delete prefs(t).dateLocation

            else prefs(t).dateLocation = value

        }

        ,

        approveCycle: (t: Tenant): ApproveCycle => prefs(t).approveCycle ?? defaultApprovalCycleOption

        ,

        approveCycleRef: (id: string): ApproveCycle => self.approveCycle(tenantFrom(id))

        ,

        setApproveCycle: (t: Tenant, value: ApproveCycle | undefined) => {

            if (!value)
                delete prefs(t).approveCycle

            else prefs(t).approveCycle = value

        }
        
        ,

        canBypassSelfApproval: (t: Tenant): boolean => !!prefs(t).bypassSelfApproval

        ,

        canBypassSelfApprovalRef: (id: string): boolean =>  self.canBypassSelfApproval(tenantFrom(id))
        

        ,

        setBypassSelfApproval: (t: Tenant, value: boolean) => {

            if (!value)
                delete prefs(t).bypassSelfApproval

            else prefs(t).bypassSelfApproval = value

        }

    }

    return self
}



export const useCampaignUserPreferences = () => {

    const tenantpreferences = useCampaignTenantPreferences()

    const logged = useLogged()

    const userstore = useUserStore()

    const self = {


        location: (u: User) => u.details.preferences.location ?? self.defaultLocation(u)

        ,

        defaultLocation: (u: User) => u ? tenantpreferences.locationFor(u.tenant) : undefined

        ,

        setLocation: (u: User, value: string) => {

            if (!value || value === self.defaultLocation(u))
                delete u.details.preferences.location

            else u.details.preferences.location = value

        }

        ,

        dateLocation: (u: User) => u.details.preferences.dateLocation ?? self.defaultDateLocation(u)

       ,

       defaultDateLocation: (u: User) => u ? tenantpreferences.dateLocationFor(u.tenant) : undefined

        ,

        setDateLocation: (u: User, value: DateLocation) => {

            if (!value || value === self.defaultDateLocation(u))
                delete u.details.preferences.dateLocation

            else u.details.preferences.dateLocation = value

        }

        ,

        dashboardView: (u: User = logged): DashboardView => u.details.preferences.defaultDashboardView ?? self.dashboardViewDefault(),


        dashboardViewDefault: (): DashboardView => summaryRoute

        ,


        setDashboardView: (u: User, value: DashboardView | undefined) => {

            if (!value || value === self.dashboardViewDefault())
                delete u.details.preferences.defaultDashboardView

            else u.details.preferences.defaultDashboardView = value

        }

        ,

        lastVisitedCampaign: (u: User = logged) => u.details.preferences.lastVisitedCampaign


        ,

        setLastVisitedCampaign: (u: User, value: string) => Promise.resolve(u.details.preferences.lastVisitedCampaign = value).then(self.updateLoggedUserProfile)

        ,

        calendarView: (u: User = logged): CalendarView => u.details.preferences.defaultCalendarView ?? self.calendarViewDefault()

        ,

        calendarViewDefault: (): CalendarView => 'month'

        ,

        setDefaultCalendarView: (u: User, value: CalendarView | undefined) => Promise.resolve(u.details.preferences.defaultCalendarView = value).then(self.updateLoggedUserProfile)

        ,

        updateLoggedUserProfile: () => userstore.updateUserProfileQuietly(logged.details.preferences)

    }

    return self;


}