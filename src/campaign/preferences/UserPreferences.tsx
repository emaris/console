import { dashboardIcon } from '#dashboard/constants'
import { Icon } from 'antd'
import { Label } from '#app/components/Label'
import { FormState } from '#app/form/hooks'
import { SelectBox } from '#app/form/SelectBox'
import { useT } from '#app/intl/api'
import { tenantType } from '#app/tenant/constants'
import { ZonePickerBox } from '#app/time/TimeZonePicker'
import { User } from '#app/user/model'
import { useLogged } from '#app/user/store'
import * as React from 'react'
import { AiOutlineCalendar, AiOutlineUnorderedList } from 'react-icons/ai'
import { useCampaignUserPreferences } from './api'
import { calendarOptions, dashboardOptions, dateLocationOptions } from './model'
import { useCampaignPreferencesValidation } from './validation'




export const UserPreferencesForm = (formstate: FormState<User>) => {

    const t = useT()

    const { edited, change } = formstate

    const logged = useLogged()
    
    const cmpprefs = useCampaignUserPreferences()
    const cmpvalidationprefs = useCampaignPreferencesValidation()

    const report = cmpvalidationprefs.validateUserPreferences(formstate)

    const managesManyParties = logged.managesMultipleTenants()

    const isApplicable = (view:string) => managesManyParties || view!==tenantType
    
    return <React.Fragment>

        <ZonePickerBox label={t("campaign.fields.location.name")} validation={report.home}
            includeLocal={false} 
            value={cmpprefs.location(edited)}
            onChange={change(cmpprefs.setLocation)} />

        <SelectBox label={t("campaign.fields.preferred_zone.name")}
            getlbl={l => t(dateLocationOptions[l])}
            validation={report.timezone}
            selectedKey={cmpprefs.dateLocation(edited)}
            onChange={change(cmpprefs.setDateLocation)}  >
            {
                Object.keys(dateLocationOptions)
            }

        </SelectBox>


        <SelectBox label={t("campaign.fields.default_dashboard_view.name")}
                   lblText={l=>t(dashboardOptions[l])}
                   getlbl={l => <Label icon={dashboardIcon} title={t(dashboardOptions[l])} />}
                   validation={report.defaultDashboardView}
                   selectedKey={cmpprefs.dashboardView(edited)}
                   onChange={change(cmpprefs.setDashboardView)}  >
                    {
                     Object.keys(dashboardOptions).filter(isApplicable)
                    }
        </SelectBox>

        <SelectBox label={t("campaign.fields.default_calendar_view.name")}
                   lblText={l=>t(calendarOptions[l])}
                   getlbl={l => <Label icon={ l === 'month' ? <Icon component={AiOutlineCalendar} /> : <Icon component={AiOutlineUnorderedList} /> } title={t(calendarOptions[l])} />}
                   validation={report.defaultCalendarView}
                   selectedKey={cmpprefs.calendarView(edited)}
                   onChange={change(cmpprefs.setDefaultCalendarView)}  >
                    {
                     Object.keys(calendarOptions).filter(isApplicable)
                    }
        </SelectBox>


    </React.Fragment>

}