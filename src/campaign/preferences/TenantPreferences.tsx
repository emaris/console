import { approveCycleOptions } from '#campaign/model'
import { Radio } from 'antd'
import { Paragraph } from '#app/components/Typography'
import { FormState } from '#app/form/hooks'
import { RadioBox } from '#app/form/RadioBox'
import { SelectBox } from '#app/form/SelectBox'
import { useT } from '#app/intl/api'
import { Tenant } from '#app/tenant/model'
import { ZonePickerBox } from '#app/time/TimeZonePicker'
import * as React from 'react'
import { useCampaignTenantPreferences } from './api'
import { dateLocationOptions } from './model'
import { useCampaignPreferencesValidation } from './validation'






// ui

export const TenantPreferencesForm = (formstate: FormState<Tenant>) => {

    const t = useT()

    const { edited, change } = formstate

    const report = useCampaignPreferencesValidation().validateTenantPreferences(formstate)

    const prefs = useCampaignTenantPreferences()


    return <React.Fragment>

        <ZonePickerBox label={t("campaign.fields.location.name")} validation={report.home} 
            includeLocal={false} 
            value={prefs.location(edited)}
            onChange={change(prefs.setLocation)} />

        <SelectBox label={t("campaign.fields.preferred_zone.name")}  validation={report.timezone}
            getlbl={l => t(dateLocationOptions[l])}
            selectedKey={prefs.dateLocation(edited)}
            onChange={change(prefs.setDateLocation)}  >
            {Object.keys(dateLocationOptions)}
        </SelectBox>


        <RadioBox   label={t("campaign.fields.approval.name")}
                    validation={{
                        msg:t("campaign.fields.approval.msg"),
                        help:<React.Fragment>
                                <Paragraph>{t("campaign.fields.approval.help1")}</Paragraph>
                                <Paragraph spaced >{t("campaign.fields.approval.help2_tenants")}</Paragraph>
                             </React.Fragment>}}
                    value={ prefs.approveCycle(edited)}
                    onChange={change( (t,v)=>prefs.setApproveCycle(t,v==='none' ? undefined : v))}>
            
            {
                Object.keys(approveCycleOptions).map(o=>
            
                <Radio key={o} value={o}>{t(approveCycleOptions[o])}.</Radio>)
            
            }
        </RadioBox>

    </React.Fragment>
}

