import { productType, productPlural } from "#product/constants";
import { requirementType, requirementPlural } from "#requirement/constants";
import { tenantType, tenantPlural } from "#app/tenant/constants";
import { eventType } from "#event/constants";
import { ApproveCycle } from "#campaign/model";





type CommonPreferences = {

    location?: string
    dateLocation?: DateLocation
}




// ------------------------------------------------------------------------------------------------------ tenants

export type CampaignTenantPreferences = CommonPreferences & {

    approveCycle?: ApproveCycle
    bypassSelfApproval?: boolean
}


export type DateLocation = "home" | "local" | "universal" | "original"


export const dateLocationOptions : { [key in DateLocation] : string } = {

    local: "campaign.fields.location.local",
    home : "campaign.fields.location.home",
    original: "campaign.fields.location.original",
    universal: "campaign.fields.location.none",
}


// ------------------------------------------------------------------------------------------------------ users


export type UserPreferences = CommonPreferences & {

    lastVisitedCampaign: string
    defaultDashboardView?: DashboardView
    defaultCalendarView?: CalendarView
}


export type DashboardView = 'summary' | typeof tenantType | typeof requirementType | typeof productType | typeof eventType

export type CalendarView = 'month' | 'list'


export const dashboardOptions : { [key in DashboardView] : string } = {
    'summary':"dashboard.labels.summary",
    [tenantType]:tenantPlural,
    [requirementType]:requirementPlural,
    [productType]:productPlural,
    [eventType]:"dashboard.calendar.name"
}

export const calendarOptions : { [key in CalendarView] : string } = {
    'list' : "dashboard.calendar.list",
    'month' : "dashboard.calendar.month"
}


