import { Paragraph } from '#app/components/Typography'
import { FormState } from '#app/form/hooks'
import { Tenant } from '#app/tenant/model'
import { User } from '#app/user/model'
import { check, checkIt, notdefined } from '#app/utils/validation'
import { useT } from '#app/intl/api'


export const useCampaignPreferencesValidation = () => {


    const t = useT()

    return {


        validateUserPreferences: (_: FormState<User>) => {

            return {

                home: checkIt().nowOr(t("campaign.fields.location.msg"), t("campaign.fields.location.help")),

                timezone: checkIt().nowOr(t("campaign.fields.preferred_zone.msg"),
                    <>
                        <Paragraph>{t("campaign.fields.preferred_zone.help1")}</Paragraph>
                        <Paragraph spaced>{t("campaign.fields.preferred_zone.help2")}</Paragraph>
                    </>),

                defaultDashboardView: checkIt().nowOr(t("campaign.fields.default_dashboard_view.msg"),
                    <>
                        <Paragraph>{t("campaign.fields.default_dashboard_view.help1")}</Paragraph>
                        <Paragraph spaced>{t("campaign.fields.default_dashboard_view.help2")}</Paragraph>
                    </>),

                defaultCalendarView: checkIt().nowOr(t("campaign.fields.default_calendar_view.msg"),
                    <>
                        <Paragraph>{t("campaign.fields.default_calendar_view.help1")}</Paragraph>
                        <Paragraph spaced>{t("campaign.fields.default_calendar_view.help2")}</Paragraph>
                    </>),

            }
        }

        ,


        validateTenantPreferences: (_: FormState<Tenant>) => {


            return {
                home: check(_.edited.preferences.location).with(notdefined(t)).nowOr(t("campaign.fields.location.msg"), t("campaign.fields.location.help")),
                timezone: checkIt().nowOr(t("campaign.fields.preferred_zone.msg"),
                    <>
                        <Paragraph>{t("campaign.fields.preferred_zone.help1")}</Paragraph>
                        <Paragraph spaced>{t("campaign.fields.preferred_zone.help2")}</Paragraph>
                    </>),


                approvalCycle: checkIt().nowOr(t("campaign.fields.approval.msg"),
                    <>
                        <Paragraph>{t("campaign.fields.approval.help1")}</Paragraph>
                        <Paragraph spaced>{t("campaign.fields.approval.help2_tenants")}</Paragraph>
                    </>)
            }
        }
    
    }}