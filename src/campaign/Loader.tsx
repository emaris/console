
import { useRenderGuard } from 'apprise-frontend-core/utils/renderguard'
import * as React from "react"
import { useCampaignClient } from './client'

type Props = React.PropsWithChildren<{
  placeholder?: React.ReactNode
}>


export const CampaignLoader = (props: Props) => {

  const { areReady, fetchAll } = useCampaignClient()


  const {content} = useRenderGuard({
    when: areReady(),
    render: props.children,
    orRun: fetchAll,
    andRender: props.placeholder

  })

  return content

}