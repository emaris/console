import { useT } from '#app/intl/api'
import { defaultLanguage } from "#app/intl/model"
import { useSettings } from '#app/settings/store'
import { useToggleBusy } from "#app/system/api"
import { tenantType } from '#app/tenant/constants'
import { Tenant } from '#app/tenant/model'
import { indexMap, through } from "#app/utils/common"
import { notify, showAndThrow, showError, useFeedback } from "#app/utils/feedback"
import { useEventInstanceCalls } from '#campaign/event/calls'
import { EventInstance } from '#campaign/event/model'
import { useEventInstanceStore } from "#campaign/event/store"
import { PartyInstance, usePartyInstances } from '#campaign/party/api'
import { usePartyInstanceCalls } from '#campaign/party/calls'
import { useEventCalls } from '#event/calls'
import { eventType } from '#event/constants'
import { Event } from '#event/model'
import { useEventStore } from '#event/store'
import { useProductCalls } from '#product/calls'
import { productType } from '#product/constants'
import { Product } from '#product/model'
import { useProductStore } from '#product/store'
import { useRequirementCalls } from '#requirement/calls'
import { requirementType } from '#requirement/constants'
import { Requirement } from '#requirement/model'
import { useRequirementStore } from '#requirement/store'
import { useContext } from 'react'
import { unstable_batchedUpdates } from 'react-dom'
import { CampaignArchive, useCampaignCalls } from "./calls"
import { campaignRoute, campaignSingular, campaignType } from "./constants"
import { ArchiveState, CampaignContext, CampaignNext } from './context'
import { Campaign, CampaignAppSettings, newCampaign, newCampaignId, useNoCampaign } from "./model"
import { usePartyInstanceClient } from './party/client'
import { useProductInstances } from './product/api'
import { useProductInstanceCalls } from './product/calls'
import { useProductInstanceClient } from "./product/client"
import { CampaignChangeEvent } from './pushevents'
import { useRequirementInstances } from "./requirement/api"
import { useRequirementInstanceCalls } from './requirement/calls'
import { useRequirementInstanceClient } from './requirement/client'
import { useSubmissionCalls } from './submission/calls'
import { useSubmissionClient } from './submission/client'
import { useSubmissionStore } from "./submission/store"


export type PruneInstanceCheck = {
    predicate: (c: Campaign) => boolean
}




export const useCampaignStore = () => {

    const state = useContext(CampaignContext)

    const noCampaign = useNoCampaign()

    const self = {

        isLoaded: (c: string | Campaign) => {

            const campaign = typeof c === 'string' ? self.lookup(c) : c

            if (!campaign)
                return false

            const archived = campaign.lifecycle.state === 'archived'

            const inst = state.get().campaigns.instances[campaign.id] ?? {}

            // must have all instances and, if archived, must have the archive too.
            return inst?.products?.all && inst?.requirements?.all && inst?.events?.all && inst?.parties?.all && (!archived || !inst.archive)

        }

        ,

        isArchiveLoaded: (campaign: string) => state.get().campaigns.instances[campaign]?.archive

        ,


        all: () => state.get().campaigns.all


        ,


        setAll: (campaigns: Campaign[], props?: { noOverwrite: boolean }) => state.set(s => {

            // guard changes (used in preloading)
            if (s.campaigns.all && props?.noOverwrite)
                return

            s.campaigns.all = campaigns;
            s.campaigns.map = indexMap(campaigns).by(c => c.id)
        })

        ,

        allInstances: () => state.get().campaigns.instances

        ,

        lookup: (id: string | undefined) => id ? state.get().campaigns.map?.[id] : undefined

        ,


        lookupByName: (name: string | undefined) => state.get().campaigns.all?.find(u => u.name[defaultLanguage] === name)

        ,

        safeLookup: (id: string | undefined) => self.lookup(id) ?? noCampaign.get()


        ,


        setArchive: (campaign: Campaign, archive: CampaignArchive) => {

            if (!archive.length)
                return

            const map = archive.reduce((acc, e) => {

                switch (e.type) {

                    case tenantType: acc.tenants = [...acc.tenants ?? [], e.details]; break
                    case requirementType: acc.requirements = [...acc.requirements ?? [], e.details]; break
                    case productType: acc.products = [...acc.products ?? [], e.details]; break
                    case eventType: acc.events = [...acc.events ?? [], e.details]; break
                }

                return acc

            }, {
                requirements: [],
                products: [],
                tenants: [],
                events: []

            } as { requirements: Requirement[], products: Product[], tenants: Tenant[], events: Event[] })


            state.set(s => {

                s.campaigns.instances[campaign.id] = {

                    ...s.campaigns.instances[campaign.id] ?? {},
                    archive: ({

                        requirements: {

                            all: map.requirements,
                            map: indexMap(map.requirements).by(r => r.id)
                        }
                        ,
                        products: {

                            all: map.products,
                            map: indexMap(map.products).by(p => p.id)
                        }
                        ,
                        tenants: {

                            all: map.tenants,
                            map: indexMap(map.tenants).by(p => p.id)

                        }
                        ,
                        events: {

                            all: map.events

                        }

                    }) as ArchiveState
                }

            })

        }
        // ,

        // defaultCampaign: () => self.lookup(preferences.userPreferences().lastVisitedCampaign()) ??
        //     self.allSorted().filter(c => dates.startDate(c))[0]
        //     ?? self.allSorted()[0]



    }



    return self;

}




export const useCampaignRouting = () => {

    const state = useContext(CampaignContext)

    const settings = useSettings().get<CampaignAppSettings>().campaign

    const self = {


        routeTo: (campaign: Campaign) => campaign ? `${campaignRoute}/${campaign.id}` : undefined!


        ,

        next: (): CampaignNext => state.get().campaigns.next ?? { source: newCampaign(settings), type: 'new' }

        ,

        resetNext: () => self.setNext(undefined)

        ,

        setNext: (context: CampaignNext | undefined) => state.set(s => s.campaigns.next = context)


    }

    return self
}

export const useCampaignDetail = () => {

    const t = useT()
    const toggleBusy = useToggleBusy()
    const fb = useFeedback()

    const calls = useCampaignCalls()

    const store = useCampaignStore()
    const partyinstclient = usePartyInstanceClient()
    const reqinstclient = useRequirementInstanceClient()
    const prodinstclient = useProductInstanceClient()
    const eventinststore = useEventInstanceStore()
    const subclient = useSubmissionClient()

    const singular = t(campaignSingular).toLowerCase()

    const self = {


        fetchOne: (campaign: Campaign, clientForceRefresh?: boolean, asLineage?: boolean): Promise<Campaign> => {

            const archived = campaign.lifecycle.state === 'archived'

            return toggleBusy(`${campaignType}.fetchOne`, archived ? t("campaign.feedback.load_archive") : t("common.feedback.load_one", { singular }))

                .then(_ => self.fetchOneQuietly(campaign, clientForceRefresh, asLineage))

                .catch(e => showAndThrow(e, t("common.calls.fetch_one_error", { singular })))
                
                .finally(() => toggleBusy(`${campaignType}.fetchOne`))


        }

        ,

        fetchOneQuietly: (campaign: Campaign, clientForceRefresh?: boolean, asLineage?: boolean): Promise<Campaign> => {

            const archived = campaign.lifecycle.state === 'archived'
            const t0 = performance.now()

            const forceRefresh = clientForceRefresh || (archived && !store.isArchiveLoaded(campaign.id))

            return calls.fetchOne(campaign.id, asLineage)
                .then(({ campaign, archive }) => {

                    if (archived)
                        store.setArchive(campaign, archive)

                    return campaign

                })
                .then(through(fetched =>

                    // if no instance is mocked, then we can afford a more parallel approach.
                    Promise.all([
                        subclient.on(fetched).fetchAllTrails(forceRefresh),
                        partyinstclient.on(fetched).fetchAll(forceRefresh),
                        reqinstclient.on(fetched).fetchAll(forceRefresh),
                        prodinstclient.on(fetched).fetchAll(forceRefresh),
                        eventinststore.on(fetched).fetchAll(forceRefresh),
                    ])

                ))

                .then(through(fetched => { if (!fetched) throw Error(`Invalid resource lookup for ${campaign.id}, returned undefined.`) }))
                .then(fetched => {
                    const time = performance.now() - t0
                    console.log(`loading campaign took ${time}`)
                    return { ...fetched, loaded: true }
                })   // interning campaign
                .then(through(fetched => store.setAll(store.all().map(u => u.id === campaign.id ? fetched : u))))


        }


        ,


        save: (campaign: Campaign, context: CampaignNext): Promise<Campaign> => {


            const updateCampaign = () => calls.update(campaign)
                .then(({ campaign, archive }) => {

                    if (campaign.lifecycle.state === 'archived')
                        store.setArchive(campaign, archive)

                    return campaign

                })
                .then(saved => ({ ...saved, loaded: true }))
                .then(through(saved => store.setAll(store.all().map(c => c.id === campaign.id ? saved : c))))



            const addCampaign = () => calls.add({ ...campaign, id: newCampaignId() })
                .then(through(saved => store.setAll([saved, ...store.all()])))
                .then(through(saved => context.type === 'new' ?

                    eventinststore.on(saved).addManagedForCampaign()
                    :

                    self.cloneInstances(saved, context)))




            return toggleBusy(`${campaignType}.save`, t("common.feedback.save_changes"))


                .then(() => campaign.id ? updateCampaign() : addCampaign())

                .then(through(() => notify(t('common.feedback.saved'))))
                .catch(e => showAndThrow(e, t("common.calls.save_one_error", { singular })))
                .finally(() => toggleBusy(`${campaignType}.save`))
        }

        ,


        cloneInstances: (to: Campaign, context: CampaignNext) => {

            const { lineage } = context

            const from = lineage!

            console.log(`cloning instances from ${from.name[defaultLanguage]} into ${to.name[defaultLanguage]}`)


            return toggleBusy(`clone.campaign.${to.id}`, t("campaign.feedback.clone_resources"))

                .then(_ => Promise.all([

                    partyinstclient.on(from).fetchAll().then(all => partyinstclient.on(to).cloneAll(all, context)),
                    reqinstclient.on(from).fetchAll().then(all => reqinstclient.on(to).cloneAll(all, context)),
                    prodinstclient.on(from).fetchAll().then(all => prodinstclient.on(to).cloneAll(all, context)),
                    eventinststore.on(from).fetchAll().then(all => eventinststore.on(to).cloneAll(all, context))



                ]))
                .catch(e => showError(e, { title: t("campaign.feedback.clone_error", { singular }) }))
                .finally(() => toggleBusy(`clone.campaign.${to.id}`))


        }

        ,

        remove: (id: string, onConfirm: () => void, challenge?: boolean) =>

            fb.askConsent({

                title: t('common.consent.remove_one_title', { singular }),
                content: t('common.consent.remove_one_msg', { singular }),
                okText: t('common.consent.remove_one_confirm', { singular }),

                okChallenge: challenge ? t('common.consent.remove_challenge', { singular }) : undefined,

                onOk: () => {


                    toggleBusy(`${campaignType}.removeOne`, t("common.feedback.save_changes"))

                        .then(_ => calls.delete(id))
                        .then(_ => store.all().filter(u => id !== u.id))
                        .then(store.setAll)
                        .then(_ => notify(t('common.feedback.saved')))
                        .then(_ => onConfirm && onConfirm())

                        .catch(e => showAndThrow(e, t("common.calls.remove_one_error", { singular })))
                        .finally(() => toggleBusy(`${campaignType}.removeOne`))


                }
            }),


    }

    return self

}

export const useCampaignLoader = () => {

    const reqstore = useRequirementStore()
    const reqcalls = useRequirementCalls()
    const prodcalls = useProductCalls()
    const eventcalls = useEventCalls()

    const partyinstcalls = usePartyInstanceCalls()
    const reqinstcalls = useRequirementInstanceCalls()
    const prodinstcalls = useProductInstanceCalls()
    const eventinstcalls = useEventInstanceCalls()
    const subcalls = useSubmissionCalls()

    const partyinststore = usePartyInstances()
    const reqinststore = useRequirementInstances()
    const prodinststore = useProductInstances()
    const eventinststore = useEventInstanceStore()
    const substore = useSubmissionStore()

    const prodstore = useProductStore()
    const eventstore = useEventStore()


    const self = {

        // loads one ore more campaigns in the background, i.e. without slowing the calls required to render a first view.
        // accumulates all state updates triggered by loading, so that they can be flushed at once and trigger a single render.
        // if the client presents itw own buffer of updates, it add those that and leaves the flushing to the client.
        fetchManyQuietly: (campaigns: Campaign[], noOverwrite: boolean = false, clientudpates?: (() => void)[]) => {


            const updates: (() => void)[] = clientudpates ?? []

            // 1 fetch all campaigns and templates in parallel.
            // 2. for eah running campaign, load instances/trails in parallel and stores in client state (if it hasn't already in concurrent)
            // 3. mark end of preload for who wants to sync with it.

            // note; we'd better parallelise requests across all campaigns, not just inside single campaigns.
            // but in http/1 this would exceed the limits of browsers, to the effect that calls required for rendering (high-priority) woul be scheduled after.
            // instead we want to preload in the background and never increase the wait to render the first page.
            // we can increase parallelism significantly if we move to htto/2.

            return Promise.all([

                reqcalls.fetchAll().then($ => updates.push(() => reqstore.setAll($, { noOverwrite }))),
                prodcalls.fetchAll().then($ => updates.push(() => prodstore.setAll($, { noOverwrite }))),
                eventcalls.fetchAll().then($ => updates.push(() => eventstore.setAll($, { noOverwrite }))),

                campaigns.reduce((acc, next) => acc.then(() =>

                    Promise.resolve(console.log(`loading campaign ${next.name.en}(${next.id})`))
                        .then(() => Promise.all([

                            partyinstcalls.on(next).fetchAll().then($ => updates.push(() => partyinststore.on(next).setAll($, { noOverwrite }))),
                            reqinstcalls.on(next).fetchAll().then($ => updates.push(() => reqinststore.on(next).setAll($, { noOverwrite }))),

                            prodinstcalls.on(next).fetchAll().then($ => updates.push(() => prodinststore.on(next).setAll($, { noOverwrite }))),
                            eventinstcalls.on(next).fetchAll().then($ => updates.push(() => eventinststore.on(next).setAll($, { noOverwrite }))),
                            subcalls.on(next).fetchAllTrails().then($ => updates.push(() => substore.on(next).setAllTrails($, { noOverwrite })))

                        ]))

                ), Promise.resolve() as Promise<any>)

            ])

                .finally(() => {

                    if (!clientudpates)

                        unstable_batchedUpdates(() => {

                            updates.forEach(f => f())

                        })


                })

        }

        ,

        livePush: (event: CampaignChangeEvent) => {

            const { campaign, instanceType, instances, type, trails } = event

            unstable_batchedUpdates(() => {

                switch (instanceType) {

                    case requirementType: reqinststore.on(campaign).livePush(instances, type === 'remove' ? type : 'change'); break;
                    case productType: prodinststore.on(campaign).livePush(instances, type === 'remove' ? type : 'change'); break;
                    case tenantType: partyinststore.on(campaign).livePush(instances as PartyInstance[], type === 'remove' ? type : 'change'); break;
                    case eventType: eventinststore.on(campaign).livePush(instances as EventInstance[], type === 'remove' ? type : 'change'); break;
                }

                substore.on(campaign).livePush(trails, type === 'remove' ? type : 'change');

            })
        }


    }

    return self
}