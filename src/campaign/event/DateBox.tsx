import { Label } from "#app/components/Label"
import { CheckBox } from "#app/form/CheckBox"
import { Field, FieldProps } from "#app/form/Field"
import { FormContext } from "#app/form/Form"
import { useReadonly } from "#app/form/ReadonlyBox"
import { VSelectBox } from "#app/form/VSelectBox"
import { icns } from "#app/icons"
import { useT } from '#app/intl/api'
import { useLocale } from "#app/model/hooks"
import { useTimePicker } from "#app/time/TimePickerBox"
import { campaignType } from "#campaign/constants"
import { useCurrentCampaign } from "#campaign/hooks"
import { useCampaignStore } from '#campaign/store'
import { eventType, pinnedDateIcon, recurringDateIcon, singleUseDateIcon } from "#event/constants"
import { useEventStore } from '#event/store'
import { productType } from "#product/constants"
import { useProductStore } from '#product/store'
import { requirementType } from "#requirement/constants"
import { useRequirementStore } from '#requirement/store'
import { InputNumber, Select } from "antd"
import moment from "moment-timezone"
import * as React from "react"
import { EventInstanceLabel } from "./Label"
import { AbsoluteDate, EventDate, EventInstance, EventInstanceDto, RelativeDate, absoluteOf, isRelative, isTemporal, useEventInstanceModel } from "./model"
import { useEventInstanceDates, useEventInstanceStore } from './store'
import "./styles.scss"



export type Props = FieldProps & {

    event: EventInstance

    // in some context the target of relative dates is known, so we won't ask for one.
    fixedTarget?: string

    light?: boolean

    overlay?: EventInstanceDto[]

    dateFormat?: string

    onChange: (_: EventDate) => void

    showTime?: boolean

}

const units = {

    "days": { max: 999 },
    "weeks": { max: 999 },
    "months": { max: 999 }
}


export const DateBox = (props: Props) => {

    const t = useT()

    const campaign = useCurrentCampaign()

    const { event, light, onChange, fixedTarget, overlay, dateFormat, readonly, className = '', showTime = false, ...rest } = useReadonly(props)

    const instances = { ...useEventInstanceStore().on(campaign), ...useEventInstanceModel().on(campaign), ...useEventInstanceDates().on(campaign) }

    const initRelativeDate: RelativeDate = isRelative(event.date) ? event.date : instances.defaultRelativeDate(fixedTarget as string)
    const initCalendarDate = isRelative(event.date) ? undefined : event.date

    // preserve edits across mode switches. 
    const [relativeDate, setRelativeDate] = React.useState(initRelativeDate)
    const [calendarDate, setCalendarDate] = React.useState(initCalendarDate)

    // sync local state and external state on change
    const setRelativeDateAndChange = (d: RelativeDate) => Promise.resolve(setRelativeDate(d)).then(() => onChange(d))
    const setCalendarDateAndChange = (d: AbsoluteDate | undefined) => Promise.resolve(setCalendarDate(d)).then(() => onChange(d))

    const initMode = isRelative(event.date) || (!event.date && isTemporal(event)) ? "relative" : "absolute"

    // start with relative date if we have a relative date or we have a temporal
    const [mode, setMode] = React.useState(initMode)

    const switchMode = mode => Promise.resolve(setMode(mode)).then(() => onChange(mode === 'relative' ? relativeDate : calendarDate))


    // resets local state when form in context does.
    const resetted = React.useContext(FormContext).resetted

    const reset = () => {

        setRelativeDate(initRelativeDate)
        setCalendarDate(initCalendarDate)
        setMode(initMode)

    }


    React.useEffect(() => {

        if (resetted)
            reset()

        // eslint-disable-next-line
    }, [resetted])


    React.useEffect(() => {

        reset()

        // eslint-disable-next-line
    }, [fixedTarget])



    const { Picker } = useTimePicker()

    const targets = instances.all().filter(i => i.type !== eventType && i.id !== event.id)

    const hideIfSelected = (value: string) => ({ display: mode !== value ? 'inherit' : 'none' })

    const hideBranchTypeIfSelected = (value: string) => {
        const visible = { display: 'inherit' }
        const hidden = { display: 'none' }

        if (calendarDate?.branchType === undefined && value === 'recurring') return hidden
        return calendarDate?.branchType !== value ? visible : hidden
    }

    const isNone = calendarDate?.none

    const typeSwitch = <Select className="datebox-type" disabled={readonly || isNone} dropdownClassName="date-option" suffixIcon={icns.down} defaultValue={mode} onChange={switchMode} >
        <Select.Option style={hideIfSelected('absolute')} className="datebox-option" value="absolute">{t("campaign.date.calendar")}</Select.Option>
        <Select.Option style={hideIfSelected('relative')} className="datebox-option" value="relative">{t("campaign.date.relative")}</Select.Option>
    </Select>
    //'pinned' | 'recurring' | 'single-use'

    const branchTypeSwitch = <Select className="datebox-branch-type" disabled={readonly} dropdownClassName="date-option" suffixIcon={icns.down} defaultValue={calendarDate?.branchType || 'recurring'} onChange={bt => setCalendarDateAndChange({ ...calendarDate, branchType: bt } as AbsoluteDate)} >
        <Select.Option style={hideBranchTypeIfSelected('recurring')} className="datebox-option" value="recurring"><Label icon={recurringDateIcon} title={t("campaign.date.recurring")} tip={t("campaign.date.recurring_tip")} /></Select.Option>
        <Select.Option style={hideBranchTypeIfSelected('pinned')} className="datebox-option" value="pinned"><Label icon={pinnedDateIcon} title={t("campaign.date.pinned")} tip={t("campaign.date.pinned_tip")} /></Select.Option>
        <Select.Option style={hideBranchTypeIfSelected('single')} className="datebox-option" value="single">{<Label icon={singleUseDateIcon} title={t("campaign.date.single_use")} tip={t("campaign.date.single_use_tip")} />}</Select.Option>
    </Select>



    // keeps track of height of datebox frame
    const [height, setHeight] = React.useState(0);
    const onSwapChild = d => d == null || setHeight(d.getBoundingClientRect().height);
    const onShowTarget = d => d == null || setHeight(h => h + d.getBoundingClientRect().height)

    const absoluteDateUnformatted = instances.absolute(event.date, overlay)
    const absoluteDate = dateFormat ? absoluteDateUnformatted ? moment(absoluteDateUnformatted).format(dateFormat) : undefined : absoluteDateUnformatted

    const onNoneChange = (v: boolean | undefined) => {
        const value = v ? undefined : calendarDate?.value
        const none = v ?? undefined
        const newVal = v ? {...calendarDate, value, none, kind: 'absolute'} as AbsoluteDate : undefined
        setCalendarDateAndChange(newVal)
    }

    const noDueDateCheckBox = <div className="event-none-wrapper"><CheckBox title={t('campaign.date.none')} light onChange={onNoneChange} >{calendarDate?.none}</CheckBox></div>

    const hasNoDueDateControl = React.useMemo(() => mode === 'absolute' && !absoluteDate && !isTemporal(event), [mode, absoluteDate, event])
    const hasBranchTypeControl = React.useMemo(() => mode === 'absolute' && absoluteDate, [mode, absoluteDate])

    const rightControls = hasNoDueDateControl ? noDueDateCheckBox : hasBranchTypeControl ? branchTypeSwitch : null

    const contentsAbsolute = <div ref={onSwapChild} className="datebox-absolute">

        {typeSwitch}

        {/* on a 'reset render' we pass down the initial value so that Picker can reset in turn.
                                if we waited to reset with the effect, the Picker would not detect a 'reset render'. */}
        <Picker light disabled={readonly || isNone} showTime={showTime} format={dateFormat} onChange={m => setCalendarDateAndChange(m ? absoluteOf(m.format()) : undefined)} value={resetted ? initCalendarDate?.value : calendarDate?.value} />

        {/* {absoluteDate && branchTypeSwitch} */}
        {rightControls}

        {/* <Tooltip  mouseEnterDelay={.8} title={t("campaign.date.pinned_tip")} placement="topRight">
            <div className={`absolute-decoration ${decorationDisabled ? 'decoration-disabled' : ''} ${calendarDate?.branchType === 'pinned' ? 'decoration-selected' : calendarDate?.branchType ? 'decoration-unselected' : ''}`}
                onClick={() => decorationDisabled || setCalendarDateAndChange({ ...calendarDate, branchType: calendarDate?.branchType === 'pinned' ? undefined : 'pinned' } as AbsoluteDate)}
            >
                {pinnedDateIcon}
            </div>
        </Tooltip>
        <Tooltip  mouseEnterDelay={.8} title={t("campaign.date.recurring_tip")} placement="topRight">
            <div className={`absolute-decoration ${decorationDisabled ? 'decoration-disabled' : ''}  ${(calendarDate?.branchType === 'recurring' || calendarDate?.branchType === undefined) ? 'decoration-selected' : calendarDate?.branchType ? 'decoration-unselected' : ''}`}
                onClick={() => decorationDisabled || setCalendarDateAndChange({ ...calendarDate, branchType: calendarDate?.branchType === 'recurring' ? undefined : 'recurring' } as AbsoluteDate)}
            >
                {recurringDateIcon}
            </div>
        </Tooltip> */}
    </div>



    const contentsRelative = <div ref={onSwapChild} className="datebox-relative">

        <PureRelativeDateBox
            mode={mode}
            onChange={setRelativeDateAndChange}
            relativeDate={relativeDate}
            absoluteDate={absoluteDate}
            showTargets={!isTemporal(event) && !fixedTarget}
            targets={targets}
            onShowTarget={onShowTarget}
            validation={props.validation}
            stringifyTargets={instances.stringify}
            disabled={readonly}>
            {typeSwitch}
        </PureRelativeDateBox>

    </div>



    const contents = <div style={{ height: height + 2 }} className={`datebox-frame`}>{mode === 'relative' ? contentsRelative : contentsAbsolute}</div>


    return light ?

        <div style={rest.style} className={`datebox ${className}`}>{contents}</div>

        :

        <Field {...rest} className={`datebox ${className}`}> {contents}</Field>
}


export type PureRelativeDateBoxProps = FieldProps & {
    mode: string
    relativeDate: RelativeDate
    onChange: (t: any) => void
    absoluteDate?: string
    onShowTarget?: (d: any) => true | void
    targets?: EventInstanceDto[]
    showTargets?: boolean
    stringifyTargets?: (t: any) => string
    asFormField?: boolean
    children?: JSX.Element
}


export const PureRelativeDateBox = (props: PureRelativeDateBoxProps) => {

    const t = useT()
    const { l } = useLocale()

    const { relativeDate, onChange, mode, readonly: readonlyContext, absoluteDate, onShowTarget, targets, showTargets = false, stringifyTargets, asFormField = false, ...rest } = useReadonly(props)

    const hideIfSelected = (value: string) => ({ display: mode !== value ? 'inherit' : 'none' })

    const changePeriod = (num: number | undefined) => onChange({ ...relativeDate, period: { ...relativeDate.period, number: num! } })
    const changeUnit = (unit: "months" | "days" | "weeks") => onChange({ ...relativeDate, period: { ...relativeDate.period, unit } })
    const changeDirection = (direction: any) => onChange({ ...relativeDate, period: { ...relativeDate.period, direction } })
    const changeTarget = (target: any) => onChange({ ...relativeDate, target: target.id })

    const events = useEventStore()
    const campaignstore = useCampaignStore()
    const requirements = useRequirementStore()
    const products = useProductStore()

    const readonly = props.disabled ?? readonlyContext

    const contents = <><div className="databox-period">
        {props.children}
        <InputNumber readOnly={readonly} className="datebox-number" type="number" min={0} max={units[relativeDate.period.unit].max} value={relativeDate.period.number} onChange={changePeriod} />
        <Select className="datebox-unit" disabled={readonly} dropdownClassName="date-option" suffixIcon={icns.down} defaultValue={relativeDate.period.unit} onChange={changeUnit} >
            <Select.Option style={hideIfSelected('days')} className="datebox-option" value="days">{t("campaign.date.days")}</Select.Option>
            <Select.Option style={hideIfSelected('months')} className="datebox-option" value="months">{t("campaign.date.months")}</Select.Option>
            <Select.Option style={hideIfSelected('weeks')} className="datebox-option" value="weeks">{t("campaign.date.weeks")}</Select.Option>
        </Select>
        <Select className="datebox-direction" disabled={readonly} dropdownClassName="date-option" suffixIcon={icns.down} defaultValue={relativeDate.period.direction} onChange={changeDirection} >
            <Select.Option style={hideIfSelected('before')} className="datebox-option" value="before">{t("campaign.date.before")}</Select.Option>
            <Select.Option style={hideIfSelected('after')} className="datebox-option" value="after">{t("campaign.date.after")}</Select.Option>
        </Select></div>

        {showTargets &&

            <div className="datebox-target">

                <VSelectBox light className="datebox-target-value" disabled={readonly} validation={props.validation} placeholder={t("campaign.date.target.placeholder")}

                    options={targets?.filter(t => t.properties.relatable).sort((a, b) => a.type === campaignType && a.type !== b.type ? 1 : 0)!}

                    renderOption={target => <EventInstanceLabel noLink combined instance={target} />}
                    searchOption={stringifyTargets}
                    optionId={target => target.id}
                    lblTxt={e => {
                        const event = l(events.lookup(e.source)?.name)

                        let target = ""

                        switch (e.type) {
                            case campaignType: target = l(campaignstore.lookup(e.target)?.name); break;
                            case requirementType: target = l(requirements.lookup(e.target)?.name); break;
                            case productType: target = l(products.lookup(e.target)?.name); break;
                            default: target = ""; break;
                        }
                        return `${event} ${target}`
                    }}
                    onChange={changeTarget}>

                    {relativeDate ? [relativeDate.target] : []}

                </VSelectBox>
            </div>
        }

        {absoluteDate &&

            <div ref={onShowTarget} className='datebox-preview'>
                <div className="preview-text">{t("campaign.date.preview")}</div>
                <div className="preview-date">{absoluteDate}</div>
                <div className="preview-icon">{icns.calendar}</div>
            </div>
        }
    </>

    return asFormField ? <Field {...rest} className="datebox">
        <div className="datebox datebox-frame"><div className="datebox-relative">{contents}</div></div>
    </Field> : contents

}