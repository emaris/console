import { useT } from '#app/intl/api'
import { ValidationStatus } from '#app/utils/validation'
import { campaignStartEvent } from '#campaign/constants'
import { useCurrentCampaign } from '#campaign/hooks'
import moment from 'moment-timezone'
import { useMemo } from 'react'
import { EventInstance } from './model'
import { useEventInstanceStore } from './store'



export const useBackdatingPredicate = () => {

    const t = useT()

    const campaign = useCurrentCampaign()

    const eventinst = useEventInstanceStore().on(campaign)

    const allinstances = campaign ? eventinst.all() : []


    // eslint-disable-next-line
    const startDate = useMemo(() => campaign ? eventinst.startDate() : undefined, [allinstances])


    const self = {

        checkBackdated: (instance: EventInstance) => {

            return {
                predicate: () => {


                    if (instance.notifiedOn || !startDate)
                        return false

                    const referenceDate = instance.source === campaignStartEvent ? Date.now() : startDate

                    return moment(eventinst.absolute(instance.date)).isBefore(referenceDate)



                }, 
                
                status: 'warning' as ValidationStatus, 
                
                msg: () => t(`campaign.fields.date.backdated_${instance.source === campaignStartEvent ? 'start' : 'other'}`)
            }
        }

    }


    return self
}