import { campaignSingular, campaignType } from "#campaign/constants"
import { useCurrentCampaign } from "#campaign/hooks"
import { InstanceLabel, InstanceLabelProps } from "#campaign/InstanceLabel"
import { CampaignLabel } from "#campaign/Label"
import { InstanceRef, isInstanceRef } from "#campaign/model"
import { useCampaignStore } from '#campaign/store'
import { eventRoute } from "#event/constants"
import { EventLabel } from "#event/Label"
import { Tooltip } from "antd"
import { UnknownLabel } from "#app/components/Label"
import { icns } from "#app/icons"
import { useT } from '#app/intl/api'
import { useTime } from "#app/time/api"
import React from "react"
import { EventInstance, useEventInstanceModel } from "./model"
import { useEventInstanceProfile } from './profile'
import { useEventInstanceDates, useEventInstanceStore } from './store'

type Props = InstanceLabelProps & {

    instance: string | EventInstance | InstanceRef | undefined
    combined?: boolean
    noDate?: boolean
}

export const EventInstanceLabel = (props: Props) => {

    const t = useT()

    const time = useTime()
    const campaignstore = useCampaignStore()

    const eventinststore = useEventInstanceStore()
    const eventinstmodel = useEventInstanceModel()
    const eventinstprofile = useEventInstanceProfile()
    const eventinstdates = useEventInstanceDates()

    // if we received an id, we look in context for a campaign.
    const currentcampaign = useCurrentCampaign()

    const { instance, combined, noDate, decorations = [], noDecorations, linkTarget, ...rest } = props

    if (!instance)
     return <UnknownLabel {...props} />

    // if we have full instance we resolve its campaign, otheriwise use current one.
    const campaign = typeof instance === 'string' ? currentcampaign : campaignstore.lookup(instance?.campaign)

    if (!campaign)
        return <UnknownLabel title={t("common.labels.unknown_one", { singular: t(campaignSingular).toLowerCase() })} />

    const instances = {...eventinststore.on(campaign), ...eventinstmodel.on(campaign), ...eventinstprofile.on(campaign), ...eventinstdates.on(campaign)}

    // resolve instance if we don't have it already.
    var eventinstance = instance as EventInstance;

    if (isInstanceRef(instance))

        if (campaignstore.isLoaded(instance.campaign))
            eventinstance = instances.lookupLineage(instance)!
        else
            return <CampaignLabel campaign={instance.campaign} />

    else
     if (typeof instance === 'string')
        eventinstance = instances.lookup(instance)!

    if (!eventinstance)
     return <UnknownLabel />

    if (!noDecorations && !noDate && eventinstance.date)
        decorations.push(<Tooltip title={time.format(instances.absolute(eventinstance.date), 'long')}>{icns.bell}</Tooltip>)

    const currentcampaignEvent = currentcampaign && currentcampaign.id === eventinstance.campaign && eventinstance.type === campaignType

    const route = instances.route(eventinstance)

    const instanceLabel = props => <EventLabel {...rest} {...props} decorations={[...decorations, ...props.decorations]}
        mode={combined ? 'light' : undefined}
        linkTo={() => rest.linkTo || route} linkTarget={linkTarget ?? campaignType} event={eventinstance?.source} />

    const usecombined = combined && !currentcampaignEvent && eventinstance.type

    return <InstanceLabel
        instance={eventinstance}
        baseRoute={eventRoute}
        {...rest}
        sourceLabel={sourceprops => usecombined ?

            <>
                {instanceLabel(sourceprops)} &nbsp;|&nbsp; { instances.given(eventinstance).labelOf(eventinstance.target, { mode: 'light', noOptions: props.noOptions, noDecorations:props.noDecorations, noLink: props.noLink, linkTo: rest.linkTo, noIcon: true })}
            </>
            :
            instanceLabel(sourceprops)} />



}