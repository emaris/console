import { NoSuchRoute } from "#app/components/NoSuchRoute";
import { eventType } from "#event/constants";
import { campaignRoute } from "#campaign/constants";
import { CampaignValidation } from "#campaign/validation";
import * as React from "react";
import { Route, Switch } from "react-router-dom";
import { EventInstanceList } from "./List";


type Props = {

    report: ReturnType<CampaignValidation['validateEvents']>
}


export const EventInstances = (props:Props)=> 

            <Switch>
                <Route exact path={`${campaignRoute}/:name/${eventType}`} render={()=><EventInstanceList {...props}/>} />
                <NoSuchRoute /> 
            </Switch>