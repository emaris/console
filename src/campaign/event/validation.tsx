
import { useT } from '#app/intl/api'
import { useTagStore } from '#app/tag/store'
import { useTime } from '#app/time/api'
import { ValidationCheck, check, checkIt, empty, notdefined, provided, withReport } from "#app/utils/validation"
import { campaignType } from "#campaign/constants"
import { Campaign } from "#campaign/model"
import { eventSingular, eventType } from "#event/constants"
import moment from 'moment'
import { EventDate, EventInstance, RelativeDate, isRelative } from "./model"
import { ValidationProfile, useEventInstanceProfile } from "./profile"
import { useEventInstanceStore } from './store'
import { useBackdatingPredicate } from './utils'

export const useEventInstanceValidation = () => {

    const t = useT()

    const instprofile = useEventInstanceProfile()
    const inststore = useEventInstanceStore()

    const tags = useTagStore()
    const time = useTime()

    const singular = t(eventSingular).toLowerCase()


    const {checkBackdated} = useBackdatingPredicate()

    return {
        on: (c: Campaign) => ({


            validateInstance: (edited: EventInstance, profiles: ValidationProfile) => {

                const { validateCategories } = tags


                const eventInstances = inststore.on(c)

                const profile = profiles[edited.type || campaignType]
                const targetProfile = instprofile.on(c).given(edited)


                const targetExists: ValidationCheck<any> = {

                    predicate: target => edited.type === eventType ? !eventInstances.lookup(target) : !profile.allSources[target],
                    msg: () => t("campaign.feedback.missing")
                }

                return withReport({

                    note: checkIt().nowOr(t("campaign.fields.note.msg"))

                    ,

                    target: check(edited.target!)
                        .with(provided(!!edited.type, empty(t)))
                        .with(provided(!!edited.type, targetExists))
                        .nowOr(t("common.fields.target.msg", { singular: t(targetProfile.typeName().singular).toLowerCase() }))


                    ,

                    lineage: checkIt().nowOr(t("common.fields.lineage.msg_nochoice", { singular }))

                    ,

                    source: checkIt().nowOr(t("campaign.fields.source.msg", { singular }))

                    ,

                    relatable: checkIt().nowOr(
                        t("event.fields.relatable.msg"),
                        t("event.fields.relatable.help", { singular }),
                    )

                    ,

                    notified: checkIt().nowOr(
                        edited.notifiedOn ? t("event.fields.notified.msg_notified", { relative: moment(edited.notifiedOn).from(time.current()) })
                            : t("event.fields.notified.msg_unnotified"),
                        t("event.fields.notified.help", { singular }),
                    ),

                    date: 
                    
                        check(edited.date)
                    
                        .with(notdefined(t), { status: 'warning' })
                        .with(provided(edited.type !== eventType && isRelative(edited.date), { ...notdefined(t), predicate: (d: EventDate) => notdefined(t).predicate((d as RelativeDate).target) }))
                        .with(checkBackdated(edited))
                        .nowOr(isRelative(edited.date) ? t("campaign.fields.date.msg") : t("common.fields.date.msg"))


                    ,

                    ...validateCategories(edited.tags).for(eventType)

                })

            }
        })
    }
}
