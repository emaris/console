
import { Button } from "#app/components/Button"
import { SelectBox } from "#app/form/SelectBox"
import { SimpleBox } from "#app/form/SimpleBox"
import { icns } from "#app/icons"
import { useT } from '#app/intl/api'
import { DateBox } from "#campaign/event/DateBox"
import { EventInstanceLabel } from "#campaign/event/Label"
import { EventInstance, isTemporal } from "#campaign/event/model"
import { useEventInstanceProfile } from '#campaign/event/profile'
import { useEventInstanceValidation } from '#campaign/event/validation'
import { useCurrentCampaign } from "#campaign/hooks"
import { EventLabel } from "#event/Label"
import { eventSingular } from "#event/constants"
import { Event } from "#event/model"
import { Tooltip } from 'antd'
import { Fragment } from 'react'
import "./styles.scss"
import { useBackdatingPredicate } from './utils'

type Props = {


    templates?: Event[]

    instance: EventInstance

    //  the event co-edited along with this one.
    //  passed to DateBox as an overlay to resolve dates relatively to values not yet stored.
    context: EventInstance[]

    relativeDateTarget: string | undefined
    isNew?: boolean

    onAddTemporal?: () => void

    onChange: (_: EventInstance) => void

    warn?: boolean

}

export const EventCard = (props: Props) => {

    const t = useT()

    const campaign = useCurrentCampaign()

    const instances = { ...useEventInstanceValidation().on(campaign), ...useEventInstanceProfile().on(campaign) }

    const { instance, context, isNew, onChange, relativeDateTarget, onAddTemporal, templates = [], warn = false } = props

    const report = () => instances.validateInstance(instance, instances.validationProfile())

    const backdatingCheck = useBackdatingPredicate().checkBackdated(instance)

    const backdatingMessage = backdatingCheck.predicate() ? backdatingCheck.msg() : null


    const contents = <div>

        <div style={{ display: 'flex', alignItems: 'canter' }}>

            <div style={{flexGrow:1, marginRight:5}}>

                {isNew && templates.length > 1 ?

                    <SelectBox<string> standalone

                        placeholder={`${t("common.buttons.select_one", { singular: t(eventSingular) }).toLowerCase()}...`}
                        getlbl={c => <EventLabel noLink event={c} />}
                        selectedKey={instance.source}
                        onChange={id => onChange({ ...instance, source: id })}>

                        {templates.map(t => t.id)}

                    </SelectBox>

                    :

                    <SimpleBox light>
                        <EventInstanceLabel noLink noDate instance={instance} />
                    </SimpleBox>
                }

            </div>

            <Tooltip title={t( instance.notifiedOn? 'campaign.fields.date.already_notified':'campaign.fields.date.not_notified')} >
            <div style={{fontSize:18,color: instance.notifiedOn ? 'lightseagreen' : 'darkgray'}}>
                {icns.mail}
            </div>
            </Tooltip>


        </div>


        <div className="card-date">

            <DateBox validation={{ ...report().date, msg: undefined }}
                fixedTarget={relativeDateTarget}
                event={instance}
                overlay={context}
                showTime
                onChange={date => onChange({ ...instance, date })} />

        </div>

    </div>

    return <Fragment>
        <div className={`event-card ${isTemporal(instance) ? 'temporal-card' : ''} ${warn ? 'warn' : ''} `}>
            <div className={`card-name`}>
                {contents}

                {onAddTemporal &&

                    <div className="card-add-related" >
                        <Button iconLeft icn={icns.add} size="small" onClick={() => onAddTemporal()}>{t('campaign.buttons.add_related')}</Button>
                    </div>
                }
            </div>

        </div>
        <div className={`card-error-message message-${backdatingMessage ? 'show' : 'hide'}`}>{backdatingMessage}</div>
    </Fragment>
}