import { campaignPlural, campaignSingular, campaignType } from "#campaign/constants";
import { CampaignLabel } from "#campaign/Label";
import { Campaign, CampaignInstance } from "#campaign/model";
import { eventPlural, eventSingular, eventType } from "#event/constants";
import { useEventStore } from '#event/store';
import { productPlural, productSingular, productType } from "#product/constants";
import { ProductLabel } from "#product/Label";
import { useProductStore } from '#product/store';
import { requirementPlural, requirementSingular, requirementType } from "#requirement/constants";
import { RequirementLabel } from "#requirement/Label";
import { useRequirementStore } from '#requirement/store';
import { useL } from "#app/model/multilang";
import * as React from "react";
import { ProductInstanceLabel } from "../product/Label";
import { useProductInstances } from "../product/api";
import { RequirementInstanceLabel } from "../requirement/Label";
import { useRequirementInstances } from "../requirement/api";
import { EventInstanceLabel } from "./Label";
import { EventInstance } from "./model";
import { useEventInstanceStore } from "./store";

export type TargetProfile = {

    // resolves the target and returns a string for comparisons and filtering.
    name: () => string | undefined
    typeName: () => { singular: string, plural: string }
    lookup: (target?: string) => CampaignInstance | Campaign | undefined

    // all possible values of the target type, and their rendering as options.
    // used to select one in detail form.
    allTargets: () => string[]
    optionOf: (target: string | undefined) => JSX.Element | undefined

    // renders a target.
    labelOf: (target: string | undefined, props?) => JSX.Element | undefined
}




export type ValidationProfile = {

    [type: string]: { allSources: { [sourceid: string]: CampaignInstance | Campaign } }


}


export const useEventInstanceProfile = () => {

    const l = useL()

    const store = useEventStore()

    const reqstore = useRequirementStore()
    const prodstore = useProductStore()

    const reqinststore = useRequirementInstances()
    const prodinststore = useProductInstances()
    const eventinststore = useEventInstanceStore()

    return {

        on: (c: Campaign) => {


            const self = {


                validationProfile: (): ValidationProfile => ({


                    [productType]: {
                        allSources: (prodinststore.on(c).all() ?? []).reduce((acc, i) => ({ ...acc, [i.source]: i }), {})
                    }

                    ,

                    [eventType]: { allSources: (eventinststore.on(c).all() ?? []).reduce((acc, i) => ({ ...acc, [i.source]: i }), {}) }

                    ,

                    [requirementType]: { allSources: (reqinststore.on(c).all() ?? []).reduce((acc, i) => ({ ...acc, [i.source]: i }), {}) }

                    ,

                    [campaignType]: { allSources: { [c.id]: c } }


                })
                ,

                givenType: (type: string | undefined, target?: string): TargetProfile => {

                    var impl

                    switch (type) {

                        case requirementType: {


                            impl = {


                                name: () => l(reqstore.safeLookup(target)?.name),
                                typeName: () => ({ singular: requirementSingular, plural: requirementPlural }),

                                lookup: (target) => reqinststore.on(c).all().find(i => i.source === target),

                                allTargets: () => reqinststore.on(c).all().map(i => reqstore.safeLookup(i.source)).filter(i => i).map(i => i!.id),
                                optionOf: id => <RequirementLabel mode="light" noLink noDecorations requirement={id} />,

                                labelOf: (target, props) => {
                                    const reqInst = impl.lookup(target)
                                    const req = reqstore.safeLookup(reqInst?.source ?? '')
                                    const tip = !!req?.properties?.version
                                    return <RequirementInstanceLabel {...props} instance={reqInst} noTip={!tip} />
                                }

                            }

                            return impl
                        }

                        // temporals are different: have other event instances as their targets (instance ids, not template ids).
                        case eventType:

                            {
                                impl = {

                                    // name of template plus name of target instance, which will be about the name of its target campaign,requirement,or product.
                                    name: () => {

                                        const targetInstance = eventinststore.on(c).lookup(target)

                                        return targetInstance ? `${l(store.lookup(targetInstance.source)?.name)} ${self.given(targetInstance).name()}` : ''

                                    },
                                    typeName: () => ({ singular: eventSingular, plural: eventPlural }),
                                    lookup: (target) => eventinststore.on(c).all().find(i => i.id === target),

                                    // temmporals cannot target other temporals (eg. reminder of a reminder)
                                    allTargets: () => eventinststore.on(c).all().filter(i => i.type !== eventType).map(i => i.id),
                                    optionOf: id => <EventInstanceLabel mode="light" combined noLink noDecorations noOptions instance={id} />,

                                    labelOf: (target, props) => <EventInstanceLabel {...props} combined instance={target} />
                                }

                                return impl
                            }

                        case productType: {

                            impl = {

                                name: () => l(prodstore.safeLookup(target)?.name),
                                typeName: () => ({ singular: productSingular, plural: productPlural }),

                                allTargets: () => prodinststore.on(c).all().map(i => prodstore.safeLookup(i.source)).filter(i => i).map(i => i!.id),
                                optionOf: id => <ProductLabel mode="light" noLink product={id} />,
                                lookup: (target) => prodinststore.on(c).all().find(i => i.source === target),

                                labelOf: (target, props) => {
                                    const prodInst = impl.lookup(target)
                                    const prod = prodstore.safeLookup(prodInst?.source ?? '')
                                    const tip = !!prod?.properties?.version
                                    return <ProductInstanceLabel {...props} instance={prodInst} noTip={!tip} />
                                }
                            }

                            return impl
                        }
                        case campaignType:
                        default:


                            impl = {

                                name: () => l(c.name),
                                typeName: () => ({ singular: campaignSingular, plural: campaignPlural }),
                                lookup: () => c,

                                allTargets: () => [c.id],
                                optionOf: id => <CampaignLabel mode="light" noLink campaign={id} />,

                                labelOf: (_, props) => <CampaignLabel {...props} campaign={c} />
                            }

                            return impl
                    }

                }


                ,

                given: (instance: EventInstance | string): TargetProfile => {

                    const resolved = typeof instance === 'string' ? eventinststore.on(c).lookup(instance)! : instance

                    return self.givenType(resolved.type, resolved.target)

                }

            }

            return self;


        }
    }
}
