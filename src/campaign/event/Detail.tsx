
import { Button } from "#app/components/Button"
import { Drawer } from "#app/components/Drawer"
import { useConfig } from '#app/config/state'
import { Form } from "#app/form/Form"
import { NoteBox } from "#app/form/NoteBox"
import { SimpleBox } from "#app/form/SimpleBox"
import { Switch } from "#app/form/Switch"
import { VSelectBox } from "#app/form/VSelectBox"
import { useFormState } from "#app/form/hooks"
import { icns } from "#app/icons"
import { useT } from '#app/intl/api'
import { useLocale } from "#app/model/hooks"
import { Topbar } from "#app/scaffold/Topbar"
import { TagBoxset } from "#app/tag/TagBoxset"
import { tenantIcon } from "#app/tenant/constants"
import { useLogged } from '#app/user/store'
import { campaignType } from "#campaign/constants"
import { DateBox } from "#campaign/event/DateBox"
import { EventInstanceLabel } from "#campaign/event/Label"
import { EventInstance, isRelative, useEventInstanceModel } from "#campaign/event/model"
import { useEventInstanceProfile } from '#campaign/event/profile'
import { useEventInstanceDates, useEventInstanceStore } from '#campaign/event/store'
import { useEventInstanceValidation } from '#campaign/event/validation'
import { useCurrentCampaign } from "#campaign/hooks"
import { useCampaignStore } from '#campaign/store'
import { EventLabel } from "#event/Label"
import { eventType } from "#event/constants"
import { useEventStore } from '#event/store'
import { productType } from "#product/constants"
import { useProductStore } from '#product/store'
import { requirementType } from "#requirement/constants"
import { useRequirementStore } from '#requirement/store'
import moment from 'moment'




type Props = {

    detail: EventInstance
    onClose: () => void

    width?: number
}

export const EventInstanceDetail = (props: Props) => {

    const t = useT()
    const { l } = useLocale()

    const { width = 700, detail, onClose } = props

    const campaign = useCurrentCampaign()
    const store = useEventStore()

    const readOnly = campaign.guarded

    const instances = {
        ...useEventInstanceStore().on(campaign),
        ...useEventInstanceModel().on(campaign),
        ...useEventInstanceProfile().on(campaign),
        ...useEventInstanceValidation().on(campaign),
        ...useEventInstanceDates().on(campaign)
    }

    const config = useConfig()

    const logged = useLogged()

    const event = store.lookup(detail.source)!

    const campaignstore = useCampaignStore()
    const products = useProductStore()
    const requirements = useRequirementStore()

    const formstate = useFormState(detail)

    const { edited, dirty, initial, change, reset } = formstate

    const onSave = () => instances.save(edited).then(onClose)

    const report = instances.validateInstance(edited, instances.validationProfile())

    const revert = <Button icn={icns.revert} enabled={dirty} onClick={() => reset()}>{t("common.buttons.revert")}</Button>
    const saveact = <Button icn={icns.save} disabled={false && report.errors() > 0} enabled={dirty} type="primary" onClick={onSave}>{t("common.buttons.save")}</Button>
    const removeact =
        <Button
            key="remove"
            type="danger"
            disabled={readOnly}
            icn={icns.remove}
            onClick={() => instances.remove(edited, onClose, true)}>
            {t("common.buttons.remove")}
        </Button>

    const profile = edited.type && instances.given(edited)

    
    return <Drawer renderAfterTransition routeId={instances.detailParam()} visible width={width} warnOnClose={dirty} title={`${l(event?.name!)}`} icon={tenantIcon} onClose={onClose}>

        <Topbar autoGroupButtons={false} offset={63}>
            {removeact}
            {saveact}
            {revert}
        </Topbar>

        <Form state={formstate} >

            {profile &&

                <VSelectBox label={t("common.fields.target.name")} validation={report.target}
                    options={instances.possibleTargetsFor(event, instances.givenType(event.type).allTargets())}
                    renderOption={id => profile.optionOf(id)}
                    lblTxt={e => {
                        const resolved = instances.lookup(e)!
                        const event = l(store.lookup(resolved.source)!.name)
                        let target = ""

                        switch (resolved.type) {
                            case campaignType: target = l(campaignstore.lookup(resolved.target)?.name); break;
                            case requirementType: target = l(requirements.lookup(resolved.target)?.name); break;
                            case productType: target = l(products.lookup(resolved.target)?.name); break;
                            default: target = ""; break;
                        }

                        return `${event} ${target}`
                    }}
                    onChange={change((t, id) => {

                        const currentTarget = t.target

                        t.target = id

                        // if there's a date relative to same target, syncs the change.
                        if (isRelative(t.date) && (t.date.target === currentTarget || !t.date.target))
                            t.date.target = id

                    })}>

                    {[edited.target]}

                </VSelectBox>
            }


            <DateBox label={t("common.fields.date.name")}
                validation={report.date}
                fixedTarget={edited.type === eventType ? edited.target : undefined}
                event={edited}
                showTime
                onChange={change((t, v) => t.date = v)} />


            <TagBoxset edited={edited.tags} type={eventType} validation={report} onChange={change((t, v) => t.tags = v)} />

            <NoteBox label={t("common.fields.note.name")} validation={report.note} autoSize={{ minRows: 4, maxRows: 6 }} onChange={change((t, v) => t.properties.note = t.properties.note ? { ...t.properties.note, [logged.tenant]: v } : { [logged.tenant]: v })} >
                {edited.properties.note}
            </NoteBox>

            <SimpleBox label={t("campaign.fields.source.name")} validation={report.source} >
                <EventLabel event={edited.source} />
            </SimpleBox>

            <Switch label={t("event.fields.relatable.name")} onChange={
                change((u, v) => u.properties.relatable = v ? v : initial.properties?.relatable)
            }
                validation={report.relatable}>
                {edited.properties.relatable}
            </Switch>


            <Switch disabled={!config.devmode() && !!initial.notifiedOn && moment(instances.absolute(initial.date)).isBefore(moment.now())} label={t("event.fields.notified.name")} validation={report.notified} onChange={
                change((u, v) => u.notifiedOn = v ? initial.notifiedOn ?? moment().format() : undefined)
            }>
                {edited.notifiedOn != null}
            </Switch>


            {edited.lineage &&

                <SimpleBox label={t("common.fields.lineage.name")} validation={report.lineage} >
                    <EventInstanceLabel showCampaign instance={edited.lineage} />
                </SimpleBox>
            }

        </Form>

    </Drawer>
}
