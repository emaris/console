
import { useT } from '#app/intl/api';
import { defaultLanguage } from "#app/intl/model";
import { useToggleBusy } from "#app/system/api";
import { useTagModel } from '#app/tag/model';
import { compareDates, indexMap, through } from "#app/utils/common";
import { notify, showAndThrow, useFeedback } from "#app/utils/feedback";
import { paramsInQuery, updateQuery } from "#app/utils/routes";
import { campaignEndEvent, campaignStartEvent, campaignType, productDeadlineEvent, requirementDeadlineEvent } from "#campaign/constants";
import { CampaignContext, CampaignNext } from '#campaign/context';
import { Campaign, CampaignInstance, InstanceRef } from "#campaign/model";
import { ProductInstance } from '#campaign/product/api';
import { RequirementInstance } from '#campaign/requirement/api';
import { useCampaignRouting } from '#campaign/store';
import { useEventClient } from '#event/client';
import { eventPlural, eventSingular, eventType } from "#event/constants";
import { Event, EventDto } from "#event/model";
import { useEventStore } from '#event/store';
import { partition } from "lodash";
import moment from 'moment-timezone';
import { useContext } from 'react';
import { useEventInstanceCalls } from './calls';
import { AbsoluteDate, EventDate, EventInstance, RelativeDate, asDuration, isRelative, isTemporal, useEventInstanceModel } from "./model";
import { useEventInstanceProfile } from './profile';



export type EventInstanceState = {

    events: {

        all: EventInstance[]
        map: Record<string, EventInstance>

        startDate: string | undefined
        endDate: string | undefined
    }

}

export const initialEventInstances = (): EventInstanceState => ({

    events: {
        all: undefined!,
        map: undefined!,
        startDate: undefined,
        endDate: undefined
    }
})


export const useEventInstanceStore = () => {

    const state = useContext(CampaignContext)

    const t = useT()

    const tags = useTagModel()

    const store = useEventStore()
    const client = useEventClient()
    const calls = useEventInstanceCalls()
    const instancemodel = useEventInstanceModel()

    const campaignrouting = useCampaignRouting()
    
    const toggleBusy = useToggleBusy()
    const fb = useFeedback()

    const type = "eventinstance"
    const [singular, plural] = [t(eventSingular).toLowerCase(), t(eventPlural).toLowerCase()]

    return {

        on: (c: Campaign) => {


            // 'function alias' avoid the init cost of apis that end up not being used.
            // this can add up in tight loops (e.g. list sorting)
            const call = calls.on(c)
            const model = instancemodel.on(c)


            const self = {

                ...model,


                livePush: (instances: EventInstance[], type: 'change' | 'remove') => {


                    const map = state.get().campaigns.instances[c.id]?.events?.map ?? {}
                    let current = state.get().campaigns.instances[c.id]?.events?.all ?? []

                    instances.forEach(instance => {

                        switch (type) {

                            case "change":
                                current = map[instance.id] ? current.map(i => i.id === instance.id ? instance : i) : [...current, instance]
                                break;

                            case "remove": current = current.filter(i => i.id !== instance.id); break

                        }

                        self.setAll(current)

                    })
                }

                ,


                //  turns all dates into absolute dates, following relative date targets recursively.
                //  optionally, EventDatelves targets against an overlay before using the global state.
                absolute: (date: EventDate | string, overlay?: EventInstance[]): string | undefined => {

                    const absoluteRec = (date: EventDate | string, visited: string[], overlay?: EventInstance[]): string | undefined => {


                        if (!date)
                            return undefined

                        if (typeof date === 'string')
                            return date

                        if (!isRelative(date))
                            return (date as AbsoluteDate).value

                        if (!(date?.period?.number >= 0))
                            return undefined

                        const target = overlay?.find(e => e.id === date.target) ?? self.lookup(date.target)

                        if (target && visited.includes(target.id))
                            return undefined;

                        // resolve target date 'recursively'.
                        const targetDate = absoluteRec(target?.date, target ? [target.id, ...visited] : visited, overlay)

                        if (!targetDate)
                            return undefined

                        return moment.parseZone(targetDate).add(asDuration(date.period)).format();

                    }

                    return absoluteRec(date, [], overlay)


                }

                ,

                setAll: (instances: EventInstance[], props?: { noOverwrite: boolean }) => state.set(s => {

                    const campaign = s.campaigns.instances[c.id] ?? {}

                    if (campaign.events?.all && props?.noOverwrite)
                        return

                    s.campaigns.instances[c.id] = {
                        ...campaign,
                        events: {
                            all: instances,
                            map: indexMap(instances).by(e => e.id),
                            // note: uses pass instances as overlay to absolute(), as we haven't committed changes just yet. 
                            startDate: instances.filter(e => e.source === campaignStartEvent).map(e => self.absolute(e.date,instances)).find(d => d),
                            endDate: instances.filter(e => e.source === campaignEndEvent).map(e => self.absolute(e.date,instances)).find(d => d)
                        }
                    }

                })


                ,

                // templates currently available to build events about a given instance.
                possibleTemplatesForInstance: (instance: CampaignInstance, all?: EventInstance[]): EventDto[] => {


                    return self.possibleTemplatesFor(instance.instanceType, instance.id, all)

                }
                ,

                // templates currently available to define events about a target of a given type.
                possibleTemplatesFor: (type: string, target: string, all?: EventInstance[]): EventDto[] => {

                    const instances = all ?? self.allAbout(target)

                    return store.allOf(type).filter(t => self.isPossibleTemplateFor(t, target, instances));
                }

                ,

                isPossibleTemplateForCampaignInstance: (template: Event, target: CampaignInstance, all?: EventInstance[]) =>

                    self.isPossibleTemplateFor(template, target.source, all)


                ,

                isPossibleTemplateForCampaign: (template: Event, target: Campaign, all?: EventInstance[]) =>

                    self.isPossibleTemplateFor(template, target.id, all)


                ,

                isPossibleTemplateFor: (template: Event, target: string, all?: EventInstance[]) => {

                    if (template.lifecycle.state === 'inactive')
                        return false

                    const instances = all ?? self.allAbout(target)

                    // target is a temporal? bizrule: cannot have temporal of a temporal => no templates available.
                    if (isTemporal(instances.find(ei => ei.id === target)))
                        return false

                    const maxCardinality = template.cardinality?.max

                    return !maxCardinality || instances.filter(ei => ei.source === template.id).length < maxCardinality
                }

                ,

                // targets currently available to define instances of a given template.
                possibleTargetsFor: (template: EventDto, targets: string[]): string[] => {

                    const maxCardinality = template?.cardinality?.max

                    if (!maxCardinality)
                        return targets

                    const current = self.all().filter(ei => ei.source === template.id)

                    // keeps only targets that don't have all the instances of this template that its cardinality allows.
                    return targets.filter(t => current.filter(ei => ei.target === t).length < maxCardinality)

                }

                ,

                addAll: (instances: EventInstance[]) =>

                    toggleBusy(`${type}.addall`, t("common.feedback.save_changes"))

                        .then(() => self.addAllSilently(instances))
                        .then(through(() => notify(t('common.feedback.saved'))))

                        .catch(e => showAndThrow(e, t("common.calls.add_many_error", { plural })))
                        .finally(() => toggleBusy(`${type}.addall`))



                ,

                //  adds instances and complements with managed temporals.
                addAllSilently: (instances: EventInstance[]) => {

                    if (instances.length === 0)
                        return Promise.resolve([])

                    // generates as many events as the initial cardinality of the template.
                    const temporals = instances.flatMap(i =>

                        self.managedTemplates().forEventInstance(i).flatMap(t =>
                            Array.from({ length: t.cardinality?.initial ?? 1 }).map(_ => model.generate(t, i.id))
                        ))


                    return call.addAll([...instances, ...temporals])
                        .then(added => [...self.all() ?? [], ...added])
                        .then(through(self.setAll))

                }

                ,

                managedTemplates: () => {

                    return {

                        forCampaign: (campaign: Campaign = c) =>
                            store.allOf(campaignType).filter(e => e.lifecycle.state !== 'inactive' && self.isManaged(e).forCampaign(campaign))
                        ,

                        forInstance: (instance: CampaignInstance) =>
                            store.allOf(instance.instanceType).filter(e => e.lifecycle.state !== 'inactive' && self.isManaged(e).forInstance(instance))
                        ,


                        forEventInstance: (instance: EventInstance) =>
                            store.allOf(eventType).filter(e => e.lifecycle.state !== 'inactive' && self.isManaged(e).forEventInstance(instance))
                        ,
                    }


                }

                ,

                isManaged: (template: Event) => {

                    return {

                        forCampaign: (campaign: Campaign) =>

                            template.managed && tags.given(campaign).matches(template.managedExpression)

                        ,

                        forInstance: (instance: CampaignInstance) =>

                            template.managed &&tags.given(instance).matches(template.managedExpression)

                        ,

                        forEventInstance: (instance: EventInstance) =>

                            //temporal of temporals not allowed
                            instance.type === eventType ? false

                                :

                                template.managed &&

                                (
                                    (template.managedTypes ?? []).length === 0 ||       // no type conditions.
                                    (instance.type && template.managedTypes?.includes(instance.type))
                                )

                    }
                }

                ,

                //  adds managed events about new instances.
                addManagedForInstances: (instances: CampaignInstance[]) =>

                    self.addAllSilently(
                        instances.flatMap(i => self.managedTemplates().forInstance(i)
                            .filter(t => self.isPossibleTemplateForCampaignInstance(t, i))
                            .map(t => model.generate(t, i.source)))
                    )



                ,

                //  adds managed events about new campaigns.
                addManagedForCampaign: () =>

                    self.addAllSilently(
                        self.managedTemplates().forCampaign()
                            .filter(t => self.isPossibleTemplateForCampaign(t, c))
                            .map(e => model.generate(e, c.id))
                    )

                ,

                cloneAll: (instances: EventInstance[], context: CampaignNext) => {

                    const clones = instances.map(i => model.clone(i, context))

                    // original instance => its clone
                    const lineagemap = indexMap(clones).by(ei => ei.lineage?.source)

                    // updates temporal target events to their clones
                    clones.filter(c => isTemporal(c) && c.target).forEach(c => {

                        c.target = lineagemap[c.target!].id

                    })

                    // updates target events of relative dates to their clones
                    clones.filter(c => isRelative(c.date) && c.date.target).forEach(c => {

                        const date = c.date as RelativeDate

                        date.target = lineagemap[date.target!].id

                    })

                    // sorts by dependencies to respect integrity constraints at the backend.
                    //clones.sort(model().dependencyComparator)

                    function verifyOrder(clones: EventInstance[]) {

                        const ids = {}

                        clones.forEach(c => {

                            // console.log(`checking ${c.id}`,c);

                            if ((isRelative(c.date) && c.date.target) && !ids[c.date.target]) {
                                console.log(c.id, "misses relative", c.date.target)
                                throw Error()
                            }

                            if ((isTemporal(c) && c.target) && !ids[c.target]) {
                                console.log(c.id, "misses target", c.target)
                                throw Error()
                            }

                            ids[c.id] = true

                            //console.log(Object.values(ids).length, ids)
                        })

                    }


                    const cloned = orderByDep(clones)

                    function orderByDep(clones: EventInstance[], base: EventInstance[] = [], added: Record<string, boolean> = {}, map?: Record<string, EventInstance>): EventInstance[] {

                        if (clones.length === 0)
                            return base

                        if (!map)
                            return orderByDep(clones, base, added, clones.reduce((acc, inst) => ({ ...acc, [inst.id]: inst }), {}))

                        const [next, ...rest] = clones

                        const newbase = [...base, ...chainOf(next)]

                        return orderByDep(rest, newbase, added, map)

                        function chainOf(next: EventInstance): EventInstance[] {

                            let chain = [] as EventInstance[]

                            if (added[next.id])
                                return chain

                            added[next.id] = true

                            if (isRelative(next.date) && next.date.target && !added[next.date.target])
                                chain = chainOf(map![next.date.target])

                            if (isTemporal(next) && next.target && !added[next.target])
                                chain = chainOf(map![next.target])

                            return [...chain, next]
                        }

                    }

                    verifyOrder(cloned)

                    return call.addAll(cloned.map(i => context.type === 'clone' ? { ...i, lineage: undefined } : context.type === 'branch' ? { ...i, notifiedOn: undefined } : i)).then(self.setAll)



                }



                ,

                areReady: () => !!self.all()

                ,

                fetchAll: (forceRefresh = false) =>

                    Promise.all([

                        client.fetchAll(),

                        self.areReady() && !forceRefresh ? Promise.resolve(self.all())

                            :

                            toggleBusy(`${type}.fetchAll.${c.id}`, t("common.feedback.load", { plural }))

                                .then(_ => console.log(`fetching events for ${c.name[defaultLanguage]}...`))
                                .then(call.fetchAll) // load template dependency in parallel
                                .then(through($ => self.setAll($, { noOverwrite: !forceRefresh })))


                                .catch(e => showAndThrow(e, t("common.calls.fetch_all_error", { plural })))
                                .finally(() => toggleBusy(`${type}.fetchAll.${c.id}`))

                    ])
                        .then(([, instances]) => instances)  // continue with instances only

                ,


                lookup: (id: string | undefined): EventInstance | undefined => id ? state.get().campaigns.instances[c.id]?.events?.map[id] : undefined

                ,

                lookupLineage: (ref: InstanceRef | undefined) => ref ? state.get().campaigns.instances[ref.campaign]?.events?.all.find(ei => ei.id === ref.source) : undefined

                ,

                detailParam: () => 'ei-drawer'

                ,

                // eslint-disable-next-line
                detailInRoute: () => paramsInQuery(location.search)[self.detailParam()]

                ,

                route: (p?: EventInstance) =>

                    // eslint-disable-next-line
                    `${campaignrouting.routeTo(c)}/${eventType}?${updateQuery(location.search).with(params => {

                        params[self.detailParam()] = p ? p.id : null
                        params['source'] = p?.source ?? null

                    })}`

                ,

                all: () => state.get().campaigns.instances[c.id]?.events?.all

                ,

                allAbout: (...targets: string[]) => {

                    const all = self.all() ?? []

                    return all.filter(e => e.target && targets.includes(e.target))
                }

                ,


                allAboutWithTemporals: (target: string) => {

                    const directEvents = self.allAbout(target)


                    return [...directEvents, ...directEvents.flatMap(e => self.allAbout(e.id))]


                }

                ,


                allOf: (type: string | undefined) => self.all().filter(e => e.type === type)


                ,


                startDate: () => state.get().campaigns.instances[c.id]?.events?.startDate

                ,

                endDate: () => state.get().campaigns.instances[c.id]?.events?.endDate

                ,

                save: (instance: EventInstance, replacementOf: EventInstance = instance) =>

                    toggleBusy(`${type}.updateOne`, t("common.feedback.save_changes"))

                        .then(_ => call.updateOne(instance))
                        .then(() => self.all().map(e => e.id === replacementOf.id ? instance : e))
                        .then(self.setAll)
                        .then(() => notify(t('common.feedback.saved')))

                        .catch(e => showAndThrow(e, t("common.calls.update_one_error", { singular })))
                        .finally(() => toggleBusy(`${type}.updateOne`))

                ,


                replaceAllAbout: (instances: EventInstance[], target: string): Promise<EventInstance[]> => {

                    const [updated = [], added = []] = partition(instances, i => !!self.lookup(i.id))
                    const changemap = updated.reduce((acc, next) => ({ ...acc, [next.id]: next }), {})

                    const allAbout = self.allAboutWithTemporals(target).map(ei => ei.id)

                    const removed = allAbout.filter(id => !changemap[id])

                    const changes = {
                        added, updated, removed
                    }

                    return toggleBusy(`${type}.updateOne`, t("common.feedback.save_changes"))

                        .then(_ => call.updateMany(changes))
                        .then(through(saved => self.setAll([...self.all().filter(ei => !allAbout.includes(ei.id)), ...saved])))

                        .catch(e => showAndThrow(e, t("common.calls.update_one_error", { singular })))
                        .finally(() => toggleBusy(`${type}.updateOne`))

                }

                ,

                remove: (instance: EventInstance, onConfirm?: (...args) => void, runOnConfirmBeforeDelete?: boolean) =>

                    fb.askConsent({

                        title: t('common.consent.remove_one_title', { singular }),
                        content: t("common.consent.remove_one_msg", { singular }),
                        okText: t("common.consent.remove_one_confirm", { singular }),

                        onOk: () => {

                            toggleBusy(`${type}.removeOne`, t("common.feedback.save_changes"))

                                .then(() => (onConfirm && runOnConfirmBeforeDelete) && onConfirm(instance))
                                .then(() => call.removeMany([instance]))
                                .then(() => self.removeWithTemporals([instance]))
                                .then(() => notify(t('common.feedback.saved')))
                                .then(() => (onConfirm && !runOnConfirmBeforeDelete) && onConfirm(instance))

                                .catch(e => showAndThrow(e, t("common.calls.remove_one_error", { singular })))
                                .finally(() => toggleBusy(`${type}.removeOne`))


                        }
                    })

                ,

                removeMany: (instances: EventInstance[], onConfirm?: () => void) =>

                    fb.askConsent({

                        title: t('common.consent.remove_many_title', { count: instances.length, plural }),
                        content: t("common.consent.remove_many_msg", { count: instances.length, plural }),
                        okText: t("common.consent.remove_many_confirm", { count: instances.length, plural }),
                        onOk: () =>

                            toggleBusy(`${type}.removeMany`, t("common.feedback.save_changes"))

                                .then(() => call.removeMany(instances))
                                .then(() => self.removeWithTemporals(instances))
                                .then(() => notify(t('common.feedback.saved')))
                                .then(through(() => onConfirm && onConfirm()))

                                .catch(e => showAndThrow(e, t("common.calls.remove_many_error", { plural })))
                                .finally(() => toggleBusy(`${type}.removeMany`))

                    })

                ,

                // note: cascading should not be required because cascade-removed temporals are pushed from backend.
                // however, we the push is synchronous at the backend and arrives before we eliminate top-level events.
                // so there is a race condition here that we can only handle by redoing the work at the frontend, for now.
                removeWithTemporals: (instances: EventInstance[]) => {

                    const ids = instances.map(i => i.id)

                    const associatedTemporalIds = self.allOf(eventType).filter(i => i.target && ids.includes(i.target))

                    const instancesAndTemporals = [...instances, ...associatedTemporalIds]

                    const instancesAndTemporalsIds = instancesAndTemporals.map(i => i.id)

                    self.setAll(self.all().filter(e => !instancesAndTemporalsIds.includes(e.id)))

                }


                ,

                deadlineOfRequirement: (r: RequirementInstance) => self.allAbout(r.source).find(ei => ei.source === requirementDeadlineEvent)


                ,

                allDueDatesByRequirementId: (): { [_: string]: string } => {


                    return self.all().filter(ei => ei.source === requirementDeadlineEvent)
                        .reduce((a, ei) => ({ ...a, [ei.target!]: self.absolute(ei.date) }), {})

                }

                ,

                deadlineDateOfRequirement: (r: RequirementInstance) => self.absolute(self.deadlineOfRequirement(r)?.date),

                deadlineOfProduct: (p: ProductInstance) => self.allAbout(p.source).find(ei => ei.source === productDeadlineEvent),

                isRequirementDue: (r: RequirementInstance) => self.isEventDue(self.deadlineOfRequirement(r)),

                isProductDue: (p: ProductInstance) => self.isEventDue(self.deadlineOfProduct(p)),

                isEventDue: (ei : EventInstance | undefined): boolean => {

                   return  ei?.date ? ei.date['none'] ? false : self.absolute(ei.date) !== undefined : true
                
                },

                allDueDatesByProductId: (): { [_: string]: string } => {


                    return self.all().filter(ei => ei.source === productDeadlineEvent)
                        .reduce((a, ei) => ({ ...a, [ei.target!]: self.absolute(ei.date) }), {})

                }

                ,

                deadlineDateOfProduct: (r: ProductInstance) => self.absolute(self.deadlineOfProduct(r)?.date)



            }

            return self

        }
    }
}


export const useEventInstanceDates = () => {

    const t = useT()
    const instancestore = useEventInstanceStore()
    const instanceprofile = useEventInstanceProfile()

    return {
        on: (c: Campaign) => {

            const store = instancestore.on(c);

            const self = {



                dateOf: (source: string, target?: string) =>

                    store.all().filter(e => e.source === source && (target === undefined || e.target === target)).map(e => store.absolute(e.date)).find(d => d)


                ,

                comparator: (o1: EventInstance, o2: EventInstance) => {

                    const profile = instanceprofile.on(c)

                    return (profile.given(o1).name() ?? '').localeCompare(profile.given(o2).name() ?? '') || self.comparator(store.lookup(o1.source)!, store.lookup(o2.source)!)


                }

                ,

                // sorts by date, but groups events with their temporals.
                temporalAwareDateComparator: (o1: EventInstance, o2: EventInstance) =>

                    self.temporalAwareDateComparatorOver(store.all())(o1, o2)


                ,

                // like above, but resolves temporals and dates using a given population rather than what's in state.
                temporalAwareDateComparatorOver: (all: EventInstance[]) => (o1: EventInstance, o2: EventInstance) => {

                    const lookup = target => all.find(e => e.id === target)!

                    const temporalLast = (o1: EventInstance, o2: EventInstance) => isTemporal(o1) ? isTemporal(o2) ? 0 : 1 : isTemporal(o2) ? -1 : 0

                    // unfold temporals to their targets,   
                    const oo1 = isTemporal(o1) ? lookup(o1.target) : o1
                    const oo2 = isTemporal(o2) ? lookup(o2.target) : o2

                    const value = self.dateComparatorOver(all)(oo1, oo2) ||
                        (oo1.id === oo2.id ? temporalLast(o1, o2) || self.dateComparatorOver(all)(o1, o2)
                            :
                            self.comparator(oo1, oo2)
                        )

                    return value

                }


                ,

                // like date comparator, but resolves date using a given population rather than what's in state.
                dateComparatorOver: (all: EventInstance[]) => (o1: EventInstance, o2: EventInstance, nullFirst: boolean = true) => compareDates(store.absolute(o1.date, all), store.absolute(o2.date, all), nullFirst)

                ,

                dateComparator: (o1: EventInstance, o2: EventInstance, nullFirst: boolean = true) => compareDates(store.absolute(o1.date), store.absolute(o2.date), nullFirst)

                ,

                // by date, but temporals last after other events
                biasedDateComparator: (o1: EventInstance, o2: EventInstance, nullFirst: boolean = true) =>

                    isTemporal(o1) ?

                        (isTemporal(o2) ?

                            // both temporals
                            self.dateComparator(o1, o2, nullFirst)

                            :

                            1)

                        : isTemporal(o2) ?

                            -1

                            :

                            // both non-temporal
                            self.dateComparator(o1, o2, nullFirst)


                ,

                duedateComparator: (dl1: EventInstance, dl2: EventInstance, nullFirst: boolean = false) => {

                    const d1 = store.absolute(dl1?.date)
                    const d2 = store.absolute(dl2?.date)

                    // compare as dates if we have at least one.
                    if (d1 || d2)
                        return compareDates(d1, d2, nullFirst)

                    // otheriwse compare the string translations (coordinates with label rendering)
                    const s1 = dl1 ? t("common.labels.unknown_due_date") : t("common.labels.no_due_date")
                    const s2 = dl2 ? t("common.labels.unknown_due_date") : t("common.labels.no_due_date")

                    return s1.localeCompare(s2)


                }

            }

            return self
        }
    }
}
