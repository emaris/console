import { Campaign } from "#campaign/model";
import { useCalls } from "#app/call/call";
import { domainService } from "../../constants";
import { EventInstance } from "./model";


export type Changes = {
    added: EventInstance[]
    updated: EventInstance[]
    removed: string[]
}

const events = `/eventinstance`


export const useEventInstanceCalls = () => {

    const { at } = useCalls()

    return {
        on: (c: Campaign) => {


            return {

                fetchAll: (): Promise<EventInstance[]> => at(`${events}/search`, domainService).post({ campaign: c.id })

                ,

                addAll: (instances: EventInstance[]): Promise<EventInstance[]> => at(events, domainService).post(instances)

                ,

                updateOne: (instance: EventInstance): Promise<void> => at(`${events}/${instance.id}`, domainService).put(instance)

                ,

                updateMany: (changes: Changes): Promise<EventInstance[]> => at(events, domainService).put(changes)

                ,

                removeMany: (instances: EventInstance[]): Promise<void> => at(`${events}/remove`, domainService).post(instances.map(i => i.id))

            }
        }
    }
}