
import { campaignType } from "#campaign/constants";
import { CampaignNext } from '#campaign/context';
import { Campaign, CampaignInstance, InstanceProperties } from "#campaign/model";
import { eventType } from "#event/constants";
import { Event, useEventModel } from "#event/model";
import { useEventStore } from '#event/store';
import { deepclone } from "#app/utils/common";
import moment from "moment-timezone";
import shortid from "shortid";

export type EventInstanceDto = CampaignInstance & {

    type: string
    target: string | undefined
    date: EventDate
    notifiedOn?: string
    properties: InstanceProperties & { relatable?: boolean }


}

export type EventDate = undefined | AbsoluteDate | RelativeDate

export type AbsoluteDate = {

    kind: 'absolute'        // backend model with type info to drive deserialsiation
    value: string
    none?: boolean
    branchType?: 'pinned' | 'recurring' | 'single'

}

export type RelativeDate = {

    kind: 'relative'        // backend model with type info to drive deserialsiation
    period: Period
    target: string

}

export type Period = {

    number: number,
    unit: 'months' | 'days' | 'weeks',
    direction: 'before' | 'after'
}

// event instances have client-side identifiers, so that the UI can create web of relatonships ahead of storage.
export const newId = () => `EVI-${shortid()}`

export const isRelative = (date: EventDate | undefined): date is RelativeDate => (date as RelativeDate)?.kind === 'relative'
export const isAbsolute = (date: EventDate | undefined): date is AbsoluteDate => (date as AbsoluteDate)?.kind === 'absolute'

export const absoluteOf = (date: string | undefined): AbsoluteDate | undefined => date ? ({ kind: 'absolute', value: date }) : undefined

export const isTemporal = (i: CampaignInstance | undefined): i is EventInstance => i ? i.instanceType === eventType && (i as EventInstance).type === eventType : false

export const asDuration = (p: Period) => moment.duration(p.direction === 'before' ? -p.number : p.number, p.unit)

// export const defaultRelativeDate = (target?: string): RelativeDate => ({ kind: 'relative', period: { number: 1, unit: 'months', direction: 'after' }, target: target! })

export type EventInstance = EventInstanceDto    // future-proofs divergence from exchange model, starts, aligned.

export const useEventInstanceModel = () => {

    const model = useEventModel()
    const store = useEventStore()

    return {

        on: (c: Campaign) => {

            const self = {


                defaultRelativeDate: (target?: string): RelativeDate => c.properties.defaultRelativeDate ?
                    { ...c.properties.defaultRelativeDate, target: target! } :
                    { kind: 'relative', period: { number: 1, unit: 'months', direction: 'after' }, target: target! }

                ,


                stringify: (i: EventInstance | undefined): string => i ? `${model.stringify(store.lookup(i.source))}` : ``

                ,

                generate: (event: Event, target?: string): EventInstance => {
                    const instance = {
                        instanceType: eventType,
                        id: newId(),
                        type: event.type,
                        target: target ?? (event.type === campaignType ? c.id : undefined),
                        source: event.id,
                        campaign: c.id,
                        date: event.type === eventType ? self.defaultRelativeDate(target) : undefined,
                        tags: deepclone(event.tags),
                        notified: false,
                        properties: {
                            note: deepclone(event.properties.note),
                            relatable: event.properties.relatable || false
                        }
                    }

                    // console.log("instantiating",event,"with",target, "returning", instance)

                    return instance
                }

                ,


                clone: (instance: EventInstance, context: CampaignNext): EventInstance => {

                    const id = newId();

                    const clone = deepclone(instance)

                    const date = context.type === 'branch' ?
                        instance.date?.kind === 'absolute' ?
                            instance.date.branchType === 'recurring' || instance.date.branchType === undefined ?
                                { ...clone.date, value: moment(instance.date.value).add(context.timeOffset || 0, 'years').format() } as AbsoluteDate //absolute recurring, then shift 
                                :
                                instance.date.branchType === 'pinned' ?
                                    clone.date // keep absolute pinned
                                    :
                                    undefined // branched not pinned and not absolute ... delete it

                            :
                            clone.date // branch but relative date. Keep it
                        :
                        clone.date // is a clone. keep it anyway


                    return {
                        ...clone,
                        id,
                        target: instance.type === campaignType ? c.id : instance.target,
                        campaign: c.id,
                        // date: isRelative(instance.date) ? deepclone(instance.date) : undefined, // blanks absolute dates
                        tags: context.type === 'branch' ? store.lookup(instance.source)?.tags ?? clone.tags : clone.tags,
                        date,
                        lineage: { source: instance.id, campaign: instance.campaign }
                    }

                }





            }

            return self

        }

    }
}

