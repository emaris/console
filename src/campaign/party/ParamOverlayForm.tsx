import { Drawer } from "#app/components/Drawer"
import { FormState } from "#app/form/hooks"
import { useT } from '#app/intl/api'
import { deepequals } from "#app/utils/common"
import { useRequirementInstances } from '#campaign/requirement/api'
import { useCampaignStore } from '#campaign/store'
import { ParameterPanel } from "#layout/parameters/panel"
import { useLayoutParameters } from '#layout/parameters/api'
import { predefinedParameterIds } from "#layout/parameters/constants"
import { paramIcon } from "#layout/parameters/model"
import { Product } from "#product/model"
import { Requirement } from "#requirement/model"
import { reqParam } from "./PartyProfile"
import { PartyInstance } from "./api"


type RequirementProps = {

    target: Requirement
    onClose: ()=>void
 
    state: FormState<PartyInstance> 
}

export const RequirementOverlaySection = (props:RequirementProps) => {

    const t = useT()
    
    const {target,onClose,state} = props

    const {edited,change} = state

    const plural = t('layout.parameters.plural')

    const {allParameters} = useLayoutParameters()

    const overlays = edited.properties.parameterOverlays.requirements[target.id] ?? {}

    // const parameters = allParameters().filter(p=>!predefinedParameterIds.includes(p.id) && !predefinedRequirementParameterIds.includes(p.id))
    const parameters = allParameters().filter(p=>!predefinedParameterIds.includes(p.id))

    const campaisgnstore = useCampaignStore().safeLookup(edited.campaign)

    const allRequirementInstances = useRequirementInstances().on(campaisgnstore).allForParty(edited).filter(ri => ri.source === target.id)

    const requirementInstanceOverlays = allRequirementInstances.length === 1 ? allRequirementInstances[0].properties.parameterOverlay ?? {} : {}
  
    const overlayed = parameters.map(p=>{
        
        const value = overlays[p.name]?.value ? overlays[p.name].value : requirementInstanceOverlays[p.name] ? requirementInstanceOverlays[p.name].value : p.value 
        return {...p,value }
    })

    const onChange =  change( (t,[i,v]) => {

        const overlays = t.properties.parameterOverlays.requirements
        const parameter = parameters[i]
        const exists = overlays[target.id]

        //eslint-disable-next-line
        const unchanged = deepequals(v,parameter.value) || v == parameter.value        // the lax equality here is intended to capture equality up to type conversions.

        if ( unchanged) {
            if ( overlays[target.id] &&  overlays[target.id][parameter.name])
                delete overlays[target.id][parameter.name]
            return
        }

        if (!exists)
            overlays[target.id]= {}       
        overlays[target.id][parameter.name]={value: v, original: parameter} 
    })

    return <Drawer routeId={reqParam} visible={true} width={400} warnOnClose={false}  title={plural} icon={paramIcon} onClose={onClose}>
                <div style={{height:"100%",display:"flex",flexDirection:"column"}}>
                    <ParameterPanel forInstance parameters={overlayed} onChange={(index,value)=> onChange([index,value]) }  />
                </div>
         </Drawer>
        
}


type ProductProps = {

    target: Product
    onClose: ()=>void
 
    state: FormState<PartyInstance> 
}

export const ProductOverlaySection = (props:ProductProps) => {

    const t = useT()
    
    const {target,onClose,state} = props

    const {edited,change} = state

    const plural = t('layout.parameters.plural')

    const {allParameters} = useLayoutParameters()

    const overlays = edited.properties.parameterOverlays.products[target.id] ?? {}

    // const parameters = allParameters().filter(p=>!predefinedParameterIds.includes(p.id) && !predefinedRequirementParameterIds.includes(p.id))
    const parameters = allParameters().filter(p=>!predefinedParameterIds.includes(p.id))

    const overlayed = parameters.map(p=>{
        const value = overlays[p.name]?.value ? overlays[p.name].value : p.value 
        return {...p,value }
    })

    const onChange =  change( (t,[i,v]) => {

        const overlays = t.properties.parameterOverlays.products
        const parameter = parameters[i]
        const exists = overlays[target.id]

        //eslint-disable-next-line
        const unchanged = deepequals(v,parameter.value) || v == parameter.value        // the lax equality here is intended to capture equality up to type conversions.
      
        if (unchanged) {
            if ( overlays[target.id] &&  overlays[target.id][parameter.name])
                delete overlays[target.id][parameter.name]
            return
        }

        if (!exists)
            overlays[target.id]= {}       
       
        overlays[target.id][parameter.name]={value: v, original: parameter} 
    })

    return <Drawer routeId={reqParam} visible={true} width={400} warnOnClose={false}  title={plural} icon={paramIcon} onClose={onClose}>
                <div style={{height:"100%",display:"flex",flexDirection:"column"}}>
                    <ParameterPanel forInstance parameters={overlayed} onChange={(index,value)=> onChange([index,value]) }  />
                </div>
         </Drawer>
        
}