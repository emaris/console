import { LabelProps, UnknownLabel } from "#app/components/Label"
import { icns } from "#app/icons"
import { useT } from '#app/intl/api'
import { TenantLabel } from "#app/tenant/Label"
import { InstanceLabel, InstanceLabelProps } from "#campaign/InstanceLabel"
import { CampaignLabel } from "#campaign/Label"
import { campaignSingular, campaignType, runningIcon } from "#campaign/constants"
import { InstanceRef, isInstanceRef } from "#campaign/model"
import { useCampaignStore } from '#campaign/store'
import { useRoutedTypes } from '#config/model'
import { useDashboard } from '#dashboard/api'
import { useCampaignMode } from "#dashboard/hooks"
import { Tooltip } from "antd"
import { Link } from "react-router-dom"
import { submissionType } from "../submission/constants"
import { PartyInstance, usePartyInstances } from "./api"
import { partyRoute } from "./constants"

type Props = LabelProps & InstanceLabelProps & {

    instance: PartyInstance | InstanceRef | undefined
}


export const PartyInstanceLabel = (props: Props) => {


    const t = useT()

    const { instance, linkTo, linkTarget, options = [], ...rest } = props

    const routedTypes = useRoutedTypes()
    const campaignstore = useCampaignStore()
    const dashboard = useDashboard()
    const campaignMode = useCampaignMode()
    const partyinst = usePartyInstances()

    const campaign = campaignstore.lookup(instance?.campaign)

    if (!campaign)
        return <UnknownLabel title={t("common.labels.unknown_one", { singular: t(campaignSingular).toLowerCase() })} />


    const parties = partyinst.on(campaign)

    var partyinstance = instance as PartyInstance;

    if (isInstanceRef(instance))

        if (campaignstore.isLoaded(instance.campaign))
            partyinstance = parties.lookupLineage(instance)!
        else
            return <CampaignLabel campaign={instance.campaign} />

    if (!partyinstance)
        return <UnknownLabel {...props} />

    if (campaignMode === 'dashboard' && routedTypes.includes(campaignType))
        options.push(<Tooltip title={t("campaign.labels.campaign_view.design")}><Link to={() => partyinst.on(campaign).route(partyinstance)}>{icns.edit}</Link></Tooltip>)

    if (campaignMode === 'design' && routedTypes.includes(submissionType))
        options.push(<Tooltip title={t("campaign.labels.campaign_view.live")}><Link to={() => dashboard.on(campaign).routeToParty(partyinstance)}>{runningIcon}</Link></Tooltip>)


    return <InstanceLabel instance={partyinstance}
        noMemo={props.noMemo}
        tooltipForLineage={ref => <PartyInstanceLabel showCampaign instance={ref} />}
        baseRoute={partyRoute}
        {...rest}
        sourceLabel={props => <TenantLabel {...rest} {...props} noMemo={props.noMemo} linkTarget={linkTarget ?? campaignType} options={options} linkTo={linkTo || (() => partyinst.on(campaign).route(partyinstance))} tenant={instance?.source} />}
    />

}