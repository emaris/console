
import { Form } from "#app/form/Form"
import { RadioBox } from "#app/form/RadioBox"
import { FormState, useFormState } from "#app/form/hooks"
import { useT } from '#app/intl/api'
import { SettingsForm } from "#app/settings/SettingsForm"
import { Validation } from "#app/utils/validation"
import { useCurrentCampaign } from "#campaign/hooks"
import { CampaignSettings, approveCycleOptionDefaults, approveCycleOptions } from "#campaign/model"
import { usePartyInstanceValidation } from '#campaign/party/validation'
import { Radio } from "antd"
import * as React from "react"
import { PartyInstance, usePartyInstances } from "./api"
import { usePartyInstanceClient } from './client'
import { useLogged } from "#app/user/store"


type LivePartySettingsProps = {

    party: PartyInstance
    onSave?: () => void

}

export const LivePartySettings = (props: LivePartySettingsProps) => {

    const { party, onSave = () => { } } = props

    const campaign = useCurrentCampaign()
    const parties = { ...usePartyInstanceClient().on(campaign), ...usePartyInstanceValidation().on(campaign) }

    const state = useFormState(party)

    const report = parties.validateInstance(state.edited)

    return <SettingsForm state={state} onSave={() => parties.save(state.edited).then(onSave)} >
        <PartySettingsFields state={state} report={report} />
    </SettingsForm>


}


type PartySettingsProps = {

    state: FormState<PartyInstance>
    report: Partial<Record<keyof CampaignSettings, Validation>>


}

export const PartySettings = (props: PartySettingsProps) => {


    const { state, report } = props


    return <Form state={state}>
        <PartySettingsFields state={state} report={report} />
    </Form>

}

type PartySettingsFieldsProps = {

    state: FormState<PartyInstance>
    report: Partial<Record<keyof CampaignSettings, Validation>>
}

export const PartySettingsFields = (props: PartySettingsFieldsProps) => {

    const t = useT()

    const { state, report: clientreport } = props

    const report =  clientreport as  ReturnType<ReturnType<Pick<ReturnType<typeof usePartyInstanceValidation>,'on'>['on']>['validateInstance']>
    
    const { edited, change } = state

    const isPartyUser = !useLogged().hasNoTenant()

    const campaign = useCurrentCampaign()
    const parties = usePartyInstances().on(campaign)

    const approvalCycleDefault = approveCycleOptionDefaults[parties.approveCycleDefaultFor(edited.source)]

    const approvalOptions = Object.keys(approveCycleOptions).map(o => <Radio key={o} value={o}>{t(approveCycleOptions[o])}.</Radio>)

    approvalOptions.push(<Radio key={"default"} value={'default'}>{t("campaign.fields.approval.options.default", { value: t(approvalCycleDefault)})}.</Radio>)

    return <React.Fragment>

        <RadioBox label={t("campaign.fields.approval.name")} validation={report.approveCycle}
            value={parties.approveCycle(edited,'nodelegation') ?? 'default'}
            onChange={change((t, v) => parties.setApproveCycle(t, v === 'default' ? undefined : v))}>

            {approvalOptions}

        </RadioBox>

        <RadioBox label={t("campaign.fields.bypassapproval.name")} validation={report.bypassSelfApproval}
            value={ parties.canBypassSelfApproval(edited, 'nodelegation') ?? 'default' }
            onChange={change( (t,v) => {  parties.setCanBypassSelfApproval(t,v==='default' ? undefined : v)})} >
                <Radio value={true}>{t("campaign.fields.bypassapproval.options.can")}.</Radio>
                <Radio value={false}>{t("campaign.fields.bypassapproval.options.cannot")}.</Radio>
                <Radio value={'default'}>{t("campaign.fields.bypassapproval.options.default",
                    {value:t(parties.canBypassSelfApprovalDefault(edited)?
                                        "campaign.fields.bypassapproval.options.can_short"
                                        : "campaign.fields.bypassapproval.options.cannot_short" )})}.
                </Radio>
        </RadioBox>

        {isPartyUser && <RadioBox label={t("campaign.fields.party_can_see_not_applicable.party.name")} validation={report.partyCanSeeNotApplicable}
            value={parties.canSeeNotApplicable(edited, 'nodelegation') ?? 'default'}
            onChange={change((t, v) => parties.setCanSeeNotApplicable(t, v === 'default' ? undefined : v))}>

            <Radio value={true}>{t("campaign.fields.party_can_see_not_applicable.party.options.can")}.</Radio>
            <Radio value={false}>{t("campaign.fields.party_can_see_not_applicable.party.options.cannot")}.</Radio>
            <Radio value={'default'}>{t("campaign.fields.party_can_see_not_applicable.party.options.default",
                {
                    value: t(parties.canSeeNotApplicableDefault() ?
                        "campaign.fields.party_can_see_not_applicable.party.options.can_short"
                        : "campaign.fields.party_can_see_not_applicable.party.options.cannot_short")
                })}.</Radio>
        </RadioBox>}

    </React.Fragment>

}