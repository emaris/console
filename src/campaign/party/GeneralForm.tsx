
import { PartyInstanceLabel } from "#campaign/party/Label"
import { Form } from "#app/form/Form"
import { FormState } from "#app/form/hooks"
import { NoteBox } from "#app/form/NoteBox"
import { SimpleBox } from "#app/form/SimpleBox"
import { TagBoxset } from "#app/tag/TagBoxset"
import { tenantType } from "#app/tenant/constants"
import { TenantLabel } from "#app/tenant/Label"
import { useLogged } from '#app/user/store'
import { useT } from '#app/intl/api'
import { PartyInstance } from "./api"
import { usePartyInstanceValidation } from './validation'



type Props = {

    state: FormState<PartyInstance>
    report: ReturnType<ReturnType<ReturnType<typeof usePartyInstanceValidation>['on']>['validateInstance']>

}

export const PartyGeneralForm = (props: Props) => {

    const t = useT()

    const { state, report } = props;

    const { edited, change } = state

    const logged = useLogged()


    return <Form state={state} >


        <TagBoxset edited={edited.tags} type={tenantType} validation={report} onChange={change((t, v) => t.tags = v)} />


        <NoteBox label={t("common.fields.note.name")} validation={report.note} autoSize={{ minRows: 4, maxRows: 6 }} onChange={change((t, v) => t.properties.note = t.properties.note ? { ...t.properties.note, [logged.tenant]: v } : { [logged.tenant]: v })} >
            {edited.properties.note}
        </NoteBox>

        <SimpleBox label={t("campaign.fields.source.name")} validation={report.source} >
            <TenantLabel tenant={edited.source} />
        </SimpleBox>

        {edited.lineage &&


            <SimpleBox label={t("common.fields.lineage.name")} validation={report.lineage} >
                <PartyInstanceLabel showCampaign instance={edited.lineage} />
            </SimpleBox>

        }


    </Form>


}