import { useCalls } from "#app/call/call";
import { Campaign } from "#campaign/model";
import { domainService } from "../../constants";
import { PartyInstance } from "./api";


const parties = `/partyinstance`


export const usePartyInstanceCalls = () => {

    const { at } = useCalls()

    return {

        on: (c: Campaign | string) => {

            const cid = typeof c === 'string' ? c : c.id


            return {

                fetchAll: (): Promise<PartyInstance[]> => at(`${parties}/search`, domainService).post({ campaign: cid })

                ,

                fetchOne: (source: string): Promise<PartyInstance> => at(`${parties}/${source}?byref=true`, domainService).get()

                ,

                addAll: (instances: PartyInstance[]): Promise<PartyInstance[]> => at(parties, domainService).post(instances)

                ,

                updateOne: (instance: PartyInstance): Promise<void> => at(`${parties}/${instance.id}`, domainService).put(instance)

                ,

                removeOne: (instance: PartyInstance): Promise<void> => at(`${parties}/${instance.id}`, domainService).delete()

                ,

                removeMany: (instances: PartyInstance[]): Promise<void> => at(`${parties}/remove`, domainService).post(instances.map(i => i.id))

            }
        }
    }
}
