
import { tenantType } from "#app/tenant/constants";
import { Tenant } from '#app/tenant/model';
import { useTenantStore } from '#app/tenant/store';
import { useLogged } from '#app/user/store';
import { deepclone, indexMap } from "#app/utils/common";
import { paramsInQuery, updateQuery } from "#app/utils/routes";
import { withReport } from '#app/utils/validation';
import { CampaignContext, CampaignNext } from '#campaign/context';
import { ApproveCycle, Campaign, CampaignInstance, CampaignSettings, InstanceProperties, InstanceRef, Overlay, useCampaignModel } from "#campaign/model";
import { useCampaignTenantPreferences } from '#campaign/preferences/api';
import { useCampaignRouting } from '#campaign/store';
import { useLayoutRegistry } from '#layout/registry';
import { Topic } from '#messages/model';
import { useContext } from 'react';


export type PartyInstanceStore = {

    parties: {
        all: PartyInstance[]
        map: Record<string, PartyInstance>
    }

}


export type PartyInstanceDto = CampaignInstance & {

    properties: PartyProperties

}
export type PartyInstance = PartyInstanceDto       // future-proofs divergence from exchange model, starts, aligned.

export type PartyOverlays = { [instance in 'requirements' | 'products']: Record<string, Overlay> }


export type PartyProperties = InstanceProperties & {

    settings: Partial<CampaignSettings> & { bypassSelfApproval?: boolean }
    parameterOverlays: PartyOverlays
}

export const newPartyProperties = () => ({

    settings: {},

    parameterOverlays: {

        requirements: {} as Record<string, Overlay>,
        products: {} as Record<string, Overlay>
    } as PartyOverlays

})



export const initialPartyInstances = (): PartyInstanceStore => ({

    parties: {
        all: undefined!,
        map: undefined!
    }
})


export const usePartyInstanceStore = () => {

    const state = useContext(CampaignContext)

    const campaigns = useCampaignModel()

    const tenantprefs = useCampaignTenantPreferences()

    return {

        on: (c: Campaign) => {


            const self = {


                all: () => state.get().campaigns.instances[c.id]?.parties?.all

                ,

                noInstance: () => ({

                    id: undefined!,
                    instanceType: tenantType,
                    source: "unknown",
                    campaign: c.id,
                    tags: [],
                    properties: newPartyProperties()

                }) as PartyInstance


                ,

                livePush: (instances: PartyInstance[], type: 'change' | 'remove') => {

                    const map = state.get().campaigns.instances[c.id]?.parties?.map ?? {}
                    let current = state.get().campaigns.instances[c.id]?.parties?.all ?? []

                    instances.forEach(instance => {

                        switch (type) {

                            case "change":
                                current = map[instance.id] ? current.map(i => i.id === instance.id ? instance : i) : [...current, instance]
                                break;

                            case "remove": current = current.filter(i => i.id !== instance.id); break

                        }

                    })

                    self.setAll(current)
                }

                ,

                setAll: (instances: PartyInstance[], props?: { noOverwrite: boolean }) => state.set(s => {

                    const campaign = s.campaigns.instances[c.id] ?? {}
                    const parties = campaign.parties ?? initialPartyInstances()

                    if (parties.all && props?.noOverwrite)
                        return

                    s.campaigns.instances[c.id] = { ...campaign, parties: { ...parties, all: instances, map: indexMap(instances).by(i => i.id) } }

                })


                ,
                lookup: (id: string | undefined) => id ? state.get().campaigns.instances[c.id]?.parties?.map[id] : undefined

                ,

                safeLookup: (id: string | undefined) => self.lookup(id) ?? self.noInstance()

                ,

                lookupBySource: (source: string | undefined) => self.all().find(p => p.source === source)

                ,


                safeLookupBySource: (id: string | undefined) => self.lookupBySource(id) ?? self.noInstance()

                ,


                lookupLineage: (ref: InstanceRef | undefined) => ref ? state.get().campaigns.instances[ref.campaign]?.parties.all.find(p => p.source === ref.source) : undefined




                ,

                generate: (tenant: Tenant): PartyInstance => ({

                    id: undefined!,
                    instanceType: tenantType,
                    source: tenant.id,
                    campaign: c.id,
                    tags: deepclone(tenant.tags),
                    properties: {

                        ...newPartyProperties(),
                        note: tenant.preferences.note
                    }
                })


                ,

                validateInstance: (p: PartyInstance) => withReport({}) // nothing to validate for now

                ,

                // resolves to instance, optionally defaulting to tenant preference.
                approveCycleFor: (source: string, delegation: 'delegate' | 'nodelegation' = 'delegate') => self.approveCycle(self.safeLookupBySource(source), delegation)

                ,

                // optionally defaulting to tenant preference.
                approveCycle: (p: PartyInstance, delegation: 'delegate' | 'nodelegation' = 'delegate') => p.properties.settings.approveCycle ?? (delegation === 'delegate' ? self.approveCycleDefaultFor(p.source) : undefined)


                ,

                // defaults to tenant preference.
                approveCycleDefaultFor: (source: string) => tenantprefs.approveCycleRef(source)

                ,

                setApproveCycle: (p: PartyInstance, value: ApproveCycle | undefined) => {

                    if (value)
                        p.properties.settings.approveCycle = value
                    else
                        delete p.properties.settings.approveCycle
                }

                ,

                // optionally defaulting to tenant preference.
                canBypassSelfApproval: (p: PartyInstance, delegation: 'delegate' | 'nodelegation' = 'delegate') => p.properties.settings.bypassSelfApproval ?? (delegation === 'delegate' ? self.canBypassSelfApprovalDefault(p) : undefined)


                ,

                // defaults to tenant preference.
                canBypassSelfApprovalDefault: (p: PartyInstance) => tenantprefs.canBypassSelfApprovalRef(p.source)

                ,

                // defaults to tenant preference.
                canBypassSelfApprovalFor: (source: string, delegation: 'delegate' | 'nodelegation' = 'delegate') => self.canBypassSelfApproval(self.safeLookupBySource(source), delegation)



                ,


                setCanBypassSelfApproval: (p: PartyInstance, value: boolean | undefined) => {

                    if (value === undefined)
                        delete p.properties.settings.bypassSelfApproval
                    else
                        p.properties.settings.bypassSelfApproval = value
                }

                ,

                setCanSeeNotApplicable: (p: PartyInstance, value: boolean | undefined) => {

                    if (value === undefined)
                        delete p.properties.settings.partyCanSeeNotApplicable

                    else
                        p.properties.settings.partyCanSeeNotApplicable = value

                }

                ,

                canSeeNotApplicable: (p: PartyInstance, delegation: 'delegate' | 'nodelegation' = 'delegate') => p.properties.settings.partyCanSeeNotApplicable ?? (delegation === 'delegate' ? self.canSeeNotApplicableDefault() : undefined)

                ,

                canSeeNotApplicableDefault: () => campaigns.canPartySeeNotApplicable(c)

                ,

                topics: (p: PartyInstance): Topic[] => [{ type: tenantType, name: `${c.id}:${p.source}` }]
            }

            return self
        }
    }
}


export const usePartyInstances = () => {

    const store = usePartyInstanceStore()

    const tenantstore = useTenantStore()

    const campaignrouting = useCampaignRouting()

    const logged = useLogged()

    const layoutRegistry = useLayoutRegistry()

    return {

        on: (c: Campaign) => {


            const self = {

                ...store.on(c)

                ,

                allSources: () => tenantstore.all()

                ,

                lookupSource: (instance: string | PartyInstance | undefined) => tenantstore.safeLookup(typeof instance === 'string' ? instance : instance?.source)

                ,


                detailParam: () => 'ti-drawer'

                ,

                // eslint-disable-next-line
                detailInRoute: () => paramsInQuery(location.search)[self.detailParam()] as string

                ,


                route: (p?: PartyInstance) =>

                    // eslint-disable-next-line
                    `${campaignrouting.routeTo(c)}/${tenantType}?${updateQuery(location.search).with(params => {

                        params[self.detailParam()] = p ? p.id : null
                        params['source'] = p?.source ?? null
                    })}`


                ,


                allSorted: () => {

                    return [...self.all() ?? []].sort(self.comparator)
                }

                ,

                allForStats: () => {

                    return logged.hasNoTenant() ? self.all().filter(p => !(c.properties.statExcludeList ?? []).includes(p.source)) : self.all()
                }


                ,

                stringify: (i: PartyInstance) => `${tenantstore.stringifyRef(i.source)}`



                ,


                clone: (instance: PartyInstance, campaign: CampaignNext): PartyInstance => {

                    const { type } = campaign

                    const clone = deepclone(instance)

                    const overlays = instance.properties.parameterOverlays ?? {}
                    const productsOverlay = overlays.products ?? {}
                    const requirementsOverlay = overlays.requirements ?? {}

                    //PartyOverlays are a map of requirements/products.
                    // Inside of each we have the a map of instanceid: Overlay {paramid: Param}
                    // for this we need a double reduce.
                    const clonedOverlays = {
                        products: {
                            ...Object.keys(productsOverlay).reduce(
                                (acc1, cur1) =>
                                (
                                    {
                                        ...acc1,
                                        [cur1]: Object.keys(productsOverlay[cur1]).reduce((acc2, cur2) => {
                                            const param = productsOverlay[cur1][cur2]
                                            const clonedParam = type === 'branch' ?
                                                layoutRegistry.lookupParameter(param.original.spec).clone({ ...param.original, value: param.value }, campaign)
                                                : param
                                            return clonedParam ? { ...acc2, [cur2]: clonedParam } : acc2
                                        }, {})
                                    })
                                , {})
                        },
                        requirements: {
                            ...Object.keys(requirementsOverlay).reduce(
                                (acc1, cur1) =>
                                (
                                    {
                                        ...acc1,
                                        [cur1]: Object.keys(requirementsOverlay[cur1]).reduce((acc2, cur2) => {
                                            const param = requirementsOverlay[cur1][cur2]
                                            const clonedParam = type === 'branch' ?
                                                layoutRegistry.lookupParameter(param.original.spec).clone({ ...param.original, value: param.value }, campaign)
                                                : param
                                            return clonedParam ? { ...acc2, [cur2]: clonedParam } : acc2
                                        }, {})
                                    })
                                , {})
                        }
                    } as PartyOverlays



                    return {
                        ...clone,
                        id: undefined!,
                        campaign: c.id,
                        tags: type === 'branch' ? tenantstore.lookup(instance.source)?.tags ?? clone.tags : clone.tags,
                        lineage: type === 'branch' ? { source: instance.source, campaign: instance.campaign } : undefined,
                        properties: { ...clone.properties, parameterOverlays: clonedOverlays }
                    }

                }

                ,

                comparator: (o1: PartyInstance, o2: PartyInstance) => {

                    return tenantstore.comparator(tenantstore.safeLookup(o1.source)!, tenantstore.safeLookup(o2.source)!)
                }


            }

            return self
        }
    }
}






