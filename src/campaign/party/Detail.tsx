
import { Button } from "#app/components/Button"
import { Drawer } from "#app/components/Drawer"
import { useFormState } from "#app/form/hooks"
import { icns } from "#app/icons"
import { useT } from '#app/intl/api'
import { useLocale } from "#app/model/hooks"
import { Tab } from "#app/scaffold/Tab"
import { Topbar } from "#app/scaffold/Topbar"
import { tenantIcon } from "#app/tenant/constants"
import { useTenantStore } from '#app/tenant/store'
import { paramsInQuery } from "#app/utils/routes"
import { ValidationReport } from "#app/utils/validation"
import { useCurrentCampaign } from "#campaign/hooks"
import { submissionType } from "#campaign/submission/constants"
import { useRoutedTypes } from '#config/model'
import { useDashboard } from '#dashboard/api'
import { useHistory, useLocation } from "react-router-dom"
import { PartyGeneralForm } from "./GeneralForm"
import { PartyProfile } from "./PartyProfile"
import { PartySettings } from "./Settings"
import { PartyInstance, usePartyInstances } from "./api"
import { usePartyInstanceClient } from './client'
import { usePartyInstanceValidation } from './validation'




type Props =  {

    detail:PartyInstance
    onClose:()=>void

    width?: number
}


export const PartyInstanceDetail = (props:Props) => {

    const t = useT()
    const {l} = useLocale()
    const {search} = useLocation()
    const history = useHistory()

    const {width=650,detail,onClose} = props

    const campaign = useCurrentCampaign()
    const {safeLookup} = useTenantStore()
    const dashboard = useDashboard()
    const routedTypes = useRoutedTypes()
    const {detailParam} = usePartyInstances().on(campaign)
    const {save} = usePartyInstanceClient().on(campaign)
    const {validateInstance} = usePartyInstanceValidation().on(campaign)
    
    const tenant = safeLookup(detail.source)

    const formstate  = useFormState(detail)
    const {edited, dirty, reset} = formstate

    const liveBtn = <Button enabledOnReadOnly iconLeft icn={icns.edit} onClick={()=>history.push(dashboard.on(campaign).routeToParty(edited))}>{t("campaign.labels.campaign_view.live")}</Button>


    const onSave = ()=>save(edited).then(onClose)

    const revert = <Button icn={icns.revert} enabled={dirty} onClick={()=>reset()}>{t("common.buttons.revert")}</Button>
    const saveact = <Button icn={icns.save} enabled={dirty} type="primary" onClick={onSave}>{t("common.buttons.save")}</Button> 

    const {tab='info'} = paramsInQuery(search)

    const report = validateInstance(edited)

    return  <Drawer readonly={!edited.source} renderAfterTransition routeId={detailParam()} visible width={width} warnOnClose={dirty} title={t("common.buttons.edit_one", {singular:l(tenant?.name!)})} icon={tenantIcon} onClose={onClose}>
                
                <Topbar offset={63}>
                    {saveact}
                    {revert}
                    {routedTypes.includes(submissionType) && liveBtn}

                    <Tab default id='info' icon={icns.form} name={t("common.labels.general")} />
                    <Tab  id="preferences" icon={icns.settings}  name={t("common.labels.preferences")}  />
                    <Tab  id="profile" icon={icns.profile}  name={t("campaign.labels.profile")}  />
    
                </Topbar>

                { tab==='info' &&  <PartyGeneralForm state={formstate} report={report} />  }
                { tab==='preferences' && <PartySettings state={formstate} report={report as ValidationReport} /> }
                { tab==='profile' &&  <PartyProfile state={formstate} /> }

              
            </Drawer>
}