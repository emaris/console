import { useT } from '#app/intl/api';
import { defaultLanguage } from "#app/intl/model";
import { useToggleBusy } from "#app/system/api";
import { tenantPlural, tenantSingular } from "#app/tenant/constants";
import { useTenantStore } from '#app/tenant/store';
import { through } from "#app/utils/common";
import { notify, showAndThrow, useFeedback } from "#app/utils/feedback";
import { CampaignNext } from '#campaign/context';
import { Campaign } from "#campaign/model";
import { PartyInstance, PartyInstanceDto, usePartyInstances } from './api';
import { usePartyInstanceCalls } from './calls';


const type = "partyinstance"


export const usePartyInstanceClient = () => {

    const instances = usePartyInstances()

    const t = useT()

    const toggleBusy = useToggleBusy()
    const fb = useFeedback()

    const tenants = useTenantStore()

    const partyinstcalls = usePartyInstanceCalls()

    const singular = t(tenantSingular).toLowerCase()
    const plural = t(tenantPlural).toLowerCase()
    
    return {

        on: (c: Campaign) => {

            const cinst = instances.on(c)

            const self = {

                areReady: () => !!cinst.all()

                ,
                
                addAll: (instances: PartyInstance[]) =>

                    toggleBusy(`${type}.addall`, t("common.feedback.save_changes"))

                        .then(_ => partyinstcalls.on(c).addAll(instances))
                        .then((added: PartyInstanceDto[]) => [...cinst.all(), ...added])
                        .then(cinst.setAll)
                        .then(() => notify(t('common.feedback.saved')))

                        .catch(e => showAndThrow(e, t("common.calls.add_many_error", { plural })))
                        .finally(() => toggleBusy(`${type}.addall`))

                ,

                cloneAll: (instances: PartyInstance[], context: CampaignNext) => partyinstcalls.on(c).addAll(instances.map(i => cinst.clone(i, context))).then(cinst.setAll)

      
                ,

                fetchAll: (forceRefresh = false) =>

                    self.areReady() && !forceRefresh ? Promise.resolve(cinst.all())

                        :

                        toggleBusy(`${type}.fetchAll.${c.id}`, t("common.feedback.load", { plural }))

                            .then(_ => console.log(`fetching parties for ${c.name[defaultLanguage]}...`))
                            .then(_ => Promise.all([partyinstcalls.on(c).fetchAll(), tenants.fetchAll()]))  // load template dependency in parallel
                            .then(([instances]) => instances)    // continues with instances only
                            .then(through($ => cinst.setAll($, { noOverwrite: !forceRefresh })))


                            .catch(e => showAndThrow(e, t("common.calls.fetch_all_error", { plural })))
                            .finally(() => toggleBusy(`${type}.fetchAll.${c.id}`))

                ,

                fetchAllQuietly: (forceRefresh = false) =>

                    self.areReady() && !forceRefresh ? Promise.resolve(cinst.all())

                        :

                        toggleBusy(`${type}.fetchAll.${c.id}`, t("common.feedback.load", { plural }))

                            .then(_ => console.log(`fetching parties for ${c.name[defaultLanguage]}...`))
                            .then(_ => Promise.all([partyinstcalls.on(c).fetchAll(), tenants.fetchAll()]))  // load template dependency in parallel
                            .then(([instances]) => instances)    // continues with instances only
                            .then(through(cinst.setAll))


                            .catch(e => showAndThrow(e, t("common.calls.fetch_all_error", { plural })))
                            .finally(() => toggleBusy(`${type}.fetchAll.${c.id}`))

                ,

    
                save: (instance: PartyInstance, replacementOf: PartyInstance = instance) =>

                    toggleBusy(`${type}.updateOne`, t("common.feedback.save_changes"))

                        .then(_ => partyinstcalls.on(c).updateOne(instance))
                        .then(() => cinst.all().map(p => p.id === replacementOf.id ? instance : p))
                        .then(cinst.setAll)
                        .then(() => notify(t('common.feedback.saved')))

                        .catch(e => showAndThrow(e, t("common.calls.update_one_error", { singular })))
                        .finally(() => toggleBusy(`${type}.updateOne`))

                ,

                remove: (instance: PartyInstance, onConfirm?: (...args) => void) =>

                    fb.askConsent({

                        title: t('common.consent.remove_one_title', { singular }),
                        content: t("common.consent.remove_one_msg", { singular }),
                        okText: t("common.consent.remove_one_confirm", { singular }),

                        onOk: () => {

                            toggleBusy(`${type}.removeOne`, t("common.feedback.save_changes"))

                                .then(_ => partyinstcalls.on(c).removeOne(instance))
                                .then(() => cinst.all().filter(p => instance.id !== p.id))
                                .then(cinst.setAll)
                                .then(() => notify(t('common.feedback.saved')))
                                .then(() => onConfirm && onConfirm(instance))

                                .catch(e => showAndThrow(e, t("common.calls.remove_one_error", { singular })))
                                .finally(() => toggleBusy(`${type}.removeOne`))


                        }
                    })

                ,

                removeMany: (instances: PartyInstance[], onConfirm?: () => void) =>


                    fb.askConsent({

                        title: t('common.consent.remove_many_title', { count: instances.length, plural }),
                        content: t("common.consent.remove_many_msg", { count: instances.length, plural }),
                        okText: t("common.consent.remove_many_confirm", { count: instances.length, plural }),

                        onOk: () => {

                            const ids = instances.map(i => i.id)

                            toggleBusy(`${type}.removeMany`, t("common.feedback.save_changes"))

                                .then(_ => partyinstcalls.on(c).removeMany(instances))
                                .then(() => cinst.all().filter(p => !ids.includes(p.id)))
                                .then(through(cinst.setAll))
                                .then(() => notify(t('common.feedback.saved')))
                                .then(through(() => onConfirm && onConfirm()))

                                .catch(e => showAndThrow(e, t("common.calls.remove_many_error", { plural })))
                                .finally(() => toggleBusy(`${type}.removeMany`))

                        }
                    })

                
            }

            return self
        }
    }
}






