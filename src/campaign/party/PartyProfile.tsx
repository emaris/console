

import { Button } from "#app/components/Button"
import { DrawerAnchor } from "#app/components/Drawer"
import { Column, VirtualTable } from "#app/components/VirtualTable"
import { FormState } from "#app/form/hooks"
import { icns } from "#app/icons"
import { useT } from '#app/intl/api'
import { TocEntry, TocLayout } from "#app/scaffold/TocLayout"
import { BytestreamedContext } from '#app/stream/BytestreamedHelper'
import { paramsInQuery, updateQuery } from "#app/utils/routes"
import { useCurrentCampaign } from "#campaign/hooks"
import { ProductInstanceLabel } from "#campaign/product/Label"
import { ProductInstance, useProductInstances } from "#campaign/product/api"
import { RequirementInstanceLabel } from "#campaign/requirement/Label"
import { RequirementInstance, useRequirementInstances } from "#campaign/requirement/api"
import { instanceFrom } from "#layout/model"
import { LayoutProvider } from "#layout/provider"
import { ProductDetailLoader } from '#product/DetailLoader'
import { productPlural, productType } from "#product/constants"
import { useProductLayoutInitialiser } from '#product/layoutinitialiser'
import { useProductStore } from '#product/store'
import { RequirementDetailLoader } from '#requirement/DetailLoader'
import { requirementPlural, requirementType } from "#requirement/constants"
import { useRequirementLayoutInitialiser } from '#requirement/layoutinitialiser'
import { useRequirementStore } from '#requirement/store'
import { useHistory, useLocation } from "react-router-dom"
import { ProductOverlaySection, RequirementOverlaySection } from "./ParamOverlayForm"
import { PartyInstance } from "./api"
import "./styles.scss"

type Props = {

    state: FormState<PartyInstance>
}

// offset for anchors to hanf off the top of the page
const anchorOffset = 160

export const PartyProfile = (props: Props) => {

    const t = useT()

    return <TocLayout className="party-profile" offsetTop={anchorOffset + 10}>

        <TocEntry id={requirementType} title={t(requirementPlural)} />
        <TocEntry id={productType} title={t(productPlural)} />

        <div className="profile-instances">

            <BytestreamedContext>
                <RequirementSection {...props} />
                <ProductSession {...props} />
            </BytestreamedContext>

        </div>

    </TocLayout>
}


export const reqParam = "reqparameter"


const RequirementSection = (props: Props) => {

    const t = useT()

    const history = useHistory()
    const { pathname, search } = useLocation()

    const campaign = useCurrentCampaign()
    const requirements = useRequirementStore()

    const { [reqParam]: reqparameter } = paramsInQuery(search)

    const target = requirements.lookup(reqparameter as string)

    const editRequirements = (r: RequirementInstance) => history.push(`${pathname}?${updateQuery(search).with(params => params[reqParam] = r.source)}`)
    const closeRequirements = () => history.push(`${pathname}?${updateQuery(search).with(params => params[reqParam] = null)}`)

    const all = useRequirementInstances().on(campaign).all()

    const plural = t('layout.parameters.plural')

    const editParams = (r: RequirementInstance) => <Button icn={icns.edit} key="edit" onClick={() => editRequirements(r)} > {t("common.buttons.edit_many", { plural })}</Button>

    return <DrawerAnchor id={requirementType} className={`instance-section`} offset={anchorOffset}>

        <h1 className=".section-title">{t(requirementPlural)}</h1>

        <VirtualTable height={2000} rowHeight={40} data={all} selectable={false} rowKey="source" filtered={all.length > 15}
            onDoubleClick={r => editRequirements(r)}
            actions={p => [editParams(p)]}>

            <Column<RequirementInstance> flexGrow={1} title={t("common.fields.name_multi.name")} dataKey="name" dataGetter={p => <RequirementInstanceLabel lineage instance={p} />} />

        </VirtualTable>



        {target &&
            <RequirementDetailLoader id={target.id} >{loaded =>

                <LayoutProvider useInitialiser={useRequirementLayoutInitialiser} layout={instanceFrom(loaded?.properties.layout)} onChange={() => {/* no real change intended here, only overlays. */ }}>
                    <RequirementOverlaySection key={loaded?.id} {...props} target={target} onClose={closeRequirements} />
                </LayoutProvider>

            }</RequirementDetailLoader>
        }


    </DrawerAnchor>


}




const ProductSession = (props: Props) => {

    const t = useT()

    const history = useHistory()
    const { pathname, search } = useLocation()

    const campaign = useCurrentCampaign()
    const products = useProductStore()

    const { prodparameter } = paramsInQuery(search)

    const target = products.lookup(prodparameter as string)

    const editParameters = (r: RequirementInstance) => history.push(`${pathname}?${updateQuery(search).with(params => params.prodparameter = r.source)}`)
    const close = () => history.push(`${pathname}?${updateQuery(search).with(params => params.prodparameter = null)}`)

    const instances = useProductInstances().on(campaign)
    const all = instances.allSorted()

    const plural = t('layout.parameters.plural')

    const editParams = (r: ProductInstance) => <Button icn={icns.edit} key="edit" onClick={() => editParameters(r)} > {t("common.buttons.edit_many", { plural })}</Button>

    return <DrawerAnchor id={productType} className={`instance-section`} offset={anchorOffset}>

        <h1 className=".section-title">{t(productPlural)}</h1>

        <VirtualTable height={2000} rowHeight={40} data={all} selectable={false} rowKey="source" filtered={all.length > 15}
            onDoubleClick={r => editParameters(r)}
            actions={p => [editParams(p)]}>

            <Column<ProductInstance> flexGrow={1} title={t("common.fields.name_multi.name")} dataKey="name" dataGetter={p => <ProductInstanceLabel link instance={p} />} />

        </VirtualTable>


        {target &&

            <ProductDetailLoader id={target.id} >{loaded =>

                <LayoutProvider useInitialiser={useProductLayoutInitialiser} layout={instanceFrom(loaded.properties.layout)} onChange={() => {/* no real change intended here, only overlays. */ }}>
                    <ProductOverlaySection key={loaded.id} {...props} target={target} onClose={close} />
                </LayoutProvider>
            }</ProductDetailLoader>
        }

    </DrawerAnchor>


}