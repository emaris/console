
import { useCurrentCampaign } from "#campaign/hooks";
import { PartyInstanceLabel } from "#campaign/party/Label";
import { CampaignValidation } from "#campaign/validation";
import { Button, ButtonGroup } from "#app/components/Button";
import { useListState } from "#app/components/hooks";
import { NoSuchRoute } from "#app/components/NoSuchRoute";
import { Column, VirtualTable } from "#app/components/VirtualTable";
import { icns } from "#app/icons";
import { useT } from '#app/intl/api';
import { useLocale } from "#app/model/hooks";
import { useTagFilter } from '#app/tag/filter';
import { TagList } from "#app/tag/Label";
import { useTagStore } from '#app/tag/store';
import { tenantIcon, tenantPlural, tenantType } from "#app/tenant/constants";
import { TenantLabel } from "#app/tenant/Label";
import { Tenant } from "#app/tenant/model";
import { useTenantStore } from '#app/tenant/store';
import * as React from "react";
import { useHistory } from "react-router-dom";
import { usePicker } from "../Picker";
import { PartyInstanceDetail } from "./Detail";
import { PartyInstance } from "./api";
import { usePartyInstances } from './api';
import { usePartyInstanceClient } from './client';


type Props = {

    report: ReturnType<CampaignValidation['validateParties']>
}

export const PartyInstanceList = (props: Props) => {

    const campaign = useCurrentCampaign()

    const parties = usePartyInstances().on(campaign)

    const source = parties.detailInRoute()

    const detail = parties.lookup(source)

    return source && !detail ?  <NoSuchRoute /> : <InnerPartyInstanceList {...props} detail={detail!} /> 
}


const InnerPartyInstanceList = (props: Props & { detail:PartyInstance }) => {

    const t = useT()
    const { l } = useLocale()

    const history = useHistory()

    const tags = useTagStore()
    const tenants = useTenantStore()
    const campaign = useCurrentCampaign()

    const readOnly = campaign.guarded

    const { detail, report } = props

    const {Picker, setPickerVisible, pickerVisible} = usePicker()

    const parties = usePartyInstances().on(campaign)
    const partyinstclient = usePartyInstanceClient().on(campaign)
    const liststate = useListState<PartyInstance>()

    const { selected, resetSelected, removeOne} = liststate

     const plural = t(tenantPlural)
        
    const unfilteredData = parties.all()

    // eslint-disable-next-line
    const sortedData = React.useMemo(() => parties.allSorted(), [unfilteredData])

    const partyinstanceGroup = `${campaign.id}-${tenantType}`

    const { TagFilter, tagFilteredData } = useTagFilter({
        filtered: sortedData,
        group: partyinstanceGroup,
        key:`tags`
    })

    const data = tagFilteredData

    const editBtn = (p: PartyInstance) =>
        <Button
            key="edit"
            onClick={() => history.push(parties.route(p))}>
            {t("common.buttons.open")}
        </Button>

    const removeBtn = (p: PartyInstance) =>
        <Button
            key="remove"
            type="danger"
            disabled={readOnly}
            onClick={() => partyinstclient.remove(p, removeOne) }>
            {t("common.buttons.remove")}
        </Button>

    const addBtn =
        <Button
            icn={icns.add}
            type="primary"
            disabled={readOnly}
            onClick={()=>setPickerVisible(true)}>
            {t("common.buttons.add_many", { plural })}
        </Button>

    const removeSelectedBtn =
        <Button
             enabled={selected.length >= 1}
            disabled={readOnly}
            onClick={() => partyinstclient.removeMany(selected, resetSelected)}>
            {t("common.buttons.remove_many", { count: selected.length })}
        </Button>


    const rowValidation = ({ rowData: party }) => report[party.id].status === "error" && icns.error(report[party.id].msg)    

    const unfilteredTemplates = tenants.all()

    // eslint-disable-next-line
    const activeTemplates = React.useMemo(()=>tenants.allSorted().filter(r => r.lifecycle.state !== 'inactive'),[unfilteredTemplates])

      return (<>
        <VirtualTable<PartyInstance> data={data} total={unfilteredData.length} state={liststate} selectAll

            filterGroup={partyinstanceGroup} filterBy={parties.stringify} filterPlaceholder={t("common.components.table.filter_placeholder", { plural: plural.toLowerCase() })}
            
            filters={[TagFilter]}
           
            decorations={[<ButtonGroup>{addBtn}{removeSelectedBtn}</ButtonGroup>]}
            
            onDoubleClick={p => history.push(parties.route(p))}

            refreshFlag={selected}
            
            actions={p => [editBtn(p), removeBtn(p)]}>

            <Column<PartyInstance> title={t("common.fields.name_multi.name")} decorations={[rowValidation]}
                dataKey="name" dataGetter={p => <PartyInstanceLabel noLineage noMemo instance={p} />} />

            {tags.allTagsOf(tenantType).length > 0 &&
                <Column flexGrow={1} sortable={false} title={t("common.fields.tags.name")} dataKey="t.tags" dataGetter={(pi: PartyInstance) => <TagList truncateEllipsisLink={parties.route(pi)} taglist={pi.tags} />} />
            }
        </VirtualTable>


        {
            
            pickerVisible && 
            
            <Picker id='partyinst' title={plural} icon={tenantIcon} noContext
                filterGroup={`${partyinstanceGroup}-picker`}
                resources={activeTemplates}
                selected={sortedData.map(i => i.source)}
                onSelection={added => Promise.resolve(partyinstclient.addAll(added.map(parties.generate as any)))}>

                <Column title={t("common.fields.name_multi.name")} flexGrow={1} dataKey="t.name.en" dataGetter={(t: Tenant) => l(t.name)} cellRenderer={cell => <TenantLabel noLink tenant={cell.rowData} />} />
                <Column title={t("common.fields.tags.name")} dataKey="t.tags" dataGetter={(t: Tenant) => <TagList truncateEllipsisLink={tenants.routeTo(t)} taglist={t.tags} />} flexGrow={1} />

            </Picker>

        }

        { detail && 
        
            <PartyInstanceDetail detail={detail} onClose={() => history.push(parties.route())} />
          
        }
        


    </>
    )
}