import { NoSuchRoute } from "#app/components/NoSuchRoute";
import { tenantType } from "#app/tenant/constants";
import { campaignRoute } from "#campaign/constants";
import { CampaignValidation } from "#campaign/validation";
import * as React from "react";
import { Route, Switch } from "react-router-dom";
import { PartyInstanceList } from "./List";


type Props = {

    report: ReturnType<CampaignValidation['validateParties']>
}

export const Parties = (props: Props) => {

    return <Switch>
        <Route exact path={`${campaignRoute}/:name/${tenantType}`}>
            <PartyInstanceList {...props} />
        </Route>
        <NoSuchRoute />
    </Switch>

}