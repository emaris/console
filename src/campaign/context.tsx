import { DashboardState } from '#dashboard/store'
import { EventState } from '#event/context'
import { ProductState } from '#product/context'
import { RequirementState } from '#requirement/context'
import { fallbackStateOver, State } from 'apprise-frontend-core/state/api'
import { TenantState } from '#app/tenant/context'
import { createContext } from 'react'
import { EventInstanceState } from './event/store'
import { Campaign } from './model'
import { PartyInstanceStore } from './party/api'
import { ProductInstanceState } from './product/api'
import { RequirementInstanceState } from './requirement/api'
import { SubmissionState } from './submission/store'

export type CampaignNext = {

    source: Campaign
    type: 'new' | 'branch' | 'clone'
    lineage?: Campaign
    timeOffset?: number
}

export type CampaignState = {

    campaigns: {

        all: Campaign[]
        map: Record<string, Campaign>
        next?: CampaignNext
        instances: {

            [key: string]: PartyInstanceStore & RequirementInstanceState & ProductInstanceState & EventInstanceState & SubmissionState & { archive: ArchiveState }
        }

    } & DashboardState
}

export type ArchiveState = RequirementState & ProductState & EventState & TenantState


export const initialCampaigns: CampaignState = {

    campaigns: {

        all: undefined!,
        map: undefined!,
        instances: {},

        dashboard: {}

    }


}


export const CampaignContext = createContext<State<CampaignState>>(fallbackStateOver(initialCampaigns))



export type UsageData = {

    instances: Record<string, string[]>

}

export const CampaignUsageContext = createContext<State<UsageData>>(undefined!)
