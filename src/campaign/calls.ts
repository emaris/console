
import { useCalls } from "#app/call/call"
import { domainService } from "#constants"
import { usePreload } from 'apprise-frontend-core/client/preload'
import { Campaign, CampaignDto, Summary } from "./model"

export const campaignApi = "/campaign"
const includeArchiveParam = 'archive'

export type CampaignAndSummary = {

    campaign: CampaignDto
    summary: Summary

}

export type CampaignAndArchive = {

    campaign: CampaignDto
    archive: CampaignArchive

}



export type ArchiveEntry = {

    details: any
    type: string
}

export type CampaignArchive = ArchiveEntry[]


export const useCampaignCalls = () => {

    const {at} = useCalls()

    const intern = (dto: CampaignDto): Campaign => ({ ...dto, loaded: false })

    const extern = ({ loaded, ...rest }: Campaign) => rest

    const preload = usePreload()

    return {


        fetchAll: () =>  (preload.get<CampaignAndSummary[]>(campaignApi) ??  at(campaignApi,domainService).get<CampaignAndSummary[]>()).then(all=>all.map(cs=>intern({...cs.campaign,summary:cs.summary})))
        

        , 

        fetchOne: (id:String, includeArchive: boolean = true) =>  at(`${campaignApi}/${id}?${includeArchiveParam}=${includeArchive}`,domainService).get<CampaignAndArchive>().then(c => ({...c, campaign: intern(c.campaign)})) 
        
        ,

        fetchWith: (filter:{type:string,id:string}) => at(`${campaignApi}/search`,domainService).post<string[]>(filter)

        ,

        add: (campaign:Campaign)  =>  at(campaignApi,domainService).post<Campaign>(extern(campaign)).then(intern)
        
        ,
    
        update: (campaign:Campaign) => at(`${campaignApi}/${campaign.id}`,domainService).put<CampaignAndArchive>(extern(campaign)).then(c => ({...c, campaign: intern(c.campaign)})) 
        
        , 

        delete: (id:string) => at(`${campaignApi}/${id}`,domainService).delete()


    }

}