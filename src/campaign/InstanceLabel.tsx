import { LabelProps, sameLabel, UnknownLabel } from "#app/components/Label"
import { icns } from "#app/icons"
import { useLogged } from '#app/user/store'
import { useRoutedTypes } from '#config/model'
import { Tooltip } from "antd"
import React from "react"
import { CampaignLabel } from "./Label"
import { CampaignInstance, InstanceRef } from "./model"
import { useCampaignStore } from './store'

export type InstanceLabelProps = LabelProps & {

    showCampaign?: boolean
    noLineage?: boolean

    children?: any
}

export type InternalProps = InstanceLabelProps & {

    instance: CampaignInstance | undefined
    sourceLabel: (props: LabelProps) => JSX.Element

    tooltipForLineage?: (ref: InstanceRef) => JSX.Element | undefined
    baseRoute: string

}

export const InstanceLabel = React.memo((props: InternalProps) => {

   
    const logged = useLogged()
    const routedTypes = useRoutedTypes()
    const campaignstore = useCampaignStore()
        
    const { instance, sourceLabel, decorations = [], showCampaign, linkTarget,  tooltipForLineage, noLineage } = props

    if (!instance)
        return <UnknownLabel />

   
    const campaign = campaignstore.lookup(instance.campaign)

    if (!campaign)
        return <UnknownLabel tip={() => instance.source} />

    if (!props.noDecorations && props.mode !== 'light') {

        if (instance.properties?.note?.[logged.tenant])
            decorations.push(<Tooltip title={instance.properties.note[logged.tenant]}>{icns.note}</Tooltip>)
            

        if (!noLineage && instance.lineage && (!linkTarget || routedTypes.includes(linkTarget))) {
           
            const tip = tooltipForLineage?.(instance.lineage)

            //const tip = campaigns.isLoaded(instance.lineage.campaign) ? tooltipForLineage?.(instance.lineage) :  <CampaignLabel loadTrigger campaign={instance.lineage.campaign} /> 

            decorations.push(<Tooltip overlayClassName="long-tooltip" title={tip}>{icns.branch}</Tooltip>)
        }

    }

    return <span style={{ display: "flex", alignItems: "center" }}>
        {sourceLabel({ decorations })}
        {showCampaign && <>&nbsp;&nbsp;|&nbsp;&nbsp;<CampaignLabel campaign={campaign} /></>}
    </span>

},($, $$) => $.instance === $$.instance && sameLabel($, $$))