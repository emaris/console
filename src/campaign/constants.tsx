

import { Icon } from "antd";
import { tenantType } from '#app/tenant/constants';
import * as React from "react";
import { AiOutlineCheckSquare, AiOutlineClockCircle } from "react-icons/ai";
import { FiLayers } from "react-icons/fi";
import { MdSpaceDashboard } from 'react-icons/md';
import { RelativeDate } from "#campaign/event/model";
import { Campaign } from "./model";


// note: moved here from module, to avoid cycles. after all, they're broadly used constants.
export const campaignSingular = "campaign.module.name_singular"
export const campaignPlural = "campaign.module.name_plural"

export const complianceType="compliance"
export const complianceSingular = "campaign.compliance_module.name_singular"
export const compliancePlural = "campaign.compliance_module.name_plural"
export const complianceIcon = <Icon component={AiOutlineCheckSquare} />

export const timelinessType="timeliness"
export const timelinessSingular = "campaign.timeliness_module.name_singular"
export const timelinessPlural = "campaign.timeliness_module.name_plural"
export const timelinessIcon = <Icon component={AiOutlineClockCircle} />

export const campaignType="campaign"
export const campaignIcon=<Icon className="fi" component={FiLayers} />
export const campaignRoute="/campaign"

export const runningIcon = <Icon className="anticon-play running-icon" component={MdSpaceDashboard} />

export const splitId = (id:string | undefined ) => id ? id.split('@') : []

export const campaignDetailRoute = (cid:string) => `${campaignRoute}/${cid}`
export const campaignApiRoute = (c:Campaign) => `${campaignRoute}/${c.id}`


export const anyPartyTopic = { type: tenantType, name: '*' }

// predefined tags/categories


export const complianceScaleCategory = "TGC-compliance-defaultscale"
export const complianceValueCustomProp = 'compliance-value'
export const timelinessValueCustomProp = 'timeliness-value'

export const defaultRelativeDate = {kind: 'relative', period: {number: 1, unit: 'months', direction: 'before'}} as RelativeDate

// predefined event identifiers
export const campaignStartEvent = 'EV-campaign-start'
export const campaignEndEvent = 'EV-campaign-end'
export const requirementDeadlineEvent = 'EV-requirement-deadline'
export const productDeadlineEvent = 'EV-product-deadline'


export const defaultStatisticalHorizon = 30

export const defaultTimelinessScale = 'TGC-timeliness-defaultscale'

export const notReported = "layout.parameters.unreported"

export const subchangeMailTemplate = 'subchange'

export const subchangeMailTopic = 'submission'
export const subAssessedMailTopic = 'submission.assessed'
export const subAssessementRevokedMailTopic = 'submission.assessed.revoked'
export const subSubmittedMailTopic = 'submission.submitted'
export const subPendingApprovalMailTopic = 'submission.pendingapproval'
export const subRequestForChangeMailTopic = 'submission.requestchange'
export const subPublishedMailTopic = 'submission.published'
export const subSharedMailTopic = 'submission.shared'
export const dueDateMailTopic = 'event'
export const dueDateReminderMailTopic = 'event.reminder'