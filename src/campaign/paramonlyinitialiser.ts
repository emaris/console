
import { useIntegrationParameter } from '#integration/parameter'
import { useAssetParameter } from '#layout/parameters/asset'
import { useCampaignParameter } from '#layout/parameters/campaign'
import { useDateYearParam } from '#layout/parameters/dateyear'
import { useNumberParameter } from '#layout/parameters/number'
import { usePartyParameter } from '#layout/parameters/party'
import { useProductParameter } from '#layout/parameters/product'
import { useReferenceParameter } from '#layout/parameters/reference'
import { useRequirementParameter } from '#layout/parameters/requirement'
import { useTenantParameter } from '#layout/parameters/tenant'
import { useTextParameter } from '#layout/parameters/text'
import { useLayoutRegistry } from '#layout/registry'


export const useParameterOnlyInitialiser = () => {

    const registry = useLayoutRegistry()
    const textparam = useTextParameter()
    const numberparam = useNumberParameter()
    const dateyearparam = useDateYearParam()
    const integrationParam = useIntegrationParameter()

    const partyparam = usePartyParameter()
    const campaignparam = useCampaignParameter()
    const requirementParam = useRequirementParameter()

    const tenantparam = useTenantParameter()
    const referenceparam = useReferenceParameter()
    const requirementparam = useRequirementParameter()
    const productparam = useProductParameter()
    const assetparam = useAssetParameter()

    return () => {

        registry.addParameters([
            textparam,
            numberparam,
            dateyearparam,
            partyparam,
            campaignparam,
            requirementParam,
            integrationParam,
            textparam,
            numberparam,
            dateyearparam,
            tenantparam,
            partyparam,
            campaignparam,
            referenceparam,
            requirementparam,
            productparam,
            assetparam,
        ])
        
    }


}