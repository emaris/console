

import { ConfigContext, ConfigState } from '#app/config/context'
import { useConfig } from '#app/config/state'
import { useRoutedTypes } from '#config/model'
import { StateProvider } from 'apprise-frontend-core/state/provider'
import * as React from "react"
import { campaignType } from './constants'
import { CampaignContext } from './context'
import { Campaign } from "./model"
import { submissionType } from './submission/constants'


const CurrentContext = React.createContext<Campaign>(undefined!)

export const ModifiableCampaignContext = React.createContext<{ campaign: Campaign, setCampaign: (_: Campaign) => void }>(undefined!)


export const useCurrentCampaign = () => React.useContext(CurrentContext)

export const useModifiableCampaign = () => React.useContext(ModifiableCampaignContext)


// mounts the current campaign in context.
// if the campaign is archived, also overlayes the archived versions of its dependencies over application state. 
export const CurrentCampaignContext = (props: React.PropsWithChildren<{ campaign: Campaign }>) => {

    const { campaign } = props

    // first, overlay state if required.

    const base = React.useContext(CampaignContext)

    const archived = campaign.lifecycle.state === 'archived'

    const overlay = () => archived ?

        {
            ...base.get(), ...{

                requirements: base.get().campaigns.instances[campaign.id].archive?.requirements ?? { all: [], map: {} },
                products: base.get().campaigns.instances[campaign.id].archive?.products ?? { all: [], map: {} },
                tenants: base.get().campaigns.instances[campaign.id].archive?.tenants ?? { all: [], map: {} },
                events: base.get().campaigns.instances[campaign.id].archive?.events ?? { all: [], map: {} },

            }
        }

        : base.get()


    const baseConfig = useConfig().get()
    const routedTypes = useRoutedTypes()

    const configOverlay = () => archived ? { ...baseConfig, routedTypes: routedTypes.includes(campaignType) ? [submissionType, campaignType] : [submissionType] } : baseConfig


    // eslint-disable-next-line
    const state = React.useMemo(overlay, [archived, base, campaign])

    // eslint-disable-next-line
    const config = React.useMemo(configOverlay, [archived, baseConfig, campaign])

    // second, mount the overlayed app state so that useAppState() or useEmarisState() get it in lieu of the original one.
    // all hooks and APIs are based on those base hooks.

    return <CurrentContext.Provider value={campaign}>{

        archived ?

            <StateProvider initialState={{ config } as ConfigState} context={ConfigContext}>
                <StateProvider initialState={state} context={CampaignContext}>
                    {props.children}
                </StateProvider>
            </StateProvider>

            : props.children

    }</CurrentContext.Provider>
}

