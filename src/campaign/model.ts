import { buildinfo } from '#app/buildinfo'
import { useT } from '#app/intl/api'
import { Lifecycle, newLifecycle } from "#app/model/lifecycle"
import { MultilangDto, newMultiLang, useCompareMultiLang, useL } from "#app/model/multilang"
import { useSettings } from '#app/settings/store'
import { defaultContext } from "#app/system/constants"
import { TagExpression, TagMap, Tagged, newTagged } from "#app/tag/model"
import { useTagStore } from '#app/tag/store'
import { TenantAudience } from '#app/tenant/AudienceList'
import { useTime } from '#app/time/api'
import { compareDates, deepclone, shortid } from "#app/utils/common"
import { RelativeDate } from "#campaign/event/model"
import { useEventInstanceStore } from '#campaign/event/store'
import { ParameterRef } from '#layout/parameters/model'
import { Topic } from "#messages/model"
import { productType } from "#product/constants"
import { requirementType } from "#requirement/constants"
import moment from "moment-timezone"
import { useContext } from 'react'
import { campaignType, defaultRelativeDate, defaultTimelinessScale } from "./constants"
import { CampaignContext } from './context'
import { useCampaignStore } from './store'



export type Lineage = {

    lineage?: InstanceRef
}



export type InstanceRef = {
    source: string,
    campaign: string
}



export const isInstanceRef = (ci: any): ci is InstanceRef => ci && ci.campaign && ci.source && !(ci as CampaignInstance).instanceType

export type CampaignDto = Tagged & {

    id: string,

    guarded?: boolean
    predefined?: boolean

    lineage?: string,

    name: MultilangDto,
    description: MultilangDto,

    // note: we use events to track the key states (open/closed), state goes unused for now.
    lifecycle: Lifecycle<CampaignState>

    tags: string[],

    properties: CampaignProperties

    summary?: Summary

}

type CampaignState = 'active' | 'archived'




export type Campaign = CampaignDto & {

    loaded: boolean
}

export type CampaignProperties = Partial<CampaignSettings> & {

    note?: Record<string, string>
    defaultRelativeDate?: RelativeDate
    muted?: boolean
    statExcludeList?: string[]

}

export type CampaignSettings = {

    complianceScale?: string
    timelinessScale?: string
    approveCycle: ApproveCycle
    liveGuard?: boolean
    statisticalHorizon: number
    adminCanEdit: boolean
    adminCanSubmit: boolean
    partyCanSeeNotApplicable: boolean
    defaultRelativeDate: RelativeDate
    timeZone: string
    suspendOnEnd: boolean
    suspendSubmissions: boolean
}

export type CampaignAppSettings = Record<typeof campaignType, CampaignSettings>


export type ApproveCycle = typeof requirementType | typeof productType | 'all' | 'none'

export const defaultApprovalCycleOption =  'none'

export const approveCycleOptions: Record<ApproveCycle,string> = {
    [defaultApprovalCycleOption]: "campaign.fields.approval.options.none",
    all: "campaign.fields.approval.options.all",
    [requirementType]: "campaign.fields.approval.options.requirement",
    [productType]: "campaign.fields.approval.options.product"
}

export const approveCycleOptionDefaults: Record<ApproveCycle,string> = {
    none: "campaign.fields.approval.options.none_short",
    all: "campaign.fields.approval.options.all_short",
    [requirementType]: "campaign.fields.approval.options.requirement_short",
    [productType]: "campaign.fields.approval.options.product_short"
}


export type Summary = {

    startDate: string | undefined
    endDate: string | undefined
}

export type CampaignInstance = Tagged & Lineage & {

    id: string
    instanceType: string
    campaign: string
    source: string
    audience?: TagExpression
    audienceList?: TenantAudience
    userProfile?: TagExpression

    properties: InstanceProperties

}

// export type Overlay = { [name:string] : Parameter }
export type Overlay = Record<string, ParameterRef>

export type AssetInstanceProperties = InstanceProperties & {

    parameterOverlay?: Overlay
    editable?: boolean
    versionable?: boolean
    assessed?: boolean
    submissionTagMap?: TagMap[]
    version?: string | undefined
}

export type AssetInstance = CampaignInstance & {

    properties: AssetInstanceProperties
}


export type InstanceProperties = {

    note?: Record<string, string | undefined>

}


export const newCampaign = (campaignSettings: CampaignSettings) => {


    return {


        ...newTagged,
        id: undefined!,
        lifecycle: newLifecycle('active'),


        name: newMultiLang(),
        description: newMultiLang(),

        loaded: false,
        tags: [defaultContext],

        archive: false,

        properties: {
            ...campaignSettings,
            silent: false
        }

    } as Campaign
}

export const useNoCampaign = () => {

    const settings = useSettings().get<CampaignAppSettings>().campaign

    const campaign = newCampaign(settings)

    return { get: () => ({ ...campaign, id: 'unknown', lifecycle: {} }) }

}

export const noCampaignInstance = (type: string, campaign: string) => ({ id: undefined!, instanceType: type, notified: false, source: 'unknown', campaign, tags: [], properties: { defaultRelativeDate: defaultRelativeDate, editable: undefined, versionable: undefined, assessed: undefined, version: undefined } }) as CampaignInstance

export const newCampaignId = () => `C-${shortid()}`

export const unresolvedCampaignId = "unknown"

export const unresolvedCampaign: (id: string | undefined) => Campaign = id => ({
    ...newTagged,
    id: unresolvedCampaignId,
    name: { en: `Unknown campaign ${id}` },
    description: { en: `Unknown campaign ${id}` },
    lifecycle: newLifecycle('active'),
    loaded: false,
    archive: false,
    properties: {}
})


export const useCampaignStatus = () => {

    const state = useContext(CampaignContext)

    const t = useT()

    const model = useCampaignModel()

    const eventinststore = useEventInstanceStore()

    const time = useTime()

    const self = {

        startDate: (c: Campaign) => {

            return eventinststore.on(c).startDate() ?? c.summary?.startDate    // uses loaded data or summary from backend if data not loaded yet.

        }
        ,

        isStarted: (c: Campaign) => {

            const now = time.current()

            const startDate = self.startDate(c)

            return !!startDate && now.isAfter(startDate)
        }

        ,

        allStarted: () => {

            return state.get().campaigns.all?.filter(self.isStarted).sort(self.startedComparator) ?? []

        }
        ,



        startedComparator: (o1: Campaign, o2: Campaign) => {

            const started1 = self.startDate(o1)
            const started2 = self.startDate(o2)

            return started1 ? (started2 ? compareDates(started1, started2) : -1) : started2 ? 1 : compareDates(started1, started2)
        }

        ,

        endDate: (c: Campaign) => {

            return eventinststore.on(c).endDate() ?? c.summary?.endDate      // uses loaded data or summary from backend if data not loaded yet.

        }

        ,

        isRunning: (c: Campaign) => {

            const now = time.current()
            const start = self.startDate(c)
            const end = self.endDate(c)

            return !!start && moment(start).isBefore(now) && (!end || moment(end).isAfter(now))

        }

        ,

        isEnded: (c: Campaign) => {
            const endDate = self.endDate(c)
            return endDate ? moment(endDate).isBefore(time.current()) : false
        }

        ,

        isSuspended: (c: Campaign) => model.isSuspendSubmissions(c) || (self.isEnded(c) && model.isSuspendOnEnd(c))

        ,


        canArchive: (c: Campaign) => !model.isArchived(c) && self.isSuspended(c)

        ,

        openComparator: (o1: Campaign, o2: Campaign) => {

            const open1 = self.isRunning(o1)
            const open2 = self.isRunning(o2)

            // TODO: based on some temporal notion, to clarify.

            return open1 ? (open2 ? model.comparator(o1, o2) : -1) : open2 ? 1 : model.comparator(o1, o2)

        }


        ,

        status: (campaign?: Campaign, at?: moment.Moment) => {

            const now = at ?? time.current()

            const start = campaign ? self.startDate(campaign) : undefined
            const end = campaign ? self.endDate(campaign) : undefined

            const started = moment(start).isBefore(now)
            const ended = moment(end).isBefore(now)

            const label = started ? t(ended ? "campaign.lifecycle.closed_state" : "campaign.lifecycle.open_state") : t("campaign.lifecycle.design_state")

            return { label, start, end, planned: !started, started, ended, running: started && !ended }


        }
    }

    return self
}





export const useCampaignModel = () => {

    const l = useL()
    const compare = useCompareMultiLang()

    const settings = useSettings()

    const store = useCampaignStore()
    const tags = useTagStore()

    const self = {


        clone: (c: Campaign): Campaign => {

            const cloned = deepclone(c)

            return { ...cloned, id: undefined!, lineage: c.lineage, lifecycle: newLifecycle(), summary: undefined, properties: { ...cloned.properties, muted: true } }
        }

        ,

        branch: (c: Campaign): Campaign => ({ ...self.clone(c), lineage: c.id })

        ,

        comparator: (o1: Campaign, o2: Campaign) => compare(o1.name, o2.name)

        ,

        stringify: (c: Campaign) => `${l(c.name)} ${l(c.description) ?? ''}`

        ,


        stringifyRef: (c: string) => self.stringify(store.safeLookup(c))

        ,


        liveGuard: () => settings.get<CampaignAppSettings>().campaign?.liveGuard ?? !buildinfo

        ,


        approveCycle: (c: Campaign) => c.properties.approveCycle ?? defaultApprovalCycleOption


        ,

        setApprovalCycle: (c: Campaign, value: ApproveCycle | undefined) => {

            if (!value)
                delete c.properties.approveCycle

            else
                c.properties.approveCycle = value

        }

        ,


        complianceScale: (c: Campaign) => c.properties.complianceScale

        ,

        setComplianceScale: (c: Campaign, value: string | undefined) => {

            if (!value) {
                delete c.properties.complianceScale
                self.setTimelinessScale(c, undefined)
            }

            else
                c.properties.complianceScale = value

        }

        ,

        timelinessScale: (c: Campaign) => self.currentTimelinessScale(c).id

        ,

        setTimelinessScale: (c: Campaign, value: string | undefined) => {

            if (!value)
                delete c.properties.timelinessScale

            else
                c.properties.timelinessScale = value

        }

        ,

        // resolves tag ref and faces all clients except the config editor.
        currentComplianceScale: (c: Campaign) => c.properties.complianceScale ? tags.lookupCategory(c.properties.complianceScale) : undefined

        ,

        currentTimelinessScale: (c: Campaign) => c.properties.timelinessScale ? tags.lookupCategory(c.properties.timelinessScale) : tags.lookupCategory(defaultTimelinessScale)


        ,

        statisticalHorizon: (c: Campaign) => c.properties.statisticalHorizon

        ,

        setStatisticalHorizon: (c: Campaign, value: number | undefined) => {

            if (!value)
                delete c.properties.statisticalHorizon

            else
                c.properties.statisticalHorizon = value

        }

        ,

        isSuspendOnEnd: (c: Campaign) => c.properties.suspendOnEnd ?? settings.get<CampaignAppSettings>().campaign?.suspendOnEnd ?? true

        ,

        setSuspendOnEnd: (c: Campaign, value: boolean) => c.properties.suspendOnEnd = value

        ,

        isMuted: (c: Campaign) => c.properties.muted


        ,

        setMuted: (c: Campaign, value: boolean) => c.properties.muted = value

        ,

        isCampaignMuted: (c: Campaign) => c.properties.muted


        ,

        isSuspendSubmissions: (c: Campaign) => c.properties.suspendSubmissions

        ,



        setSuspendSubmissions: (c: Campaign, value: boolean) => c.properties.suspendSubmissions = value

        ,

        isArchived: (c: Campaign) => c.lifecycle.state === 'archived'

        ,

        setArchived: (c: Campaign) => c.lifecycle.state = 'archived'

        ,

        timeZone: (c: Campaign) => c.properties.timeZone

        ,

        setTimeZone: (c: Campaign, value: string | undefined) => {

            if (!value)
                delete c.properties.timeZone

            else
                c.properties.timeZone = value

        }
        ,

        defaultRelativeDate: (c: Campaign) => c.properties.defaultRelativeDate

        ,

        setDefaultRelativeDate: (c: Campaign, value: RelativeDate | undefined) => {
            if (!value)
                delete c.properties.defaultRelativeDate

            else
                c.properties.defaultRelativeDate = value
        }

        ,

        currentHorizon: (c: Campaign) => moment().add(self.statisticalHorizon(c), 'days')

        ,

        setPartyCanSeeNotApplicable: (c: Campaign, value: boolean) => {

            if (!value)
                delete c.properties.partyCanSeeNotApplicable

            else
                c.properties.partyCanSeeNotApplicable = true

        }

        ,

        topics: (c: Campaign): Topic[] => [{ type: campaignType, name: c.id }]

        ,


        canPartySeeNotApplicable: (c: Campaign) => !!c.properties.partyCanSeeNotApplicable


    }

    return self

}
