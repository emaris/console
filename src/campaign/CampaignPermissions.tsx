

import { ChangesProps } from '#app/iam/permission'
import { usePermissionDrawer } from '#app/iam/PermissionForm'
import { ResourceProps, StateProps, SubjectProps } from '#app/iam/PermissionTable'
import { useT } from '#app/intl/api'
import { useLocale } from '#app/model/hooks'
import { User } from '#app/user/model'
import { UserPermissions } from '#app/user/UserPermissionTable'
import * as React from 'react'
import { campaignActions } from './actions'
import { campaignPlural, campaignSingular, campaignType } from './constants'
import { CampaignLabel } from './Label'
import { Campaign } from './model'
import { useCampaignStore } from './store'


type PermissionsProps = ChangesProps & Partial<ResourceProps<Campaign>> & Partial<SubjectProps<User>> & Partial<StateProps<User,Campaign>> & {
    edited? : User
}


//  wraps UserPermissions for Requirements, so as to inject appropriate defaults.

export const CampaignPermissions =  (props:PermissionsProps) => {

    const t = useT()
    const {l} = useLocale()
    

    const campaignstore = useCampaignStore()
    

    return  <UserPermissions {...props}
                id="campaign-permissions" 
                resourceSingular={props.resourceSingular || t(campaignSingular)}
                resourcePlural={props.resourcePlural || t(campaignPlural)}
                renderResource={props.renderResource || ((t:Campaign) => <CampaignLabel campaign={t} /> )}
                resourceId={props.resourceId || ((r:Campaign) => r.id) }
                resourceText={t=>l(t.name)}
                renderResourceOption={props.renderResourceOption || ((r:Campaign) => <CampaignLabel noLink campaign={r} />)} 
                resourceRange={props.resourceRange || campaignstore.all() } 
                resourceType={campaignType}

                actions={Object.values(campaignActions)} />
                
}


export const useCampaignPermissions = () => usePermissionDrawer(CampaignPermissions, { types:[campaignType] } )