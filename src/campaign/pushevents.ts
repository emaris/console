import { useAskReloadOnPushChange, useAskReloadOnPushRemove } from '#app/push/constants';
import { Subscription } from '#app/push/model';
import { PushEventSlot } from '#app/push/module';
import { tenantRoute, tenantSingular, tenantType } from '#app/tenant/constants';
import { Tenant } from '#app/tenant/model';
import { useTenantStore } from '#app/tenant/store';
import { useLogin } from '#app/user/store';
import { Campaign } from '#campaign/model';
import { useDashboard, useRoutableCampaigns } from '#dashboard/api';
import { dashboardRoute } from '#dashboard/constants';
import { productSingular } from '#product/constants';
import { requirementSingular, requirementType } from '#requirement/constants';
import { unstable_batchedUpdates } from 'react-dom';
import shortid from 'shortid';
import { campaignRoute, campaignSingular, campaignType } from './constants';
import { CampaignInstance } from './model';
import { useCampaignUserPreferences } from './preferences/api';
import { useCampaignLoader, useCampaignRouting, useCampaignStore } from './store';
import { Trail } from './submission/model';

export type CampaignChangeEvent = {

    campaign: Campaign
    type: 'add' | 'remove' | 'change'

    instances: CampaignInstance[]
    instanceType: string

    trails: Trail[]
}



export type TenantChangeEvent = {

    tenant: Tenant
    type: 'add' | 'remove' | 'change'

}


export const usePushCampaignSlot = (): PushEventSlot => {


    const tenants = useTenantStore()
    const campaigns = useCampaignStore()

    const askReloadOnPushChange = useAskReloadOnPushChange()
    const askReloadOnPushRemove = useAskReloadOnPushRemove()

    const onInstanceChange = useOnInstanceChange()
    const onCampaignEvent = useOnCampaignEvent()

    return {

        onSubcribe: () => {
 

            console.log("subscribing for campaign and tenant changes...")

            return [

                {

                    topics: [campaignType]

                    ,

                    onEvent: (event: CampaignChangeEvent, history, location) => {

                        const { campaign, type, instances } = event

                        console.log(`received ${type} event for campaign ${campaign.name.en}...`, { event })

                        if (instances.length > 0)
                            return onInstanceChange(event, history, location)

                        return onCampaignEvent(event, history, location)
                    }

                    ,



                },

                {

                    topics: [tenantType]

                    ,

                    onEvent: (event: TenantChangeEvent, history, location) => {



                        const { tenant, type } = event

                        const currentUrl = `${location.pathname}${location.search}`

                        console.log(`received ${type} event for tenant ${tenant.name.en}...`, { event })


                        const detailOrDashBordOrTrailOnScreen = currentUrl.includes(tenant.id)
                        const detailOnScreen = detailOrDashBordOrTrailOnScreen && !currentUrl.includes(dashboardRoute)

                        let campaignArchived = false

                        if (detailOrDashBordOrTrailOnScreen) {
                            const campaignId = location.pathname.split("/")[2]
                            const campaign = campaigns.lookup(campaignId)
                            campaignArchived = campaignArchived || campaign?.lifecycle.state === 'archived'
                        }

                        switch (type) {

                            case 'add': {

                                tenants.setAll([...tenants.all(), tenant])

                                break
                            }


                            case 'change': {

                                const change = () => tenants.setAll(tenants.all().map(c => c.id === tenant.id ? tenant : c))
                                const changeAndRefresh = () => { change(); history.push(currentUrl, shortid()) } // uses location state to send down a signal for remount.

                                // note: we don't expect abrupt changes in trails, so we don't warn there.
                                if (detailOnScreen && !campaignArchived)
                                    askReloadOnPushChange({ singular: tenantSingular, onCancel: change, onOk: changeAndRefresh })

                                else change()

                                break
                            }


                            case 'remove': {

                                // when designing, we go back to list. in dshboard we go back to summary current campaign.
                                const fallbackRoute = detailOnScreen ? tenantRoute : dashboardRoute

                                const remove = () => tenants.setAll(tenants.all().filter(r => r.id !== tenant.id))
                                const leaveAndRemove = () => { history.push(fallbackRoute); remove() }

                                if (detailOrDashBordOrTrailOnScreen && !campaignArchived)
                                    askReloadOnPushRemove({ singular: tenantSingular, onOk: leaveAndRemove })

                                else remove()

                                break
                            }

                        }

                    }

                }



            ]
        }
    }
}

const useOnInstanceChange = (): Subscription<CampaignChangeEvent>['onEvent'] => {

    const askReloadOnPushChange = useAskReloadOnPushChange()
    const askReloadOnPushRemove = useAskReloadOnPushRemove()
    const cmpstore = useCampaignStore()
    const cmppreload = useCampaignLoader()
    const campaignrouting = useCampaignRouting()
    const dashboard = useDashboard()


    const preferences = useCampaignUserPreferences()

    const login = useLogin()


    return (event: CampaignChangeEvent, history, location) => {

        const { campaign, type, instances } = event

        const logged = login.logged()


        const pathname = location.pathname
        const currentUrl = `${pathname}${location.search}`



        const campaignDashboardRoute = dashboard.routeTo(campaign)

        let singular: string

        switch (instances[0].instanceType) {

            case tenantType: singular = tenantSingular; break;
            case requirementType: singular = requirementSingular; break;
            default: singular = productSingular; break;
        }

        const detailOnScreen = currentUrl.includes(campaignrouting.routeTo(campaign)) && instances.some(i => currentUrl.includes(i.id))
        const dashboardOrTrailOnScreen = currentUrl.includes(dashboard.routeTo(campaign)) && instances.some(i => currentUrl.includes(i.source))

        switch (type) {

            case 'change': {


                // we shouldn't add a campaign until the tenant's party is in it.
                if (logged.isTenantUser() && event.instanceType === tenantType) {

                    // buffer the updates required to add the campaign and load its data in the background.
                    // then flushes the buffer for a single re-render.
                    const updates: (() => void)[] = []

                    const tenantInstance = event.instances.find(i => i.source === logged.tenant)

                    if (tenantInstance) {

                        updates.push(() => cmpstore.setAll([...cmpstore.all(), campaign]))

                        cmppreload.fetchManyQuietly([campaign], false, updates).then(() =>

                            unstable_batchedUpdates(() => updates.forEach(f => f()))

                        )

                        return

                    }

                }

                const change = () => cmppreload.livePush(event)
                const changeAndRefresh = () => { change(); history.push(currentUrl, shortid()) }

                // change applies to instances.
                if (detailOnScreen)
                    askReloadOnPushChange({ singular, onOk: changeAndRefresh })

                else change()

                break
            }

            case 'remove': {

                // in design, just close the instance drawer (remove query and keep pathname) 
                // in dashboard, go back to campaign dashboard.
                const fallbackRoute = detailOnScreen ? pathname : campaignDashboardRoute

                const removeWholeCampaign = logged.isTenantUser() && event.instanceType === tenantType

                const remove = () => cmppreload.livePush(event)

                const leaveAndRemove = () => {

                    if (removeWholeCampaign) {

                        preferences.setLastVisitedCampaign(logged, undefined!)
                            .then(() => cmpstore.setAll(cmpstore.all().filter(c => c.id !== campaign.id)))
                            .then(() => history.push(dashboardRoute))

                    }
                    else {

                        history.push(fallbackRoute)

                        remove()
                    }
                }

                // remove applies to instances.
                if (detailOnScreen || dashboardOrTrailOnScreen)
                    askReloadOnPushRemove({ singular, onOk: leaveAndRemove })

                else remove()

                break
            }

        }

    }
}


const useOnCampaignEvent = (): Subscription<CampaignChangeEvent>['onEvent'] => {

    const askReloadOnPushChange = useAskReloadOnPushChange()
    const askReloadOnPushRemove = useAskReloadOnPushRemove()

    const cmpstore = useCampaignStore()
    const cmppreload = useCampaignLoader()
    const campaignrouting = useCampaignRouting()
    const routables = useRoutableCampaigns()

    const cmpprefs = useCampaignUserPreferences()
    const login = useLogin()


    return (event: CampaignChangeEvent, history, location) => {

        const { campaign, type } = event

        const currentUrl = `${location.pathname}${location.search}`

        const logged = login.logged()

        const detailOnScreen = currentUrl.includes(campaignrouting.routeTo(campaign))
        const dashboardOnScreen = currentUrl.includes(routables.routeTo(campaign))


        switch (type) {

            case 'add': {

                // buffer the updates required to add the campaign and load its data in the background.
                // then flushes the buffer for a single re-render.
                const updates: (() => void)[] = []

                // we shouldn't add a campaign until the tenant's party is in it.
                if (logged.isTenantUser())
                    return

                updates.push(() => cmpstore.setAll([...cmpstore.all(), campaign]))

                cmppreload.fetchManyQuietly([campaign], false, updates).then(() =>

                    unstable_batchedUpdates(() => updates.forEach(f => f()))

                )

                break
            }

            case 'change': {

                const change = () => cmpstore.setAll(cmpstore.all().map(c => c.id === campaign.id ? campaign : c))
                const changeAndRefresh = () => { change(); history.push(currentUrl, shortid()) } // uses location state to send down a signal for remount.

                if (detailOnScreen)
                    askReloadOnPushChange({ singular: campaignSingular, onCancel: change, onOk: changeAndRefresh })

                else change()

                break
            }

            case 'remove': {

                const fallbackRoute = detailOnScreen ? campaignRoute : dashboardRoute

                const remove = () => {

                    if (routables.getDefault()?.id === campaign.id)
                        cmpprefs.setLastVisitedCampaign(logged, undefined!)

                    cmpstore.setAll(cmpstore.all().filter(c => c.id !== campaign.id))


                }
                const leaveAndRemove = () => { history.push(fallbackRoute); remove() }

                if (detailOnScreen || dashboardOnScreen)
                    askReloadOnPushRemove({ singular: campaignSingular, onOk: leaveAndRemove })

                else remove()

                break

            }

        }

    }
}
