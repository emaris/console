
import { Label } from "#app/components/Label"
import { Form } from "#app/form/Form"
import { FormState } from "#app/form/hooks"
import { RadioBox } from "#app/form/RadioBox"
import { SliderBox } from "#app/form/SliderBox"
import { Switch } from "#app/form/Switch"
import { useT } from '#app/intl/api'
import { BaseSettings, SettingsProps } from "#app/settings/model"
import { CategoryBox } from "#app/tag/CategoryBox"
import { categoryLabelIcon } from "#app/tag/constants"
import { ZonePickerBox } from "#app/time/TimeZonePicker"
import { Validation } from "#app/utils/validation"
import { PureRelativeDateBox } from "#campaign/event/DateBox"
import { Icon, Radio } from "antd"
import moment from "moment-timezone"
import * as React from "react"
import { campaignType, complianceType, defaultRelativeDate, defaultStatisticalHorizon, defaultTimelinessScale, timelinessType } from "./constants"
import { approveCycleOptions, Campaign, CampaignAppSettings, CampaignSettings, useCampaignModel, useCampaignStatus } from "./model"
import { useSubmissionStore } from './submission/store'


type CampaignSettingsFieldsProps = {

    state: FormState<Campaign>
    report: Partial<Record<keyof CampaignSettings, Validation>>

}

const horizonSteps = 5

// setting fields to mount inside live drawer or design form.
export const CampaignSettingsFields = (props: CampaignSettingsFieldsProps) => {

    const t = useT()

    const { state, report } = props

    const { edited, change } = state

    const campaignmodel = useCampaignModel()
    const campaignstatus = useCampaignStatus()

    const substore = useSubmissionStore().on(edited)

    // may not have been loaded yet
    const assessmentsExist = substore.allTrails()?.flatMap(t => t.submissions).some(s => s.lifecycle.compliance?.state)
    const timelinessAssessmentExist = substore.allTrails()?.flatMap(t => t.submissions).some(s => s.lifecycle.compliance?.timeliness)

    const now = new Date()

    const maxHorizonDays = moment(now).add(1.5, 'year').diff(moment(now), 'days')

    const computedMaxHorizonDays = maxHorizonDays - (maxHorizonDays % horizonSteps)

    const campaignEnd = campaignstatus.endDate(edited)

    return <React.Fragment>

        <CategoryBox disabled={assessmentsExist} label={t("campaign.fields.compliance_scale.name")} validation={report.complianceScale}
            type={complianceType}
            allowClear
            undefinedOption={<Label icon={<Icon style={{ color: 'lightgray' }} component={categoryLabelIcon} />} title={t("campaign.fields.compliance_scale.no_scale")} />}
            onChange={change(campaignmodel.setComplianceScale)}>

            {campaignmodel.complianceScale(edited)}

        </CategoryBox>

        <CategoryBox disabled={timelinessAssessmentExist} label={t("campaign.fields.timeliness_scale.name")} validation={report.timelinessScale}
            type={timelinessType}
            allowClear
            onChange={change(campaignmodel.setTimelinessScale)}>

            {campaignmodel.timelinessScale(edited)}

        </CategoryBox>

        <RadioBox label={t("campaign.fields.approval.name")} validation={report.approveCycle}
            value={campaignmodel.approveCycle(edited)}
            onChange={change((t, v) => campaignmodel.setApprovalCycle(t, v === 'none' ? undefined : v))}>
            {
                Object.keys(approveCycleOptions).map(o =>

                    <Radio key={o} value={o}>{t(approveCycleOptions[o])}.</Radio>)

            }
        </RadioBox>

        <Switch label={t("campaign.fields.party_can_see_not_applicable.name")} validation={report.partyCanSeeNotApplicable} onChange={change(campaignmodel.setPartyCanSeeNotApplicable)}>{campaignmodel.canPartySeeNotApplicable(edited)}</Switch>

        <SliderBox width={300}
            label={t("campaign.fields.statistical_horizon.name")}
            validation={report.statisticalHorizon}
            tipFormatter={days => (campaignEnd && days === computedMaxHorizonDays) ? t("campaign.labels.end_of_campaign") : t("campaign.fields.statistical_horizon.slider_tip", { days })}
            step={horizonSteps}
            max={computedMaxHorizonDays}
            value={campaignmodel.statisticalHorizon(edited)}
            onChange={change(campaignmodel.setStatisticalHorizon)}
            showValuesFormatter={(days: number) => t("dashboard.summary.performance.horizon.reminder", { horizon: days })}
        />

        <Switch label={t("campaign.fields.suspend_on_end.name")} validation={report.suspendOnEnd} onChange={change(campaignmodel.setSuspendOnEnd)}>{campaignmodel.isSuspendOnEnd(edited)}</Switch>

        <ZonePickerBox
            value={edited.properties.timeZone}
            onChange={change((t, v) => campaignmodel.setTimeZone(t, v))}
        />

        <PureRelativeDateBox
            asFormField
            mode='relative'
            label={t("campaign.fields.default_relative_date.name")}
            validation={report.defaultRelativeDate}
            relativeDate={campaignmodel.defaultRelativeDate(edited) || defaultRelativeDate}
            onChange={change(campaignmodel.setDefaultRelativeDate)} />


    </React.Fragment>

}

export type ApplicationSettings = BaseSettings & CampaignAppSettings

export const ApplicationSettingsFields = (props: SettingsProps<ApplicationSettings>) => {

    const t = useT()

    const { state, report } = props

    const { edited, change } = state

    const now = new Date()

    const maxHorizonDays = moment(now).add(1.5, 'year').diff(moment(now), 'days')

    const computedMaxHorizonDays = maxHorizonDays - (maxHorizonDays % horizonSteps)

    return <Form state={state} sidebar>

        <CategoryBox label={t("campaign.fields.compliance_scale.name")} validation={report.complianceScale}
            type={complianceType}
            allowClear
            undefinedOption={<Label icon={<Icon style={{ color: 'lightgray' }} component={categoryLabelIcon} />} title={t("campaign.fields.compliance_scale.no_scale")} />}
            onChange={change((t, v) => { v ? t[campaignType].complianceScale = v : delete t[campaignType].complianceScale })}>

            {edited.campaign.complianceScale}

        </CategoryBox>

        <CategoryBox label={t("campaign.fields.timeliness_scale.name")} validation={report.timelinessScale}
            type={timelinessType}
            allowClear
            onChange={change((t, v) => { v ? t[campaignType].timelinessScale = v : delete t[campaignType].timelinessScale })}>

            {edited.campaign.timelinessScale ?? defaultTimelinessScale}

        </CategoryBox>

        <Switch label={t("campaign.fields.liveguard.name")} validation={report.liveguard} onChange={change((t, v) => t[campaignType].liveGuard = v)}>{edited[campaignType].liveGuard}</Switch>

        <RadioBox label={t("campaign.fields.approval.name")} validation={report.approveCycle}
            value={edited[campaignType].approveCycle ?? 'none'}
            onChange={change((t, v) => t[campaignType].approveCycle = v === 'none' ? undefined : v)}>
            {
                Object.keys(approveCycleOptions).map(o =>

                    <Radio key={o} value={o}>{t(approveCycleOptions[o])}.</Radio>)

            }
        </RadioBox>

        <Switch label={t("campaign.fields.admin_can_edit.name")} validation={report.adminCanEdit} onChange={change((t, v) => t[campaignType].adminCanEdit = v)}>{edited[campaignType].adminCanEdit}</Switch>
        <Switch label={t("campaign.fields.admin_can_submit.name")} validation={report.adminCanSubmit} onChange={change((t, v) => t[campaignType].adminCanSubmit = v)}>{edited[campaignType].adminCanSubmit}</Switch>


        <SliderBox width={300} label={t("campaign.fields.statistical_horizon.name")}
            validation={report.statisticalHorizon}
            tipFormatter={days => days === computedMaxHorizonDays ? t("campaign.labels.end_of_campaign") : t("campaign.fields.statistical_horizon.slider_tip", { days })}
            step={horizonSteps}
            max={computedMaxHorizonDays}
            value={edited[campaignType].statisticalHorizon ?? defaultStatisticalHorizon}
            onChange={change((t, v) => t[campaignType].statisticalHorizon = v)}
            showValuesFormatter={(days: number) => t("dashboard.summary.performance.horizon.reminder", { horizon: days })}
        />

        <Switch label={t("campaign.fields.suspend_on_end.name")} validation={report.suspendOnEnd} onChange={change((t, v) => t[campaignType].suspendOnEnd = v)}>{edited[campaignType].suspendOnEnd ?? true}</Switch>

        <ZonePickerBox
            value={edited[campaignType].timeZone}
            onChange={change((t, v) => t[campaignType].timeZone = v)}
            validation={report.defaultTimeZone}
        />

        <PureRelativeDateBox
            asFormField
            mode='relative'
            label={t("campaign.fields.default_relative_date.name")}
            validation={report.defaultRelativeDate}
            relativeDate={edited[campaignType].defaultRelativeDate}
            onChange={change((t, v) => t[campaignType].defaultRelativeDate = v)} />


    </Form>
}