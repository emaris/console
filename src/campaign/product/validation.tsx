
import { NamedAsset } from '#layout/components/assetsection'
import { productPlural, productSingular, productType } from "#product/constants"
import { ProductLabel } from '#product/Label'
import { useProductStore } from '#product/store'
import { requirementType } from '#requirement/constants'
import { RequirementLabel } from '#requirement/Label'
import { useT } from '#app/intl/api'
import { useTagStore } from '#app/tag/store'
import { tenantPlural } from '#app/tenant/constants'
import { userPlural } from '#app/user/constants'
import { checkIt, Validation, withReport } from "#app/utils/validation"
import { Campaign } from "../model"
import { useRequirementInstances } from '../requirement/api'
import { ProductInstance } from "./api"
import { useProductInstances } from './api'

export const useProductInstanceValidation = () => {

    const t = useT()

    const reqinstances = useRequirementInstances()
    const prodinstances = useProductInstances()

    const products = useProductStore()
    

    const { validateCategories } = useTagStore()

    const singular = t(productSingular).toLowerCase()
    const plural = t(productPlural).toLowerCase()


    const partyPlural = t(tenantPlural).toLowerCase()

    return {
        on: (c: Campaign) => {


            const validateProfile = (edited: ProductInstance) => ({

                note: checkIt().nowOr(t("campaign.fields.note.msg"))

                ,

                lineage: checkIt().nowOr(t("common.fields.lineage.msg_nochoice", { singular }))

                ,

                source: checkIt().nowOr(t("campaign.fields.source.msg", { singular }))

                ,

                audience: checkIt().nowOr(
                    t("common.fields.audience.msg"),
                    t("common.fields.audience.help", { singular, plural: partyPlural })
                ),

                userProfile: checkIt().nowOr(
                    t("common.fields.user_profile.msg"),
                    t("common.fields.user_profile.help", { singular, plural: userPlural })
                )

                ,

                editable: checkIt().nowOr(
                    t("common.fields.editable.help", { plural, parties: partyPlural })
                )

                ,

                versionable: checkIt().nowOr(
                    t("common.fields.versionable.help", { plural, parties: partyPlural })
                )

                ,

                assessed: checkIt().nowOr(
                    t("common.fields.assessed.help", { plural })
                )

                ,

                ...validateCategories(edited.tags).for(productType),

                ...validateCategories(edited.properties.submissionTagMap?.flatMap(tagmap => tagmap.tags)).include(...(edited.properties.submissionTagMap?.map(tagmap => tagmap.category) ?? []))

            })


            const validateAssetDependencies = (edited: ProductInstance): Record<string, Validation> => {


                const instances = [...reqinstances.on(c).all(), ...prodinstances.on(c).all()]

                const product = products.safeLookup(edited.source!)
                const dependencies = product.properties.dependencies ?? []

                const labelOf = (asset: NamedAsset) => asset.type === requirementType ? <RequirementLabel noDecorations requirement={asset.asset} /> : <ProductLabel noDecorations product={asset.asset} />

                return dependencies
                    .filter(({ asset }) => !instances.some(i => i.source === asset))
                    .map(asset => ({
                        id: asset.asset, validation: {
                            status: "error",
                            msg: labelOf(asset)

                        } as Validation
                    }))
                    .reduce((acc, { id, validation }) => ({ [id]: validation, ...acc }), {} as Record<string, Validation>)
            }



            const self = {

                validateInstanceProfile: (edited: ProductInstance) => withReport(validateProfile(edited))

                ,

                validateAssetDependencies

                ,

                validateInstance: (edited: ProductInstance) => withReport({

                    ...validateAssetDependencies(edited),

                    ...validateProfile(edited)
                })

            }

            return self

        }
    }
}
