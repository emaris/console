import { Label, UnknownLabel } from "#app/components/Label"
import { icns } from "#app/icons"
import { useT } from '#app/intl/api'
import { TimeLabel } from "#app/time/Label"
import { useCampaignStore } from '#campaign/store'
import { useRoutedTypes } from '#config/model'
import { useDashboard } from '#dashboard/api'
import { useCampaignMode } from "#dashboard/hooks"
import { ProductLabel } from "#product/Label"
import { productRoute } from "#product/constants"
import { Tooltip } from "antd"
import { Link } from "react-router-dom"
import { InstanceLabel, InstanceLabelProps } from "../InstanceLabel"
import { CampaignLabel } from "../Label"
import { campaignSingular, campaignType, runningIcon } from "../constants"
import { useEventInstanceStore } from '../event/store'
import { InstanceRef, isInstanceRef } from "../model"
import { submissionType } from "../submission/constants"
import { ProductInstance, useProductInstances } from "./api"
import { isAbsolute } from "#campaign/event/model"

type Props = InstanceLabelProps & {

    instance: ProductInstance | InstanceRef | undefined
    link?: boolean

    deadlineOnly?: boolean
    showTitle?: boolean
    tipTitle?: boolean
}

const emptySpace = <div style={{ width: 13 }} />

export const ProductInstanceLabel = (props: Props) => {

    const routedTypes = useRoutedTypes()
    const dashboard = useDashboard()

    const campaigns = useCampaignStore()
    const campaignMode = useCampaignMode()
    const prodinst = useProductInstances()
    const eventinstancestore = useEventInstanceStore()

    const { instance, linkTo, linkTarget, options = [], showCampaign, deadlineOnly, showTitle = false, tipTitle = false, ...rest } = props

    let noVersion = false

    const t = useT()

    const campaign = campaigns.lookup(instance?.campaign)

    if (!campaign)
        return <UnknownLabel title={t("common.labels.unknown_one", { singular: t(campaignSingular).toLowerCase() })} />

   
    const campaignTimeZone = campaign.properties.timeZone ? { 'original': campaign.properties.timeZone } : undefined


    var productinstance = instance as ProductInstance;

    if (isInstanceRef(instance))

        if (campaigns.isLoaded(instance.campaign))
            productinstance = prodinst.on(campaign).lookupLineage(instance)!
        else
            return <CampaignLabel campaign={instance.campaign} />

    if (!productinstance)
        return <UnknownLabel {...props} />

   
    if (deadlineOnly) {

        const events = eventinstancestore.on(campaign)
 
        const deadline = events.deadlineOfProduct(productinstance)

        if (deadline) {

            if (isAbsolute(deadline.date) && deadline.date.none) {
                return <Label {...props} noIcon noLink title={t("common.labels.no_due_date")} disabled />
            }

            const date = events.absolute(deadline.date)
            
            return date ? <TimeLabel accentFrame='weeks' format='numbers' render={props.mode === 'tag' ? date => t('campaign.date.due', { date }) : undefined} value={date} timezones={campaignTimeZone} {...rest} />
                : <Label {...props} noIcon noLink title={t("common.labels.unknown_due_date")} disabled />
        }
        else return <Label {...props} noIcon noLink title={t("common.labels.no_due_date")} disabled />

    }
    // console.log(productType, submissionType, linkTarget, linkTo)

    if (campaignMode === 'dashboard') {
        noVersion = true
        if (routedTypes.includes(campaignType))
            options.push(<Tooltip title={t("campaign.labels.campaign_view.design")}><Link to={() => prodinst.on(campaign).route(productinstance)}>{icns.edit}</Link></Tooltip>)
    }
    else
        options.push(emptySpace)

    if (campaignMode === 'design' && routedTypes.includes(submissionType))
        options.push(<Tooltip title={t("campaign.labels.campaign_view.live")}><Link to={() => dashboard.on(campaign).routeToAsset(productinstance)}>{runningIcon}</Link></Tooltip>)

    return <InstanceLabel instance={productinstance} noMemo={props.noMemo} tooltipForLineage={ref => <ProductInstanceLabel showCampaign instance={ref} />}
        baseRoute={productRoute}
        {...rest}
        sourceLabel={props => <ProductLabel {...props} {...rest} noMemo={props.noMemo} showTitle={showTitle} tipTitle={tipTitle} options={options} noVersion={noVersion} linkTo={linkTo || (() => prodinst.on(campaign).route(productinstance))} linkTarget={submissionType} product={instance?.source} />} />


}
