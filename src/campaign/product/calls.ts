import { useCalls } from "#app/call/call";
import { domainService } from "../../constants";
import { Campaign } from "../model";
import { ProductInstance } from "./api";


const products = `/productinstance`

export const useProductInstanceCalls = () => {


    const { at } = useCalls()

    return {
        on: (c: Campaign | string) => {

            const cid = typeof c === 'string' ? c : c.id

            return {

                fetchAll: (): Promise<ProductInstance[]> => at(`${products}/search`, domainService).post({ campaign: cid })

                ,

                fetchOne: (id: string): Promise<ProductInstance> => at(`${products}/${id}?byref=true`, domainService).get()

                ,

                addAll: (instances: ProductInstance[]): Promise<ProductInstance[]> => at(products, domainService).post(instances)

                ,

                updateOne: (instance: ProductInstance): Promise<void> => at(`${products}/${instance.id}`, domainService).put(instance)

                ,

                removeOne: (instance: ProductInstance): Promise<void> => at(`${products}/${instance.id}`, domainService).delete()

                ,

                removeMany: (instances: ProductInstance[]): Promise<void> => at(`${products}/remove`, domainService).post(instances.map(i => i.id))

            }
        }
    }
}

