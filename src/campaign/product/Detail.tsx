
import { Button } from "#app/components/Button"
import { Drawer } from "#app/components/Drawer"
import { Form } from "#app/form/Form"
import { NoteBox } from "#app/form/NoteBox"
import { SimpleBox } from "#app/form/SimpleBox"
import { Switch } from "#app/form/Switch"
import { FormState, useFormState } from "#app/form/hooks"
import { icns } from "#app/icons"
import { useT } from '#app/intl/api'
import { useLocale } from "#app/model/hooks"
import { Tab } from "#app/scaffold/Tab"
import { Topbar } from "#app/scaffold/Topbar"
import { BytestreamedContext } from '#app/stream/BytestreamedHelper'
import { TagBoxset } from "#app/tag/TagBoxset"
import { TagMapBox } from "#app/tag/TagMapBox"
import { TagRefBox } from "#app/tag/TagRefBox"
import { AudienceList } from "#app/tenant/AudienceList"
import { tenantIcon, tenantType } from "#app/tenant/constants"
import { userType } from '#app/user/constants'
import { useLogged } from '#app/user/store'
import { through } from "#app/utils/common"
import { paramsInQuery } from "#app/utils/routes"
import { AssetOverlayForm } from "#campaign/AssetOverlayForm"
import { EventList } from "#campaign/event/EventList"
import { EventInstance } from "#campaign/event/model"
import { useEventInstanceDates, useEventInstanceStore } from '#campaign/event/store'
import { useCurrentCampaign } from "#campaign/hooks"
import { ProductInstanceLabel } from "#campaign/product/Label"
import { ProductInstance, useProductInstances } from "#campaign/product/api"
import { useProductInstanceValidation } from '#campaign/product/validation'
import { submissionType } from "#campaign/submission/constants"
import { useDashboard } from '#dashboard/api'
import { instanceFrom } from '#layout/model'
import { predefinedProductParameterIds } from '#layout/parameters/constants'
import { paramIcon } from '#layout/parameters/model'
import { LayoutProvider } from '#layout/provider'
import { ProductDetailLoader } from '#product/DetailLoader'
import { ProductLabel } from "#product/Label"
import { ProductRequirementList } from "#product/RequirementList"
import { productType } from "#product/constants"
import { useProductStore } from '#product/store'
import { requirementIcon, requirementPlural } from "#requirement/constants"
import * as React from "react"
import { useHistory, useLocation } from "react-router-dom"
import { useProductInstanceClient } from './client'

import { useRoutedTypes } from '#config/model'
import { useProductLayoutInitialiser } from '#product/layoutinitialiser'
import './styles.scss'



type Props = {

    detail: ProductInstance
    onClose: () => void

    width?: number
}

export const ProductInstanceDetail = (props: Props) => {

    const { detail } = props

    return <ProductDetailLoader id={detail.source}>{

        () => <InnerProductInstanceDetail {...props} />

    }</ProductDetailLoader>
}

const InnerProductInstanceDetail = (props: Props) => {

    const t = useT()
    const { l } = useLocale()
    const history = useHistory()
    const { search } = useLocation()

    const sources = useProductStore()
    const routedTypes = useRoutedTypes()
    const dashboard = useDashboard()

    const campaign = useCurrentCampaign()

    const { detail, width = 650, onClose } = props

    const products = { ...useProductInstanceValidation().on(campaign), ...useProductInstances().on(campaign), ...useProductInstanceClient().on(campaign) }
    const events = useEventInstanceStore().on(campaign)

    // existing events about this detail, including temporals.
    // eslint-disable-next-line
    const initialRelatedEvents = events.allAboutWithTemporals(detail.source)

    type FormData = { instance: ProductInstance, relatedEvents: EventInstance[] }

    const formstate = useFormState<FormData>({ instance: detail, relatedEvents: initialRelatedEvents })

    const source = sources.safeLookup(detail.source)

    const { edited, initial, dirty, change, reset, softReset } = formstate

    // resyncs on external changes (push events)
    // eslint-disable-next-line
    React.useEffect(() => softReset({ instance, relatedEvents: initialRelatedEvents }), [events.all()])


    const { instance, relatedEvents } = formstate.edited

    const liveBtn = <Button enabledOnReadOnly iconLeft icn={icns.edit} onClick={() => history.push(dashboard.on(campaign).routeToAsset(edited.instance))}>{t("campaign.labels.campaign_view.live")}</Button>


    const onSave = () => events.replaceAllAbout(relatedEvents, detail.source)
        .then(through(_ => products.save(instance, initial.instance)))
        .then(relatedEvents => softReset({ ...edited, relatedEvents }))

    const revert = <Button icn={icns.revert} enabled={dirty} onClick={() => reset()}>{t("common.buttons.revert")}</Button>
    const saveact = <Button type="primary" icn={icns.save} enabled={dirty} onClick={onSave}>{t("common.buttons.save")}</Button>

    const pluralRequirements = t(requirementPlural)


    const { tab } = paramsInQuery(search)

    const profileReport = products.validateInstanceProfile(instance)
    const requirementReport = { errors: () => 0 }

    const report = { ...profileReport, ...requirementReport }

    const tabcompo = (() => {

        switch (tab) {

            case pluralRequirements: return <ProductRequirementList product={source} report={report} />

            case 'overlay': return (
                <BytestreamedContext>
                    <LayoutProvider useInitialiser={useProductLayoutInitialiser} layout={instanceFrom(source.properties.layout)} onChange={() => {/* no real change intended here, only overlays. */ }}>
                        <AssetOverlayForm
                            instance={edited.instance}
                            excludes={p => predefinedProductParameterIds.includes(p.id)}
                            onChange={instance => change((t, v) => t.instance = v)(instance)} />
                    </LayoutProvider>
                </BytestreamedContext>

            )



            default: return <GeneralForm report={report} {...formstate} />
        }
    })()

    return <Drawer readonly={!edited.instance.source} renderAfterTransition routeId={products.detailParam()} visible width={width} warnOnClose={dirty} title={t("common.buttons.edit_one", { singular: l(source?.name!) })} icon={tenantIcon} onClose={onClose}>

        <div style={{ height: "100%", display: "flex", flexDirection: "column" }} >

            <Topbar offset={63}>

                <Tab default id={"info"} icon={icns.form} name={t("common.labels.general")} badge={profileReport.errors() > 0} />
                <Tab id={'overlay'} icon={paramIcon} name={t('layout.parameters.plural')} />
                <Tab id={pluralRequirements} icon={requirementIcon} name={pluralRequirements} badge={requirementReport.errors() > 0} />

                {saveact}
                {revert}
                {routedTypes.includes(submissionType) && liveBtn}

            </Topbar>

            {/* tabcompo can stretch 100% if it wants to (e.g. a table) */}
            <div style={{ flexGrow: 1 }}>

                {tabcompo}

            </div>

        </div>


    </Drawer>
}


type FormProps = FormState<{ instance: ProductInstance, relatedEvents: EventInstance[] }> & {

    report: any
}


const GeneralForm = (props: FormProps) => {

    const { edited, change, report, dirty } = props

    const { instance, relatedEvents } = edited

    const t = useT()

    const logged = useLogged()

    const campaign = useCurrentCampaign()

    const events = useEventInstanceDates().on(campaign)
    const instances = useProductInstanceValidation().on(campaign)

    const onScreenEvents = dirty ? relatedEvents : [...relatedEvents].sort(events.temporalAwareDateComparatorOver(relatedEvents))

    const errors = React.useMemo(() => instances.validateAssetDependencies(edited.instance)

        //eslint-disable-next-line
        , [edited.instance])

    return <Form state={props} >

        {Object.values(errors).length > 0 &&

            <div className='missing-dependencies-list'>
                <div className='list-title'>{icns.error()}&nbsp;{t('campaign.feedback.missing_dependencies_errors')}</div>
                <ul>
                    {Object.values(errors).map((error, id) =>

                        <li key={id}>{error.msg}</li>

                    )}
                </ul>
            </div>

        }

        <EventList instance={instance} events={onScreenEvents} onChange={change((e, newevents) => { e.relatedEvents = newevents })} />

        <TagRefBox mode="multi" label={t("common.fields.audience.name")} validation={report.audience} expression={edited.instance.audience} type={tenantType} onChange={change((t, v) => t.instance.audience = v)} />

        <AudienceList label={t("common.fields.audience_list.name")} validation={report.audienceList} onChange={change((t, v) => t.instance.audienceList = v)}>{edited.instance.audienceList}</AudienceList>

        <TagRefBox mode="multi" label={t("common.fields.user_profile.name")} validation={report.userProfile} expression={edited.instance.userProfile} type={userType} onChange={change((t, v) => t.instance.userProfile = v)} />


        <TagBoxset edited={instance.tags} type={productType} validation={report} onChange={change((t, v) => t.instance.tags = v)} />

        <TagMapBox type={submissionType} className="instance-tag-boxes" label={t("product.fields.tagmap.label")} singleLabel={t("product.fields.tagmap.add")} validationReport={report} onChange={change((t, v) => t.instance.properties.submissionTagMap = v)}>{edited.instance.properties.submissionTagMap}</TagMapBox>

        <NoteBox label={t("common.fields.note.name")} validation={report.note} autoSize={{ minRows: 4, maxRows: 6 }} onChange={change((t, v) => t.instance.properties.note = t.instance.properties.note ? { ...t.instance.properties.note, [logged.tenant]: v } : { [logged.tenant]: v })} >
            {instance.properties.note}
        </NoteBox>

        <SimpleBox label={t("campaign.fields.source.name")} validation={report.source} >
            <ProductLabel product={instance.source} />
        </SimpleBox>

        {instance.lineage &&

            <SimpleBox label={t("common.fields.lineage.name")} validation={report.lineage} >
                <ProductInstanceLabel showCampaign instance={instance.lineage} />
            </SimpleBox>

        }

        <Switch label={t("common.fields.editable.name")} onChange={change((u, v) => u.instance.properties.editable = v)} validation={report.editable}>
            {edited.instance.properties.editable !== undefined ? edited.instance.properties.editable : true}
        </Switch>
        
        <Switch label={t("common.fields.versionable.name")} onChange={change((u, v) => u.instance.properties.versionable = v)} validation={report.versionable}>
            {edited.instance.properties.versionable !== undefined ? edited.instance.properties.versionable : true}
        </Switch>

        {
            campaign.properties.complianceScale &&
            <Switch label={t("common.fields.assessed.name")} onChange={change((u, v) => u.instance.properties.assessed = v)} validation={report.assessed}>
                {edited.instance.properties.assessed !== undefined ? edited.instance.properties.assessed : true}
            </Switch>
        }

    </Form>
}