import { NoSuchRoute } from "#app/components/NoSuchRoute";
import { campaignRoute } from "#campaign/constants";
import { CampaignValidation } from "#campaign/validation";
import { productType } from "#product/constants";
import * as React from "react";
import { Route, Switch } from "react-router-dom";
import { ProductInstanceList } from "./List";


type Props = {

    report: ReturnType<CampaignValidation['validateProducts']>
}

export const ProductInstances = (props:Props)=> {

    return   <Switch>
                <Route exact path={`${campaignRoute}/:name/${productType}`} render={()=><ProductInstanceList {...props} />} />
                <NoSuchRoute /> 
            </Switch>
                

}