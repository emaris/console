
import { useLayoutRegistry } from '#layout/registry';
import { TagExpression, useTagModel } from '#app/tag/model';
import { noTenant } from '#app/tenant/constants';
import { TenantDto } from "#app/tenant/model";
import { User } from '#app/user/model';
import { deepclone, indexMap } from "#app/utils/common";
import { paramsInQuery, updateQuery } from "#app/utils/routes";
import { CampaignContext, CampaignNext } from '#campaign/context';
import { RequirementInstance } from '#campaign/requirement/api';
import { useCampaignRouting } from '#campaign/store';
import { Topic } from '#messages/model';
import { productType } from "#product/constants";
import { Product, useProductModel } from '#product/model';
import { useProductStore } from '#product/store';
import moment from 'moment-timezone';
import { useContext } from 'react';
import { productDeadlineEvent } from '../constants';
import { useEventInstanceStore } from '../event/store';
import { AssetInstance, AssetInstanceProperties, Campaign, CampaignInstance, InstanceRef, noCampaignInstance } from "../model";
import { PartyInstance, usePartyInstances } from "../party/api";
import { ParameterRef } from '#layout/parameters/model';


export type ProductInstanceState = {

    products: {
        all: ProductInstance[]
        map: Record<string, ProductInstance>
        sourceMap: Record<string, ProductInstance>
    }

}

export const initialProductInstances = (): ProductInstanceState => ({

    products: {
        all: undefined!,
        map: undefined!,
        sourceMap: undefined!
    }
})



export type ProductInstanceDto = AssetInstance & {

    // requirements: string[]
    properties: AssetInstanceProperties & {

        userProfile?: TagExpression

    }

}

export type ProductInstance = ProductInstanceDto       // future-proofs divergence from exchange model, starts, aligned.


export const useProductInstanceStore = () => {

    const state = useContext(CampaignContext)

   
    return {
        on: (c: Campaign) => {

            const self = {
            


                noInstance: () => ({

                    id: undefined!,
                    instanceType: productType,
                    source: "unknown",
                    campaign: c.id,
                    tags: [],
                    properties: {
                        editable: true,
                        versionable: true,
                        assessed: true,
                        version: undefined
                    }

                }) as ProductInstance
            
                ,

                all: () => state.get().campaigns.instances[c.id]?.products?.all                

                ,


                setAll: (instances: ProductInstance[], props?: { noOverwrite: boolean }) => state.set(s => {

                    const campaign = s.campaigns.instances[c.id] ?? {}
                    const products = campaign.products ?? initialProductInstances()

                    if (products.all && props?.noOverwrite)
                        return

                    s.campaigns.instances[c.id] = { ...campaign, products: { ...products, all: instances, map: indexMap(instances).by(i => i.id), sourceMap: indexMap(instances).by(i => i.source) } }

                })
                ,


                livePush: (instances: ProductInstance[], type: 'change' | 'remove') => {

                    const map = state.get().campaigns.instances[c.id]?.products.map ?? {}
                    let current = state.get().campaigns.instances[c.id]?.products.all ?? []

                    instances.forEach(instance => {

                        switch (type) {

                            case "change":
                                current = map[instance.id] ? current.map(i => i.id === instance.id ? instance : i) : [...current, instance]
                                break;

                            case "remove": current = current.filter(i => i.id !== instance.id); break

                        }

                    })

                    self.setAll(current)

                }


                ,

                lookup: (id: string | undefined) : RequirementInstance|undefined => id ? state.get().campaigns.instances[c.id]?.products?.map[id] : undefined

                ,

                safeLookup: (id: string | undefined) => (self.lookup(id) ?? self.noInstance()) as RequirementInstance

                ,

                lookupLineage: (ref: InstanceRef | undefined) => ref ? state.get().campaigns.instances[ref.campaign]?.products.all.find(p => p.source === ref.source) : undefined

                ,

                lookupBySource: (source: string | undefined) => source ? state.get().campaigns.instances[c.id]?.products?.sourceMap[source] : undefined
                //self.all().find(p => p.source !== undefined && p.source === source)

                ,

                safeLookupBySource: (source: string | undefined) => self.lookupBySource(source) ?? noCampaignInstance(productType, c.id) as ProductInstance

   
                ,

                generate: (product: Product): ProductInstance => ({
                    id: undefined!,
                    instanceType: productType,
                    source: product.id,
                    campaign: c.id,
                    //   requirements: deepclone(products.requirementsFor(product)),
                    tags: deepclone(product.tags),
                    audience: product.audience,
                    audienceList: product.audienceList,
                    userProfile: product.userProfile,
                    properties: {
                        note: product.properties.note,
                        editable: product.properties.editable,
                        versionable: product.properties.versionable,
                        assessed: product.properties.assessed,
                        submissionTagMap: product.properties.submissionTagMap,
                        version: product.properties.version
                    }
                })


                ,


                audienceOf: (p: ProductInstance) => self.safeLookup(p.source).audience

                ,

                topics: (p: CampaignInstance): Topic[] => [{ type: productType, name: `${c.id}:${p.source}` }]



            }

            return self
        }
    }
}


export const useProductInstances = () => {

    const store = useProductInstanceStore()

    const prodstore = useProductStore()
    const prodmodel = useProductModel()

    const tags = useTagModel()

    const eventinststore = useEventInstanceStore()
    const partyinst = usePartyInstances()
    const campaigns = useCampaignRouting()

    const layoutRegistry = useLayoutRegistry()

    return {
        on: (c: Campaign) => {

            const parties = partyinst.on(c)

            const self = {

                ...store.on(c)

                ,

                allSources: () => prodstore.all()

                ,

                lookupSource: (instance:string | ProductInstance | undefined) => prodstore.safeLookup(typeof instance === 'string' ? instance : instance?.source)


                ,


                detailParam: () => 'ri-drawer'

                ,

                // eslint-disable-next-line
                detailInRoute: () => paramsInQuery(location.search)[self.detailParam()] as string

                ,


                route: (p?: ProductInstance) =>

                    // eslint-disable-next-line
                    `${campaigns.routeTo(c)}/${productType}?${updateQuery(location.search).with(params => {

                        params[self.detailParam()] = p ? p.id : null
                        params['source'] = p?.source ?? null

                    })}`

              
                ,

                allSorted: () => {

                    return [...self.all() ?? []].sort(self.comparator)
                }

                ,

                allForParty: (party: PartyInstance, includeNotApplicable = parties.canSeeNotApplicable(party)) =>

                    includeNotApplicable ? self.allSorted() : self.allSorted().filter(pi => self.isForParty(party, pi))

                ,

                isForParty: (party: PartyInstance | TenantDto, ci: CampaignInstance) => {

                    const partyId = (party as PartyInstance).source ?? (party as TenantDto).id

                    const audienceListCheckInclude = ci.audienceList ? ci.audienceList.includes.includes(partyId) || ci.audienceList.includes.length === 0 : true
                    const audienceListCheckExclude = ci.audienceList ? !ci.audienceList.excludes.includes(partyId) || ci.audienceList.excludes.length === 0 : true

                    const audienceListCheck = audienceListCheckExclude && audienceListCheckInclude

                    return audienceListCheck && (ci.audience ? tags.expression(ci.audience).matches(party) : true)

                }
                ,


                pastDue: (party?: PartyInstance) => {

                    const now = Date.now()
                    
                    const { all, absolute } = eventinststore.on(c)

                    const targets = (party ? self.allForParty(party) : self.all()).map(r => r.source)

                    return all().filter(ei => ei.source === productDeadlineEvent
                        && targets.includes(ei.target!)
                        && ei.date && moment(new Date(absolute(ei.date)!)).isBefore(now))
                }

                
                ,

                stringify: (i: ProductInstance) => self.stringifySourceRef(i.source)

                ,

                stringifySourceRef: prodmodel.stringifyRef

                ,

                stringifySource: prodmodel.stringify


                ,


                clone: (instance: ProductInstance, campaign: CampaignNext) => {
                    const { type } = campaign
                    const clone = deepclone(instance)

                    const parameterOverlay = clone.properties.parameterOverlay

                    const overlays = parameterOverlay &&
                        Object.keys(parameterOverlay).reduce(
                            (acc, cur) => {
                                const param = parameterOverlay![cur] as ParameterRef
                                const clonedParam = type === 'branch' ?
                                    layoutRegistry.lookupParameter(param.original.spec).clone({ ...param.original, value: param.value }, campaign)
                                    :
                                    { ...param.original, value: param.value }
                                return clonedParam ?
                                    { ...acc, [cur]: { original: param.original, value: clonedParam.value } }
                                    : acc
                            }
                            , {})

                    return {
                        ...clone,
                        id: undefined!,
                        campaign: c.id,
                        tags: type === 'branch' ? prodstore.lookup(instance.source)?.tags ?? clone.tags : clone.tags,
                        lineage: type === 'branch' ? { source: instance.source, campaign: instance.campaign } : undefined,
                        properties: { ...clone.properties, parameterOverlay: overlays }
                    }


                }

                ,


                comparator: (o1: ProductInstance, o2: ProductInstance) => prodmodel.comparator(prodstore.safeLookup(o1.source)!, prodstore.safeLookup(o2.source)!)

                ,


                nameOf: (i: ProductInstance) => `${prodmodel.nameOfRef(i?.source)}`
                
                ,
              
                matches: (p: ProductInstance, u: User) => {

                    const { given } = tags

                    return u.tenant === noTenant || given(u).matches(p.userProfile)

                }

            }

            return self
        }
    }
}
