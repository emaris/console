
import { useT } from '#app/intl/api';
import { defaultLanguage } from "#app/intl/model";
import { useToggleBusy } from "#app/system/api";
import { through } from "#app/utils/common";
import { notify, showAndThrow, useFeedback } from "#app/utils/feedback";
import { CampaignNext } from '#campaign/context';
import { useProductClient } from '#product/client';
import { productPlural, productSingular } from "#product/constants";
import { useEventInstanceStore } from '../event/store';
import { Campaign } from "../model";
import { useProductInstanceCalls } from "./calls";
import { ProductInstance } from "./api";
import { useProductInstances } from './api';


export type ProductInstanceState = {

    products: {
        all: ProductInstance[]
        map: Record<string, ProductInstance>
        sourceMap: Record<string, ProductInstance>
    }

}

export const initialProductInstances = (): ProductInstanceState => ({

    products: {
        all: undefined!,
        map: undefined!,
        sourceMap: undefined!
    }
})


export const useProductInstanceClient = () => {

    const store = useProductInstances()

    const t = useT()
    const toggleBusy = useToggleBusy()
    const fb = useFeedback()

    const type = "productinstance"
    
    const singular = t(productSingular).toLowerCase()
    const plural = t(productPlural).toLowerCase()

    const prodclient = useProductClient()

    const instancecalls = useProductInstanceCalls()
    const eventinststore = useEventInstanceStore()

    return {
        on: (c: Campaign) => {

            const events = eventinststore.on(c)
            const cstore = store.on(c)

            const self = {

                areReady: () => !!cstore.all()


                ,

                addAll: (instances: ProductInstance[]) =>

                    toggleBusy(`${type}.addall`, t("common.feedback.save_changes"))

                        .then(_ => instancecalls.on(c).addAll(instances))
                        .then(through(added => cstore.setAll([...cstore.all(), ...added])))
                        // notifies the event system to generate automanaged events.
                        .then(through(added => events.addManagedForInstances(added)))
                        .then(through(() => notify(t('common.feedback.saved'))))

                        .catch(e => showAndThrow(e, t("common.calls.add_many_error", { plural })))
                        .finally(() => toggleBusy(`${type}.addall`))

                ,

                cloneAll: (instances: ProductInstance[], context: CampaignNext) => instancecalls.on(c).addAll(instances.map(i => cstore.clone(i, context))).then(cstore.setAll)

                ,


                fetchAll: (forceRefresh = false) =>

                    Promise.all([prodclient.fetchAll(),


                    self.areReady() && !forceRefresh ?

                        Promise.resolve(cstore.all())

                        :

                        toggleBusy(`${type}.fetchAll.${c.id}`, t("common.feedback.load", { plural }))

                            .then(_ => console.log(`fetching products for ${c.name[defaultLanguage]}...`))
                            .then(instancecalls.on(c).fetchAll) // load template dependency in parallel
                            .then(through($ => cstore.setAll($, { noOverwrite: !forceRefresh })))


                            .catch(e => showAndThrow(e, t("common.calls.fetch_all_error", { plural })))
                            .finally(() => toggleBusy(`${type}.fetchAll.${c.id}`))


                    ]).then(([, instances]) => instances)


,
              
                save: (instance: ProductInstance, replacementOf: ProductInstance = instance) =>

                    toggleBusy(`${type}.updateOne`, t("common.feedback.save_changes"))

                        .then(_ => instancecalls.on(c).updateOne(instance))
                        .then(() => cstore.all().map(p => p.id === replacementOf.id ? instance : p))
                        .then(cstore.setAll)
                        .then(() => notify(t('common.feedback.saved')))

                        .catch(e => showAndThrow(e, t("common.calls.update_one_error", { singular })))
                        .finally(() => toggleBusy(`${type}.updateOne`))



                ,

                remove: (instance: ProductInstance, onConfirm?: (...args) => void) =>

                    fb.askConsent({

                        title: t('common.consent.remove_one_title', { singular }),
                        content: t("common.consent.remove_one_msg", { singular }),
                        okText: t("common.consent.remove_one_confirm", { singular }),

                        onOk: () => {


                            toggleBusy(`${type}.removeOne`, t("common.feedback.save_changes"))

                                .then(_ => instancecalls.on(c).removeOne(instance))
                                .then(() => cstore.all().filter(p => instance.id !== p.id))
                                .then(cstore.setAll)
                                .then(() => notify(t('common.feedback.saved')))
                                .then(() => onConfirm && onConfirm(instance))

                                .catch(e => showAndThrow(e, t("common.calls.remove_one_error", { singular })))
                                .finally(() => toggleBusy(`${type}.removeOne`))


                        }
                    })

                ,

                removeMany: (instances: ProductInstance[], onConfirm?: () => void) =>

                    fb.askConsent({

                        title: t('common.consent.remove_many_title', { count: instances.length, plural }),
                        content: t("common.consent.remove_many_msg", { count: instances.length, plural }),
                        okText: t("common.consent.remove_many_confirm", { count: instances.length, plural }),

                        onOk: () => {

                            const ids = instances.map(i => i.id)

                            toggleBusy(`${type}.removeMany`, t("common.feedback.save_changes"))

                                .then(_ => instancecalls.on(c).removeMany(instances))
                                .then(() => cstore.all().filter(p => !ids.includes(p.id)))
                                .then(through(cstore.setAll))
                                .then(() => notify(t('common.feedback.saved')))
                                .then(through(() => onConfirm && onConfirm()))

                                .catch(e => showAndThrow(e, t("common.calls.remove_many_error", { plural })))
                                .finally(() => toggleBusy(`${type}.removeMany`))

                        }
                    })
            }

            return self
        }
    }
}
