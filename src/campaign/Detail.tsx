
import { buildinfo } from '#app/buildinfo';
import { Button } from "#app/components/Button";
import { Drawer, DrawerProps, useRoutableDrawer } from "#app/components/Drawer";
import { IdProperty } from "#app/components/IdProperty";
import { NoSuchRoute } from "#app/components/NoSuchRoute";
import { Placeholder } from "#app/components/Placeholder";
import { RouteGuard } from "#app/components/RouteGuard";
import { SideList } from "#app/components/SiderList";
import { Paragraph, Text } from "#app/components/Typography";
import { Form } from "#app/form/Form";
import { SliderBox } from "#app/form/SliderBox";
import { FormState, useFormState } from "#app/form/hooks";
import { iamPlural } from "#app/iam/constants";
import { any, specialise } from "#app/iam/model";
import { icns } from "#app/icons";
import { useT } from '#app/intl/api';
import { LifecycleSummary } from "#app/model/lifecycle";
import { useL } from '#app/model/multilang';
import { PushGuard } from '#app/push/PushGuard';
import { Page } from "#app/scaffold/Page";
import { Subtitle, Titlebar } from "#app/scaffold/PageHeader";
import { Sidebar } from "#app/scaffold/Sidebar";
import { Tab } from "#app/scaffold/Tab";
import { Topbar } from "#app/scaffold/Topbar";
import { TagList } from "#app/tag/Label";
import { tenantIcon, tenantPlural, tenantType } from "#app/tenant/constants";
import { TimeZoneContext } from "#app/time/hooks";
import { useLogged } from '#app/user/store';
import { through } from "#app/utils/common";
import { useFeedback } from '#app/utils/feedback';
import { useAsyncRender } from "#app/utils/hooks";
import { parentIn } from "#app/utils/routes";
import { withReport } from "#app/utils/validation";
import { useCampaignPermissions } from "#campaign/CampaignPermissions";
import { ArchiveLabel, CampaignLabel, MutedLabel, SuspendedLabel } from "#campaign/Label";
import { campaignPlural, campaignRoute, campaignSingular } from "#campaign/constants";
import { EventInstanceLabel } from "#campaign/event/Label";
import { AbsoluteDate, useEventInstanceModel } from "#campaign/event/model";
import { useEventInstanceDates, useEventInstanceStore } from '#campaign/event/store';
import { CurrentCampaignContext } from "#campaign/hooks";
import { Campaign, useCampaignModel, useCampaignStatus } from "#campaign/model";
import { usePartyInstances } from '#campaign/party/api';
import { useProductInstances } from '#campaign/product/api';
import { useRequirementInstances } from '#campaign/requirement/api';
import { useCampaignDetail, useCampaignRouting, useCampaignStore } from '#campaign/store';
import { submissionType } from "#campaign/submission/constants";
import { useRoutedTypes } from '#config/model';
import { useDashboard } from '#dashboard/api';
import { useEventClient } from '#event/client';
import { eventIcon, eventPlural, eventType } from "#event/constants";
import { Layout } from '#layout/model';
import { LayoutProvider } from '#layout/provider';
import { productIcon, productPlural, productType } from "#product/constants";
import { requirementIcon, requirementPlural, requirementType } from "#requirement/constants";
import { Icon, Tooltip } from "antd";
import moment from "moment-timezone";
import * as React from "react";
import { Trans } from "react-i18next";
import { BsSafe2Fill } from "react-icons/bs";
import { HiPause, HiPlay } from 'react-icons/hi';
import { RiNotificationLine, RiNotificationOffLine } from 'react-icons/ri';
import { useHistory, useLocation, useParams } from "react-router";
import { ProfileForm } from "./GeneralForm";
import { campaignGroup } from './List';
import { LiveGuardProvider, useCampaignLiveGuard } from './LiveGuard';
import { campaignActions } from './actions';
import { EventInstances } from "./event/Events";
import { useParameterOnlyInitialiser } from './paramonlyinitialiser';
import { Parties } from "./party/Parties";
import { ProductInstances } from "./product/Products";
import { RequirementInstances } from "./requirement/Requirements";
import { useCampaignValidation } from './validation';

export const CampaignDetail = () => {

    const { pathname } = useLocation();

    const { id } = useParams<{ id: string }>()

    const campaignstore = useCampaignStore()

    const current = campaignstore.lookup(id)

    if (!current)
        return <NoSuchRoute backTo={parentIn(pathname)} />

    return <LiveGuardProvider>
        <PushGuard>
            <CampaignDetailLoader key={current.id} detail={current} />
        </PushGuard>
    </LiveGuardProvider>

}


export const NewCampaignDetail = () => {

    const client = useEventClient()

    const [fetched, setFetched] = React.useState<boolean>(false)

    //New campaign loads events to use them on save time to import managed campaign events
    const [render] = useAsyncRender({
        when: fetched,
        task: () => client.fetchAll().then(_ => setFetched(true)),

        // we mount a dummy layout in order to get the registry setup for pararmeters to be cloned/branched.
        content: <LayoutProvider useInitialiser={useParameterOnlyInitialiser} layout={{ parameters: [] as any } as Layout}>
            <InnerCampaignDetail isNew={true} />
        </LayoutProvider>,

        placeholder: Placeholder.page

    })

    return render

}


const CampaignDetailLoader = (props: { detail: Campaign }) => {

    const { detail } = props

    const store = useCampaignStore()
    const cmpdetail = useCampaignDetail()

    const initialValue = store.isLoaded(detail.id) ? detail : undefined

    const [fetched, setFetched] = React.useState<Campaign | undefined>(initialValue)

    const [render] = useAsyncRender({
        when: !!fetched && fetched.id === detail.id,
        task: () => cmpdetail.fetchOne(detail).then(setFetched),

        content: () => <CurrentCampaignContext campaign={fetched!}>
            <InnerCampaignDetail isNew={false} detail={fetched!} />
        </CurrentCampaignContext>,

        placeholder: Placeholder.page

    })


    return render

}


const InnerCampaignDetail = (props: { isNew: boolean, detail?: Campaign }) => {

    const l = useL()
    const t = useT()

    const history = useHistory()
    const { search } = useLocation()
    const { type } = useParams<{ type: string }>()


    const [Permissions, showPermissions] = useCampaignPermissions()

    const logged = useLogged()

    const routedTypes = useRoutedTypes()
    const store = useCampaignStore()
    const cmpdetail = useCampaignDetail()
    const model = useCampaignModel()
    const status = useCampaignStatus()
    const campaignvalidation = useCampaignValidation()
    const campaignrouting = useCampaignRouting()

    const context = React.useRef(campaignrouting.next())

    // clean up context
    React.useEffect(() => {

        campaignrouting.resetNext()

    }   //eslint-disable-next-line
        , [])

    const { isNew, detail = context.current.source } = props

    const dashboard = useDashboard()

    const formstate = useFormState<Campaign>(detail);

    const { edited, initial, reset, dirty } = formstate

    const { liveGuard, liveGuarded } = useCampaignLiveGuard({
        campaign: edited
    })

    const singular = t(campaignSingular)
    const name = l(edited.name)

    const [drawerVisible, setDrawerVisible] = React.useState(false)

    const { Drawer: SuspendDrawer, open: openSuspend, close: closeSuspend } = useRoutableDrawer({ id: 'suspend' })
    const { Drawer: ArchiveDrawer, open: openArchive, close: closeArchive } = useRoutableDrawer({ id: 'archive' })
    const { Drawer: MuteDrawer, open: openMute, close: closeMute } = useRoutableDrawer({ id: 'mute' })

    // -------------- authz
    const manageIt = specialise(campaignActions.manage, detail.id)

    // these reports can be costly to compute at each render, so we memoise them on a per-instance basis.
    // based on whether the underlying instances lists have changed or not. 

    // Unforutnately, these lists are indirect dependencies of the report and the linter thinks are spurious. 
    // Conversely, it think the apparent dependencies are important. I disable it here, as I cannot change
    // api design to accomodate it.

    const partyinstances = usePartyInstances().on(edited).all()
    const reqinstances = useRequirementInstances().on(edited).all()
    const prodinstances = useProductInstances().on(edited).all()
    const eventinstances = useEventInstanceStore().on(edited).all()


    //eslint-disable-next-line
    const profileReport = React.useMemo(() => campaignvalidation.validateProfile(edited, initial), [edited])
    //eslint-disable-next-line
    const partiesReport = React.useMemo(() => partyinstances && campaignvalidation.validateParties(edited), [partyinstances])
    //eslint-disable-next-line
    const productsReport = React.useMemo(() => prodinstances && campaignvalidation.validateProducts(edited), [prodinstances])
    //eslint-disable-next-line
    const requirementsReport = React.useMemo(() => reqinstances && campaignvalidation.validateRequirements(edited), [reqinstances])
    //eslint-disable-next-line
    const eventsReport = React.useMemo(() => eventinstances && campaignvalidation.validateEvents(edited), [eventinstances])

    // -------------- error report
    const report = {
        profile: profileReport,
        parties: isNew ? withReport({}) : partiesReport,
        products: isNew ? withReport({}) : productsReport,
        requirements: isNew ? withReport({}) : requirementsReport,
        events: isNew ? withReport({}) : eventsReport,
    }

    const totalErrors = Object.keys(report).reduce((total, key) => total += report[key].errors(), 0)


    // -------------- actions
    const onSave = (campaign: Campaign) => cmpdetail.save(campaign, context.current)
        // reset to saved as initial state (or will appear dirty)
        .then(through(saved => reset(saved, false)))
        .then(through(saved => history.push(`${campaignrouting.routeTo(saved)}${search}`)))

    const onClone = (campaign: Campaign) => {
        campaignrouting.setNext({ source: model.clone(campaign), lineage: campaign, type: 'clone' })
        history.push(`${campaignRoute}/new`)
    }

    const onBranch = (campaign: Campaign, offset: number) => {
        campaignrouting.setNext({ source: model.branch(campaign), lineage: campaign, type: 'branch', timeOffset: offset })
        history.push(`${campaignRoute}/new`)
    }

    const suspended = !!model.isSuspendSubmissions(edited)
    const suspendedStatus = suspended ? 'off' : 'on'


    const onSuspendChange = () => {

        model.setSuspendSubmissions(detail, !suspended)

        onSave(detail).then(closeSuspend)

    }

    const muted = !!model.isMuted(edited)
    const mutedStatus = muted ? 'off' : 'on'

    const onMutedChange = () => {

        model.setMuted(detail, !muted)

        onSave(detail).then(closeMute)

    }

    const archive = () => {

        model.setArchived(detail)

        onSave(detail).then(closeArchive)
    }




    const readOnly = liveGuarded || !logged.can(manageIt) || model.isArchived(edited)

    // -------------- buttons
    const saveBtn = <Button
        icn={icns.save}
        enabled={dirty}
        disabled={report.profile.errors() > 0}
        dot={report.profile.errors() > 0}
        onClick={() => onSave(edited)}>
        {t("common.buttons.save")}
    </Button>

    const cantremove = isNew || dirty
    const removeBtn = <Button
        enabledOnReadOnly={buildinfo.development}
        icn={icns.remove}
        disabled={cantremove}
        onClick={() => { cmpdetail.remove(detail.id, () => history.push(campaignRoute), !buildinfo.development) }}>
        {t("common.buttons.remove")}
    </Button>

    const revertBtn = <Button
        icn={icns.revert}
        enabled={dirty}
        type="danger"
        onClick={() => reset()}>
        {t("common.buttons.revert")}
    </Button>

    const cantclone = isNew || !logged.can(campaignActions.manage)
    const clone = <Button
        enabledOnReadOnly={!model.isArchived(edited)} // overrides reules unless it's archived
        disabled={cantclone}
        icn={icns.clone}
        onClick={() => onClone(edited)}>
        {t("common.buttons.clone")}
    </Button>

    const cantbranch = isNew || !logged.can(campaignActions.manage)
    const branch = <Button
        enabledOnReadOnly={!model.isArchived(edited)}   // overrides reules unless it's archived
        disabled={cantbranch}
        icn={icns.branch}
        onClick={() => setDrawerVisible(true)}>
        {t("campaign.buttons.branch")}
    </Button>

    const liveBtn = <Button
        enabledOnReadOnly
        enabled={status.isStarted(edited)}
        iconLeft
        icn={icns.edit}
        onClick={() => history.push(dashboard.routeTo(edited))}>
        {t("campaign.labels.campaign_view.live")}
    </Button>


    const rightsBtn = <Button
        icn={icns.permissions}
        enabledOnReadOnly
        disabled={isNew || dirty}
        onClick={showPermissions}>
        {t(iamPlural)}
    </Button>

    const addCampaignBtn = <Button
        type="primary"
        enabledOnReadOnly
        icn={icns.add}
        enabled={logged.can(campaignActions.manage)}
        onClick={() => campaignrouting.setNext({ ...campaignrouting.next(), type: 'new' })}
        linkTo={`${campaignRoute}/new`} >
        {t("common.buttons.add_one", { singular })}
    </Button>

    const cantmuteorsuspend = isNew || dirty || model.isArchived(edited)

    const suspendBtn = <Button
        type="danger"
        enabledOnReadOnly
        icn={icns.pause}
        disabled={cantmuteorsuspend}
        onClick={openSuspend}>
        {t(`campaign.suspend.suspend_btn_${suspendedStatus}`)}
    </Button>

    const muteBtn = <Button
        type="danger"
        enabledOnReadOnly
        disabled={cantmuteorsuspend}
        icn={icns.muted}
        onClick={openMute}>
        {t(`campaign.mute.mute_btn_${mutedStatus}`)}
    </Button>



    const cantarchive = dirty || !status.canArchive(edited)
    const archiveBtn = <Button
        icn={icns.archive}
        disabled={cantarchive}
        onClick={openArchive}>
        {t("campaign.archive.archive_btn")}
    </Button>

    const onTabChange = key => history.push(`${campaignrouting.routeTo(detail)}${key === 'info' ? '' : `/${key}`}${search}`)

    const tabcompo = (() => {

        switch (type) {
            case tenantType: return <Parties report={report.parties} />
            case requirementType: return <RequirementInstances report={report.requirements} />
            case productType: return <ProductInstances report={report.products} />
            case eventType: return <EventInstances report={report.events} />
            default: return <ProfileForm isNew={isNew} report={report.profile} {...formstate} />
        }
    })()

    const primaryBtn = !isNew && !dirty && status.isEnded(edited) && status.canArchive(edited) ? archiveBtn : saveBtn

    return <Page readOnly={readOnly}>

        <Sidebar>
            {React.cloneElement(primaryBtn, { type: 'primary' })}
            {primaryBtn === archiveBtn && saveBtn}
            {revertBtn}
            {clone}
            {branch}
            {removeBtn}
            {rightsBtn}

            <br />

            {muteBtn}
            {suspendBtn}


            <br />

            <IdProperty id={detail.id} />

            <br />

            <LifecycleSummary {...edited.lifecycle} />

            <br />

            <div className="sidebar-property">
                <Paragraph>
                    {totalErrors > 0 ?

                        <Text type="danger">{t("common.validation.total_error_count", { totalErrors })}</Text>
                        :
                        <Text smaller className="emphasis">{t("common.validation.zero_error_count")}</Text>

                    }
                </Paragraph>
            </div>

            {(logged.can(campaignActions.manage) && !isNew) &&
                <>
                    <div style={{ marginTop: "25px" }}>{addCampaignBtn}</div>
                </>
            }
            <SideList data={store.all()}
                filterGroup={campaignGroup} filterBy={model.stringify} filterPlaceholder={t("common.components.table.filter_placeholder", { plural: t(campaignPlural.toLowerCase()).toLowerCase() })}
                renderData={c => l(c.name)}
                render={c => <CampaignLabel noMemo selected={c.id === edited.id} campaign={c} />} />


        </Sidebar>

        <Topbar onTabChange={onTabChange} activeTab={type}>

            <Titlebar title={name || (name === undefined ? `<${t('common.labels.new')}>` : "")}>
                {edited.lineage && <Subtitle className="lineage-subtitle">
                    <div className='subtitle-label'>
                        <span >{t("common.fields.lineage.name")}:&nbsp;&nbsp;</span><CampaignLabel targetMode='lineage' noIcon campaign={edited} />
                    </div>
                </Subtitle>}
                <div style={{ display: "flex", alignItems: "center" }}>
                    <CampaignLabel mode='tag' statusOnly campaign={edited} />
                    <ArchiveLabel campaign={edited} />
                    <SuspendedLabel campaign={edited} />
                    <MutedLabel campaign={edited} />
                    <TagList taglist={edited.tags} />
                </div>
            </Titlebar>


            <Tab default id="info" icon={icns.form} name={t("common.labels.general")} badge={report.profile.errors() > 0} />
            <Tab id={tenantType} disabled={isNew} icon={tenantIcon} name={t(tenantPlural)} badge={report.parties.errors() > 0} />
            <Tab id={requirementType} disabled={isNew} icon={requirementIcon} name={t(requirementPlural)} badge={report.requirements.errors() > 0} />
            <Tab id={productType} disabled={isNew} icon={productIcon} name={t(productPlural)} badge={report.products.errors() > 0} />
            <Tab id={eventType} disabled={isNew} icon={eventIcon} name={t(eventPlural)} badge={report.events.errors() > 0} />

            {liveGuard}


            {React.cloneElement(primaryBtn, { type: 'primary' })}
            {primaryBtn === archiveBtn ? saveBtn : status.canArchive(edited) && archiveBtn}
            {revertBtn}
            {clone}
            {branch}
            {removeBtn}
            {rightsBtn}
            {routedTypes.includes(submissionType) && liveBtn}
            {muteBtn}
            {suspendBtn}

        </Topbar>

        <Permissions resourceCentric resourceRange={[edited]} filter={{ resources: [edited.id, any] }} />

        <TimeZoneContext.Provider value={model.timeZone(edited)}>
            {tabcompo}
        </TimeZoneContext.Provider>

        <RouteGuard when={dirty} onOk={() => reset(edited, false)} />

        <BranchDrawer enabledOnReadOnly readonly={cantbranch} visible={drawerVisible} setVisible={setDrawerVisible} onBranch={onBranch} edited={edited} formstate={formstate} />

        <SuspendDrawer enabledOnReadOnly readonly={cantmuteorsuspend} width={400} className='suspend-drawer' icon={icns.pause} title={t('campaign.suspend.suspend_title')}>
            <SuspendDrawerPanel status={suspendedStatus} onConfirm={onSuspendChange} />
        </SuspendDrawer>

        <ArchiveDrawer readonly={cantarchive} className='archive-drawer' icon={icns.archive} title={t('campaign.archive.archive_title')}>
            <ArchiveDrawerPanel onConfirm={archive} ended={status.isEnded(edited)} />
        </ArchiveDrawer>

        <MuteDrawer enabledOnReadOnly readonly={cantmuteorsuspend} width={400} className='mute-drawer' icon={icns.muted} title={t('campaign.mute.mute_title')}>
            <MuteDrawerPanel status={mutedStatus} onConfirm={onMutedChange} />
        </MuteDrawer>
    </Page>

}

type BranchDrawerProps = Partial<DrawerProps> & {
    setVisible: (_: boolean) => void
    onBranch: (_: any, __: any) => void
    edited: Campaign
    formstate: FormState<Campaign>
}

const BranchDrawer = (props: BranchDrawerProps) => {

    const t = useT()

    const { visible, setVisible, edited, onBranch, formstate } = props


    const [branchOffset, setBranchOffset] = React.useState(1)

    const instances = { ...useEventInstanceModel().on(edited), ...useEventInstanceStore().on(edited), ...useEventInstanceDates().on(edited) }

    const allEventInstances = instances.all()

    //eslint-disable-next-line
    const firstRecurringEvent = React.useMemo(() => allEventInstances && allEventInstances.sort(instances.dateComparator).find(i => i.date?.kind === 'absolute' && (i.date?.branchType === 'recurring' || i.date?.branchType === undefined)), [allEventInstances])
    //eslint-disable-next-line
    const firstRecurringDate = React.useMemo(() => firstRecurringEvent ? +moment((firstRecurringEvent.date as AbsoluteDate).value).format("YYYY") : undefined, [firstRecurringEvent])
    //eslint-disable-next-line
    const branchStartYear = React.useMemo(() => firstRecurringDate || new Date().getFullYear(), [firstRecurringDate])

    return <Drawer placement="right" icon={icns.branch} enabledOnReadOnly={props.enabledOnReadOnly} visible={visible} onClose={() => setVisible(false)} title={t("campaign.branch.name", { singular: t(campaignSingular) })} width={800} className="branch-drawer">
        <Topbar autoGroupButtons={false}>
            <Button type="default" key={1} onClick={() => setVisible(false)}>{t("common.buttons.cancel")}</Button>
            <Button type="primary" key={2} onClick={() => onBranch(edited, branchOffset)}>{t("campaign.branch.button")}</Button>
        </Topbar>

        <div className="branch-info">

            {<Trans i18nKey="campaign.branch.blurb" />}
            {firstRecurringEvent && <><br /><br />
                <Trans
                    i18nKey="campaign.branch.action"
                    components={[<Tooltip title={<EventInstanceLabel noLineage noMemo noDate instance={firstRecurringEvent} />}>{firstRecurringDate}</Tooltip>]}
                    values={{ firstRecurringDate }}
                />
            </>}

        </div>

        <Form state={formstate}>
            <div className="slider">
                <SliderBox
                    label={t("campaign.branch.field.name")}
                    validation={{ msg: t("campaign.branch.field.msg", { singular: t(campaignSingular) }) }}
                    defaultValue={firstRecurringDate ? branchOffset : 0}
                    min={0}
                    max={10}
                    disabled={firstRecurringDate === undefined}
                    marks={Array(11).fill(0).map((_, i) => i).reduce((acc, cur) => ({ ...acc, [cur]: branchStartYear + cur }), {})}
                    showValues={false}
                    tooltipVisible={false}
                    onChange={v => setBranchOffset(v as number)} />
            </div>
        </Form>

    </Drawer>
}

const SuspendDrawerPanel = (props: {

    status: 'off' | 'on'
    onConfirm: () => any

}) => {

    const { status, onConfirm } = props

    const t = useT()

    return <div className={`suspend-drawer-panel drawer-${status}`}>

        <Paragraph style={{ fontSize: 16 }} >{t(`campaign.suspend.suspend_intro_${status}`)}</Paragraph>

        <Icon component={status === 'on' ? HiPause : HiPlay} className='suspend-drawer-icon' />

        <Button type='primary' onClick={onConfirm}>{t(`campaign.suspend.suspend_confirm_${status}`)}</Button>

    </div>

}


const ArchiveDrawerPanel = (props: {
    onConfirm: () => any
    ended: boolean
}) => {

    const { onConfirm, ended } = props

    const t = useT()
    const { askConsent } = useFeedback()

    const confirm = () => askConsent({

        title: t('campaign.archive.archive_confirm_title'),
        content: t('campaign.archive.archive_confirm_text'),
        okText: t('campaign.archive.archive_confirm_btn'),
        noValediction: true,
        okChallenge: buildinfo.development ? undefined : t('campaign.archive.archive_confirm_challenge'),
        onOk: onConfirm

    })

    return <div className="archive-drawer-panel">

        <Paragraph style={{ fontSize: 16 }} >{t(`campaign.archive.archive_intro`)}</Paragraph>

        <br />

        <Paragraph strong style={{ fontSize: 16 }}>{t(`campaign.archive.archive_warn`)}</Paragraph>

        <Icon style={{ color: "darkorange" }} component={BsSafe2Fill} className={'archive-drawer-icon'} />


        <Button type='primary' disabled={!ended} onClick={confirm}>{t(`campaign.archive.archive_action_btn`)}</Button>

        {ended || <Paragraph type='warning' strong style={{ marginTop: 30 }}>{t(`campaign.archive.archive_warn_end_1`)}</Paragraph>}
        {ended || <Paragraph strong style={{ marginTop: 30 }}>{t(`campaign.archive.archive_warn_end_2`)}</Paragraph>}

    </div>

}


const MuteDrawerPanel = (props: {

    status: 'off' | 'on'
    onConfirm: () => any

}) => {

    const { status, onConfirm } = props

    const t = useT()

    return <div className={`mute-drawer-panel drawer-${status}`}>

        <Paragraph style={{ fontSize: 16 }} >{t(`campaign.mute.mute_intro_${status}`)}</Paragraph>

        <Icon component={status === 'on' ? RiNotificationOffLine : RiNotificationLine} className='mute-drawer-icon' />

        <Button type='primary' onClick={onConfirm}>{t(`campaign.mute.mute_confirm_${status}`)}</Button>

    </div>

}