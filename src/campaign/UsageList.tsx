
import { Placeholder } from "#app/components/Placeholder"
import { Column, VirtualTable } from "#app/components/VirtualTable"
import { useT } from '#app/intl/api'
import { useLocale } from "#app/model/hooks"
import { TagList } from "#app/tag/Label"
import { useRenderGuard } from 'apprise-frontend-core/utils/renderguard'
import * as React from "react"
import { useHistory } from "react-router-dom"
import { useCampaignClient } from './client'
import { campaignApiRoute } from "./constants"
import { CampaignLabel } from "./Label"
import { Campaign, CampaignDto } from "./model"

type Props = {

    type:string
    target:string
    height?:number

}

type ComponentProps = {

  
    height?: number
    campaigns:Campaign[]

}

export const useCampaignUsage = (props:Props) => {

    const [filtered,setFiltered] = React.useState<Campaign[]>(undefined!)

    const campaignstore = useCampaignClient()

    const {type,target,height} = props

    const {content} = useRenderGuard({

        when:!!filtered,
        render: <CampaignUsageList campaigns={filtered} height={height} />,
        orRun: ()=>campaignstore.fetchAllWith(type,target).then(setFiltered),
        andRender:Placeholder.list
    })


    return [content,filtered] as [JSX.Element,CampaignDto[]]

}

const CampaignUsageList = (props:ComponentProps) => {

    const history=useHistory()
    const t = useT()
    const {l} = useLocale()

   
    const {campaigns, height} = props

    return <VirtualTable<Campaign>  height={height} selectable={false} filtered={campaigns.length>15} 
                            data={campaigns} rowKey="id"
                            onDoubleClick={c => history.push(`${campaignApiRoute}/${c.id}`)} >

                        <Column width={250} title={t("common.fields.name_multi.name")} dataKey="name" 
                                dataGetter={(r:Campaign)=>l(r.name)} cellRenderer={r=><CampaignLabel campaign={r.rowData}/>} />

                        <Column flexGrow={1} title={t("common.fields.tags.name")} dataKey="tags" dataGetter={(r:Campaign)=><TagList taglist={r.tags} />} />

                        </VirtualTable>


}