
import { useCalls } from '#app/call/call';
import { useCampaignModel } from '#campaign/model';
import { domainService } from '#constants';
import { StateProvider } from 'apprise-frontend-core/state/provider';
import { PropsWithChildren, useContext, useEffect } from 'react';
import { campaignApi } from './calls';
import { CampaignContext, CampaignUsageContext } from './context';




const initialUsage = {
        
    instances: {}

}

export const CampaignUsage = (props: PropsWithChildren<{}>) => {

    const { children } = props

    return <StateProvider initialState={initialUsage} context={CampaignUsageContext}>
        <InstanceMonitor />
        { children }
    </StateProvider >

}

// monitor changes to instances and resets any usage data.
// this is course grained but adequately cheap, ie.:
// 1. going unncessarily back to backend to check usage is cheap
// 2. inconsistent usage date as a result of local change is uncanny.
const InstanceMonitor = () => {


    const campaigns = useContext(CampaignContext)
    const usage = useContext(CampaignUsageContext)
    
    const instances = campaigns.get().campaigns.instances

    useEffect(()=> {

        if (instances)
            usage.resetQuietly()

    // eslint-disable-next-line
    },[instances])

    return null

}

export type Usage = ReturnType<typeof useCampaignUsage>

type UsageDto = {

    campaigns: string[]

}


export const useCampaignUsage = () => {


    const calls = useCalls()

    const usage = useContext(CampaignUsageContext)
    const campaignmodel = useCampaignModel()

    const self = {

        ...usage

        ,

        // special default value further protects from corrupt usage map (undefined is indexed)
        usedBy: (target: string | { id: string } = '__missing') => usage.get().instances[typeof target === 'string' ? target : target?.id] ?? []

        ,

        isInUse: (target: string | { id: string }) => campaignmodel.liveGuard() && self.usedBy(target).length > 0

        ,

        fetchFor: async (type: string, source: string) => {


            if (!campaignmodel.liveGuard() || usage.get().instances[source])
                return
            
            const data = await calls.at(`${campaignApi}/usage/${type}/${source}`, domainService).get<UsageDto>()

            usage.set( u => u.instances[source] = data.campaigns)

        }

    }

    return self
}