
import { Action, any } from "#app/iam/model"
import { campaignIcon, campaignType } from "./constants"

const baseAction = { icon:campaignIcon, type: campaignType, resource :any, actionType : 'admin' } as Action

export const campaignActions = {  
    
    manage:  {...baseAction, labels:["manage"], shortName:"campaign.actions.manage.short", name:"campaign.actions.manage.name", description: "campaign.actions.manage.desc"}
}