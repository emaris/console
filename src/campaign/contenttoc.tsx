import { Button } from '#app/components/Button'
import { DrawerProps, RoutableDrawerProps, useRoutableDrawer } from '#app/components/Drawer'
import { Label } from '#app/components/Label'
import { Form } from '#app/form/Form'
import { icns } from '#app/icons'
import { useT } from '#app/intl/api'
import { useStable } from '#app/utils/function'
import { anchorClassId } from '#layout/components/anchor'
import { layoutFinalSelector } from '#layout/renderstrategy'
import { Input } from 'antd'
import { useScrollToId } from 'apprise-frontend-core/utils/scrolltohash'
import { useEffect, useRef, useState } from 'react'
import { Link, useLocation } from 'react-router-dom'

export const tocIcon = icns.list

export const tocParam = 'toc'

export const useTocDrawer = (opts: Partial<RoutableDrawerProps> = {}) => {

    const t = useT()

    const [filter, filterSet] = useState<string>('')

    const { search,hash } = useLocation()

    const [ready,setReady] = useState(false)

    useEffect(() => {


        const readyObserver = new MutationObserver(() => {

            if (document.querySelector?.(`.${layoutFinalSelector}`)) {

                setReady(true)
                readyObserver.disconnect()

            }

        })

        readyObserver.observe(document.body, { childList: true, subtree: true })


    }, [])


    // this is cheap
    const findAnchors = () => {

        const anchors = [] as { id: string | null, label: string | null, level: number }[]

        if (ready)
            document.querySelectorAll(`.${anchorClassId}`).forEach(e => anchors.push({ id: e.getAttribute('id'), label: e.getAttribute('aria-label'), level: Number.parseInt(e.getAttribute('aria-level')!) }))

        return anchors

    }

    const anchors = findAnchors()
   
    
    // eslint-disable-next-line
    const filteredContents = anchors.filter(a => !a.label || a.label?.toLowerCase().includes(filter))

    const { id = tocParam, title = t("submission.toc.title"), icon = tocIcon } = opts

    const { Drawer, open, close, visible, route, param } = useRoutableDrawer({ id, title, icon })

    const hashref = useRef(hash)

    const scrollTo = useScrollToId({adjustments:2})

    const rescrollToCurrentHashIfRequired = async () => {
        
        // if hash changes, do nothijg, other mechanism take core of the scroll
        if (hashref.current !== hash)
             return
            
        scrollTo(hash.slice(1))

        close()
    }
    


    useEffect(()=> {

        if (hashref.current !== hash){
            hashref.current = hash
            close()
        }

        //eslint-disable-next-line
    },[hash])


    const DrawerProxy = useStable((props: Partial<DrawerProps>) =>

        anchors.length === 0 ?

            null
            :

            <Drawer className='toc=drawer' width={500} icon={icon} title={title}{...props}>
                <Form>

                    {anchors.length > 15 &&
                        <Input addonBefore={icns.search} suffix={false} addonAfter={false} placeholder="filter..." onChange={e => filterSet(e.target.value?.toLowerCase() ?? '')} value={filter} />
                    }

                    <div className='toc-list' style={{ marginTop: 30 }}>
                        {filteredContents.map(a =>

                            <div style={{marginLeft:a.level*10}} key={a.id} className='toc-entry' >

                                <Link to={{search,hash:`#${a.id}`}} onClick={rescrollToCurrentHashIfRequired}  >
                                    <Label icon={icns.right} title={<span dangerouslySetInnerHTML={{ __html: a.label! }} />} />
                                </Link>

                            </div>

                        )}
                    </div>

                </Form>

            </Drawer>

    )


    const tocBtn = !!anchors?.length && <Button disabled={visible} enabledOnReadOnly icn={tocIcon} iconLeft onClick={open}>
        {t('submission.toc.btn')}
    </Button>


    return { TocDrawer: DrawerProxy, openToc: open, tocRoute: route, closeToc: close, visibleToc: visible, tocBtn, tocParam: param }


}