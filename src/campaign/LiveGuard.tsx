import { Button } from '#app/components/Button'
import { useRoutableDrawer } from '#app/components/Drawer'
import { Paragraph, Text } from '#app/components/Typography'
import { useT } from '#app/intl/api'
import { CampaignLabel } from '#campaign/Label'
import { Campaign, useCampaignModel, useCampaignStatus } from '#campaign/model'
import { useDashboard } from '#dashboard/api'
import { eventType } from '#event/constants'
import { Icon } from 'antd'
import { createContext, PropsWithChildren, useContext, useState } from 'react'
import { AiFillLock, AiFillUnlock } from 'react-icons/ai'
import { FaExpeditedssl } from 'react-icons/fa'
import { useHistory } from 'react-router-dom'
import { useCampaignStore } from './store'
import { useCampaignUsage } from './Usage'
import { StateProvider } from 'apprise-frontend-core/state/provider'
import { fallbackStateOver, State } from 'apprise-frontend-core/state/api'


export const useLiveAssetGuard = (props: {

    type: string
    id: string
    disabled: boolean
    singular: string

}) => {

    const { id, type, singular: s, disabled } = props

    const singular = s.toLowerCase()

    const t = useT()

    const history = useHistory()

    const dashboard = useDashboard()
    const store = useCampaignStore()
    const usage = useCampaignUsage()

    const link = (campaignId: string) => {
    
        const c = store.safeLookup(campaignId)
        
        return type === eventType ? `${dashboard.on(c).routeToCampaign()}/${eventType}` : dashboard.on(c).routeToAssetWith(type, id)
    
    }
    
    const [liveGuarded, liveGuardedSet] = useState(true)
    const { Drawer, open, close } = useRoutableDrawer({ id: 'liveguard' })

    const clickGuard = () => liveGuarded ? open() : liveGuardedSet(true)

    const unlock = () => {

        liveGuardedSet(false)
        close()
    }

    const status = liveGuarded ? 'guarded' : 'unguarded'

    const icon = liveGuarded ? <Icon component={AiFillLock} /> : <Icon component={AiFillUnlock} />

    const inUse = usage.isInUse(id)

    const liveGuard = inUse && <div className={`liveguard liveguard-${status} ${disabled ? `liveguard-disabled` : ''}`} >

        <Button disabled={disabled} tooltip={t(`campaign.liveguard.tip_${status}`)} className='liveguard-label' icn={icon} enabledOnReadOnly noborder type='ghost' onClick={clickGuard}>
            {t('campaign.liveguard.label')}
        </Button>

        <Drawer width={500} className='liveguard-drawer' onClose={close}
            icon={<Icon style={{ color: "red" }} component={AiFillLock} />} title={t("campaign.liveguard.title")}>

            <div className="liveguard-dialog-panel">

                <Paragraph style={{ fontSize: 16 }} >{t("campaign.liveguard.warning1", { singular })}</Paragraph>

                <FaExpeditedssl className='liveguard-dialog-icon' />

                <Paragraph style={{ marginTop: 30 }}><Text italics>{t("campaign.liveguard.usage1", { singular })}</Text></Paragraph>

                <Paragraph style={{ marginTop: 20 }}><Text italics>{t("campaign.liveguard.usage2", { singular })}</Text></Paragraph>

                <Paragraph style={{ marginTop: 20 }}><Text italics>{t("campaign.liveguard.warning2", { singular })}</Text></Paragraph>

                <Button enabledOnReadOnly style={{ marginTop: 30 }} type="danger" onClick={unlock}>{t("campaign.liveguard.btn", { singular })}</Button>

                <div className="liveguard-dialog-section">

                    <div className="liveguard-dialog-section-title">
                        {t('campaign.liveguard.campaigns')}
                    </div>

                    <Paragraph style={{ fontSize: 16 }}><Text italics>{t("campaign.liveguard.campaigns_explainer", { singular })}</Text></Paragraph>

                    <div className='liveguard-campaigns'>
                        {
                            usage.usedBy(id).map(c =>

                                <div key={c} className='liveguard-campaign' >
                                    <CampaignLabel campaign={c} noDecorations noOptions noLink />
                                    <Button enabledOnReadOnly size='small' type='primary' className='liveguardcampaign-open' onClick={() => history.push(link(c))}>
                                        {t('campaign.liveguard.campaign_open',{singular})}
                                    </Button>
                                </div>
                            )
                        }
                    </div>
                </div>

            </div>

        </Drawer>

    </div>





    return { liveGuarded: inUse && liveGuarded, liveGuard, usage }
}


export const LiveGuardContext = createContext<State<boolean>>(fallbackStateOver(false))

export const LiveGuardProvider = (props: PropsWithChildren<{}>) => {

    const {children} = props
    
     return <StateProvider initialState={true} context={LiveGuardContext}>
        {children}
    </StateProvider>
}

export const useCampaignLiveGuard = (props: {

    campaign: Campaign

}) => {

    const { campaign } = props

    const t = useT()

    const singular = t('campaign.module.name_singular').toLowerCase()

    const history = useHistory()

    const cmpstatus = useCampaignStatus()
    const cmpmodel = useCampaignModel()

    const dashboards = useDashboard()

    const inUse = cmpmodel.liveGuard() && cmpstatus.isRunning(campaign)

    const ctx = useContext(LiveGuardContext)

    const liveGuarded = ctx.get()
    const liveGuardedSet = ctx.reset

    const { Drawer, open, close } = useRoutableDrawer({ id: 'liveguard' })

    const clickGuard = () => liveGuarded ? open() : liveGuardedSet(true)

    const unlock = () => {

        liveGuardedSet(false)
        close()
    }

    const status = liveGuarded ? 'guarded' : 'unguarded'

    const icon = liveGuarded ? <Icon component={AiFillLock} /> : <Icon component={AiFillUnlock} />

    const liveGuard = inUse && <div className={`liveguard liveguard-${status}`} >

        <Button tooltip={t(`campaign.liveguard.campaign_tip_${status}`)} className='liveguard-label' icn={icon} enabledOnReadOnly noborder type='ghost' onClick={clickGuard}>
            {t('campaign.liveguard.label')}
        </Button>

        <Drawer width={500} className='liveguard-drawer' onClose={close}
            icon={<Icon style={{ color: "red" }} component={AiFillLock} />} title={t("campaign.liveguard.title")}>

            <div className="liveguard-dialog-panel">

                <Paragraph style={{ fontSize: 16 }} >{t("campaign.liveguard.campaign_warning", { singular })}</Paragraph>

                <FaExpeditedssl className='liveguard-dialog-icon' />

                <Paragraph style={{ marginTop: 30 }}><Text italics>{t("campaign.liveguard.campaign_usage", { singular })}</Text></Paragraph>

                <div className='liveguard-campaign-btns'>
                    <Button enabledOnReadOnly type="danger" onClick={unlock}>{t("campaign.liveguard.btn", { singular })}</Button>
                    <Button enabledOnReadOnly type='primary' className='liveguardcampaign-open' onClick={() => history.push(dashboards.on(campaign).routeToCampaign())}>
                                        {t('campaign.liveguard.campaign_open', {singular})}
                    </Button>
                </div>

            </div>

        </Drawer>

    </div>


    return { liveGuarded: inUse && liveGuarded, liveGuard }
}

