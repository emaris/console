import { mailType } from "#app/mail/constants"
import { MailSlot } from "#app/mail/module"
import { Module } from "#app/module/model"
import { pushEventType } from '#app/push/constants'
import { settingsType } from "#app/settings/constants"
import { SettingsSlot } from "#app/settings/module"
import { tenantType } from "#app/tenant/constants"
import { userType } from "#app/user/constants"
import { UserSlot } from "#app/user/module"
import { useLogin } from '#app/user/store'
import { deepequals } from '#app/utils/common'
import { UserPreferencesForm } from "#campaign//preferences/UserPreferences"
import { PartyInstanceLabel } from "#campaign/party/Label"
import { useDashboard } from '#dashboard/api'
import { eventType } from "#event/constants"
import { EventSlot } from "#event/module"
import { messageType } from "#messages/constants"
import { Topic } from "#messages/model"
import { MessageSlot } from "#messages/module"
import { RouteResolver, TopicInfo, TopicResolver } from "#messages/registry"
import { productType } from "#product/constants"
import { useProductStore } from "#product/store"
import { requirementType } from "#requirement/constants"
import { useRequirementStore } from "#requirement/store"
import moment from "moment-timezone"
import { ApplicationSettings, ApplicationSettingsFields } from "./Settings"
import { anyPartyTopic, campaignIcon, campaignPlural, campaignSingular, campaignType, complianceIcon, compliancePlural, complianceScaleCategory, complianceSingular, complianceType, defaultRelativeDate, defaultStatisticalHorizon, dueDateMailTopic, dueDateReminderMailTopic, subAssessedMailTopic, subPendingApprovalMailTopic, subRequestForChangeMailTopic, subSharedMailTopic, subSubmittedMailTopic, subchangeMailTopic, timelinessIcon, timelinessPlural, timelinessSingular, timelinessType } from "./constants"
import { useMessageRouter } from "./messagerouter"
import { CampaignAppSettings } from "./model"
import { usePartyInstanceStore } from './party/api'
import { useCampaignPreferencesValidation } from './preferences/validation'
import { ProductInstanceLabel } from "./product/Label"
import { ProductInstance, useProductInstanceStore } from "./product/api"
import { usePushCampaignSlot } from "./pushevents"
import { RequirementInstanceLabel } from "./requirement/Label"
import { RequirementInstance, useRequirementInstanceStore } from "./requirement/api"
import { useCampaignStore } from './store'
import "./styles.scss"
import { submissionType } from "./submission/constants"
import { useRevisions } from './submission/revision'
import { useSubmissionStore } from './submission/store'
import { useCampaignValidation } from './validation'


export const useCampaignModule = (): Module => {

    const validation = useCampaignValidation()
    const prefvalidation = useCampaignPreferencesValidation()
    const cmpstore = useCampaignStore()
    const partyinst = usePartyInstanceStore()
    const reqinst = useRequirementInstanceStore()
    const prodinst = useProductInstanceStore()
    const dashboard = useDashboard()
    const substore = useSubmissionStore()
    const msgroute = useMessageRouter()
    const reqstore = useRequirementStore()
    const prodstore = useProductStore()

    const login = useLogin()

    const pushCampaignSlot = usePushCampaignSlot()
    const revisions = useRevisions()

    return {

        icon: campaignIcon,
        type: campaignType,

        nameSingular: campaignSingular,
        namePlural: campaignPlural,

        [eventType]: {

            enable: true

        } as EventSlot


        ,

        [settingsType]: {

            validate: (settings: ApplicationSettings) => validation.validateApplicationSettings(settings as any),

            defaults: (): CampaignAppSettings => ({

                [campaignType]: {
                    approveCycle: 'all',
                    adminCanEdit: false,
                    liveGuard: undefined,
                    suspendOnEnd: true,
                    suspendSubmissions: false,
                    adminCanSubmit: false,
                    partyCanSeeNotApplicable: false,
                    statisticalHorizon: defaultStatisticalHorizon,
                    complianceScale: complianceScaleCategory,
                    defaultRelativeDate: defaultRelativeDate,
                    timeZone: moment.tz.guess()

                }
            }),

            component: ApplicationSettingsFields

        } as SettingsSlot<ApplicationSettings>


        ,

        [userType]: {

            preferencesComponent: UserPreferencesForm,

            preferencesValidation: prefvalidation.validateUserPreferences


        } as UserSlot

        ,

        [mailType]: {
            topics: [
                { key: 'campaign.mail.topic.due', value: dueDateMailTopic },
                { key: 'campaign.mail.topic.due_reminder', value: dueDateReminderMailTopic },
                { key: 'campaign.mail.topic.submission', value: subchangeMailTopic },
                { key: 'campaign.mail.topic.submission_pendingapproval', value: subPendingApprovalMailTopic },
                { key: 'campaign.mail.topic.submission_requestchanges', value: subRequestForChangeMailTopic },
                { key: 'campaign.mail.topic.submission_submitted', value: subSubmittedMailTopic },
                { key: 'campaign.mail.topic.submission_assessed', value: subAssessedMailTopic },
                // {key: 'campaign.mail.topic.submission_published', value: subPublishedMailTopic},
                { key: 'campaign.mail.topic.submission_shared', value: subSharedMailTopic }
            ]
        } as MailSlot

        ,

        [pushEventType]: pushCampaignSlot

        ,

        [messageType]: {

            resolvers: (): TopicResolver[] => [

                {
                    type: tenantType,
                    resolve: (topic: Topic) => {

                        if (deepequals(topic, anyPartyTopic))
                            return undefined

                        const [cid, tid] = topic.name.split(":")

                        const campaign = cmpstore.lookup(cid);

                        if (!campaign)
                            return { label: topic.name } as TopicInfo

                        const party = partyinst.on(campaign).safeLookupBySource(tid)

                        const route = `${dashboard.on(campaign).routeToParty(party)}`

                        const routeToMessages = `${route}/${messageType}`

                        return {

                            label: <PartyInstanceLabel linkTo={route} linkTarget={submissionType} instance={party} className='topic-icon' noDecorations noOptions />,
                            tip: <PartyInstanceLabel instance={party} noLink noIcon noOptions noDecorations />,
                            link: routeToMessages
                        }
                    }
                },



                {
                    type: requirementType,
                    resolve: (topic: Topic) => {

                        const [cid, id] = topic.name.split(":")

                        const campaign = cmpstore.lookup(cid);

                        if (!campaign)
                            return { label: topic.name } as TopicInfo

                        const asset = reqinst.on(campaign).safeLookupBySource(id)

                        const req = reqstore.safeLookup(asset.source)
                        const tip = !!req?.properties?.version

                        const logged = login.logged()

                        if (logged.managesMultipleTenants()) {

                            const route = `${dashboard.on(campaign).routeToAsset(asset)}`
                            const routeToMessages = `${route}/${messageType}`

                            return {

                                label: <RequirementInstanceLabel linkTo={route} instance={asset as RequirementInstance} className='topic-icon' noDecorations noOptions noTip={!tip} />,
                                tip: <RequirementInstanceLabel instance={asset as RequirementInstance} noLink noIcon noOptions />,
                                link: routeToMessages
                            }

                        }

                        else {

                            const submissions = substore.on(campaign)

                            const trail = submissions.lookupTrailKey({ campaign: cid, party: logged.tenant, asset: id, assetType: requirementType })

                            const revs = revisions.on(trail)

                            // const submission = submissions.lookupTrail(tid)?.submissions.find(s=>s.id===sid)
                            const submission = revs.latest()

                            if (!submission)
                                return {

                                    label: <RequirementInstanceLabel instance={asset as RequirementInstance} className='topic-icon' noLink noDecorations noOptions noTip={!tip} />,

                                }


                            const linkTo = dashboard.on(campaign).partyRouteToSubmission(submission)
                            const linkToMessage = `${linkTo}?tab=${messageType}`

                            return {

                                label: <RequirementInstanceLabel linkTo={linkTo} instance={asset as RequirementInstance} className='topic-icon' noDecorations noOptions noTip={!tip} />,
                                tip: <RequirementInstanceLabel instance={asset as RequirementInstance} noLink noIcon noOptions />,
                                link: linkToMessage
                            }

                        }
                    }
                },

                {
                    type: productType,
                    resolve: (topic: Topic) => {

                        const [cid, id] = topic.name.split(":")

                        const campaign = cmpstore.lookup(cid);

                        if (!campaign)
                            return { label: topic.name } as TopicInfo

                        const asset = prodinst.on(campaign).safeLookupBySource(id)

                        const prod = prodstore.safeLookup(asset.source)
                        const tip = !!prod?.properties?.version

                        const logged = login.logged()

                        if (logged.managesMultipleTenants()) {

                            const route = `${dashboard.on(campaign).routeToAsset(asset)}`
                            const routeToMessages = `${route}/${messageType}`

                            return {

                                label: <ProductInstanceLabel linkTo={route} instance={asset as ProductInstance} className='topic-icon' noDecorations noOptions noTip={!tip} />,
                                tip: <ProductInstanceLabel instance={asset as ProductInstance} noLink noIcon noOptions />,
                                link: routeToMessages
                            }

                        }

                        // resolves to latest submission of corresponding trail
                        else {

                            const submissions = substore.on(campaign);

                            const trail = submissions.lookupTrailKey({ campaign: cid, party: logged.tenant, asset: id, assetType: productType })

                            const revs = revisions.on(trail)

                            // const submission = submissions.lookupTrail(tid)?.submissions.find(s=>s.id===sid)
                            const submission = revs.latest()

                            if (!submission)
                                return {

                                    label: <ProductInstanceLabel instance={asset as ProductInstance} className='topic-icon' noLink noDecorations noOptions noTip={!tip} />,

                                }


                            const linkTo = dashboard.on(campaign).partyRouteToSubmission(submission)
                            const linkToMessage = `${linkTo}?tab=${messageType}`

                            return {

                                label: <ProductInstanceLabel linkTo={linkTo} instance={asset as ProductInstance} className='topic-icon' noDecorations noOptions noTip={!tip} />,
                                tip: <ProductInstanceLabel instance={asset as ProductInstance} noLink noIcon noOptions />,
                                link: linkToMessage
                            }

                        }


                    }
                }
            ],

            routes: (): RouteResolver => ({ routeFor: msgroute.route })

        } as MessageSlot


    }
}


export const useComplianceModule = (): Module => {

    return {

        icon: complianceIcon,
        type: complianceType,

        nameSingular: complianceSingular,
        namePlural: compliancePlural,
    }
}

export const useTimelinessModule = (): Module => {

    return {
        icon: timelinessIcon,
        type: timelinessType,

        nameSingular: timelinessSingular,
        namePlural: timelinessPlural,

    }
}