
import { buildinfo } from "#app/buildinfo"
import { Button } from "#app/components/Button"
import { useListState } from "#app/components/hooks"
import { Column, VirtualTable } from "#app/components/VirtualTable"
import { iamPlural } from "#app/iam/constants"
import { specialise } from "#app/iam/model"
import { icns } from "#app/icons"
import { useT } from '#app/intl/api'
import { useL } from '#app/model/multilang'
import { Page } from "#app/scaffold/Page"
import { Titlebar } from "#app/scaffold/PageHeader"
import { Sidebar } from "#app/scaffold/Sidebar"
import { Topbar } from "#app/scaffold/Topbar"
import { useContextFilter } from '#app/system/filter'
import { TagList } from "#app/tag/Label"
import { useTagStore } from '#app/tag/store'
import { useLogged } from '#app/user/store'
import { useCampaignPermissions } from "#campaign/CampaignPermissions"
import { campaignPlural, campaignSingular, campaignType } from "#campaign/constants"
import { ArchiveLabel, CampaignLabel, SuspendedLabel } from "#campaign/Label"
import { Campaign, useCampaignModel, useCampaignStatus } from "#campaign/model"
import { useCampaignRouting, useCampaignStore } from '#campaign/store'
import { useLocation } from "react-router"
import { campaignActions } from './actions'
import { useCampaignClient } from './client'
import { useCampaignValidation } from './validation'


export const campaignGroup = campaignType


export const CampaignList = () => {

    const l = useL()
    const t = useT()

    const [Permissions, showPermissions] = useCampaignPermissions()

    const { pathname } = useLocation()
    //const history = useHistory()

    const [singular, plural] = [t(campaignSingular), t(campaignPlural)]

    const tags = useTagStore()
    const logged = useLogged()
    const client = useCampaignClient()
    const store = useCampaignStore()
    const model = useCampaignModel()
    const status = useCampaignStatus()
    const routing = useCampaignRouting()

    
    const {validationErrors } = useCampaignValidation()

    const liststate = useListState<Campaign>()

    // const open = (c: Campaign) => <Button key={1} icn={icns.open} linkTo={campaigns.routeTo(c)}>{t("common.buttons.open")}</Button>
    // const remove = (campaign: Campaign) =>
    //     <Button
    //         key={2}
    //         icn={icns.remove}
    //         enabled={logged.can(manage)}
    //         disabled={campaigns.isArchived(campaign)}
    //         onClick={() =>
    //             campaigns.remove(
    //                 campaign.id,
    //                 () => liststate.setSelected(liststate.selected.filter(l => l.id !== campaign.id)),
    //                 !buildinfo.development
    //             )
    //         } >{t("common.buttons.remove")}
    //     </Button>

    // const clone = (campaign: Campaign) => <Button key={3} disabled={campaigns.isArchived(campaign)} 
    //      icn={icns.clone} onClick={() => {
    //         campaigns.setNext({ source: campaigns.clone(campaign), lineage: campaign, type: 'clone' })
    //         history.push(`${pathname}/new`)
    //     }} >{t("common.buttons.clone")}</Button>

    const removeAll = <Button type="danger" disabled={liststate.selected.length < 1}
        onClick={() => client.removeAll(liststate.selected.map(l => l.id), () => liststate.setSelected([]))} >
        <span style={{ fontVariantCaps: "all-small-caps" }}>(DEV)</span> {t("common.labels.remove_all", { count: liststate.selected.length })}
    </Button>

    const rights = <Button enabled={liststate.selected.length > 0} icn={icns.permissions} style={{ marginTop: 30 }} onClick={showPermissions}>{t(iamPlural)}</Button>

    const add = <Button type="primary" icn={icns.add} enabled={logged.can(campaignActions.manage)} onClick={() => routing.setNext({ ...routing.next() })} linkTo={`${pathname}/new`} >{t("common.buttons.add_one", { singular })}</ Button>

    const canManage = (r: Campaign) => logged.can(specialise(campaignActions.manage, r.id))
    const readonly = ({ rowData }) => canManage(rowData) || icns.readonly(t("common.labels.readonly"))


    const unfilteredData = store.all().sort(model.comparator)

    const { ContextSelector, contextFilteredData } = useContextFilter({
        data: unfilteredData,
        group: campaignGroup
    })

    // eslint-disable-next-line
    const rowValidation = ({ rowData: campaign }) => {

       if (store.isLoaded(campaign.id)) {
            const errors = validationErrors(campaign)
            return errors === 0 || icns.error(t("common.feedback.issues", { count: errors }))
        }

        //campaigns.fetchOne(campaign.id)

        return true

    }

    const data = contextFilteredData

    return (
        <Page>

            <Sidebar>
                {add}
                {rights}

                {buildinfo.development &&
                    <>
                        <br />
                        {removeAll}
                    </>
                }
            </Sidebar>

            <Topbar>

                <Titlebar title={plural} />

                {add}
                {rights}

            </Topbar>


            <VirtualTable data={data} rowKey="id" state={liststate} 
                filterGroup={campaignGroup} filterBy={model.stringify} filterPlaceholder={t("common.components.table.filter_placeholder", { plural: plural.toLowerCase() })}
                filters={[ContextSelector]}
                sortBy={[['name', 'asc']]}
                //actions={c => [open(c), remove(c), clone(c)]}
                 >

                <Column  flexGrow={2} title={t("common.fields.name_multi.name")} decorations={[readonly, /* rowValidation */]}
                    dataKey="name" dataGetter={(c: Campaign) => l(c.name)} cellRenderer={r => <CampaignLabel campaign={r.rowData} />} />

                <Column width={150} title={t("campaign.fields.status.name")} dataKey="status" dataGetter={(c: Campaign) => status.status(c).label}

                    cellRenderer={r => model.isArchived(r.rowData) ? <ArchiveLabel campaign={r.rowData} /> : model.isSuspendSubmissions(r.rowData) ? <SuspendedLabel campaign={r.rowData} /> : <CampaignLabel mode='tag' statusOnly campaign={r.rowData} />} />

                <Column flexGrow={1} sortable={false} title={t("common.fields.lineage.name")} dataKey="lineage" dataGetter={(c: Campaign) => <CampaignLabel targetMode='lineage' campaign={c} />} />

                {tags.allTagsOf(campaignType).length > 0 &&
                    <Column sortable={false} flexGrow={1} title={t("common.fields.tags.name")} dataKey="tags" dataGetter={(c: Campaign) => <TagList taglist={c.tags} />} />
                }

            </VirtualTable>


            <Permissions resourceRange={liststate.selected} />

        </Page>
    )

}