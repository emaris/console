import { tenantType } from '#app/tenant/constants'
import { Message, Topic, messageTopicSeparator } from '#messages/model'
import { productType } from '#product/constants'
import { requirementType } from '#requirement/constants'
import { utils } from 'apprise-frontend-core/utils/common'
import { anyPartyTopic, campaignType } from './constants'
import { submissionType } from './submission/constants'



export const useMessageHelper = () => {


    const self = {

        campaignTopics: (c: string): Topic[] => [{ type: campaignType, name: c }],

        assetTopics: (type: string, c: string, a: string): Topic[] => [{ type, name: `${c}:${a}` }],

        partyTopics: (c: string, p: string): Topic[] =>  [{ type: tenantType, name: `${c}:${p}` }],

        submissionTopics: (c: string, t:string): Topic[] =>  [{ type: submissionType, name: `${c}:${t}` }],

        message: (m: Message) => {


            const selfmsg = {

                includesTopic: (type:string) => m.topics.some(t => t.type ===type)

                ,

                extract: () => {

                    const extracts = {

                        campaign: undefined! as string,
                        requirements: [] as string[],
                        products: [] as string[],
                        tenants: [] as string[]

                    }
                    m.topics.forEach(t => {

                        switch (t.type) {

                            case campaignType: extracts.campaign = t?.name.split(messageTopicSeparator)?.[0]; break
                            case requirementType: extracts.requirements.push(t?.name.split(messageTopicSeparator)?.[1]); break
                            case productType: extracts.products.push(t?.name.split(messageTopicSeparator)?.[1]); break

                            // make sure not to include * topic.
                            case tenantType: utils().deepequals(t, anyPartyTopic) || extracts.tenants.push(t?.name.split(messageTopicSeparator)?.[1]); break
                            
                        }

                    })


                    return extracts
                }


            }

            return selfmsg
        }


    }

    return self
}