
import { campaignRoute } from "#campaign/constants"
import { CampaignDetail, NewCampaignDetail } from "#campaign/Detail"
import { Route, Switch } from "react-router"
import { CampaignList } from './List'


export const Campaigns = () =>

    <Switch>
        <Route exact path={campaignRoute} component={CampaignList} />
        <Route path={`${campaignRoute}/new`} component={NewCampaignDetail} />
        <Route path={`${campaignRoute}/:id/:type`} component={CampaignDetail} />
        <Route path={`${campaignRoute}/:id`} component={CampaignDetail} />
    </Switch>

