import { EventEmitter } from 'events';


// sprinkles some typing over a global event emitter, adding support for hierarchical topics.



let globalEmitter: EventEmitter;


export type Emitter = ReturnType<ReturnType<typeof getEmitter>['for']>

export const initEmitter = () => globalEmitter = new EventEmitter()

const separator = '.'

export const getEmitter = () => {


    return {

        for: <E>(topic: string) => {

            const self = {

                publish: (e: E) => {
                    
                    subtopics(topic).forEach(t => globalEmitter.emit(t, e))

                },

                subscribe: (listener: (_: E) => void) => {

                    globalEmitter.on(topic, listener)

                    return () => globalEmitter.off(topic, listener)

                }

            }

            return self
        }


    }

}


const subtopics = (topic: string): string[] => {

    const lastdot = topic.lastIndexOf(separator)

    return [topic, ...lastdot > 0 ? subtopics(topic.slice(0, lastdot)) : []]
}
