
// Black: 30
// Red: 31
// Green: 32
// Yellow: 33
// Blue: 34
// Magenta: 35
// Cyan: 36
// White: 37

export const greenlog = '\x1b[32m%s\x1b[0m'
export const bluelog = '\x1b[36m%s\x1b[0m'
export const redlog = '\x1b[31m%s\x1b[0m'