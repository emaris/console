
import { AsyncLocalStorage } from 'async_hooks';
import { utils } from './common';

// builds on Async Local Storage (ASL) to provide a in-memory bus with nested "async scopes".
// an "async scope" extends over chains of async invocations like promises, async functions, and various built-in (but not callbacks).
// contexts are arbitrary records, and since ASL holds only context identifiers, they can hold data of any kind and size.
// runInContext() creates a new context for the execution of its argument. the context masks the current context, if any exists.
// this means that data in the current context is visible in the new context but it can be overridden.
// when a context goes out of scope any data added to the context is no longer accessible.
// a context is mutable and can be changed with plain assignments. 
// this means that changes made to object structures can escape the current context, and should be avoided.


export type Context = Record<string, any>
export type ContextualTask<T> = () => T

const als = new AsyncLocalStorage<String>()
const contexts = new WeakMap<String, Context>()

const noContext = 'none'

export const context = <C extends Context = Context>() => {


    const self = {


        // returns the current context.
        get: (fallback: C = {} as C) => (contexts.get(als.getStore() ?? noContext) ?? fallback) as C,

        // changes the current context (equivalent to read and assign separately).
        set: (producer: (_: C) => void) => producer(self.get()),

        //  returns a proxy of a contextual value which access it lazily, ie. using the current context at the time of usage.
        //  works with objects (proxies property dereferences) and functions (proxies invocations). 
        getProxy: <T extends object>(producer: (_: C) => T) => {

            // the initial value is used to pass runtime checks, such as: can this be invoked as a function?
            // if there isn't an initial value, one can still exist at the time of usage, so we use a dummy function as a fallback.
            // why a function? because an object fallback can't be invoked like a function, so wouldn't pass the checks.
            // iow, there are check on functions, not callbacks.
            const initialValue = producer(self.get()) ?? (() => { })

            return new Proxy(initialValue, {

                get: (_, prop) => Reflect.get(producer(self.get()) ?? {}, prop),
                set: (_, prop, value) => Reflect.set(producer(self.get()) ?? {}, prop, value),

                apply: (_, ...rest) => Reflect.apply(producer(self.get()) as Function, ...rest),
                deleteProperty: (_, prop) => Reflect.deleteProperty(producer(self.get()) ?? {}, prop)

            })
        }

        ,

        // creates a new context for the lifetime of an async task.
        // the context inherits from the current context, if one exists.
        // if the context is imitialised with a stable key, 
        // and some data is already associated with that key, the data will be reused.
        create: (initial: C = {} as C) => {

            const clause = {

                as: (key: String) => ({

                    for: <T>(task: ContextualTask<T>) => {

                        if (!contexts.has(key)) {

                            const merged = contexts.get(key) ?? utils().merge(self.get(), initial)

                            contexts.set(key, merged)
                        }

                        
                        return als.run(key, task)

                    }
                }),

                for: <T>(task: ContextualTask<T>) => clause.as(new String(`ctx-${crypto.randomUUID()}`)).for(task)

            }

            return clause
        }

        ,

        close: () => contexts.delete(als.getStore() || noContext)

    }

    return self


}

// takes a task that wouldn't run in the current context
// and returns a function that runs it in the current context.
export const contextAware = <T extends (...args: any[]) => any>(task: T) => {

    const runInCurrentContext = AsyncLocalStorage.snapshot()

    return (...args: Parameters<T>): ReturnType<T> => runInCurrentContext(task, ...args)

}