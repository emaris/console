import { redlog } from './constants';
import { context } from './context';
import { initEmitter } from './emitter';


// wraps a global context and error boundary around application code.
// if the code is asynchronous, it wait for the output, so as to catch possible errors.


export const initApp = async (initialiser: () => unknown) => {

    try {

        initEmitter()

        return await context().create().for(initialiser)

    }
    catch (e) {

        console.error(redlog, "error on init", e)
    }

}