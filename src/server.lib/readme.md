
`server.lib` collects code that should move to library support for future fullstack apps. 

it's organised into folders that roughly correspond to the libraries that ought to host the code.

but the code is too experimental for now, and scanty overall, to move it to the actual libraries.

also the libraries have been named as frontend libraries, so that knot would need to be untied before we rush to put backend code into them.

an alternative to support the first fullstack app would be to copy this folder into a generic `apprise-fullstack-support`.
