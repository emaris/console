import { PendingQuery, Row } from 'postgres'
import { getSql } from './sql'


export type QueryFragment = PendingQuery<Row[]>

/* eslint-disable */
export const dbutils = () => {

    const rav = getSql()


    const self = {
        

        join: (parts: QueryFragment[] | undefined, separator: QueryFragment) => 
            
            parts?.length ? rav`${parts.flatMap((a,i) => i ? [separator,a] : a)}`  : rav``


    }

    return self
}