import { context } from '#server.lib/core/context'
import postgres from 'postgres'


export type Sql = {

    sql: ReturnType<typeof postgres>

}

let sql: Sql['sql']


export const sqlInitialised = () => !!sql

export const setSql = (fun:typeof sql) => sql = fun

// returns the tag function for SQL queries.
// should be assigned to a variable called sql for syntax checks via SafeQL.
// it's a proxy that resolves to the tag function currently in contxt.
export const getSql = () => context<Sql>().getProxy(c => c.sql ?? sql)