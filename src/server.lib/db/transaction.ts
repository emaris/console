import { context } from '#server.lib/core/context'
import postgres from 'postgres'
import { getSql, Sql } from './sql'


export const isTransactional = (sql: postgres.Sql) => sql[transactionalMarker] != undefined
export const transactionId = (sql: postgres.Sql) => isTransactional(sql) ? sql[transactionalMarker] : '<not in transaction>'


const transactionalMarker = Symbol('transaction')

// beings
export const transaction = <T>(task: () => T) => {

    const { begin } = getSql()

    return begin ?

        begin(transactional => {

            transactional[transactionalMarker] = `T-${crypto.randomUUID()}`

            return context<Sql>().create({ sql: transactional }).for(task)

        })

        : task()
}