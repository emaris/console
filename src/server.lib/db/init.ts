import { greenlog } from '#server.lib/core/constants'
import postgres, { Options } from 'postgres'
import { setSql, sqlInitialised } from './sql'



export const initDatabase = async (options?: Options<{}>) => {

    if (sqlInitialised())
        return


    const required = {
        host: process.env.DB_HOST,
        database: process.env.DB_NAME,
        port:  parseInt(process.env.DB_PORT || '5432'),
        user: process.env.DB_USER,
        password: process.env.DB_PWD
    }


    const merged = { ...required, ...options }


    const missing = Object.keys(required).filter(prop => merged[prop] === undefined)

    if (missing.length)
        throw new Error(`missing database configuration for [${missing.join(",")}]`)

    const { host, database, port, user, password } = merged

    if (isNaN(port) || port < 1000 || port > 65535)
        throw new Error(`invalid database port: ${port}`)


    const url = `postgres://${user}:${password}@${host}:${port}/${database}`

    const sql = postgres(url, {
        max: 10,
        ...merged,
        ssl:"prefer"

    })

    setSql(sql)

    await sql<{ test: number }[]>`select 1 as test`

    console.log(greenlog, "initialised db connection...")


}