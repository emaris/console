import { bluelog, greenlog } from '#server.lib/core/constants'
import traps from '@dnlup/fastify-traps'
import { serverConfig } from './config'
import { registerGlobalContextPlugin, registerRequestContextPlugin, registerRequestLogPlugin } from './plugins'
import { createServer, ServerOptions } from './server'


export const defaultPort = '8080'


type Mode = 'production' | 'development'

export const initServer = (opts: Partial<{

    name?:string
    mode: Mode

} & ServerOptions> = {}) => {

    const { mode = import.meta.env.MODE as Mode, name='api server', ...rest } = opts

    const server = createServer(rest)

    const config = serverConfig('validate')

    registerGlobalContextPlugin()
    registerRequestContextPlugin()
    registerRequestLogPlugin()

    return {

        instance: server,

        start: async () => {

            if (mode === 'development') {

                await server.ready()

                console.log(greenlog, `${name} ready as middleware...`)
            }

            else {


                if (mode === 'production')
                    server.register(traps)

                server.listen({ port: config.port, host: '0.0.0.0' }, (err, address) => {  // listens to all addresses in-container

                    if (err) {
                        server.log.error(err)
                        process.exit(1)
                    }

                    console.log(bluelog, `sever ready listening on ${address}...`)

                })

            }


            return server
        }
    }

}
