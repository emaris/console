
const defaultPort = '8080'

export const serverConfig = (validate?: 'validate') => {

    const port = parseInt(process.env.PORT ?? defaultPort)

    if (validate) {
        
        if (isNaN(port))
            throw new Error("invalid port")

        // todo?
    }

    return { port }
}