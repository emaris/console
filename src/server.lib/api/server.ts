import { context } from '#server.lib/core/context'
import fastify, { FastifyHttpOptions, FastifyInstance } from 'fastify'


// keeps server in module scope, expects it created at init time.


let server: FastifyInstance

export type ServerOptions = FastifyHttpOptions<any,any>

export const getServer = () => server

// note: it's important to create the instance explicitly on init, because live reload at dev time may preserve it 
// but re-run clients that try to configure it. this blows up, because the instance is already started.
// createServer() always uses a fresh instance, overwriting older ones that might have been preserved on reload.
export const createServer = (opts: Partial<ServerOptions>={}) => server = fastify(opts)





export type Request = {

    request: {
        id: string
    }

}

export const request = () => context<Request>().getProxy(c => c.request)