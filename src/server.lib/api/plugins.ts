import { bluelog } from '#server.lib/core/constants';
import { context, contextAware } from '#server.lib/core/context';
import { getServer, request, Request } from './server';


// global context is lost in the request cycle if we register a callback (reasons unknown inside fastify).
// so we register first a callback only to capture the context, so all subsequent ones will see it.
export const registerGlobalContextPlugin = () => {

    const server = getServer()

    server.addHook('onRequest', contextAware((_, __, done) => {
        done()
    }))

}

// initialises request with a request id.
export const registerRequestContextPlugin = () => {

    const server = getServer()

    server.addHook('onRequest', (_, __, done) => {

        const id = `R-${crypto.randomUUID()}`

        context<Request>().create({ request: { id } }).for(done)

    })

    // cleanup.
    server.addHook('onResponse', async () => {

        //console.log('closing request context')


        context<Request>().close()

    })


}

// wraps a context around the request.
// initialises the context with a request id.
export const registerRequestLogPlugin = () => {

    const server = getServer()

    server.addHook('onRequest', async (req) => {

        var originalLog = console.log;

        console.log = function (...args: Parameters<typeof console.log>) {

            const dts = new Date().toLocaleDateString(undefined, { day: '2-digit', month: '2-digit' })
            const tts = new Date().toLocaleTimeString(undefined, { timeStyle: 'short', hourCycle: 'h24' })

            originalLog.apply(console, [bluelog, `[${request().id} ${dts} ${tts}]:`, ...args]);
        };

        (req as any).originalLog = originalLog
    })

    server.addHook('onResponse', async req => {

        //console.log('restoring standard log...')

        console.log = (req as any).originalLog;

    })

}