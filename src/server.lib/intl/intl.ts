
import { context } from '#server.lib/core/context';
import { Language } from 'apprise-frontend-core/intl/language';
import i18next, { TFunction } from "i18next";


export const defaultLanguage = 'en'

export type IntlContext = {

    language: Language
    t: TFunction                            // generated for current language.
    tFor: typeof i18next.getFixedT          // cross-language.   
}


let language : Language
let t : TFunction
let tFor: typeof i18next.getFixedT  

export const intlInitialised = () => !!tFor

// returns the translation for a given language, annd the current lanaguage by default.
export const getT = (lng?: Language) =>  context<IntlContext>().getProxy(c => lng ? c.tFor?.(lng) ?? c.tFor?.(lng) : c.t ?? t) 

export const setT = (fun: typeof tFor) => tFor = fun

export const setCurrentLanguage = (lng: Language = defaultLanguage) => {

    language = lng

    // updates current translation function
    t = tFor(lng)

}


export const getCurrentLanguage = () => context<IntlContext>().get().language ?? language