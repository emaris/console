

import { greenlog, redlog } from '#server.lib/core/constants';
import { Language } from 'apprise-frontend-core/intl/language';
import i18next, { ResourceKey } from "i18next";
import { intlInitialised, setCurrentLanguage, setT } from './intl';



export const initIntl = async (translations: Record<string, ResourceKey> = { }) => {

    if (intlInitialised())
        return

    let error: any

    const resources = Object.entries(translations).reduce((acc, [lang, content]) => ({ ...acc, [lang]: { translation: content } }), {})

    const language = Object.keys(resources)?.[0] ?? 'en'

    try {

        await i18next.init({

            partialBundledLanguages: true,

            lng: language,
            returnEmptyString: false,
            fallbackLng: language,

            debug: false,
            nsSeparator: "::",
            resources,
            interpolation: {
                escapeValue: false,
                format: function (value, format) {
                    if (format === 'uc') return value.toUpperCase();
                    if (format === 'lc') return value.toLowerCase();
                    return value;
                }
            }

        }, e => {

            if (e)
                error = e
        }

        )

        if (error)
            throw error

 
        setT(i18next.getFixedT.bind(i18next))
        setCurrentLanguage(language as Language)

        console.log(greenlog, "initialised intl...")


    }
    catch (e) {

        console.log(redlog, "can't initialise intl", e)
        throw e

    }

}