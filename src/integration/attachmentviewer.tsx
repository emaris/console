

import { Topbar } from '#app/scaffold/Topbar'
import { PathwayConfig, PathwayData, PathwayRenderProps } from '#integration/model'
import { Fragment, PropsWithChildren } from 'react'
import { AttachmentDownloaderProps, useAttachmentDownlod } from './attachmentDownloader'


export const AttachmentViewer = <C extends PathwayConfig, D extends PathwayData>(props: PathwayRenderProps<C, D> & PropsWithChildren<AttachmentDownloaderProps>) => {

    const { children } = props

    const { downloadBtn } = useAttachmentDownlod(props)

    return <Fragment>

        <Topbar>
            {downloadBtn}
        </Topbar>

        {children}

    </Fragment>



}
