import { initServer } from '#server.lib/api/init'
import { initApp } from '#server.lib/core/app'

// entry-point to the backend.

// runs initialisation code.
const app = await initApp(async () => {

     const server = initServer()

      server.instance.get("/", () => {
        
        return "sws operational"
    
    })

    return server.start()


})

// export used at dev time, so that module can be awaited on and reloaded in dev server.
export default app