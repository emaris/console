import { Button } from "#app/components/Button"
import { Form } from '#app/form/Form'
import { icns } from '#app/icons'
import { useT } from "#app/intl/api"
import { useToggleBusy } from '#app/system/api'
import { TimeLabel } from '#app/time/Label'
import { notify, showAndThrow } from '#app/utils/feedback'
import { utils } from 'apprise-frontend-core/utils/common'
import moment from 'moment-timezone'
import { Fragment, PropsWithChildren, useContext, useEffect, useRef } from "react"
import { PathwayConfig, PathwayData, PathwayRenderProps } from './model'
import { MultiPathwayContext } from './multipathway/context'
import { ravIntegrationId } from './rav/constants'
import './renderform.scss'

export type RenderFormProps<C extends PathwayConfig = PathwayConfig, D extends PathwayData = PathwayData> = PathwayRenderProps<C,D> & PropsWithChildren<{

    explainer: JSX.Element

    onRefresh: 'compose' | (() => Promise<void>)

    disabled?: boolean

}>


export const RenderForm = <C extends PathwayConfig = PathwayConfig, D extends PathwayData = PathwayData> (props: RenderFormProps<C,D>) => {

    const t = useT()

    const toggleBusy = useToggleBusy()

    const { children, onRefresh, data, explainer } = props

    const ctx = useContext(MultiPathwayContext)

    const composer = onRefresh === 'compose'
    const composed = ctx && !composer

    const disabled = composer ? ctx?.get().renderForms.reduce((acc, next) => acc && !!next.disabled, true) : props.disabled

    // this tracks the subscription order with the composer's context.
    // so that we know what to update when the subscription needs to change.
    const position = useRef<number>(undefined!)

    // if this is not composing, then subscribe with composer (if any), on mount and whenever disabled changes.
    useEffect(() => {

        if (composer || !ctx)
            return

        if (position.current === undefined)
            position.current = ctx.get().renderForms?.length


        ctx.set(s => s.renderForms[position.current] = props)

        // eslint-disable-next-line
    }, [disabled])

    // for composed tasks, this gets never executed because button is never mounted (so no individual task handling occurs).
    // for composer tasks, this executed by aggregating the tasks of each composed task (so task handling is aggregated too).
    const refresh = async () => {

        try {

            toggleBusy(ravIntegrationId, t('integration.loading_msg'))

            const composedRefresh = async () => {

                const tasks = ctx?.get().renderForms.map(f => f.onRefresh === 'compose' ? Promise.resolve() : f.onRefresh())

                await Promise.all(tasks)

            }

            const task = composer ? composedRefresh : onRefresh

            await utils().waitNow(200)

            await task()


            notify(t('integration.result_notif'), {icon: icns.checkGreen})

        }

        catch (e) {

            showAndThrow(e as Error, t('integration.refresh_error'))
        }

        finally {

            toggleBusy(ravIntegrationId)
        }
    }

    if (composed)
        return <Fragment>{children}</Fragment>

    return <div className='pathway-render-form'>

        {explainer}

        <Form>
            {children}
        </Form>

        <Button type='primary' icn={icns.download} iconLeft className="integration-refresh-btn" onClick={refresh} disabled={disabled}>
            {t('integration.refresh_btn')}
        </Button>


        {data.timestamp &&

            <div className='integration-timestamp'>
                <div className='timestamp-lbl' >Last Refreshed</div>
                <div className='timestamp-value'>
                <TimeLabel value={moment(data.timestamp)} noIcon />
                </div>
            </div>
            }


    </div>



}