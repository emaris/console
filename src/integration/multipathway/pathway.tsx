import { useStable } from '#app/utils/function'
import { Pathway, PathwayConfig, PathwayData, PathwayDetailProps, PathwayParameterFactory, PathwayRenderProps } from '#integration/model'
import { Detail } from './detail'
import { useParameterSpec } from './parameter'
import { Render } from './render'
import "./styles.scss"
import { Viewer } from './viewer'



export const multipathwayId = 'multipathway'


export type Subpathways = {

    pathways: Pathway<any, any>[]

}


export type Config = PathwayConfig & {

    pathways: { id: string, pathwayId: string, alias?: string, config: any }[]

}

export type MultiPathwayAttachment = Record<string, any>

export interface Data extends PathwayData {

    attachment: { pathways: { id: string, data: { attachment: any } }[] }

}


export const useMultiPathway = (props: {

    icon: JSX.Element
    pathways: Pathway<any, any>[],
    pathwayParameterSpecs: Record<string, PathwayParameterFactory<any, any>>

}): Pathway<Config, Data> => {

    const { icon, pathways, pathwayParameterSpecs } = props

    return {

        id: multipathwayId,
        name: 'integration.multi.name',

        Detail: useStable((props: PathwayDetailProps<Config>) => <Detail pathways={pathways} {...props} />),
        Render: useStable((props: PathwayRenderProps<Config, Data> & { icon?: JSX.Element }) => <Render pathways={pathways} icon={icon} {...props} />),
        Viewer: useStable((props: PathwayRenderProps<Config, Data>) => <Viewer pathways={pathways} {...props} />),

        useParameterSpec: useParameterSpec({ pathways, pathwayParameterSpecs })

    }

}