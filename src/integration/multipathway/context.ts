import { RenderFormProps } from '#integration/renderform';
import { State } from 'apprise-frontend-core/state/api';
import { createContext } from 'react';



export type MultiPathwayState = {

    renderForms: RenderFormProps<any,any>[]

}

export const MultiPathwayContext = createContext<State<MultiPathwayState>>(undefined!)

export const initialMultiPathwayState: MultiPathwayState = {

    renderForms: []
}
