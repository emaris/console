import { useStable } from '#app/utils/function'
import { PathwayExplainer } from '#integration/explainer'
import { Pathway, PathwayRenderProps } from '#integration/model'
import { RenderForm } from '#integration/renderform'
import { StateProvider } from 'apprise-frontend-core/state/provider'
import { initialMultiPathwayState, MultiPathwayContext } from './context'
import { Config, Data, Subpathways } from './pathway'


export type RenderProps = PathwayRenderProps<Config, Data> & Subpathways & {

    icon: JSX.Element
    
}

export const Render = (props: RenderProps) => {

    const { pathways, pathway, data, onChangeData } = props

    const subpathways = pathway.pathways ?? []

    const subdatapathways = data.attachment?.pathways ?? Array.from(subpathways).map(({id}) => ({id}))

    // this functions sees the latest closure as each task updates the same array.
    const changeSubdata = useStable((id:string, subdata: any) => {

         onChangeData({ ...data, attachment: { pathways: subdatapathways.map(sp => sp.id === id ? { ...sp, data: subdata } : sp) } })
    })

    

    return <StateProvider initialState={initialMultiPathwayState} context={MultiPathwayContext}>

        <RenderForm onRefresh='compose' explainer={<PathwayExplainer {...props} />} {...props} >

            {
                // calls each pathway in turn against a matching view its config and data.
                subpathways.map(({ id, pathwayId, config }) => {

                    const pathwaydef = pathways.find(p => p.id === pathwayId) ?? {} as Pathway<any, any>

                    const renderprops = {
                        pathway: config,
                        data: subdatapathways.find(sp => sp.id === id),
                        onChangeData: subdata => changeSubdata(id,subdata)
                    }

                    // renders the pathway in grouped mode.
                    return <pathwaydef.Render key={id} {...renderprops} />

                })}

        </RenderForm>

    </StateProvider>


}