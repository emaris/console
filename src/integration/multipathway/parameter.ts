import { PathwayParameterFactory, PathwayRenderProps } from '#integration/model'
import { IntegrationParameterValue, withPathwayParameterSpec } from '#integration/parameter'
import { Parameter } from '#layout/parameters/model'
import { Config, Data, Subpathways } from './pathway'



export const useParameterSpec = (props: Subpathways & {

    pathwayParameterSpecs: Record<string, PathwayParameterFactory<any, any>>

}) => {

    const { pathwayParameterSpecs } = props

    return () => (props: PathwayRenderProps<Config, Data>) => {

        const { pathway, data } = props

        return withPathwayParameterSpec({

            references: (p: Parameter<IntegrationParameterValue>) => {


                return pathway.pathways.flatMap(({ id, pathwayId, alias, config }) => {

                    const renderprops = { ...props, pathway: config }

                    const { specs } = pathwayParameterSpecs[pathwayId](renderprops)

                    return specs.references(p).map(r => {

                        const [first, ...rest] = r.value.split(".")

                        // wraps reference id to idenfy subpathway that can resolve in textValue().
                        const wrappedId = `${pathwayId}.${id}.${r.id}`

                        // optionally wraps reference values to disambiguate params across sub pathways.
                        const wrappedValue = alias ? `${first}.${alias.toLowerCase()}.${rest.join('.')}` : r.value

                        return ({ ...r, id: wrappedId, value: wrappedValue })


                    })


                })

            }

            ,

            textValue: (p, path) => {

                const [pathwayId, id, ...subpath] = path

                const currentPathway = pathway.pathways.find(sp => sp.id === id)

                const renderprops = {
                    ...props,
                    pathway: currentPathway?.config,
                    data: data.attachment?.pathways.find(sp => sp.id === id)?.data ?? {}

                }

                // console.log(pathway,{pathwayId,id, subpath}, renderprops)

                // NOTE:
                // "pathwayId" is deprecated, we use "id" to identify the pathway. This lets us switch sub-queries without 
                //  invalidating references. 
                //  We can't remove "pathwayId", however as this may break all the references, so we may as well use it as a fall-back
                //  if we don't identify the "pathway".
                const { specs } = pathwayParameterSpecs[currentPathway?.pathwayId ?? pathwayId](renderprops)

                return specs.textValue(p, subpath)
            }

        })

    }

}
