import { useT } from '#app/intl/api';
import { AttachmentViewer } from '#integration/attachmentviewer';
import { PathwayRenderProps } from '#integration/model';
import { PathwayTreeViewer } from '#integration/treeviewer';
import { utils } from 'apprise-frontend-core/utils/common';
import { Config, Data, Subpathways } from './pathway';



export const Viewer = (props: PathwayRenderProps<Config, Data> & Subpathways) => {

    const t = useT()

    const { pathway, data } = props

    const pathwaymap = utils().index(pathway.pathways).by(sp =>sp.id)

    const subdata : any = data.attachment?.pathways?.reduce((acc, next) => ({ ...acc, [pathwaymap[next.id]?.alias ?? next.id]: next.data?.attachment ?? {} }), {})

    const nameparts = pathway.pathways.filter( p=> p.alias).map(p => p.alias)

    const augmentednameparts = nameparts.length ? nameparts.length === pathway.pathways.length ? nameparts : [...nameparts, t('integration.multi.other_namepart').toLowerCase()] : undefined

    const filename = augmentednameparts?.join("+")
    
    return <AttachmentViewer {...props} filename={filename}>
                <PathwayTreeViewer {...props} data={{ ...data, attachment: subdata }} />
            </AttachmentViewer>
}