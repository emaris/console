import { DetailForm } from '#integration/detailform'
import { Pathway, PathwayDetailProps } from '#integration/model'

import { CardlistBox } from '#app/components/CardlistBox'
import { SelectBox } from '#app/form/SelectBox'
import { useT } from '#app/intl/api'
import { StateProvider } from 'apprise-frontend-core/state/provider'
import { utils } from 'apprise-frontend-core/utils/common'
import { cloneElement } from 'react'
import shortid from 'shortid'
import { initialMultiPathwayState, MultiPathwayContext } from './context'
import { Config, Subpathways, } from './pathway'
import { TextBox } from '#app/form/TextBox'



export const Detail = (props: PathwayDetailProps<Config> & Subpathways) => {

    const t = useT()

    const { pathways, pathway, onChange } = props

    const pathwayMap = utils().index(pathways).by(p => p.id)

    const subpathways = pathway.pathways ?? []

    return <DetailForm  {...props}>

        <StateProvider initialState={initialMultiPathwayState} context={MultiPathwayContext}>

            <CardlistBox id='subdetail' className='multipathway-subdetails' singleLabel={t('integration.pathway_lbl')}

                onAdd={() => onChange({ ...pathway, pathways: [...subpathways, { id: shortid(), pathwayId: pathways[0].id, config: {} }] })}

                onMove={(from,to) => onChange({ ...pathway, pathways: subpathways.map((_, i) => i === from ?  subpathways[to] : i === to ? subpathways[from] : _ ) })}

                onRemove={position => onChange({ ...pathway, pathways: subpathways.filter((_, i) => i !== position) })} >

                {subpathways.map(({ id, pathwayId: subpathway, alias, config }, position) => {

                    const pathwaydef = pathways.find(p => p.id === subpathway) ?? {} as Pathway<any, any>

                    const detailprops = {
                        pathway: config ?? {},
                        onChange: (config: any) => onChange({ ...pathway, pathways: subpathways.map((sp, i) => i === position ? { ...sp, config } : sp) })
                    }

                    // simulates component render
                    const detail = cloneElement(<pathwaydef.Detail {...detailprops} />, { grouped: true })

                    return <div key={subpathway} className='multipathway-subdetail'>

                        <SelectBox className='subquery' label={t('integration.pathway_lbl')} validation={{ msg: t('integration.pathway_msg') }}

                            selectedKey={subpathway}
                            onChange={(pathwayId: string) => onChange({ ...pathway, pathways: subpathways.map((sp, i) => i === position ? { id: sp.id, pathwayId, config: {} } : sp) })}
                            getlbl={p => t(pathwayMap[p].name)}
                        >

                            {Object.keys(pathwayMap)}

                        </SelectBox>

                        <TextBox label={t('integration.alias_lbl')} validation={{ msg: t('integration.alias_msg') }}
                                onChange={alias => onChange({...pathway,  pathways: subpathways.map( sp => sp.id === id ? { ...sp, alias } : sp) })}>
                            {alias}
                        </TextBox>

                        {detail}

                    </div>
                })}
            </CardlistBox>

        </StateProvider>

    </DetailForm>

}
