

import { TimeLabel } from "#app/time/Label"
import { PathwayConfig, PathwayData, PathwayRenderProps } from '#integration/model'
import { Divider } from "antd"
import moment from "moment-timezone"
import "./jsonviewer.scss"


export const PathwayJsonViewer = <C extends PathwayConfig, D extends PathwayData>(props: PathwayRenderProps<C, D> & {

    className?: string

}) => {

    const { data, className = '' } = props

    const { attachment, timestamp } = data

    if (!data.attachment)
        return null

   
   return <div className='pathway-viewer'>

        <Divider className="divider" orientation="center">
            <TimeLabel value={moment(timestamp)} noIcon />
        </Divider>

        <pre className={`viewer-data ${className}`} dangerouslySetInnerHTML={{ __html: syntaxHighlight(attachment) }} />

    </div>

}

const syntaxHighlight = (json) => {

    let str = JSON.stringify(json, undefined, 4)
    if (!str) return ""

    str = str
        .replace(/&/g, "&amp;")
        .replace(/</g, "&lt;")
        .replace(/>/g, "&gt;")

    return str.replace(
        /("(\\u[a-zA-Z0-9]{4}|\\[^u]|[^\\"])*"(\s*:)?|\b(true|false|null)\b|-?\d+(?:\.\d*)?(?:[eE][+-]?\d+)?)/g,
        function (match) {
            var cls = "number";
            if (/^"/.test(match)) {
                if (/:$/.test(match)) {
                    cls = "key";
                } else {
                    cls = "string";
                }
            } else if (/true|false/.test(match)) {
                cls = "boolean";
            } else if (/null/.test(match)) {
                cls = "null";
            }
            return '<span class="' + cls + '">' + match + "</span>";
        }
    )
}