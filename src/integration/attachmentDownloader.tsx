import { Button } from '#app/components/Button'
import { icns } from '#app/icons'
import { useT } from '#app/intl/api'
import { saveAs } from 'file-saver'
import { PathwayRenderProps } from './model'


export type AttachmentDownloaderProps = PathwayRenderProps<any,any> & Partial<{

    blob: Blob
    contentType?: string
    filename? : string

}>

export const useAttachmentDownlod = (props: AttachmentDownloaderProps) => {


    const t = useT()
    
    const { data, blob, contentType, filename } = props

    const download = () => saveAs(new Blob([ blob ?? JSON.stringify(data.attachment)], { type: contentType ?? 'application/json' }), filename ?? 'data')

    const downloadBtn = <Button type='primary' icn={icns.download} key="download" enabled={!!data.attachment} enabledOnReadOnly onClick={download}>
        {t("integration.download_btn")}
    </Button>


    return { download, downloadBtn }

}