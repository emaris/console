

import { PathwayConfig, PathwayData, PathwayRenderProps } from '#integration/model'
import { Tree } from "antd"
import { TreeNodeNormal } from 'antd/lib/tree/Tree'
import { isArray } from 'lodash'
import { useCallback, useMemo } from 'react'
import shortid from 'shortid'
import './treeeview.scss'

export const PathwayTreeViewer = <C extends PathwayConfig, D extends PathwayData>(props: PathwayRenderProps<C, D>) => {

    const { data } = props

    const treedata = useCallback( (data: any = {}): TreeNodeNormal[] => data ?

        isArray(data) ?

            data.map((e, j) => {
                
                const deeper = typeof e === 'object' || isArray(e)

                
                return   ({ title: deeper ? <Element v={`[${j + 1}]`} /> : <Value v={e} />, key: shortid(), children: deeper ? treedata(e) : undefined })
            
            
            })


            : typeof data === 'object' ?

                Object.entries(data).reduce((acc, [k, v]) => {

                    const deeper = typeof v === 'object' || isArray(v)

                    return [...acc, {  title: deeper ? <Key k={k} /> : <KeyValue k={k} v={v} />, key:shortid(), children: deeper ? treedata(v) : undefined }]

                }, [] as TreeNodeNormal[])

                : [{ title: <Element v={data} />, key: shortid() }]

        : [], [])

    const memoed = useMemo (() => treedata(data.attachment), [treedata,data.attachment] )


    return  <Tree className='pathway-treeviewer' selectable={false} defaultExpandAll treeData={memoed} />



}

const Dash = () => <span className='keyvalue-dash'>-</span>

const KeyValue = (props: { k: string, v: any }) => {

    const { k, v } = props

    return <div className='keyvalue'>
        <div className='keyvalue-key'><Dash /> {k}:</div>
        <div className='keyvalue-value'>{v}</div>

    </div>
}

const Key = (props: { k: any }) => {

    const { k } = props

    return <div className='key'> {k}</div>

}


const Value = (props: { v: any }) => {

    const { v } = props

    return <div><span><Dash /></span> <span className='value'>{v}</span></div>

}

const Element = (props: { v: any }) => {

    const { v } = props

    return <div className='array-element'>{v}</div>

}
