import { useT } from '#app/intl/api'
import { ParameterContext } from '#layout/context'
import { newParameterFrom, Parameter, ParameterSpec, partialParameterSpec } from '#layout/parameters/model'
import { PropsWithChildren, useContext } from 'react'


export const integrationParamId = `integration-parameter`



export type IntegrationParameter = Parameter<IntegrationParameterValue>
export type IntegrationParameterValue = ParameterSpec


export type PathwayParamBuilder = {

    specs: ParameterSpec,
    generate: (_: string) => IntegrationParameter
}

export type SpecBuilder = (__: Partial<ParameterSpec>) => PathwayParamBuilder


export const withPathwayParameterSpec: SpecBuilder = (spec: Partial<ParameterSpec>) => {

    const fullspecs: ParameterSpec<IntegrationParameterValue> = {

        ...partialParameterSpec,
        ...spec
    }

    return {

        specs: fullspecs,

        generate: (id) => ({

            ...newParameterFrom(integrationParamId),
            id,
            value: {
                ...fullspecs,
                references: p => fullspecs.references(p).map(r => ({ ...r, id: `${id}.${r.id}` }))
            }
        })
    }
}

export const useIntegrationParameter = (): ParameterSpec<IntegrationParameterValue, IntegrationParameter> => {

    return {


        ...partialParameterSpec,


        id: integrationParamId,
        name: 'genericparam',


        references: param => param.value.references(param)


        ,

        textValue: (param, path) => param.value.textValue(param, path)

    }


}

export const IntegrationParameterProvider = (props: PropsWithChildren<{

    param: IntegrationParameter

}>) => {

    const { param, children } = props

    const parent = useContext(ParameterContext) ?? []

    return <ParameterContext.Provider value={param ? [...parent,param] : parent} >
        {children}
    </ParameterContext.Provider>


}


export const useParameterFallbacks = () => {

    const t = useT()

    return {

        NA: `<code class="param-value unresolved">${t('integration.param_na')}</code>`,
        TBC: `<code class="param-value unresolved">${t('integration.param_tbc')}</code>`,
        ERR: `<code class="param-value failed">${t('integration.param_err')}</code>`    
    }
}