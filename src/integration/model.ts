import { Container } from '#layout/components/model'
import { Parameter } from '#layout/parameters/model'
import { StyleProps } from '#layout/style/model'
import { Multilang } from 'apprise-frontend-core/intl/multilang'
import { PathwayParamBuilder } from './parameter'


export type IntegrationParameters = Record<string, any>


export type PathwayConfig = {

    id: string
    explainer?: Multilang
    trigger?: Multilang
}



export type PathwayDetailProps<C extends PathwayConfig> = {

    defaultExplainer?: string | Multilang
    defaultTrigger?: string | Multilang

    pathway: C,
    onChange: (_: C) => void

}

export interface PathwayData {

    timestamp: string
    attachment: any
}


export type PathwayRenderProps<C extends PathwayConfig = PathwayConfig, D extends PathwayData = PathwayData> = {

    pathway: C
    data: D
    onChangeData: (_: D) => void
}

export type PathwayParameterFactory<C extends PathwayConfig = PathwayConfig, D extends PathwayData = PathwayData> = (_: PathwayRenderProps<C, D>) => PathwayParamBuilder

export type Pathway<C extends PathwayConfig = PathwayConfig, D extends PathwayData = PathwayData> = {

    id: string
    name: string

    useParameterSpec: () => PathwayParameterFactory<C, D>

    Detail: React.FC<PathwayDetailProps<C>>
    ComposableDetail?: React.FC<PathwayDetailProps<C>>
    Render: React.FC<PathwayRenderProps<C, D>>
    Viewer: React.FC<PathwayRenderProps<C, D>>
}

export const noPathway: Pathway = {

    id: undefined!,
    name: 'no-pathway',
    useParameterSpec: () => () => ({ generate: () => undefined! as Parameter, specs: undefined! }),
    Detail: () => null,
    Render: () => null,
    Viewer: () => null

}


export type IntegrationSection = StyleProps & Container & {

    pathway: PathwayConfig
}