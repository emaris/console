import { useT } from '#app/intl/api'
import { isMultilang, useTranslatedMultilang } from '#app/model/multilang'
import { useLayoutApis } from '#layout/apicontext'
import { ParametricText } from '#layout/parameters/parametricboxes'
import { Multilang } from "apprise-frontend-core/intl/multilang"
import { cloneElement, PropsWithChildren } from 'react'
import './explainer.scss'
import { PathwayRenderProps } from './model'


export const PathwayExplainer = (props: PathwayRenderProps & PropsWithChildren<{

    defaultExplainer?: string | Multilang

    icon?: JSX.Element

}>) => {

    const t = useT()

    const { settings: { ll } } = useLayoutApis()

    const multilangOf = useTranslatedMultilang()

    const { pathway, defaultExplainer = t('integration.explainer_default'), icon, children } = props

    const mlexplainer = isMultilang(defaultExplainer) ? defaultExplainer : multilangOf(defaultExplainer)

    const explainer = { ...mlexplainer, ...pathway.explainer }

    return <div className='integration-explainer'>

        <ParametricText className='integration-dialog-intro' text={ll(explainer)} />

        {icon && <div className='integration-dialog-section'>

            {cloneElement(icon, { className: 'integration-dialog-icon' })}

        </div>}

        {children}

    </div>
}





