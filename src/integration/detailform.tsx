import { useT } from '#app/intl/api'
import { isMultilang, useTranslatedMultilang } from '#app/model/multilang'
import { PathwayConfig, PathwayDetailProps } from '#integration/model'
import { ParametricMultiTextBox } from '#layout/parameters/parametricboxes'
import { Fragment, PropsWithChildren, useContext } from 'react'
import { MultiPathwayContext } from './multipathway/context'


// standard arrangement for pathway details.
// currently just adds an explainer that defaults to a pathways-specific message.
export const DetailForm = <C extends PathwayConfig>(props: PathwayDetailProps<C> & PropsWithChildren<{}>) => {

    const t = useT()

    const multilangOf = useTranslatedMultilang()

    const composed = useContext(MultiPathwayContext) !== undefined

    const { defaultExplainer = 'integration.explainer_default', defaultTrigger = 'integration.trigger_default', children, pathway, onChange } = props

    const mlexplainer = isMultilang(defaultExplainer) ? defaultExplainer : multilangOf(defaultExplainer)
    const mltrigger = isMultilang(defaultTrigger) ? defaultTrigger : multilangOf(defaultTrigger)

    return <Fragment>

        {children}

        {composed || <Fragment>      
        
         <ParametricMultiTextBox rte
            id={`pathway-explainer`}
            defaultValue={mlexplainer}
            label={t('integration.explainer_lbl')}
            validation={{ msg: t('integration.explainer_msg') }}
            onChange={explainer => onChange({ ...pathway, explainer })}>
            {pathway.explainer}
        </ParametricMultiTextBox>

        <ParametricMultiTextBox
            id={`pathway-trigger`}
            defaultValue={mltrigger}
            label={t('integration.trigger_lbl')}
            validation={{ msg: t('integration.trigger_msg') }}
            onChange={trigger => onChange({ ...pathway, trigger })}>
            {pathway.trigger}
        </ParametricMultiTextBox>

        </Fragment>

        }


    </Fragment >
}