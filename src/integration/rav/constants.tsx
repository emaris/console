

import { Icon } from 'antd'
import { IoMdBoat } from "react-icons/io"

export const ravIntegrationIcon = <Icon component={IoMdBoat} />

export const ravIntegrationId = 'rav'
export const parameterPrefix = ravIntegrationId

export const otherPseudoGroup= 'other'

export const ravservice = '/ravint'
export const allgears ='allgears'
export const alltypes ='alltypes'
export const allranges ='allramges'
export const allmissing ='allmissing'
export const allmissingcv ='allmissingcv'
export const vcbygear ='countbygear'
export const vcbytype ='countbytype'
export const vcbylength ='countsbylength'
export const vcbyrange ='countsbyrange'
export const vcbymissing ='countsbymissing'
export const vcbymissingcv ='countsbymissingcv'
export const vcbycvprops ='countsbycvprops'
export const vcbyfvprops ='countsbyfvprops'

export const allCombined = 'ALL'
export const euCountries = ['ESP','FRA','ITA','DEU','LTU','NLD','PRT','BEL']