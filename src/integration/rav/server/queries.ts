import { getSql } from "#server.lib/db/sql"
import { dbutils } from '#server.lib/db/utils'
import { VcByCVPropsAttachment, VcByCVPropsQuery } from "../client/vcbycvprops/pathway"
import { VcByFVPropsAttachment, VcByFVPropsQuery } from "../client/vcbyfvprops/pathway"
import { VcByGearAttachment, VcByGearQuery } from '../client/vcbygear/pathway'
import { VcByLengthQuery } from '../client/vcbylength/pathway'
import { VcByMissingCVAttachment, VcByMissingCVQuery } from '../client/vcbymissingcv/pathway'
import { VcByMissingFVAttachment, VcByMissingFVQuery } from '../client/vcbymissingfv/pathway'
import { VcByRangeAttachment, VcByRangeQuery } from '../client/vcbyrange/pathway'
import { VcByTypeAttachment, VcByTypeQuery } from "../client/vcbytype/pathway"
import { allCombined, euCountries } from '../constants'
import { RavPathwayQuery } from "../model"


export const ravQueries = () => {

    const rav = getSql()
    const utils = dbutils()


    const self = {


        // all vessels with given flag that were current in the registry at a given cutoff date.
        // note 1: must use the historical table due to the cutoff in the past.
        // note 2: normalise missing data in old, historical entries.
        // note 3: must exclude vessels delisted at cutoff, not before (then reclaimed) or after.

        currentAtCutoff: (request: RavPathwayQuery) => {

            const query = rav`

                with current as (

                    select distinct on (iotc_no) *,                                                 -- first is most recent.
                
                        coalesce(nullif (vessel_type, ''), 'UNK') as vessel_type_coalesced,         -- normalises missing data
                        coalesce(nullif (operating_range, ''), 'UNK') as vessel_range_coalesced     -- normalises missing data

                    from rav.record_history

                    where coalesce(submission_timestamp, timestamp) < ${new Date(request.date)}                                       -- cutoff

                    order by iotc_no, coalesce(submission_timestamp, "timestamp") desc                                              -- order by timestamp for each vessel
                )
                
                select c.* 
                from current c                                                                                                   
                left join rav.record_history_delisted d on c.id = d.record_id

                where d.reason is null                                                              -- exlcude delisted
                                                                                                    -- EU expansion
                and ${request.flag === 'EU' ?

                    rav`flag_country in ${rav(euCountries)}`
                    :
                    rav`flag_country = ${request.flag}`}                 
            `

            return query
        }


        ,


        // all FVs with given flag that were in the record at a given date.
        fishingVesselsAtCutoff: (slice: RavPathwayQuery) => rav`
        
            with current as  (${self.currentAtCutoff(slice)})
            select * from current
            where type_record = 'FV'    
        `

        ,

        // all CVs with given flag that were in the record at a given date.
        carrierVesselsAtCutoff: (slice: RavPathwayQuery) => rav`
        
            with current as (${self.currentAtCutoff(slice)})
            select * from current
            where type_record = 'CV' 
         
        `

        ,

        carrierVesselsTrxAtCutoff: (slice: RavPathwayQuery) => rav`
            select distinct on (rh.iotc_no) trx.record_id, 
                trx.country_code as "trx_country_code", 
                trx.country as "trx_country", 
                trx.type as "trx_type", 
                trx.date_from as "trx_date_from", 
                trx.date_to as "trx_date_to" 
            from rav.record_history rh
            join rav.record_history_to_trx_authz trx on rh.id = trx.record_id
            where coalesce(rh.submission_timestamp, rh.timestamp) < ${new Date(slice.date)} 
            and ${slice.flag === 'EU' ?

                    rav`trx.country_code in ${rav(euCountries)}`
                    :
                    rav`trx.country_code = ${slice.flag}`}
            order by rh.iotc_no, coalesce(rh.submission_timestamp, rh."timestamp") desc
        `
        
        ,

        carrierVesselsTrxInCutoff: (slice: RavPathwayQuery) => rav`
            select distinct on (rh.iotc_no) trx.record_id, 
                trx.country_code as "trx_country_code", 
                trx.country as "trx_country", 
                trx.type as "trx_type", 
                trx.date_from as "trx_date_from", 
                trx.date_to as "trx_date_to",
                rh.flag_country as "flag_country"
            from rav.record_history rh
            join rav.record_history_to_trx_authz trx on rh.id = trx.record_id
            where extract(year from coalesce(rh.submission_timestamp, rh.timestamp)) = ${new Date(slice.date).getFullYear()} 
            and trx.country_code = ${slice.flag}`

        ,

        allVesselTypes: () => rav<{ vessel_type: string }[]>`
            select distinct(coalesce(nullif (vessel_type, ''), 'UNK')) as vessel_type
            from rav.record_history
        `
        ,


        allVesselRanges: () => rav<{ vessel_range: string }[]>`
            select distinct(coalesce(nullif (operating_range, ''), 'UNK')) as vessel_range
            from rav.record_history
        `

        ,

        allGears: () => rav<{ gear: string }[]>`
            select distinct(upper(gear_id)) as gear
            from rav.record_history_to_gear
        `

        ,


        allMissingPropertiesFV: () => rav<{ missing: string }[]>`
            select * from (values 
                ('noimo'),
                ('nocfr'),
                ('noircs'),
                ('noname'),
                ('noport'),
                ('noregno'),
                ('notype'),
                ('noloa'),
                ('nogt'),
                ('nocapacity'),
                ('noownername'),
                ('noowneraddress'),
                ('nooperatorname'),
                ('nooperatoraddress'),
                ('nobeneficialname'),
                ('nobeneficialaddress'),
                ('nocompanyname'),
                ('nocompanyaddress'),
                ('nocompanyregno'),
                ('nogears'),
                ('noauthorizedfrom'),
                ('noauthorizedto'),
                ('nophotostarboard'), 
                ('nophotoportside'),
                ('nophotobow')
            ) as t(missing)
        `

        ,

        allMissingPropertiesCV: () => rav<{ missing: string }[]>`
            select * from (values 
                ('noimo'),
                ('noircs'),
                ('noname'),
                ('noport'),
                ('noregno'),
                ('notype'),
                ('noloa'),
                ('nogt'),
                ('nocapacity'),
                ('noownername'),
                ('noowneraddress'),
                ('nooperatorname'),
                ('nooperatoraddress'),
                ('noauthorizedfrom'),
                ('noauthorizedto'),
                ('nophotostarboard'),
                ('nophotoportside'),
                ('nophotobow'),
                ('notrxtype'),
                ('notrxdatefrom'),
                ('notrxdateto')
            )as t(missing)
        `

        ,

        allPropertiesCV: () => rav<{ missing: string }[]>`
            select * from (values 
                ('noimo'),
                ('noircs'),
                ('noname'),
                ('noport'),
                ('noregno'),
                ('notype'),
                ('noloa'),
                ('nogt'),
                ('nocapacity'),
                ('noownername'),
                ('noowneraddress'),
                ('nooperatorname'),
                ('nooperatoraddress'),
                ('noauthorizedfrom'),
                ('noauthorizedto'),
                ('nophotostarboard'),
                ('nophotoportside'),
                ('nophotobow'),
                ('notrxtype'),
                ('notrxdatefrom'),
                ('notrxdateto'),
                ('')
            )as t(missing)
        `

        ,

        // current FVs at cutoff, optionally filtered by LOA category, and counted by type.
        countByType: async (request: VcByTypeQuery) => {

            const lenghtconds = request.lengths?.map(l => l === 'under24' ? rav`loa < 24` : rav`loa >= 24`)

            const whereclause = lenghtconds ? rav`where ${utils.join(lenghtconds, rav` or `)}` : rav``

            const query = rav<VcByTypeAttachment>`  

                with current as (${self.fishingVesselsAtCutoff(request)}),
                filtered_current as (select * from current ${whereclause}),             -- filter goes before the group to include all types.
                all_types as (${self.allVesselTypes()})
                
                select vt.vessel_type, count(case when vessel_type_coalesced is not null then 1 else null end)::int as count        -- cast required for JS conversion.
                from all_types vt
                left join filtered_current on vessel_type_coalesced = vt.vessel_type
                group by vt.vessel_type 
           `

            // console.log(await rav`${query}`.describe())

            return query
        }

        ,

        // current at cutoff, optionally filtered by LOA category, and counted by range.
        countByRange: async (request: VcByRangeQuery) => {

            const lenghtconds = request.lengths?.map(l => l === 'under24' ? rav`loa < 24` : rav`loa >= 24`)

            const whereclause = lenghtconds ? rav`where ${utils.join(lenghtconds, rav` or `)}` : rav``

            const query = rav<VcByRangeAttachment>`
                
                with current as (${self.currentAtCutoff(request)}), 
                filtered_current as (select * from current ${whereclause}),
                vessel_ranges as (${self.allVesselRanges()})           

                select vr.vessel_range, coalesce(count(case when vessel_range_coalesced is not null then 1 else null end),0)::int as count
                from vessel_ranges vr 
                left join filtered_current c on c.vessel_range_coalesced = vr.vessel_range
                group by vr.vessel_range
            `

            //console.log(await rav`${query}`.describe())

            return query

        }

        ,

        // current FVs at cutoff, optionally filtered by length, and counted by gear.
        countByGears: async (slice: VcByGearQuery) => {

            const gears = utils.join(slice.gears?.map(g => rav`${g}`), rav`,`)

            const query = rav<VcByGearAttachment>`

                with current_with_gears as (                                                 -- augments records with gears    
                    with current as (${self.fishingVesselsAtCutoff(slice)})
                    select record_id, array_agg(gear_id) as gears
                    from rav.record_history_to_gear join current on id = record_id
                    group by record_id

                ),
                all_gears as (${self.allGears()})

                select gear, count(case when gears is not null then 1 else null end)::int as count
                from all_gears 
                left join current_with_gears on gear = ANY(gears)
                group by gear
                
                ${slice.gears?.length ? rav`  
                    union 
                    select ${allCombined}, count(*)
                    from current_with_gears
                    where ARRAY[${gears}] <@ gears` : rav``}  
            `

            //console.log(await rav`${query}`.describe())

            return query
        }

        ,

        // current at cutoff, counted by LOA category.

        countByLength: async (slice: VcByLengthQuery) => {


            const under24Condition = slice.under24?.ranges?.length ? rav`and vessel_range_coalesced in ${rav(slice.under24.ranges)}` : rav``
            const over24Condition = slice.over24?.ranges?.length ? rav`and vessel_range_coalesced in ${rav(slice.over24.ranges)}` : rav``

            const query = rav`
            
                with vessels as (${self.fishingVesselsAtCutoff(slice)}) 
                select count(*)::int from vessels where loa >= 24 ${over24Condition}
                union all 
                select count(*)::int from vessels where loa < 24 ${under24Condition}

            `

            // console.log(await rav`${query}`.describe())

            return query
        }

        ,

        countMissingFV: async (request: VcByMissingFVQuery): Promise<VcByMissingFVAttachment> => {

            const lenghtconds = request.lengths?.map(l => l === 'under24' ? rav`a.loa < 24` : rav`a.loa >= 24`)

            const whereclause = lenghtconds ? rav`where ${utils.join(lenghtconds, rav` or `)}` : rav``

            const query = rav<{ missing: string, count: number }[]>`

                        with current as (${self.fishingVesselsAtCutoff(request)}),
                        dist_rtg as (select record_id as rtg_rec_id, count(*) as rtg_count from rav.record_history_to_gear group by (record_id)),
                        filtered_current as (select * from current a left join dist_rtg b on b.rtg_rec_id = a.id ${whereclause})

                        select 'noimo' as missing , count(*)::int from filtered_current where nullif (imo, '') is null
                        union
                        select 'nocfr' , count(*)::int from filtered_current where nullif (iotc_no, '') is null
                        union
                        select 'noircs', count(*)::int from filtered_current where nullif (ircs, '') is null
                        union
                        select 'noname', count(*)::int from filtered_current where nullif (vessel_name, '') is null
                        union
                        select 'noport', count(*)::int from filtered_current where nullif (port_of_registration, '') is null
                        union
                        select 'noregno', count(*)::int from filtered_current where nullif (registration_no, '') is null
                        union
                        select 'notype', count(*)::int from filtered_current where nullif (vessel_type, '') is null
                        union
                        select 'noloa', count(*)::int from filtered_current where loa is null
                        union
                        select 'nogt', count(*)::int from filtered_current where gt is null
                        union
                        select 'nocapacity', count(*)::int from filtered_current where ccm3 is null
                        union
                        select 'noownername', count(*)::int from filtered_current where nullif (owner_name, '') is null
                        union
                        select 'noowneraddress', count(*)::int from filtered_current where nullif (owner_address, '') is null
                        union
                        select 'nooperatorname', count(*)::int from filtered_current where nullif (operator_name, '') is null
                        union
                        select 'nooperatoraddress', count(*)::int from filtered_current where nullif (operator_address, '') is null
                        union
                        select 'nobeneficialname', count(*)::int from filtered_current where nullif (beneficiary_owner_name, '') is null
                        union
                        select 'nobeneficialaddress', count(*)::int from filtered_current where nullif (beneficiary_owner_address, '') is null
                        union
                        select 'nocompanyname', count(*)::int from filtered_current where nullif (operating_company_name, '') is null
                        union
                        select 'nocompanyaddress', count(*)::int from filtered_current where nullif (operating_company_address, '') is null
                        union
                        select 'nocompanyregno', count(*)::int from filtered_current where nullif (operating_company_regno, '') is null
                        union
                        select 'nogears', count(*)::int from filtered_current where rtg_count is null
                        union
                        select 'noauthorizedfrom', count(*)::int from filtered_current where authorized_from is null
                        union
                        select 'noauthorizedto', count(*)::int from filtered_current where authorized_to is null
                        union
                        select 'nophotostarboard', count(*)::int from filtered_current where nullif (photo_starboard, '') is null
                        union
                        select 'nophotoportside', count(*)::int from filtered_current where nullif (photo_portside, '') is null
                        union
                        select 'nophotobow', count(*)::int from filtered_current where nullif (photo_bow, '') is null
                `

            // console.log(await rav`${query}`.describe())

            return query
        }

        ,

        countMissingTrx: (request: VcByMissingCVQuery) => rav`
            with current as (${self.carrierVesselsTrxAtCutoff(request)})
            select 'notrxtype', count(*)::int from current where nullif (trx_type, '') is null
            union
            select 'notrxdatefrom', count(*)::int from current where trx_date_from is null
            union
            select 'notrxdateto', count(*)::int from current where trx_date_to is null
        `

        ,

        countMissingCV: async (request: VcByMissingCVQuery): Promise<VcByMissingCVAttachment> => {

            const lenghtconds = request.lengths?.map(l => l === 'under24' ? rav`loa < 24` : rav`loa >= 24`)

            const whereclause = lenghtconds ? rav`where ${utils.join(lenghtconds, rav` or `)}` : rav``

            const query = rav<{ missing: string, count: number }[]>`

                        with current as (${self.carrierVesselsAtCutoff(request)}),
                        filtered_current as (select * from current ${whereclause})

                        select 'noimo' as missing , count(*)::int from filtered_current where nullif (imo, '') is null
                        union
                        select 'noircs', count(*)::int from filtered_current where nullif (ircs, '') is null
                        union
                        select 'noname', count(*)::int from filtered_current where nullif (vessel_name, '') is null
                        union
                        select 'noport', count(*)::int from filtered_current where nullif (port_of_registration, '') is null
                        union
                        select 'noregno', count(*)::int from filtered_current where nullif (registration_no, '') is null
                        union
                        select 'notype', count(*)::int from filtered_current where nullif (vessel_type, '') is null
                        union
                        select 'noloa', count(*)::int from filtered_current where loa is null
                        union
                        select 'nogt', count(*)::int from filtered_current where gt is null
                        union
                        select 'nocapacity', count(*)::int from filtered_current where ccm3 is null
                        union
                        select 'noownername', count(*)::int from filtered_current where nullif (owner_name, '') is null
                        union
                        select 'noowneraddress', count(*)::int from filtered_current where nullif (owner_address, '') is null
                        union
                        select 'nooperatorname', count(*)::int from filtered_current where nullif (operator_name, '') is null
                        union
                        select 'nooperatoraddress', count(*)::int from filtered_current where nullif (operator_address, '') is null
                        union
                        select 'noauthorizedfrom', count(*)::int from filtered_current where authorized_from is null
                        union
                        select 'noauthorizedto', count(*)::int from filtered_current where authorized_to is null
                        union
                        select 'nophotostarboard', count(*)::int from filtered_current where nullif (photo_starboard, '') is null
                        union
                        select 'nophotoportside', count(*)::int from filtered_current where nullif (photo_portside, '') is null
                        union
                        select 'nophotobow', count(*)::int from filtered_current where nullif (photo_bow, '') is null
                        union
                        (${self.countMissingTrx(request)})
                `

            //console.log(await rav`${query}`.describe())

            return query

        }

        ,

        countCVProps: async (request: VcByCVPropsQuery): Promise<VcByCVPropsAttachment> => {

            const missingQuery = (await self.countMissingCV(request)).map(rec => ({ property: rec.missing, count: rec.count }))

            const query = rav<{ property: string, count: number }[]>`
                with current as (${self.carrierVesselsTrxInCutoff(request)})
                select 'trxAuthToSameFlagCountOther' as property , count(*)::int from current where flag_country = ${request.flag}
                union
                select 'trxAuthToOtherFlagCount', count(*)::int from current where flag_country != ${request.flag}
            `
            return [...missingQuery, ...await query]
            
        }

        ,

        countFVProps: async (request: VcByFVPropsQuery): Promise<VcByFVPropsAttachment> => {

            const missingQuery = (await self.countMissingFV(request)).map(rec => ({ property: rec.missing, count: rec.count }))
            
            return [...missingQuery]
            
        }
    }

    return self
}