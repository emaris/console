
import { initServer } from '#server.lib/api/init'
import { initApp } from '#server.lib/core/app'
import { initDatabase } from '#server.lib/db/init'
import { initApi } from './api'
// import { getSql } from '#server.lib/db/sql'


// runs initialisation code.
const service = await initApp(async () => {

  const server = initServer({
    name: 'rav integration'
  })

  await initDatabase({

    host: process.env.RAVDB_HOST,
    database: process.env.RAVDB_NAME,
    port: parseInt(process.env.RAVDB_PORT || '5432'),
    user: process.env.RAVDB_USER,
    password: process.env.RAVDB_PWD
  })

  initApi()

  return server.start()


})

// export used at dev time, so that module can be awaited on and reloaded in dev server.
export default service