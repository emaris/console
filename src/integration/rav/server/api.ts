import { getServer } from "#server.lib/api/server";
import { VcByCVPropsAttachment } from "../client/vcbycvprops/pathway";
import { VcByFVPropsAttachment } from "../client/vcbyfvprops/pathway";
import { VcByGearAttachment, VcByGearQuery } from '../client/vcbygear/pathway';
import { VcByLengthAttachment, VcByLengthQuery } from '../client/vcbylength/pathway';
import { VcByMissingCVAttachment } from '../client/vcbymissingcv/pathway';
import { VcByMissingFVAttachment } from '../client/vcbymissingfv/pathway';
import { VcByRangeAttachment, VcByRangeQuery } from '../client/vcbyrange/pathway';
import { VcByTypeAttachment, VcByTypeQuery } from '../client/vcbytype/pathway';
import { allgears, allmissing, allmissingcv, allranges, alltypes, vcbycvprops, vcbyfvprops, vcbygear, vcbylength, vcbymissing, vcbymissingcv, vcbyrange, vcbytype } from '../constants';
import { RavPathwayQuery } from "../model";
import { ravQueries } from "./queries";





export const initApi = () => {

    const server = getServer()

    const queries = ravQueries()


    server.get<{}>(`/${alltypes}`, () => queries.allVesselTypes().values().then(v =>v.flat()))

    server.get<{}>(`/${allgears}`, () => queries.allGears().values().then(v =>v.flat()))

    server.get<{}>(`/${allranges}`, () => queries.allVesselRanges().values().then(v =>v.flat()))

    server.get<{}>(`/${allmissing}`, () => queries.allMissingPropertiesFV().values().then(v =>v.flat()))

    server.get<{}>(`/${allmissingcv}`, () => queries.allMissingPropertiesCV().values().then(v =>v.flat()))

    server.post<{Body: VcByTypeQuery}>(`/${vcbytype}`, (request) : Promise<VcByTypeAttachment> =>  queries.countByType(request.body))

    server.post<{Body: VcByRangeQuery}>(`/${vcbyrange}`, (request) : Promise<VcByRangeAttachment> => queries.countByRange(request.body))

    server.post<{Body: VcByGearQuery}>(`/${vcbygear}`, (request) : Promise<VcByGearAttachment>  => queries.countByGears(request.body))

    server.post<{Body: RavPathwayQuery}>(`/${vcbymissing}`, (request) : Promise<VcByMissingFVAttachment>  => queries.countMissingFV(request.body))

    server.post<{Body: RavPathwayQuery}>(`/${vcbymissingcv}`, (request) : Promise<VcByMissingCVAttachment>   => queries.countMissingCV(request.body))

    server.post<{Body: RavPathwayQuery}>(`/${vcbycvprops}`, (request) : Promise<VcByCVPropsAttachment>   => queries.countCVProps(request.body))

    server.post<{Body: RavPathwayQuery}>(`/${vcbyfvprops}`, (request) : Promise<VcByFVPropsAttachment>   => queries.countFVProps(request.body))

    server.post<{Body: VcByLengthQuery}>(`/${vcbylength}`, async (request) : Promise<VcByLengthAttachment> => {

        const queryRes = await queries.countByLength(request.body)

        return {
            over_24_meters_count: queryRes[0].count,
            under_24_meters_count: queryRes[1].count
        }

    })





}