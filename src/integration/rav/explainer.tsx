import { useT } from '#app/intl/api'
import { PathwayExplainer } from '#integration/explainer'
import { PathwayRenderProps } from '#integration/model'
import { Multilang } from 'apprise-frontend-core/intl/multilang'
import { ravIntegrationIcon } from './constants'


export const RavExplainer = (props: PathwayRenderProps<any, any> & {

    defaultExplainer?: string |  Multilang

}) => {

    const t = useT()
    
    return <PathwayExplainer icon={ravIntegrationIcon} {...props}>
        <div style={{  margin: '0px 20px'}}>{t('integration.rav.explainer_results')}</div>
    </PathwayExplainer>

}