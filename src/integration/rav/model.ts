import { PathwayConfig } from '#integration/model'

export type RavPathwayConfig = PathwayConfig & DateFilter

export type RavPathwayQuery = DateFilter & FlagFilter


export type FlagFilter = {

    flag: string
}


export type DateFilter = {

    date: string
}

export type RangeFilter = {
    ranges: string[]
}

export type GearFilter = {
    gears: string[]
}



export type LengthFilter = {

    lengths: ('under24' | 'over24')[]
}