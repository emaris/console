import moment from 'moment-timezone'
import { PathwayRenderProps } from "../../../model"
import { useParameterFallbacks, withPathwayParameterSpec } from '../../../parameter'
import { parameterPrefix } from '../../constants'
import { Config, Data } from './pathway'



export const useParameterSpec = () =>  {

   
    const fb = useParameterFallbacks()

   
    return (props: PathwayRenderProps<Config, Data>) => {
        
        const { data, pathway } = props

        const alias = moment(pathway.date).year().toString()

        const NA = data.attachment ?  fb.NA : fb.TBC
        
        return withPathwayParameterSpec({

        references: () => [
            { id: `vesselCount`, value: `${parameterPrefix}.${alias}.vesselCount` },
            { id: `year`, value: `${parameterPrefix}.${alias}.year` }

        ]

        ,

        textValue: (_, path) => {

            switch (path[0]) {
                case "vesselCount": return `${data.attachment?.flag ?? NA}`
                case "year": return alias

            }

            return fb.NA

        }


    })

}}


