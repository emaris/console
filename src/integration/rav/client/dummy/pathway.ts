import { FlagFilter, RavPathwayConfig, RavPathwayQuery } from '#integration/rav/model'
import { PathwayJsonViewer } from '#integration/jsonviewer'
import { Pathway, PathwayData } from "../../../model"
import { Detail } from './detail'
import { useParameterSpec } from './parameter'
import { Render } from './render'
 

export type Config = RavPathwayConfig

export type DummyQuery = RavPathwayQuery & FlagFilter

export type DummyAttachment = {

    flag: string
    count: number

}


export interface Data extends PathwayData {

    flag: string

    attachment: DummyAttachment

}


export const useDummyPathway = (): Pathway<RavPathwayConfig, Data> => {

    return {

        id: 'dummy',
        name: 'Test Pathway',

        Detail: Detail,
        Render: Render,
        Viewer: PathwayJsonViewer,

        useParameterSpec
    }

}


