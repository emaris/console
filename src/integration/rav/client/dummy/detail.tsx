import { DetailForm } from '../../../detailform'
import { PathwayDetailProps } from "../../../model"
import { CutoffDateBox } from "../utils"
import { Config } from './pathway'




export const Detail = (props: PathwayDetailProps<Config>) => {

    return <DetailForm defaultTrigger='integration.rav.trigger_default_lbl'  {...props}>

        <CutoffDateBox {...props} />

    </DetailForm>

}
