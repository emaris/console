import { useCalls } from '#app/call/call'
import { SelectBox } from '#app/form/SelectBox'
import { TenantLabel } from '#app/tenant/Label'
import { useTenantStore } from '#app/tenant/store'
import { showError } from '#app/utils/feedback'
import { RavExplainer } from '#integration/rav/explainer'
import { useState } from 'react'
import { PathwayRenderProps } from "../../../model"
import { RenderForm } from '../../../renderform'
import { ravservice, vcbylength } from '../../constants'
import { useDefaultConfig } from '../utils'
import { Config, Data, DummyAttachment, DummyQuery } from './pathway'





export const Render = (props: PathwayRenderProps<Config, Data>) => {

    const calls = useCalls()

    const {defaultDate} = useDefaultConfig()

    const tenants = useTenantStore().allSorted()

    const { pathway, data, onChangeData } = props

    const [edited, setEdited] = useState(data)

    const refresh = async () => {

        const request = { date: pathway.date ?? defaultDate , flag: edited.flag.substring(2) }

        try {

            const attachment =  await calls.atPath(`${ravservice}/${vcbylength}`).post<DummyAttachment, DummyQuery>(request)       
    
            onChangeData({ ...edited, attachment })
    
        }
        catch(e) {

            showError(e as Error,"my error instead of the generic one")
        }
        
    }

    const disabled = !edited?.flag

    const explainer = <RavExplainer {...props} defaultExplainer='Fetches the count of all vessels that...' />

    return <RenderForm disabled={disabled} onRefresh={refresh} explainer={explainer} {...props}  >

        <SelectBox label={'Some Dynamic Param'} style={{ width: 300 }}
            getkey={t => t.id}
            getlbl={t => <TenantLabel tenant={t} noLink />}
            onChange={(flag: string) => setEdited({ ...edited, flag })}
            selectedKey={edited.flag}>
            {tenants}
        </SelectBox>

    </RenderForm>
}
