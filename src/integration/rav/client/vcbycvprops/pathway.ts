import { Pathway, PathwayData } from '#integration/model'
import { LengthFilter, RavPathwayConfig, RavPathwayQuery } from '#integration/rav/model'
import { vcbycvprops } from '../../constants'
import { RavViewer } from '../utils'
import { Detail } from './detail'
import { useParameterSpec } from './parameter'
import { Render } from './render'



export type Config = RavPathwayConfig & Partial<LengthFilter>

export type VcByCVPropsQuery = RavPathwayQuery & Partial<LengthFilter>

export type VcByCVPropsAttachment = {

    property: string
    count: number

}[]


export interface Data extends PathwayData {

    attachment: VcByCVPropsAttachment

}




export const useVcByCVPropsPathway = (): Pathway<Config, Data> => {

    return {

        id: vcbycvprops,
        name: 'integration.rav.vcbycvprops.name',

        Detail,
        Render,
        Viewer: RavViewer,

        useParameterSpec


    }

}

