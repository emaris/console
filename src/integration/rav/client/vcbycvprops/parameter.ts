import { PathwayRenderProps } from '#integration/model'
import { useParameterFallbacks, withPathwayParameterSpec } from '../../../parameter'
import { parameterPrefix } from '../../constants'
import { useMissingProperties } from '../cache/missingcvprops'
import { Config, Data } from './pathway'



export const useParameterSpec = () => {


    const fb = useParameterFallbacks()

    const allmissing = useMissingProperties()

    return (props: PathwayRenderProps<Config, Data>) => {

        const { data } = props

       // const alias = moment(pathway.date).year().toString()

        const NA = data.attachment ? fb.NA : fb.TBC

        return withPathwayParameterSpec({

            references: () => {

                const fixedReferences = [
                    { id: `trxAuthToSameFlagCountOther`, value: `${parameterPrefix}.trxAuthToSameFlagCountOther` },
                    { id: `trxAuthToOtherFlagCount`, value: `${parameterPrefix}.trxAuthToOtherFlagCount` }
                ]

                const dynamicReferences = allmissing?.map(entry => ({ id: entry, value: `${parameterPrefix}.${entry}.count` })) ?? []

                return [...dynamicReferences, ...fixedReferences]

            }

            ,

            textValue: (_, path) => {


                const resolved = data.attachment?.find(e => e.property === path[0] || e['missing'] === path[0]) // missing is a retrocompatible fall-back

                    
                return resolved?.count === undefined ? NA : `${resolved.count}`

            }

        })

    }

}
