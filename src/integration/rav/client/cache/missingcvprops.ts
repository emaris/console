import { useCalls } from '#app/call/call'
import { allmissingcv, ravservice } from '#integration/rav/constants'
import { useEffect } from 'react'


let missingprops: string[] = undefined!




export const useMissingProperties = () => {

    const calls = useCalls()


    const fetch = async () => {


        if (missingprops)
            return

        // avoids race conditions that occur when this is mounted multiple times before the first call returns.
        missingprops = []

        console.log('fetching RAV CV missing props...')

        const ranges = await calls.atPath(`${ravservice}/${allmissingcv}`).get<string[]>()

        missingprops = ranges

    }


    useEffect(() => {

        fetch()

        // eslint-disable-next-line
    }, [])


    return missingprops


}