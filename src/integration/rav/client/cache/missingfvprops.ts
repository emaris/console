import { useCalls } from '#app/call/call'
import { allmissing, ravservice } from '#integration/rav/constants'
import { useEffect } from 'react'


let missingprops: string[] = undefined!




export const useMissingProperties = () => {

    const calls = useCalls()

    //const [, render] = useState(0)

    const fetch = async () => {

        if (missingprops)
            return

        // this intermediate step can be used to avoid race conditions
        // that would otherwise occur if this component was mounted multiple times before the first call returns.
        missingprops = []

        console.log('fetching RAV FV missing props...')

        const ranges = await calls.atPath(`${ravservice}/${allmissing}`).get<string[]>()

        //render(o => o++)

        missingprops = ranges
    }


    useEffect(() => {

        fetch()



        // eslint-disable-next-line
    }, [])


    return missingprops


}