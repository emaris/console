import { useCalls } from '#app/call/call'
import { useToggleBusy } from '#app/system/api'
import { allgears, ravIntegrationId, ravservice } from '#integration/rav/constants'
import { useEffect, useState } from 'react'


let vesselGears: string[] = undefined!




export const useVesselGears = () => {

    const toggleBusy = useToggleBusy()

    const calls = useCalls()

    const [, render] = useState(0)

    const fetch = async () => {

        if (vesselGears)
            return
            
          // avoids race conditions that occur when this is mounted multiple times before the first call returns.
        vesselGears=[]

        console.log('fetching RAV vessel gears...')

    
        try {

            toggleBusy(ravIntegrationId)


            const gears = await calls.atPath(`${ravservice}/${allgears}`).get<string[]>()

            render(o => o++)

            vesselGears=gears

        }
        finally {

            toggleBusy(ravIntegrationId)
        }

    }


    useEffect(() => {

        fetch()


        // eslint-disable-next-line
    }, [])


    return vesselGears


}