import { useCalls } from '#app/call/call'
import { useToggleBusy } from '#app/system/api'
import { alltypes, ravIntegrationId, ravservice } from '#integration/rav/constants'
import { useEffect, useState } from 'react'


let vesselTypes: string[] = undefined!




export const useVesselTypes = () => {

    const toggleBusy = useToggleBusy()

    const calls = useCalls()

    const [, render] = useState(0)

    const fetch = async () => {

        if (vesselTypes)
            return
            
          // avoids race conditions that occur when this is mounted multiple times before the first call returns.
        vesselTypes=[]

        console.log('fetching RAV vessel types...')

    
        try {

            toggleBusy(ravIntegrationId)


            const types = await calls.atPath(`${ravservice}/${alltypes}`).get<string[]>()

            render(o => o++)

            vesselTypes=types

        }
        finally {

            toggleBusy(ravIntegrationId)
        }

    }


    useEffect(() => {

        fetch()


        // eslint-disable-next-line
    }, [])


    return vesselTypes


}