import { useCalls } from '#app/call/call'
import { allranges, ravservice } from '#integration/rav/constants'
import { useEffect } from 'react'


let vesselRanges: string[] = undefined!




export const useVesselRanges = () => {

    const calls = useCalls()

    const fetch = async () => {


        if (vesselRanges)
            return

        // avoids race conditions that occur when this is mounted multiple times before the first call returns.
        vesselRanges = []

        console.log('fetching RAV vessel ranges...')

        const ranges = await calls.atPath(`${ravservice}/${allranges}`).get<string[]>()

        vesselRanges = ranges

    }


    useEffect(() => {

        fetch()

        // eslint-disable-next-line
    }, [])


    return vesselRanges


}