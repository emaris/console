import { Button } from "#app/components/Button"
import { useRoutableDrawer } from "#app/components/Drawer"
import { SelectBox } from "#app/form/SelectBox"
import { useT } from "#app/intl/api"
import { submissionType } from '#campaign/submission/constants'
import { IntegrationSection, noPathway, Pathway, PathwayData, PathwayRenderProps } from "#integration/model"
import { usePathway, useRavPathways } from "#integration/rav/client/pathways"
import { useLayoutApis } from "#layout/apicontext"
import { useLayout } from "#layout/context"
import { isInstance } from "#layout/model"
import { alignmentStyleId } from "#layout/style/alignment"
import { backgroundStyleId } from "#layout/style/background"
import { borderStyleId } from "#layout/style/border"
import { heightStyleId } from "#layout/style/height"
import { spacingStyleId } from "#layout/style/spacing"
import { StyledBox } from "#layout/style/styledbox"
import { widthStyleId } from "#layout/style/width"
import { Fragment, useContext, useState } from "react"
import { useComponentData } from "../../../layout/components/api"
import { DetailProps } from "../../../layout/components/detail"
import { ComponentDraggable, ComponentDroppable } from "../../../layout/components/dnd"
import { useSpecOf } from "../../../layout/components/helper"
import { ComponentSpec, newComponentFrom, PreviewProps, PrintProps, RenderProps } from "../../../layout/components/model"
import { ComponentPreview } from "../../../layout/components/preview"

import { icns } from '#app/icons'
import { ReadOnlyContext } from '#app/scaffold/ReadOnly'
import { Tab } from '#app/scaffold/Tab'
import { Topbar } from '#app/scaffold/Topbar'
import { IntegrationParameterProvider } from '#integration/parameter'
import { ComponentCustomName } from '#layout/components/componenthelper'
import { ParametricText } from '#layout/parameters/parametricboxes'
import { ravIntegrationIcon } from '../constants'
import './section.scss'



export const ravSectionId = 'rav-section'


export const useRavSectionComponent = (): ComponentSpec<IntegrationSection> => {

    const name = 'integration.rav.component_name'

    const { pathwayParameterSpecs } = useRavPathways()

    return useSpecOf({


        id: ravSectionId,
        icon: ravIntegrationIcon,
        name,

        generate: (): IntegrationSection => ({

            ...newComponentFrom(ravSectionId),
            pathway: {} as Pathway,
            style: {},
            children: [],
        }),

        styles: () => ({

            [backgroundStyleId]: {},
            [widthStyleId]: {},
            [heightStyleId]: {},
            [borderStyleId]: {},
            [alignmentStyleId]: {},
            [spacingStyleId]: {}

        })

        ,

        submittedDataOf: (component: IntegrationSection, { data = {} }) => {

            const specs = pathwayParameterSpecs[component.pathway.id]

            const param = specs({ pathway: component.pathway, data, onChangeData: () => { } }).generate(component.id)

            //console.log({generated:param})

            return { parameters: [param] }
        }

        ,

        Detail,
        Render,
        Preview,
        Print,
        PrintPreview

    })


}


const Detail = (props: DetailProps<IntegrationSection>) => {

    const t = useT()

    const { component, onChange } = props

    const { pathway: config, ...rest } = component

    const { pathways } = useRavPathways()

    const pathway = pathways[config.id] ?? noPathway

    return <Fragment>

        <ComponentCustomName {...props} />

        <SelectBox label={t('integration.pathway_lbl')} validation={{ msg: t('integration.pathway_msg') }}

            selectedKey={pathway.id}
            onChange={(id: string) => onChange({ ...component, pathway: { id } })}
            getlbl={id => t(pathways[id].name)}>

            {Object.keys(pathways)}

        </SelectBox>

        <pathway.Detail defaultTrigger={t('integration.rav.trigger_default_lbl')} key={config.id} pathway={component.pathway as any} onChange={pathway => onChange({ ...rest, pathway })} />

    </Fragment>

}

function Preview(props: PreviewProps<IntegrationSection>) {

    const { path, component } = props

    const { components } = useLayoutApis()

    const nextPosition = component.children.length


    const pathway = usePathway(component)


    return <ComponentDroppable path={path}>
        <ComponentDraggable path={path} component={component}>
            <ComponentPreview path={path} component={component}>

                <RenderChildren key={pathway.id} {...props} >{

                    component.children.map((child, position) => {

                        const spec = components.componentSpecOf(child)

                        return <spec.Preview key={position} component={child} path={components.extend(path).at(position)} />

                    })

                }</RenderChildren>

                <ComponentDroppable key={nextPosition} className="component-placeholder" path={components.extend(path).at(nextPosition)} />
            </ComponentPreview>
        </ComponentDraggable>
    </ComponentDroppable>

}

const Render = (props: RenderProps<IntegrationSection>) => {

    const { component } = props

    const { components } = useLayoutApis()

    return <RenderChildren {...props}>

        {component.children.map((component) => {

            const spec = components.componentSpecOf(component)

            return <spec.Render key={component.id} component={component} />
        })}

    </RenderChildren>

}

const RenderChildren = (props: RenderProps<IntegrationSection>) => {

    const t = useT()

    const { settings: { ll } } = useLayoutApis()

    const { component, children } = props

    const layout = useLayout()

    const connect = useRoutableDrawer({ id: `${submissionType}-${component.id}` })

    const pathway = usePathway(component)

    const renderProps = usePathwayRenderProps(component)

    const specs = pathway.useParameterSpec()

    const param = specs(renderProps as any).generate(component.id)

    const trigger = ll(component.pathway.trigger) ?? t('integration.rav.trigger_default_lbl')

    const readonly = useContext(ReadOnlyContext)

    const [tab, tabSet] = useState('trigger')

    return <IntegrationParameterProvider key={pathway.id} param={param} >

        <connect.Drawer destroyOnClose title={t('integration.rav.drawer_title')} icon={ravIntegrationIcon} >

            <Topbar onTabChange={tabSet} activeTab={tab}>
                <Tab default id="trigger" icon={icns.form} name={t("integration.rav.query_tab")} />
                <Tab id="attachment" disabled={!renderProps.data.attachment} icon={ravIntegrationIcon} name={t("integration.rav.result_tab")} />
            </Topbar>


            {tab === 'attachment' ?

                <pathway.Viewer {...renderProps} />
                :
                <pathway.Render {...renderProps} />

            }



        </connect.Drawer>

        <StyledBox component={component} className='pathway-render'>

            {isInstance(layout) && !readonly ?

                <Fragment>


                    <div className='section-separator separator-start'>
                        <Button icn={ravIntegrationIcon} iconLeft className='section-trigger' size='small' onClick={connect.open}>
                            <ParametricText text={trigger} />
                        </Button>
                    </div>


                    {children}


                    <div className='section-separator separator-end'>
                        <div>
                            <span><ParametricText text={trigger} /></span> &nbsp; <span>{t('integration.trigger_section_end')}</span>
                        </div>
                    </div>


                </Fragment>


                :

                children

            }

        </StyledBox>

    </IntegrationParameterProvider>
}



const usePathwayRenderProps = (component: IntegrationSection): PathwayRenderProps => {

    const { dataFor, changeDataFor } = useComponentData()

    const data = dataFor(component) ?? {}

    return {

        pathway: component.pathway,

        data,

        onChangeData: (v: PathwayData) => {

            // we timestamp the change if the client hasn't already.
            const newtimestamp = v.timestamp && data.timestamp !== v.timestamp

            changeDataFor(component, { ...v, timestamp: newtimestamp ? v.timestamp : new Date().toISOString() })
        }
    }
}



const Print = (props: PrintProps<IntegrationSection>) => {

    const { component, wrap = true, View } = props

    const { components } = useLayoutApis()

    return <PrintChildren {...props}>

        {component.children.map((child, position) => {

            const specs = components.componentSpecOf(child)

            return <View wrap={wrap} key={position} style={{ display: 'flex', flexDirection: 'column', width: '100%' }}>

                <specs.Print {...props} key={position} component={child} />

            </View>

        })}

    </PrintChildren>



}

const PrintPreview = (props: PrintProps<IntegrationSection>) => {

    const { component, wrap = true, View } = props

    const { components } = useLayoutApis()

    return <PrintChildren {...props}>

        {component.children.map((child, position) => {

            const spec = components.componentSpecOf(child)

            return <View wrap={wrap} key={position} style={{ display: 'flex', flexDirection: 'column', width: '100%' }}>

                <spec.PrintPreview {...props} key={position} component={child} />

            </View>

        })}

    </PrintChildren>
}

const PrintChildren = (props: PrintProps<IntegrationSection>) => {

    const { component, View, children } = props

    const pathway = usePathway(component)

    const renderProps = usePathwayRenderProps(component)

    const specs = pathway.useParameterSpec()

    const param = specs(renderProps as any).generate(component.id)

    console.log({ param })

    return <IntegrationParameterProvider key={pathway.id} param={param} >

        <View style={{ marginBottom: 15 }}>

            {children}

        </View>

    </IntegrationParameterProvider>
}

