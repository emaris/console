import { useCalls } from '#app/call/call'
import { PathwayRenderProps } from '#integration/model'
import { RavExplainer } from '#integration/rav/explainer'
import { RenderForm } from '../../../renderform'
import { ravservice, vcbymissingcv } from '../../constants'
import { useCurrentFlag, useDefaultConfig } from '../utils'
import { Config, Data, VcByMissingCVAttachment, VcByMissingCVQuery } from './pathway'



export const Render = (props: PathwayRenderProps<Config, Data>) => {

    const calls = useCalls()

    const { defaultDate } = useDefaultConfig()

    const { pathway, data, onChangeData } = props

    const flag = useCurrentFlag()
    const date = pathway.date ?? defaultDate
    const lengths = pathway.lengths

    const requestValid = flag && date

    const refresh = async () => {


        const attachment = await calls.atPath(`${ravservice}/${vcbymissingcv}`).post<VcByMissingCVAttachment, VcByMissingCVQuery>({ date, flag, lengths })

        onChangeData({ ...data, attachment })

    }

    const explainer = <RavExplainer {...props} defaultExplainer='integration.rav.vcbymissingcv.default_explainer' />


    return <RenderForm disabled={!requestValid} onRefresh={refresh} explainer={explainer} {...props} />


}
