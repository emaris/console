import { Pathway, PathwayData } from '#integration/model'
import { LengthFilter, RavPathwayConfig, RavPathwayQuery } from '#integration/rav/model'
import { vcbymissingcv } from '../../constants'
import { RavViewer } from '../utils'
import { Detail } from './detail'
import { useParameterSpec } from './parameter'
import { Render } from './render'



export type Config = RavPathwayConfig & Partial<LengthFilter>

export type VcByMissingCVQuery = RavPathwayQuery & Partial<LengthFilter>

export type VcByMissingCVAttachment = {

    missing: string
    count: number

}[]


export interface Data extends PathwayData {

    attachment: VcByMissingCVAttachment

}




export const useVcByMissingCVPathway = (): Pathway<Config, Data> => {

    return {

        id: vcbymissingcv,
        name: 'integration.rav.vcbymissingcv.name',

        Detail,
        Render,
        Viewer: RavViewer,

        useParameterSpec


    }

}

