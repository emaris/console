import { SelectBox } from "#app/form/SelectBox"
import { useT } from "#app/intl/api"
import { PathwayDetailProps } from '#integration/model'

import { DetailForm } from '../../../detailform'
import { CutoffDateBox, VesselLengthBox } from "../utils"
import { Config } from './pathway'
import { useVesselTypes } from '../cache/vesseltypes'



export const Detail = (props: PathwayDetailProps<Config>) => {

    const t = useT()

    const { pathway, onChange } = props

    const types = useVesselTypes()

    return <DetailForm defaultExplainer='integration.rav.vcbytype.default_explainer' {...props}>

        <CutoffDateBox {...props} />

        <SelectBox multi label={t('integration.rav.types_lbl')} validation={{ msg: t('integration.rav.types_msg') }}

            selectedKey={pathway.types ?? []}
            onChange={(types: string[]) => onChange({ ...pathway, types })}>

            {types}

        </SelectBox>

        <VesselLengthBox {...props} />

    </DetailForm>

}
