import { Pathway, PathwayData } from '#integration/model'
import { LengthFilter, RavPathwayConfig, RavPathwayQuery } from '#integration/rav/model'
import { vcbytype } from '../../constants'
import { RavViewer } from '../utils'
import { Detail } from './detail'
import { useParameterSpec } from './parameter'
import { Render } from './render'


export type Config = RavPathwayConfig & Partial<LengthFilter> & {

    types: string[]
}


export type VcByTypeQuery = RavPathwayQuery & Partial<LengthFilter> 

export type VcByTypeAttachment = {

    vessel_type: string
    count: number
}[]


export interface Data extends PathwayData {

    attachment: VcByTypeAttachment

}


export const useVcByTypePathway = (): Pathway<Config, Data> => {

    return {

        id: vcbytype,
        name: 'integration.rav.vcbytype.name',

        Detail,
        Render,
        Viewer: RavViewer,

        useParameterSpec


    }

}
