import { PathwayRenderProps } from '#integration/model'
import { useParameterFallbacks, withPathwayParameterSpec } from '../../../parameter'
import { otherPseudoGroup, parameterPrefix } from '../../constants'
import { Config, Data } from './pathway'



export const useParameterSpec = () => {


    const fb = useParameterFallbacks()

    return (props: PathwayRenderProps<Config, Data>) => {

        const { data, pathway } = props

       // const alias = moment(pathway.date).year().toString()

        const NA = data.attachment ? fb.NA : fb.TBC

        return withPathwayParameterSpec({

            references: () => {

                const references = pathway.types?.map(entry => ({ id: entry, value: `${parameterPrefix}.${entry}.count` })) ?? []

                const other = { id: otherPseudoGroup, value: `${parameterPrefix}.OTHER.count` }

                references.push(other)

                return references

            }

            ,

            textValue: (_, path) => {

                const pseudoType = path[0] === otherPseudoGroup

                // this would happen if the type selection changed over time.
                if (!pseudoType && !pathway.types?.includes(path[0]))
                    return fb.ERR


                const resolved = pseudoType ?

                    // sum count across all non-selected types.
                    { count: data.attachment?.filter(e => !pathway.types?.includes(e.vessel_type)).reduce((acc, cur) => acc + cur.count, 0) }

                    :

                    data.attachment?.find(e => e.vessel_type === path[0])

                    
                return resolved?.count === undefined ? NA : `${resolved.count}`

            }

        })

    }

}
