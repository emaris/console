import { PathwayDetailProps } from '#integration/model'
import { DetailForm } from '../../../detailform'
import { CutoffDateBox, VesselLengthBox } from "../utils"
import { Config, useDefaultExplainer } from './pathway'



export const Detail = (props: PathwayDetailProps<Config>) => {

    const { pathway } = props

    return <DetailForm defaultExplainer={useDefaultExplainer(pathway)} {...props}>

        <CutoffDateBox {...props} />

        <VesselLengthBox {...props} />

    </DetailForm>

}
