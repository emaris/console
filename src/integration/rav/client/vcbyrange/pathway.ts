import { useT } from '#app/intl/api'
import { useTranslatedMultilang } from '#app/model/multilang'
import { Pathway, PathwayData } from '#integration/model'
import { LengthFilter, RavPathwayConfig, RavPathwayQuery } from '#integration/rav/model'
import { vcbyrange } from '../../constants'
import { RavViewer } from '../utils'
import { Detail } from './detail'
import { useParameterSpec } from './parameter'
import { Render } from './render'



export type Config = RavPathwayConfig & Partial<LengthFilter>

export type VcByRangeQuery = RavPathwayQuery & Partial<LengthFilter>

export type VcByRangeAttachment = {

    vessel_range: string
    count: number

}[]

export interface Data extends PathwayData {

    attachment: VcByRangeAttachment

}

export const useVcByRangePathway = (): Pathway<Config, Data> => {

    return {

        id: vcbyrange,
        name: 'integration.rav.vcbyrange.name',

        Detail,
        Render,
        Viewer: RavViewer,

        useParameterSpec


    }

}


export const useDefaultExplainer = (pathway: Config) => {

    const t = useT()

    const multilangWith = useTranslatedMultilang()
    
    return multilangWith(t('integration.rav.vcbyrange.default_explainer'))

}