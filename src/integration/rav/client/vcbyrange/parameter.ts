import { PathwayRenderProps } from '#integration/model'
import { useParameterFallbacks, withPathwayParameterSpec } from '../../../parameter'
import { parameterPrefix } from '../../constants'
import { Config, Data } from './pathway'
import { useVesselRanges } from '../cache/vesselranges'



export const useParameterSpec = () => {


    const fb = useParameterFallbacks()

    const allRanges = useVesselRanges()

    return (props: PathwayRenderProps<Config, Data>) => {

        const { data } = props

       // const alias = moment(pathway.date).year().toString()

        const NA = data .attachment ? fb.NA : fb.TBC

        return withPathwayParameterSpec({

            references: () => {

                const references = allRanges?.map(entry => ({ id: entry, value: `${parameterPrefix}.${entry}.count` })) ?? []

                return references

            }

            ,

            textValue: (_, path) => {


                const resolved = data.attachment?.find(e => e.vessel_range === path[0])

                    
                return resolved?.count === undefined ? NA : `${resolved.count}`

            }

        })

    }

}
