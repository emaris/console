import { SelectBox } from "#app/form/SelectBox"
import { useT } from "#app/intl/api"
import { PathwayDetailProps } from '#integration/model'

import { DetailForm } from '../../../detailform'
import { CutoffDateBox } from "../utils"
import { Config } from './pathway'
import { useVesselGears } from '../cache/vesselgears'



export const Detail = (props: PathwayDetailProps<Config>) => {

    const t = useT()

    const { pathway, onChange } = props

    const gears = useVesselGears()

    return <DetailForm defaultExplainer='integration.rav.vcbygear.default_explainer' {...props}>

        <CutoffDateBox {...props} />


        <SelectBox multi label={t('integration.rav.gears_lbl')} validation={{ msg: t('integration.rav.gears_msg') }}

            selectedKey={pathway.gears ?? []}
            onChange={(gears: string[]) => onChange({ ...pathway, gears })}>

            {(gears ?? []).sort()}

        </SelectBox>

    </DetailForm>

}
