import { PathwayRenderProps } from '#integration/model'
import { useParameterFallbacks, withPathwayParameterSpec } from '../../../parameter'
import { allCombined, parameterPrefix } from '../../constants'
import { Config, Data } from './pathway'



export const useParameterSpec = () => {


    const fb = useParameterFallbacks()

    return (props: PathwayRenderProps<Config, Data>) => {

        const { data, pathway } = props

       // const alias = moment(pathway.date).year().toString()

        const NA = data.attachment ? fb.NA : fb.TBC

        
        return withPathwayParameterSpec({

            references: () => {

                const references = pathway.gears?.map(entry => ({ id: entry, value: `${parameterPrefix}.${entry}.count` })) ?? []

                const combined = { id: allCombined, value: `${parameterPrefix}.${allCombined}.count` }

                references.push(combined)

                return references

            }

            ,

            textValue: (_, path) => {

                // this would happen if selection changed over time.
                if (path[0]!==allCombined && !pathway.gears?.includes(path[0]))
                    return fb.ERR

                const resolved = data.attachment?.find(e => e.gear === path[0])

                return resolved?.count === undefined ? NA : `${resolved.count}`

            }

        })

    }

}
