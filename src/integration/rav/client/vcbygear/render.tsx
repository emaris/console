import { useCalls } from '#app/call/call'
import { PathwayRenderProps } from '#integration/model'
import { RavExplainer } from '#integration/rav/explainer'
import { RenderForm } from '../../../renderform'
import { ravservice, vcbygear } from '../../constants'
import { GearFilter, RavPathwayQuery } from '../../model'
import { useCurrentFlag, useDefaultConfig } from '../utils'
import { Config, Data, VcByGearAttachment } from './pathway'



export const Render = (props: PathwayRenderProps<Config, Data>) => {

    const calls = useCalls()

    const {defaultDate} = useDefaultConfig()

    const { pathway, data, onChangeData } = props

    const flag = useCurrentFlag()
    const date = pathway.date ?? defaultDate
    const gears = pathway.gears ?? []
   
    const requestValid = flag && date
    
    const refresh = async () => {

            const attachment = await calls.atPath(`${ravservice}/${vcbygear}`).post<VcByGearAttachment, RavPathwayQuery & GearFilter>({date,flag, gears})

            onChangeData({ ...data, attachment })

    }


    const explainer = <RavExplainer {...props} defaultExplainer='integration.rav.vcbygear.default_explainer' />

    return <RenderForm disabled={!requestValid} onRefresh={refresh} explainer={explainer} {...props} />


}
