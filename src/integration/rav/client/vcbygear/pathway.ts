import { Pathway, PathwayData } from '#integration/model'
import { GearFilter, RangeFilter, RavPathwayConfig, RavPathwayQuery } from '#integration/rav/model'
import { vcbygear } from '../../constants'
import { RavViewer } from '../utils'
import { Detail } from './detail'
import { useParameterSpec } from './parameter'
import { Render } from './render'


export type Config = RavPathwayConfig & Partial<RangeFilter> & Partial<GearFilter>

export type VcByGearQuery = RavPathwayQuery & Partial<GearFilter>

export type VcByGearAttachment = {

    gear: string
    count: number

}[]

export interface Data extends PathwayData {

    attachment: VcByGearAttachment

}


export const useVcByGearPathway = (): Pathway<Config, Data> => {

    return {

        id: vcbygear,
        name: 'integration.rav.vcbygear.name',

        Detail,
        Render,
        Viewer: RavViewer,

        useParameterSpec

    }

}