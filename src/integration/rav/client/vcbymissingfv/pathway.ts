import { Pathway, PathwayData } from '#integration/model'
import { LengthFilter, RavPathwayConfig, RavPathwayQuery } from '#integration/rav/model'
import { vcbymissing } from '../../constants'
import { RavViewer } from '../utils'
import { Detail } from './detail'
import { useParameterSpec } from './parameter'
import { Render } from './render'


export type Config = RavPathwayConfig & Partial<LengthFilter>

export type VcByMissingFVQuery = RavPathwayQuery & Partial<LengthFilter>

export type VcByMissingFVAttachment = {

    missing: string
    count: number

}[]



export interface Data extends PathwayData {

    attachment: VcByMissingFVAttachment

}




export const useVcByMissingPathway = (): Pathway<Config, Data> => {

    return {

        id: vcbymissing,
        name: 'integration.rav.vcbymissingfv.name',

        Detail,
        Render,
        Viewer: RavViewer,

        useParameterSpec


    }

}