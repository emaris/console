import { PathwayDetailProps } from '#integration/model'
import { DetailForm } from '../../../detailform'
import { CutoffDateBox, VesselLengthBox } from "../utils"
import { Config } from './pathway'



export const Detail = (props: PathwayDetailProps<Config>) => {

 
    return <DetailForm defaultExplainer='integration.rav.vcbymissingfv.default_explainer' {...props}>

        <CutoffDateBox {...props} />

        <VesselLengthBox {...props} />

    </DetailForm>

}
