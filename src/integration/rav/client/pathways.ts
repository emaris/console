import { IntegrationSection, noPathway, Pathway, PathwayParameterFactory } from '#integration/model'
import { useVcByGearPathway } from './vcbygear/pathway'

import { useMultiPathway } from '#integration/multipathway/pathway'
import { ravIntegrationIcon } from '../constants'
import { useVcByCVPropsPathway } from './vcbycvprops/pathway'
import { useVcByFVPropsPathway } from './vcbyfvprops/pathway'
import { useVesselCountLenghtPathway } from "./vcbylength/pathway"
import { useVcByRangePathway } from './vcbyrange/pathway'
import { useVcByTypePathway } from './vcbytype/pathway'

export const useRavPathways = () => {

    const vcByLength = useVesselCountLenghtPathway()
    const vcByType = useVcByTypePathway()
    const vcByGear = useVcByGearPathway()
    const vcByRange = useVcByRangePathway()
    const vcByFVProps = useVcByFVPropsPathway()
    const vcByCVProps = useVcByCVPropsPathway()

    const result = {

        pathways: {
            [vcByType.id]: vcByType,
            [vcByGear.id]: vcByGear,
            [vcByLength.id]: vcByLength,
            [vcByRange.id]: vcByRange,
            [vcByFVProps.id]: vcByFVProps,
            [vcByCVProps.id]: vcByCVProps,
            // [vcByMissing.id]: vcByMissing,
            // [vcByMissingCV.id]: vcByMissingCV
            
        } as Record<string, Pathway<any, any>>                         // generic type

        ,

        pathwayParameterSpecs: {
            [vcByType.id]: vcByType.useParameterSpec(),
            [vcByGear.id]: vcByGear.useParameterSpec(),
            [vcByLength.id]: vcByLength.useParameterSpec(),
            [vcByRange.id]: vcByRange.useParameterSpec(),
            [vcByFVProps.id]: vcByFVProps.useParameterSpec(),
            [vcByCVProps.id]: vcByCVProps.useParameterSpec(),
            // [vcByMissing.id]: vcByMissing.useParameterSpec(),
            // [vcByMissingCV.id]: vcByMissingCV.useParameterSpec()

        } as Record<string, PathwayParameterFactory<any, any>>       // generic type

    }

    const multi = useMultiPathway({
    
        icon : ravIntegrationIcon,
        pathways : Object.values(result.pathways), 
        pathwayParameterSpecs: result.pathwayParameterSpecs
    
    } )

    result.pathways[multi.id] = multi
    result.pathwayParameterSpecs[multi.id] = multi.useParameterSpec()

    return result

}


export const usePathway = (component: IntegrationSection) => useRavPathways().pathways[component.pathway?.id] ?? noPathway