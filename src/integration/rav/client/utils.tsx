import { CheckGroupBox } from '#app/form/CheckBox'
import { useT } from '#app/intl/api'
import { useTenantStore } from '#app/tenant/store'
import { useTimePicker } from '#app/time/TimePickerBox'
import { dashboardPartyParam } from '#dashboard/constants'
import { AttachmentViewer } from '#integration/attachmentviewer'
import { PathwayConfig, PathwayDetailProps, PathwayRenderProps } from '#integration/model'
import { PathwayTreeViewer } from '#integration/treeviewer'
import { predefinedRequirementReportingYearParameterId } from '#layout/parameters/constants'
import { defaultDateParameterValue } from '#layout/parameters/dateyear'
import { useParameters } from '#layout/parameters/hooks'
import { newParameterFrom, Parameter } from "#layout/parameters/model"
import { CheckboxValueType } from 'antd/lib/checkbox/Group'
import moment from "moment-timezone"
import { CSSProperties } from 'react'
import { parameterPrefix } from '../constants'
import { DateFilter, LengthFilter } from '../model'



export const makeParameterFor = (id: string, alias: string, spec: string, value: any): Parameter => ({

    ...newParameterFrom(parameterPrefix),
    id,
    name: alias,
    spec,
    value

})

export const getMomentForYear = (year: number) => moment({
    year,
    month: 11,
    day: 31,
    hour: 23,
    minute: 59,
    second: 59
})


export const useDefaultConfig = () => {

    const { allParameters } = useParameters()

    const reportedForYearParam = allParameters().find(p => p.id === predefinedRequirementReportingYearParameterId)?.value as ReturnType<typeof defaultDateParameterValue>

    return {

        defaultDate: getMomentForYear(reportedForYearParam?.date ?? new Date().getFullYear()).toISOString()

    }


}

export const useCurrentFlag = () => {

    const { allParameters } = useParameters()

    const tenants = useTenantStore()

    const flag = allParameters().find(p => p.id === dashboardPartyParam)?.value

    const tenant = tenants.safeLookup(flag)

    return tenant.code
}



export const CutoffDateBox = <T extends PathwayConfig & DateFilter> (props: PathwayDetailProps<T>) => {

    const t = useT()

    const { pathway, onChange } = props

    const { Picker } = useTimePicker()

    return <Picker key={pathway?.date} allowClear
        label={t('integration.rav.year_lbl')} validation={{ msg: t('integration.rav.year_msg') }}
        style={{ width: '100%' }}
        showTime={true}
        format={'l'}
        value={pathway?.date ? moment(pathway.date) : undefined}
        onChange={v => onChange({ ...pathway, date: v ? v.toISOString() : undefined })} />


}


export const VesselLengthBox = <T extends PathwayConfig & Partial<LengthFilter>> (props: PathwayDetailProps<T>) => {

    const t = useT()

    const { pathway, onChange } = props

    const verticalStyle: CSSProperties = {display:'flex', flexDirection: 'column'}

    return <CheckGroupBox style={verticalStyle} label={t('integration.rav.length_lbl')} validation={{msg:t("integration.rav.length_msg")}}
            
            defaultValue={['under24','over24']}
            value={pathway.lengths} 
            onChange={(v: CheckboxValueType[]) => onChange({ ...pathway, lengths: v})}>
            
            {[
                { value: 'under24',label: t("integration.rav.under24_lbl")},
                {value: 'over24',label: t("integration.rav.over24_lbl") }
            ]}

    </CheckGroupBox>


}


export const RavViewer = (props: PathwayRenderProps<any,any>) => {

    return <AttachmentViewer {...props} contentType='application/json'>
            <PathwayTreeViewer {...props} />
    </AttachmentViewer>

}