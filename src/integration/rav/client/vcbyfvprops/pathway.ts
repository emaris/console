import { Pathway, PathwayData } from '#integration/model'
import { LengthFilter, RavPathwayConfig, RavPathwayQuery } from '#integration/rav/model'
import { vcbyfvprops } from '../../constants'
import { RavViewer } from '../utils'
import { Detail } from './detail'
import { useParameterSpec } from './parameter'
import { Render } from './render'


export type Config = RavPathwayConfig & Partial<LengthFilter>

export type VcByFVPropsQuery = RavPathwayQuery & Partial<LengthFilter>

export type VcByFVPropsAttachment = {

    property: string
    count: number

}[]



export interface Data extends PathwayData {

    attachment: VcByFVPropsAttachment

}




export const useVcByFVPropsPathway = (): Pathway<Config, Data> => {

    return {

        id: vcbyfvprops,
        name: 'integration.rav.vcbyfvprops.name',

        Detail,
        Render,
        Viewer: RavViewer,

        useParameterSpec


    }

}