import { Pathway, PathwayData } from '#integration/model'
import { RangeFilter, RavPathwayConfig, RavPathwayQuery } from '#integration/rav/model'
import { vcbylength } from '../../constants'
import { RavViewer } from '../utils'
import { Detail } from './detail'
import { useParameterSpec } from './parameter'
import { Render } from './render'


export type LenghtRanges = Partial<{
    
    under24: RangeFilter

}> & Partial<{
    
    over24: RangeFilter

}>

export type Config = RavPathwayConfig & LenghtRanges

export type VcByLengthQuery = RavPathwayQuery & LenghtRanges

export type VcByLengthAttachment = {

    over_24_meters_count: number
    under_24_meters_count: number

}

export interface Data extends PathwayData {

    attachment: VcByLengthAttachment

}


export const useVesselCountLenghtPathway = (): Pathway<Config,Data> => {

     return {

        id: vcbylength,
        name: 'integration.rav.vcbylength.name',

        Detail,
        Render,
        Viewer: RavViewer,

        useParameterSpec
    }

}

