import { PathwayRenderProps } from '#integration/model'
import { useParameterFallbacks, withPathwayParameterSpec } from '../../../parameter'
import { parameterPrefix } from '../../constants'
import { Config, Data } from './pathway'



export const useParameterSpec = () => {

    const fb = useParameterFallbacks()


    return (props: PathwayRenderProps<Config, Data>) => {


        const { data } = props

        const NA = data?.attachment ? fb.NA : fb.TBC

        return withPathwayParameterSpec({

            references: () => {

                return [

                    { id: `over24Count`, value: `${parameterPrefix}.over24Count` },
                    { id: `under24Count`, value: `${parameterPrefix}.under24Count` }

                ]

            }

            ,

            textValue: (_, path) => {

                switch (path[0]) {

                    case 'over24Count': return `${data.attachment?.over_24_meters_count ?? NA}`
                    case 'under24Count': return `${data.attachment?.under_24_meters_count ?? NA}`
                }

                return fb.ERR

            }

        })

    }


}