import { useCalls } from '#app/call/call'
import { PathwayRenderProps } from '#integration/model'
import { RavExplainer } from '#integration/rav/explainer'
import { RenderForm } from '../../../renderform'
import { ravservice, vcbylength } from '../../constants'
import { useCurrentFlag, useDefaultConfig } from '../utils'
import { Config, Data, VcByLengthAttachment, VcByLengthQuery } from './pathway'


export const Render = (props: PathwayRenderProps<Config, Data>) => {


    const calls = useCalls()

    const { defaultDate } = useDefaultConfig()

    const { pathway, data, onChangeData } = props

    const flag = useCurrentFlag()
    const date = pathway.date ?? defaultDate
    const under24 = pathway.under24
    const over24 = pathway.over24

    const requestValid = flag && date

    const refresh = async () => {

        const attachment = await calls.atPath(`${ravservice}/${vcbylength}`).post<VcByLengthAttachment, VcByLengthQuery>({ date, flag, under24, over24 })

        onChangeData({ ...data, attachment })

    }

    const explainer = <RavExplainer {...props} defaultExplainer='integration.rav.vcbylength.default_explainer' />

    return <RenderForm disabled={!requestValid} onRefresh={refresh} explainer={explainer} {...props} />

}