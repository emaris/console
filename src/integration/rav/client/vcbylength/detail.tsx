import { SelectBox } from "#app/form/SelectBox"
import { useT } from "#app/intl/api"
import { PathwayDetailProps } from '#integration/model'
import { DetailForm } from '../../../detailform'
import { CutoffDateBox } from "../utils"
import { useVesselRanges } from "../cache/vesselranges"
import { Config } from './pathway'


export const Detail = (props: PathwayDetailProps<Config>) => {

    const t = useT()

    const { pathway, onChange } = props

    const ranges = useVesselRanges()

    return <DetailForm defaultExplainer='integration.rav.vcbylength.default_explainer' {...props}>

        <CutoffDateBox {...props} />

        <SelectBox multi undefinedOption={t('integration.rav.ranges_all')} label={t('integration.rav.ranges_under24_lbl')} validation={{ msg: t('integration.rav.ranges_msg') }}

            selectedKey={pathway.under24?.ranges ?? []}
            onChange={(ranges: string[]) => onChange({ ...pathway, under24: { ranges } })}>

            {ranges}

        </SelectBox>

        <SelectBox multi undefinedOption={t('integration.rav.ranges_all')} label={t('integration.rav.ranges_over24_lbl')} validation={{ msg: t('integration.rav.ranges_msg') }}

            selectedKey={pathway.over24?.ranges ?? []}
            onChange={(ranges: string[]) => onChange({ ...pathway, over24: { ranges } })}>

            {ranges}

        </SelectBox>

    </DetailForm>

}