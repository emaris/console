
import { useRenderGuard } from 'apprise-frontend-core/utils/renderguard'
import { useConfig, useConfigLoader } from "./state"


export const ConfigLoader = (props:React.PropsWithChildren<{}>) => {

  const config = useConfig()
  const  loader = useConfigLoader()

  const {content} = useRenderGuard({
    when:config.isDefined(),
    render:props.children,
    orRun:loader.fetch
  })

  return content
}