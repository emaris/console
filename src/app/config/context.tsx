import { fallbackStateOver, State } from 'apprise-frontend-core/state/api'
import { createContext } from 'react'
import { BaseConfig } from './model'


export type ConfigState = {

    config: BaseConfig
  
  }
  
  export const initialConfig: ConfigState = {
  
    config: undefined!
  
  }

export const ConfigContext = createContext<State<ConfigState>>(fallbackStateOver(initialConfig))