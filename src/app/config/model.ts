import { IntlConfig } from "../intl/model";




export enum Mode { dev="development", production="production", staging="staging"}

export type BaseConfig = IntlConfig & {
  name:string
  mode: Mode
  toggles: string[]
  services: { [key: string]:ServiceConfig }
  fileLimitSize: number

  latest: boolean
  version: string | undefined

}

export type ServiceConfig = {

  prefix: string
  label:string
  default?: boolean

}


export const withBaseConfig =  <T extends BaseConfig>(self:T) => ({ 
  
  ...self,

  name: self?.name || 'app'

  ,

  toggles: self?.toggles ?? []

  ,

  service: (name:string) : ServiceConfig  => {

                const service = self.services[name]
                
                if (!service) 
                  throw new Error(`unknown service '${name}'`);
              
                return service;
  },

  defaultService: () : ServiceConfig => {
  
                      for (let k in self.services)
                        if (self.services[k].default)
                          return self.services[k]
                      
                    
                      const keys = Object.keys(self.services)
                    
                      if (keys.length === 1)
                        return self.services[keys[0]]
                      
                      throw new Error("no default service!"); 
                    
    }

})