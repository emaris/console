

import { useT } from '#app/intl/api';
import { usePreload } from 'apprise-frontend-core/client/preload';
import * as React from "react";
import { buildinfo } from "../buildinfo";
import { useCalls } from "../call/call";
import { useToggleBusy } from "../system/api";
import { failWith, through } from "../utils/common";
import { showAndThrow } from "../utils/feedback";
import { ConfigContext } from './context';
import { BaseConfig, Mode, withBaseConfig } from "./model";






export const useConfig = () => {

  const state = React.useContext(ConfigContext)
  
  // using apis here may form cycles at runtime: config, intl, and system have circular dependencies

  const self = {

    isDefined: () => state.get().config !== undefined,

    //  wraps with API and returns
    get: () => withBaseConfig({...state.get().config!}),

    set: (c: BaseConfig) => {
      
      state.set(s => s.config = c)
    
    },

    devmode: () => buildinfo.development || self.get().mode === Mode.dev,
    

    // mode is: a) DEV if it's a dev build, 2) whatever the config says it is, or 3) DEV if the config says nothing.
    runmode: (): Mode => buildinfo.development ? Mode.dev : (self.get().mode ?? (self.isDefined() && Mode.dev))
    


  }

  return self

}

export const useConfigLoader = () => {


  const t = useT()

  const toggleBusy = useToggleBusy()

  const call = useCalls()

  const config = useConfig()

  const preload = usePreload()

  // using apis here may form cycles at runtime: config, intl, and system have circular dependencies

  const self = {

    fetch: (): Promise<BaseConfig> => {


      return toggleBusy('config.fetch', t("config.feedback.loading"))

        .then(_ => console.log('loading configuration...'))
        .then(_ => preload.get<BaseConfig>(`/config.json`) ??  call.staticAt(`/config.json`).get<BaseConfig>())
        .then(through(validate))
        .then(through(config.set))

        .catch(e => showAndThrow(e, t("config.calls.fetch_error")))
        .finally(() => toggleBusy(`config.fetch`))


    }


  }

  return self

}



////    helpers


const validate = (config: BaseConfig) => {

  try {

    config.services || failWith("no services defined")


  }
  catch (e) {
    failWith(`configuration is invalid: ${e}.`)
  }
}



