import { useActions } from '#app/iam/model';
import { useT } from '#app/intl/api';
import { useModuleRegistry } from '#app/module/registry';
import { usePushEvents } from '#app/push/api';
import { useLogged } from '#app/user/store';
import { } from 'nats.ws';
import { mailSubject, mailTopicSeparator, mailType, noMailTopic } from './constants';
import { Mail, MailContent, MailTarget, MailTopic, TopicAudience, TParams, TranslatedMailContent } from './model';
import { MailSlot } from './module';


export const useMail = () => {

    const events = usePushEvents()

    const self = {

        sendTo: (target: MailTarget | MailTarget[]) => {

            const on = (content: MailContent) => {

                const self = {

                    on: (topic: MailTopic) => 
                    
                        // TODO: when we have push for settings changes, may use to short-circuit here instead of having the stack drop the message.
                        events.publish(mailSubject, { topic, target: typeof target === 'string' ? target : target.join(':'), content } as Mail)
                        
                    

                    ,

                    onTopic: (value: string, audience?: TopicAudience) => self.on({ key: undefined!, value, audience })

                }

                return self
            }



            return {

                subject: (subject: TranslatedMailContent['subject']) => ({


                    text: (text: TranslatedMailContent['text']) => on({ subject, text }) // translated mail content


                })

                ,

                content: on

                ,

                template: (template: string) => ({

                    params: (parameters: TParams) => on({ template, parameters }) // raw mail content

                })


            }
        }


    }


    return self;
}

export const useMailTopics = () => {

    const t = useT()
    const logged = useLogged()
    const actions = useActions()

    const registry = useModuleRegistry()


    const self = {

        all: () => {

            const modules = registry.allWith(mailType)

            // poor-man sorting of top-level topics based on module name
            modules.sort((m1, m2) => t(m1.namePlural).localeCompare(t(m2.namePlural)))

            const unsorted = modules.map(m => m[mailType] as MailSlot).flatMap(s => s.topics ?? [])

            const topics = unsorted.filter(t => logged.canAny(self.audienceOf(t)))

            topics.sort(self.comparator)

            return [noMailTopic,...topics] as MailTopic[];

        }

        ,

        extends: (t1: MailTopic | string, t2: MailTopic | string) => {
            

            const lbls1 = (typeof t1 === 'string' ? t1 : t1.value)?.split(mailTopicSeparator)
            const lbls2 = (typeof t2 === 'string' ? t2 : t2.value)?.split(mailTopicSeparator)

            return lbls1.length >= lbls2.length && lbls2.every((l2, i) => lbls1[i] === l2)
        }

        ,

        extendsAny: (t1: MailTopic | string, t2: (MailTopic | string)[] = []) => t2.some(t => self.extends(t1, t))

        ,


        comparator: (t1: MailTopic | string, t2: MailTopic | string) => self.extends(t1, t2) ? 1 : self.extends(t2, t1) ? -1 : 0

        ,

        audienceOf: (t: MailTopic) => (t.audience ?? []).map(actions.intern)

        ,

        reconcile: (topics: MailTopic[]) => {

            if (topics.includes(noMailTopic))
                return [noMailTopic]
                
            // sort upfront from most generic to most specific, so that reconciliation can be treated in 'streaming' fashion
            const sorted = [...topics]

            sorted.sort(self.comparator);

            //  simply add one by one if not already implied by previous ones
            return sorted.reduce((acc, a) => self.extendsAny(a, acc) ? acc : [a, ...acc], [] as MailTopic[])

        }


    }

    return self
}