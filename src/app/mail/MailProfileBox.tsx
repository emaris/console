import { Label } from '#app/components/Label'
import { Paragraph } from '#app/components/Typography'
import { FieldProps } from '#app/form/Field'
import { SelectBox } from '#app/form/SelectBox'
import { MailProfile, MailTopic } from '#app/mail/model'
import { useT } from '#app/intl/api'
import { useMailTopics } from './api'
import { mailIcon, noMailTopic } from "./constants"
import './styles.scss'

type MailProfileProps = FieldProps & {

    children?: MailProfile
    onChange: (_: MailProfile | undefined) => any

}



export const MailProfileBox = (props: MailProfileProps) => {

    const t = useT()
    
    const defaultHelpMessage = <>
        <Paragraph ellipsis={{ rows: 4, expandable: true }}>
            {t("mail.profile.help1")}
        </Paragraph>
        <Paragraph style={{ paddingTop: 5 }}>
            {t("mail.profile.help2")}
        </Paragraph>
    </>

    const { children: profile, onChange, label = t('mail.profile.label'), validation, ...rest } = props

    const { msg = t('mail.profile.msg'), help = defaultHelpMessage } = validation ?? {};

    const topics = useMailTopics()

    const allTopics = topics.all()

    const topicmap = allTopics.reduce( (acc,next) => ({...acc, [next.key] : next}), {} as Record<string,MailTopic>)

    const profileTopics = (profile?.topics ?? []).map(t => typeof t === 'string' ? {value:t} : t)

    const onChangeProfile = (newtopics: MailTopic[]) => {

        const reconciled = topics.reconcile(newtopics).map(({key,...rest})=>rest)

        return onChange(newtopics && reconciled.length > 0 ? { ...profile ?? {}, topics: reconciled } : undefined)

    }

    const selected = allTopics.filter(topic => profileTopics.some(t => t?.value===topic.value))

    const alreadyIncluded = (topic: MailTopic) => (topic!==noMailTopic && selected.includes(noMailTopic)) || (!!topic.value && topics.extendsAny(topic.value, profileTopics.filter(t=>t.value!==topic.value).map(t=>t.value)))

    const isRoot = (topic: MailTopic) => !topic.value || !topics.extendsAny(topic, allTopics.filter(t => t.value !== topic.value))
    

    const render = (topic: MailTopic) => <Label icon={mailIcon} className={isRoot(topic) ? 'root-topic' : ''} title={t(topic.key)}/>

    return <SelectBox placeholder={`[${t('mail.all_topics')}]`} label={label} validation={{ msg, help }}
        disabledOption={alreadyIncluded}
        includeSelected
        selectedKey={selected.map(s => s.key)}
        lblText={topic => t(topic.key)}
        getkey={topic => topic.key}
        getlbl={render}
        onChange={(values:string[]) => onChangeProfile(values.map(v => topicmap[v]))}
        {...rest}>
        {allTopics}
    </SelectBox>

} 