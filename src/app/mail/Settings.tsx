import { Form } from '#app/form/Form'
import { Switch } from '#app/form/Switch'
import { TextBox } from '#app/form/TextBox'
import { BaseSettings, SettingsProps } from '#app/settings/model'
import * as React from 'react'
import { useT } from '#app/intl/api'
import { MailAppSettings } from './model'

export type MailSettingsProps = BaseSettings & MailAppSettings


export const MailSettingsForm = (props: SettingsProps<MailAppSettings>) => {

    const t = useT()

    const {state,report} = props

    const {edited,change} = state

    return <Form state={state} sidebar>

        <Switch 
            label={t("mail.settings.silent.name")}
            validation={report.silent}
            onChange={change((t,v) => t.mail = v ? {...t.mail, quietMode: true, echoMode: false, divertMode: false} : {...t.mail, quietMode: v})}>
                {edited.mail.quietMode}
        </Switch>

        <Switch
            label={t("mail.settings.echo.name")}
            validation={report.echo} 
            disabled={edited.mail.quietMode === true}
            onChange={change((t,v) => t.mail = {...t.mail, echoMode: v})}>
                {edited.mail.echoMode}    
        </Switch>

        <TextBox
            label={t("mail.settings.echo_address.name")}
            validation={report.echoAddress} 
            disabled={!edited.mail.echoMode} 
            onChange={change((t,v)=>t.mail = {...t.mail, echoAddress: v})}
        >
            {edited.mail.echoAddress}
        </TextBox>

        <Switch
            label={t("mail.settings.divert.name")}
            validation={report.divert} 
            disabled={edited.mail.quietMode === true}
            onChange={change((t,v) => t.mail = {...t.mail, divertMode: v})}>
                {edited.mail.divertMode}    
        </Switch>

        <TextBox
            label={t("mail.settings.divert_address.name")}
            validation={report.divertAddress} 
            disabled={!edited.mail.divertMode} 
            onChange={change((t,v)=>t.mail = {...t.mail, divertAddress: v})}
        >
            {edited.mail.divertAddress}
        </TextBox>

    </Form>


}