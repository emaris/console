import { ActionDto } from '#app/iam/model'
import { Multilang, MultilangDto } from '#app/model/multilang'
import { noTenant } from '#app/tenant/constants'
import { TOptions } from 'i18next'
import { mailType } from './constants'


export type MailProfile = {
    topics: (MailTopicInstance | string)[]
}

export type MailTopicInstance = {

    value: string
    audience?: TopicAudience

}


export type MailTopic = {

    key: string
    
    value: string

    // who can subscribe to this topic
    audience?: TopicAudience

}

export type TopicAudience = ActionDto[]

export type Mail = {

    target: MailTarget;
    topic: MailTopic;
    content:  MailContent

}


export type MailTarget = 'all' | typeof noTenant | string

export type MailContent = TranslatedMailContent | TemplatedMailContent

export type TranslatedMailContent = {

    subject: Multilang
    text: Multilang
}

export type TemplatedMailContent = {

    template: string
    parameters: TParams
}

export type TParams = Record<string, MultilangDto | TArgs | string>

export type TArgs = {

    key: string

    params?: TParams
    options?: TOptions

}

export type MailSettings = {
    
    quietMode : boolean
    echoMode: boolean
    echoAddress: string
    divertMode: boolean
    divertAddress: string
  
}

export type MailAppSettings = Record<typeof mailType, MailSettings>
