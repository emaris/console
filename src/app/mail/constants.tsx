import { icns } from "#app/icons"


export const mailType = "mail"
export const mailIcon = icns.mail

export const mailSingular = 'mail.singular'
export const mailPlural = mailSingular

export const mailSubject = 'mail'
export const anyTopic = '*'
export const anyTarget = '*'
export const mailTopicSeparator = '.'


export const noMailTopic = {key: 'mail.no_topic', value: 'none'}

