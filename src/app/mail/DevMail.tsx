import { Icon } from "antd"
import { Button } from "#app/components/Button"
import { useMail } from "#app/mail/api"
import { MailTopic } from "#app/mail/model"
import { useLogged } from '#app/user/store'
import { notify } from "#app/utils/feedback"
import { useT } from '#app/intl/api'
import { AiOutlineMail } from "react-icons/ai"

type TemplateType = 'personaltest' | 'subchange' | 'duedate'

export const useDevMail = () => {

  const logged = useLogged()

  const name = logged.details.firstname ?? logged.username
  const mailer = useMail()

  const t = useT()


  const tmpl = `{"name": "${name}"}`

  const to = logged.username
  const topic = { key: undefined!, value: to, audience: undefined! } as MailTopic


  const sendMail = (template: TemplateType) => Promise.resolve(mailer.sendTo(to).template(template).params(JSON.parse(tmpl)).on(topic))
    .then(_ => notify(t("common.labels.mail_sent")))

  return <div style={{ display: "flex", flexDirection: 'column', alignItems: "flex-start" }}>
    <Button
      className="mail-button"
      type='default'
      size="small"
      iconLeft
      icn={<Icon component={AiOutlineMail} />}
      onClick={() => sendMail('personaltest')}
    >{t('common.buttons.simulate_email')}</Button>
  </div>

}