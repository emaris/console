
import { useT } from '#app/intl/api'
import { check, checkIt, ValidationCheck, withReport } from '#app/utils/validation'
import { TFunction } from 'i18next'
import { mailType } from './constants'
import { MailSettings } from './model'

const emailvalidation = (t: TFunction, def: boolean): ValidationCheck<string | undefined> => ({
    predicate: (s: string | undefined) => def === false ? false : s ? !s.match(/\S+@\S+\.\S+/) : true,
    msg: () => t("common.validation.invalid_email")
})

export const useMailValidation = () => {

    const t = useT()

    const self = {
        validatesettings: (edited:{[mailType] : MailSettings}) => {

            return withReport({
                silent: checkIt().nowOr(
                    t('mail.settings.silent.msg'),
                    t('mail.settings.silent.help')
                ),
                echo: checkIt().nowOr(
                    t('mail.settings.echo.msg'),
                    t('mail.settings.echo.help')
                ),
                divert: checkIt().nowOr(
                    t('mail.settings.divert.msg'),
                    t('mail.settings.divert.help')
                ),
                echoAddress: check(edited.mail.echoAddress).with(emailvalidation(t, !!edited.mail.echoMode)).nowOr(
                    t('mail.settings.echo_address.msg'),
                    t('mail.settings.echo_address.help')
                ),
                divertAddress: check(edited.mail.divertAddress).with(emailvalidation(t, !!edited.mail.divertMode)).nowOr(
                    t('mail.settings.divert_address.msg'),
                    t('mail.settings.divert_address.help')
                )
            })

        }
    }

    return self
}

