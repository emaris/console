import { ReadOnly } from '#app/scaffold/ReadOnly'
import { useFeedback } from '#app/utils/feedback'
import * as React from 'react'
import { useT } from '#app/intl/api'
import { PushEventListener, PushListenerProps } from './PushListener'



export type ChangeGuardProps = React.PropsWithChildren<PushListenerProps & {

    singular: string,
    askConsent?: boolean
    onReload: () => void


}>


export const RemoteChangeGuard = (props: ChangeGuardProps) => {

    const { singular, onReload, subscriptions = [], askConsent:withConsent=true, ...rest } = props

    const t = useT()

    const { askConsent } = useFeedback()

    const [changeDetected, changeDetectedSet] = React.useState(false)
    const [ignored, ignoredSet] = React.useState(false)

    const subs = Array.isArray(subscriptions) ? subscriptions : subscriptions() 

    const adaptedSubscriptions = subs.map(s => {

        return {

            ...s,
            onEvent: e => {

                if (!changeDetected && !ignored)
                    changeDetectedSet(true)

                s.onEvent?.(e)

            }
        }
    })

    const adaptedOnReload = () => {

        changeDetectedSet(false);
        onReload?.()
    }

    React.useEffect(() => {

        if (changeDetected)
            if (withConsent)
                askConsent({


                    title: t('push.consent.change_detection_title'),
                    content: <>
                        <p>{t("push.consent.change_detection_content_one", { singular: singular.toLowerCase() })}</p>
                        <p>{t("push.consent.change_detection_content_two")}</p>
                    </>,
                    okText: t('push.consent.change_detection_reload'),
                    cancelText: t(t('push.consent.change_detection_ignore')),

                    noValediction: true,

                    onOk: adaptedOnReload,
                    onCancel: () => ignoredSet(true)

                })
           
            else
                adaptedOnReload()

    // eslint-disable-next-line
    }, [withConsent,changeDetected])


    return <ReadOnly value={ignored}>
        <PushEventListener {...rest} subscriptions={adaptedSubscriptions} />
    </ReadOnly>
}