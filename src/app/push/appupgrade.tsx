
import { Button } from '#app/components/Button';
import { useConfig } from '#app/config/state';
import { useT } from '#app/intl/api';
import { PushEventListener } from '#app/push/PushListener';
import { Tooltip } from 'antd';


export type InfosysLifecycleEvent = {

    version: string
}

export const VersionUpgradeObserver = () => {

    // const config = useConfigState()
    // const lang = useLanguage()
    const config = useConfig()


    const checkUpgrade = (e: InfosysLifecycleEvent) => {

        const current = config.get()

        if (e.version === undefined || current.version === e.version)
            return

        // a reset of config has the drawback of resetting the lang to the default language.
        // so we override that to the language of the current user.
        // const defaultLanguage = lang.current();
        const newconfig = { ...current, version: e.version, latest: false }

        console.log("lifecycle event", e, "config:", { current, new: newconfig })

        config.set(newconfig)

    }

    return <PushEventListener name={`console-lifecycle-listener`} subscriptions={[{ topics: ['console'], onEvent: checkUpgrade }]} />


}


export const UpgradeButton = () => {

    const t = useT()

    const config = useConfig().get()

    return config.latest ?

        null :

        <Tooltip placement='bottom' title={t('upgrade_btn_tooltip')} >
            <Button enabledOnReadOnly className='upgradebutton' size='small' onClick={() => window.location.reload()}>
                {t('upgrade_btn')}
            </Button>
        </Tooltip>

}