import { PushSubscription } from "#app/push/model";
import * as React from 'react';
import { usePushEvents } from './api';

export type PushListenerProps = React.PropsWithChildren<{

    name: string
    subscriptions: (()=>PushSubscription[]) | PushSubscription[]

}>


export const PushEventListener = (props: PushListenerProps) => {

    const { subscriptions, name, children } = props

    const push = usePushEvents()

    const pushEventsMode = push.mode()

    React.useEffect(() => {

        console.log(`subscribing ${name} to event bus`)

        const subs = Array.isArray(subscriptions) ? subscriptions : subscriptions()

        const registered = push.register(subs)

        if (subs.length && registered.length===0)
            console.error(`can't subscribe ${name} for concurrent changes `)

        else 
        
        return () => {
            
            console.log(`unsubscribing ${name} from event bus`)

            registered.forEach(sub => sub.unsubscribe())
        }

        // eslint-disable-next-line
    }, [pushEventsMode])


    return <>{children}</>
}