import { PropsWithChildren } from 'react';
import { useLocation } from 'react-router-dom';

type PushGuardProps = PropsWithChildren<{}>

export const PushGuard = (props:PushGuardProps) => {

    const { children } = props
   
    const location = useLocation()

    return <div style={{height:'100%', width:'100%'}} key={`location-${location.state}`}>{children}</div>
}