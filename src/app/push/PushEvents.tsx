import { useT } from '#app/intl/api';
import { useModuleRegistry } from '#app/module/registry';
import { useLogged } from '#app/user/store';
import { notify } from '#app/utils/feedback';
import { useRenderGuard } from 'apprise-frontend-core/utils/renderguard';
import { PropsWithChildren, useContext, useRef } from 'react';
import { BiAlarmExclamation } from 'react-icons/bi';
import { useHistory, useLocation } from 'react-router-dom';
import { PushEventListener } from './PushListener';
import { usePushEvents } from './api';
import { pushEventType } from './constants';
import { PushStateContext } from './context';
import { PushEventSlot } from './module';



export const PushEvents = (props: PropsWithChildren<{}>) => {

    const { children } = props

    const t = useT()

    const history = useHistory();

    const registry = useModuleRegistry()

    const events = usePushEvents()

    const state = useContext(PushStateContext)

    const initialise = async (mode: 'connect' | 'reconnect') => {

        const initialised = await events.init(mode)

        state.set(s => s.push.connection = initialised!)

        return initialised
        
    }


    const initiliseAndMonitor = async () => {

        await initialise('connect')

        let notified = {mode: 'connected'}

        setInterval(async () => {

            const { connection: currentConnection } = state.get().push

            if (!currentConnection || currentConnection.isClosed()) {

                const newConnection = await initialise('reconnect')

                if (newConnection && notified.mode === 'disconnected') {
                    notify(t("bus.connection_established"))
                    notified.mode = 'connected'
                }

                if (!newConnection && notified.mode === 'connected') {
                    notify(t("bus.connection_failure"), {icon: <BiAlarmExclamation color='orange'  />})
                    notified.mode = 'disconnected'
                }

                
            }

        }, 5000)

    }

    const logged = useLogged()

    const locationRef = useRef<ReturnType<typeof useLocation>>(undefined!)

    locationRef.current = useLocation()


    // we use global state directly here as what we can capture now will be stale when events arrive.
    // so we use it also for onSubscribe() for uniformity.

    const subscriptions = () => {

        return registry.allSlotsWith<PushEventSlot>(pushEventType)
            .flatMap(slot => slot.onSubcribe(logged))
            .map(sub => ({
                topics: sub.topics,
                onEvent: (event: any) => {

                    try {
                        sub.onEvent(event, history, locationRef.current)

                    }
                    catch (e) {
                        console.error("can't process event:", { event: e })
                    }
                }
            })
            )
    }

    const { content } = useRenderGuard({
        orRun: initiliseAndMonitor,
        render: () =>

            <PushEventListener name='application' subscriptions={subscriptions}>
                {children}
            </PushEventListener>,
    })


    return content

}

