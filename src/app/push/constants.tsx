import { useT } from '#app/intl/api'
import { useFeedback } from '#app/utils/feedback'


export const pushEventType = "pushevent"


export const connectionProperties = () => {

    return {

        servers: `${window.location.protocol === 'http:' ? 'ws' : 'wss'}://${window.location.host}/event`,
        timeout: 10000

    }
}


export const useAskReloadOnPushChange = () => {

    const t = useT()
    const { askConsent } = useFeedback()



    return (props: {

        singular: string
        onOk: () => any
        onCancel?: () => any

    }) => {

        const { singular, onOk, onCancel } = props


        askConsent({

            title: t('push.consent.change_detection_title'),
            content: t(onCancel ? "push.consent.change_detection_content_change" : "push.consent.change_detection_content_change_nocancel", { singular: t(singular).toLowerCase() }),
            okText: t('push.consent.change_detection_reload'),
            okCancel: !!onCancel,
            noValediction: true,

            onOk,
            onCancel: onCancel ?? onOk

        })
    }
}


export const useAskReloadOnPushRemove = () => {

    const t = useT()

    const { askConsent } = useFeedback()

    return (props: {

        singular: string
        onOk: () => any

    }) => {

        const { singular, onOk } = props

        askConsent({

            title: t('push.consent.change_detection_title'),
            content: t("push.consent.change_detection_content_remove", { singular: t(singular).toLowerCase() }),
            okText: t('push.consent.change_detection_reload'),
            okCancel: false,
            noValediction: true,

            onOk,
            onCancel: onOk
        })
    }
}

