import { User } from '#app/user/model';
import { Subscription } from './model';


export type PushEventSlot<E=any> = {

    onSubcribe: (logged: User) => Subscription<E>[]
}