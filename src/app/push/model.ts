import { useHistory, useLocation } from "react-router-dom"


export type Subscription<E=any> = {

    topics: string[]
    onEvent: (_:E, history: ReturnType<typeof useHistory>, location: ReturnType<typeof useLocation>) => void

}


export type PushSubscription<E=any> = {

    topics: string[]
    filter?: (_:E) => boolean
    onEvent?: (_:E) => void

}

