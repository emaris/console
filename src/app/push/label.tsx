import { Fragment, useEffect, useState } from "react"

import './styles.scss'
import { Tooltip } from "antd"
import { usePushEvents } from "./api"
import { useT } from "#app/intl/api"


type Props = {
    disconnectedOnly?: boolean
}


export const EventBusConnectionLabel = (props: Props) => {

    const { disconnectedOnly = false } = props

    const t = useT()

    // const ctx = useContext(EventConnectionContext)
    const { connection } = usePushEvents()

    const [isConnected, setConnected] = useState(true)

    useEffect(() => {
        const interval = setInterval(() => setConnected(!(connection()?.isClosed() ?? true)), 3000)
        return () => clearInterval(interval)
        //eslint-disable-next-line
    }, [])

    const disconnected = <Tooltip title={t('bus.connection_failure')}><div className='disconnectbadge'>
        {t('bus.disconnected')}
    </div></Tooltip>

    const connected = <Tooltip title={t('bus.connection_ok')}><div className='connectbadge'>
        {t('bus.connected')}
    </div></Tooltip>

    return <Fragment>
        {
            disconnectedOnly ? !isConnected ? disconnected : null
                :
                isConnected ? connected : disconnected
        }
    </Fragment>

}