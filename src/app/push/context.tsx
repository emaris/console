import { fallbackStateOver, State } from 'apprise-frontend-core/state/api'
import { NatsConnection } from 'nats.ws'
import { createContext } from 'react'


export type PushEventState = {

    push: {
        connection: NatsConnection
        mode: 'active' | 'silent'
    }

}

export const initialPushEventState: PushEventState = {

    push: {
        connection: undefined!,
        mode: 'active'
    }
}


export const PushStateContext = createContext<State<PushEventState>>(fallbackStateOver(initialPushEventState))