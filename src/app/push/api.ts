import { useT } from '#app/intl/api';
import { useFeedback } from '#app/utils/feedback';
import { useClientSession } from 'apprise-frontend-core/client/session';
import { JSONCodec, connect } from 'nats.ws';
import { useContext } from 'react';
import { connectionProperties } from './constants';
import { PushStateContext } from './context';
import { PushSubscription } from './model';


const codec = JSONCodec()

export const usePushEvents = () => {

    const state = useContext(PushStateContext)

    const session = useClientSession()

    const t = useT()

    const fb = useFeedback()

    const self = {

        init: (mode: 'connect' | 'reconnect') => {

            console.log(mode === 'reconnect' ? 're-connecting to event bus...' : 'connecting to event bus...')

            return connect(connectionProperties())

                .then(connection => {

                    state.set(s => s.push.connection = connection)

                    connection.closed().then(err => {

                        if (err)
                            console.log(`error disconnecting from event bus`, err)

                    })

                    return connection

                })
                .catch(err => {

                    mode === 'reconnect' || console.log(`cannot connect to event bus`, err)
                })



        }

        ,

        register: (subscriptions: PushSubscription[]) => {


            // register subscriptions only if we have established a connection.
            return !self.connection() || self.connection().isClosed() ? [] :

                subscriptions.flatMap(subscription => {

                    const { topics, onEvent = console.log, filter = () => true } = subscription

                    const modeAwareFilter = (event: any) => state.get().push.mode === 'active' && filter(event)

                    return topics.map(topic => {

                        const sub = self.connection().subscribe(topic)

                        const waitOnMessage = async () => {

                            for await (const m of sub) {

                                const event = codec.decode(m.data) as { originSession: string }

                                const sameSession = event.originSession && session.get() === event.originSession

                                if (sameSession)
                                    continue

                                if (modeAwareFilter(event))
                                    onEvent(event)

                            }
                        }

                        waitOnMessage();

                        return sub

                    })

                })
        }

        ,

        connection: () => state.get().push.connection

        ,

        publish: <T extends any>(topic: string, data: T) => {

            console.log(`publishing ${topic} event`, JSON.stringify(data))

            self.connection() && !self.connection().isClosed() && self.connection()?.publish(topic, codec.encode(data))
        }

        ,

        mode: () => state.get().push.mode

        ,

        toggleMode: () => self.setMode(state.get().push.mode === 'active' ? 'silent' : 'active')

        ,

        setMode: (mode: 'active' | 'silent') => {
            const currentMode = state.get().push.mode

            const reloadSystem = () => window.location.reload()

            if (currentMode === 'silent') {

                fb.askConsent({
                    title: t("common.push_notif_mode.alert.title"),
                    content: t("common.push_notif_mode.alert.contents"),
                    onOk: reloadSystem,
                    okText: t("common.push_notif_mode.alert.ok"),
                    cancelText: t("common.push_notif_mode.alert.cancel")
                })


            } else
                state.set(s => s.push.mode = mode)
        }


    }

    return self;



}