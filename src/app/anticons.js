
//  we list below ant icons we want to bundle.
//  this is referred to from 'config-overrides.js' to intercept and reroute antd-native icon resolution (ie. <Icon .. type="..." />).
//  note: we only list the icons we cannot import from react-icons in 'icons.tsx', 
// such as two-tone icons or those that for some reason have problematic wrappers.

export {
    default as FlagTwoTone
} from "@ant-design/icons/lib/twotone/FlagTwoTone";


export {
    default as PlusOutline
} from "@ant-design/icons/lib/outline/PlusOutline";


export {
    default as MinusOutline
} from "@ant-design/icons/lib/outline/MinusOutline";

export {
    default as CheckCircleOutline
} from "@ant-design/icons/lib/outline/CheckCircleOutline";

export {
    default as CheckOutline
} from "@ant-design/icons/lib/outline/CheckOutline";

export {
    default as CloseCircleFill
} from "@ant-design/icons/lib/fill/CloseCircleFill";


export {
    default as RightOutline
} from "@ant-design/icons/lib/outline/RightOutline";


export {
    default as Exclamation
} from "@ant-design/icons/lib/outline/ExclamationOutline";


export {
    default as Calendar
} from "@ant-design/icons/lib/outline/CalendarOutline";


export {

    default as Up

} from "@ant-design/icons/lib/outline/UpOutline";


export {

    default as Down

} from "@ant-design/icons/lib/outline/DownOutline";