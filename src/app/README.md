## APPRISE-FRONTEND

Common library to be used as scaffold for apprise react based webapp.
The library must be linked to any webapp that wish to use it.

```
cd path/to/apprise-frontend
yarn link
```
