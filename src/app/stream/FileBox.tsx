import { Button } from "#app/components/Button"
import { HtmlSnippet } from "#app/components/HtmlSnippet"
import { Label } from "#app/components/Label"
import { useConfig } from "#app/config/state"
import { directoryFilter, FileDroppable } from "#app/dnd/Droppable"
import { Field, FieldProps } from "#app/form/Field"
import { ReadonlyProps, useReadonly } from '#app/form/ReadonlyBox'
import { ResetBtn } from '#app/form/resetbtn'
import { icns } from "#app/icons"
import { useFixedT } from '#app/intl/api'
import { Language } from "#app/intl/model"
import { useIntl } from '#app/intl/store'
import { showWarning } from "#app/utils/feedback"
import { Validation } from "#app/utils/validation"
import { Icon } from "antd"
import { utils } from 'apprise-frontend-core/utils/common'
import * as React from "react"
import { AiTwotoneFolderOpen } from "react-icons/ai"
import { useBytestreams } from "./api"
import { fileTypeIcon, FileTypeInfo, fileTypeInfo } from "./constants"
import { Bytestream } from "./model"
import "./styles.scss"



export type FileBoxProps = Omit<FieldProps, 'onChange'> & ReadonlyProps & {

    resources?: Bytestream[]
    defaultValue?: Bytestream[]

    onRemove?: (_: Bytestream) => void
    onDrop: (_: File[]) => void
    onReset?: () => any

    render?: (_: Bytestream) => React.ReactNode

    mode?: "multi" | "single"

    dragMsg?: string
    dropMsg?: string

    allowedMimeTypes?: FileTypeInfo[] | string[]

    light?: boolean

    language?: Language

}

export const FileBox = (props: FileBoxProps) => {

    const intl = useIntl()

    const currentLanguage = intl.currentLanguage()
    const { language = currentLanguage } = props

    const t = useFixedT(language)

    const config = useConfig()

    const bytestreams = useBytestreams()

    const {

        className,

        defaultValue,
        resources = defaultValue ?? [],

        mode = "multi",

        onDrop,
        onRemove = () => { },
        onReset,

        render = stream => <a href={bytestreams.linkOf(stream.id)} download>{stream.name}</a>,

        validation,
        light,

        dragMsg,
        dropMsg = t("common.feedback.drop_files"),

        allowedMimeTypes,

        focusDecorations,

        enabledOnReadOnly,
        readonlyMode,
        readonly,
        readonlyUnlocked,
        readonlyIcon,
        

        ...rest } = useReadonly(props)


 
    const canDropMore = mode === 'multi' || resources.length === 0

    const fileLimitSize = config.get().fileLimitSize

    const validateFileLength = (f: Bytestream | File) =>
        fileLimitSize ?
            f.size <= (fileLimitSize * Math.pow(1024, 2)) ? true : false
            : true


    const computedAllowedMimeTypes = allowedMimeTypes ?
        (allowedMimeTypes as (FileTypeInfo | string)[])
            .every(e => typeof e === "string") ?
            allowedMimeTypes as string[] :
            (allowedMimeTypes as FileTypeInfo[]).reduce((acc, cur) => [...acc, ...fileTypeInfo.find(ft => ft.name === cur.name)!.mime], [] as string[]).filter((val, index, self) => self.indexOf(val) === index) as string[]
        : undefined

    const validateResources = (): boolean => (computedAllowedMimeTypes === undefined || computedAllowedMimeTypes.length === 0) ? true :
        resources.reduce((acc, cur) => [...acc, cur.type], [] as string[]).map(m => computedAllowedMimeTypes?.some(c => c === m) ? true : false).some(m => m === false) ? false : true



    const validateResource = (f: Bytestream | File): boolean => (computedAllowedMimeTypes === undefined || computedAllowedMimeTypes.length === 0) ? true :
        computedAllowedMimeTypes.some(cr => cr === f.type) ? true : false


    // detect
    const validationWrap = (): Validation | undefined => {

        let msg = ""
        if (mode === 'single' && resources.length > 1) msg = `${msg}${t("common.validation.too_many")}`
        if (!validateResources()) msg = `${msg} ${t("common.validation.error_file")}`

        return msg.length === 0 ? validation : { status: "error", msg } as Validation

    }

    const hiddenFileInputRef = React.createRef<HTMLInputElement>()

    const onAddFile = (e) => {
        const allFiles = e.target.files?.item(0) ? Array.from(e.target.files) as File[] : []
        const uploadedFilesCount = e.target.files?.item(0) ? Array.from(e.target.files).length : 0
        const validFiles = Array.from(allFiles).filter(f => validateFileLength(f as File))
        if (validFiles.length < uploadedFilesCount) {
            showWarning({
                title: t("bytestream.upload_warning.title"),
                message: t("bytestream.upload_warning.msg", { limit: fileLimitSize })
            })
        }
        // To handle multiplicity of files we create an array of legth file, reduce it to an array of files from the ref component and send it to the onDrop
        validFiles.length > 0 && Promise.resolve(directoryFilter(validFiles).then(onDrop))
        // Reset the ref component
        if (hiddenFileInputRef.current?.value) hiddenFileInputRef.current.value = ''
    }


    const openFileDialog = () => hiddenFileInputRef.current?.click()


    const resettable = !!props.defaultValue && !!props.resources && !utils().deepequals(props.defaultValue,props.resources) && !readonly

    const decorations = [...focusDecorations ?? []]

    // add readonly icon only if can be edited or has been unlocked. 
    if (readonlyMode==='chrome' && (readonly || readonlyUnlocked))
        decorations.push(readonlyIcon)

    if (onReset && resettable)
        decorations.push(<ResetBtn enabledOnReadonly={enabledOnReadOnly} onClick={onReset} />)

    const readonlyData = resources ?? defaultValue

    const defaulting = props.defaultValue?.length && !props.resources

    const classes = `${className ?? ''} filebox ${defaulting ? 'filebox-default-value' : ''}`

    if (readonly && readonlyMode === 'content')

        return <Field className={classes} label={props.label} >
            <div className={`file-resources  ${readonlyData?.length ? '' : 'readonly-placeholder'}`}>{

                resources?.length === 0 ? '- ' :

                    resources.map((r, i) => <div key={i} className="file-resource-borderless">
                        <Label className="resource-name" icon={iconFor(r)} title={render(r)} />
                    </div>)

            }</div>
        </Field>

    const placeholder = dragMsg || (t(mode === "multi" ? "common.feedback.drag_many_files" : "common.feedback.drag_one_file"))

    const contents = <>

        <input ref={hiddenFileInputRef} type="file" multiple={mode === 'multi'} name="name" style={{ "display": "none" }} onChange={onAddFile} />

        <div className='file-resources'>{

            resources.map((r, i) => <div key={i} className={validateResource(r) ? "file-resource" : "file-resource error"}>
                <Label className="resource-name" icon={iconFor(r)} title={render(r)} />
                <Button enabledOnReadOnly={enabledOnReadOnly} noborder size="small" icn={icns.removeEntry} onClick={_ => onRemove(resources[i])} />
            </div>)
        
        }</div>

        {canDropMore &&

            <FileDroppable className="file-droppable" onDrop={onDrop} unless={props.disabled || readonly}>
                <div className={`file-msg msg-drop`}><span>{dropMsg}</span></div>
                <div className={`file-msg msg-drag`}>
                    <span>{placeholder && <HtmlSnippet snippet={placeholder} />}</span>
                    <Button className="file-dialog-button" enabledOnReadOnly={enabledOnReadOnly} disabled={props.disabled} noborder icn={<Icon component={AiTwotoneFolderOpen} />} onClick={openFileDialog} />
                    {fileLimitSize && <div style={{ fontSize: "smaller", marginLeft: 25 }}><span>{t("common.labels.max_file_size", { size: fileLimitSize })}</span></div>}
                </div>
            </FileDroppable>
        }
    </>



    return light ?

        <div className={classes}>
            {contents}
        </div>

        :

        <Field className={classes} focusDecorations={decorations} {...rest} validation={validationWrap()} >
            {contents}
        </Field>

}


export const iconFor = (resource: Bytestream | string) => typeof resource === 'string' ? fileTypeIcon[resource] ?? icns.file : fileTypeIcon[resource.type] ?? icns.file