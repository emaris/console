import { indexMap } from "#app/utils/common";
import produce from "immer";
import * as React from 'react';
import { PropsWithChildren, useState } from "react";
import { useBytestreams } from "./api";
import { BytestreamChange, Bytestreamed, FileDescriptor } from "./model";


type StreamState = {

    tenant? : string
    target?: string
    descriptors: FileDescriptor[]
    setDescriptors: React.Dispatch<React.SetStateAction<FileDescriptor[]>>

}

export const Context = React.createContext<StreamState>(undefined!)

export const useBytestreamedContext =  () => React.useContext(Context)

export const BytestreamedContext = (props: PropsWithChildren<Partial<StreamState>>) => {

    const {descriptors:clientDescriptors=[],...rest} = props

    const [descriptors, setDescriptors] = useState<FileDescriptor[]>(clientDescriptors)

    return <Context.Provider value={{ descriptors, setDescriptors, ...rest }}>
        {props.children}
    </Context.Provider>
}



export type BytestreamedHelper = ReturnType<typeof useBytestreamedHelper>

export const useBytestreamedHelper = () => {

    const {descriptors, setDescriptors} = useBytestreamedContext() ?? {}

    const streams = useBytestreams()

    const self = {

            
        addOrReplace: ({ id }: Bytestreamed, file: File) => {

            const descriptor = { resource: id, file }
            
            const index = descriptors.findIndex(d => d.resource === descriptor.resource)

            setDescriptors(produce( s => {
                
                if (index < 0) 
                    s.push(descriptor)
                else
                    s.splice(index, 1, descriptor) 
            
            }))

        }

        ,

        remove: (resourceId: string) => {

            const index = descriptors.findIndex(d => d.resource === resourceId)

            if (index >= 0)
            setDescriptors(produce(s => { s.splice(index, 1) }))

        }

        ,

        // promises to return the resource after handling an update to one of its streams.
        updateWith: <T extends Bytestreamed> (resource: T, change?: BytestreamChange) => {
            if (change?.mode === 'upload')
                self.addOrReplace(resource, change.file!)
                
            else if (change?.mode === 'uploadRemoval')
                self.remove(resource.id)

            return Promise.resolve(resource)    
        }

        ,

        all: () => [...descriptors]

        ,


        upload:<T extends Bytestreamed> (...resources: T[]) => {
            
            const resourceMap = indexMap(descriptors).by(d => d.resource)
            const streamsAndBlobs = resources.filter(r=>Object.keys(resourceMap).includes(r.id)).map(r => [r, resourceMap[r.id].file] as [T, File])

            return streams.upload(streamsAndBlobs)

        }

        ,

        reset: () => setDescriptors([])


    }

    return self;
}
