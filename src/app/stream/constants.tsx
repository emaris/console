import { Icon } from "antd"
import * as React from "react"
import * as anticns from "react-icons/ai"


export const bytestreamType="bytestream"

export const nameSingular = "bytestream.name_singular"
export const namePlural = "bytestream.name_plural"

export const fileMimeType = {

    text : 'text/plain',
    pdf: 'application/pdf',
    zip : 'application/zip',
    tar : 'application/x-xz',
    rar : 'application/vnd.rar',
    _7z :'application/x-7z-compressed',
    png : 'image/png',
    jpeg : 'image/jpeg',
    msword : 'application/msword',
    opendocword : 'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
    msexcel : 'application/vnd.ms-excel',
    opendocexcel : 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
    csv : 'text/csv',
    msppt : 'application/vnd.ms-powerpoint',
    opendocppt : 'application/vnd.openxmlformats-officedocument.presentationml.presentation'
}

export const fileTypeIcon = {
    [fileMimeType.text] : <Icon className="file-icon" style={{color: "black"}} component={anticns.AiOutlineFileText} />,
    [fileMimeType.pdf] : <Icon className="file-icon" style={{color: "red"}} component={anticns.AiOutlineFilePdf} />,
    [fileMimeType.zip]: <Icon className="file-icon" style={{color: "rgb(177, 177, 0)"}} component={anticns.AiOutlineFileZip} />,
    [fileMimeType.tar]: <Icon className="file-icon" component={anticns.AiOutlineFileZip} />,
    [fileMimeType.rar]: <Icon className="file-icon" style={{color: "rgb(138, 20, 20)"}} component={anticns.AiOutlineFileZip} />,
    [fileMimeType._7z]: <Icon className="file-icon" style={{color: "rgb(116, 138, 20)"}} component={anticns.AiOutlineFileZip} />,
    [fileMimeType.png]: <Icon className="file-icon" style={{color: "#DC4172"}} component={anticns.AiOutlineFileImage} />,
    [fileMimeType.jpeg]: <Icon className="file-icon" style={{color: "rgb(167, 167, 1)"}} component={anticns.AiOutlineFileJpg} />,
    [fileMimeType.msword]: <Icon className="file-icon" style={{color: "blue"}} component={anticns.AiOutlineFileWord} />,
    [fileMimeType.opendocword]: <Icon className="file-icon" style={{color: "blue"}} component={anticns.AiOutlineFileWord} />,
    [fileMimeType.msexcel]: <Icon className="file-icon" style={{color: "green"}} component={anticns.AiOutlineFileExcel} />,
    [fileMimeType.opendocexcel]: <Icon className="file-icon" style={{color: "green"}} component={anticns.AiOutlineFileExcel} />,
    [fileMimeType.csv] : <Icon className="file-icon" style={{color: "black"}} component={anticns.AiOutlineFileText} />,
    [fileMimeType.msppt] : <Icon className="file-icon" style={{color: "orange"}} component={anticns.AiOutlineFilePpt} />,
    [fileMimeType.opendocppt] : <Icon className="file-icon" style={{color: "orange"}} component={anticns.AiOutlineFilePpt} />
}

export type FileTypeInfo = {
    name: string,
    description: string,
    mime: string[]
}

export const fileTypeInfo:FileTypeInfo[] = [
    {
        name : "common.file.document.name",
        description : "common.file.document.msg",
        mime : [fileMimeType.msword, fileMimeType.opendocword, fileMimeType.pdf, fileMimeType.text]
    },
    {
        name : "common.file.tabular.name",
        description : "common.file.tabular.msg",
        mime: [fileMimeType.msexcel, fileMimeType.opendocexcel, fileMimeType.csv, fileMimeType.text]
    },
    {
        name: "common.file.image.name",
        description : "common.file.image.msg",
        mime: [fileMimeType.jpeg, fileMimeType.png]
    },
    {
        name: "common.file.compressed.name",
        description : "common.file.compressed.msg",
        mime: [fileMimeType.zip, fileMimeType.tar, fileMimeType.rar, fileMimeType._7z]
    },
    {
        name: "common.file.word.name",
        description : "common.file.word.msg",
        mime: [fileMimeType.msword, fileMimeType.opendocword]
    },
    {
        name: "common.file.pdf.name",
        description : "common.file.pdf.msg",
        mime: [fileMimeType.pdf]
    },
    {
        name: "common.file.excel.name",
        description : "common.file.excel.msg",
        mime: [fileMimeType.msexcel,fileMimeType.opendocexcel]
    },
    {
        name: "common.file.csv.name",
        description : "common.file.csv.msg",
        mime: [fileMimeType.csv]
    },
    {
        name: "common.file.zip.name",
        description : "common.file.zip.msg",
        mime: [fileMimeType.zip]
    }
]

export type FileType = {
    mime: string,
    name: string
}

export const fileTypes:FileType[] = [
    { mime: fileMimeType.text, name: "common.file.mime.text"},
    { mime: fileMimeType.pdf, name: "common.file.mime.pdf"},
    { mime: fileMimeType.zip, name: "common.file.mime.zip"},
    { mime: fileMimeType.tar, name: "common.file.mime.tar"},
    { mime: fileMimeType.rar, name: "common.file.mime.rar"},
    { mime: fileMimeType._7z, name: "common.file.mime.7z"},
    { mime: fileMimeType.png, name: "common.file.mime.png"},
    { mime: fileMimeType.jpeg, name: "common.file.mime.jpeg"},
    { mime: fileMimeType.msword, name: "common.file.mime.msword"},
    { mime: fileMimeType.opendocword, name: "common.file.mime.opendocword"},
    { mime: fileMimeType.msexcel, name: "common.file.mime.msexcel"},
    { mime: fileMimeType.opendocexcel, name: "common.file.mime.opendocexcel"},
    { mime: fileMimeType.csv, name: "common.file.mime.csv"},
    { mime: fileMimeType.msppt, name: "common.file.mime.msppt"},
    { mime: fileMimeType.opendocppt, name: "common.file.mime.opendocppt"} 
]

export const unknownFileType: FileType = {mime: undefined!, name: "common.file.mime.unknown"}