import { Col, Input } from 'antd'
import { useConfig } from '#app/config/state'
import { Field, FieldProps } from '#app/form/Field'
import { FormContext } from '#app/form/Form'
import { MultiBox } from '#app/form/MultiBox'
import { ReadonlyProps, useReadonly } from '#app/form/ReadonlyBox'
import { allLanguages, defaultLanguage, Language } from '#app/intl/model'
import { useIntl } from '#app/intl/store'
import * as React from 'react'
import shortid from 'shortid'
import { useBytestreams } from './api'
import { useBytestreamedContext } from './BytestreamedHelper'
import { FileBox, FileBoxProps } from './FileBox'
import { Bytestream, bytestreamFor, Document, DocumentChange, FileDescriptor, newDocument } from './model'




export type Props = Pick<FileBoxProps, 'allowedMimeTypes'> & {

    tenant?: string
    target?: string

    descriptors?: FileDescriptor[]
    children: Document

    multi?: boolean

    onChange: (_: Document, streamChange?: DocumentChange) => void

    language?: Language

}

export const DocumentBox = (props: Props) => {

    const intl = useIntl()
    const currentLanguage = intl.currentLanguage()
    const {language=currentLanguage} = props

    const t = intl.getFixedT(language)


    const ctx = useBytestreamedContext() ?? {}

    const streams = useBytestreams()

    const id = React.useRef(shortid())

    const { children: edited = newDocument(), allowedMimeTypes, onChange, descriptors = ctx.descriptors, tenant= ctx.tenant, target: ref = ctx.target } = props

    const onDrop = (lang: Language, files: File[]) => {

        const file = files[0]

        return onChange({ ...edited, streams: {...edited.streams, [lang]: { ...bytestreamFor(file), tenant, ref }} }, { lang, mode: 'upload', file })
    }

    const onRemove = (lang:Language) =>  onChange({ ...edited, streams: {...edited.streams, [lang]: undefined }}, { lang, mode: 'uploadRemoval' })

    const resources = edited.streams ?? {'en': undefined!}

    const onRender = (stream: Bytestream) => {

        const match = descriptors.find(d => d.resource === edited.id)

        return match ?

            <a download={match.file.name} href={URL.createObjectURL(match.file)}>{stream.name}</a>

            :

            <a download href={streams.linkOf(stream.id)}>{stream.name}</a>

    }


    return <>

        <MultiBox id={`${id.current}-title`} label={t("common.fields.name_multi.name")} onChange={title => onChange({ ...edited, title })} >
            {edited.title}
        </MultiBox>

        <MultiBox id={`${id.current}-description`} label={t("common.fields.description_multi.name")} onChange={description => onChange({ ...edited, description })} >
            {edited.description}
        </MultiBox>

        <MultiBytestreamBox id={'test'} language={language} allowedMimeTypes={allowedMimeTypes} onDrop={onDrop} onRemove={onRemove} resources={resources} render={onRender} />

    </>
}


export type MultiBytestreamBoxProps = FieldProps & ReadonlyProps & Pick<FileBoxProps, 'allowedMimeTypes'> & {
    id: string
    onDrop: (lang: Language, t: any) => void
    onRemove: (lang: Language) => void
    render?: (_:Bytestream) => React.ReactNode 
    required?: Language[]
    languages?: Language[]
    language?: Language

    resources: {[key in Language]? : Bytestream}

    langProps?: (l: Language, val: string | undefined) => Partial<{

        disabled: boolean
        readonly: boolean

    }>

    readOnlyLang? : Language
}

export const MultiBytestreamBox = (props: MultiBytestreamBoxProps) => {
    // const t = useT()

    const { auxstate = {}, auxchange = ((() => { }) as Function) } = React.useContext(FormContext) ?? {}

    const { intl } = useConfig().get()

    const currentLanguage = useIntl().currentLanguage()
    const {language=currentLanguage} = props

    const {  id, onDrop, allowedMimeTypes, onRemove, render, resources={}, langProps, readOnlyLang, readonly, readonlyMode, 
        ReadOnlyBox,canUnlock, readonlyUnlocked, ...rest } = useReadonly(props)

    const { languages, required } = requiredFrom(props, intl.languages, intl.required)

    // const [choices, setChoices]: [Language[], (l: Language[]) => any] = [auxstate[id] || initialChoicesFrom(props, intl.languages, required), auxchange((t, v) => t[id] = v)]
    const [choices]: [Language[], (l: Language[]) => any] = [auxstate[id] || initialChoicesFrom(props, intl.languages, required), auxchange((t, v) => t[id] = v)]

    const other = languages.filter(l => !choices.includes(l))

    // const otherLanguageName = other.length === 1 ? t(fullnameOf(other[0])) : undefined

    const classes = `multibox ${other.length > 0 ? 'with-language-choice' : ''} ${readonly ? 'readonlybox' : ''} ${readonly && readonlyMode === 'content' ? 'readonlybox-content' : ''}`

    // const langclasses = choices.length > 1 ? 'in-multibox' : ''

    const replace = (l: Language, value: File[]) => onDrop(l, value)

    return <Field className={classes} {...rest}>
        <>

            {choices.map(l => {


                const res = ((resources && resources[l] && resources[l]) ? [resources[l]] : [])  as Bytestream[]

                const change = (v: File[]) => replace(l, v)

                const remove = () => onRemove(l)

                return <Input.Group key={l} className="multibox-language" compact={true}>
                    { choices.length <=1 || 

                        <Col className="prefix-container" xs={1}>
                            <span className="prefix">{l}</span>
                        </Col>
                    }
                    <Col style={{width: '100%'}}>
                        <div className='multiFileBox'>
                        <FileBox 
                            mode='single'
                            onDrop={change}
                            onRemove={remove}
                            resources={res}
                            render={render}
                            language={language}
                            light
                            allowedMimeTypes={allowedMimeTypes}
                        />
                        </div>
                    </Col>
                </Input.Group>
            })}

        </>

    </Field>

}


const requiredFrom = (props: MultiBytestreamBoxProps, configLanguages: Language[] = allLanguages, requiredLanguages: Language[] = []) => {

    // props may override
    const { languages = configLanguages, required = requiredLanguages } = props;

    return { languages, required };

}
const initialChoicesFrom = (props: MultiBytestreamBoxProps, languages: Language[], required: Language[]) => {

    const { resources = [] } = props

    const defined = (languages as Language[]).filter(l => required.includes(l) || resources[l])

    const choices = defined.length > 0 ? defined : [defaultLanguage]

    return choices;

}
