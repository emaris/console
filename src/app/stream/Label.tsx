import { Icon, Tooltip } from 'antd'
import { useRoutableDrawer } from '#app/components/Drawer'
import { Label, LabelProps } from "#app/components/Label"
import { icns } from '#app/icons'
import { Language } from '#app/intl/model'
import { Document } from "#app/stream/model"
import { stripHtmlTags } from '#app/utils/common'
import { saveAs } from 'file-saver'
import moment from 'moment-timezone'
import { useT } from '#app/intl/api'
import { AiOutlineDownload, AiOutlineFullscreen } from 'react-icons/ai'
import { BsCardImage, BsInfo } from 'react-icons/bs'
import { useBytestreams } from "./api"
import { useDocument } from './hooks'


type DocumentLabelProps = LabelProps & {
    document: Document | undefined
    lang?: Language
    noDecorations?: boolean
}

export const DocumentLabel = (props: DocumentLabelProps) => {

    const t = useT()

    const { icon, title: text, description: tip, descriptor, availableLang, stream, link } = useDocument(props)

    const { style = {} } = props

    const streams = useBytestreams()

    const className = props.className ? `resource-name ${props.className}` : 'resource-name'


    const title = stream ?

        descriptor ?

            <a download={descriptor.file.name} href={URL.createObjectURL(descriptor.file)}>{text}</a>

            :

            <a download href={link}>{text}</a>


        :

        text

    const streamInfo = stream ?
        [<Tooltip title={
            <div className="fileInfo">
                <div className="container"><span className="lbl">{t("common.file.file_name")}:</span> <span className="info">{stream.name}</span></div>
                <div className="container"><span className="lbl">{t("common.file.file_language")}:</span> <span className="info">{availableLang.toUpperCase()}</span></div>
                <div className="container"><span className="lbl">{t("common.file.file_type")}:</span> <span className="info">{t(streams.getFileType(stream.type).name)}</span></div>
                <div className="container"><span className="lbl">{t("common.file.file_size")}:</span> <span className="info">{(stream.size / Math.pow(1024, 2)).toFixed(3)}Mb</span></div>
                {stream.lifecycle && <>
                    <div className="container"><span className="lbl">{t("common.file.file_last_updated")}:</span> <span className="info">{moment(stream.lifecycle.lastModified).format("YYYY, DD MMM")}</span></div>
                    <div className="container"><span className="lbl">{t("common.file.file_modified_by")}:</span> <span className="info">{stream.lifecycle.lastModifiedBy}</span></div>
                </>
                }
            </div>}>{icns.info}</Tooltip>] : []

    return <div style={{ display: 'flex', alignItems: 'center' }}>
        <Label
            noMemo
            className={className}
            style={style}
            icon={icon}
            options={streamInfo}
            noTip={tip ? false : true}
            tip={tip ? () => tip : undefined}
            title={title}
        />
        {!!stream || icns.error(t('document.no_content'))}
    </div>

}


export const ImageLabel = (props: DocumentLabelProps) => {

    const t = useT()

    const { descriptor, title: imgTitle, stream, link, description, noDecorations } = useDocument(props)

    const streams = useBytestreams()

    const { style = {} } = props

    const url = stream ? descriptor ? URL.createObjectURL(descriptor.file) : link : undefined

    const drawerTiltle = t('image.drawer.title')

    const { Drawer, open } = useRoutableDrawer({
        id: 'img-viewer',
        title: drawerTiltle,
        icon: <Icon component={BsCardImage} />
    })

    const title = <div className='img-container'>

        {stream ?

            <>
                <div>
                <img alt={stripHtmlTags(description as string)} style={style} src={url} className='img-source' />
                <div className="img-overlay">
                    <Tooltip title={t("image.controls.download")}>
                        <AiOutlineDownload className='download' onClick={() => saveAs(url!, imgTitle as string)} />
                    </Tooltip>
                    <Tooltip title={t("image.controls.full_screen")}>
                        <AiOutlineFullscreen className='fullscreen' onClick={() => open()} />
                    </Tooltip>

                    <Tooltip title={
                        <div className="fileInfo">
                            <div className="container"><span className="lbl">{t("common.file.file_name")}:</span> <span className="info">{stream.name}</span></div>
                            <div className="container"><span className="lbl">{t("common.file.file_type")}:</span> <span className="info">{t(streams.getFileType(stream.type).name)}</span></div>
                            <div className="container"><span className="lbl">{t("common.file.file_size")}:</span> <span className="info">{(stream.size / Math.pow(1024, 2)).toFixed(3)}Mb</span></div>
                            {stream.lifecycle && <>
                                <div className="container"><span className="lbl">{t("common.file.file_last_updated")}:</span> <span className="info">{moment(stream.lifecycle.lastModified).format("YYYY, DD MMM")}</span></div>
                            </>
                            }
                        </div>}><BsInfo className='info' /></Tooltip>
                </div>
                    {!noDecorations && <div className='img-caption'>{imgTitle}</div>}
                </div>
            </>

            :

            <div className="img-placeholder">
                <div className="icon">{<Icon component={BsCardImage} />}</div>
                <div className="text">{t('image.placeholder')}</div>
                <div className="attention">{icns.error(t('document.no_content'))}</div>
            </div>
        }
    </div>

    return <div style={{ display: 'flex', alignItems: 'center' }}>
        {title}
        <Drawer width={1000}>
            <img alt={imgTitle as string} src={url} />
        </Drawer>
    </div>

}