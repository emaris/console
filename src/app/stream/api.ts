
import { useCalls } from "#app/call/call";
import { useT } from '#app/intl/api';
import { wait } from "#app/utils/common";
import { showAndThrow } from "#app/utils/feedback";
import { saveAs } from "file-saver";
import { useToggleBusy } from "../system/api";
import { bytestreamType, FileType, fileTypes, nameSingular, unknownFileType } from "./constants";
import { Bytestream, Bytestreamed } from "./model";




const contentdispositionPattern = /filename="(?<filename>.*)"/


export const useBytestreams = () => {

    const toggleBusy = useToggleBusy()


    const t = useT()


    const bytestreams = "/stream"

    const { at, fq } = useCalls()

    const self = {


        linkOf: (stream: string) => fq(`${bytestreams}/${stream}`)

        ,

        download: (id: string) =>


            toggleBusy(`${bytestreamType}.download`, t("common.feedback.load", { plural: t(nameSingular).toLowerCase() })).then(

                () => at(`${bytestreams}/${id}`).rawget().then(response => {

                    saveAs(new Blob([JSON.stringify(response.data)], { type: response.headers['content-type'] }),
                        contentdispositionPattern.exec(response.headers['content-disposition'])?.groups?.filename)


                })

                    .catch(e => showAndThrow(e, t("bytestream.download_error")))
                    .finally(() => toggleBusy(`${bytestreamType}.download`))

            )

        ,

        // OLD PARALLEL VERSION: NO ATOMICITY, PLUS RACE-CONDITION ISSUES AT THE BACKEND (cf #913)
        // upload:<T extends Bytestreamed> (streamsAndBlobs: [T, Blob][], replace?: boolean) => {

        //     const post = replace ? false : true

        //     var count = streamsAndBlobs?.length ?? 0

        //     if (count === 0)
        //         return Promise.resolve([] as T[])

        //     return toggleBusy(`${bytestreamType}.upload`, t("bytestream.uploading", { count }))

        //         .then(wait(200))
        //         .then(

        //             () => Promise.all(streamsAndBlobs.map(([streamed, blob]) => {

        //                 const multipart = new FormData();

        //                 multipart.append('descriptor', new Blob([JSON.stringify(streamed.stream)], { type: "application/json" }))
        //                 multipart.append('stream', blob)

        //                 // send a single document
        //                 return post ? at(`${bytestreams}`).post<Bytestream[]>(multipart, { headers: { 'content-type': 'multipart/form-data' } })
        //                     .then(through(() => count--))
        //                     .then(through(() => {
        //                         if (count > 0) updateBusy(`${bytestreamType}.upload`, t("bytestream.upload_progress", { name: streamed.stream?.name, count }))
        //                     }))
        //                     .then(through(wait(200)))
        //                     .then(([stream]) => ({...streamed,stream} as T))
        //                 :
        //                     at(`${bytestreams}`).put<Bytestream[]>(multipart, { headers: { 'content-type': 'multipart/form-data' } })
        //                     .then(through(() => count--))
        //                     .then(through(() => {
        //                         if (count > 0) updateBusy(`${bytestreamType}.upload`, t("bytestream.upload_progress", { name: streamed.stream?.name, count }))
        //                     }))
        //                     .then(through(wait(200)))
        //                     .then(([stream]) => ({...streamed,stream} as T))

        //             }))


        //         )
        //         .catch(e => showAndThrow(e, t("bytestream.upload_error")))
        //         .finally(() => toggleBusy(`${bytestreamType}.upload`))

        // },

        // multi-file upload, atomic.
        upload: <T extends Bytestreamed>(streamsAndBlobs: [T, Blob][], replace?: boolean) => {

            const post = replace ? false : true

            var count = streamsAndBlobs?.length ?? 0

            if (count === 0)
                return Promise.resolve([] as T[])

            return toggleBusy(`${bytestreamType}.upload`, t("bytestream.uploading", { count }))

                .then(wait(200))
                .then(() => {


                    const multipart = new FormData();

                    streamsAndBlobs.forEach(([streamed, blob]) => {

                        multipart.append('descriptor', new Blob([JSON.stringify(streamed.stream)], { type: "application/json" }))
                        multipart.append('stream', blob)

                    })

                    return (post ?

                        at(`${bytestreams}`).post<Bytestream[]>(multipart, { headers: { 'content-type': 'multipart/form-data' } })

                        :
                        at(`${bytestreams}`).put<Bytestream[]>(multipart, { headers: { 'content-type': 'multipart/form-data' } })
                    )
                        .then(streams => streams.map((stream, i) => ({ ...streamsAndBlobs[i][0], stream } as T))




                        )
                })
                .catch(e => showAndThrow(e, t("bytestream.upload_error")))
                .finally(() => toggleBusy(`${bytestreamType}.upload`))

        },

        clone: async <T extends Bytestreamed>(sources: T[], ref: string) => {

            if (!sources.length)
                return sources

            toggleBusy(`${bytestreamType}.upload`, t("bytestream.uploading", { count: sources.length })).then(wait(200))

            try {
                const cloned = await at(`${bytestreams}/clone`).post<T[]>({ sources:sources.map(s=>s.stream), ref })

                return cloned.map((stream, i) => ({ ...sources[i], stream } as T))
            
            }
            catch(e:any) {

                throw showAndThrow(e, t("bytestream.upload_error"))
            }                
            finally {

                toggleBusy(`${bytestreamType}.upload`)
            }

    }
        ,


        // SEQUENTIAL VERSION: NO ATOMICITY, BUT SOLVES THE RACE-CONDITION ISSUES AT THE BACKEND (cf #913)
        // upload:<T extends Bytestreamed> (streamsAndBlobs: [T, Blob][], replace?: boolean) => {

        //     const post = replace ? false : true

        //     var uploaded : string[] = []
        //     var count = streamsAndBlobs?.length ?? 0

        //     if (count === 0)
        //         return Promise.resolve([] as T[])

        //     return toggleBusy(`${bytestreamType}.upload`, t("bytestream.uploading", { count }))

        //         .then(wait(200))
        //         .then(

        //             () => streamsAndBlobs.map(([streamed, blob]) => () => {

        //                 const multipart = new FormData();

        //                 multipart.append('descriptor', new Blob([JSON.stringify(streamed.stream)], { type: "application/json" }))
        //                 multipart.append('stream', blob)

        //                 // send a single document
        //                 return post ? at(`${bytestreams}`).post<Bytestream[]>(multipart, { headers: { 'content-type': 'multipart/form-data' } })
        //                     .then(through(() => {

        //                         uploaded.push(streamed.stream!.id)

        //                         return count--;
        //                     }))
        //                     .then(through(() => {
        //                         if (count > 0) updateBusy(`${bytestreamType}.upload`, t("bytestream.upload_progress", { name: streamed.stream?.name, count }))
        //                     }))
        //                     .then(through(wait(200)))
        //                     .then(([stream]) => ({...streamed,stream} as T))
        //                 :
        //                     at(`${bytestreams}`).put<Bytestream[]>(multipart, { headers: { 'content-type': 'multipart/form-data' } })
        //                     .then(through(() => count--))
        //                     .then(through(() => {
        //                         if (count > 0) updateBusy(`${bytestreamType}.upload`, t("bytestream.upload_progress", { name: streamed.stream?.name, count }))
        //                     }))
        //                     .then(through(wait(200)))
        //                     .then(([stream]) => ({...streamed,stream} as T))

        //             })
        //             .reduce((chain, task) =>  chain.then(results => task().then(result =>[ ...results, result ])), Promise.resolve([] as T[]))

        //         )
        //         .catch(e => showAndThrow(e, t("bytestream.upload_error")))
        //         .finally(() => toggleBusy(`${bytestreamType}.upload`))

        // },

        getFileType: (mime: string): FileType => {
    const ft = fileTypes.filter(info => info.mime.includes(mime))
    return ft.length > 0 ? ft[0] : unknownFileType
}

    }

return self
}