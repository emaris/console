import { LabelProps } from "#app/components/Label"
import { icns } from '#app/icons'
import { useCurrentLanguage } from '#app/intl/api'
import { defaultLanguage, Language } from '#app/intl/model'
import { Document, localisedIdOf, newDocument } from "#app/stream/model"
import * as React from 'react'
import { useT } from '#app/intl/api'
import { useBytestreams } from "./api"
import { useBytestreamedContext } from "./BytestreamedHelper"
import { iconFor } from "./FileBox"


type DocumentLabelProps = LabelProps & {
    document: Document | undefined
    lang?: Language
    noDecorations?: boolean
}

export const useDocument = (props: DocumentLabelProps) => {

    const t = useT()
    const currentLanguage = useCurrentLanguage()
    const streams = useBytestreams()
    const ctx = useBytestreamedContext() ?? {}

    const { document = newDocument(), lang = currentLanguage, noDecorations=false } = props

    const availableLang = document.streams[lang] ? lang : document.streams[defaultLanguage] ? defaultLanguage : Object.keys(document.streams).find(lang => document.streams[lang]) as Language

    const stream = document.streams[availableLang]

    const descriptor = ctx.descriptors.find(d => d.resource === localisedIdOf(document, availableLang))

    const icon = descriptor ? iconFor(descriptor.file.type) : stream ? iconFor(stream) : icns.file

    const baseText = document.title[lang] ?? document.title[availableLang] ?? stream?.name

    const title = baseText ?
        // if stream available only in a different language, decorate to say so.
        !availableLang || availableLang === lang || noDecorations ? baseText
            :
            <span>{baseText} <span style={{ color: 'lightgrey' }}>({availableLang.toUpperCase()})</span></span>

        : t("document.placeholder")

    const description = document.description?.[lang] ?? document.description?.[availableLang]

    const link = streams.linkOf(stream?.id!)

    const isUploaded = !descriptor

    return { icon, title, description, descriptor, availableLang, stream, link, isUploaded, noDecorations }
}
