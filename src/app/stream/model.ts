import { Language } from "#app/intl/model"
import { Lifecycle, newLifecycle } from "#app/model/lifecycle"
import { MultilangDto, newMultiLang } from "#app/model/multilang"
import shortid from "shortid"



export type Bytestream = {

    id: string
    name: string
    type: string
    size: number

    ref?: string
    tenant?: string

    lifecycle?: Lifecycle<string>

    properties: Record<string, any>

}

export const newBytestreamId = () => `BS-${shortid()}`

export const bytestreamFor = ({ name, type, size }: File): Bytestream => ({

    id: newBytestreamId(),
    name,
    type,
    size,
    properties: {}

})

export const bytestreamForBlob = (name: string, { type, size }: Blob): Bytestream => ({

    id: newBytestreamId(),
    name,
    type,
    size,
    properties: {}

})

export type BytestreamChange = {

    mode: 'upload' | 'uploadRemoval'
    file?: File

}


export type FileDescriptor = {

    resource: string
    file: File
}

export const descriptorFrom = (file: File, r: Bytestream): FileDescriptor => ({ file, resource: r.id })


export type Bytestreamed = {

    id: string
    stream: Bytestream | undefined
}

export type DocumentState = "active" | "inactive"

// a document with metadata and mutable stream content
export type Document = {

    id: string

    title: MultilangDto
    description?: MultilangDto

    lifecycle: Lifecycle<DocumentState>

    streams: {
        [lang in Language]?: Bytestream
    }

}


export type DocumentLocalisedStream = Bytestreamed & {
    lang: Language
}

export const localisedIdOf = (d: Document, lang: Language) => `${d.id}-${lang}`
export const localisedStreamOf = (d: Document, lang: Language): DocumentLocalisedStream => ({ id: localisedIdOf(d, lang), lang, stream: d.streams[lang] })
export const localisedStreamsOf = (d: Document) => Object.keys(d.streams).map(lang => localisedStreamOf(d, lang as Language))

export type DocumentChange = BytestreamChange & {

    lang: Language

}

export const newDocument = (): Document => ({

    id: `D-${shortid()}`,
    title: newMultiLang(),
    description: newMultiLang(),
    lifecycle: newLifecycle('inactive'),
    streams: { 'en': undefined! }
})