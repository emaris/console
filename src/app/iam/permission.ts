import { ChangesDto } from "./calls"
import { Action, ActionDto, compare, idOf, impliedByAny, useActions } from "./model"

export type PermissionDto = {

    subject: string
    action: ActionDto

}

export type Permission = {

    subject: string
    action: Action
}


export const withPermission = (self: Permission) => ({

    ...self,

    id: `${self.subject}:${idOf(self.action)}`

})


export type ChangesProps = {

    permissions: Permission[]
    initial: Permission[]
    onChange: (_: Permission[]) => any

}



type IdMap = { [id: string]: boolean }



export const usePermissions = () => {

    const actions = useActions()

    const self =  {


        intern: ({ subject, action }: PermissionDto): Permission => {
            return ({ subject, action: actions.intern(action) })
        },

        extern: ({ subject, action }: Permission): PermissionDto => {
            return ({ subject, action: actions.extern(action) })
        },

        idmapOf: (ps: Permission[]): IdMap => ps.reduce((acc, p) => ({ [withPermission(p).id]: true, ...acc }), {}),

        externChanges: (current: Permission[], initial: Permission[]): ChangesDto => {

            const currentMap = self.idmapOf(current);
            const initialMap = self.idmapOf(initial);

            const granted = current.reduce((acc, p) => initialMap[withPermission(p).id] ? acc : [p, ...acc], [] as Permission[])
            const revoked = initial.reduce((acc, p) => currentMap[withPermission(p).id] ? acc : [p, ...acc], [] as Permission[])

            return { ...(granted.length > 0 ? { granted: granted.map(p => self.extern(p)) } : {}), ...(revoked.length > 0 ? { revoked: revoked.map(p => self.extern(p)) } : {}) }

        },

        reconcile: (permissions: Permission[]): Permission[] => {

            // sort upfront from most generic to most specific, so that reconciliation can be treated in 'streaming' fashion
            const sorted = [...permissions]
            sorted.sort((p1, p2) => compare(p1.action, p2.action))

            //  simply add one by one if not already implied by previous ones
            const outcome = sorted.reduce((acc, p) => impliedByAny(p.action, acc.filter(pp => p.subject === pp.subject).map(p => p.action)) ? acc : [p, ...acc], [] as Permission[])

            //console.log("to reconcile:",permissions,"reconciled:", outcome);

            return outcome;

        }

    }

    return self

}


