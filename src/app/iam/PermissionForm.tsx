
import { useT } from '#app/intl/api';
import { ReadOnlyContext } from '#app/scaffold/ReadOnly';
import { paramsInQuery, updateQuery } from "#app/utils/routes";
import * as React from "react";
import { useHistory, useLocation } from "react-router-dom";
import { Button } from "../components/Button";
import { Drawer } from "../components/Drawer";
import { Placeholder } from "../components/Placeholder";
import { Form } from "../form/Form";
import { FormState, useFormState } from "../form/hooks";
import { icns } from "../icons";
import { Topbar } from "../scaffold/Topbar";
import { useAsyncRender } from "../utils/hooks";
import { PermFilter, usePermissionCalls } from "./calls";
import { iamPlural } from "./constants";
import { ChangesProps, Permission, usePermissions } from "./permission";

type ProxyProps = {

    resourceCentric?:boolean
    width?: number
    filter?: PermFilter
    
}

export type ModalFormProps = ChangesProps & {

    state: FormState<Permission[]>
}

export const usePermissionDrawer = <T extends Object> (Compo: (_: T & ModalFormProps) => any , defaultFilter?:PermFilter) => {

   const history = useHistory()
   const {pathname,search} = useLocation()
   const visible = !!paramsInQuery(search)[permissionParam]

   // shortcuts rendering if drawer ain't visible, so that clients don't need to worry about guarding when they mount the form.
   // if the drawer is visible it memoises the form so that it preserves the load state (without entering a loop).  
    // eslint-disable-next-line
   const Formmemo = React.useCallback( visible ? withDrawerForm(Compo,defaultFilter) : ()=>null,[visible])

   return [Formmemo,()=>history.push(`${pathname}?${updateQuery(search).with(params=>params[permissionParam]='true' )}`),visible] as [any,()=>void,boolean]

}

export const permissionParam="permission-drawer"

export const withDrawerForm = <T extends Object> ( Compo: (_: T & ModalFormProps) => any , defaultFilter?:PermFilter ) => ( props: T & ProxyProps) => {

    const t = useT()

    const {width=800} = props

    const history = useHistory()
    const {pathname,search} = useLocation()

    const readonly = React.useContext(ReadOnlyContext)

    const perms = usePermissions()

    const visible = !!paramsInQuery(search)[permissionParam]
    const onClose = () => history.push(`${pathname}?${updateQuery(search).with(params=>params[permissionParam]=null )}`)

    const calls = usePermissionCalls()

    const formstate = useFormState<Permission[]>(undefined!) 
    
    const [transitionFinished,setTransitionFinished] = React.useState(false)

    const { edited, initial, dirty, reset, set, softReset } = formstate

    const isReady = !!edited

    const filter = {...defaultFilter,...props.filter}
   
    const save = ()=> calls.update(perms.externChanges(edited,initial)).then(_=>softReset(undefined!))

    const revert = <Button icon="undo" enabled={dirty} onClick={()=>reset()}>Revert</Button>
    const saveact = <Button icon="save" enabled={dirty} type="primary" onClick={save}>Save</Button> 

    const [render] = useAsyncRender({
        id:"loadpermissions",
        when: isReady,
        runWhen: visible && transitionFinished,
        task: () => calls.search(filter).then(softReset),
        placeholder: Placeholder.none,
        content: <React.Fragment>

                    <Topbar autoGroupButtons={false} offset={65}>
                        {saveact}
                        {revert}
                    </Topbar>

                    <Form state={formstate}>
                        <Compo {...props} state={formstate} permissions={edited} initial={initial} onChange={set} />
                    </Form>

                </React.Fragment>
    })

    return  <Drawer readonly={readonly} icon={icns.permissions} routeId={permissionParam} title={t(iamPlural)} warnOnClose={dirty} width={width} visible={visible} onClose={onClose} afterVisibleChange={setTransitionFinished} >
                {render}
            </Drawer>

}
