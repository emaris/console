
import { icns } from "#app/icons"
import { useT } from '#app/intl/api'
import { Empty, Switch, Tooltip } from "antd"
import * as React from "react"
import { Column, VirtualTable } from "../components/VirtualTable"
import { FieldRow } from "../form/FieldRow"
import { FormContext } from "../form/Form"
import { VSelectBox } from "../form/VSelectBox"
import { ReadOnlyContext } from "../scaffold/ReadOnly"
import { tenantActions } from "../tenant/actions"
import { noTenant } from "../tenant/constants"
import { hasTenant, TenantResource, useTenantResourceType } from "../tenant/model"
import { User, useUser } from "../user/model"
import { Action, any, impliedByAny, likeAny, specialise } from "./model"
import { ChangesProps, Permission, withPermission, usePermissions } from "./permission"
import "./styles.scss"




// works generically with subjects and resources, though users are only subjects in view for now.
// works embedded in forms or outside forms, through only embeddings are in view for now.
type Props<S, R> = SubjectProps<S> & ResourceProps<R> & StateProps<S, R> & PermissionProps & { id: string }  // id needed for embeddings 

//  component can manage its own state, whether the state is held internally or in external forms.
//  state can be be "controlled" by clients by providing only 1) initial values, or 2) values at each render.
export type StateProps<S, R> = {

        logged: User

        subjects?: S[]   // if set, component is controlled by client
        defaultSubjects?: S[]
        setSubjects?: (subjects: S[]) => void

        resources?: R[]   // if set, component is controlled by client
        defaultResources?: R[]
        setResources?: (subjects: R[]) => void
}


//  the component needs to present information extracted from subjects, and check their privileges.
export type SubjectProps<S> = {

        //  the subjects to consider for the cartesian product.
        subjectRange: S[]

        //  indicates subjects are known from the context, cannot be selected and don't show in rows. 
        //  if omitted, it is inferred from a single-subject range.
        subjectCentric?: boolean

        subjectSingular: string;
        subjectPlural: string;
        subjectId: (t: S) => any
        subjectText: (t: S) => string
        renderSubject?: (t: S) => any
        renderSubjectOption: (t: S) => any


}

//  the component needs to present information extracted from resources.
export type ResourceProps<R> = {

        //  the resources to consider for the cartesian product.
        resourceRange: R[]

        //  indicates resources are known from the context, cannot be selected and don't show in rows. 
        //  if omitted, it is inferred from a single-resource range.
        resourceCentric?: boolean

        resourceSingular: string
        resourcePlural: string

        resourceType: string
        resourceText: (t: R) => string
        resourceId: (t: R) => any
        renderResource?: (t: R) => any
        renderResourceOption: (t: R) => any
        sortResources?: (t1: R, t2: R) => number



        includeAll?: boolean
}


//  the component needs to plot permissions to performe a fixed set of actions
export type PermissionProps = ChangesProps & {


        actions: Action[]

}


// public component: choose state-management strategy based on context. 
export const PermissionTable = <S extends Object, R extends Object>(props: Props<S, R>) => {

        const form = React.useContext(FormContext)

        return form ? EmbeddedPermissionTable(props) : StandalonePermissionTable(props)

}

//  keeps state in parent form (as form aux state) and 
const EmbeddedPermissionTable = <S extends Object, R extends Object>(props: Props<S, R>) => {

        const { id, defaultSubjects = [], subjects, setSubjects = () => { }, resources, defaultResources = [], setResources = () => { } } = props

        const { auxstate, auxchange } = React.useContext(FormContext)

        const subjectsId = `${id}-subjects`
        const resourcesId = `${id}-resources`

        const [storedSubjects, setStoredSubjects] = [auxstate[subjectsId] as S[] || defaultSubjects, auxchange((t, v) => t[subjectsId] = v)]
        const [storedResources, setStoredResources] = [auxstate[resourcesId] as R[] || defaultResources, auxchange((t, v) => t[resourcesId] = v)]

        const setSubjectsWrapper = (ts: S[]) => { setStoredSubjects(ts); setSubjects(ts) }
        const setResourcesWrapper = (ts: R[]) => { setStoredResources(ts); setResources(ts) }

        return PurePermissionTable({
                ...props,
                subjects: subjects || storedSubjects,
                setSubjects: setSubjectsWrapper,
                resources: resources || storedResources,
                setResources: setResourcesWrapper
        })


}

const StandalonePermissionTable = <S extends Object, R extends Object>(props: Props<S, R>) => {

        const {
                defaultSubjects = [], subjects, setSubjects = () => { },
                defaultResources = [], resources, setResources = () => { }

        } = props

        const [storedSubjects, setStoredSubjects] = React.useState(defaultSubjects)
        const [storedResources, setStoredResources] = React.useState(defaultResources)

        const setSubjectsWrapper = (us: S[]) => { setStoredSubjects(us); setSubjects(us) }
        const setResourcesWrapper = (ts: R[]) => { setStoredResources(ts); setResources(ts) }


        return PurePermissionTable({
                ...props,
                subjects: subjects || storedSubjects,
                setSubjects: setSubjectsWrapper,
                resources: resources || storedResources,
                setResources: setResourcesWrapper
        })

}


type PureProps<S, R> = SubjectProps<S> & ResourceProps<R> & PermissionProps & {

        logged: User

        disabled?: boolean

        subjects: S[]
        setSubjects: (ts: S[]) => void

        resources: R[]
        setResources: (ts: R[]) => void
        includeAll?: boolean
}




export const PurePermissionTable = <S extends Object, R extends Object>(initialprops: PureProps<S, R>) => {

        const props = usePrepareProps(initialprops)
        const permissionapi = usePermissions()

        const t = useT()

        const grant = (permissions: Permission[], p: Permission) => {
                //console.log("granting",p,"given",permissions)
                return permissionapi.reconcile([p, ...permissions])
        }

        const revoke = (permissions: Permission[], p: Permission) => {
                // console.log("revoking",p)
                const index = permissions.findIndex(pp => withPermission(p).id === withPermission(pp).id)
                if (index >= 0) {
                        const copy = [...permissions]
                        copy.splice(index, 1)
                        return permissionapi.reconcile(copy)
                }
                return permissions
        }

        const { disabled } = props
        const readOnlyContext = React.useContext(ReadOnlyContext)
        const readOnlyState = disabled || readOnlyContext


        const table = useTableFor(props)

        if (props.resourceRange.length === 0 || props.subjectRange.length === 0)
                return <Empty image={Empty.PRESENTED_IMAGE_SIMPLE} style={{ marginBottom: 50 }} description={t("iam.feedback.empty_msg")} />



        const options = remainingOptions(table, props)

        const subjectComparator = (s1: S & any, s2: S & any) => s1 === any ? s1 : s2 === any ? s2 : props.subjectText(s1).localeCompare(props.subjectText(s2))
        const resourceComparator = (r1: S & any, r2: S & any) => r1 === any ? r1 : r2 === any ? r2 : props.resourceText(r1).localeCompare(props.resourceText(r2))

        return <div className="permission-table" style={{ display: "flex", flexDirection: "column", marginBottom: "20px" }}>

                <FieldRow standalone >

                        {props.subjectCentric || (props.resourceCentric && options.subjects.length === 0) ||

                                <VSelectBox grouped mode="multiple" placeholder={t("iam.feedback.select_msg", { plural: props.subjectPlural.toLowerCase() })}
                                        renderOption={props.renderSubjectOption}
                                        optionId={props.subjectId}
                                        style={{ flex: 1, margin: props.subjectCentric ? 0 : "10px 2px" }}
                                        onChange={props.setSubjects}
                                        options={options.subjects.sort(subjectComparator)}
                                        lblTxt={s => s === any ? t('iam.labels.all', { plural: props.subjectPlural }) : props.subjectText(s)}>

                                        {props.subjects}

                                </VSelectBox>
                        }

                        {props.resourceCentric || (props.subjectCentric && options.resources.length === 0) ||

                                <VSelectBox grouped mode="multiple" placeholder={t("iam.feedback.select_msg", { plural: props.resourcePlural.toLowerCase() })}
                                        renderOption={props.renderResourceOption}
                                        optionId={props.resourceId}
                                        style={{ flex: 1, margin: props.resourceCentric ? 0 : "10px 2px" }}
                                        onChange={props.setResources}
                                        options={options.resources.sort(resourceComparator)}
                                        lblTxt={r => r === any ? t(`iam.labels.all_${props.resourceType}`) : props.resourceText(r)}>

                                        {props.resources}

                                </VSelectBox>
                        }
                </FieldRow>


                <VirtualTable fixedHeight filtered={false} selectable={false} rowKey="key" state={null!} data={table} emptyMessage={t("iam.feedback.empty_msg")}

                        sortBy={[['new', 'asc'], ['name', 'asc']]}
                        rowClassName={({ rowData: row }) => `permission-row ${(row.isnew && row.resource !== any) ? "new-perm" : ""}`}  >

                        <Column<Row> dataKey="new" title="new"
                                hidden dataGetter={({ isnew, resource }) => resource === any ? "b" : isnew ? "a" : "c"} />

                        <Column<Row> dataKey="name" title="name"
                                hidden dataGetter={({ name }) => name} />

                        <Column<Row> dataKey="subject" title={props.subjectSingular} flexGrow={1}
                                hidden={props.subjectCentric} sortable={false}
                                dataGetter={({ subject }) => subject === any ? t('iam.labels.all', { plural: props.subjectPlural }) : props.renderSubject!(subject)} />

                        <Column<Row> dataKey="resource" title={props.resourceSingular} flexGrow={1}
                                hidden={props.resourceCentric} sortable={false}
                                dataGetter={({ resource }) => resource === any ? t(`iam.labels.all_${props.resourceType}`) : props.renderResource!(resource)} />




                        {props.actions.map((a, i) => {

                                const name = t(a.shortName || a.name)

                                //  custom 'permissions' props forces refresh of row whenever permissions change and dataGetter fetches it from there.
                                //  standard closure wouldn't work here, as row wouldn't be refreshed.

                                return <Column<Row> width={120} permissions={props.permissions} key={i} dataKey={name} sortable={false} style={{ justifyContent: "center" }} title={name}
                                        headerRenderer={() => a.description ? <Tooltip title={t(a.description)}><span>{name}</span></Tooltip> : <span>{name}</span>}

                                        dataGetter={(row, col) => {

                                                const disabled = (readOnlyState || row[a.name].disabled)
                                                const readoOnlyIcon = disabled && icns.readonly(row[a.name].disabledMessage, { color: "lightgrey", top: -7, left: -16, position: "absolute" })

                                                return <div style={{ display: 'flex', alignItems: 'center', justifyContent: 'center' }}>

                                                        <div style={{ position: "relative" }}>{readoOnlyIcon}</div>
                                                        <Switch key={i} disabled={disabled}

                                                                onClick={value => {

                                                                        const p = { subject: row.subject, action: row[a.name].action }

                                                                        props.onChange(value ? grant(col.permissions, p) : revoke(col.permissions, p))

                                                                }} checked={row[a.name].active} />
                                                </div>

                                        }


                                        } />
                        })}

                </VirtualTable>



        </div>



}

type Row = { key: string, subject: any, resource: any, name: string, isnew: boolean, disabledMessage: string | undefined }

const retype = <T extends any>(t: T) => (t as any) as T & TenantResource

const usePrepareProps = <S, R>(props: PureProps<S, R>) => {

        const t = useT()

        // filters resources and subjects by the tenants they have in common in a bid to reduce or eliminate altogether 
        // combinations that are useless because go across tenants.

        const retypedResources = (props.resourceRange || []).map(retype)
        const resourceTenants = Array.from(new Set(retypedResources.filter(r => r.tenant).map(r => r.tenant)))

        const retypedSubjects = (props.subjectRange || []).map(retype)
        const subjectTenants = Array.from(new Set(retypedSubjects.filter(r => r.tenant).map(r => r.tenant)))

        const tenantsInScope = resourceTenants.length > 0 && subjectTenants.length > 0 && subjectTenants.filter(t => resourceTenants.includes(t))

        const resourcesInScope = tenantsInScope && !props.resourceCentric ? retypedResources.filter(r => tenantsInScope.includes(r.tenant)) : props.resourceRange
        const subjectsInScope = tenantsInScope && !props.subjectCentric ? retypedSubjects.filter(s => tenantsInScope.includes(s.tenant)) : props.subjectRange

        const {
                resourceCentric,
                includeAll = !resourceCentric                // by default, included when resources are explicit.      

        } = props
        // sets defaults
        const prepared = { ...props, includeAll, subjectRange: subjectsInScope, resourceRange: resourcesInScope }

        prepared.resourceRange = [...prepared.resourceRange]
        if (props.sortResources) {
                prepared.resourceRange.sort(props.sortResources)
        }

        const tForAll = props['id'] ?
                props['id'] === 'requirement-permissions' ? t('iam.feedback.perm_all_resources_requirements')
                        : props['id'] === 'product-permissions' ? t('iam.feedback.perm_all_resources_products')
                                : props['id'] === 'campaign-permissions' ? t('iam.feedback.perm_all_resources_campaigns')
                                        : props['id'] === 'event-permissions' ? t('iam.feedback.perm_all_resources_events')
                                                : props['id'] === 'tenants-permissions' ? t('iam.feedback.perm_all_resources_tenants')
                                                        : t('iam.feedback.perm_all_resources', { plural: t(props.resourcePlural) })
                : t('iam.feedback.perm_all_resources', { plural: t(props.resourcePlural) })

        if (includeAll) {

                prepared.resourceRange = [any, ...prepared.resourceRange] as R[]
                prepared.renderResourceOption = (r: R | string) => r === any ? tForAll : props.renderResourceOption(r as R)
                prepared.resourceId = (r: R | string) => r === any ? any : props.resourceId(r as R)
        }

        if (prepared.resourceRange.length === 1 && prepared.resources.length === 0)
                prepared.resources = [...prepared.resourceRange]


        if (prepared.subjectRange.length === 1 && prepared.subjects.length === 0)
                prepared.subjects = [...prepared.subjectRange]


        return prepared

}

// excludes subjects/resources that have been already selected.
// but keep them, if they can still be combined with unselected ones.

const remainingOptions = <S, R>(table: Row[], props: PureProps<S, R>) => {

        // avoids option filtering in the NxM case, provided there are subjects or resources left to combine with those already rendered.
        const caseNxM = !props.subjectCentric && !props.resourceCentric
        const keepFullSubjectRange = caseNxM && props.resourceRange.length !== props.resources.length
        const keepFullResourceRange = caseNxM && props.subjectRange.length !== props.subjects.length

        if (keepFullSubjectRange && keepFullResourceRange)
                return { subjects: props.subjectRange, resources: props.resourceRange }

        const tableSubjects = table.map(row => row.subject)
        const subjects = props.subjectRange.filter(s => !tableSubjects.includes(props.subjectId(s)))

        const tableResources = table.map(row => row.resource)
        const resources = props.resourceRange.filter(r => !tableResources.includes(props.resourceId(r)))

        return { subjects, resources }
}


const useTableFor = <S, R>(props: PureProps<S, R>) => {

        const tenantResourceType = useTenantResourceType()
        const withUser = useUser()
        const ppermissions = usePermissions()

        const t = useT()

        const { subjectRange, renderSubjectOption, renderResourceOption, initial, subjects, resources, resourceCentric, resourceId, resourceRange, permissions, subjectId, actions } = props

        const permissionIds = ppermissions.idmapOf(permissions)

        const initialPermissionIds = ppermissions.idmapOf(initial)

        const loggedUser = withUser(props.logged)

        const table = subjectRange.flatMap(s => rowsFor(s)).filter(r => r.visible) as Row[];

        return table;


        // helper functions are hoisted 
        function rowFor(subject: any, subjectName: string, r: R, subjectActions: Action[], initialActions: Action[]) {

                const resource = resourceId(r)
                const tenant = (hasTenant(r) && r.tenant) || noTenant
                const passthrough =
                        tenantResourceType(props.resourceType) &&
                        subject?.tenant &&
                        likeAny(tenantActions.manage, subjectActions)  // no specific tenant
                const initiallyPassthrough =
                        tenantResourceType(props.resourceType) &&
                        subject?.tenant &&
                        impliedByAny(specialise(tenantActions.manage, tenant), initialActions)

                const resourceName = renderResourceOption(r)

                return actions.map(a => specialise(a, resource))
                        .reduce((acc, a) => {


                                const permission = withPermission({ subject, action: a })
                                const contained = permissionIds[withPermission(permission).id]
                                const active = contained || passthrough || impliedByAny(a, subjectActions)

                                // disabled if virtual, implied, own, passthrough, or forbidd
                                const disabledTest = ([

                                        [() => loggedUser.username === subject, t("iam.feedback.perm_locked_your_own")],
                                        [() => !!a.virtual, t("iam.feedback.perm_locked_virtual")],
                                        [() => passthrough, t("iam.feedback.perm_locked_passthrough")],
                                        [() => active && !contained, t("iam.feedback.perm_locked_implied")],
                                        [() => !loggedUser.can(a), t("iam.feedback.perm_locked_unauthorized")]

                                ] as [() => boolean, string][]).find(test => test[0]())


                                const disabled = !!disabledTest
                                const disabledMessage = disabledTest && disabledTest[1]


                                const subjectSelected = props.subjectCentric ? true : subjects.find(s => subjectId(s) === subject)
                                const resourceSelected = props.resourceCentric ? true : resources.find(r => resourceId(r) === resource)
                                const selected = subjectSelected && resourceSelected

                                const initiallyContained = initialPermissionIds[withPermission(permission).id]
                                const initiallyActive = initiallyPassthrough || impliedByAny(a, initialActions);

                                // does this cell make the row visible? 

                                const visible = acc.visible ||
                                        !!selected              // a) the user wants to see it
                                        || contained             // b) shows current state
                                        || initiallyContained    // c) clarifies next state...
                                        // ...and required when 'demoting' (mustn't disappear mid-flight)

                                        // resource-centric, we must also show users with permissions implied by templates
                                        // or we lose info (in subject-centric we mustn't, it'd be redundant)
                                        || (!!resourceCentric && (active || initiallyActive))



                                const isnew = acc.isnew || !initiallyActive

                                return { ...acc, visible, isnew, [a.name]: { action: a, active, disabled, disabledMessage, tenant } }

                        }, { key: `${subject}:${resource}`, subject, resource, name: `${subjectName}:${resourceName}`, isnew: false, visible: false, disabledMessage: undefined })

        }

        function rowsFor(s: S) {

                const subject = subjectId(s)
                const subjectActions = permissions.filter(p => p.subject === subject).map(p => p.action)
                const initialActions = initial.filter(p => p.subject === subject).map(p => p.action)

                return resourceRange.map(r => rowFor(subject, renderSubjectOption(s), r, subjectActions, initialActions))
        }

}

