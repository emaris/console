
import { useT } from '#app/intl/api';
import { useCalls } from "../call/call";
import { useToggleBusy } from "../system/api";
import { notify, showAndThrow } from "../utils/feedback";
import { iamPlural } from "./constants";
import { Permission, PermissionDto, usePermissions } from "./permission";

const svc="admin"
const permissions = "/permission"

export type PermFilter = {

    subjects?: String[]
    actions?:String[]
    types?:String[]
    resources?:String[]
}


export type ChangesDto = {

    granted?: PermissionDto[]
    revoked?:PermissionDto[]
}



export const usePermissionCalls = () => {

   const toggleBusy = useToggleBusy()
   
   const t = useT()

   const calls = useCalls()
   const perms = usePermissions()

   const type="permissions"
   const plural = t(iamPlural.toLowerCase())

   return {

        search: (filter:PermFilter) :  Promise<Permission[]> =>  
        
            toggleBusy(`${type}.search`,t("common.feedback.load",{plural}))

                .then(_=>Promise.resolve(console.log(`fetching permissions...`)))
                .then(_=>calls.at(`${permissions}/search`,svc).post<PermissionDto[]>(filter))
                .then(ps=>ps.map(perms.intern))
                .then(perms.reconcile)
        
            .catch( e => showAndThrow(e,t("common.calls.fetch_all_error",{plural})))
            .finally(() => toggleBusy(`${type}.search`))
                            
        , 
       
        update: (changes:ChangesDto)  =>  
        
            toggleBusy(`${type}.update`,t("common.feedback.save_changes"))
             
                .then(_=> calls.at(permissions,svc).put(changes))
                .then(()=>notify(t('common.feedback.saved')))
        
        
             .catch( e=> showAndThrow(e,t("common.calls.save_many_error",{plural})))
             .finally(() => toggleBusy(`${type}.update`))
        

    }
}