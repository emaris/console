import { icns } from "../icons";
import { any } from "./model";
import { Role } from "./roles";


//  common rights

export const standardActions = { 
    
    doAnything : {
        type : any,
        icon: icns.permissions,
        name: "iam.actions.do_anything.name",
        description: "iam.actions.do_anything.description",
        virtual:false,
        labels : [ any ]
    },

    doNothing : {
        type : any,
        icon: icns.lock(),
        name: "iam.actions.do_nothing.name",
        description: "iam.actions.do_nothing.description",
        virtual:true,
        labels : [ ]
    }
}







export const standardRoles : {[id:string]:Role} = {
    
    admin: {
        id:"admin",
        name:"iam.roles.admin.name",
        description:"iam.roles.admin.description", 
        icon:icns.admin,
        virtual:false,
        permissions : [ standardActions.doAnything ]
    } ,

    user: {
        id:"user",name:"iam.roles.user.name",description:"iam.roles.user.description", icon:icns.reguser,
        virtual:true,
        permissions : []
    } ,

    guest: {
        id:"guest",name:"iam.roles.guest.name",description:"iam.roles.guest.description", icon:icns.guest,
        virtual:true,
        permissions : [standardActions.doNothing ]
    } 
    
}