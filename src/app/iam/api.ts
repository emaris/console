import { useActions } from "./model"
import { usePermissions } from "./permission"

export const useIam = () => ({

    actions: useActions,
    permissions: usePermissions,



})