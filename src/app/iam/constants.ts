import { icns } from "../icons"


export const iamSingular = "iam.module.name_singular"
export const iamPlural = "iam.module.name_plural"

export const iamType="iam"
export const iamIcon=icns.permissions