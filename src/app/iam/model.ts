import { useModuleRegistry } from '#app/module/registry';
import { ReactNode } from "react";
import { icns } from "../icons";
import { iamType } from "./constants";
import { IamSlot } from "./module";



//  types

export const any = "*"
export const notype = "app"


// data transfer object
export type ActionDto = {

    labels: string[]
    resource?: string
    type: string

}

// enriched local object
export type Action = ActionDto & {

    name: string
    shortName?: string
    icon: ReactNode
    description: string
    virtual?: boolean
    scope?: string
    actionType?: 'admin' | 'tenant'
}




//  helpers

export const concat = (type: string, labels: string[], resource: string = any) => `${type}:${labels.join(":")}:${resource}`




// apis

export const useActions = () => {

    const registry = useModuleRegistry()

    const self = {

        all: () => registry.allSlotsWith<IamSlot>(iamType).flatMap(slot => Object.values(slot.actions)).filter(r => !r.virtual),

        intern: (dto: ActionDto): Action => {

            const actionid = concat(dto.type, dto.labels)
            
            const resolved = self.all().find(a => idOf(a) === actionid)

            const merged: Action | undefined = resolved && { ...resolved, resource: dto.resource }

            const unknownAction: Action = { ...dto, icon: icns.unknown, name: actionid, description: "unknown" }

            return merged || unknownAction
        },

        extern: ({ type, labels, resource }: Action): ActionDto => ({ type, labels, resource: (resource || any) }),

        reconcile: <T extends ActionDto>(actions: T[]): T[] => {

            // sort upfront from most generic to most specific, so that reconciliation can be treated in 'streaming' fashion
            const sorted = [...actions]

            sorted.sort(compare)

            //  simply add one by one if not already implied by previous ones
            return sorted.reduce((acc, a) => impliedByAny(a, acc) ? acc : [a, ...acc], [] as T[])

        }

    }

    return self;

}

export const idOf = ({ type, labels, resource }: ActionDto) => concat(type, labels, resource)

export const specialise = (a: Action, resource: string): Action => ({ ...a, resource })

// matching labels
const matchLabels = (a1: ActionDto, a2: ActionDto) => !a2.labels.some((lbl, i) => lbl !== any && lbl !== a1.labels[i])

// matching and types
export const match = (a1: ActionDto, a2: ActionDto) => {

    const matchtype = (a1.type === a2.type || a2.type === any || !a2.type)

    // matching type: same (including both any or missing), or action's is any or missing
    return matchtype && matchLabels(a1, a2)
}

// matching labels and types and resources
export const impliedBy = (a1: ActionDto, a2: ActionDto) => match(a1, a2) && (a1.resource === a2.resource || a2.resource === any || a2.resource === undefined)

export const impliedByAny = (a: ActionDto, actions: ActionDto[]) => actions.some(aa => impliedBy(a, aa))

export const likeAny = (a: ActionDto, actions: ActionDto[]) => actions.some(aa => match(a, aa))

const text = (a: ActionDto) => `${idOf(a)}:${a.resource}`

export const compare = (a1: ActionDto, a2: ActionDto) => impliedBy(a1, a2) ? impliedBy(a2, a1) ? 0 : 1 : impliedBy(a2, a1) ? -1 : text(a2).localeCompare(text(a1))




