import { ReactNode } from "react";
import { Action, ActionDto } from "./model";


export type RoleDto = {
    id: string
    name: string
    permissions: ActionDto[]
}

export type Role = RoleDto & {

    icon: ReactNode
    description: string
    virtual: boolean
    permissions: Action[]

}