import { Module } from "../module/model"
import { standardActions } from "./actions"
import { iamIcon, iamPlural, iamSingular, iamType } from "./constants"
import { Action } from "./model"


export const iammodule : Module = {

    icon: iamIcon,
    type: iamType,
    nameSingular: iamSingular,
    namePlural: iamPlural,

    // uses it own slow to register standard actions and roles
    [iamType]: {
       
          actions: standardActions
    
    }  as IamSlot

}

export type IamSlot = {

    actions: { [key: string] : Action }

}

