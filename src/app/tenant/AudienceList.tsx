import { Select } from 'antd'
import { Field, FieldProps } from '#app/form/Field'
import { FieldRow } from '#app/form/FieldRow'
import { ReadonlyProps, useReadonly } from '#app/form/ReadonlyBox'
import { SelectBox } from '#app/form/SelectBox'
import { useT } from '#app/intl/api'
import { useLocale } from '#app/model/hooks'
import * as React from 'react'
import { TenantLabel } from './Label'
import { useTenantStore } from './store'
import "./styles.scss"

export type AudienceInclusions = 'includes' | 'excludes'

export type TenantAudience = {
    [key in AudienceInclusions]: string[]
}

type Props = FieldProps & ReadonlyProps & {
    onChange: (_: TenantAudience | undefined) => void
    children?: TenantAudience
}

export const AudienceList = (props: Props) => {

    const { children, onChange, ReadOnlyBox, ...rest } = useReadonly(props)

    const t = useT()
    const { l } = useLocale()

    const tenants = useTenantStore()
    const allTenants = tenants.allSorted()

    const defaultKeyValue = Object.keys(children ?? {}).find(c => children?.[c].length > 0) ?? 'includes'

    const [currentKey, setCurrentKey] = React.useState<AudienceInclusions>(defaultKeyValue as AudienceInclusions)

    const onChangeIncusion = (inclusion: AudienceInclusions) => {
        setCurrentKey(inclusion)
        const otherKey = (inclusion === 'includes' ? 'excludes' : 'includes') as AudienceInclusions
        const currentVal = children?.[otherKey] ? children[otherKey] : []

        if (currentVal)
            onChange({ [otherKey]: [], [inclusion]: currentVal } as TenantAudience)
        else
            onChange({ [inclusion]: [] as string[] } as TenantAudience)
    }

    const onChangeTenants = (tenants: string[]) => onChange({ ...children, [currentKey]: tenants } as TenantAudience)

    return <Field {...rest}>

        <FieldRow>

            <ReadOnlyBox className='audience-list-selector' light grouped disabled={props.disabled}  >
                <Select  dropdownClassName={`audience-list-selector ${rest.readonly ? 'readonly' : ''}`}
                    showArrow={true}
                    disabled={props.disabled}
                    
                    value={currentKey}
                    defaultValue={currentKey}
                    onChange={onChangeIncusion}  >
                    <Select.Option className="combinator-option" value="includes">{t("tenant.audience.include.name")}</Select.Option>
                    <Select.Option className="combinator-option" value="excludes">{t("tenant.audience.exclude.name")}</Select.Option>
                </Select>
            </ReadOnlyBox>

            <SelectBox
                style={{ flexGrow: 1 }}
                grouped
                standalone
                allowClear={true}
                readonly={props.readonly}
                readonlyMode={props.readonlyMode}
                onChange={onChangeTenants}
                selectedKey={(children ? children[currentKey] : []) as string[]}
                getlbl={t => <TenantLabel tenant={t} />}
                disabledOption={t => tenants.lookup(t)?.lifecycle.state === 'inactive'}
                lblText={t => l(tenants.lookup(t)?.name)}
            >
                {allTenants.map(t => t.id)}
            </SelectBox>

        </FieldRow>

    </Field>
}