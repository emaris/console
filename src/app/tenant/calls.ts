import { useCalls } from "../call/call";
import { Tenant } from "./model";


const svc="admin"
export const tenantApi = "/tenant"


export const useTenantCalls = () => {

   const {at} = useCalls()


    return {


    fetchAll: () => at(tenantApi,svc).get<Tenant[]>()
    
    , 
   
    fetchOne: (id:String) =>  at(`${tenantApi}/${id}`,svc).get<Tenant>()
    
    ,
    
    add: (tenant:Tenant) =>  at(tenantApi,svc).post<Tenant>(tenant)
    
    ,

    update: (tenant:Tenant) => at(`${tenantApi}/${tenant.id}`,svc).put<Tenant>(tenant)
    
    , 

    delete: (id:string) => at(`${tenantApi}/${id}`,svc).delete()

    }
  
}