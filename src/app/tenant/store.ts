
import { buildinfo } from '#app/buildinfo';
import { pushLogoutTopic } from '#app/call/constants';
import { useT } from '#app/intl/api';
import { newLifecycle } from '#app/model/lifecycle';
import { usePushEvents } from '#app/push/api';
import { useTagModel } from '#app/tag/model';
import { useUserCalls } from '#app/user/calls';
import { useLogged, useUserStore } from '#app/user/store';
import { useContext } from 'react';
import { useCompareMultiLang, useL, useMultilang } from "../model/multilang";
import { useToggleBusy } from "../system/api";
import { deepclone, indexMap, through } from "../utils/common";
import { notify, showAndThrow, useFeedback } from "../utils/feedback";
import { tenantApi, useTenantCalls } from "./calls";
import { tenantPlural, tenantRoute, tenantSingular, tenantType } from "./constants";
import { TenantContext, TenantNext } from './context';
import { completeOnAdd, newTenant, Tenant, useNoTenant, useTenantModel } from "./model";
import { usePreload } from 'apprise-frontend-core/client/preload';

const type = tenantType

export const useTenantStore = () => {

  const state = useContext(TenantContext)

  const calls = useTenantCalls()
  const model = useTenantModel()

  const t = useT()
  const l = useL()
  const tags = useTagModel()
  const compare = useCompareMultiLang()

  const ml = useMultilang()
  const toggleBusy = useToggleBusy()
  const fb = useFeedback()

  const push = usePushEvents()

  const [singular, plural] = [tenantSingular, tenantPlural].map(n => n.toLowerCase())

  const logged = useLogged()

  const userstore = useUserStore()
  const usercalls = useUserCalls()

  const noTenant = useNoTenant()

  const preload = usePreload()

  const all = () => state.get().tenants.all

  const setAll =  (tenants: Tenant[]) => state.set(s => {

    s.tenants.all = tenants;
    s.tenants.map = indexMap(tenants).by(t => t.id)

  })

  const fetchAll= (forceRefresh = false): Promise<Tenant[]> =>

  !!all() && !forceRefresh ? Promise.resolve(all())

      :

      toggleBusy(`${type}.fetchAll`, t("common.feedback.load", { plural }))

        .then(_ => console.log(`fetching tenants...`))
        .then(_ => preload.get<Tenant[]>(tenantApi) ?? calls.fetchAll())
        .then(through(setAll))


        .catch(e => showAndThrow(e, t("common.calls.fetch_all_error", { plural })))
        .finally(() => toggleBusy(`${type}.fetchAll`))



  const self = {

    all
    
    ,

    setAll

    ,

    areReady: () => !!all()

    ,

    allSorted: () => {

      return [...all()].sort(model.comparator)

    }

    ,

    compare: (t1: Tenant, t2: Tenant) => t1.id === logged.tenant ? -1 : t2.id === logged.tenant ? 1 : ml(t1.name).compareTo(ml(t2.name))

    ,

    lookup: (tenant: string | undefined) => tenant ? state.get().tenants.map?.[tenant] : undefined

    ,

    safeLookup: (tenant: string | undefined) => self.lookup(tenant) ?? noTenant.get()

    ,

   fetchAll


    ,

    routeTo: (t: Tenant) => t ? `${tenantRoute}/${t.id}` : tenantRoute


    ,


    fetchOne: (id: string): Promise<Tenant> =>

      toggleBusy(`${type}.fetchOne`, t("common.feedback.load_one", { singular }))

        .then(_ => calls.fetchOne(id))
        .then(through(fetched => { if (!fetched) throw Error(`Invalid resource lookup for ${id}, returned undefined.`) }))
        .then(through(fetched => setAll(all().map(u => u.id === id ? fetched : u))))

        .catch(e => showAndThrow(e, t("common.calls.fetch_one_error", { singular })))
        .finally(() => toggleBusy(`${type}.fetchOne`))

    ,


    next: () => {

      const next = state.get().tenants.next || { model: newTenant() }
      return next;
    }

    ,

    resetNext: () => {

      self.setNext(undefined)
    }

    ,

    setNext: (t: TenantNext | Tenant | undefined): TenantNext | undefined => {
      const model = t ? t.hasOwnProperty('model') ? t as TenantNext : { model: t as Tenant } : undefined
      state.set(s => s.tenants.next = model)
      return model

    },


    save: (tenant: Tenant, previous: Tenant, then: (saved: Tenant) => any) => tenant.id ? self.update(tenant, previous, then) : self.add(tenant).then(then)

    ,


    add: (tenant: Tenant): Promise<Tenant> =>

      toggleBusy(`${type}.save`, t("common.feedback.save_changes"))

        .then(() => calls.add(completeOnAdd(tenant)).then(through(saved => setAll([saved, ...all()]))))

        .then(through(() => notify(t('common.feedback.saved'))))
        .catch(e => showAndThrow(e, t("common.calls.save_one_error", { singular })))
        .finally(() => toggleBusy(`${type}.save`))


    ,

    update: (tenant: Tenant, previous: Tenant, then: (saved: Tenant) => any) => {

      const deactivated = previous.lifecycle.state !== 'inactive' && tenant.lifecycle.state === 'inactive'

      const update = () =>

        toggleBusy(`${type}.save`, t("common.feedback.save_changes"))
          .then(() => calls.update(tenant).then(through(saved => setAll(all().map(u => u.id === tenant.id ? saved : u)))))
          .then(through(() => notify(t('common.feedback.saved'))))
          .catch(e => showAndThrow(e, t("common.calls.save_one_error", { singular })))
          .finally(() => toggleBusy(`${type}.save`))

      if (deactivated)

        fb.askConsent({

          title: t('tenant.deactivate.title', { singular }),
          content: t('tenant.deactivate.msg', { singular }),
          okText: t('tenant.deactivate.confirm', { singular }),

          okChallenge: buildinfo.development ? undefined : t('tenant.deactivate.challenge'),

          onOk: async close => {

            const saved = await update()

            try {

              const { updateMany, deactivateAccounts } = usercalls

              const usersInTenant = userstore.all().filter(u => u.tenant === saved.id)

              await updateMany(usersInTenant.map(u => ({ ...u, lifecycle: { ...u.lifecycle, state: 'inactive' } })))
                .then(saved => userstore.setAll(userstore.all().map(u => {

                  const match = saved.find(uu => uu.username === u.username)

                  return match ?? u

                })))
                .catch(e => {

                  close()

                  showAndThrow(e, t("tenant.deactivate.error"))

                })


              const accounts = usersInTenant.map(u => u.details.preferences.accountId!).filter(u => u)

              if (accounts.length)
                await deactivateAccounts(accounts).catch(e => {

                  close()
                  showAndThrow(e, t("tenant.deactivate_account.error"))

                })

              push.publish(pushLogoutTopic, { users: usersInTenant.map(u => u.username) })



            }
            catch (tolerate) {

              console.error("can't deactivate/suspend tenant users/accounts", tolerate)

            }

            await then(saved)

          }


        })

      else update().then(then)

    }



    ,


    remove: (id: string, onConfirm?: () => void, challenge?: boolean) => {

      const tenantUsers = userstore.all().filter(u => u.tenant === id)

      const message = tenantUsers.length < 1 ? t('common.consent.remove_one_msg', { singular }) : `${t('tenant.has_users', { number: tenantUsers.length })}${t('common.consent.remove_one_msg', { singular })}`

      fb.askConsent({

        title: t('common.consent.remove_one_title', { singular }),
        content: message,
        okText: t('common.consent.remove_one_confirm', { singular }),

        okChallenge: challenge ? t('common.consent.remove_challenge', { singular }) : undefined,

        onOk: () => {


          toggleBusy(`${type}.removeOne`, t("common.feedback.save_changes"))

            .then(_ => calls.delete(id))
            .then(_ => all().filter(u => id !== u.id))
            .then(setAll)
            .then(_ => userstore.setAll(userstore.all().filter(u => u.tenant !== id)))
            .then(_ => notify(t('common.feedback.saved')))
            .then(_ => onConfirm && onConfirm())

            .catch(e => showAndThrow(e, t("common.calls.remove_one_error", { singular })))
            .finally(() => toggleBusy(`${type}.removeOne`))


        }
      })
    }
    ,

    removeAll: (list: string[], onConfirm?: () => void) =>

      fb.askConsent({


        title: t('common.consent.remove_many_title', { count: list.length, plural }),
        content: t('common.consent.remove_many_msg', { plural }),
        okText: t('common.consent.remove_many_confirm', { count: list.length, plural }),

        onOk: () => {


          toggleBusy(`${type}.removeAll`, t("common.feedback.save_changes"))

            .then(_ => list.forEach(id => calls.delete(id)))
            .then(_ => all().filter(u => !list.includes(u.id)))
            .then(setAll)
            .then(_ => notify(t('common.feedback.saved')))
            .then(_ => onConfirm && onConfirm())

            .catch(e => showAndThrow(e, t("common.calls.remove_one_error", { singular })))
            .finally(() => toggleBusy(`${type}.removeAll`))


        }
      })

      ,


      stringify: (t:Tenant|undefined) => t ? `${l(t.name)} ${l(t.description)??''} ${tags.stringifyRefs(t.tags)}` : ''


      ,


      stringifyRef: (t:string|undefined) => self.stringify(self.safeLookup(t))
        
      ,

      clone: (t:Tenant) : Tenant => ({...deepclone(t), id: undefined!, lifecycle:newLifecycle('inactive')})
      
      
      ,

      comparator: (o1:Tenant,o2:Tenant) => compare(o1.name,o2.name)


  }

  return self;

}