import { SelectBox } from "#app/form/SelectBox";
import { SimpleBox } from "#app/form/SimpleBox";
import { useT } from '#app/intl/api';
import { Module } from "#app/module/model";
import { useLogged } from '#app/user/store';
import { checkIt } from "#app/utils/validation";
import { VSelectBoxProps } from "../form/VSelectBox";
import { useLocale } from "../model/hooks";
import { noTenant, tenantSingular, tenantType } from "./constants";
import { TenantLabel } from "./Label";
import { hasTenant, TenantResource } from "./model";
import { useTenantStore } from './store';


type Props = {

    editable: boolean
    module: Module
    allowNoTenant?: boolean
    resource: TenantResource | TenantResource[] | string | string[]

} & Omit<VSelectBoxProps, 'options' | 'children'>


export const TenantBox = (props: Props) => {

    const t = useT()
    const { l } = useLocale()

    const logged = useLogged()
    const { all, lookup } = useTenantStore()

    const { resource,
        editable = true,
        module,
        allowNoTenant=true,
        validation = checkIt().nowOr(
            t(editable ? "common.fields.tenant.msg_new" : "common.fields.tenant.msg", { singular: t(module.nameSingular) }),
            t("common.fields.tenant.help", { plural: t(module.namePlural) })

        ),
        label = t(tenantSingular),
        ...rest } = props

    const options = [...((logged.hasNoTenant() && allowNoTenant) ? [noTenant] : []), ...all().map(t => t.id).sort((a, b) => l(lookup(a)?.name).localeCompare(l(lookup(b)?.name)))]

    // const procprops = {
    //     ...rest,


    //     mode: undefined,
    //     renderOption: props.renderOption || (tenant => tenant === noTenant ? t("common.labels.none") : l(lookup(tenant)!.name)),
    //     options

    // }

    const isTenantObject = (obj) => hasTenant(obj) || typeof obj === 'object' ? Object.keys(obj).includes(tenantType) : false

    const selected = Array.isArray(resource) ? resource.map(res => isTenantObject(res) ? res.tenant : res) : isTenantObject(resource) ? (resource as TenantResource).tenant : resource as string

    return (editable ?

        // <VSelectBox validation={validation} label={label} {...procprops} lblTxt={t=>l(lookup(t)?.name)}>
        //     {[resource.tenant]}
        // </VSelectBox>

        <SelectBox
            label={label}
            validation={validation}
            allowClear
            selectedKey={selected}
            getlbl={t => <TenantLabel tenant={t} />}
            lblText={tenant => tenant === noTenant ? t("common.labels.none") : l(lookup(tenant)?.name)} {...rest}>

            {options}

        </SelectBox>

        :

        <SimpleBox label={label} validation={validation} {...rest}>
            {Array.isArray(selected) ?
                selected.map(tenant => tenant === noTenant ? undefined : <TenantLabel tenant={tenant} />).filter(tenant => tenant !== undefined)
                :
                <TenantLabel tenant={selected as string} />

            }
        </SimpleBox>

    )




}