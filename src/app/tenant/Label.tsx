

import { Tooltip } from "antd"
import { icns } from "#app/icons"
import * as React from "react"
import { useT } from '#app/intl/api'
import { useLocation } from "react-router"
import { Label, LabelProps, sameLabel, UnknownLabel } from "../components/Label"
import { useLocale } from "../model/hooks"
import { noTenant, tenantIcon, tenantRoute, tenantType } from "./constants"
import { Tenant } from "./model"
import { useTenantStore } from './store'


type Props = LabelProps & {

    tenant: string | Tenant | undefined

}

const innerrouteRegexp = new RegExp(`${tenantRoute}\\/(.+?)(\\?|\\/|$)`)


export const TenantLabel = (props: Props) => {

        const { tenant, ...rest } = props

        const t = useT()
        const location = useLocation()
        const tenants = useTenantStore()

        if (!tenant)
            return <UnknownLabel />

        if (tenant === noTenant)
            return <Label {...rest} title={t("common.labels.none")} icon={tenantIcon} />

        const resolved = typeof tenant === 'string' ? tenants.lookup(tenant) : tenant

        if (!resolved)
            return <UnknownLabel tip={() => tenant as string} />


        return <PureTenantLabel {...props} tenant={resolved} tenants={tenants} location={location} />
    }

type PureProps = Omit<Props,'tenant'> & {

    tenant: Tenant
    tenants: ReturnType<typeof useTenantStore>,
    location: ReturnType<typeof useLocation>
}

const PureTenantLabel = React.memo((props: PureProps) => {

    const { l } = useLocale()

    const { location, tenants, tenant, linkTo, decorations = [], icon, inactive, ...rest } = props

    const { pathname, search } = location

    // like routeTo() if the current route is not a campaign route, otherwise replaces the current campaign with the target one.
    // undefined, if the two campaigns coincide.
    const innerRouteTo = (target: Tenant) => {

        const regexpmatch = pathname.match(innerrouteRegexp)?.[1]
        const linkname = target?.id

        return regexpmatch ?
            regexpmatch === linkname ? undefined : `${pathname.replace(regexpmatch, linkname!)}${search}`
            : tenants.routeTo(target)


    }

    const route = linkTo ?? innerRouteTo(tenant)

    const description = l(tenant.description)

    if (description)
        decorations.push(<Tooltip title={description}>{icns.note}</Tooltip>)

    const inactiveState = tenant.lifecycle.state === 'inactive'


    return <Label linkTarget={tenantType} title={l(tenant.name)} icon={icon ?? tenantIcon} linkTo={route} decorations={decorations} {...rest} inactive={inactive ?? inactiveState} />




}, ($, $$) => $.tenant === $$.tenant && sameLabel($, $$))