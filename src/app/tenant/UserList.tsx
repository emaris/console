
import { userActions } from '#app/user/actions';
import { UserLabel } from "#app/user/Label";
import { useLogged, useUserStore } from '#app/user/store';
import * as React from "react";
import { useT } from '#app/intl/api';
import { useHistory } from "react-router";
import { Button } from "../components/Button";
import { ListState, useListState } from "../components/hooks";
import { Column, VirtualTable } from "../components/VirtualTable";
import { userIcon, userRoute } from "../user/constants";
import { nameOf, User } from "../user/model";


type Props = {
    filter: (u:User)=>boolean
}

export const TenantUserList = (props:Props) => {

    const url = `${userRoute}`
    const t = useT()

    const history = useHistory()

    const logged = useLogged()

    const {all,  remove, setNext} = useUserStore()
    
    const {manage} = userActions

    const liststate : ListState<User> = useListState<User>();


    const openBtn = ({username}:User) => <Button key={1} enabledOnReadOnly icon= "edit" linkTo={ `${url}/${username}` }>Open</Button>
    const removeBtn =  (user:User) => <Button key={2} icon="delete" enabled={logged.can(manage)} onClick={ ()=> remove(user) } >Remove</Button>

    const cloneBtn = (user:User)  =>  <Button key={3} icon="copy" onClick={()=>{
                                                setNext({...user,username:`${user.username}_cloned`})
                                                history.push(`${url}/new`)}} >Clone</Button>
    

   return  <VirtualTable<User> state={liststate} data={all().filter(props.filter)} 
                            actions= { u => [openBtn(u),removeBtn(u),cloneBtn(u)]} 
                            rowKey={'username'}
                            sortBy={[['name', 'asc']]} >
                                
        <Column<User> flexGrow={1} title={t("common.fields.name_multi.name")} icon={userIcon} dataKey="name" 
                dataGetter={user=>nameOf(user)}
                cellRenderer={({rowData:u}) => <UserLabel user={u} />} />

        {/* <Column title="Name" dataKey="name" dataGetter={nameOf} flexGrow={1} /> */}
        {/* <Column title="Tags" dataKey="tags" dataGetter={(t:User)=><TagList taglist={t.tags} />} flexGrow={1} /> */}
        <Column<User> flexGrow={1} title={t("user.fields.email.name")} dataKey="email" dataGetter={(t:User)=><a href={`mailto:${t.details.email}`}>{t.details.email}</a>}/>
    
    </VirtualTable>

}