import { any, Action } from "../iam/model";
import { tenantIcon, tenantType } from "./constants";

const baseAction = { icon:tenantIcon, type: tenantType, resource :any }

export const tenantActions: {[id:string]:Action} = {  
    
    manage: {...baseAction, shortName:"tenant.actions.manage.short",name:"tenant.actions.manage.name", labels:["manage"], description: "tenant.actions.manage.desc"},
    admin:  {...baseAction, virtual: true, shortName:"tenant.actions.manage_users.short", name:"tenant.actions.manage_users.name",labels:["manage","admin"],  description: "tenant.actions.manage_users.desc"},
    rights:  {...baseAction, virtual: true, shortName:"tenant.actions.manage_rights.short", name:"tenant.actions.manage_rights.name",labels:["manage","admin"],  description: "tenant.actions.manage_rights.desc"}
}