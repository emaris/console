
import { Icon } from "antd"
import * as React from "react"
import { AiOutlineTeam } from "react-icons/ai"



export const  tenantSingular= "tenant.module.name_singular"
export const  tenantPlural= "tenant.module.name_plural"


export const tenantRoute="/tenant"
export const tenantType = "tenant"
export const tenantIcon = <Icon component={AiOutlineTeam} />

export const noTenant ="*"

export const tenantMailTopic='tenant'
export const tenantActiveMailTopic='tenant.active'
export const tenantInactiveMailTopic='tenant.inactive'
