import { iamType } from '#app/iam/constants'
import { useSettings } from '#app/settings/store'

export type IAMSettings = {

    betaTesters?: string[]
    disableTenantRemoval: boolean

}


export const useIAMSettings = () => useSettings().get<IAMSettings>()[iamType] ?? {}

