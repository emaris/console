

import { useT } from '#app/intl/api'
import { Language } from "#app/intl/model"
import { MailProfile } from '#app/mail/model'
import { useModuleRegistry } from '#app/module/registry'
import { useTagModel } from '#app/tag/model'
import { deepclone } from "#app/utils/common"
import shortid from "shortid"
import { Lifecycle, newLifecycle } from "../model/lifecycle"
import { MultilangDto, newMultiLang, useCompareMultiLang, useL } from "../model/multilang"
import { tenantType } from "./constants"
import { TenantSlot } from './module'


export type TenantState =  "active" | "inactive"

export type TenantDto = {

    id: string,
    name: MultilangDto
    guarded?: boolean
    description?: MultilangDto
    tags: string[]
    code: string
    preferences: Partial<TenantPreferences>
    lifecycle: Lifecycle<TenantState>
}

export type Tenant = TenantDto


export type TenantPreferences = {

    language: Language,
    location: string
    note?:Record<string, string>
    email: string
    mailProfile: MailProfile
    focalPoint?: string

  } & {[key:string]:any}

export type TenantResource = {

    tenant : string

}


export const completeOnAdd  = (tenant:Tenant) =>  ({...tenant, id:newTenantId()})

export const newTenantId = () =>`T-${shortid()}`

export const useTenantResourceType = () => {

    const registry = useModuleRegistry()
    
    return (type:string) => !!(registry.get(type)?.[tenantType] as TenantSlot)?.tenantResource

}

export const hasTenant = (obj: any): obj is TenantResource => {
    return obj.tenant
}

export const newTenant = (): Tenant => ({
    
                id: undefined!,
                name:newMultiLang(),
                description:newMultiLang(),
                tags:[],
                code: undefined!,
                preferences: {
                    language: 'en',
                    location: undefined!
                },
                lifecycle: newLifecycle('inactive')
                }) 


export const useNoTenant =  () => {
    
    const t = useT()
   
    return { get: () : Tenant => ({...newTenant(), id: t('common.labels.unknown') })}

}

//  currently justs identity transforms
export const useTenantModel = () => {

    const l = useL()
    const tags = useTagModel()
    const compare = useCompareMultiLang()
    
    const self = {


        stringify: (t:Tenant|undefined) => t ? `${l(t.name)} ${l(t.description)??''} ${tags.stringifyRefs(t.tags)}` : ''
        
        ,

        clone: (t:Tenant) : Tenant => ({...deepclone(t), id: undefined!, lifecycle:newLifecycle('inactive')})
        
        
        ,

        comparator: (o1:Tenant,o2:Tenant) => compare(o1.name,o2.name)


    }

    return self;


}