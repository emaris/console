import { fallbackStateOver, State } from 'apprise-frontend-core/state/api';
import { createContext } from 'react';
import { Tenant } from './model';



export type TenantNext = {
    model: Tenant
    linkBack?: string
  }
  
  export type TenantState = {
  
    tenants: {
      all: Tenant[]
      map: Record<string, Tenant>
      next: TenantNext | undefined
  
    }
  
  };
  
  
  export const initialTenants: TenantState = {
  
    tenants: {
      all: undefined!,
      map: undefined!,
      next: undefined
    }
  }

  

export const TenantContext = createContext<State<TenantState>>(fallbackStateOver(initialTenants))