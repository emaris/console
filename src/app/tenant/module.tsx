import { iamType } from '#app/iam/constants'
import { IamSlot } from '#app/iam/module'
import { Module } from '#app/module/model'

import { tenantActions } from './actions'
import { tenantIcon, tenantPlural, tenantSingular, tenantType } from './constants'


export const useTenantModule = () : Module => {

      
      return  {

            icon: tenantIcon,
            type: tenantType,

            nameSingular: tenantSingular,
            namePlural: tenantPlural,

             [tenantType]: {

                  tenantResource: true

            } as TenantSlot,

            [iamType]: {
                  actions: tenantActions,

            } as IamSlot
      }

}

export type TenantSlot = {

      tenantResource?: boolean
}