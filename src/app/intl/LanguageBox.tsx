import { Label } from '#app/components/Label'
import { SelectBox, SelectBoxProps } from '#app/form/SelectBox'
import { icns } from '#app/icons'
import * as React from 'react'
import { useT } from '#app/intl/api'
import { fullnameOf, Language } from './model'
import { useIntl } from './store'

type Props = Omit<SelectBoxProps<Language>,"small"|"getlbl"|"children">  & {

    // recognises a language as the default and doesn't store it (if it wasn't already)
    defaultLanguage? : Language
    iconOnLabel?: boolean

}

export const LanguageBox = (props:Props) => {

    const t = useT()

    const {languages, loadLanguage} = useIntl()

    const {validation={}, onChange, selectedKey, defaultLanguage, iconOnLabel=false, ...rest} = props

    const initial = React.useRef(selectedKey);
   
    const defaultValidation = {msg:t("common.fields.language.msg"),help:t("common.fields.language.help")}

    const defaultedValidation = {...defaultValidation,...validation}

    // stores the default language as undefined, unless it was the initial value. 
    const change = (v:any) => Promise.resolve(loadLanguage(v)).then(_=>onChange( v === defaultLanguage && v!== initial.current ? undefined : v ))

    const label = iconOnLabel ? <Label icon={icns.language} title={t("common.fields.language.name")} /> :  `${t("common.fields.language.name")}`

    return <SelectBox {...rest}  validation={defaultedValidation} label={label}  
                lblText={l => t(fullnameOf(l)) }
                getlbl={(l:any) => <Label icon ={!iconOnLabel && icns.language} title={t(fullnameOf(l))} />  }
                selectedKey={selectedKey ?? defaultLanguage}
                onChange={change}>
                {languages()}
            </SelectBox> 

}