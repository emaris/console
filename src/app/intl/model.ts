export type Language = "fr" | "en" | "ar" | "zh" | "es" | "ru"

export const allLanguages : Language[] = ["fr","en","ar","zh","es","ru"]
export const defaultLanguage = "en" as Language

export type IntlConfig = {
  intl: {
    languages:Language[]
    required?:Language[]
    default: Language
  }
}

export const intlconfig  = {
  
  default: defaultLanguage,
  loadPath: 'locale/{{lng}}_{{ns}}.json',
  languages: allLanguages,     // support all languages by default
  required: [defaultLanguage],
  debug:false
}

export const fullnameOf = (lang:Language) => {

  switch (lang) {

    case "en" : return "common.languages.en"
    case "fr" : return "common.languages.fr"
    case "es" : return "common.languages.es"
    case "zh" : return "common.languages.zh"
    case "ru" : return "common.languages.ru"
    case "ar" : return "common.languages.ar"
  }
}