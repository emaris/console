
import i18next from 'i18next';
import * as React from "react";
import { Language } from "./model";

export const LanguageContext = React.createContext<Language>(undefined!)

export const useCurrentLanguage = ()=> React.useContext(LanguageContext)


const t = i18next.t.bind(i18next)

export const useT =  () => t


const fixedT = i18next.getFixedT.bind(i18next)


export const useFixedT =  (lang:Language) => fixedT(lang)