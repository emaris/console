import { fallbackStateOver, State } from 'apprise-frontend-core/state/api'
import { i18n } from 'i18next'
import { createContext } from 'react'
import { defaultLanguage, Language } from './model'



export type IntlState = {

    intl: i18n
    language: Language
  
  }
  
  export const initialIntl: IntlState = {
  
    intl: undefined!,
    language: defaultLanguage
  }
  
  
  

export const IntlContext = createContext<State<IntlState>>(fallbackStateOver(initialIntl))
