
import { Spin } from "antd"
import { useRenderGuard } from 'apprise-frontend-core/utils/renderguard'
import * as React from "react"
import { useTranslation } from 'react-i18next'
import { Placeholder } from "../components/Placeholder"
import { LanguageContext } from './api'
import { Language } from "./model"
import { useIntl } from './store'

type Props = React.PropsWithChildren<{}>

export const IntlLoader = (props: Props) => {

  const { isReady, fetch } = useIntl()

  const { content } = useRenderGuard({
    when: isReady(),
    render: props.children,
    orRun: fetch,
    andRender: Placeholder.none
  })

  return content;
}


export type LanaguageLoaderProps = React.PropsWithChildren<{

  onReady: (lang: Language) => void

}>
//  little trick to have a spinner when language loads, without preventing
//  rendering of children, which creates a flash of content or worse.

export const LanguageLoader = (props: LanaguageLoaderProps) => {

  // use hook just to force language loading
  const { ready } = useTranslation();

  const { onReady } = props

  const intl = useIntl()

  const language = intl.currentLanguage()

  React.useEffect(() => {

    if (ready)
      onReady(language)


  }, [ready, language, onReady])

  // render always, even when not ready
  return <Spin spinning={!ready} size={'small'} >
    <LanguageContext.Provider value={language}>
      {props.children}
    </LanguageContext.Provider>
  </Spin>
}