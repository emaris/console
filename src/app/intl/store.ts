

import { useConfig } from '#app/config/state';
import { useToggleBusy } from "#app/system/api";
import i18next, { i18n } from "i18next";
import XHR from "i18next-http-backend";
import { useContext } from 'react';
import { initReactI18next } from "react-i18next";
import { buildinfo } from "../buildinfo";
import { showAndThrow } from "../utils/feedback";
import { IntlContext } from './context';
import { allLanguages, defaultLanguage, intlconfig, Language } from "./model";
import { usePreload } from 'apprise-frontend-core/client/preload';


export const useIntl = () => {

  const state = useContext(IntlContext)

  const toggleBusy = useToggleBusy()

  const config = useConfig()

  const preload = usePreload()


  // intl is a ubiquitous dependency. it should use other apis sparingly, and always in the methods below.

  const thisapi = {

    currentLanguage: () => state.get().language as Language

    ,

    setCurrentLanguage: async (l: Language) => {
      await state.get().intl.changeLanguage(l)
      state.set(t => t.language = l)
    }

    ,

    defaultLanguage: () => config.get().intl.default || intlconfig.default

    ,

    languages: () => config.get().intl?.languages || intlconfig.languages


    ,

    requiredLanguages: () => config.get().intl.required ?? []

    ,

    set: (intl: i18n) => state.set(s => s.intl = intl)

    ,

    get: () => state.get().intl


    ,

    getFixedT: i18next.getFixedT.bind(i18next) // exports translation method as a standalone function


    ,

    isReady: () => state.get().intl !== undefined

    ,

    loadLanguage: (lang: Language) => {

      return toggleBusy(`lang-loader-${lang}`, i18next.t("common.feedback-sync"))
        .then(_ => i18next.loadLanguages(lang))
        .catch(e => showAndThrow(e, ''))
        .finally(() => toggleBusy(`lang-loader-${lang}`))
    }

    ,

    fetch: async () => {

      console.log("fetching translations...")


      const preloaded = Promise.all(allLanguages
        .map(lang => preload.get(`/intl/${lang}`)?.then(res => ({ [lang]: { translation: res } })) ?? Promise.resolve()))
        .then(resources => resources.reduce((acc, res) => ({ ...acc, ...res }), {}))

      const preloadedResources: any = await preloaded



      const path = `${buildinfo.prefix}/${intlconfig.loadPath}`

      var error: any = undefined;

      return i18next
        .use(XHR)
        .use(initReactI18next) // passes i18n down to react-i18next
        .init({
          partialBundledLanguages: true,
          resources: preloadedResources,
          lng: defaultLanguage,
          returnEmptyString: false,
          fallbackLng: defaultLanguage,
          nsSeparator: "__",
          react: {
            useSuspense: false
          },
          debug: false,
          backend: { loadPath: path },
          interpolation: { escapeValue: false } // react already safe from XSS

        },
          //  throwing errors from here won't propagae to thenables (must be caught)
          //  so we track the error manually and throw from the first thenable.
          e => { if (e) error = new Error(e) }
        ).then(() => { if (error) throw error })

        .catch(e => showAndThrow(e, "Cannot load translations."))  // can't possibly internationalise this
        .then(() => thisapi.set(i18next))

    }

  }

  return thisapi

}

