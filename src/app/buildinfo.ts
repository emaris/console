

export const buildinfo = {
    development: import.meta.env.MODE==='development',
    prefix: import.meta.env.BASE_URL,
    version: import.meta.env.VITE_VERSION,
    timestamp: import.meta.env.VITE_TIMESTAMP,
    commit: {
        id: import.meta.env.VITE_COMMIT_ID,
        author:import.meta.env.VITE_COMMIT_AUTHOR,
        message:import.meta.env.VITE_COMMIT_MSG,
        time: import.meta.env.VITE_COMMIT_TS
    }

}