import * as React from "react"
import { useDrag } from "react-dnd"
import { defaultItemType, Item } from "./model"

type Props<T extends Item> = React.PropsWithChildren<{

    className?:string
    style?: React.CSSProperties
    item?:T
    type?:string
}>

export type DraggableProperties = {

    isDragging: boolean
}

export const Draggable =<T extends Item=Item> (props:Props<T>) =>{
   
    const {item={type:defaultItemType},className='',style,children} = props

    const [childprops, drag] = useDrag({
        // type:  '',
        
        item,

        collect: dnd => ({
        
            isDragging: !!dnd.isDragging()
        
        }) as DraggableProperties,
    })

    const ref = React.useRef<HTMLDivElement>(null)
    
    drag(ref)

    const {isDragging} = childprops

    const renderedChild = typeof children === 'function' ? children(childprops) : children

    return <div ref={ref} style={style} className={`draggable ${className} ${isDragging ? 'dragging' : ''}`} >
                {renderedChild}
           </div>

}