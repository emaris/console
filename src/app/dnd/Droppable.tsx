import * as React from "react"
import { useDrop } from "react-dnd"
import { NativeTypes } from "react-dnd-html5-backend"
import { defaultItemType, Item } from "./model"
import "./styles.scss"



type DropProps = {

    isOver: boolean
    didDrop: boolean
    canDrop: boolean
}

type Props<T extends Item = Item> = {

    path: string
    types?: string[]
    className?: string

    onDrop: (path: string, item: T) => void
    onDoubleClick?: () => void

    unless?: boolean | ((_: T) => boolean)

    children: React.ReactNode | ((_: DropProps) => React.ReactNode)

}


export const Droppable = <T extends Item = Item>(props: Props<T>) => {

    const { path, onDrop, types = [defaultItemType], className, unless, children } = props

    const normalizedUnless = (item: T) => typeof unless === 'boolean' ? unless : unless?.(item)

    const [childprops, ref] = useDrop({
        accept: types,

        // do nothing if event already handled, otherwwise call back client.
        drop: (item: T, monitor) => normalizedUnless(item) || monitor.didDrop() ? undefined : onDrop(path, item)

        ,

        collect: monitor => ({
            isOver: monitor.isOver({ shallow: false }) && !normalizedUnless(monitor.getItem() as T),
            didDrop: monitor.didDrop(),
            canDrop: monitor.canDrop() && !normalizedUnless(monitor.getItem() as T),
        }),
    })

    const { isOver } = childprops

    const renderedChild = typeof children === 'function' ? children(childprops) : children

    return <div onDoubleClick={() => props.onDoubleClick && props.onDoubleClick()} ref={ref} className={`droppable ${className} ${isOver ? 'hovered' : ''}`} >
        {renderedChild}
    </div>

}
type FileProps = {

    className?: string
    path?: string

    unless?: boolean

    onDrop: (files: File[]) => void
    onDoubleClick?: () => void

    children: Props['children']

}

const isNotADirectory = async (file: File): Promise<File | undefined> => {
    try {
        await file.slice(0, 1).arrayBuffer()
        return file
    } catch (err) { 
        return undefined 
    }
}

export const directoryFilter = async(files: File[]) : Promise<File[]> => {

    let filtered = [] as File[]

    for (let i = 0; i < files.length; i++) {
        const file = await isNotADirectory(files[i])
        if (file !== undefined) filtered.push(file)
    }

    return filtered

}

export const FileDroppable = (props: FileProps) => {

    const { onDrop, children, unless, className, ...rest } = props

    const [hasError, error] = React.useState<boolean>(false)

    const innerOnDrop = (files) => {
        if (files.length === 0)  return

        Promise.resolve(directoryFilter(files)).then(filtered => {
            if (filtered.length === 0) {
                error(true)
                setTimeout(() => {
                    error(false)
                }, 2000)
            } else {
                error(false)
                onDrop(filtered)
            }
        })
    }

    const innerClassName = `${className} ${hasError ? 'error' : ''}`

    return <Droppable<Item & { files: File[] }> className={innerClassName} types={[NativeTypes.FILE]} path={"none"} unless={unless} onDrop={(_, { files }) => innerOnDrop(files)} {...rest} >
        {props.children}
    </Droppable>

}