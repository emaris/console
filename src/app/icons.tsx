
import { Icon, Tooltip } from "antd"
import * as React from "react"
import * as anticns from "react-icons/ai"
import * as biicns from "react-icons/bi"
import * as faicns from "react-icons/fa"
import * as fiicns from "react-icons/fi"
import * as goicns from "react-icons/go"
import * as hiicns from "react-icons/hi"
import * as ioicns from "react-icons/io"
import * as mdicns from "react-icons/md"
import * as riicns from "react-icons/ri"
import * as tiicns from "react-icons/ti"
import * as vscicns from "react-icons/vsc"

//  constants to icons as react components, for sharing and abstraction (e.g. open => edit, permissions => unlock, etc. ).
//  components are based on Ant's own Icon and custom svgs provided by react-icons over many font iconset, inclduing antd's own iconset.
//  when picking icons from other sets, ant's own CSS are retained for greater con sistency.

// cannot use react-icons for two-tone antd icons, either because we cannot - as it seems - or because we don't know how to.
// so for two-tone icons, we use the native offering of antd (still define constants below). 
// we must also list them one by one in anticons.js, which is used as a filter so that webpack doesn't end up bundling the full iconset. 

export const icns = {


   loading : (small:boolean) => <Icon className="anticon-loading" style={{ fontSize: small ? 18 : 24 }} spin component={anticns.AiOutlineLoading}/>,
   
    zoom: <Icon component={anticns.AiOutlineSearch} />, 
    search: <Icon component={anticns.AiOutlineSearch} />, 
    filter: <Icon className='anticon-filter' component={anticns.AiOutlineFilter} />,
    menu : <Icon className="anticon-menu" component={fiicns.FiMenu} />,
    left : <Icon className="anticon-left" component={anticns.AiOutlineLeft} />,
    right : <Icon className="anticon-right" component={anticns.AiOutlineRight} />,
    down : <Icon className="anticon-down" component={anticns.AiOutlineDown} />,
    up : <Icon className="anticon-up" component={anticns.AiOutlineUp} />,
    upload : <Icon className="anticon-upload" component={anticns.AiOutlineUpload} />,
    layers: <Icon className="fi" component={fiicns.FiLayers} />,
    download : <Icon className="anticon-download" component={anticns.AiOutlineDownload} />,
    form : <Icon className="anticon-form" component={anticns.AiOutlineForm} />,
    edit : <Icon className="anticon-edit" component={anticns.AiOutlineEdit} />,
    link : <Icon className="link-icon anticon-link" style={{marginLeft:2}} component={tiicns.TiArrowForward} />,
    revert : <Icon className="anticon-undreloado" component={anticns.AiOutlineUndo} />,
    reload : <Icon className="anticon-undo" component={anticns.AiOutlineReload} />,
    save : <Icon className="anticon-save" component={anticns.AiOutlineSave} />,
    list : <Icon className="anticon-bars" component={anticns.AiOutlineBars} />,
    add : <Icon className="native-icon" type="plus" />,
    addEntry: <Icon className="add-entry-icon anticon-add" component={anticns.AiFillPlusCircle} />,
    open : <Icon className="anticon-edit" component={anticns.AiOutlineEdit} />,
    remove : <Icon className="native-icon" type="minus" />,
    styledRemove: (className?: string, style?: React.CSSProperties) => <Icon className={className ?? ''} style={style ?? {}} type='minus' />,
    close : <Icon className="anticon anticon-close ant-select-remove-icon" component={anticns.AiOutlineClose} />,
    removeEntry: <Icon className="remove-entry-icon anticon-delete" component={anticns.AiOutlineMinusCircle} />,
    clone : <Icon className="anticon-copy" component={anticns.AiOutlineCopy} />,
    permissions : <Icon className="anticon-unlock" component={anticns.AiOutlineUnlock} />,
    language : <Icon className="native-icon" theme="twoTone" type="flag" />,
    languageMono : <Icon className="anticon-flag" component={anticns.AiOutlineFlag} />,
    history: <Icon className="history-icon anticon-history" component={anticns.AiOutlineHistory} />, 
    profile: <Icon className="anticon-profile" component={anticns.AiOutlineProfile} />, 
    bell: <Icon className="anticon-bell" component={anticns.AiOutlineBell} />,  
    readonly : (tip?:string, style?:React.CSSProperties) => { const icn = <Icon style={style ? style : {}}className="readonly-icon anticon-lock" component={anticns.AiOutlineLock} />; return tip? <Tooltip title={tip}>{icn}</Tooltip>:icn },
    note: <Icon className="note-icon anticon-book" component={anticns.AiOutlineBook} />,
    logout : <Icon className="anticon-logout" component={faicns.FaRegBookmark} />,
    admin : <Icon className="anticon-user" component={faicns.FaUserTie} />,
    reguser : <Icon className="anticon-user" component={faicns.FaUserCircle} />,
    guest : <Icon className="anticon-user" component={faicns.FaUser} />,
    error: (tip?:string) => { const icn = <Icon className="error-icon native-icon" type="exclamation"/> ; return tip? <Tooltip overlayClassName="error-tooltip" title={tip}>{icn}</Tooltip>:icn },
    check: <Icon className="check-icon anticon-check" component={anticns.AiFillCheckSquare} />,
    checkGreen: <Icon style={{color: "#169785"}} component={anticns.AiOutlineCheck} />,
    checkRed: <Icon style={{color: "red"}} component={anticns.AiOutlineCheck} />,
    checkIt: (color? : "green" | "red") => {
        const style:React.CSSProperties = color ? color === 'green' ? {color: "#169785"} : {color: "red"} : {}
        return <Icon style={style} component={anticns.AiOutlineCheck} />
    },
    dot: <Icon component={goicns.GoDot} />,
    usage: <Icon component={fiicns.FiBarChart2} />,
    generic: <Icon className="anticon-antdesign" component={anticns.AiOutlineAntDesign} />, 
    layout: <Icon className="anticon-layout" component={anticns.AiOutlineLayout} />,
    unknown: <Icon className="anticon-question" component={anticns.AiOutlineQuestionCircle} />,
    clock: <Icon className="anticon-clock" component={anticns.AiOutlineClockCircle} />,
    calendar: <Icon className="native-icon" type="calendar" />,
    home: <Icon className="anticon-home" component={anticns.AiOutlineHome} />,
    location: <Icon component={goicns.GoLocation} />,
    globe: <Icon className="anticon-global" component={ioicns.IoMdGlobe}  />,
    inactive: <Icon className="inactive-icon anticon-stop" component={anticns.AiOutlineStop} />,
    collapse: <Icon className="anticon-columnheight" component={hiicns.HiChevronLeft} />,
    collapseAll: <Icon className="anticon-columnheight" component={hiicns.HiChevronDoubleLeft} />,
    expand: <Icon className="anticon-columnheight" component={hiicns.HiChevronDown} />,
    expandAll: <Icon className="anticon-columnheight" component={hiicns.HiChevronDoubleDown} />,
    settings: <Icon component={ioicns.IoMdOptions} />,
    expandSquare: <Icon component={ioicns.IoMdExpand}/>,
    contract: <Icon component={ioicns.IoMdContract}/>,
    preview: <Icon className="anticon-eye" component={anticns.AiOutlineEye } />,
    file: <Icon className="anticon-file" component={anticns.AiOutlineFile} />,
    boolean: <Icon component={anticns.AiOutlineSwitcher} />,
    statistics: <Icon component={anticns.AiOutlinePieChart} />,
    system: <Icon component={mdicns.MdComputer} />,
    pause: <Icon className="anticon-pause" component={anticns.AiOutlinePause}/>,
    lock: (style?:React.CSSProperties) => <Icon style={style} component={anticns.AiOutlineLock} />,
    unlock: (style?:React.CSSProperties) => <Icon style={style} component={anticns.AiOutlineUnlock} />,
    warn: <Icon className="anticon-pause" component={anticns.AiOutlineWarning}/>,
    info: <Icon component={anticns.AiOutlineInfoCircle} />,
    branch: <Icon component={anticns.AiOutlineBranches} />,
    singleUse: <Icon component={biicns.BiDice1} />,
    rootFolder: <Icon component={vscicns.VscRootFolderOpened} />,
    dots: <Icon component={hiicns.HiDotsHorizontal} />,
    publish: <Icon component={mdicns.MdPublic} />,
    share: <Icon component={anticns.AiOutlineShareAlt} />,
    mail: <Icon component={mdicns.MdMail} />,
    row: <Icon component={anticns.AiOutlineInsertRowAbove} />,
    rowSection: <Icon component={anticns.AiOutlinePicCenter} />,
    rectangle: <Icon component={biicns.BiRectangle} />,
    html: <Icon component={anticns.AiOutlineHtml5} />,
    pdf: <Icon component={anticns.AiOutlineFilePdf} />,
    asset: <Icon component={mdicns.MdWebAsset} />,
    stop: <Icon component={anticns.AiOutlineStop} />,
    archive: <Icon component={biicns.BiArchive} />,
    muted: <Icon component={riicns.RiNotificationOffLine} />,
    unmuted: <Icon component={riicns.RiNotificationLine} />,
    help: (className?: string, style?: React.CSSProperties) => <Icon className={className ?? ''} style={style ?? {}} component={ioicns.IoIosHelpCircleOutline} />,
    vscrollEnabled: <Icon component={ anticns.AiOutlineEye }></Icon>,
    vscrollDisabled: <Icon component={ anticns.AiOutlineEyeInvisible }></Icon>,
}