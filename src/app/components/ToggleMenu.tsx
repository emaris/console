import { Menu, Checkbox, Divider, Dropdown } from 'antd'
import { icns } from '#app/icons'
import { arrayOf, deepclone } from '#app/utils/common'
import * as React from 'react'
import { useT } from '#app/intl/api'
import { Label, LabelProps } from './Label'
import { Button } from '#app/components/Button'


export type ToggleMenuProps = {
    icon?: JSX.Element
    noIcon?: boolean
    title?: string | React.ReactNode
    selectAll?: boolean
    setter?: (_: string[]) => void
    onVisibleChange?: (visible: boolean) => void
    visible?: boolean
    children: JSX.Element | JSX.Element[]
    selected?: string[]
    distincts?: string[]
    highlightWhenActive?: boolean
    iconRight?: boolean
}

const ToggleLabel = (props: LabelProps) => <Label style={{ border: 0 }} {...props} />

export const ToggleMenu = (props: ToggleMenuProps) => {
    const t = useT()

    const {icon=icns.unknown, selectAll=true, title='<Toggle Menu>', setter=()=>undefined, visible=false, onVisibleChange=()=>undefined, children: items, selected=[], distincts=[], noIcon=false, highlightWhenActive=true, iconRight=false} = props

    const highlight = highlightWhenActive && (selected.length !== distincts.length || (selected.length === 0 && !(distincts.length === 0)))

    const selectAllCheckBox = <Menu.Item key='select_all'>
            <Checkbox
                checked={selected.length === distincts.length}
                onChange={e => {
                    e.target.checked ?
                        setter(distincts)
                        :
                        setter([])

                }}>
                <ToggleLabel noLink icon={icon} mode='tag' title={t("common.labels.select_all")} />
            </Checkbox>
            <Divider style={{ margin: '10px 0 0 0' }} />
        </Menu.Item>

    const menu = <Menu className="selectable-filter-menu">
        {selectAll && selectAllCheckBox}
        {
            arrayOf(items ?? []).map((item, i) => {
                let compo
                if (item.type?.name === ToggleMenu.Item.name) {
                    const {value, name, icon} = item.props as ToggleMenuItemProps
                    compo = <Menu.Item key={i}>
                        <Checkbox
                            checked={selected.includes(value)}
                            onChange={e => {
                                if (e.target.checked) {
                                    setter([...selected, value])
                                } else {
                                    let current = deepclone(selected)
                                    current.splice(current.findIndex(a => a === value), 1)
                                    setter(current)
                                }
                            }}>
                            <ToggleLabel noLink icon={icon} mode='tag' title={name} />
                        </Checkbox>
                    </Menu.Item>
                }
                if (item.type?.name === ToggleMenu.Separator.name) {
                    compo = <Divider key={i} style={{ margin: '10px 0 0 0' }} />
                }
                return compo
            })
        }
    </Menu>


    return <Dropdown trigger={['click']} overlay={menu} className={highlight ? 'toggle-menu-highlight' : ''} overlayClassName="ant-dropdown-memu" visible={visible} placement='bottomRight' onVisibleChange={onVisibleChange}>
                <Button type={highlight ? 'primary' : 'default'} className='button-color' iconLeft={!iconRight} icn={noIcon ? undefined : icon} >{title}</Button>
            </Dropdown>
}

export type ToggleMenuItemProps = {
    name?: string | JSX.Element
    icon?: JSX.Element
    value: string
}

ToggleMenu.Item = function Item(_:ToggleMenuItemProps) { return null }
ToggleMenu.Separator = function Separator() { return null }


export const useToggleMenu = (props: ToggleMenuProps) => {

    const {selected: selectedItems, children: items} = props

    const distincts = arrayOf(items ?? []).reduce( (acc, item) => {
        if (item.type?.name === ToggleMenu.Item.name) {
            const {value} = item.props as ToggleMenuItemProps
            return [...acc, value]
        } 
        return acc
    }, [] as string [])

    const [selected, setSelected] = React.useState(selectedItems || distincts)
    const [visible, setVisible] = React.useState(false)

    return [<ToggleMenu {...props} 
                selected={props.selected ? props.selected : selected} 
                distincts={distincts} 
                setter={props.setter ? props.setter : setSelected} 
                visible={visible} 
                onVisibleChange={() => setVisible(v => !v)}  >{items}</ToggleMenu>, selected, visible] as [JSX.Element, string[], boolean]


}