import * as React from "react"
import { Paragraph, Text } from "./Typography"
import { Tooltip } from "antd"
import { useT } from '#app/intl/api'


type Props = {

    id:string|undefined
}

export const IdProperty = (props:Props) => {

    const t = useT()

    const {id=t("common.labels.unknown").toLocaleLowerCase()} = props 

    return  <div className="sidebar-property">                
                <Paragraph><Text smaller>{t("common.fields.id.name")}</Text></Paragraph>
                <Paragraph><Text smaller className="emphasis"><Tooltip overlayStyle={{fontSize:"12px"}} title={t("common.fields.id.tooltip")}>{id}</Tooltip></Text></Paragraph>                
            </div>
}