import * as React from "react"
import { BlockProps } from "antd/lib/typography/Base"
import AntParagraph from "antd/lib/typography/Paragraph"
import AntText, { TextProps as AntTextProps } from 'antd/lib/typography/Text'


type ParagraphProps = BlockProps & {

    spaced?: boolean
}

export const Paragraph = (props:ParagraphProps) => {

    const {spaced,...rest} = props

    return <AntParagraph {...rest} style={{ marginTop: spaced ? 5: 0, marginBottom: 0 ,...props.style}}>{props.children}</AntParagraph>

}


type TextProps = AntTextProps & {

    emphasis?: 'warning' | 'success' | 'primary' | 'error'
    smaller?: boolean
    small?: boolean
    secondary?: boolean
    italics?: boolean
    smallcaps?: boolean

}
export const Text = (props: TextProps ) => {

    const {smaller,className='',emphasis,small,secondary,italics,type:t, smallcaps, style={}, ...rest} = props

    const type = secondary ? "secondary" : t

    if (smaller) style.fontSize=12
    if (small) style.fontSize=10
    if (italics) style.fontStyle="italic"
    if (smallcaps) style.fontVariant="small-caps"

    return <AntText className={`${className} ${emphasis ? `text-${emphasis}` : ''}`}  {...rest} type={type} style={style}>{props.children}</AntText> 
}