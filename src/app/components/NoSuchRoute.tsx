import Title from "antd/lib/typography/Title";
import * as React from "react";
import { RouteComponentProps, withRouter } from "react-router";
import { Button } from "./Button";


type Props = RouteComponentProps & {
    backTo?:string
    msg?:string
    fullMsg?: string
}

export const NoSuchRoute =  withRouter ( ({msg,fullMsg,history,backTo}:Props) => {

    
    return      <>

                    <div className="covering bullseye" style={{"flexDirection":"column"}}>
                        
                        <Title>{fullMsg ? fullMsg : `There's absolutely nothing ${msg ? `at '${msg}'` : 'here'}.`}</Title>
                        <div  style={{"marginTop":"30px"}} >
                            {backTo ? <Button size="large" linkTo={backTo}>Back</Button> : 
                                      <Button size="large" onClick={history.goBack}>Back</Button> }
                                      &nbsp;&nbsp;
                            <Button size="large" linkTo="/" >Home</Button> 
                        </div>
                        
                    </div>       

                </>

})