
import * as React from "react"
import Title from "antd/lib/typography/Title";
import { buildinfo } from "#app/buildinfo";

export const Todo = ({msg}:{msg?:React.ReactNode}) => <div className="bullseye" style={{"flexDirection":"column"}}>
                                <img  alt="in-development" style={{objectFit:"cover"}}  src={`${buildinfo.prefix}/images/todo.jpg`} />
                                <Title>{msg || "It's one more thing that needs doing."}</Title>
                          </div>