import * as React from "react"
import { Column, SortMode, TableProps, VirtualTable } from "./VirtualTable"

type Props<T> = Omit<TableProps<T>,'children'|'sortBy'> & {

    data: T[]
    renderData?: (_:T) => React.ReactNode
    render: (_:T) => React.ReactNode

    refreshOn?: any

    sortMode?: SortMode 
    noSort?:boolean
}

export const SideList =  <T extends Object> (props:Props<T>) => {

    const {data,render, renderData, refreshOn, sortMode, noSort, ...rest} = props

    // wraps in container that stretches at the end of sider
    return <div className="vtable-mini">   
                <VirtualTable {...rest} countBadge='right' clearFilters={false} sortBy={[['name',sortMode ?? 'asc']]} skeletonOnTransition={false} rowHeight={20} headerHeight={0} selectable={false} data={data}>

                    <Column<T> refreshOn={refreshOn} flexGrow={1} dataKey="name" dataGetter={renderData ?? render} cellRenderer={row=>render(row.rowData)} />

                </VirtualTable>     
            </div>         

}