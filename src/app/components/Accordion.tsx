import { Tooltip } from 'antd';
import { useStable } from '#app/utils/function';
import { createContext, CSSProperties, MouseEventHandler, PropsWithChildren, ReactNode, useContext, useEffect, useMemo, useRef, useState } from 'react';
import { useT } from '#app/intl/api';
import { Label } from './Label';




// utility type for accordion children.
export type AccordionChildProps = {
	onCollapseToggle?: MouseEventHandler<HTMLElement>
	collapsed?: boolean
}


export type AccordionState = {

	isCollapsed: (key: string) => boolean
	setCollapsed: (key: string) => (val: boolean) => any

}

const AccordionContext = createContext<AccordionState>(undefined!)



export const AccordionProvider = (props: PropsWithChildren<{}>) => {

	const { children } = props

	const [state, stateSet] = useState<Record<string, boolean>>({})

	const value = useMemo<AccordionState>(() => ({

		isCollapsed: key => !!state[key],
		setCollapsed: (key) => (val) => state[key] !== !!val && stateSet(s => ({ ...s, [key]: val }))

	}), [state])

	return <AccordionContext.Provider value={value}>
		{children}
	</AccordionContext.Provider>
}




export const useAccordion = (props: {

	key: string,
	collapsedSize?: number; // Collapsed dimension
	axes?: 'width' | 'height'; // axes to collapse

}) => {

	const { key, collapsedSize = 30, axes = 'width' } = props ?? {};

	const ctx = useContext(AccordionContext)

	const collapsed = ctx.isCollapsed(key)
	const setCollapsed = ctx.setCollapsed(key)

	const elementRef = useRef<HTMLDivElement>(null)
	const expandedSizeRef = useRef<number>(-1)

	//console.log("render", { collapsed, size: expandedSizeRef.current, elem: domRef.current })

	const previousCollapsed = useRef<boolean>(false)

	// trigger transition on toggle.
	useEffect(() => {
	
		// a toggle changes the state, otherwise is an initial transition and it's handled differently.
		if (previousCollapsed.current===collapsed)
			return
			

		// console.log("toggling...",{previous:previousCollapsed.current, current: collapsed})

		if (collapsed)
			transitionToCollapsed({animate:true})
		else
			transitionToExpanded()

		// eslint-disable-next-line
	}, [collapsed])

	// runs second to remember current state at the next render
	useEffect( () => {

		previousCollapsed.current=collapsed

	})

	const transitionToCollapsed = (props:{animate: boolean}) => {

		if (!elementRef.current)
			return

		// remember expanded size we will return to.
		expandedSizeRef.current = elementRef.current.getBoundingClientRect()[axes]

		// set start state for transition. may be animated, or instantaneous.
		if (props.animate) {
			elementRef.current.style.setProperty("--duration", '300ms')
			elementRef.current.style.setProperty('--size', toPx(expandedSizeRef.current))
			elementRef.current.classList.add('transition', 'collapsed', 'no-grow')
			elementRef.current.classList.remove('transition-end')
		}
		else {
			elementRef.current.style.setProperty("--duration", '0ms')
			elementRef.current.classList.add('transition-end', 'collapsed', 'no-grow')
		}

		// set end state for transition.
		requestAnimationFrame(() => {
			elementRef.current?.style.setProperty('--size', toPx(collapsedSize));
			elementRef.current?.style.setProperty('--min-size', toPx(collapsedSize));
		})

	}


	const transitionToExpanded = () => {

		if (!elementRef.current)
			return

		// set start state for animation.
		elementRef.current.style.setProperty("--duration", '300ms')
		elementRef.current.classList.remove('transition-end');
		elementRef.current.classList.add('transition');

		// set end state for animation
		requestAnimationFrame(() => {

			elementRef.current?.classList.remove('collapsed');
			elementRef.current?.style.setProperty('--size', toPx(expandedSizeRef.current));
			elementRef.current?.style.setProperty('--min-size', toPx(expandedSizeRef.current));
		});

	};

	const onTransitionEnd = () => {

		if (elementRef.current === null)
			return;

		elementRef.current.classList.remove('transition');
		elementRef.current.classList.add('transition-end');

		// console.log("end transition with ", { collapsed, size: expandedSizeRef.current })

		if (!collapsed) {

			elementRef.current.classList.remove('transition', 'no-grow');
			elementRef.current.style.removeProperty('--min-size');
			elementRef.current.style.removeProperty('--size');

		}

	};

	const UnstableAccordion = (props: PropsWithChildren<Partial<{

		style: CSSProperties
		className: string

	}>>) => {

		const { style, className, children } = props


		useEffect(() => {
			
			// adjust padding: this is fragile..
			const comp = getComputedStyle(elementRef.current!.firstElementChild!);
			const padd = parseInt(comp[axes === 'width' ? 'marginLeft' : 'marginTop']) +
						parseInt(comp[axes === 'width' ? 'marginRight' : 'marginBottom']);

			elementRef.current!.style.setProperty('--padd', toPx(padd));

			// starts already collapsed ? transition without animation
			if (collapsed)
				transitionToCollapsed({animate:false})

		}, [])

		const onRelevantTransitionEnd = ({ target, propertyName }) => {

			if (!['height', 'width'].includes(propertyName))
				return

			if (elementRef.current !== target)
				return;

			onTransitionEnd()

		}

		return <div ref={elementRef} style={style} className={`collapse ${axes} ${className ?? ''}`} onTransitionEnd={onRelevantTransitionEnd}>
			{children}
		</div>

	}


	const Accordion = useStable(UnstableAccordion)

	return { Accordion, collapsed, toggleCollapsed: () => setCollapsed(!collapsed) }

};

export type AccordionLabelProps = {
	title: ReactNode
	icon: ReactNode
	className: string
} & AccordionChildProps;

export const AccordionLabel = (props: AccordionLabelProps) => {
	const t = useT()
	const { onCollapseToggle, collapsed, icon, title, className } = props

	return <Label className={className} noMemo icon={
		<Tooltip mouseEnterDelay={.5} title={collapsed ? t("dashboard.section_expand_tip") : t("dashboard.section_collapse_tip")}>
			<div onClick={onCollapseToggle}>
				{icon}
			</div>
		</Tooltip>
	} title={title} />
}


export const toPx = (n: number): string => `${n}px`;