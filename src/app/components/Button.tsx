
import { Button as AntButton, Dropdown, Icon, Menu, Badge, Tooltip } from "antd";
import * as React from "react";
import { Link } from "react-router-dom";
import { ButtonProps } from "antd/lib/button";
import { ReadOnlyContext } from "../scaffold/ReadOnly";
import { icns } from "../icons";
import { childrenIn } from "../utils/children";
import { TooltipPlacement } from 'antd/lib/tooltip';


export type BtnProps = {

  dark?: boolean
  enabled?: boolean
  noborder?: boolean
  iconLeft?: boolean 
  dot?: boolean
  light?:boolean

  tooltip?: React.ReactNode
  tooltipPlacement?: TooltipPlacement
  tooltipDelay?: number

  icn?: React.ReactNode

  linkTo?: string
  children?: React.ReactNode
  enabledOnReadOnly?: boolean

  xsize?: "xsmall" | "small" | "default" | "large"
  

} & ButtonProps


export const Button = (props:BtnProps) =>{

  const {icon,xsize, icn,dark,dot,linkTo,enabled=true,disabled=false, light, iconLeft,noborder,className,tooltip,tooltipPlacement, tooltipDelay, enabledOnReadOnly,...rest} = props

  const readOnlyContext = React.useContext(ReadOnlyContext)
  const readOnlyState = readOnlyContext && !enabledOnReadOnly
  
  const classes  = [className || ''] 

  if (noborder)
     classes.push("noborder")

  if (dark)
     classes.push("dark")

  if (dot)
     classes.push("dotted")

  if (light)
    classes.push("btn-light")

  if (xsize==="xsmall")
    classes.push("ant-btn-xsm")

  const size = xsize ? xsize === "xsmall" ? "small" : xsize : props.size

  var contents =   <>
    {iconLeft && (( icon && <Icon type={icon} />) || (icn && icn)) }
    {React.Children.count(props.children)>0 && <span>{props.children}</span>}
    {iconLeft || (icon && <Icon type={icon}/>) || (icn && icn)}
  </>

 const isDisabled = disabled || enabled===false    // we want false, not false negatives for undefined

 contents = dot? <Badge offset={[5,2]} dot>{contents}</Badge> : contents

  contents = <AntButton  {...rest} size={size} disabled={isDisabled || !enabled || readOnlyState} className={classes.join(' ')} >
              {contents}
            </AntButton>

  contents = tooltip ? <Tooltip mouseEnterDelay={tooltipDelay ?? 0.8} placement={tooltipPlacement} title={tooltip}>{contents}</Tooltip> : contents

 return linkTo ? <Link to={linkTo}>{contents}</Link> : contents

}

export const Buttons = (props:{children:any}) => {

  const actions  = childrenIn(props).type(Button).mapProps() as BtnProps[]

  const readOnlyState = React.useContext(ReadOnlyContext) 

  if (!actions || actions.length===0)  return null

  const Item = ({children,icon,icn}:BtnProps) =>

  <div className="action-menu-item" >
      <span className="item-name">{children}</span>
      { (icon &&  <Icon type={icon} />) ||
        (icn && icn) } 
  </div>
  
  const isDisabled = (a: BtnProps) => a.disabled===true || a.enabled===false || (readOnlyState && !a.enabledOnReadOnly)

  const disabled = actions.filter(isDisabled).length === actions.length;
  const small = actions.filter(a=>a.size==="small").length>0

  const menu = <Menu> {
            actions.map((a, key)=>  <Menu.Item 
                                      key={key} 
                                      disabled={isDisabled(a)} onClick={()=>a.onClick && a.onClick(undefined!) } > {
                                         a.linkTo ?  <Link to={a.linkTo} className={isDisabled(a) ? 'disabled-link' : ''}><Item {...a}/></Link> : <Item {...a}/>
                                    }</Menu.Item>
              )}
              </Menu>
  
  return <Dropdown trigger={['click']}  disabled={disabled} overlay={menu} placement='bottomRight'>
            <AntButton disabled={disabled} className="ant-btn-icon-only" size={small? "small":undefined}>
                   {icns.down}
            </AntButton>
        </Dropdown>

}


export const ButtonGroup = (props:{children:any,style?:React.CSSProperties}) => {

  const {primary, other=[]}= childrenIn(props).byProps([["primary",props => props?.type=== "primary" ]])

  return <div className={`action-group ${other?.length>0 && primary?.length>0 ? 'action-group-with-menu' : ''}`} style={props.style}>
              <span style={{marginRight:5}}>{primary?.[0] }</span>
              <Buttons>{[primary && primary.slice(1),...other]}</Buttons>
        </div>

}




