
import { Tooltip } from "antd"
import { Button } from "#app/components/Button"
import { Draggable } from "#app/dnd/Draggable"
import { Droppable } from "#app/dnd/Droppable"
import { Item } from "#app/dnd/model"
import { Field, FieldProps } from "#app/form/Field"
import { ReadonlyProps, useReadonly } from '#app/form/ReadonlyBox'
import { icns } from "#app/icons"
import * as React from "react"
import { useT } from '#app/intl/api'


type ListProps = FieldProps & ReadonlyProps & {

    id: string

    children: React.ReactNode[]

    singleLabel: string


    addIf?: boolean,
    removeIf?: (index: number) => boolean

    onRemove?: (index: number) => void
    onAdd?: () => void
    onMove?: (from: number, to: number) => void

    noPlaceholder?: boolean

}

type ItemType = Item & {

    index: number
}

export const CardlistBox = (props: ListProps) => {

    const t = useT()

    const { id, readonly, addIf = true, removeIf = () => true, onRemove = () => { }, singleLabel: singular, onAdd, onMove, children = [], className, label, noPlaceholder, ...rest } = useReadonly(props)

    const { disabled } = rest

    const removebtn = index => <Tooltip title={t("common.buttons.remove")}><Button disabled={disabled} noborder icn={icns.removeEntry} onClick={() => onRemove(index)} /></Tooltip>
    const addBtn = <Button disabled={disabled} xsize="xsmall" icn={icns.add} onClick={onAdd}>{t("common.buttons.add_one", { singular })}</Button>
    const addBtnPlaceholder = <Button disabled={disabled} noborder icn={icns.add} onClick={onAdd}>{t("common.buttons.add_one", { singular })}</Button>

    const lbl = <div className="list-label">
        {children.length > 0 && <div>{label}</div>}
        {/* {children.length > 0 && addIf && <div className="list-topbar">{addBtn}</div>} */}
    </div>

    const itemOf = (child, i) => <div className="list-row">
        <div className="list-item">
            {child}
        </div>
        {readonly || disabled || !removeIf(i) || <div className="list-bar">
            {removebtn(i)}
        </div>}
    </div>




    return <Field className={`listbox ${className ?? ''} `} label={lbl} {...rest}>
        <div>
            {children.map((child, i) =>

                onMove ? <Droppable<ItemType> key={i} className="list-droppable" types={[id]} path={`${i}`} onDrop={(_, item) => item.index !== i && onMove(item.index, i)}>
                    <Draggable<ItemType> className="list-draggable" item={{ type: id, index: i }}>

                        {itemOf(child, i)}

                    </Draggable>
                </Droppable>

                    :

                    <div key={i} className="list-droppable">
                        {itemOf(child, i)}
                    </div>

            )}
        </div>
        {
            children.length === 0 && !noPlaceholder && <div className="list-placeholder" style={{ pointerEvents: disabled ? 'none' : 'inherit', opacity: disabled ? .5 : 1 }}>{addBtnPlaceholder}</div>
        }
        {
            children.length > 0 && addIf && <div className="list-add">{addBtn}</div>
        }
    </Field>
}