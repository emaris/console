
import { useT } from '#app/intl/api'
import { useInfo } from '#app/settings/calls'
import * as React from 'react'
import { icns } from '../icons'
import { Info } from "../settings/model"
import { showAndThrow } from '../utils/feedback'
import { useAsyncRender } from '../utils/hooks'
import { Button } from './Button'
import { Placeholder } from './Placeholder'
import { Paragraph, Text } from './Typography'


export const Sheet = (props:React.PropsWithChildren<{}>) => {

    return <div className="sheet">
                 {props.children}
           </div>
 }


type RowProps = {

    label: React.ReactNode
    style?: React.CSSProperties
    startsGroup?: boolean 
    children: any
}

export const Row = ({label,style,startsGroup,children}:RowProps) => {

    if (startsGroup)
        style = {...style, ...{marginTop:20} };

    return <div style={style} className="sheet-row">
        <span className="sheet-label">{label}</span>
        <div className="sheet-value">
            {children}
       </div>
     </div>

}



type ServiceRowProps = {

    service:string
    label:string
}

export const ServiceRow = (props:ServiceRowProps) => {

    //const state = useBaseState()

    const {label,service}=props

    const [info,store] = React.useState<Info>(undefined!)
    

    const t = useT()
    const settings = useInfo()

    const [content,reload,loading] = useAsyncRender({

        when: !!info,
        task: () => Promise.resolve(console.log(`fetching info from ${service} service...`))
                    .then( () =>settings.fetchInfo(service) as Promise<Info>)
                    .then(store)
                    .catch( e => showAndThrow(e,t("common.calls.fetch_one_error",{singular:"service"}))),
        content: (state) =>   <div style={{height:110, opacity: state.override ? 0.5 : 1}}>
                                <Paragraph><Text>{info.build.version}<Text secondary> ({info.build.commit})</Text></Text></Paragraph>
                                <Paragraph><Text secondary smaller>{t("settings.builtOn")}  {info.build.timestamp}</Text></Paragraph>
                                <Paragraph><Text secondary smaller>{t("settings.builtWith")} </Text><Text secondary smaller italics>"{info.build.message}"</Text></Paragraph>
                                <Paragraph><Text secondary small >{t("settings.committedBy")} {info.build.user}</Text></Paragraph>
                                <Paragraph><Text secondary small>{t("settings.committedOn")} {info.build.time}</Text></Paragraph>   
                            </div>,
        placeholder: Placeholder.text,
        errorPlaceholder: "nodata"
    
    })

    return  <Row label={<Text>{label}</Text>}>
                {content}
                <Button iconLeft onClick={reload} xsize="xsmall" icn={loading ? icns.loading(true) : icns.reload } style={{margin:"8px 0"}} >{t("common.buttons.reload")}</Button>             
            </Row>
}

