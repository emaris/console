import { useConfig } from '#app/config/state'
import { MultiBox } from '#app/form/MultiBox'
import { useT } from '#app/intl/api'
import { MultilangDto, useL } from '#app/model/multilang'
import { Tag, useTagModel } from '#app/tag/model'
import { useTagStore } from '#app/tag/store'
import { Validation, ValidationReport, check, provided, requireLanguages } from '#app/utils/validation'
import { useLayoutSettings } from '#layout/settings/api'
import { Fragment } from 'react'


export const capCategoryId = 'TGC-custom-asset-property'
export const capPredefinedProp = 'cap_predefined'
export const capTypeProp = 'cap_type'
export const capDefaultProp = 'cap_default'
export const allTypePropValue = 'all'

export type CapProperties = Record<string, MultilangDto>

export const CapList = (props: {
    edited: CapProperties
    onChange: (_: CapProperties) => void
    type: string,
    singular: string

}) => {

    const t = useT()
    const l = useL()

    const { type, edited, onChange, singular } = props

    const cap = useCapProperties()

    const report = cap.validationFor(edited)

    const properties = cap.propertiesFor(type)

    const changeCap = (p:Tag)=>(v:MultilangDto) => {
        
        if (v)
          onChange({ ...edited, [p.id]: v })

        else {

            const copy = {...edited}

            delete copy[p.id]  
    
            onChange(copy)

        }
    }
        

    return <Fragment>

        {properties.map((p) =>{

            const validation ={ msg: t('asset.cap_msg'), help: l(p.description) || t('asset.cap_help',{singular:t(singular).toLowerCase()}), ...report[p.id] }

            return <MultiBox className='caplist-prop' key={p.id} id={`caplist-${p.id}`} label={l(p.name)} validation={validation} onChange={changeCap(p)}>
                {edited[p.id]}
            </MultiBox>

        })}
    </Fragment>
}


export const useCapProperties = () => {

    const tagstore = useTagStore()
    const tagmodel = useTagModel()
    const config = useConfig().get()

    const t = useT()
    const {ll} = useLayoutSettings()

    const self =  {

        propertiesFor: (type: string) : Tag[] => {

            return tagstore.allTagsOfCategory(capCategoryId).filter(t =>

                t.lifecycle.state === 'active' &&
               [allTypePropValue,type].includes(t.properties?.custom?.[capTypeProp] ?? type))
               .sort(tagmodel.comparator)

        },

        defaultsFor: (type: string) : CapProperties => {


            const props = self.propertiesFor(type)
            const defaults = props.reduce( (acc, prop) => ({...acc, [prop.id]: prop.properties.custom?.[capDefaultProp]}),  {})

            return props.length ? defaults : undefined!
        },

        referenceFrom: (props: CapProperties = {}, prop:Tag) =>  ( {exists: `${!!ll(props[prop.id])}`, value:ll(props[prop.id]),name: ll(prop.name), code: prop.code})

        ,

        validationFor: (props: CapProperties = {}) => {


            return Object.entries(props).reduce((acc,[id,value]) => ({...acc,

                [id] : {

                    ...check(value).with(provided(Object.values(value ?? {} as MultilangDto).some(v=>!!v?.length),requireLanguages(config.intl.required ??[], t ))).now()

                } as Validation

            }),{} as ValidationReport)

        }
    }

    return self
}