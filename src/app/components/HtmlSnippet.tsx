import { stripHtmlTags } from "#app/utils/common"
import * as React from "react"
import _ from 'lodash'
 
type Props = {

    snippet: string
    className?:string
    style?: React.CSSProperties
    stripHtml?: boolean
}


export const HtmlSnippet = (props:Props) => {

   const {snippet,className, style, stripHtml=false} = props


   return stripHtml ? 
        <span className={`html-snippet ${className ||''}`} style={style} >{_.unescape(stripHtmlTags(snippet))}</span>
   :
        <span className={`html-snippet ${className ||''}`} style={style} dangerouslySetInnerHTML={{
            // __html: (snippet ?? '').match(/<p>(.*)<\/p>/)?.[1] ?? snippet //
            __html: snippet //
        }} />
    


}