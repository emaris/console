import { Badge, Checkbox, Empty, Icon, Input, Skeleton, Tooltip } from "antd"
import { Draggable } from "#app/dnd/Draggable"
import { Droppable } from "#app/dnd/Droppable"
import { useT } from '#app/intl/api'
import { ClearFilters, useFilterState } from "#app/utils/filter"
import fastsort from "fast-sort"
import { TFunction } from "i18next"
import { debounce, isArray } from "lodash"
import * as React from "react"
import BaseTable, { Column as BaseColumn, ColumnShape } from "react-base-table"
import 'react-base-table/styles.css'
import { AiOutlineArrowDown, AiOutlineArrowUp } from "react-icons/ai"
import { icns } from "../icons"
import { TransitionContext } from "../scaffold/Page"
import { childrenIn } from "../utils/children"
import { asGenerator } from "../utils/common"
import { Buttons } from "./Button"
import { ListState } from "./hooks"
import { ThrottledAutoResizer } from "./ThrottledAutoResizez"

type BaseColumnProps<T> = ColumnShape<T>

export type BarProps<T> = {

    data: T[]
    total?: number

    filtered?: boolean

    filterKey? : string
    filterGroup?: string
    clearFilters?: boolean

    // filter components
    filters?: React.ReactNode[]

    filterWith?: (filter: string, t: T) => boolean
    filterBy?: (t: T) => string,
    filterPlaceholder?: string
    debounceFor?: number

    countBadge?: 'left' | 'right'
    decorations?: React.ReactNode[]
}

export type DataProps<T> = {

    className?: string
    rowClassName?: string | ((_: RowData<T>) => string)
    style?: React.CSSProperties

    onDrop?: (from: number, to: number) => void
    dropIf?: (_: T) => boolean

    skeletonOnTransition?: boolean

    height?: number
    rowHeight?: number
    headerHeight?: number
    fixedHeight?: boolean // Force the table to calculate the height based on the input object length

    state?: ListState<T>
    data: T[]
    rowKey?: string | number

    fixed?: boolean
    selectable?: boolean
    selectAll?: boolean

    actions?: (item: T) => React.ReactNode | React.ReactNode[]

    onDoubleClick?: (_: T, rowIndex: number, rowKey: any) => any

    offsetTop?: number

    sortBy?: SortSpec[]

    showHeader?: boolean

    customProperties?: (_: RowData<T>) => Object
    customRowRenderer?: (_: RowData<T>) => JSX.Element

    renderEmpty?: () => JSX.Element
    emptyMessage?: string

    children: any

    scrollToRow?: number

    // clients can change this to force a refresh of all memoisations carried out by the table to increase performance on re-renders.
    refreshFlag?: any


    id?: string
    debug?: boolean

    filterActive?: boolean,
    countFilter?: (_?: T) => boolean

}

export type TableProps<T> = BarProps<T> & DataProps<T>

export type RowData<T> = { columns, rowData: T, rowIndex: number }

export const VirtualTable = <T extends Object>(props: TableProps<T>) => {

    const {
        id = 'table',
        debug,
        data,
        total,
        decorations,
        customProperties,
        filtered = true,
        filterBy = JSON.stringify,
        filterPlaceholder,
        filterWith = (f, i) => (filterBy(i) ?? '').toLowerCase().includes(f),
        filters,
        children,
        actions,
        sortBy,
        onDoubleClick,
        refreshFlag,
        countFilter,
        countBadge,
        filterKey,
        filterGroup,
        clearFilters=true,
        ...rest
    } = props


    
    const [uncontrolledFilter, setUncontrolledFilter] = React.useState<string | undefined>(undefined!)

    const filterCtx = useFilterState(filterGroup ?? '')

    const filter = filterGroup ? filterCtx.get(filterKey ?? 'vtable') : uncontrolledFilter
    const setFilter = filterGroup ? filterCtx.set(filterKey ?? 'vtable') : setUncontrolledFilter

    // eslint-disable-next-line
    const memofilterWith = React.useCallback(filterWith, [refreshFlag])

    //  filter, if required.  (interaction with sort, means it's hard to cache this too)
    // eslint-disable-next-line 
    const filtereddata = React.useMemo(() => filter ? data.filter(t => memofilterWith(filter, t)) : data, [data, filter, memofilterWith, refreshFlag])

    const initialDataLength = React.useRef(total ?? data.length)

    const refreshableInitialDataLength = React.useMemo(() => total ?? initialDataLength.current, [total,initialDataLength])

    const filterActive = refreshableInitialDataLength !== data.length || data.length !== filtereddata.length

    // refreshes columns also when data changes, so that new renderers and decorations are detected.
    // eslint-disable-next-line
    const memochildren = React.useMemo(() => children, [data, refreshFlag])

    // eslint-disable-next-line
    const memoactions = React.useMemo(() => actions, [refreshFlag, data])

    // eslint-disable-next-line
    const memodoubleclick = React.useCallback(() => onDoubleClick, [refreshFlag])

    // eslint-disable-next-line
    const memoSortyBy = React.useMemo(() => sortBy, [refreshFlag])

    // eslint-disable-next-line
    const memocustomproperties = React.useMemo(() => customProperties, [refreshFlag])

    return <>

        {(filtered || !!decorations?.length || !!filters?.length) &&

            <TableBar filterActive={filterActive} count={countFilter ? filtereddata.filter(countFilter).length : filtereddata.length} countBadge={countBadge}
                filters={ filterGroup && clearFilters ? [...filters ??[],  <ClearFilters enableOnReadOnly key='clearfilters' group={filterGroup} /> ] : filters}
                decorations={decorations}
                debounceFor={props.debounceFor}
                filter={filter}
                setFilter={setFilter}
                filterPlaceholder={filterPlaceholder}
            />

        }

        <InnerTable {...rest}
            data={filtereddata}
            actions={memoactions}
            customProperties={memocustomproperties}
            sortBy={memoSortyBy}
            onDoubleClick={memodoubleclick}
            debug={debug}
            id={id}
            filterActive={filterActive}
            refreshFlag={refreshFlag}>{memochildren}</InnerTable>

    </>

}

export type SortMode = 'asc' | 'desc'

export type SortSpec = [string, SortMode]

const defaultComparator = new Intl.Collator(undefined, { numeric: true, sensitivity: 'base' }).compare



export const InnerTable = React.memo(<T extends Object>(props: DataProps<T>) => {

    const t = useT()



    const {
        id,
        debug,
        state = {} as any,
        data,
        rowKey = "id",
        offsetTop,
        selectable = true,
        selectAll,
        actions,
        onDoubleClick,
        children,
        renderEmpty,
        sortBy,
        filterActive,
        skeletonOnTransition = true,
        headerHeight = props.rowHeight,
        showHeader = true,
        fixedHeight,
        height,
        onDrop,
        dropIf = () => true,
        customProperties = () => ({}),
        customRowRenderer,
        scrollToRow, ...rest } = props


    // -----------------------------  (multi) sort handling

    // indexes column props by dataKey for easy lookup.
    const colpropsByKey = React.useRef(childrenIn(props).toArray().reduce((acc, next) => ({ ...acc, [next.props.dataKey]: next.props }), {}))


    // keep sort directives: initially provided by client, then changed by user by clicking on headers.
    const [sortSpecs, setSortSpecs] = React.useState<SortSpec[]>(sortBy ?? [])

    const sortRows = () => {

        const colprops = colpropsByKey.current

        // adapt our directives for fast-sort.
        const adaptedspecs = sortSpecs
            .map(spec => {

                if (!colprops[spec[0]])
                    throw Error(`invalid sort spec: no column for key '${spec[0]}'`)

                return [spec, colprops[spec[0]]]

            })  // pair up (spec,matching col)
            .map(([spec, col]) => ({ [spec[1]]: col.dataGetter ?? (s => s), comparer: col.comparator ? isArray(col.comparator) ? spec[1] === 'asc' ? col.comparator[0] : col.comparator[1] : col.comparator : defaultComparator }))

        fastsort(data).by(adaptedspecs)
    }

    // sort rows whenever data or specs change.
    React.useMemo(() => {

        if (sortSpecs.length > 0)
            sortRows()

        // eslint-disable-next-line
    }, [sortSpecs, data])


    const flipSortMode = (mode: SortMode): SortMode => mode === 'asc' ? 'desc' : 'asc'

    const onSortChange = (e: React.MouseEvent, colKey: string, spec: SortSpec | undefined) => {

        const { shiftKey } = e  // capture event outside callback (before object gets recycled)

        setSortSpecs(v => {

            // alt key ? then add to multisort
            return shiftKey ?

                // change direction of preselected
                spec ?

                    v.map(v => v[0] === spec[0] ? [v[0], flipSortMode(spec[1])] : v)
                    :

                    // or appending new col to sort
                    [...v, [colKey, 'asc']]

                // no alt key? monosort (lose previous multisort if any)
                :
                // change direction of preselected
                spec ?

                    [[spec[0], flipSortMode(spec[1])]]

                    :

                    [[colKey, 'asc']]
        })

    }

    const sortProps = (c: ColumnProps<T>) => {

        if (c.props.sortable !== undefined && c.props.sortable === false)
            return {}

        const spec = sortSpecs.find(spec => spec[0] === c.props.dataKey)

        return {
            sortSpecs, //  just to force change
            sortMode: spec?.[1],
            onSortChange: (e: React.MouseEvent) => onSortChange(e, c.props.dataKey, spec)

        }
    }



    // -----------------------------------

    const onSelected = (item: T) => {
        const index = state.selected.findIndex(s => s[rowKey] === item[rowKey])
        const newstate = index < 0 ? [...state.selected, item] : [...state.selected.slice(0, index), ...state.selected.slice(index + 1)]
        state.setSelected(newstate)
    }

    const onAllSelected = () => {

        const newstate = state.selected.length === data.length ? [] : data
        state.setSelected(newstate)

    }

    const rowClassNames = (args: RowData<T>) => `table-row ${props.rowClassName ? asGenerator(props.rowClassName)(args) : ''}`

    const { transitionOngoing } = React.useContext(TransitionContext) ?? {}

    const vTableRowHeight = props.rowHeight || 50
    const vTableHeaderHeight = showHeader ? headerHeight ?? 50 : 0

    const calculatedheight = data.length === 0 ? 200 : (data.length * vTableRowHeight) + vTableHeaderHeight
    const vTableHeight = height ? (Math.min(calculatedheight, height)) : (fixedHeight ? (calculatedheight) : 'auto')

    // Use a callback as ref to have the scrollToPosition triggered anytime the ref is attached to the BaseTable
    const tableRef = React.useCallback((tbl) => {
        tbl && tbl.table.scrollToPosition({ scrollTop: tbl.table.props.rowHeight * (scrollToRow ? scrollToRow : 0) })
    }, [scrollToRow])

    const ActionButtons = React.memo(({ row }: { row: any }) => <Buttons>{actions!(row)}</Buttons>)

    const DndPlaceHolder = DnDRow(data.length, null, null, true)

    const emptyMessage = filterActive ? t('common.components.table.no_match') : props.emptyMessage

    return <div className={`vtable ${props.className ?? ''}`} style={{ height: vTableHeight === "auto" ? "100%" : "auto" }} >

        <div className="vtable-body" style={{ height: vTableHeight, maxHeight: vTableHeight }}>
            <ThrottledAutoResizer>
                {({ width, height }) => {

                    return skeletonOnTransition && transitionOngoing ?

                        <div style={{ height: vTableHeight || height, width, padding: "20px 80px" }}>
                            {Array(5).fill(0).map((_, i) =>
                                <Skeleton key={i} />
                            )}
                        </div>

                        :

                        <BaseTable ref={tableRef}

                            {...rest}
                            ignoreFunctionInColumnCompare={false}
                            emptyRenderer={renderEmpty || (() => <Empty className={filterActive ? 'filter-active' : ''} image={Empty.PRESENTED_IMAGE_SIMPLE} style={{ marginTop: 50 }} description={emptyMessage} />)}
                            className={`table-rows ${props.className ?? ''}`}
                            rowClassName={rowClassNames}
                            data={data}
                            headerClassName="table-header"
                            headerHeight={vTableHeaderHeight}
                            width={width}
                            height={height}
                            rowProps={onDrop ? ({ rowIndex, rowData }) => dropIf(rowData) && ({ tagName: DnDRow(rowIndex, rowData, onDoubleClick), onDrop }) : customRowRenderer ? props => ({ tagName: customRowRenderer, ...customProperties(props) }) : undefined}
                            rowEventHandlers={onDoubleClick && { 'onDoubleClick': ({ rowData, ...rest }) => onDoubleClick(rowData, rest.rowIndex, rest.rowKey) }}
                            rowKey={rowKey}
                        >



                            {/* selection and actions columns take a data property for the sole purpose to force a refresh of each row when data changes.
                                this appears to forego optimisation but both columns are stateful and need to work on fresh state even if a row hasn't changed directly,
                                or future changes will be with respect to stale state. 
                            */}


                            {selectable &&
                                <BaseColumn data={data} key="__selection" rowKey={rowKey}
                                    minWidth={60} width={60}
                                    selected={state.selected || []}
                                    onSelected={onSelected}
                                    onAllSelected={onAllSelected}
                                    headerRenderer={selectAll ? SelectionHeaderCellRenderer : null}
                                    cellRenderer={SelCell} />
                            }

                            {
                                childrenIn(props).toArray().map(c => c.type({

                                    ...c.props,
                                    t,
                                    ...sortProps(c)
                                })
                                )

                            }

                            {actions &&

                                <BaseColumn data={data} minWidth={40} width={40} key="__actions"
                                    align='right' frozen='right'
                                    cellRenderer={({ rowData: u }) => <ActionButtons row={u} />} />
                            }

                        </BaseTable>


                }
                }

            </ThrottledAutoResizer>

        </div>


        {onDrop && <DndPlaceHolder onDrop={onDrop} />}

    </div>

}) as <T extends Object> (props: DataProps<T>) => JSX.Element

type InternalColumnProps<T> = {

    title?: string
    key?: any
    dataKey: string

    headerIcon?: string


    dataGetter?: (item: T, column: BaseColumnProps<T>, columnIndex: number, rowIndex: number, columns: BaseColumnProps<T>[]) => React.ReactNode

    // The comparator can get a compare function that is used to sort columns asc and desc.
    // If we want a different sorting behavior for asc a sort we can set an array of compare function.
    //    the one at index 0 is used for asc and the one with index 1 is used for desc.
    comparator?: { (t1: any, t2: any): number }[] | ((t1: any, t2: any) => number)

    headerClassName?: string
    className?: string
    style?: React.CSSProperties


    width?: number
    maxWidth?: number
    minWidth?: number

    flexGrow?: number
    flexShrink?: number

    cellRenderer?: (props: CellRenderProps<T>) => any
    headerRenderer?: (props: HeaderRenderProps) => any
    headerTooltip?: React.ReactNode

    align?: 'left' | 'center' | 'right'
    frozen?: 'left' | 'right' | boolean

    sortable?: boolean
    resizable?: boolean
    hidden?: boolean

    // injected by table, not clients
    t?: TFunction
    sortMode?: 'asc' | 'desc'
    onSortChange?: () => void

    decorations?: ((props: CellRenderProps<T>) => any)[]

}


export type ColumnProps<T> = InternalColumnProps<T> & { [other: string]: any }



type HeaderRenderProps = {


    columns, column, columnIndex, headerIndex, container

}

export const Column = <T extends Object>(props: ColumnProps<T>) => {

    const { t, sortMode, onSortChange, resizable = true, dataGetter = t => t, headerIcon, width = 200, minWidth = 50, cellRenderer, headerRenderer, headerClassName, headerTooltip, decorations = [], ...rest } = props
    const headerClasses = `table-header-cell ${headerClassName} ${props.resizable && "resizable"}`

    const baserenderer = cellRenderer || (({ cellData }) => cellData)

    const renderer = args => <span className="BaseTable__row-cell-text table-cell" >{baserenderer(args)}</span>

    const decoratingRenderer = (props: CellRenderProps<T>) => {

        return <div className="vtable-cell">
            <div className="vtable-cell-data">
                {renderer(props)}
            </div>
            <div className="vtable-cell-decorations"> {decorations.map((d, i) => <span key={i}>{d(props)}</span>)}</div>


        </div>
    }

    const hrendererBase = headerRenderer ? headerRenderer : headerIcon ? ({ column }) => <span className="BaseTable__row-cell-text table-header-cell">{column.title}</span> : undefined
    const hrendererTooltip = (props) => <Tooltip title={headerTooltip ? headerTooltip : ""}>{hrendererBase ? hrendererBase(props) : props.column.title}</Tooltip>
    const hrenderer = props => <Header {...props} sortMode={sortMode} onSortChange={onSortChange}>{headerTooltip ? hrendererTooltip : hrendererBase}</Header>

    return <BaseColumn key={`${props.dataKey}`}
        dataGetter={({ rowData, column, columnIndex, rowIndex, columns }) => dataGetter(rowData, column, columnIndex, rowIndex, columns)}
        sortable={false}
        resizable={resizable}
        width={width} a
        minWidth={minWidth}
        cellRenderer={decoratingRenderer}
        headerRenderer={hrenderer}
        headerClassName={headerClasses}
        {...rest} />
}


type HeaderProps = HeaderRenderProps & {

    children: (_: HeaderRenderProps) => JSX.Element
    sortMode: SortMode
    onSortChange: (e: React.MouseEvent) => void

}

export const Header = (props: HeaderProps) => {

    const { children, sortMode, onSortChange, ...rest } = props

    return <div className="vtable-header" onClick={onSortChange}>
        <div className="vtable-header-content">{children ? children(props) : rest.column.title}</div>
        {sortMode && <div className="vtable-header-sort">
            <Icon component={sortMode === 'asc' ? AiOutlineArrowUp : AiOutlineArrowDown} />
        </div>}
    </div>

}

export type CellRenderProps<T> = {

    cellData: any

    column: BaseColumnProps<T>,
    columnIndex: number,
    rowData: T,
    rowIndex: number,
    container: typeof BaseTable,
    isScrolling: boolean

}


const SelCell = <T extends Object>({ rowData, column }: CellRenderProps<T>) => {

    return <Checkbox className="table-selection-checkbox" checked={!!column.selected.find(s => s[column.rowKey] === rowData[column.rowKey])} onChange={() => column.onSelected(rowData)} />
}

const SelectionHeaderCellRenderer = ({ column }: HeaderRenderProps) => {

    const nonempty = column.selected.length > 0
    const full = column.selected.length > 0 && column.selected.length === column.data.length

    return <Badge count={column.selected.length} offset={[8, -8]} overflowCount={9999} >
        <Checkbox checked={full} indeterminate={nonempty && !full} onChange={() => column.onAllSelected()} />
    </Badge>
}






type RowDndItem = {

    type: "vtable-row",
    rowIndex: number
}

const DnDRow = (rowIndex, rowData, onDoubleClick, placeholder = false) => (props) => {

    const { className, style, children, onDrop } = props

    return <Droppable<RowDndItem> onDoubleClick={() => onDoubleClick(rowData)} className={placeholder ? 'vtable-placeholder' : 'vtable-row'} path={rowIndex} types={["vtable-row"]} onDrop={(path, item) => onDrop(item.rowIndex, path)}>
        <Draggable<RowDndItem> style={style} item={{ type: "vtable-row", rowIndex }} className={className}>
            {children}
        </Draggable>
    </Droppable>

}

export type TableBarFilterProps = {

    count: number
    countBadge?: 'left' | 'right'
    filterPlaceholder?: string
    debounceFor?: number
    filter: string | undefined
    setFilter: (_: string | undefined) => void
    filterActive: boolean
    filters?: React.ReactNode[]

}

export type TableBarDecorationProps = {

    decorations: React.ReactNode[]

}

export type TableBarProps = TableBarFilterProps & Partial<TableBarDecorationProps> & {

    filtered?: boolean

}


export const TableBar = (props: TableBarProps) => {

    const { filtered = true, decorations, ...rest } = props

    return <div className="vtable-bar">

        {filtered &&

            <TableBarFilter {...rest} />
        }
        {decorations &&
            <TableBarDecorations decorations={decorations} />
        }

    </div>
}


export const TableBarFilter = React.memo((props: TableBarFilterProps) => {

    const t = useT()

    const { count, countBadge='left', filterActive, filter, filters, setFilter, debounceFor = 200, filterPlaceholder = t("common.components.table.filter_placeholder") } = props

    //eslint-disable-next-line
    const debouncedSetFilter = debounce(setFilter, debounceFor)

    const badge =  <Badge showZero className={`table-search-badge badge-${countBadge} ${filterActive ? 'filter-active' : ''}`} overflowCount={9999} count={count} />

    const ref = React.useRef<Input>(undefined!)

    // resyncs on external changes, like resets.
    React.useEffect(()=>{

        if (filter!==ref.current?.input.value)
            ref.current?.setValue?.(filter!)

    },[filter])

    return <div className='vtable-bar-search'>
        {countBadge==='left' && badge }
        <Input ref={ref} className="table-search-box"
            suffix={icns.search}
            defaultValue={filter}
            onChange={e => debouncedSetFilter(e.target.value.toLowerCase())}
            placeholder={filterPlaceholder} />
        {filters && filters.length > 0 && <div className="vtable-bar-filters"> {filters.map((d, i) => <div key={i} className="vtable-bar-inner-filter">{d}</div>)} </div>}
        {countBadge==='right' && badge }
    </div>
})



export const TableBarDecorations = React.memo((props: TableBarDecorationProps) => {

    const { decorations } = props

    return <div className="vtable-bar-decorations"> {decorations.map((d, i) => <div key={i} className="vtable-bar-inner-decoration">{d}</div>)} </div>
})