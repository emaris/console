
import { Empty, Tooltip } from "antd";
import { icns } from "#app/icons";
import debounce from "lodash/debounce";
import React, { CSSProperties, PropsWithChildren, ReactNode, ReactText, useEffect, useRef, useState } from 'react';
import 'react-base-table/styles.css';
import { useT } from '#app/intl/api';
import AutoSizer from "react-virtualized-auto-sizer";
import { ListOnItemsRenderedProps, ListOnScrollProps, VariableSizeList } from "react-window";
import { Button } from "./Button";
import { Label } from "./Label";

type Props<T> = {

    className?: string
    style?: CSSProperties

    height: number
    width: number

    itemIcon?: ReactNode


    loadingPlaceholder?: string | JSX.Element

    // data mgmt
    data: T[]
    loadNext: () => Promise<T[]>
    hasNext: boolean

    itemKey: (_: T) => ReactText
    renderItem: (_: T) => JSX.Element

    notificationCount?: number

    onScroll: (rows: T[], fullyScrolled: boolean) => void

    debug?: boolean

    apiRef?: (_: BackwardInfiniteListApi) => void

}


export type BackwardInfiniteListApi = {

    fullyScrolled: () => boolean
    scroll: (index?: number) => void

}


// an outer wrapper: sorts out sizing and provides defaults, then hands off to an inner component. 
export const BackwardInfiniteList = <T extends Object>(props: Partial<Props<T>>) => {

    const t = useT()

    const [loading, setLoading] = useState(false)

    const { height,
        className = '',
        style,
        width,
        itemKey = () => `id-${Math.random()}`,
        renderItem = t => <span>{itemKey ? itemKey(t) : 'item'}</span>,
        hasNext = false,
        loadingPlaceholder,
        data = [],
        onScroll = () => { },
        loadNext = () => Promise.resolve([]), ...rest } = props


    // customises a defaul placeholder or makes room for a fully custom one.
    const defaultPlaceholder = plural => <Label icon={icns.loading(true)} title={t('common.feedback.loading_more', { plural: plural })} />
    const placeholder = loadingPlaceholder ? typeof loadingPlaceholder === 'string' ? defaultPlaceholder(loadingPlaceholder) : loadingPlaceholder : defaultPlaceholder(undefined)

    // stretches to fill available space wherever fixed dimensions aren't provided.
    return <div className={`backwardinfinitelist ${className}`}>
        <div className={`bil-batch-loader ${loading ? 'active' : ''}`} >
            {placeholder}
        </div>
        <div className='list-contents'>
            <div style={{ height: height ?? '100%', width: width ?? '100%' }}>
                <AutoSizer>
                    {({ height, width }) => {

                        return <InnerList {...rest}
                            renderItem={renderItem}
                            hasNext={hasNext}
                            data={data}
                            onScroll={onScroll}
                            loadNext={loadNext}
                            height={height}
                            itemKey={itemKey}
                            loading={loading}
                            setLoading={setLoading}
                            width={width} />

                    }}

                </AutoSizer>
            </div>
        </div>
    </div>

}



type InnerProps<T> = Props<T> & {

    loading: boolean
    setLoading: (_: boolean) => void
}

const InnerList = <T extends Object>(props: InnerProps<T>) => {

    const t = useT()

    const { loading, setLoading, hasNext, itemKey, renderItem, loadNext, data, onScroll, height, width, itemIcon, debug } = props

    const [scrolled, setScrolled] = useState(false)

    const [preview, setPreview] = useState<T[]>([])

    const listRef = useRef<any>()

    // tracks items appended since downloading the first batch. 
    // along with a flag that indicates wheher they have been seen by the user.
    const [unseen,setUnseen] = useState<Record<string, boolean>>({})

    // tracks the last item to recognise when new items are appended to the bottom of the list,
    // so we know we should keep the list scrolled in onItemsRendered().
    const lastKey = useRef<string | number | undefined>(undefined)

    // the heights of each item, measured on-the-fly when previewing each incoming batches.
    const heights = useRef<Record<string, number>>({})




    const api: BackwardInfiniteListApi = {

        fullyScrolled: () => scrolled,
        scroll: index => listRef.current?.scrollToItem(index ?? data.length - 1, 'end')

    }

    const readyToLoad = !loading && hasNext
    const sumOfHeights = Object.values(heights.current).reduce((a, b) => a + b, 0)
    const currentListHeight = Math.min(height, sumOfHeights)
    const scrollable = sumOfHeights > height
    const unseenCount = Object.values(unseen).length

    // passes latet api to client.
    useEffect(() => {

        props.apiRef?.(api)

    })
    
    // previews appended items, if any, keeping the list scrolled if it was.
    // eslint-disable-next-line
    useEffect(() => {

        // have new elements been appeneded at the bottom? if so schedule to preview them and learn their height.
        const lastIndex = lastKey.current ? data.findIndex(i => itemKey(i) === lastKey.current) : -1
        const appended = lastIndex >= 0 ? data.slice(lastIndex + 1) : []

        if (appended.length > 0) {

            // if (debug) console.log('previewing appended items..')

            setPreview(appended)


            // keeps the list fully scrolled.
            if (scrolled)
               
                api.scroll()

            else {

                const tracked = appended.reduce((acc,n)=>({...acc,[itemKey(n)]:true}),{} as Record<string,boolean>)
                setUnseen(v=>({...v,...tracked}))
               
            }

        }

        // updates record of last element for future checks.
        lastKey.current = data[data.length - 1] ? itemKey(data[data.length - 1]) : undefined
    })


    // scrolls to bottom when the viewport fills up.
    useEffect(() => {

        if (scrollable)
            api.scroll()

        // eslint-disable-next-line
    }, [scrollable])


    // after each load, load more to fill up viewport or - if viewport is full - scroll back to previous page.
    useEffect(() => {

        if (loading)                        // wait for loading to finish.
            return

        if (!scrollable && readyToLoad)     // fill viewport.
            loadMore()


        // or scroll to previous.
        else listRef.current?.scrollToItem(preview.length, 'start')


        // eslint-disable-next-line
    }, [loading])


    // passed on to items to report height after rendering.
    const setHeight = (key, s) => {

        heights.current[key] = s
        listRef.current.resetAfterIndex(0, false)
    }


    const loadMore = () =>

        Promise.resolve(setLoading(true))
            .then(loadNext)
            .then(setPreview)   // this renders the preview to compute actual heights
            .then(() => setLoading(false)) // this renders with all known heights





    const onListScroll = debounce(({ scrollDirection, scrollOffset }: ListOnScrollProps) => {

        if (scrollDirection === 'backward' && scrollOffset === 0 && readyToLoad)
            loadMore()

    })

    const onItemsRendered = (p: ListOnItemsRenderedProps) => {

        const nowScrolled = p.visibleStopIndex === p.overscanStopIndex

        setScrolled(nowScrolled)

        const showing = data.slice(p.visibleStartIndex, p.visibleStopIndex + 1)

        // untrack items in view
        showing.forEach(i=>delete unseen[itemKey(i)])
        setUnseen(unseen)

        // call back client.
        onScroll(showing, nowScrolled)

    }


    if (debug)
        console.log("render with", data, "preview", preview, "heights", heights.current, "unseen",Object.keys(unseen),"ready", readyToLoad)



    return <div style={{ height, width }}>

        {data.length === 0 && !hasNext ?

            <Empty className='bil-nodata' image={Empty.PRESENTED_IMAGE_SIMPLE} />

            :

            <>

                <div className={`bil-scroll-indicator ${!scrolled ? 'active' : ''}`} >
                    <Tooltip  placement="bottomRight" mouseEnterDelay={1} title={t('common.feedback.scroll_to_bottom')}>
                        <Button enabledOnReadOnly shape='circle' icn={icns.down} onClick={() => api.scroll()} />

                    </Tooltip>
                </div>

                <div className={`bil-unseen-indicator ${unseenCount ? 'active' : ''}`} >
                    <Tooltip placement="bottomRight" title={t('common.feedback.new_items', { count: unseenCount })}>
                        <Button enabledOnReadOnly shape='circle' icn={itemIcon} type='primary' onClick={() => api.scroll()}>{itemIcon ? undefined : (unseenCount || undefined)}</Button>
                    </Tooltip>
                </div>

                {/* renders hidden preview of latest new batch to compute heights. */}
                <div style={{ height: 0, overflow: 'hidden' }} > {

                    preview.map((t, index) => <Item key={itemKey(t)} itemKey={itemKey(t)} setHeight={setHeight} >
                        {renderItem(preview[index])}
                    </Item>
                    )
                }
                </div>

                <VariableSizeList<RowItemProps<T>['data']> ref={listRef} height={currentListHeight} width={width}
                    onScroll={onListScroll} onItemsRendered={onItemsRendered}
                    itemCount={data.length}

                    // react-window passes this to RowItems so that they can render.
                    itemData={{ items: data, renderItem, setHeight, itemKey }}

                    itemKey={i => itemKey(data[i])}
                    itemSize={i => data[i] ? heights.current[itemKey(data[i])] ?? 0 : 0}>

                    {RowItem}

                </VariableSizeList>
            </>

        }

    </div>


}



type ItemProps = PropsWithChildren<{

    itemKey: ReactText

    setHeight: (index: ReactText, _: number) => void

}>

// wraps the items provided by the client so as to measure their height on mount.
const Item = ({ itemKey, children, setHeight }: ItemProps) => {

    const ref = useRef<HTMLDivElement>(null)

    // on mount, record real item height.
    useEffect(() => {

        setHeight(itemKey, ref.current?.getBoundingClientRect().height ?? 0)

        // eslint-disable-next-line
    }, [])

    return <div ref={ref}>
        {children}
    </div>
}


// wraps a data item as a row in the react-window's table, which wants to apply styles with absolute positioning.
// the 'data' property is react-window's idiomatic way to propagate data and functions required for rendering and
// provided in the 'itemData' property. (it could be substituted with a proper react'context).

type RowItemProps<T extends Object> = {

    index: number

    style?: CSSProperties

    data: { items: T[], itemKey: (_: T) => ReactText, renderItem: (_: T) => JSX.Element, setHeight: (key: ReactText, _: number) => void }

}

const RowItem = <T extends Object>(props: RowItemProps<T>) => {

    const { index, style, data } = props

    const { items, itemKey, setHeight } = data

    const item = items[index]

    return <div style={style}>
        <Item itemKey={itemKey(item)} setHeight={setHeight}>
            {data.renderItem(item)}
        </Item>
    </div>
}
