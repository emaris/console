import { Skeleton } from "antd"
import * as React from "react"
import { WaitPage } from "../scaffold/Page"



export const Placeholder = {

    none : <div style={{padding:20,display:"flex"}}/>
    
    ,

    page: <WaitPage />

    ,
    
    list: <div style={{padding:20}}>
            <Skeleton avatar active/>
            <Skeleton avatar active/>
         </div> 
    ,

    text:< Skeleton title paragraph={{ rows: 1 }}/>
    

}
  