
import * as React from "react";
import { showFailure } from "../utils/feedback";

//  handles gracefully uncaught errors generated when rendering
export class ErrorBarrier extends React.Component<{},any> {

  constructor(props) {
    super(props);
    this.state = { error: false}
  }


  componentDidCatch(error, info) {
    
    showFailure({ message:error.toString(), details: info.componentStack, onOk : close => { 
              // close(); 
              // this.setState({error:false});
              window.location.reload()  
          }  // clears state for next render
      })

  }

  //  the state generated when failure occurs
  static getDerivedStateFromError = () => ({ error: true })
  

  render() {

    const sprawl = {  height: "100vh", width: "100vw", background: "black", margin: 0 }

    return this.state.error ? <div style={sprawl}/> : this.props.children;
  
  }
}