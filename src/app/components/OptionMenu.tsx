import { Checkbox, Dropdown, Menu } from 'antd'
import { Button } from '#app/components/Button'
import { icns } from '#app/icons'
import * as React from 'react'
import { useT } from '#app/intl/api'

export const useOptionMenu = <T extends any = any>(initiallySelected: T[] = [], initiallyVisible?: boolean) => {


    // state is in refs so we can memoise the proxy _and: access the latest data.
    // because we use refs, we trigger renders manually when the ref value changes. 
    const selected = React.useRef(initiallySelected)
    const visible = React.useRef(initiallyVisible)

    const [, render] = React.useState(0)

    // updates options and trigger a render
    const setSelected = options => {
        selected.current = options
        render(i => ++i)
    }

    // updates options and trigger a render
    const setVisible = value => {
        visible.current = value
        render(i => ++i)
    }

    // if we don't memoize, React sees elements of different components and remounts them at each render.
    const StableProxy = React.useCallback(

        (props: Partial<ClientMenuProps>) => <OptionMenu<T> {...props} selected={selected.current} setSelected={setSelected} visible={visible.current} setVisible={setVisible} />

        , [])


    return {

        OptionMenu: StableProxy,
        Option: Option,
        Divider: Divider,
        selected: selected.current,
        setSelected,
        visible: visible.current,
        setVisible
    }


}


export type BaseMenuProps<T = any> = {

    selected?: T[]
    setSelected: (_: T[]) => any

    visible?: boolean
    setVisible?: (_: boolean) => any
}

export type ClientMenuProps<T = any> = React.PropsWithChildren<{

    style?: React.CSSProperties
    className?: string


    id: (_: T) => string | number

    placeholder: React.ReactNode
    placeholderIcon: React.ReactNode

    noHighlight: boolean

    noSelectAll: boolean

    allOptionsAsUndef: boolean

}>

export type OptionMenuProps<T extends any = any> = BaseMenuProps<T> & Partial<ClientMenuProps<T>>

const selectAllOptionThreashold = 3

// display selectable options and reports on their selection.
export const OptionMenu = <T extends any = any>(props: OptionMenuProps<T>) => {

    const t = useT()

    const { id = t => t, selected = [], setSelected, visible, setVisible } = props

    const { className, style, placeholder = t("common.components.menu.option_placeholder"), placeholderIcon = icns.settings, noHighlight, noSelectAll, allOptionsAsUndef=true } = props

    let allOptions: T[] = []

    const [innerVisible, innerVisibleSet] = React.useState(visible)

    const normaliseValueOnAdd = (val:T[]) => ((val.length === allOptions.length) && allOptionsAsUndef === true ) ? undefined! : val

    const children = React.Children.toArray(props.children).map((child: any, i) => {

        if (child.type === Option) {

            const { value, label } = child.props as OptionProps<T>
            const index = selected.findIndex(o => id(o) === id(value))
            const checked = index >= 0

            const onClick = () => setSelected(checked ? selected.filter((_, j) => j !== index) : normaliseValueOnAdd( [...selected, value]))

            allOptions.push(value)

            return <Menu.Item key={`menuitem_${i}`}>
                <Checkbox style={{ display: 'flex', alignItems: 'center' }} checked={checked} onClick={onClick}>
                    {label}
                </Checkbox>
            </Menu.Item>

        }

        if (child.type === Divider)
            return divider(`divider_${i}`)



        return child
    })

    const previousOptions = React.useRef(allOptions)

    const isEqual = (a: T, b: T): boolean => {
        if (typeof a === 'object' && typeof b === 'object') {
            const keyA = Object.keys(a as Object)
            const keyB = Object.keys(b as Object)
            if (keyA.length !== keyB.length) return false
            return !keyA.map(ka => keyB.includes(ka) ? isEqual(a?.[ka], b?.[ka]) : false).some(a => !a)
        }
        if (
            (typeof a === 'string' && typeof b === 'string') ||
            (typeof a === 'number' && typeof b === 'number') ||
            (typeof a === 'boolean' && typeof b === 'boolean')
            ) 
            return a === b

        return false
    }

    React.useEffect(() => {

        // new options?

        const newOptions = allOptions.filter(o => previousOptions.current.findIndex(oo => id(o) === id(oo)) < 0).filter(o => !selected.map(s => isEqual(s, o)).some(s => s===false))

        if (newOptions.length > 0)
            setSelected(normaliseValueOnAdd([...selected, ...newOptions]))


        previousOptions.current = allOptions

        // removed options?

        const removedOptionIds = selected.filter(o => allOptions.findIndex(oo => id(o) === id(oo)) < 0).map(o => id(o))

        if (removedOptionIds.length > 0)
            setSelected(selected.filter(o => !removedOptionIds.includes(id(o))))


        // console.log("reconciled",allOptions,"new",newOptions,"removed",removedOptionIds)


        //eslint-disable-next-line
    }, [allOptions.length])

    


    const useSelectAll = noSelectAll || (allOptions.length > selectAllOptionThreashold)
    const allSelected = selected.length === allOptions.length
    const onSelectAll = () => setSelected(allSelected ? [] : normaliseValueOnAdd(allOptions))

    const menu = <Menu  >
        {useSelectAll && <Menu.Item key='select_all'>
            <Checkbox checked={allSelected} onClick={onSelectAll}>
                {t("common.labels.select_all")}
            </Checkbox>
        </Menu.Item>}
        {useSelectAll && divider("select_all_divider")}
        {children}
    </Menu>


    return <Dropdown trigger={['click']}  className={className} overlayClassName={className} disabled={React.Children.toArray(props.children).length === 0} overlayStyle={style} overlay={menu} visible={visible ?? innerVisible} onVisibleChange={v => setVisible?.(v) ?? innerVisibleSet(v)} placement='bottomRight' >
        <Button enabledOnReadOnly className={`option-menu-icon ${allSelected ? 'no-highlight' : 'filter-active'}`} icn={placeholderIcon} iconLeft type={noHighlight || allSelected ? 'default' : 'primary'}>{placeholder}</Button>
    </Dropdown>
}



export type OptionProps<T = any> = {
    label: React.ReactNode
    value: T
}

const Option = <T extends any = any>(_: OptionProps<T>) => { return null }
const Divider = () => { return null }

OptionMenu.Option = Option
OptionMenu.Divider = Divider

const divider = (key: string) => <Menu.Divider key={key} style={{ margin: '3px 0 3px 0' }} />
