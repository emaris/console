// import { ReactElement, useMemo, useRef, useState } from "react";
import { AutoResizer, AutoResizerProps } from "react-base-table"
// import { throttle } from "lodash";
// import { Size } from "react-virtualized-auto-sizer";

export const THROTTLE_RESIZE_TIME = 180;

export type ThrottledAutoResizerProps = {
    wait?: number
} & AutoResizerProps;

export const ThrottledAutoResizer = (props: ThrottledAutoResizerProps) => {
    const { children, ...others } = props;
    // const { children, wait = THROTTLE_RESIZE_TIME, ...others } = props;
//     const [throttledValue, setThrottledValue] = useState<any>(undefined);
//     const sizeref = useRef<any>(null);
//     const childrenRetRef = useRef<any>(1);
//     const thref = useRef<any>(null);

//     const tChildren = useMemo(
//         () => {
//             thref.current && thref.current.cancel();
//             return (thref.current = throttle(
//                 size => {
//                     console.log('[++]THROTTLED:', { size });
//                     sizeref.current = size;
//                     const ret = children(size);
//                     childrenRetRef.current || setThrottledValue(ret);
//                     return ret;
//                 },
//                 wait,
//                 { trailing: true, leading: false }
//             ));
//         },
//     [ children ]);

    return <AutoResizer { ...others }>

        { size => {
            return children(size);
            // console.log('[++]ORIG:', { size });
            // if (sizeref.current?.height !== size.height || sizeref.current.width !== size.width) {
            //     childrenRetRef.current = tChildren(size);
            // }
            // return childrenRetRef.current || throttledValue;
          }
        }
    </AutoResizer>;
};
