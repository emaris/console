import { Spin } from "antd"
import { buildinfo } from "#app/buildinfo"
import { useSystem } from "#app/system/api"
import { paramsInQuery } from "#app/utils/routes"
import * as React from "react"
import { useT } from '#app/intl/api'
import { useLocation } from "react-router-dom"
import { useDelayedTruth } from "../utils/hooks"

type Props = React.PropsWithChildren<{

    id?:string
    delay?: number
    delayAfter?: number
    spinNow? : boolean

}>


// todo: revise 'pro                                                                       duction' settings based on evidence
const defaultDelay = buildinfo.development ? 50 : 100
const defaultDelayAfter = buildinfo.development ? 0 : 200

export const BusySpinner = (props:Props) => {

    const t = useT()

    const {search} = useLocation()
    const system = useSystem()
    

    const lastMessage = React.useRef<string|undefined>(undefined)
   
    const {id:targetDrawer,children,delay=defaultDelay, spinNow, delayAfter=defaultDelayAfter} = props

    const busytask = system.findBusy()

    const openDrawers = Object.keys(paramsInQuery(search)).filter(p=>p.includes('drawer'))

    let canHandleIt = targetDrawer ? openDrawers.includes(targetDrawer) : openDrawers.length===0


    // there must be a running task, and if a drawer is open then we need a clear signal to handle it.
    let show = spinNow || (canHandleIt && !!busytask?.status)

    //  hiding the spinner too soon after showing it will flash the user. 
    //  this smoothens things by keeping the spinner up for a bit longer-
    let smoothShow = useDelayedTruth(show,delayAfter,delay)

    // tracks last known message in case we need to preserve it in smooth show.
    if (busytask)      
        lastMessage.current = t(busytask.msg ?? "common.feedback.sync")

    React.useEffect(() => {

        if(busytask?.msg){
            window.dispatchEvent(new CustomEvent("spin-msg", { detail: busytask.msg }))
        }
    
        } ,[busytask?.msg])


    //console.log("delay",delay,"task",busytask,"show",show,"smoothly",smoothShow)

    return  <Spin delay={delay} spinning={smoothShow} tip={lastMessage.current} size={'small'} >   
                    {children}
            </Spin> 
   
  }


