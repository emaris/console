import { useGlobalRef } from '#app/utils/hooks'
import _ from 'lodash'
import * as React from "react"

export type ListState<T> = {

  selected: T[]
  setSelected: React.Dispatch<React.SetStateAction<T[]>>
  resetSelected: () => void
  removeOne: (t: T) => void

  noSelection: boolean

}


export const useListState = <T>(): ListState<T> => {

  const [selected, setSelected] = React.useState<T[]>([])

  const resetSelected = React.useCallback(() => setSelected([]), [])
  const noSelection = React.useMemo(() => selected.length === 0, [selected])

  const removeOne = React.useCallback((t: T) => setSelected(selected.filter(s => !_.isEqual(t, s))), [selected])

  const state = React.useMemo(() => ({ selected, setSelected, resetSelected, noSelection, removeOne }), [selected, resetSelected, removeOne, noSelection])

  return state

}


// memorises scroll poosition in local storage when unomounting, then restores it.
// scroll position may be relative to an element, and restoration may be conditional and/or scoped to an identifier.

type RememberProps = {

  id?: string,
  ref?: React.MutableRefObject<any>,
  when?: boolean,
  restoreWhen?: boolean
}

export const useRememberScroll = (key: string, props: RememberProps): void => {

  const { id, ref, when = true, restoreWhen = true } = props

  const [state, stateSet] = useGlobalRef(key, { id, scroll:0 })

  React.useLayoutEffect(() => {

    if (state.scroll !== 0 && restoreWhen && state.id === id) {

      window.scrollTo(0, state.scroll)
      
      //console.log("resetting scroll",state.scroll,id)

      return () => stateSet({ id, scroll: 0 })

    }


  }, [restoreWhen, state, stateSet,id])

  // remembers on unmount
  React.useLayoutEffect(() => {


    return () => {

      // eslint-disable-next-line
      const scrollPosition = window.scrollY + (ref?.current.offsetTop ?? 0)

      if (when) {

        //console.log("remembering scroll:", scrollPosition, id)
        
        stateSet({ id, scroll: scrollPosition })
      }

    }

    // eslint-disable-next-line
  }, [])
}