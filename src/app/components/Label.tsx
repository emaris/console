import { useT } from '#app/intl/api'
import { useRoutedTypes } from '#config/model'
import { Tag, Tooltip } from "antd"
import * as React from "react"
import { Link } from "react-router-dom"
import { icns } from "../icons"

//  NOTE ON PERFORMANCE
//  -------------------
//  labels are performance-sensitive because they can pack a rich array of components (tooltips,links,icons).
//  rendering costs accumulate rapidly when labels occur in table cells, and become a performance bottleneck.

//  memoization limits the impact in re-renders. re-renders may be triggered by table filters,
//  drawer overlays, and more generally when tables co-occur with other components in rich pages, like dashboards.
//  memoizing the tables themselves isn't enough, because most labels are connected to the global state and re-render
//  in sync with its changes, unrelated though they may be. the base label here isn't connected,
//  but most labels built on top it are. these labels must either memoize the element they return (with React.useMemo()),
//  or else split into a 'connected component' and a 'pure component'. the connected component re-renders always but 
//  at low cost, the pure component can be memoized and re-render only to show actual changes. the first builds on the second
//  injecting relevant state into it.

//  a further complication is that labels have a few object properties that don't memoise easily, like decoration 
//  and option arrays, tooltip and link functions, style objects. typically these objects don't have stable identities 
//  at the client, ie. change systematically even when they render the same. and yet, in most cases they render constantly 
//  throghout the lifecycle of the label. so it'd be impractical to ask clients to memoize them systematically.

//  given all the above, the current strategy is to re-render labels only when:
//  - selected primitive properties change, adding more as evidence shows they might change during the lifecycle of labels. 
//  - noMemo=true, which explicitly foregoes memoization in uses cases where there are genuine changes to object properties. 

//  the strategy is implemented in sameLabel() which serves as the comparison function for React.memo().
//  as a function, it can be reused by labels that build on top of the base one, provided they continue to prefer 
//  React.memo() over React.useMemo(). Since the connected/pure split comes at the cost of more boilerplate, however,
//  labels can start with the simpler useMemo() and split as need arises.

export type LabelProps = {

    title?: React.ReactNode
    icon?: React.ReactNode

    noIcon?: boolean
    iconClassName?: string
    iconStyle?: React.CSSProperties

    noLink?: boolean
    linkTo?: string | (() => string)
    linkStyle?: React.CSSProperties
    linkTarget?: string

    tip?: () => React.ReactNode
    tipDelay?: number
    tipClassName?: string
    tipPlacement?: 'top' | 'left' | 'right' | 'bottom' | 'topLeft' | "topRight"
    noTip?: boolean

    decorations?: React.ReactNode[]
    noDecorations?: boolean

    options?: React.ReactNode[]
    noOptions?: boolean

    mode?: 'light' | 'tag' | 'option'

    className?: string

    style?: React.CSSProperties
    titleStyle?: React.CSSProperties
    titleClassName?: string

    noMemo?: boolean

    isProtected?:boolean
    
    readonly?: boolean
    selected?: boolean
    inactive?: boolean

    locale?: string

    strikethrough?: boolean
    disabled?: boolean
   
}


export const Label = React.memo(function PureLabel(props: LabelProps) {

    const routedTypes = useRoutedTypes()
    const t = useT()

    const { title, icon = icns.generic, mode = 'light', tip, tipPlacement, noTip, tipDelay, tipClassName, noIcon,
        linkTarget, noLink, linkTo, linkStyle = {}, decorations = [], noDecorations, options = [], noOptions,
        titleStyle, iconStyle, style, className = '', iconClassName='', titleClassName='', strikethrough=false, readonly, isProtected, inactive, disabled, selected } = props

    // we an icon and the client is not raising objections.
    const useIcon = icon && !noIcon

    const titleContent = <span style={{ color: "$primary-color", ...titleStyle }} className={`label-title ${titleClassName}`}>{title}</span>

    let content = <div className="label-main">
        {useIcon ? <span style={{ marginRight: 6, ...iconStyle }} className={`label-icon ${iconClassName}`}>{icon}</span> : null}
        {(tip && !noTip) ? <Tooltip overlayClassName={tipClassName} placement={tipPlacement} mouseEnterDelay={tipDelay} title={props.tip}>{titleContent}</Tooltip> : titleContent}
    </div>

    // client says to, we have a route, and it's not the one
    const useLink = !noLink && linkTo && (!linkTarget || routedTypes.includes(linkTarget))

    let decos = [...decorations]

    if (mode === 'option')
        return <div className={`label ${className}`}>{content}</div>

    if (useLink && linkTo)
        content = <Link className='label-link' style={{ display: "flex", alignItems: "center", ...linkStyle }} to={linkTo}>
            {content}
        </Link>

    if (!noDecorations && props.mode !== 'light') {
        if (inactive)
        decos.unshift(<Tooltip title={t("common.labels.inactive")}>{icns.inactive}</Tooltip>)

        if (readonly || isProtected)
            decos.unshift(<Tooltip title={readonly ? t("common.labels.readonly") : t("common.labels.protected")}>{icns.readonly()}</Tooltip>)

         content = <div style={{ display: "flex", alignItems: "center" }}>
            {content}
            {decos.map((d, i) => <span key={i} className="label-decoration">{d}</span>)}
        </div>
    }

    if (!noOptions)
        content = <div style={{ display: "flex", alignItems: "center" }}>
            {content}
            {options.map((d, i) => <span key={i} className="label-option">{d}</span>)}
        </div>

    const disabledClass = disabled ? `label-disabled` : ''
    const strikethroughClass = strikethrough ? `label-strikethrough` : ''
    const selectedClass = selected ? `label-selected` : ''

    const classes = `${className} ${disabledClass} ${strikethroughClass} ${selectedClass}`
    return mode === 'light' ? <div style={style} className={`label ${classes}`}>{content}</div> : <Tag style={style} className={`label label-tag ${classes}`}>{content}</Tag>


}, sameLabel)

export function sameLabel($: LabelProps, $$: LabelProps) {

    return !$$.noMemo &&
        $.title === $$.title &&
        $.tip === $$.tip &&
        $.className === $$.className &&
        $.strikethrough === $$.strikethrough &&
        $.disabled === $$.disabled &&
        $.inactive === $$.inactive &&
        $.isProtected === $$.isProtected &&
        $.locale === $$.locale
}


export const UnknownLabel = (props: Partial<LabelProps>) => {

    const t = useT()

    const { title = t("common.labels.unknown"), tip = () => t("common.labels.unknown"), ...rest } = props

    return <Label className="unresolved" {...rest}  tip={tip} icon={icns.unknown} title={title} />

}

