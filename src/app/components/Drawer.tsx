import { useReadonly } from '#app/form/ReadonlyBox';
import { icns } from "#app/icons";
import { useSystem } from "#app/system/api";
import { useFeedback } from '#app/utils/feedback';
import { useStable } from '#app/utils/function';
import { paramsInQuery, updateQuery } from "#app/utils/routes";
import { Drawer as AntDrawer, Icon } from "antd";
import { DrawerProps as AntDrawerProps } from "antd/lib/drawer";
import { ModalFuncProps } from "antd/lib/modal";
import Title from "antd/lib/typography/Title";
import * as React from "react";
import { useHistory, useLocation } from "react-router-dom";
import { ReadOnly } from "../scaffold/ReadOnly";
import { BusySpinner } from "./BusySpinner";
import "./styles.scss";




export type DrawerProps = {

  routeId?: string
  icon?: JSX.Element
  readonly?: boolean,
  enabledOnReadOnly?: boolean,
  children: any
  warnOnClose?: boolean
  warnMessage?: Partial<ModalFuncProps>
  name?: string
  title: React.ReactNode

  renderAfterTransition?: boolean

} & AntDrawerProps


export const Drawer = (props: DrawerProps) => {


  const {
    routeId,
    children,
    warnMessage,
    className,
    closable = false,
    visible = true,
    width = 500,
    title,
    icon = icns.generic,
    warnOnClose,
    afterVisibleChange,
    renderAfterTransition,
    placement = "right",
    readonly,
    enabledOnReadOnly,
    ...rest } = props

  const readonlyProps = useReadonly({ readonly, enabledOnReadOnly })


  const classes = className ? "slideout " + className : "slideout"

  const wrappedTitle = <Title style={{ marginBottom: 0 }} level={3}>{typeof (icon) === 'string' ? <Icon type={icon} /> : icon} <span>{title}</span></Title>

  const { findBusy } = useSystem()

  const fb = useFeedback()

  // inhibts close when busy spinning
  const onClose = findBusy()?.status || !props.onClose ? () => { } : props.onClose

  const [transitioned, setTransitioned] = React.useState(!renderAfterTransition)

  return <AntDrawer

    {...rest}

    className={classes}
    title={wrappedTitle}
    closable={closable}
    width={width}
    placement={placement}
    visible={visible}
    afterVisibleChange={visible =>{
      
      afterVisibleChange?.(visible) 
      
      // if we'just become visible and we weren't rendered yet, render now.
      if (visible && renderAfterTransition)
          setTransitioned(true)
        
    }}

    onClose={e => warnOnClose ? fb.askConsent({ ...(warnMessage ? warnMessage : fb.unsavedConsentDialog()), noValediction: true, onOk: () => onClose(e) }) : onClose(e)} >


    <BusySpinner spinNow={!transitioned} delay={0} id={routeId}>
      <ReadOnly value={readonlyProps.readonly}>

      {transitioned &&
          children 
        }
      </ReadOnly>

    </BusySpinner>

  </AntDrawer>

}

type AnchorProps = React.PropsWithChildren<{

  id: string,
  style?: React.CSSProperties,
  className?: string

  offset?: number

}>


//  caters for linking to internal anchors - typically from TOCs - which would end up covered underneath the header or other sticky content (eg. topbar).
//  trick is standard: anchor is placed on a first span child that is:
//  - negatively-positioned enough to offset the total height of the sticky headers. so the span is covered but its next siblings are not. 
//  - invisible, so that is doesn't cover the content above it.

export const DrawerAnchor = (props: AnchorProps) => {

  // the default offset (90) assumes just the drawer header (64), must be overridden for extra sticky headers, eg. topbars.

  const { id, children, style, className, offset = 90 } = props

  // pushes children lower
  const offsetStickyHeaders: React.CSSProperties = {
    visibility: "hidden",
    display: "block",
    position: "relative",
    top: -offset
  }

  return <div className={`anchorTarget ${className ?? ''}`} style={style}>
    <span id={id} style={offsetStickyHeaders} />
    {children}
  </div>
}




//  returns a Drawer component that:
//  a) can be controlled, ie. opened,closed,inspected.
//  b) is routed, ie. the current URL for control state.

export type RoutableDrawerProps = Partial<{

  id: string
  title: React.ReactNode
  icon: JSX.Element
}>

export const useRoutableDrawer = (props: RoutableDrawerProps = {}) => {

  const history = useHistory()
  const { pathname, search,hash } = useLocation()

  const { id = "param", title = "<Title Here>" } = props
  const param = `drawer-${id}`

  const visible = !!paramsInQuery(search)[param]

  const route = (open: boolean = true) => `${pathname}?${updateQuery(search).with(ps => ps[param] = open ? 'true' : null)}${hash}`

  const open = () => history.push(route())
  const close = () => history.push(route(false))
  

  const Proxy = useStable((props: Partial<DrawerProps>) => <Drawer title={title} routeId={param} key={param} {...props} onClose={e => {  close();  props.onClose?.(e)}} visible={visible}>{props.children}</Drawer>)

  return { Drawer: Proxy, visible, open, close, route, param }
}
