
import { arrayOf } from '#app/utils/common';
import { useFeedback } from '#app/utils/feedback';
import { paramsInQuery, updateQuery } from '#app/utils/routes';
import * as React from "react";
import { Prompt, Redirect, RouteComponentProps, useHistory, useLocation, withRouter } from "react-router";

type Props = RouteComponentProps & {
    when:boolean
    onOk?: ()=>void
}

export const RouteGuard =  withRouter ( ({when,onOk=()=>null,match:{url}}:Props)  =>{

    const [path,set] = React.useState<string>(undefined!)

    const fb = useFeedback()

    return path ? <Redirect to={path} /> : <Prompt when={when} message={ location => {

        if (location.pathname===url )
            return true;

        fb.askConsent({ ...fb.unsavedConsentDialog(),onOk: ()=> {onOk(); set(location.pathname);} })
            
        return false }} />

})

type AdavncedProps = React.PropsWithChildren<{
    when:boolean
    onOk?: ()=>void
    ignoreQueryParams?: string | string[]
    escapePattern?: string
}>

export const AdvancedRouteGuard =  (props:AdavncedProps)  =>{

    const history = useHistory()

    const location = useLocation()

    const {when,ignoreQueryParams=[],escapePattern, onOk,children} = props

    const fb = useFeedback()

    return <>{

        <Prompt when={when} message={targetLocation => {

            const targetRoute = `${targetLocation.pathname}${targetLocation.search}`

            const ignores = arrayOf(ignoreQueryParams) 

            const currentSearch = ignores.length === 0  ? location.search : updateQuery(location.search).with(params => {

                const targetParams = paramsInQuery(targetLocation.search)
              
                // augment current url with ignored parameters from target url.
                ignores.forEach(i => params[i] = targetParams[i])

                // aughment current url with all escape-matching parameters from target url.
                if (escapePattern)
                    Object.keys({...params, ...targetParams}).filter( key => key.startsWith(`drawer-${escapePattern}`)).forEach(key => params[key] = targetParams[key])


            })


            const samePath = location.pathname === targetLocation.pathname

            if (targetLocation.state === 'pass' || (samePath && currentSearch===targetLocation.search.replace("?","")))
                return true

            fb.askConsent({ ...fb.unsavedConsentDialog(),onOk: () => {onOk?.(); history.push(targetRoute, 'pass')}})

            return false

        }} />


    }

        {children}

    </>

}