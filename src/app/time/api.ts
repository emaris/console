
import { useT } from '#app/intl/api'
import { allLanguages, Language } from '#app/intl/model'
import { useIntl } from '#app/intl/store'
import { MultilangDto } from '#app/model/multilang'
import { useLogged } from '#app/user/store'
import moment, { Moment } from "moment-timezone"
import { useContext } from 'react'
import { timeformats } from "./constants"
import { TimeContext } from './context'
import "./styles.scss"
import { useTimeZone } from './hooks'


export const useTime = () => {

    const s = useContext(TimeContext)

    const t = useT()
    const intl = useIntl()

    const logged = useLogged()

    const { timezone: defaultTimezone } = useTimeZone()

    const self = {

        setCurrent: (newtime: moment.Moment) => {

            console.log("resetting time to", newtime.format())

            const newZone = newtime.tz()

            const now = newZone ? moment().tz(newZone, true) : moment()

            // negative if we're moving into the past.
            const shift = newtime.diff(now)

            s.set(s => s.time = { ...s.time, currentZone: newZone, shift })


        }

        ,

        reset: () => s.set(s => s.time = { shift: 0, currentZone: undefined })

        ,

        current: () => {

            const { shift, currentZone } = s.get().time

            const now = currentZone ? moment().tz(currentZone, true) : moment()

            if (!shift || shift === 0)
                return now

            // will subtract a negative difference, going into the past.
            return now.add(shift)


        }

        ,

        formatsFor: (date: Moment | string | undefined, timezones?: Record<string, string>) => {


            if (!date)
                return []

            const home = logged.details.preferences?.location

            const value = (typeof date === 'string' ? moment(date) : date).clone()

            const ret = Object.keys(timeformats).map(id => {

                var zone

                const f = timeformats[id]

                const timezone = (timezones ? timezones[id] : undefined)

                switch (id) {
                    case 'home': zone = timezone || home || moment.tz.guess(); break;
                    case 'local': zone = timezone || moment.tz.guess(); break;
                    case 'original': zone = timezone || defaultTimezone || value.tz() || moment.tz.guess(); break;

                    default: zone = 'Etc/UTC'
                }

                return { id, ...{ ...f, syntax: t(f.syntax) }, zone }

            })


            return ret
        }

        ,

        preferredFormatFor: (date: Moment | string | undefined, timezones?: Record<string, string>) => {


            const preferred = logged.details.preferences?.dateLocation || 'home'

            const formats = self.formatsFor(date, timezones)

            return (formats.find(f => f.id === preferred) ?? formats.find(f => f.id === 'home'))!

        }

        ,


        format: (date: Moment | string | undefined, mode: 'short' | 'long' = 'long', locale?: Language) => {

            const currentLocale = locale ?? intl.currentLanguage()
            return date ? moment(date).locale(currentLocale === 'en' ? 'en-gb' : currentLocale).format(mode === 'short' ? 'l' : self.preferredFormatFor(date)?.syntax ?? 'lll') : ''

        }
        ,

        formatAsMultilang: (date: Moment | string | undefined, mode: 'short' | 'long' = 'long'): MultilangDto =>
            allLanguages.reduce((acc, lang) => ({ ...acc, [lang]: self.format(date, mode, lang) }), {})
        ,

        auxiliaryFormats: () => [
            "DD-MM-YYYY HH:mm",
            "DD-MM-YYYY",
            "DD/MM/YYYY HH:mm",
            "DD/MM/YYYY",
            "DDMMYYYYHHmm",
            "DDMMYYYY HHmm",
            "DDMMYYYY"
        ]



    }

    return self
}