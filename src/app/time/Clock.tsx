

import { useLogged } from '#app/user/store'
import * as React from "react"
import { useTime } from "./api"
import { useInterval } from "./hooks"
import { TimeLabel } from "./Label"
import { useTimePicker } from "./TimePickerBox"


export const Clock = ()=>{

    const time = useTime()

    // eslint-disable-next-line
    const memotime = React.useMemo(()=>time,[])

    const logged = useLogged()

    const username = logged.username

    // eslint-disable-next-line
    const memologged = React.useMemo(()=>logged,[username])

    return <InnerClock time={memotime} logged={memologged} />
}



const InnerClock = React.memo( (props:{time:ReturnType<typeof useTime>,logged:ReturnType<typeof useLogged>}) => {

    // const t = useT()

    const {time} = props

    const {Picker:TimePicker,open:isOpen} = useTimePicker();

    // const canEdit = logged.can(standardActions.doAnything)

    return  <div className="clock">
        
                { isOpen ? 

                        <TimePicker light showToday={false} className="clock-picker" size="small" allowClear={false} value={time.current()} onChange={time.setCurrent}   /> 
                    : 
                   
                    /*<div className={`clock-face ${canEdit ? "editable":''}`}>
                      <div className="clock-ctrl" onClick={open}><Tooltip title={()=>t("common.buttons.edit")}>{icns.edit}</Tooltip></div>
                      <div className="clock-ctrl" onClick={time.reset}><Tooltip title={()=>t("common.buttons.reset")}>{icns.revert}</Tooltip></div>
                       
                       <LiveDisplay />
                    </div>*/
                    <LiveDisplay />
                }

            </div>
})


export const LiveDisplay = () => {

    const [count,update]  = React.useState(0)

    const time = useTime()

    // console.log('rendering', time.current())

    // on first render (count==0), delay is seconds to the start of the minute. Otherwise, it's exactly 60 seconds.
    const delay = (60-( count===0 ? time.current().seconds() : 0)) *1000 

    useInterval(()=> update(i=>++i), delay)

    return <TimeLabel noMemo id='clock' value={time.current()} placement="bottom" style={{fontSize:"small"}} noTip />
}