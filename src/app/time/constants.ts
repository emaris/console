

import { icns } from "#app/icons"
import { ReactNode } from "react"

// export const defaultTimeFormat = "MMM DD, YYYY HH:mm"
export const defaultTimeFormat = "time.default_format_full"

 export type TypeInfo = {name:string,syntax:string,icon:ReactNode}

 export const timeformats : {[id:string] : TypeInfo } =  {     
    
     local:  { name: 'time.misc.format_local', syntax:defaultTimeFormat,icon:icns.location},
     home:   {name: 'time.misc.format_home', syntax: defaultTimeFormat,icon: icns.home },
     original: {name: 'time.misc.format_original', syntax: defaultTimeFormat,icon: icns.layers },
     universal: {name:'time.misc.format_global', syntax: defaultTimeFormat, icon:icns.globe }

 }



