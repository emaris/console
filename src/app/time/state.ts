
export type TimeState = {

    time: {
        shift: number
        currentZone: string | undefined
    }

}

export const initialTime: TimeState = {

    time:  {

        shift: 0,
        currentZone:undefined!

    }
}
