import { Label } from "#app/components/Label"
import { Field, FieldProps } from "#app/form/Field"
import { VSelectBox } from "#app/form/VSelectBox"
import { icns } from "#app/icons"
import { ReadOnlyContext } from "#app/scaffold/ReadOnly"
import moment from "moment-timezone"
import * as React from "react"
import { useT } from '#app/intl/api'
import { useTime } from "./api"

type Props = {
    
    onChange: (_:string) => void
    time?: moment.Moment
    value: string | undefined

    disabled?:boolean

    includeLocal?:boolean

    veryLight?: boolean
}

export const ZonePickerBox = (props:FieldProps & Props) => {

    const t = useT()

    const defaultValidation = {msg:t("common.fields.timezone.msg"),help:t("common.fields.timezone.help")}

    const {label=t("common.fields.timezone.name"),onChange, validation,includeLocal,...rest} = props

    const defaultedValidation = {...defaultValidation,...validation}

    return <Field label={label} validation={defaultedValidation} {...rest}>
                <ZonePicker includeLocal={includeLocal} onChange={onChange} {...rest} />
            </Field>
}

export const ZonePicker = (props:Props) => {

    const t = useT()
    
    const api = useTime()
    
    const {onChange,time=api.current(),value,includeLocal,disabled,veryLight=false} = props

    const timeclone = time.clone()

   
    const options = moment.tz.names()

    // eslint-disable-next-line
    const formats = React.useMemo(()=>options.reduce((acc,n)=>({...acc,[n]:timeclone.tz(n).format('z Z')}),{}),[])

   const localzone = t("time.misc.current_location")

    const initialValue = value || (includeLocal && localzone) || (!disabled && undefined)

    const [currentValue, setCurrentValue] = React.useState(initialValue)

    const readonly = React.useContext(ReadOnlyContext)

    
    return  veryLight? 
            <select className="timezone-picker" value={value} onChange={(event:any)=>{onChange(event.target.value); setCurrentValue(event.target.value);}}>
                {
                    options.map((o,idx)=><option key={idx} value={o}>{o}</option>)
                }
            </select>

            : <VSelectBox className="timezone-picker" light  
                        hideSelectedOptions={false}
                        undefinedOption={includeLocal ? localzone : undefined}
                        options={options}
                        disabled={disabled || readonly}
                        lblTxt={z=>z}
                        optionId={z => z}
                        renderOption={z =>  typeof(z) === 'object' ? currentValue :
                                <Label icon={icns.clock} title={`${z} (${formats[z]})`}/>
                        } 
                        onChange={(v:any)=>{
                            onChange(v);
                            setCurrentValue(v)
                        }} >
                {[initialValue]}
            </VSelectBox>
            
}