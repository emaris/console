import { Text } from "#app/components/Typography"
import { Field, FieldProps } from "#app/form/Field"
import { FormContext } from "#app/form/Form"
import { ReadonlyProps, useReadonly } from '#app/form/ReadonlyBox'
import { ResetBtn } from '#app/form/resetbtn'
import { useT } from '#app/intl/api'
import { through } from "#app/utils/common"
import { DatePicker } from "antd"
import { DatePickerProps } from "antd/lib/date-picker/interface"
import moment from "moment-timezone"
import * as React from "react"
import { useTime } from "./api"
import { useTimeZone } from "./hooks"


type Props = FieldProps & ReadonlyProps & Omit<DatePickerProps, "onChange" | "onOk" | "value" | "onOpenChange"> & {


    light?: boolean,

    value: string | moment.Moment | undefined
    onChange: (_: moment.Moment) => void

    onClose: () => void

    format?: string

    initialTime?: string

    mode?: 'time' | 'date' | 'month' | 'year' | 'decade'

}



type ProxyProps = Omit<Props, "open" | "onClose">

export const useTimePicker = () => {

    const [open, setOpen] = React.useState<boolean | undefined>(undefined)

    const onOpen = React.useCallback(() => setOpen(true), [])
    const onClose = React.useCallback(() => setOpen(o => o ? false : undefined), [])         // toggles if open-controlled, preserved undefined otherwise.

    const Proxy = React.useCallback((props: ProxyProps) => <TimePickerBox {...props} open={props.value ? open : undefined} onClose={onClose} />, [open, onClose])

    return { Picker: Proxy, open, onOpen, onClose, setOpen } as { Picker: (props: ProxyProps) => JSX.Element, open: boolean, onOpen: () => void, onClose: () => void, setOpen: (_: boolean) => void }
}

export const TimePickerBox = (props: Props) => {

    const { open, light, onClose, className = '', fieldClassName = '', enabledOnReadOnly, style, placeholder,

        disabled,
        renderExtraFooter,
        onChange,
        showTime,
        mode = 'date',
        defaultValue,
        defaultPickerValue,
        format,
        value: v5,
        initialTime = '23:59',
        focusDecorations = [],
        readonly,
        readonlyMode,
        readonlyUnlocked,
        readonlyIcon,

        ...rest } = useReadonly(props)


    const t = useT()
    const time = useTime()

    const { timezone } = useTimeZone()

    const defaultTimeValue = moment(initialTime, 'HH:mm', timezone)

    const value = props.value ? typeof props.value === 'string' ? moment(props.value) : props.value : undefined

    const [selection, setSelection] = React.useState<moment.Moment | undefined>(() => value ? value.tz(timezone) : undefined)

    // resets local state when form in context does.
    const resetted = React.useContext(FormContext)?.resetted

    React.useEffect(() => {

        if (resetted)
            setSelection(value!)

    }, [resetted, value])

    // antd picker's wants to either control the opening, or to leave it to the client with the 'open' prop.
    // strangely, open===undefined is allowed but taken to mean open===false.
    // to wrap both use cases, we must avoid 'spreading' 'open' altogether when the client doesn't use it. 
    const openProp = open === undefined ? {} : { open }

    const ZoneOptions = mode => <div className="timezone-footer">

        {mode === "time" ? null :

            <>
                {timezone && <div className="timezone">
                    <div className="timezone-label">
                        <Text>{t("time.misc.timezone")}:</Text>
                    </div>
                    <div className="selected-timezone">
                        <Text>{timezone}</Text>
                    </div>
                </div>}
            </>
        }
    </div>

    const formats = [...(format ? [format] : []), time.preferredFormatFor(selection)?.syntax as string, ...time.auxiliaryFormats()].filter(f => f !== undefined)

    const modes = mode === 'date' ? {} : { mode }


    const onYearChange = mode === 'year' ?
        (t) => {
            
            defaultMode.current = false

            Promise.resolve(t === null ? undefined : t.tz(timezone, true)).then(through(t => setSelection(t!))).then(t => onChange(t!)).then(onClose)
        
        }
        
        
        : 
        
        () => null

    const onDateChange = t => {
        
        defaultMode.current = false

        Promise.resolve(t === null ? undefined : t.tz(timezone, true)).then(through(t => setSelection(t!))).then(t => onChange(t!)) 
    
    }

    // should defaulting logic trigger at all? it should on mount and after a reset. 
    // without it, defaulting would trigger also when a user blanks a field: it'd look like a reset.
    const defaultMode = React.useRef(true)
 
    // show we show default?
    const showDefault = defaultMode.current ? !!defaultValue && !selection : false

    // show we allow reset to default?
    const resettable = !defaultMode.current && !readonly && !!defaultValue && !defaultValue.isSame(selection)

    // remount if default mode or defaults change (or field will not refresh).
    const key = defaultMode.current && showDefault ? JSON.stringify(defaultValue): undefined 

     const valueprops = showDefault ? { defaultValue: defaultValue } : { value: selection! }


    const decorations = [...focusDecorations ?? []]

    // add readonly icon only if can be edited or has been unlocked. 
    if (readonlyMode==='chrome' && (readonly || readonlyUnlocked))
        decorations.push(readonlyIcon)

    if (resettable)
        decorations.push(<ResetBtn enabledOnReadonly={enabledOnReadOnly} onClick={() => {
            defaultMode.current = true; 
            setSelection(undefined)
            onChange(undefined!)

        }} />)



    const classes = `time-picker ${showDefault ? 'time-picker-default-value' : ''} ${readonly ? 'time-picker-readonly' : ''}`

    const readonlyFallback = readonly && readonlyMode === 'content' ? '-' : undefined

    const readonlyData = value ?? defaultValue

    if (readonly && readonlyMode === 'content')

        return <Field label={props.label} >
            <div className={`${classes}  ${readonlyData ? '' : 'readonly-placeholder'}`}>
                {readonlyData?.format?.(format) ?? readonlyFallback}
            </div>
        </Field>

    const picker = <DatePicker key={key} style={style} className={classes} dropdownClassName={`time-picker ${fieldClassName}`}

        disabled={disabled}

        placeholder={placeholder ?? t("time.picker.placeholder")}

        defaultPickerValue={defaultPickerValue}
        format={formats}
        showTime={showTime ? { format: 'HH:mm', defaultValue: defaultTimeValue } : false}
        renderExtraFooter={ZoneOptions}

        {...openProp}

        onPanelChange={onYearChange}
        onOpenChange={v => !v && onClose()}

        // we assert the timezone in context on the new selection, without converting to it (cf. t.tz(timezone,true))
        onChange={onDateChange}
        onOk={_ => Promise.resolve(onChange).then(onClose)}
        {...valueprops}
        {...modes} />

    return light ? <div className={`time-picker ${className}`}>{picker}</div> : <Field {...rest} focusDecorations={decorations} >{picker}</Field>
}