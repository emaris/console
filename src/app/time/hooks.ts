import { useTenantStore } from '#app/tenant/store'
import { useLogged } from '#app/user/store'
import * as React from "react"

export const TimeZoneContext = React.createContext<string | undefined>(undefined!)
const browserTimeZoneGetter = () => Intl.DateTimeFormat().resolvedOptions().timeZone

export const useTimeZone = () => {

  const logged = useLogged()
  const tenants = useTenantStore()

  const tenantTimezone = logged.hasNoTenant() ? undefined : tenants.lookup(logged.tenant)?.preferences?.location
  const loggedUserTimezone = logged.details.preferences.location

  const currentTimezone = React.useContext(TimeZoneContext) || loggedUserTimezone || tenantTimezone || browserTimeZoneGetter()

  return {timezone : currentTimezone}
}

export function useInterval(callback: ()=>any, delay?:number) {

    const savedCallback = React.useRef<(...args:any[])=>any>();
  
    // Remember the latest callback.
    React.useEffect(() => {
      savedCallback.current = callback;
    }, [callback]);
  
    // Set up the interval.
    React.useEffect(() => {

      function tick() {
        savedCallback.current?.();
      }

      if (delay !== null) {
        let id = setInterval(tick, delay);
        return () => clearInterval(id);
      }

    }, [delay]);
  }