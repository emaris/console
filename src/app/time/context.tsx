import { fallbackStateOver, State } from 'apprise-frontend-core/state/api';
import { createContext } from 'react';

import { initialTime, TimeState } from './state';



export const TimeContext = createContext<State<TimeState>>(fallbackStateOver(initialTime))
