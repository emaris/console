
import { Label, LabelProps, sameLabel } from "#app/components/Label";
import { icns } from "#app/icons";
import { useLogged } from '#app/user/store';
import moment from "moment-timezone";
import * as React from "react";
import { useT } from '#app/intl/api';
import { useTime } from "./api";
import { timeformats } from "./constants";




type Props = LabelProps & {

    value?: moment.Moment | string
    className?: string
    style?: React.CSSProperties

    displayMode?: "default" | "relative",
    format?: 'numbers' | "short" | "day"
    withTime?: boolean
    placement?: 'top' | 'left' | 'right' | 'bottom'

    nullTime? : string

    render?: (formatted: string) => string

    accentMode?: "alert" | "record"
    accentFrame?: "none" | "months" | "weeks" | "days" | AccentFrame

    noGreyAccent?: boolean

    id?: string

    timezones? : Record<string,string>

    prefix?: string

    suffix?: string

}

const sameTimeLabel = ($:Props, $$:Props) => sameLabel($,$$) && $.value === $$.value

export const TimeLabel = (props: Props) => {

    const intl = useT()
    const logged = useLogged()
    const time = useTime();

    return <InnerTimeLabel {...props} logged={logged} time={time} intl={intl} locale={props.locale ?? moment.locale()}/>

}

const InnerTimeLabel = React.memo((props: Props & {
    logged: ReturnType<typeof useLogged>,
    time: ReturnType<typeof useTime>,
    intl: ReturnType<typeof useT>
}) => {

    const { value: input, intl:t, time, logged, nullTime=undefined } = props
    const { className = '', locale=moment.locale(), style, placement, displayMode, render = $ => $, accentFrame = 'none', accentMode = "alert", noGreyAccent=false, prefix, suffix, ...rest } = props
    


    if (input === undefined) {
        if (nullTime) {
            return <Label {...rest} className={className} style={style} title={nullTime} decorations={props.decorations} />
        }
        return null
    }

    const home = logged.details.preferences?.location

    const value = (typeof input === 'string' ? moment(new Date(input)) : input).clone()

    
    const formats = time.formatsFor(value, props.timezones)

    var format = time.preferredFormatFor(value, props.timezones)

    value.locale(locale === 'en' ? 'en-gb' : locale).tz(format.zone)

    var other = Object.values(formats).filter(f => f !== format)

    const tooltip = props.noTip ? undefined : props.tip ?? (() => other.map((f, i) => <div key={i} className="time-label-other">
        <div className="other-name">{t(f.name)}</div>
        <div className="other-value">{value.clone().tz(f.zone!).format(f.syntax)}</div>
    </div>))

    let preferredFormat = props.format === 'numbers' ? 'l' : props.format === 'short' ? 'll' : props.format === 'day' ? 'dddd D' : format.syntax 
    preferredFormat = props.withTime && props.format ? `${preferredFormat} HH:mm` : preferredFormat

    let title = render(displayMode === 'relative' ? value.from(time.current()) : value.format(preferredFormat))
    
    if (prefix) {
        title = `${prefix} ${title}`
    }
    if (suffix) {
        title = `${title} ${suffix}`
    }

    //console.log("time label", title, props.timezones, formats, format)

    // if (props.id === 'clock')
    //     console.log('rendering innertimelabel with locale,', value.locale(), locale, title, ' ---- ', value.format(preferredFormat))

    const scheme = accentFrame === 'none' ? undefined : accentFrame === 'months' ? monthsAccent : accentFrame === 'weeks' ? weeksAccent : accentFrame === 'days' ? daysAccent : accentFrame

    let accentclass = ''

    if (scheme) {
        const now = time.current()
        const hasbeen = now.isAfter(value)
        if ((hasbeen && accentMode === 'alert') || (!hasbeen && accentMode === 'record'))
            accentclass = `time-label-${accentMode}-none`
        else {
            const diff = Math.abs(value.diff(time.current(), "days"))
            const suffix = diff > scheme.soon ? "far" : diff > scheme.imminent ? "close" : "closer"
            accentclass = `time-label-${accentMode}-${suffix}`
        }
    }

    return <Label {...rest} tip={tooltip} tipPlacement={placement} icon={props.icon || (format.zone === home ? timeformats.home.icon : format.icon)} className={`time-label ${noGreyAccent ? '' : accentclass} ${className}`} titleStyle={style} title={title} />


}, sameTimeLabel)

type AccentFrame = {
    imminent: number
    soon: number
}

// TODO: extrude into confguration
const monthsAccent: AccentFrame = { imminent: 30, soon: 60 }
const weeksAccent: AccentFrame = { imminent: 5, soon: 15 }
const daysAccent: AccentFrame = { imminent: 2, soon: 5 }



type ZoneProps = {

    zone?: string
    time?: moment.Moment
    light?: boolean
    icon?: boolean
}

export const TimeZoneLabel = (props: ZoneProps) => {

    const { time: timepoint, zone, light, icon = true } = props

    const time = useTime()
  
    const logged = useLogged()


    const timeclone = (timepoint ?? time.current()).clone()
    const timezone = zone ?? moment.tz.guess()
    const zoneicon = icon && (zone === logged.details.preferences.location ? icns.home : icns.clock)

    return <Label icon={zoneicon} title={light ? timezone : `${timezone} (${timeclone.tz(timezone).format('z Z')})`} />




}