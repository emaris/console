import { Badge, Tabs } from "antd";
import * as React from "react";
import { useHistory, useLocation } from "react-router";
import { Button, ButtonGroup } from "../components/Button";
import { childrenIn, givenChildren } from "../utils/children";
import { paramsInQuery, updateQuery } from "../utils/routes";
import { Titlebar } from "./PageHeader";
import { Tab } from "./Tab";


type Props = {

    className?: string
    style?: React.CSSProperties

    autoGroupButtons?: boolean

    affix?: boolean
    offset?: number

    onTabChange?: (tab: string) => void
    activeTab?: string

    tabParam?: string
    tabBarAnimated?: boolean

    leftControls?: JSX.Element[]

    children: any
}



export const Topbar = (props: Props) => {


    const { titlebar, btns, tabs, other } = childrenIn(props).byTypes([["titlebar", Titlebar], ["btns", Button], ["tabs", Tab]])
    const defaultTabProps = tabs && givenChildren(tabs).with(props => props.default).mapProp("id")


    const defaultKey = defaultTabProps && defaultTabProps[0]

    const { autoGroupButtons = true, affix = true, offset, activeTab, onTabChange, tabParam = 'tab', leftControls, tabBarAnimated = true } = props

    const history = useHistory()
    const { pathname, search } = useLocation()

    const params = paramsInQuery(search)

    const tab = activeTab || params[tabParam]

    const style = (affix ? { position: "sticky", top: offset || 0, zIndex: 100 } : {}) as React.CSSProperties

    const onChange = onTabChange || (key => history.push(`${pathname}?${updateQuery(search).with(params => params[tabParam] = key)}`))

    const contents = <div className={`apprise-controlbar ${props.className || ''}`} style={{ ...style, ...props.style }}>
        {leftControls && <div className="controlbar-buttons-left">{leftControls}</div>}
        {tabs &&
            <Tabs destroyInactiveTabPane={false} activeKey={tab || defaultKey} onChange={onChange} animated={tabBarAnimated}>
                {tabs.map(t => {

                   const {id,name, badge,icon, children, ...rest} = t.props

                    const tabname = <span>{icon}{name}</span>  
                    const fullname = badge ? <Badge dot offset={[3,2]}>{tabname}</Badge> : tabname

                    return <Tabs.TabPane {...rest} forceRender key={id} tab={fullname}>{children}</Tabs.TabPane>
                })}
            </Tabs>
        }

        <div className="controlbar-other">
            {other}
        </div>

        <div className="controlbar-actions">

            {btns &&
                <div className="controlbar-buttons-right">{autoGroupButtons ? <ButtonGroup>{btns}</ButtonGroup> : btns}</div>
            }


        </div>

    </div>

    return <>
        {titlebar && titlebar}
        {contents}
    </>

}



