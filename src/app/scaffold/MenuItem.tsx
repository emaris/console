import { ButtonModel, ItemModel } from "./model"


type Props = ItemModel & {

    // extend if required
    
}

// render is 'dummy' becase we're just using JSX to capture the model
export const MenuItem = (props:Props) => null
export const ButtonItem = (props:ButtonModel) => null