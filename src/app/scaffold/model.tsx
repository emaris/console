import { ReactNode, CSSProperties, ReactElement } from "react"
import { Crumbs } from "./Breadcrumb"


export type ScaffoldModel = {

    title : string
    shortTitle?: string
    icon: ReactNode
    banner?: JSX.Element
    sections: SectionModel[]
    links: LinkModel[]
    sliders: SliderModel[]
    menuitems: ItemModel[]
    buttons: ButtonModel[]

}
             
export type SectionModel = {

    title: string
    icon:string
    disabled:boolean
    enabled:boolean
    route: { path: string, exact: boolean}
    crumbs: Crumbs
    content: ReactElement
    showOnSider: boolean
}

export type LinkModel = {

    title: string
    icon:ReactNode
    disabled:boolean
    enabled:boolean
    style?: CSSProperties
    href:string
}


export type SliderModel = {

    id:string
    disabled:boolean
    enabled:boolean
    content: React.ReactElement
}

export type ItemModel = {

    title:string,
    icon:ReactNode,
    onClick?: () => void,
    onClickToggle?: string,
    enabled?: boolean,
    disabled?: boolean,
    className?:string,
    style?:CSSProperties
}

export type ButtonModel = React.PropsWithChildren<{

    title:string,
    icon:ReactNode,
    onClick: () => void,
    enabled?: boolean,
    disabled?: boolean,
    className?:string,
    style?:CSSProperties,
    noTip?: boolean
}>