import * as React from "react";
import { Crumbs } from "./Breadcrumb";


type Props = {

    title:string,
    icon: string | React.ReactNode,
    disabled?:boolean,
    enabled?:boolean,
    path: string,
    exact?:boolean
    children?:any
    crumbs?:Crumbs
    showOnSider?: boolean
}

export const Section = (props:Props) => {

    
    return <div>s{props.children}</div>;

}