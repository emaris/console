import { Layout } from "antd";
import * as React from "react";
import { givenChildren } from "../utils/children";
import { falseIs, trueIs } from "../utils/common";
import { Header } from "./Header";
import { ReadOnly } from "./ReadOnly";
import { DrawerContext, ScaffoldContext } from "./Scaffold";
import { Sidebar } from "./Sidebar";
import { Sider } from "./Sider";
import { Topbar } from "./Topbar";

type Props = {
    style?: {}
    className?: string
    headerClassName?: string
    children: any
    readOnly?: boolean
    delegateReadonly?: boolean
    placeholder?: boolean
}

//  marks and control transitions, for clients that want to optimise their own rendering during transitions.
type TransitionState = {
    transitionOngoing: boolean
    setTransitionOngoing: (s: boolean) => void
}

export const TransitionContext = React.createContext<TransitionState>(undefined!)



export const Page = React.forwardRef<HTMLDivElement, Props>((props, ref) => {

    const [transitionOngoing, setTransitionOngoing] = React.useState<boolean>(false)

    const { style, className, headerClassName, children, placeholder, readOnly = false, delegateReadonly } = props;

    const { sidebar, topbar, other } = givenChildren(children).byTypes([["sidebar", Sidebar], ["topbar", Topbar]]);

    //  some pages may not be below the scaffold\
    const scaffold = React.useContext(ScaffoldContext)

    const sliderCtx = React.useContext(DrawerContext)

    const transitionValue = React.useMemo(() => ({ transitionOngoing, setTransitionOngoing }), [transitionOngoing, setTransitionOngoing])

    return <Layout className="apprise-layout" >
        <TransitionContext.Provider value={transitionValue}>

            <ReadOnly value={readOnly} delegate={delegateReadonly} >

                <Sider model={scaffold} content={sidebar} defaultOpen={sidebar?.[0].props.defaultOpen} />

                <Layout id='layout-main' className="main">

                    <Header className={headerClassName} model={scaffold} style={style} />


                    {<Layout.Content>
                        {topbar}
                        <div ref={ref} className={`main-content ${className ?? ''}`}>
                            {other}
                        </div>
                    </Layout.Content>}


                </Layout>

            </ReadOnly>

            {/* slider components */}
            {placeholder || (scaffold && scaffold.sliders
                .filter(s => trueIs(s.enabled) && falseIs(s.disabled))
                .filter(s => sliderCtx.isVisible(s.id))
                .map((s, i) => React.cloneElement(s.content, { key: i, id: s.id })))

            }

        </TransitionContext.Provider>
    </Layout>



})

export const WaitPage = () => {


    return <Page placeholder> </Page>


}



type SiderProps = React.PropsWithChildren<{

    className?: string
    style?: React.CSSProperties

    width?: number

}>

export const PageSider = (props: SiderProps) => {

    const { className = '', style = {} } = props

    return <div className={`main-sider ${className}`} style={style}>
        {props.children}
    </div>
}
