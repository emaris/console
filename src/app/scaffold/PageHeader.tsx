import { givenChildren } from "#app/utils/children";
import Title from "antd/lib/typography/Title";
import * as React from "react";
import { ReadOnlyContext, ReadOnlyLabel } from "./ReadOnly";
import { ScaffoldContext } from './Scaffold';


type Props = {

    title: React.ReactNode
    children?: any

    style?: React.CSSProperties
    className?: string

    readonlyLbl?: JSX.Element
}

export const Titlebar = React.memo(({ title, children, className = '', style, readonlyLbl }: Props) => {

    // const t = useT()
    const elementRef = React.useRef<HTMLDivElement>(null);

    const scaffold = React.useContext(ScaffoldContext)

    const readonly = React.useContext(ReadOnlyContext)

    const { subtitle, other } = givenChildren(children).byTypes([["subtitle", Subtitle]]);

    const ctxtitle = scaffold.shortTitle ?? scaffold.title

    React.useEffect(() => {

        if (typeof title === 'string')
            document.title = `${ctxtitle} - ${title}`

    }, [title, ctxtitle ])

    React.useEffect(() => {

        return () => {
            document.title = ctxtitle
        }
    }, [ctxtitle])

    React.useEffect(() => {
        const { current: elem } = elementRef;

        requestAnimationFrame(() => {
            elem!.style.height = `${ elem!.clientHeight - parseInt(getComputedStyle(elem!).paddingTop, 10) }px`;
            elem!.style.overflow = 'hidden';
        });

	// eslint-disable-next-line
    }, [title, ctxtitle, other, subtitle]);

    return <div ref={elementRef} className={`apprise-titlebar ${className} `} style={style}>

        <MainTitle>{title}</MainTitle>

        {subtitle}

        <div style={{ display: "flex", alignItems: "center" }}>
            {readonly && (readonlyLbl ?? <ReadOnlyLabel />)}
            {other}
        </div>

    </div>

})



export const MainTitle = React.memo((props: React.PropsWithChildren<{}>) => {

    return <Title className="main-title">{props.children}</Title>
})

export const Subtitle = React.memo((props: React.PropsWithChildren<{ className?: string }>) => {

    return <div className={`subtitle ${props.className ?? ''} `}>{props.children}</div>
})
