import { Badge, Tabs } from "antd";
import { TabPaneProps } from "antd/lib/tabs";
import * as React from "react";


export type TabProps = TabPaneProps & {
    
    id:string
    name:React.ReactNode 
    icon?: React.ReactNode   
    badge?: boolean
    paramType?: 'query' | 'path' // used by Topbar
    default?: boolean // used by Topbar
}


export const Tab = ({id,name, badge,icon, ...rest}: TabProps ) =>  { 

  const tabname = <span>{icon}{name}</span>  
  const fullname = badge ? <Badge dot offset={[3,2]}>{tabname}</Badge> : tabname

  return <Tabs.TabPane {...rest} key={id} tab={fullname} />
  
}
