import * as React from "react"
import Title from "antd/lib/typography/Title"

type Props = {

    title: React.ReactNode
    subtitle?:React.ReactNode

    className?: string
    style?:React.CSSProperties

}

export const ContentHeader = (props:Props) => {


    const {title,subtitle,className,style} = props

    return <div className={`content-header ${className ?? ''}`} style={style}>
                 <Title level={3} className="content-header-title">{title}</Title>
                 {subtitle && <div className="content-header-subtitle">{subtitle}</div>}
           </div>

}