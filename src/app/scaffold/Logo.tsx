import * as React from 'react'


import { Icon } from 'antd';

type LogoProps = {
    title: string,
    icon: string|React.ReactNode
}

export const Logo = React.memo(({title, icon}:LogoProps) => (
    <div className="sidebar-logo">
        {typeof icon === 'string' ? <Icon type={icon}/> : <div>{icon}</div>}
        <div className="sidebar-maintitle">{title}</div>
    </div>

),()=>true)