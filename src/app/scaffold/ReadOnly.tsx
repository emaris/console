import { Label, LabelProps } from '#app/components/Label'
import { icns } from '#app/icons'
import { useT } from '#app/intl/api'
import * as React from 'react'

type Props = {
    children: any
    value: boolean
    delegate?: boolean
}


export const ReadOnlyContext = React.createContext(false);


export const ReadOnly = (props: Props) => {

    const { children, value, delegate } = props

    const inherited = React.useContext(ReadOnlyContext) && !!delegate

    return <ReadOnlyContext.Provider value={value || inherited}>
        {children}
    </ReadOnlyContext.Provider>
}


export const ReadOnlyLab = ($: React.PropsWithChildren<any>) => <ReadOnlyContext.Provider value={true}>{$.children}</ReadOnlyContext.Provider>


export const ReadOnlyLabel = (props : LabelProps) => {

    const t = useT()

    return <Label {...props} className="readonly-tag" mode="tag" icon={icns.readonly()} title={t("common.labels.readonly")} />

}