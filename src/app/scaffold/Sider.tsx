import { Icon, Layout, Tooltip } from "antd";
import { Location } from "history";
import * as React from "react";
import { NavLink } from "react-router-dom";
import { icns } from "../icons";
import { falseIs, trueIs } from "../utils/common";
import { Logo } from "./Logo";
import { LinkModel, ScaffoldModel, SectionModel } from "./model";
import { TransitionContext } from "./Page";
import { SidebarContext } from "./Scaffold";


type Props = {

  model: ScaffoldModel
  content: React.ReactNode
  defaultOpen?: boolean

}


export const Sider = ({ content, model, defaultOpen }: Props) => {

  // why not local state? because this is rendered in-page, but we need to track collapsed status across pages.
  // so we place it in scaffold and pick it here in each page.
  const { collapsed, setCollapsed } = React.useContext(SidebarContext)
  const { setTransitionOngoing } = React.useContext(TransitionContext)

  const startTransitionAndCollapse = () => { setTransitionOngoing(true); setCollapsed(defaultedCollapsed ? 'open' : 'closed'); }

  const enabledSection = (s: SectionModel) => trueIs(s.enabled) && falseIs(s.disabled) && trueIs(s.showOnSider)
  const enabledLink = (l: LinkModel) => trueIs(l.enabled) && falseIs(l.disabled)

  const hasContent = !!content?.[0].props.children
  const defaultedCollapsed = !hasContent || collapsed === 'closed' || (collapsed === undefined && !defaultOpen)

  return <Layout.Sider className={`sidebar ${hasContent ? '' : 'no-content'} focus-mode-aware-left`} width={320} collapsible collapsed={defaultedCollapsed}
    trigger={defaultedCollapsed ? icns.right : icns.left}
    onTransitionEnd={e => { if (e.propertyName === 'width') setTransitionOngoing(false) }}
    onCollapse={startTransitionAndCollapse}>

    <Logo icon={model.icon} title={model.title} />

    <div className="sidebar-content">

      <div className="sidebar-iconbar" >

        <div className="iconbar-sections">

          {model.sections.filter(enabledSection).map((section, i) =>

            <Tooltip key={i} placement="right" title={() => section.title} >

              <NavLink className="iconbar-item section-item" exact={section.route.exact} to={section.route.path}
                activeClassName="active"
                replace={isRouteActive(section, location)}
              // onClick={e => { if (isRouteActive(section, location) && hasContent) { startTransitionAndCollapse(); e.preventDefault() } }}
              >
                {section.icon}
              </NavLink>


            </Tooltip>

          )}</div>

        <div className="button-items">

          {model.buttons.map((button, i) => {

            const contents = <div key={i} className={button.className ? `${button.className} btn-clickable` : 'btn-clickable'} style={button.style ?? {}} onClick={button.onClick}>
              {button.icon}{button.children}
            </div>

            return button.noTip ? contents : <Tooltip key={i} placement="right" title={button.title}>{contents}</Tooltip>

          }

          )}</div>

        <div className="iconbar-links">

          {model.links.filter(enabledLink).map((link, i) =>

            <Tooltip key={i} placement="right" title={() => link.title} >
              <a style={link.style} className="iconbar-item link-item" href={link.href}>
                {
                  typeof (link.icon) === 'string' ?
                    <Icon type={link.icon} />
                    :
                    link.icon
                }
              </a>
            </Tooltip>

          )}</div>

      </div>

      {defaultedCollapsed || <div className="sidebar-panel">
        {content}
      </div>}

    </div>

  </Layout.Sider>



}

const isRouteActive = ({ route: { path } }: SectionModel, { pathname }: Location<any>) => path === "/" ? pathname === path : pathname.startsWith(path)
