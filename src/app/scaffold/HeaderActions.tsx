import { Dropdown, Menu } from "antd";
import { Clock } from "#app/time/Clock";
import { UserLabel } from '#app/user/Label';
import { useLogged } from '#app/user/store';
import * as React from "react";
import { icns } from "../icons";
import { falseIs, trueIs } from "../utils/common";
import { ScaffoldModel } from "./model";
import { DrawerContext } from "./Scaffold";


type Props = React.PropsWithChildren<ScaffoldModel> 

export const HeaderActions =  (props:Props) => {
  
  const [visible, setVisible] = React.useState(false)

  const drawers = React.useContext(DrawerContext)
  
  const logged = useLogged()

  const entries = <Menu onClick={()=>setVisible(false)}>

    {props.menuitems.filter(i=>trueIs(i.enabled) && falseIs(i.disabled)).map((item,i)=>

      <Menu.Item key={i} className={item.className} style={item.style} onClick={item.onClick || (()=> drawers.open(item.onClickToggle!))}>
        {item.icon}
        <span>{item.title}</span> 
      </Menu.Item>

    )}

  </Menu>

  return  <span  className="header-actions" >
          
           <Clock />

           <div className="header-logged"><UserLabel style={{fontWeight:500,fontSize:'small'}} noIcon noLink user={logged} /></div>
          
            <Dropdown trigger={['click']} visible={visible} onVisibleChange={setVisible} overlay={entries} >
         
              <span className="header-menu">{icns.menu}</span>

            </Dropdown>
          </span>

      
  

}
