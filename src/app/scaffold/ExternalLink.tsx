
import * as React from 'react'

type Props = {

    title:string,
    icon: string | React.ReactNode,
    disabled?:boolean,
    enabled?:boolean,
    href: string,
    style?: React.CSSProperties
}

// render is 'dummy' becase we're just using JSX to capture the model
export const ExternalLink = (props:Props) => null