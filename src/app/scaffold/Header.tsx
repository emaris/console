import { Button } from "#app/components/Button";
import { useFocusMode } from "#app/components/FocusMode";
import { useT } from '#app/intl/api';
import { usePushEvents } from "#app/push/api";
import { EventBusConnectionLabel } from "#app/push/label";
import { Icon, Layout } from "antd";
import * as React from 'react';
import { BiExitFullscreen, BiFullscreen } from "react-icons/bi";
import { IoMdNotifications, IoMdNotificationsOff } from 'react-icons/io';
import { Route } from "react-router-dom";
import { Breadcrumb } from "./Breadcrumb";
import { HeaderActions } from "./HeaderActions";
import { ScaffoldModel } from "./model";
import { UpgradeButton } from '#app/push/appupgrade';
/* import { Zoomer } from "./Zoomer"; */

type Props = {
  model: ScaffoldModel
  className?: string
  style?: {}
}

export const Header = (props: Props) => {

  const t = useT()


  const events = usePushEvents()

  const { toggleFocusMode, focusModeEnabled } = useFocusMode();

 const { mode, toggleMode } = usePushEvents()

  const pushActive = mode() === 'active'

  const { model, className } = props;
  const { sections, banner } = model;
  const focusModeTip = focusModeEnabled ? t("common.focus_mode.exit") : t("common.focus_mode.enter")
  const FocusModeBtn = <span><Button type="ghost" noborder enabledOnReadOnly onClick={toggleFocusMode} tooltipDelay={0} tooltip={focusModeTip} className="focus-mode-btn">
    <Icon component={focusModeEnabled ? BiExitFullscreen : BiFullscreen} />
  </Button></span>

  const pushNotifToggleTooltip = pushActive ? t('common.push_notif_mode.active_tooltip') : t('common.push_notif_mode.inactive_tooltip')
  const pushNotifToggleStatus = t('common.push_notif_mode.inactive')

  const pushNotifButtonClass = `notif-mode-btn ${pushActive ? 'active' : 'inactive'}`
  const pushButtonIcon = <Icon component={pushActive ? IoMdNotifications : IoMdNotificationsOff} />
  
  const PushNotifToggle = <span className="notif-mode-btn-container"><Button
    type="ghost"
    noborder
    enabledOnReadOnly
    onClick={toggleMode}
    tooltipDelay={0}
    tooltip={pushNotifToggleTooltip}
    className={pushNotifButtonClass}
    iconLeft
    icn={pushButtonIcon}
  > {pushNotifToggleStatus}
  </Button></span>


  const [disconnected, setDisconnected] = React.useState(false)


  React.useEffect(() => {
    
    const interval = setInterval(() => setDisconnected(events.connection()?.isClosed()), 3000)

    return () => clearInterval(interval)

    //eslint-disable-next-line
  }, [])

  // const connectionStatus = <span className="disconnected-status">
  //   {
  //     disconnected &&
  //     <Tooltip title={t("push.connection_failure")} >{t("push.disconnected")}</Tooltip>
  //   }
  // </span>

  return <Layout.Header className={`main-header ${className ?? ''} focus-mode-aware-top`}>
    {banner}
    <Route exact={false} path="/" render={_ =>

      <Breadcrumb crumbs={sections.map(s => s.crumbs)} />}

    />
    <div className='main-header-right-buttons'>
      {/* {connectionStatus} */}
      <UpgradeButton />
      <EventBusConnectionLabel disconnectedOnly />
      {!disconnected && PushNotifToggle}
      {FocusModeBtn}
      <HeaderActions {...model} />
    </div>
  </Layout.Header>
}
