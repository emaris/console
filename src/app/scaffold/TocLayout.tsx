import { Label } from "#app/components/Label";
import { useT } from '#app/intl/api';
import { givenChildren } from "#app/utils/children";
import * as React from "react";
import { useLocation } from "react-router-dom";

type Props = React.PropsWithChildren<{

    className?: string
    style?: React.CSSProperties,

    offsetTop?:number

}>

export const TocLayout = (props:Props) => {

    const {hash}= useLocation()

    const {className='',style={}, offsetTop=10, children} = props

    const {entries, other=[]} = givenChildren(children).byTypes([["entries",TocEntry]])

    const firstHash = React.useRef(hash)

    React.useEffect(()=>{
        
        const id = firstHash.current?.slice(1)
        const el = entries?.find( l => l.props.id === id) ? document.getElementById(id) : undefined

        if (el)
            el.scrollIntoView()

    })

    return <div style={style} className={`toc-layout ${className}`}>

       {entries?.length > 0 && 
       
        <div style={{top:offsetTop}} className={`layout-toc`}>
            
            <div className={`layout-entries`}>
                { entries.map((l,i)=><Label className={`toc-label ${ (hash ? hash === `#${l.props.id}` : i===0  )  ? `toc-selected` : ''}`} key={i} icon={<span/>} title={l}/>) }
            </div>
            
        </div>
        
        }

    <div className={`layout-content`}>
        {other}
    </div>


    </div>


}

type EntryProps = {
    id:string
    title:string
}


export const TocEntry = (props:EntryProps) => {

    const t = useT()
    const {id,title} = props

    const href = `#${id}`
  

    return <a target="" href={href}>{t(title,{id})}</a>

}