import { Breadcrumb as AntBreadcrumb } from 'antd';
import { isMultilang, MultilangDto, useL } from '#app/model/multilang';
import { capitalise } from '#app/utils/common';
import * as React from 'react';
import { Link, useLocation } from 'react-router-dom';




export type Crumb = Partial<{

    name: string,
    omit: boolean,
    resolver: CrumbResolver
}>

export type CrumbResolver = (seg: string, path: string) => string | MultilangDto | undefined

export type Crumbs= { [_: string]: Crumb }



type Props = {
    crumbs: Crumbs[]
}


const toItems = (path: string, crumbs: Crumbs) => {


    return [{ seg: '', path: '/' }].concat(

        path.split('/')
            .filter(seg => seg)  // remove start/end spaces
            .map((seg, i, self) => ({ seg, path: "/" + self.slice(0, i + 1).join('/') }))

    )
        .map(({ seg, path }) => {

            const matched = match(path, crumbs)

            return matched ? { ...matched, path, seg } : { name: seg, path, omit: false, resolver: undefined, seg }

        })

}

const match = (path: string, crumbs: Crumbs): Crumb | undefined => {

    const segs = path.split("/")

    const key = Object.keys(crumbs).find(key => {

        const keysegs = key.split("/")
        return keysegs.length === segs.length && !keysegs.some((s, i) => s !== "*" && s !== segs[i])

    })

    return key ? crumbs[key] : undefined
}

export const Breadcrumb = (props: Props) => {

    const { pathname } = useLocation()
   
    
    const l = useL()

    const { crumbs } = props

    const allcrumbs = React.useMemo(() => crumbs.reduce((acc, s) => ({ ...acc, ...s })), [crumbs])


    const items = toItems(pathname, allcrumbs);

    const resolve = (i: typeof items[0]) => {

        const resolved = i.resolver?.(decodeURIComponent(i.seg), i.path) ?? i.name ?? i.seg

        return capitalise(isMultilang(resolved) ? l(resolved) : resolved)
    }

    return items.length === 1 ? null : <AntBreadcrumb>

        {items.filter(i => !i.omit).map((i, j) =>
            <AntBreadcrumb.Item key={i.name ?? j}>
                <Link to={decodeURI(i.path)}>{resolve(i)}</Link>
            </AntBreadcrumb.Item>
        )
        }
    </AntBreadcrumb>
}