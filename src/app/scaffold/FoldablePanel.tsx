import { Button } from '#app/components/Button'
import { icns } from '#app/icons'
import * as React from 'react'

type Props = React.PropsWithChildren<{

    icon?: React.ReactElement
    title?: string

    height: number
    startOpen?: boolean

    className?: string
    style?: React.CSSProperties

}>


const entryMargin = 10

export const FoldablePanel = (props: Props) => {

    const { title, height, icon = icns.generic, startOpen, className = '', style, children } = props

    const [open, setOpen] = React.useState(startOpen)

    const togglePanel = () => setOpen(v => !v)

    const effectiveHeight = height + (entryMargin * 2 * (React.Children.count(children) + 1))

    return <div className={`foldable-panel ${className}`} style={style}>

            <div className={`foldable-panel-header`} onDoubleClick={togglePanel} >
                <div>{icon}</div>
                <div style={{ marginLeft: 5, flexGrow: 1 }}>{title}</div>
                <Button enabledOnReadOnly icn={open ? icns.up : icns.down} size="small" light onClick={togglePanel} />
            </div>

            <div className={`foldable-panel-content ${open ? 'panel-open' : ''}`} style={{ maxHeight: open ? effectiveHeight : 0 }}>
                {React.Children.map(children, c => <div className="panel-entry">{c}</div>)}
            </div>

        </div>


}