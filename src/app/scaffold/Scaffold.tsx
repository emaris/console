import 'antd/dist/antd.css';
import { FocusModeProvider } from '#app/components/FocusMode';
import { FilterContext } from '#app/utils/filter';
import { paramsInQuery, updateQuery } from "#app/utils/routes";
import * as React from "react";
import { Route, Switch, useHistory, useLocation } from "react-router-dom";
import { NoSuchRoute } from "../components/NoSuchRoute";
import { childrenIn } from "../utils/children";
import { falseIs, trueIs } from "../utils/common";
import { ExternalLink } from "./ExternalLink";
import { ButtonItem, MenuItem } from "./MenuItem";
import { ScaffoldModel } from "./model";
import { Section } from "./Section";
import { Slider } from "./Slider";
import './styles.scss';
import { ZoomerProvider } from './Zoomer';






type Props = {

  title: string,
  shortTitle?: string,
  icon: string | React.ReactNode
  children: any
  banner?: JSX.Element

}

// sidebar state must be handled here, as each page would has its own 'copy'.

export const ScaffoldContext = React.createContext<ScaffoldModel>(undefined!);

type SidebarState = {
  collapsed: CollapsedState
  setCollapsed: (s: CollapsedState) => void
}

export const SidebarContext = React.createContext<SidebarState>(undefined!);


// marks and control the visibility of drawers

type DrawerState = {

  isVisible: (key: string) => boolean
  open: (key: string) => any
  close: (key: string, ...other: string[]) => any
}

export const DrawerContext = React.createContext<DrawerState>(undefined!)


export const actionDrawerParam = "action-drawer"

export type CollapsedState = 'closed' | 'open' | undefined

export const Scaffold = (props: Props) => {

  const { pathname, search } = useLocation()
  const history = useHistory()
  //const { Fullscreen, setFullscreen } = useFullscreen()

  //setFullscreen();
  //;(window as any).full = setFullscreen;
  const params = paramsInQuery(search)

  const model = modelFrom(props)

  const [collapsed, setCollapsed] = React.useState<CollapsedState>(undefined)

  const drawerValue: DrawerState = {
    isVisible: key => params[actionDrawerParam] === key,
    open: key => history.push(`${pathname}?${updateQuery(search).with(q => q[actionDrawerParam] = key)}`),
    close: (...keys: string[]) => history.push(`${pathname}?${updateQuery(search).with(q => {

      q[actionDrawerParam] = null
      keys.forEach(p => q[p] = null)

    })}`)
  }

  return (

    <ScaffoldContext.Provider value={model} >
      <DrawerContext.Provider value={drawerValue}>
        <SidebarContext.Provider value={{ collapsed, setCollapsed }}>
          <ZoomerProvider>
            <FocusModeProvider>
              <FilterContext>

                {/*  
                          note: for reasons still unknknown, antd's configprovider forces a rerender of all ants commponents when used in conjuction with the browser router.  
                          we can't remove it though, as we use it to localise antd's own component for the current user, so our re-renders are less optimised than they could,
                        */}
                <Switch>

                  {model.sections.filter(s => trueIs(s.enabled) && falseIs(s.disabled)).map((s, i) =>

                    <Route exact={s.route.exact} path={s.route.path} key={i} >
                      {s.content}
                    </Route>

                  )}

                  <Route component={NoSuchRoute} />

                </Switch>

              </FilterContext>
            </FocusModeProvider>
          </ZoomerProvider>
        </SidebarContext.Provider>
      </DrawerContext.Provider>
    </ScaffoldContext.Provider>
  )

}


const modelFrom = (props: Props): ScaffoldModel => {


  const sections = childrenIn(props).type(Section).toArray().map(s => {

    const { title, enabled = true, disabled, icon, path, exact = false, children, crumbs = {}, showOnSider = true, banner } = s.props
    const content = children;
    return { title, disabled, enabled, icon, route: { path, exact }, content, crumbs, showOnSider, banner }

  })

  const links = childrenIn(props).type(ExternalLink).toArray().map(l => l.props)

  const sliders = childrenIn(props).type(Slider).toArray().map(s => ({ id: s.props.id, enabled: s.props.enabled, disabled: s.props.disabled, content: s.props.children }))

  const menuitems = childrenIn(props).type(MenuItem).toArray().map(s => s.props)

  const buttons = childrenIn(props).type(ButtonItem).toArray().map(s => s.props)

  return { ...props, sections, links, sliders, menuitems, buttons }

}