import { Icon, Tooltip } from "antd";
import { createContext, PropsWithChildren, useContext, useEffect, useLayoutEffect, useMemo, useState } from "react";
import { useT } from '#app/intl/api';
import { AiOutlineMinusCircle, AiOutlinePlusCircle } from "react-icons/ai";

const ZOOM_OUT = 80;
const ZOOM_IN = 120;
const ZOOM_DEFAULT = 100;
// const ZOOM_DURATION = 300;
const DESELECTED_OPACITY = .4;

export const Zoomer = () => {
    const t = useT();
    const { zoom, setZoom } = useZoomer();

    const refresh = (e?: any) => document.body.style.cssText = `
        --zoomer-duration: 0;
        height: ${ (ZOOM_DEFAULT / zoom) * window.innerHeight }px;
        width: ${ (ZOOM_DEFAULT / zoom) * window.innerWidth }px;
        transform: scale(${ zoom / ZOOM_DEFAULT });
    `;

    useEffect(() => {
        window.addEventListener('resize', refresh);
        return () => {
            window.removeEventListener('resize', refresh);
        }
    });

    useLayoutEffect(() => {
        refresh();
        if (zoom === ZOOM_DEFAULT) {
            document.documentElement.classList.remove('zoomed');
        } else {
            document.documentElement.classList.add('zoomed');
        }
    //eslint-disable-next-line
    }, [ zoom ]);

    return <div className="zoomer">
        <Tooltip title={ t('common.zoom.out') }>
            <Icon
              component={ AiOutlineMinusCircle }
              style={{ opacity: zoom === ZOOM_IN ? DESELECTED_OPACITY : 1 }}
              className={ zoom === ZOOM_OUT ? 'zoomer-minus active' : 'zoomer-minus' }
              onClick={ () => setZoom(zoom !== ZOOM_OUT ? ZOOM_OUT : ZOOM_DEFAULT) }
            />
        </Tooltip>
        <Tooltip title={ t('common.zoom.in') }>
            <Icon
                component={ AiOutlinePlusCircle }
                style={{ opacity: zoom === ZOOM_OUT ? DESELECTED_OPACITY : 1 }}
                className={ zoom === ZOOM_IN ? 'zoomer-plus active' : 'zoomer-plus' }
                onClick={ () => setZoom(zoom !== ZOOM_IN ? ZOOM_IN : ZOOM_DEFAULT) }
            />
        </Tooltip>
    </div>
};

export type ZoomerState = {
    zoom: number
    setZoom: React.Dispatch<React.SetStateAction<number>>
};

const ZoomerContext = createContext<ZoomerState>(undefined!);

export const useZoomer = () => useContext(ZoomerContext);

export const ZoomerProvider = (props: PropsWithChildren<{}>) => {

    const { children } = props;
    const [ zoom, setZoom ] = useState<number>(100);

    const value: ZoomerState = useMemo(() => ({
        zoom,
        setZoom
    }), [ zoom ]);

    return <ZoomerContext.Provider value={ value }>
        { children }
    </ZoomerContext.Provider>
};
