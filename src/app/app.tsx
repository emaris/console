
import { AppState, initialState } from '#state';
import { ConfigProvider } from "antd";
import { useClientSession } from 'apprise-frontend-core/client/session';
import { useRenderGuard } from 'apprise-frontend-core/utils/renderguard';
import * as React from "react";
import { DndProvider } from "react-dnd";
import Backend from 'react-dnd-html5-backend';
import { BrowserRouter as Router } from "react-router-dom";
import { ToastContainer } from "react-toastify";
import 'react-toastify/dist/ReactToastify.css';
import { DevLogin } from './call/DevLogin';
import { BusySpinner } from "./components/BusySpinner";
import { ErrorBarrier } from "./components/ErrorBarrier";
import { ConfigLoader } from "./config/Loader";
import { IntlLoader, LanguageLoader } from "./intl/Loader";
import { ModuleInitialiser } from './module/loader';
import { useSettingsStore } from './settings/store';
import { useTagClient } from './tag/client';
import { useTenantStore } from './tenant/store';
import { LoggedUserLoader } from './user/Loader';
import { useUserStore } from './user/store';
import { localeFrom } from "./utils/locale";


export const App = (props: React.PropsWithChildren<{}>) => {

  const [locale, setLocale] = React.useState<ReturnType<typeof localeFrom> | undefined>(undefined)

  const onReady = React.useCallback(lang => setLocale(localeFrom(lang)), [])

  const session = useClientSession()
  
  const sessionId = session.get()

  React.useEffect(()=> {

    if (!sessionId)
      session.create()
      
    // eslint-disable-next-line
  },[sessionId])


  return <ErrorBarrier>
    <ConfigProvider locale={locale}>
      <Router basename={location.pathname.split("/")[1]}>
        <DndProvider backend={Backend}>

          <AppState value={initialState({

            toggles:[]
            
          })}>
            <ToastContainer position='bottom-right' closeButton={false} draggable={false} />
            {/* // load translation first, as APIs will neeed them at init time.  */}

            <DevLogin >
              <IntlLoader>
                <ConfigLoader>
                  <LoggedUserLoader>
                    <CoreLoader>
                      <LanguageLoader onReady={onReady}>
                        <ModuleInitialiser>
                          <BusySpinner>
                              {props.children}
                          </BusySpinner>
                        </ModuleInitialiser>
                      </LanguageLoader>
                    </CoreLoader>
                  </LoggedUserLoader>
                </ConfigLoader>
              </IntlLoader>
            </DevLogin>

          </AppState>
        </DndProvider>
      </Router>
    </ConfigProvider>
  </ErrorBarrier>

}






export const CoreLoader = (props: React.PropsWithChildren<{}>) => {


  const tags = useTagClient()
  const users = useUserStore()
  const settings = useSettingsStore()
  const tenants = useTenantStore()


  const { content } = useRenderGuard({
    render: props.children,
    orRun: () => Promise.all([settings.fetch(), tags.fetchAllCategories(), tags.fetchAllTags(), users.fetchAll(), tenants.fetchAll()]),
  })

  return content

}
