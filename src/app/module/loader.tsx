import { iammodule } from '#app/iam/module'
import { useModuleRegistry } from '#app/module/registry'
import { useSystemModule } from '#app/system/module'
import { useTenantModule } from '#app/tenant/module'
import { useUserModule } from '#app/user/module'
import { noop } from '#app/utils/function'
import { useCampaignModule, useComplianceModule, useTimelinessModule } from '#campaign/module'
import { useProductSubmissionModule, useRequirementSubmissionModule, useSubmissionModule } from '#campaign/submission/module'
import { useEventModule } from '#event/module'
import { useLayoutModule } from '#layout/module'
import { useMessageModule } from '#messages/module'
import { useProductModule } from '#product/module'
import { useRequirementModule } from '#requirement/module'
import { useRenderGuard } from 'apprise-frontend-core/utils/renderguard'
import { PropsWithChildren, useEffect } from 'react'


// the initialiser has a large number of dependencies with a deep closure.
// so it's heavier than more components, and yet re-renders more often because it indirectly depends on most context.
// this is particularly unlucky, because it should reall do its job once.
// so we unmount it altogether after the first run .
// this also releases its closure, so that it doesn't hang around forever.

export const ModuleInitialiser = (props: PropsWithChildren<{}>) => {

  const { children } = props

  const { content } = useRenderGuard({
    render: children,
    orRun: noop,
    andRender: <OneShotInitialiser />
  })

  return content
}


export const OneShotInitialiser = () => {

  const moduleRegistry = useModuleRegistry()

  const system = useSystemModule()

  const users = useUserModule()
  const tenants = useTenantModule()
  const events = useEventModule()
  const campaigns = useCampaignModule()
  const messages = useMessageModule()
  const requirements = useRequirementModule()
  const products = useProductModule()
  const layout = useLayoutModule()
  const submissions = useSubmissionModule()
  const reqsubmissions = useRequirementSubmissionModule()
  const prodsubmissions = useProductSubmissionModule()
  const compliance = useComplianceModule()
  const timeliness = useTimelinessModule()


  useEffect(() => {

    const modules = [system, iammodule, users, tenants, messages, events, requirements, products, layout, campaigns, submissions, reqsubmissions, prodsubmissions, compliance, timeliness]

    moduleRegistry.register(...modules)

    modules.forEach(m => m.onInit?.())

    //eslint-disable-next-line
  }, [])

  return null
}
