import { State } from 'apprise-frontend-core/state/api'
import { createContext } from 'react'
import { Module } from './model'



export type ModuleRegistryState = {

    modules: Module[]
}

export const initialModuleRegistry: ModuleRegistryState = { modules: [] }

export const ModuleRegistryContext = createContext<State<ModuleRegistryState>>(undefined!)
