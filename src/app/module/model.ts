



export type Module = {

    type: string
    icon?: JSX.Element
    nameSingular: string
    namePlural: string

    onInit?: () => void

} & { [key: string]: any }


export type ModuleRegistryState = {

    modules: Module[]
}