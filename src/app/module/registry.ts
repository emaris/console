
import { useContext } from 'react'
import { ModuleRegistryContext } from './context'
import { Module } from './model'




export const useModuleRegistry = () => {

    const state = useContext(ModuleRegistryContext)

    return {

        register: (...ms: Module[]) => state.set(s => s.modules.push(...ms)),

        get: (type: string | undefined) => state.get().modules.find(m => m.type === type),

        reset: () => state.set(s => s.modules.splice(0, state.get().modules.length)),

        all: () => [...state.get().modules],

        allWith: (slot: string) => state.get().modules.filter(m => m[slot]),

        allSlotsWith: <T>(slot: string) => state.get().modules.filter(m => m[slot]).map(m => m[slot] as T)

    }

}