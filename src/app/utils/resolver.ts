import { stripHtmlTags } from "./common"

const getConditions = /[^(}]+(?=\))/g



const getInnerMostParenthesis = (text) => {

    let open = [] as number[]
    let coords = [] as any[]

    for (let i = 0; i < text.length; i++) {
        if (i > 0 && text[i - 1] === '{' && text[i] === '{') {
            open.push(i)
        }

        if (i < text.length && open.length > 0 && text[i - 1] === '}' && text[i] === '}') {
            coords.push({ open: open[open.length - 1] + 1, close: i - 1 })
            open = []
        }
    }
    return coords.map(coords => text.substring(coords.open, coords.close))
}

const truthy = (x) => {
    const conditions = (
        x.match(getConditions) !== null ? x.match(getConditions)[0].split(',') : []
    ).map(x => x.trim())

    const conditionType = x.substring(0, x.indexOf('('))

    const evaluate = (x) => (x === undefined || x === null || x === '' || x === 'N/A' || x === 'false') ? false : !!resolve(x)

    switch (conditionType.toUpperCase()) {

        case 'AND': {
            if (conditions.length !== 2) console.log('Incorrect Ternary Syntax')
            return (evaluate(conditions[0]) && evaluate(conditions[1]))
        }
        case 'OR': {
            if (conditions.length !== 2) console.log('Incorrect Ternary Syntax')
            return (evaluate(conditions[0]) || evaluate(conditions[1]))
        }
        case 'NOT': {
            if (conditions.length !== 1) console.log('Incorrect Ternary Syntax')
            return !evaluate(conditions[0])
        }
        default: {
            return evaluate(x) ? x : false
        }
    }
}

export const resolve = (what) => {

    const ternaries = getInnerMostParenthesis(what)

    if (ternaries.length === 0)
        return what

    const ternaryresolve = (x) => {

        const ternary = x.trim()

        const t = ternary.split('??')
        const condition = t.length > 0 ? t[0] : ''
        const ifthen = t.length > 1 ? t[1].split('::')[0].trim() : undefined
        const ifelse = t.length > 1 && ternary.includes('::') ? ternary.trim().split('::')[1].trim() : undefined

        if (truthy(stripHtmlTags(condition.trim()))) {
            return ifthen ? resolve(ifthen.trim()) : condition
        } else {
            return ifelse ? resolve(ifelse.trim()) : ''
        }

    }

    return resolve(what.replace(`{{${ternaries[0]}}}`, ternaryresolve(ternaries[0])))

}







