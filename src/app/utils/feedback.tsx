import { Collapse, Form, Input, Modal } from "antd";
import { ModalFuncProps } from "antd/lib/modal";
import { errorHandled } from '#app/call/constants';
import { Button } from "#app/components/Button";
import { Paragraph } from "#app/components/Typography";
import { useT } from '#app/intl/api';
import * as React from "react";
import { toast } from "react-toastify";
import { icns } from "../icons";

export type ErrorProps = ModalFuncProps & {

    message: string,
    details?: string,
    [errorHandled]?: boolean
}

export type WarningProps = ModalFuncProps & ErrorProps & {
    title: string

}

export const showAndThrow = (e: Error, props: string | Partial<ErrorProps>) => {
    showError(e, props)

    throw e

}

export const showError = (e: Error, props: string | Partial<ErrorProps>) => {

    showFailure({
        message: e.message,
        details: (e as any).details || (e.stack && e.stack.toString()),
        [errorHandled]: e[errorHandled],
        ...(typeof props === "string" ? { title: props } : props)
    } as ErrorProps)

}

export const showWarning = (props: WarningProps) => {
    const { title, message, ...rest } = props
    const modalProps = {
        icon: icns.warn,
        title: title,
        content: (<>{message}</>),
        ...rest
    }

    return Modal.warning(modalProps)
}

export const showFailure = (props: ErrorProps) => {

    // short-circuit if error has been handled (or too many dialogs).
    if (props[errorHandled])
        return

    const { message, details, ...rest } = props;

    const modalProps = {

        icon: icns.error(),

        // defaults
        title: "Oh no, it's a crash.",


        content: <Collapse bordered={false}>
            <Collapse.Panel style={{ whiteSpace: 'pre-wrap' }} header={message} key="1">
                {details && details.toString()}
            </Collapse.Panel>
        </Collapse>,

        //  specific choices
        ...rest,

    }

    return Modal.error(modalProps)
}

export type AskConsentProps = ModalFuncProps & {
    okChallenge?: string
    noValediction?: boolean
}


type NotifyOptions = {

    icon: React.ReactNode,
    duration: number | false

    route: string
    routeMessage: string

    action: ()=>void
    actionMessage: string


    onClick: () => void
}


export const notify = (message: string, options: Partial<NotifyOptions> = {}) => {
    const { icon, duration, onClick, route, routeMessage, action, actionMessage} = options
    const content = <div className="toaster-container">
        <div className="toaster-icon">{icon}</div>
        <div className="toaster-message">{message}</div>
        {route ? <div className="toaster-button"><Button type="primary" xsize="small" linkTo={route}>{routeMessage || ""}</Button></div>
        : action && <div className="toaster-button"><Button type="primary" xsize="small" onClick={action}>{actionMessage || ""}</Button></div>}
        
    </div>

    toast.info(content, {

        className: "apprise-notification",

        position: 'bottom-right',
        autoClose: duration,
        onClick


    })

}



export const useFeedback = () => {

    const t = useT()

    const self = {

        unsavedConsentDialog: (): AskConsentProps => ({

            title: t("common.consent.unsaved.title"),
            content: t("common.consent.unsaved.msg"),
            okText: t("common.consent.unsaved.confirm"),
            autoFocusButton: null,
            okButtonProps: { type: 'default' },
            cancelButtonProps: { type: 'primary' }

        })

        ,

        toast: (message: string, options: Partial<NotifyOptions> = {}) => notify(message, { ...options, routeMessage: options.routeMessage || t("common.buttons.open") })

        ,

        askConsent: (props: AskConsentProps) => {

            const { okChallenge, okButtonProps, noValediction } = props

            const confirmEnabled = { disabled: false }
            const confirmDisabled = { disabled: true }

            const onChangeStatus = ({ target: { value } }) => modal.update({ okButtonProps: value.toLowerCase() === okChallenge!.toLowerCase() ? confirmEnabled : confirmDisabled })

            const maincontent = typeof props.content === 'string' ? <Paragraph>{props.content}</Paragraph> : props.content
            const valediction = noValediction || !t ? undefined : <Paragraph spaced>{t("common.consent.valediction")}</Paragraph>

            const contents = <>
                <br />
                {maincontent}
                {okChallenge ?

                    <Form.Item style={{ margin: "20px 10px", width: 400 }} help={<div style={{fontSize:'smaller'}}>{t("common.feedback.ask_consent_type_challenge")}</div>}>
                        <Input placeholder={okChallenge.toUpperCase()} onChange={onChangeStatus} />
                    </Form.Item>
                    :
                    valediction}
            </>

            const modal = Modal.warning({
                title: "Confirm Action",
                okCancel: true,

                ...props,

                width: props.width || 600,

                content: contents,
                okButtonProps: { ...okButtonProps, ...okChallenge ? confirmDisabled : confirmEnabled }
            })

            return modal
        }



        ,

        ask: <T extends any>(...props: AskConsentProps[]) => {

            const $ask = (prev: AskConsentProps[], ...props: AskConsentProps[]) =>

            ({

                if: (test: boolean) => test ? self.ask(...props) : self.ask(...prev),

                thenAsk: (...next: AskConsentProps[]) => $ask(props, ...props, ...next),

                thenRun: (task: () => T) => {

                    const chain = props.reverse()
                        .reduce((acc, step) => () => self.askConsent({
                            ...step,
                            onOk: close => Promise.resolve(acc()).finally(close)
                        })
                            , task as Function)

                    chain()
                }

            })

            return $ask([], ...props)
        }


    }

    return self
}