import { parse, ParsedQuery, stringify } from "query-string";
import { RouteComponentProps } from "react-router";

export const paramsIn = (props:RouteComponentProps) => {

    const {location: {search}} = props
    return paramsInQuery(search)

}

export const paramsInQuery = (query:string) => {

    let params = parse(query) 
    for (var p in params)
        if (params[p] && params[p]!.length === 1)
            params[p] = params[p]![0]
    
    return params

}

export const updateQuery = (query:string) => ({ with: ( change : (params:ParsedQuery) => void ) => {

        const params = paramsInQuery(query)
        change(params)
        return stringify(Object.keys(params).reduce((acc, key) => params[key] === null || params[key] === undefined ? acc : {...acc, [key]: params[key]}, {}))

}

})


export const parentIn = (pathname:string) => pathname.substring(0,pathname.lastIndexOf("/"))

export const parentPathIn = (props:RouteComponentProps) => {

    const {location:{pathname}} = props
    return parentIn(pathname)

}