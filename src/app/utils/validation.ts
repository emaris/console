
import { cardinalities, TagCategory } from "#app/tag/model";
import { TFunction } from "i18next";
import _ from "lodash";
import { fullnameOf, Language } from "../intl/model";
import { MultilangDto } from "../model/multilang";
import { deepequals } from "./common";

export type ValidationStatus = "success" | "error" | "warning"

const reservedKeywords = ['new']

export type Validation = {

    status?: ValidationStatus
    msg?: any
    help?: any
}


export type ValidationReport = {

    [key: string]: Validation
}

export const defaultMaxStringLength = 40

export const error = (msg: any) => ({ status: "error", msg }) as Validation
export const warning = (msg: any) => ({ status: "warning", msg }) as Validation
export const success = (msg?: any) => ({ status: "success", msg }) as Validation

export const invalidChars = " _:!?/\\*;,@"

export type ValidationCheck<T> = {
    predicate: (t: T) => boolean | [boolean, any]
    msg: (...any) => any
    status?: ValidationStatus
}

export const empty = (t: TFunction): ValidationCheck<string | undefined | any[]> => ({

    predicate: (s: string | undefined | any[]) => typeof s === 'string' ? !s || s.trim().length === 0 : !s || s.length === 0,
    msg: () => t("common.validation.empty_error")

})

export const stringLength = (t: TFunction): ValidationCheck<string | MultilangDto> => {

    let lang = undefined! as string

    return {
        predicate: (s: string | MultilangDto, length?: number) => {
            const l = length ?? defaultMaxStringLength
            if (typeof s === 'string') return s.length <= l ? false : true
            return Object.keys(s).map(o => {
                if (s[o] !== undefined && s[o].length > l) {
                    lang = o
                    return false
                }
                // s[o] === undefined ? true : s[o].length <= l ? true : false
                return true
            }).some(s => s === false)
        },
        msg: () => t("common.validation.too_long", { lang: t(`common.languages.${lang}`) })
    }
}

export const invalidEmail = (t: TFunction): ValidationCheck<string | undefined> => ({

    // eslint-disable-next-line
    predicate: (s: string | undefined) => !!s && !/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(s),
    msg: () => t("common.validation.error_email")

})

export const provided = <T>(cond: boolean, check: ValidationCheck<T>) => cond ? check : { predicate: (_: any) => false, msg: () => "" } as ValidationCheck<T>


export const notdefined = (t: TFunction): ValidationCheck<any> => ({

    predicate: (s: any) => !s,
    msg: () => t("common.validation.empty_error")

})

export const selectedFieldType = (t: TFunction): ValidationCheck<any> => ({

    predicate: (s: TagCategory) => s.properties.field?.enabled ? s.properties.field.type === 'radio' ? !_.isMatch(s.cardinality, cardinalities.one) : false : false,
    msg: () => t("common.validation.wrong_cardinality")

})

export const email = (t: TFunction): ValidationCheck<string> => ({
    predicate: (s: string) => !s.match(/\S+@\S+\.\S+/),
    msg: () => t("common.validation.invalid_email")
})

export const reservedKeyword = (t: TFunction): ValidationCheck<string | MultilangDto> => {

    let reservedKeywordFound = undefined! as string

    return {
        predicate: (s: string | MultilangDto) => {

            if (!s)
                return false

            return typeof s === 'string' ?

                reservedKeywords.includes(s.toLowerCase())

                :

                Object.keys(s).some(k => {
                    const res = reservedKeywords.includes(s[k]?.toLowerCase())
                    if (res)
                        reservedKeywordFound = reservedKeywords.find(r => r === s[k]?.toLowerCase())!
                    return res
                })
        },
        msg: () => t("common.validation.reserved_keyword", { keyword: reservedKeywordFound })
    }
}

export const duplicateWith = (entries: string[], t: TFunction): ValidationCheck<string | undefined> => ({
    predicate: (v: string | undefined) => !v ? false : entries.some(e => v.toLowerCase() === e.toLowerCase()),
    msg: () => t("common.validation.duplicate_error") 
})


export const requireLanguages = (required: Language[], t: TFunction): ValidationCheck<MultilangDto> => ({

    predicate: (v: MultilangDto) => required.some(l => !v?.[l]),
    msg: () => required.length === 1 ?
        t("common.validation.required_language", { language: t(fullnameOf(required[0])) }) :
        t("common.validation.required_languages", { languages: required.map(l => t(fullnameOf(l))).join(",") })

})

export const oneRequiredLanguage = (required: Language[], t: TFunction): ValidationCheck<MultilangDto> => ({

    predicate: (v: MultilangDto) => !required.some(l => v?.[l]),
    msg: () => t("common.validation.one_required_language", { languages: required.map(l => t(fullnameOf(l))).join(",") })

})

export const uniqueLanguages = (all: MultilangDto[], t: TFunction): ValidationCheck<MultilangDto> => ({

    predicate: (v: MultilangDto) => {

        const lang = all.map(other => Object.keys(other).find(lang => v[lang] === other[lang])).find(lang => !!lang)
        return [!!lang, lang]
    },

    msg: (lang: Language) => t("common.validation.unique", { language: t(fullnameOf(lang)) })

})

export const uniqueWith = <T>(all: T[], uniqueCheck: (t: T) => boolean, tr: TFunction): ValidationCheck<any> => ({

    predicate: (s: T) => all.some(uniqueCheck),
    msg: () => tr("common.validation.duplicate_error")

})


export const containsAny = (chars: string, t: TFunction): ValidationCheck<string> => {

    let invalidChar = undefined! as string

    return {

        predicate: (s: string) => {

            if (!empty(t).predicate(s)) {
                const invalidCharsFound = s.split('').find(c => chars.includes(c))
                if (invalidCharsFound && invalidCharsFound.length > 0) {
                    invalidChar = invalidCharsFound[0]
                    return true
                }
            }
            return false
        },
        msg: () => t("common.validation.invalid_chars", { invalidChar })

    }

}

export const noDupsInArray = (t: TFunction): ValidationCheck<any[]> => {

    let invalidChar = undefined! as string

    const compare = (first, second) => {
        if ((typeof first === 'string' && typeof second === 'string') 
            || (typeof first === 'number' && typeof second === 'number')
            || (typeof first === 'boolean' && typeof second === 'boolean')) return first === second

        if (Array.isArray(second) && Array.isArray(second)) return deepequals(first, second)
        if (typeof first === 'object' && typeof second === 'object') return deepequals(first, second)

        return false

    }

    return {

        predicate: (arr: any[]) => arr.map((entry, i) => arr.filter((_, j) => i !== j).some(next => compare(entry, next))).some(res => res)
        ,
        msg: () => t("common.validation.error", { invalidChar })

    }

}



export const invalidCharacters = (t: TFunction): ValidationCheck<any> => containsAny(invalidChars, t)

export const multiContainsAny = (chars: string, t: TFunction): ValidationCheck<MultilangDto> => {

    let invalidChar = undefined! as string

    return {
        predicate: (s: MultilangDto) => {
            const res = Object.keys(s).reduce((acc, lang) => {
                if (acc) return true
                if (!empty(t).predicate(s[lang])) {
                    const invalidCharsFound = s[lang].split('').find(c => chars.includes(c))
                    if (!empty(t).predicate(s[lang]) && invalidCharsFound && invalidCharsFound.length > 0) {
                        invalidChar = invalidCharsFound[0]
                        return true
                    }
                }
                return false
            }, false)
            return res
        },
        msg: () => t("common.validation.invalid_chars", {invalidChar: invalidChar === ' ' ? t("common.validation.space") : invalidChar})
    }
}

export const multiInvalidCharacters = (t: TFunction): ValidationCheck<MultilangDto> => multiContainsAny(invalidChars, t)


export const checkIt = () => _check(null, [])

export const check = <T>(t: T) => _check(t, [])

const _check = <T>(t: T, checks: ValidationCheck<T>[]) => ({

    // accumulates this predicate, optionally overriding predefined message
    with: (check: ValidationCheck<T>, optional?: { msg?: any, status?: ValidationStatus }) => {
        //augmenting the check with optional parameters
        const opt = {
            msg: optional && optional.msg ? () => optional.msg : check.msg,
            status: optional && optional.status ? optional.status : check.status
        }
        return _check(t, [...checks, { ...check, ...opt }])
    },

    // run checks without any info (just a rename of nowOr)
    now: (successmsg?: string) => _check(t, checks).nowOr(successmsg),

    // ruch ckecs, optionally showing a status message and|or a lengthier help message if there are no failures. 
    nowOr: (successmsg?: string, help: any = ' ') => {

        const failure = checks.map(p => {
            const outcome = p.predicate(t)

            return outcome instanceof Array ? [outcome[0], p.msg(outcome[1]), p.status] : [outcome, p.msg(), p.status]

        })
            .find(outcome => outcome[0])

        const warningStatus = failure?.[2] === 'warning'
        return { ...(failure ? warningStatus ? warning(failure[1]) : error(failure[1]) : success(successmsg)), help }
    }
})



export type ReportMethods = {

    errors: () => number

}

export const withReport = <T extends ValidationReport>(self: T): T & ReportMethods => ({

    ...self,

    errors: () => Object.values(self).filter(c => c.status === "error").length

})