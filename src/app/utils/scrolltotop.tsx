import { Button } from '#app/components/Button'
import { icns } from '#app/icons'
import { useT } from '#app/intl/api'
import { useEffect, useRef, useState } from 'react'
import "./scrolltotop.scss"

export const ScrollTop = (props: {

    containerId: string
    visibilityThresholdMultiplier?: number

}) => {

    const t = useT()

    const { containerId, visibilityThresholdMultiplier = 1.5 } = props

    const elref = useRef(document.getElementById(containerId))

    const [canScroll, setCanScroll] = useState(false)

    useEffect(() => {

        elref.current = document.getElementById(containerId)

        if (!elref.current) {
            console.error("no such element ", containerId)
            return
        }

        const listener = () => {

            if (!elref.current)
                return 

            const initiallyVisible = window.innerHeight - elref.current?.getBoundingClientRect().top

            setCanScroll(elref.current.scrollTop > (visibilityThresholdMultiplier*initiallyVisible))
        
        }


        elref.current.addEventListener('scroll', listener)

        return () => elref.current?.removeEventListener('scroll', listener)


    // eslint-disable-next-line
    }, [])

    
    if (!elref.current)
        return null

    const scrollup = () => {
        window.location.hash = ''
        elref.current?.scrollTo(0,0)
    }

    return <Button enabledOnReadOnly className={`scroll-to-top btn-${canScroll ? 'visible' : 'hidden'}`} type='link' size='small'
                
            iconLeft icn={icns.up} onClick={scrollup}>{t('scroll_to_top')}</Button>

}