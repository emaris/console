import { useStable } from 'apprise-frontend-core/utils/function'
import { useState } from 'react'
import { IconBaseProps } from 'react-icons'
import { GoDotFill } from 'react-icons/go'
import "./asyncrunner.scss"

// reports loading and error states for the execution of an async task, 
// so that they can be controlled at fine grain (vs. a contextual <BusyGuard>).
// the typical use case is to have a button with some feedback during the wait.
// returns a <DotIcon> with a pulsating effect to standardise visuls for this use case.
// often composed with useAsyncTask() configured with noBusyWait().

export const useAsyncRunner = () => {

    const [running, setRunning] = useState(false)
    const [error, setError] = useState<any>(false)

    
    const PulsingDotIcon = useStable((props: IconBaseProps) => <GoDotFill {...props} color={error ? 'orangered' : undefined} className={running ? 'icon-pulse' : ''} />)

    const waitFor = <S extends any, P extends any[]>(fun: (...args: P) => S) => async (...ps: P) => {


        try {

            setError(undefined!)

            setRunning(true)

            return await fun(...ps)


        }
        catch(e:any) {

            setError(e)

            throw e
        }
        finally {

            setRunning(false)

        }
    }

    return { running, error, waitFor, PulsingDotIcon }


}