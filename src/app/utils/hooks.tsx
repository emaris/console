
import { Empty } from "antd";
import { buildinfo } from "#app/buildinfo";
import moment from "moment-timezone";
import * as React from "react";
import { MutableRefObject, useRef } from 'react';
import { Placeholder } from "../components/Placeholder";


type AsyncRenderProps = {

  id?: string,
      
  // when we're ready to render.
  when:boolean

  // what do we do when we're not ready to render.
  task: () => Promise<any>

  // when all is ready to run the task (e.g. after an animation, or render might get in the way )
  runWhen?:boolean

  // what we actually render when we're ready.
  content?: React.ReactNode | ((_:AsyncRenderState)=>React.ReactNode),

  // what we render whilst we run the task and aren't ready to render.
  placeholder?: React.ReactNode

  // what we render when we're are neither ready nor trying to be by running the task.
  errorPlaceholder?: "nodata" | React.ReactNode

  // activates logging to figure out why things aren't happening as we expect.
  debug?:boolean

}


export type AsyncRenderState = {running:boolean,override:boolean}
type InternalRenderState = {phase:"init" | "running" | "completed",override:boolean}

// Runs a _task_ as a precondition to rendering some _content_, returning a _placeholder_ until the task completes.
// The task may run conditionally - _when_ or _unless_ some condition is true - or else unconditionally on demand.
// Returns a) the content to render based on the current state, 
// b) an indication of whether the task is running, and 
// c) a function that forces the task to run unconditionally. 

export const useAsyncRender = (props:AsyncRenderProps) => {

  const [state,setState] = React.useState<InternalRenderState>({phase:"init",override:false})

  const {id,when:canrender,runWhen:canrun=true,task,content=<React.Fragment />,placeholder=Placeholder.none,errorPlaceholder,debug} = props

  const {phase,override} = state

  //eslint-disable-next-line
  const taskmemo = React.useCallback(task,[canrender,override])

  const cancelled = React.useRef(false);

  // cleans up when client component unmounts 
  React.useEffect(()=>() => {cancelled.current=true}, [] )  // no deps => runs only once => cleans up only once on unmount

  React.useEffect(()=>{ 

    (canrender && !override) || (canrun && Promise.resolve(setState(s=>({...s,phase:"running"})))
                                .then(taskmemo)
                                .finally(()=> cancelled.current || Promise.resolve(setState({phase:"completed",override:false})))) 
                
  },[canrender,taskmemo,override, canrun])

  const runstate = phase!=="completed"

  //  render if we have something to render, but only once: don't render if we're still transitioning to 'complete' (cover also when we can immediately render at 'init' time.)
  const rendered =  canrender && phase!=='running' && (typeof content === 'function' ? content({running:runstate,override}) : content)

  // it's an error if we've run the task but we still have nothing to render.
  const fallback = ()=>(runstate || !canrun) ? placeholder : renderErrorPlaceholder(errorPlaceholder)

  const render = rendered || fallback()

  debug && console.log("id",id,"ready",canrender,"state",state,"render",render?.type?.name)

  return [ render, ()=>setState({phase:"init",override:true}), runstate,override] as [JSX.Element,()=>void,boolean,boolean]
                                                                  
}


const renderErrorPlaceholder = (placeholder) => {

  if (placeholder=== undefined)
    return <div style={{height:"100%",display:"flex",alignItems:"center",justifyContent:"center"}}>
      <img  alt="in-development" src={`${buildinfo.prefix}/images/500.jpg`} />
      </div>
  
  if (placeholder==="nodata")
    return <Empty image={Empty.PRESENTED_IMAGE_SIMPLE} />

  return placeholder
  
}


  // delays a true condition for at least a given number of milliseconds.
  export const useDelayedTruth = (condition:boolean, delay : number, threshold:number=0) => {

    const [delaying,startDelaying] = React.useState(false)

    const time = React.useRef(0)
    const ms = moment().valueOf()

    let delayFor = 0

    //console.log("delaying?",delaying,"condition",condition)

    if (!delaying) {

      // now showing, note the time
      if (condition && time.current === 0) {

          //console.log("first true at ",ms)
          time.current = ms
      }  
        
      
      // was showing, now shouldn't any more.
      else if (time.current>0 && !condition) {

          const elapsed = ms-time.current

          //console.log("elapsed",elapsed,"threshold",threshold)
         
          // too soon to turn off? show for a bit longer.
          if ( elapsed > threshold && (elapsed-threshold) < delay)
              delayFor = delay-(elapsed-threshold)
          
          else time.current= 0
          
      }

    }

    React.useEffect(()=>{

        if (!delaying && delayFor >0) {

            //console.log("delaying for ",delayFor, moment().valueOf())

            const timeout = setTimeout(()=>{ 
              
                //console.log("stop delaying",moment().valueOf())
                time.current=0
                startDelaying(false)
                clearTimeout(timeout) 
            }
            , delayFor)

            startDelaying(true);

        }

    },[delaying, delayFor,condition])

    return condition || delayFor>0 ||delaying

  }



  export const useLogOnMount = (name:string='component') => {

      React.useEffect(()=>console.log(name,'mounted'),[name])

  }




type InputRef<T> = ((instance: T | null) => void) | MutableRefObject<T | null> | null;

/**
 * Works like normal useRef, but accepts second argument which is array
 * of additional refs of the same type. Ref value will be shared with
 * all of those provided refs as well
 */
export function useSharedRef<T>(initialValue: T, refsToShare: Array<InputRef<T>>) {
  // actual ref that will hold the value
  const innerRef = useRef<T>(initialValue);

  // ref function that will update innerRef value as well as will publish value to all provided refs
  function sharingRef(value: T) {
    // update inner value
    innerRef.current = value;
    // for each provided ref - update it as well
    refsToShare.forEach((resolvableRef) => {
      // react supports various types of refs
      if (typeof resolvableRef === 'function') {
        // if it's functional ref - call it with new value
        resolvableRef(value);
      } else {
        // it should be ref with .current prop
        // make sure it exists - if so - assign new value
        if (resolvableRef) {
          (resolvableRef as any).current = value;
        }
      }
    });
  }

  /**
   * We want ref we return to work using .current, but it has to be function in order
   * to share value with other refs.
   *
   * Let's add custom get property called 'current' that will return
   * fresh value of innerRef.current
   */
  if (!(sharingRef as any).current) {
    Object.defineProperty(sharingRef, 'current', {
      get() {
        return innerRef.current;
      },
    });
  }

  return sharingRef as typeof sharingRef & { current: T };
}







const initiGlobalState = <T extends Object> (key:string, init:T | (()=>T)) => {

  const initial = init instanceof Function ? init() :init

  try {

    const item = window.localStorage.getItem(key)

    return item !== null ? JSON.parse(item) : initial

  }
  catch (error) {

    console.log(`can't resume state for ${key}`,error)

    return initial

  }

}

// similar to useState(),  but based on local storage.
export const useGlobalState = <T extends Object>(key:string, initial:T | (()=>T)) => {

  const [state, set] = React.useState(initiGlobalState(key,initial))


  const setter = (v: T) => {

    const value = v instanceof Function ? v(state) : v

    set(value)

    window.localStorage.setItem(key, JSON.stringify(value))

  }

  return [state, setter]

}


// similar to useRef(),  but based on local storage.
export const useGlobalRef = <T extends Object>(key:string, initial:T) => {

  const state = React.useRef(initiGlobalState(key,initial))

  const setter = (v: T) => {

    const value = v instanceof Function ? v(state) : v

    state.current=value

    window.localStorage.setItem(key, JSON.stringify(value))

  }

  return [state.current, setter]

}