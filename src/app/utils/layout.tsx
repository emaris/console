import { Layout } from "antd"
import * as React from "react"
import "./styles.scss"


// ----------------------------------------------------------------------- preserving mock layout


export  function MockTest(props) {


    return  <Layout className="apprise-layout" >
 
        <Layout.Sider className="sidebar" width={320} collapsible > 
 
        <div className="sidebar-logo">
         {Array(25).fill(undefined).map((_,i)=><div key={i}>ipse lorum dipsit. ipse lorum dipsit. ipse lorum dipsit. ipse lorum dipsit</div>)}
         </div>
 
         <div className="sidebar-content">
 
              <div className="sidebar-iconbar">
                      <div className="iconbar-sections">
                          <div className="section-icon">A</div>
                          <div className="section-icon">B</div>
                          <div className="section-icon">C</div>
                          <div className="section-icon">D</div>
 
                      </div>
                      <div className="iconbar-links">
                          <div className="link-icon">1</div>
                          <div className="link-icon">2</div>
                          <div className="link-icon">3</div>
                      </div>
              </div>
 
              <div className="sidebar-panel">
                <div>123</div>
                <div>456</div>
                <div>
                 {Array(100).fill(undefined).map((_,i)=><div key={i}>ipse lorum dipsit. ipse lorum dipsit. ipse lorum dipsit. ipse lorum dipsit</div>)}
           
                </div>
             </div>
 
         </div>
         
        </Layout.Sider>
 
        <Layout className="main">
 
             <Layout.Header className="main-header">
 
                 
             </Layout.Header>
 
             <Layout.Content>

                 <div style={{height:30,width:"100%",background:"purple",position:"sticky",top:0}} />
 
                 <div style={{display:"flex",alignItems:"flex-start"}}>
 
                 <div style={{margin:"100px 30px 30px 30px",minWidth:200,height:200,background:"black",position:"sticky",top:50}}>Sticky</div>
                 <div style={{}}>
                     {Array(100).fill(undefined).map((_,i)=><div key={i}>{Array(100).fill(undefined).map((_,i)=><span key={i}>Ipse lorum dipsit. </span>)}</div>)}
                 </div>
                 </div>
 
 
             </Layout.Content>
      
             
 
         </Layout>
 
     </Layout>
 
 }