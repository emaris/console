

import enGB from "antd/es/locale/en_GB"
import frFR from "antd/es/locale/fr_FR"
import 'moment/dist/locale/fr'
import 'moment/dist/locale/en-gb'
import { Language } from "../intl/model"


export const localeFrom = (lang:Language|undefined)  => {

  console.log('switching to ', lang)

  if (!lang)
   return enGB

  switch(lang) {

    case "en": return enGB
    case "fr": return frFR

    default: throw new Error(`Unsupported language '${lang}'`);
  }

}