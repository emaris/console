
import { Button } from '#app/components/Button'
import { OptionMenu } from '#app/components/OptionMenu'
import { icns } from '#app/icons'
import { Lifecycled } from '#app/model/lifecycle'
import * as React from 'react'
import { useT } from '#app/intl/api'




export const ClearFilters = (props: {
    group: string
    enableOnReadOnly?: boolean

}) => {

    const {group, enableOnReadOnly=false} = props

    const t = useT()
    // won't use prefix, but do need one alias to reset all related ones.
    const ctx = useFilterState(group)

    return <Button size='large' enabledOnReadOnly={enableOnReadOnly} style={{ visibility: ctx.isEmpty() ? 'hidden' : 'visible' }} type='ghost' noborder onClick={ctx.reset} tooltip={t("common.filters.clear_all")}>
        {icns.removeEntry}
    </Button>

}

type BaseProps<T> = {

    data: T[]
    readonlyUnless: (_: T) => boolean
    group: string
    key?: string
}

export const useBaseFilter = <T extends Lifecycled>(props: BaseProps<T>) => {

    const t = useT()

    const { data = [], readonlyUnless, key='base', group } = props

    const { get, set } = useFilterState(group)

    const selected = get(key) ?? [0, 1]


    const BaseFilter = <OptionMenu selected={selected} setSelected={set(key)}
        noSelectAll placeholderIcon={icns.filter} placeholder={t("common.components.menu.filter_placeholder")} >
        <OptionMenu.Option key={0} value={0} label={t("common.labels.inactive")} />
        <OptionMenu.Option key={1} value={1} label={t("common.labels.readonly")} />
    </OptionMenu>

    const isActive = (e: T) => e.lifecycle.state === 'active' as any

    const baseFilter = React.useCallback(

        (r: T) => (selected.includes(0) || isActive(r)) && (selected.includes(1) || readonlyUnless(r))

        // eslint-disable-next-line     
        , [selected])                   // recompute on filter change


    const baseFilteredData = React.useMemo(() => data.filter(baseFilter), [data, baseFilter])

    return { BaseFilter, baseFilter, baseFilteredData }
}


type FilterContextState = Record<string, Record<string, any>>

type FilterContextType = [FilterContextState, React.Dispatch<React.SetStateAction<FilterContextState>>]

const ReactFilterContext = React.createContext<FilterContextType>(undefined!)

export const FilterContext = (props: React.PropsWithChildren<{}>) => {

    const [filters, setFilters] = React.useState<FilterContextState>({})

    const value: FilterContextType = React.useMemo(() => [filters, setFilters], [filters])

    return <ReactFilterContext.Provider value={value}>
        {props.children}
    </ReactFilterContext.Provider>
}


export const useFilterState = (group: string) => {

    const [filters, setFilters] = React.useContext(ReactFilterContext)

    //console.log(group,filters)

    const self = {

        set: (key: string) => (value: any) => {

            const addKey = (fs:FilterContextState) => ({ ...fs[group], [key]: value })
            const removeKey = (fs:FilterContextState) => Object.keys(fs[group]).reduce((acc, k) => key === k ? acc : { ...acc, [k]: fs[group][k] }, {})

            setFilters(fs => ({ ...fs, [group] : value ? addKey(fs) : removeKey(fs) }))

        }
        ,

        get: (key:string) => filters[group]?.[key]

        ,

        isEmpty: () => {
            const isObject = (obj) => typeof obj === 'object' && !Array.isArray(obj) && obj !== null

            const isEmptyIneerRecursion = (obj) => {
                if (Object.keys(obj ?? {}).length===0) return true    
                else {
                    return !Object.keys(obj).map(k => isObject(obj[k]) ? isEmptyIneerRecursion(obj[k]) : false).some(k => k === false)
                }
            }
            return isEmptyIneerRecursion(filters[group])
        }

        ,

        reset: () => setFilters(fs => Object.keys(fs).reduce((acc, grp) => group === grp ? acc : { ...acc, [grp]: fs[grp] }, {}))
    }

    


    return self
}
