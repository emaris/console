
import { MultilangDto } from "#app/model/multilang"
import deepequal from "fast-deep-equal"
import { TFunction } from "i18next"
import { cloneDeep } from "lodash"
import moment, { Moment } from "moment-timezone"
import sid from "shortid"
import _ from 'lodash'


export type ValOrGen<T> = T | (() => T)

export const clone = (s: string) => `${s}__cloned`

export const shortid = () => sid.generate()

export const trueIs = (val: any) => val === undefined || val
export const falseIs = (val: any) => val === undefined || !val

export const asGenerator = (v: any): Function => (typeof v === "function") ? v : () => v

export const wait = <T>(ms: any) => (x: T) => new Promise<T>(v => setTimeout(() => v(x), ms));

export const randomNumber = (range: number = 100) =>
  Math.floor(Math.random() * Math.floor(range))

export const randomNumberBetween = (min, max) => {
  return Math.floor(Math.random() * (max - min + 1)) + min;
}

export const randomString = (range: number = 10) => [...Array(range)].map(() => Math.random().toString(36)[2]).join('')

export const compareNumbers = (a, b) => {
  if (isNaN(a)) return 1
  if (isNaN(b)) return -1
  return b - a;
}

export const compareDates = (d1: string | number | undefined, d2: string | number | undefined, nullFirst: boolean = true) => {

  // moment far too slow in parsing, matters in a loop (eg. table sorting).
  const outcome = !d1 ? (d2 ? nullFirst ? -1 : 1 : 0) : !d2 ? (nullFirst ? 1 : -1) : new Date(d1).getTime() - new Date(d2).getTime()

  //console.log("compared",d1,d2,"outcome",outcome)

  return outcome;

}

// export const compareTags = (t1: Tag | undefined, t2: Tag | undefined): number => {
//   if (t1 === undefined && t2 === undefined) return 0
//   if (t1 === undefined) return 1
//   if (t2 === undefined) return -1
//   if (t1.properties.value === undefined && t2.properties.value === undefined) return 0
//   if (t1.properties.value === undefined) return 1
//   if (t2.properties.value === undefined) return -1
//   if (isNaN(t1.properties.value) && isNaN(t2.properties.value)) return 0
//   if (isNaN(t1.properties.value)) return 1
//   if (isNaN(t2.properties.value)) return -1
//   if (t1.properties.value > t2.properties.value) return -1
//   if (t1.properties.value < t2.properties.value) return 1
//   return 0
// }

export const dedup = <T>(ts: T[]) => ts.filter((_, i) => ts.indexOf(_) === i)

export const randomBoolean = (likelihood: number = .5) => Math.random() <= likelihood

export const randomIn = <T>(vals: T[], orUndefined?: boolean): T => orUndefined ? randomBoolean() ? vals[randomNumber(vals.length)] : undefined! : vals[randomNumber(vals.length)]

export const randomSlice = <T>(vals: T[], min: number, max: number) => {

  let slice = [] as T[]

  if (vals === undefined || vals.length === 0)
    return vals

  Array.from({ length: randomNumberBetween(Math.min(vals.length, min), Math.min(vals.length, max)) }).map(_ => {
    var random: T
    do random = randomIn(vals)!; while (slice.indexOf(random) > -1)
    slice = [random, ...slice]
    return null
  })

  return slice

}

export const random = (thing: string, range: number = 100) =>
  thing + randomNumber(range + 1)

export function randomDateBetween(start: Date, end: Date) {
  return new Date(start.getTime() + Math.random() * (end.getTime() - start.getTime()));
}

export function randomMomentBetween(start: Moment, end: Moment) {
  return moment(start.valueOf() + randomNumberBetween(0, end.diff(start)))
}

export const randomColor = () => {
  const letters = "0123456789ABCDEF"
  const res = Array(6).fill(undefined).map(() => letters[Math.floor(Math.random() * 16)]).reduce((acc, n) => acc.concat(n), "")
  return "#".concat(res)
}

export function randomDateSince(start: Date) {
  return randomDateBetween(start, new Date())
}

export const failWith = (msg: any) => {
  throw Error(msg);
}

export const clock = <T>(name: string, f: () => T, threshold?:number) => {

  const t0 = performance.now()
  const result = f()

  const time = performance.now() - t0

  if (threshold === undefined || threshold < time)
    console.log(`${name} took ${time}`)

  return result;

}

export const through = <T>(fn: (t: T) => any) => (a: T): Promise<T> => {
  //fn(a);    // process side-effects
  return Promise.resolve(fn(a)).then(_ => a); // pass the data further
};

export const modelLike = <T extends Object>(t: T): T => (Object.keys(t).reduce((o, k) => {
  o[k] =
    (t[k] instanceof Array) ? []
      :
      (t[k] instanceof Object) ? modelLike(t[k])
        : undefined

  return o
}
  , {}) as any)


export const isEmpty = (v: any, recurse?) => !isNotEmpty(v, recurse)
export const isNotEmpty = (v: any, recurse: boolean = false) =>

  v !== undefined && v !== null && (
    typeof v === 'string' ? true
      : typeof v === 'object' ?
        // preserves nested empty objects: a top-level object is empty if all its props are, but empty objects nested below it don't count as empty 
        Object.keys(v).length === 0 ? recurse : Object.values(v).some(v => isNotEmpty(v, true))

        : true

  )

const canonicalEmptyOf = (v: any) => Array.isArray(v) ? [] : typeof v === 'object' && v ? {} : undefined

// normalise: <T>(current: T, previous: T, initial: T) => {

//   // values that haven't changed  don't need to be normalised against their initial counterpart.
//   if (current === previous)
//     return current

//   if (self.isEmpty(current))
//     if (self.isEmpty(initial))    // both empty, return old.
//       return initial
//     else
//       return self.canonicalEmptyOf(current)  // only new is empty, return a canonical form.
//   else
//     if (self.isEmpty(initial))
//       return current                      // only old is empty, return new.

//   // both non-empty, recurse if matching.

//   if (Array.isArray(current) && Array.isArray(initial))
//     return current.map((ae, i) => self.normalise(ae, previous?.[i], initial?.[i]))

//   else if (typeof current === 'object' && typeof initial === 'object')

//     return Object.keys(current ?? {}).reduce((acc, k) =>

//       k in (initial ?? {}) ?   // never 'cancel' a key.

//         { ...acc, [k]: self.normalise(current[k], previous?.[k], initial?.[k]) }

//         :

//         self.isEmpty(current[k]) ? acc : { ...acc, [k]: current[k] }   // prunes new empty data

//       , {})


//   return current        // return new.

// }

export const cleanWith = (val, oldval) => {

  //  console.log("comparing",val,oldval)

  if (Array.isArray(val))
    // skip 'same' element in the assumption they haven't changed and shouldn't be changed now.
    val = val.map((ae, i) =>

      (oldval[i] && ae !== oldval[i]) ? cleanWith(ae, oldval?.[i]) : ae)


  else if (typeof val === 'object' && val && typeof oldval === 'object')  // val can be undefined

    val = Object.keys(val).reduce((acc, k) =>

      // skip 'same' element in the assumption they haven't changed and shouldn't be changed now.
      k in (oldval ?? {}) && oldval?.[k] && val[k] !== oldval?.[k] ?

        { ...acc, [k]: cleanWith(val[k], oldval?.[k]) }

        :

        // prunes new empty data 
        { ...acc, [k]: val[k] }

      , {})


  // return cleaned value if it's not empty, otherwise: if old is empty, reproduce it. if old isn't empty, use the canonical empty version of it.
  const cleaned = isNotEmpty(val) ? val : isNotEmpty(oldval) ? canonicalEmptyOf(val) : deepclone(oldval)

  // if (val.spec==='section')
  // console.log("old",oldval,"new",val,"canonical empty",canonicalEmptyOf(val),"cleaned",cleaned)

  return cleaned

}



export const deepclone = <T>(t: T) => {
  return cloneDeep(t) as T
}

// export function recwalk(o: any, path: string[]) {

//   if (!o)
//     return o

//   const [first, ...rest] = path
//   return first ? recwalk(o[first], rest) : o
// }

export function recwalk(o: any, path: string[]) {

  if (!o)
    return o

  const [first, ...rest] = path

  if (first) {


    if (first in o)
      return recwalk(o[first], rest)

    if (rest?.length)
      throw new Error(`cant walk ${first}`)

    else return undefined

  }
  else return o

}

export const capitalise = (s: string) => s.charAt(0).toUpperCase() + s.slice(1)


export const cartesianProduct = (as, bs) => [].concat(...as.map(a => bs.map(b => [].concat(a, b))));

export const index = <T>(values: T[]) => ({

  by: (key: (_: T) => string | undefined) => {
    //values.reduce((acc, next) => key(next) ? ({ ...acc, [key(next)!]: [next, ...(acc[key(next)!] || [])] }) : acc, {}) as { [_: string]: T[] }
    const res = {} as Record<string, T[]>

    values.reduce((acc, next) => {
      const nextKey = key(next)
      if (nextKey) {
        if (!acc[nextKey]) acc[nextKey] = []
        acc[nextKey].push(next)
      }
      
      return acc 
    }, res)

    return res

  }

})

export const indexMap = <T>(values: T[]=[]) => ({

  by: (key: (_: T) => string | undefined) => {//values.reduce((acc, next) => key(next) ? { ...acc, [key(next)!]: next } : acc, {}) as { [_: string]: T }
    const res = {} as Record<string, T>
    
    values.reduce((acc, next) => {
      const nextKey = key(next)
      if (nextKey) 
        acc[nextKey] = next
      
      return acc 
    }, res)

    return res
  }

})

export const group = <T>(values: T[]) => ({

  by: <S>(key: (_: T) => S, compare: (t1: S, t2: S) => number) => values.reduce((acc, next) => {

    const nextkey = key(next)

    if (!nextkey)
      return acc

    const match = acc.find(e => compare(e.key, nextkey) === 0)

    if (match) {
      match.group.push(next)
      return acc
    }

    return [{ key: nextkey, group: [next] }, ...acc]

  }, [] as { key: S, group: T[] }[]).sort((g1, g2) => compare(g1.key, g2.key))



})


export const deepequals = <T>(t1: T, t2: T) => {
  return deepequal(t1, t2)
}

export const cloneMultilang = (t: TFunction) => (multilang: MultilangDto): MultilangDto =>
  Object.keys(multilang).reduce((acc, curr) => ({ ...acc, [curr]: cloneText(t)(multilang[curr]) }), {} as MultilangDto)

export const cloneText = (t: TFunction) => (text: string): string => text && text.trim() !== '' ? `${text}_${t("common.labels.cloned")}` : '';


export const all = (...fs: any) => (t: any) => fs.map(f => f instanceof Function ? f : () => f).reduce((t: any, f) => f(t), t)

export const arrayOf = <T>(vals: T | T[]) => Array.isArray(vals) ? vals : vals ? [vals] : []

export const isColor = (color: string | undefined): boolean => {
  if (color) {
    if (['inherit', 'initial', 'transparent'].includes(color)) return false
    const s = new Option().style;
    s.color = color;
    return s.color !== '';
  }
  return false
}

export const getHostname = () => {
  const hostport = window.location.port === undefined || window.location.port === '80' ? '' : `:${window.location.port}`
  return `${window.location.protocol}//${window.location.hostname}${hostport}`
}

export const stripHtmlTags = (val: string, keepTags: string[] = []) => val ? val.replace(/<\/?([a-z]+)[^>]*>/gi, function (match, tag) { return keepTags.map(t => t.toLowerCase()).includes(tag.toLowerCase()) ? match : '' }) : ''
export const stripMultiHtmlTags = (val: MultilangDto | undefined): MultilangDto => Object.keys(val ?? {}).reduce((acc, key) => ({ ...acc, [key]: stripHtmlTags(val?.[key]) }), {})
export const stripHtmlTagsForPdf = (val: string, keepTags: string[] = []) => _.unescape(stripHtmlTags(val, keepTags))
export const stripMultiHtmlTagsForPdf = (val: MultilangDto | undefined): MultilangDto => Object.keys(stripMultiHtmlTags(val)).reduce((acc, key) => ({ ...acc, [key]: _.unescape(val?.[key]) }), {})

const blockTags = [
  '<ul>',
  '<li>',
  '<ol>',
  '<br>',
  '<br/>',
  '<p>',
  `<pre`,
  '</p>'
]

export const normalizeQuillValue = (val: string) => {
  if (!val) return val
  //Strip the surrounding <p> tags that quill puts in place even if this is not a block value
  const stripped = val.trim().match(/^<p>(.*?)<\/p>$/i)?.[1]
  if (!stripped) return val
  return isQuillBlockValue(stripped) ? val : stripped
}

export const isQuillBlockValue = (val: string) =>
  blockTags.some(bt => val.match(new RegExp(bt, 'i')) !== null ? true : false)


export const arrayPartition = (array: any[], fun: { (array: any[]): number }[], iteration = 1): any[] => {

  const computedIndex = fun[iteration % fun.length](array)

  if (computedIndex === -1) return array.length > 0 ? [array] : []

  return array.length ? [array.splice(0, computedIndex)].concat(arrayPartition(array, fun, iteration + 1)).filter(a => a.length > 0) : []

}