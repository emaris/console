import { Label } from '#app/components/Label';
import { icns } from '#app/icons';
import { useT } from '#app/intl/api';



export const PredefinedLabel =  () => {

    const t = useT()

    return <Label mode='tag' iconStyle={{color:'white'}} style={{color: 'white', background:'orange'}} icon={icns.system} title={t('common.labels.predefined')}/>
    
}