
import { Tooltip } from "antd"
import { useTime } from "#app/time/api"
import moment from "moment"
import * as React from "react"
import { useT } from '#app/intl/api'
import { Paragraph, Text } from "../components/Typography"
import { UserLabel } from "../user/Label"



export type Lifecycle<T=any> = Partial<{

    state: T
    previousState: string,
    created: number,
    lastModified: number,
    lastModifiedBy: string

    children: React.ReactNode

}>

type Props = Lifecycle<any> & {

    style?: React.CSSProperties
}

export type Lifecycled<T=any> = {

    lifecycle: Lifecycle<T>
}

export const newLifecycle = <T extends any>(initial?:T): Lifecycle<T> => ({

    state:initial!,
    created: undefined!

})

export const LifecycleSummary = (props: Props) => {

    const t = useT()

    const time = useTime()

    const { created, lastModified, lastModifiedBy, children } = props

    const tooltipStyle = { fontSize: "12px" }

    const unknownLabel = t("common.labels.unknown").toLocaleLowerCase()

    return <>

        <div className="sidebar-property">

            <Paragraph><Text smaller>{t("common.lifecycle.created")}</Text></Paragraph>
            <Paragraph><Text smaller className="emphasis"><Tooltip overlayStyle={tooltipStyle} title={created && time.format(moment(created))}>{created ? time.format(moment(created), 'short') : unknownLabel}</Tooltip></Text></Paragraph>

            <Paragraph><Text smaller>{t("common.lifecycle.modified")}</Text></Paragraph>
            <Paragraph><Text smaller className="emphasis">
                <Tooltip overlayStyle={tooltipStyle} title={lastModified && time.format(moment(lastModified))}>{lastModified ? time.format(moment(lastModified),'short') : unknownLabel}</Tooltip>
            </Text></Paragraph>

            <Paragraph><Text smaller>{t("common.lifecycle.by")}</Text></Paragraph>
            <Paragraph>
                <Text style={{ display: "flex", justifyContent: "flex-end" }} smaller className="emphasis">
                    {lastModifiedBy ? <UserLabel noIcon user={lastModifiedBy} /> : unknownLabel}
                </Text>
            </Paragraph>

            {children}

        </div>
    </>
}