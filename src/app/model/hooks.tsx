import { useCurrentLanguage } from "#app/intl/api";
import { defaultLanguage } from "../intl/model";
import { MultilangDto } from "./multilang";


export const useLocale = () => {

    const lang = useCurrentLanguage()

    return { 
        
        l: (text:MultilangDto|undefined) : string => text ? text[lang]! || text[defaultLanguage]! : undefined!

    }

} 