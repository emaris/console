

import { useCurrentLanguage, useT } from '#app/intl/api';
import { allLanguages, defaultLanguage, Language } from "../intl/model";
import { clone, random } from "../utils/common";


export type MultilangDto = {

    [key in Language]?: string

}

export type Multilang = MultilangDto & {

    inCurrent: () => string
    in: (lang: Language) => string
    compareTo: (other: Multilang) => number
    setCurrent: (val: string) => Multilang
    setIn: (lang: Language) => { to: (val: string) => Multilang }
}

export const newMultiLang = () => ({ 'en': undefined }) as MultilangDto

export const isMultilang = (ml: any | undefined): ml is MultilangDto | undefined => ml ? Object.keys(ml).every(k => allLanguages.includes(k as Language)) : ml

export const fullMultilang = () => allLanguages.reduce((acc, n) => ({ ...acc, [n]: undefined }), {}) as MultilangDto

export const noMultilang = (en: string = "unknown") => ({ en }) as MultilangDto

export const randomMultilang = (base: string = "value") => { const val = random(base); return { en: val, fr: val + " en français" } }

export const cloneMultilang = (ml: MultilangDto) => Object.keys(ml).reduce((acc, lang) => ({ ...acc, ...(ml[lang] ? { [lang]: clone(ml[lang]) } : {}) }), {})


export const useTranslatedMultilang = () => {
    
    const t  = useT()

    return (key:string) => allLanguages.reduce((acc,lng) => ({ ...acc, [lng]: t(key, { lng })}), {})

}

export const mergeMultilang = (m1: MultilangDto, m2: MultilangDto) => {
    if (m1 === undefined) return m2
    if (m2 === undefined) return m1
    return Object.keys(m2).reduce((acc, lang) => ({ ...acc, [lang]: m1[lang] !== undefined ? m1[lang] : m2[lang] }), {} as MultilangDto)
}

export const useCompareMultiLang = () => {

    const l = useL()

    return (m1: MultilangDto | undefined, m2: MultilangDto | undefined) => l(m1 ?? { en: "" })?.localeCompare(l(m2 ?? { en: "" }))
}

export const useL = () => {

    const l = useMultilang()

    return (n: MultilangDto | undefined) => l(n).inCurrent()
}

export const useMultilang = () => {

    const currentLanguage = useCurrentLanguage()

    return (ml: MultilangDto | undefined = newMultiLang()): Multilang => {

        const self =  {

            ...ml,

            inCurrent: () => self.in(currentLanguage),

            in: (lang:Language) => ml[lang]! || ml[defaultLanguage]!,

            compareTo: (other:Multilang) => self.inCurrent().localeCompare(other.inCurrent()),

            setCurrent: (val: string) => self.setIn(currentLanguage).to(val),

            setIn: (lang: Language) => ({

                to: (val: string) => {

                    ml[lang] = val
                    return self;

                }
            })

        }

        return self
    }
}