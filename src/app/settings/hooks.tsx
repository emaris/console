

import { Button } from "#app/components/Button"
import { DrawerProps, RoutableDrawerProps, useRoutableDrawer } from "#app/components/Drawer"
import { icns } from "#app/icons"
import { useT } from '#app/intl/api'
import * as React from "react"



export const useSettingsDrawer  = (opts:Partial<RoutableDrawerProps>={}) => {

    const t = useT()

    const {id='settings',title=t("settings.title"),icon=icns.settings} = opts

    const {Drawer,open,close,visible,route} = useRoutableDrawer({id,title,icon})

    const DrawerProxy = (props:Partial<DrawerProps>) => <Drawer width={700} icon={icon} title={title} {...props}>{props.children}</Drawer>

    //eslint-disable-next-line
    const StableProxy = React.useCallback(DrawerProxy,[visible])

    const settingsButton = <Button icn={icon} linkTo={route()}>{t("settings.title")}</Button>

    return {SettingsDrawer:StableProxy,openSettings:open,settingsRoute:route, closeSettings:close,visibleSettings:visible,settingsButton}

}

