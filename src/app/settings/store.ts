
import { useT } from '#app/intl/api';
import { useToggleBusy } from "#app/system/api";
import { through } from "#app/utils/common";
import { notify, showAndThrow } from "#app/utils/feedback";
import { isEmpty } from "lodash";
import { useContext } from 'react';
import { settingsApi, useSettingsCalls } from "./calls";
import { settingsName, settingsType } from "./constants";
import { SettingsContext } from './context';
import { BaseSettings, useSettingsModel } from "./model";
import { usePreload } from 'apprise-frontend-core/client/preload';


export type SettingsState = {

    appsettings: BaseSettings

}


export const useSettings = () => {
    
    const state = useContext(SettingsContext)

    return {
        
        get: <T extends BaseSettings>() => state.get().appsettings as T
    
    }
}

export const useSettingsStore = () => {

    const state = useContext(SettingsContext)

    const get = useSettings()

    const call = useSettingsCalls()
    
    const model = useSettingsModel()

    const t = useT()

    const name = t(settingsName).toLowerCase()

    const type = settingsType

    const toggleBusy = useToggleBusy()

    const preload = usePreload()

    const self = {


        set: (settings: BaseSettings) => state.set(s => s.appsettings = settings)

        ,
 
       ...get

        ,

        isReady: () => !!self.get()

        ,

        fetch: (forceRefresh = false): Promise<BaseSettings> =>

            self.isReady() && !forceRefresh ? Promise.resolve(self.get())

                :

                toggleBusy(`${type}.fetch`, t("common.feedback.load", { plural: name }))

                    .then(_ => console.log(`fetching settings...`))
                    .then(_ => preload.get<BaseSettings>(settingsApi) ?? call.fetch())
                    .then(fetched => model.overlayDefaults(isEmpty(fetched) ? {} : fetched))
                    .then(through(self.set))



                    .catch(e => showAndThrow(e, t("common.calls.fetch_all_error", { plural: name })))
                    .finally(() => toggleBusy(`${type}.fetch`))

        ,

        save: (settings: BaseSettings): Promise<BaseSettings> =>

            toggleBusy(`${type}.save`, t("common.feedback.save_changes", { plural: name }))

                .then(_ => console.log(`save settings...`))
                .then(_ => call.save(settings))
                .then(through(self.set))
                .then(through(() => notify(t('common.feedback.saved'))))

                .catch(e => showAndThrow(e, t("common.calls.save_one_error", { singular: name })))
                .finally(() => toggleBusy(`${type}.save`))


    }

    return self
}