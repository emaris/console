import { Module } from "#app/module/model"
import { ValidationReport } from "#app/utils/validation"
import { settingsIcon, settingsName, settingsType } from "./constants"
import { BaseSettings, SettingsProps } from "./model"


export const settingsmodule : Module = {

      
    icon: settingsIcon,
    type: settingsType,

    nameSingular: settingsName,
    namePlural: settingsName

}

export type SettingsSlot<T extends BaseSettings=BaseSettings> = {

    renderIf?: () => boolean
    defaults: () => Partial<T>,
    validate?: (settings:T) => ValidationReport
    component: (_:SettingsProps<T>) => JSX.Element
}