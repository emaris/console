import { Button } from '#app/components/Button'
import { NoSuchRoute } from '#app/components/NoSuchRoute'
import { Mode } from '#app/config/model'
import { useConfig } from '#app/config/state'
import { useFormState } from '#app/form/hooks'
import { icns } from '#app/icons'
import { useT } from '#app/intl/api'
import { Tab } from '#app/scaffold/Tab'
import { Topbar } from '#app/scaffold/Topbar'
import { useLogged } from '#app/user/store'
import { paramsInQuery } from '#app/utils/routes'
import * as React from 'react'
import { useLocation } from 'react-router-dom'
import { Drawer } from '../components/Drawer'
import { MenuItem } from '../scaffold/MenuItem'
import { actionDrawerParam, DrawerContext } from "../scaffold/Scaffold"
import { settingsIcon, settingsType } from './constants'
import { Info } from './Info'
import { BaseSettings, useSettingsModel } from './model'
import { useSettingsStore } from './store'
import "./styles.scss"


type Props = {

    onClose?: () => void
}

export const settingsId = "settings"

export const SettingsItem = () => {

    const t = useT();

    const logged = useLogged()

    return <MenuItem enabled={logged.hasNoTenant()} icon={settingsIcon} title={`${t("settings.title")}`} onClickToggle={settingsId} />
}



export const Settings = (props: Props) => {

    const t = useT()
    const { search } = useLocation()

    const logged = useLogged()

    const { stab } = paramsInQuery(search)

    const location = useLocation()

    const ctx = React.useContext(DrawerContext)

    const config = useConfig()

    const settings = {...useSettingsModel(), ...useSettingsStore()}

    const settingsData = settings.get()

    const state = useFormState<BaseSettings>(settingsData)

    React.useEffect(() => {

        if (location.state)
            state.softReset(settingsData)

        // eslint-disable-next-line
    }, [location.state])

    const { edited, dirty, reset } = state

    const close = () => Promise.resolve(ctx.close(settingsId, "stab")).then(_ => props.onClose && props.onClose())

    const plugins = settings.allPlugins()

    const report = settings.valiadatePluginsFor(edited)

    const activePlugin = plugins.find(m => m.type === stab) ?? plugins[0]

    const childprops = { state, report, close }

    return <Drawer readonly={!logged.isAdmin()} routeId={actionDrawerParam} width={700}
        warnOnClose={dirty}
        visible={ctx.isVisible(settingsId)}
        onClose={close}
        icon={settingsIcon} title={t("settings.title")}>

        <Topbar offset={64} tabParam='stab'>

            {plugins.map((m, i) =>

                <Tab default={i === 0} key={m.type} id={m.type} name={t(m.nameSingular)} />
            )}

            {/* default if no dev and no modules */}
            {/* {config.runmode()===Mode.dev &&
                         <Tab default={plugins.length===0} id='info' name={t("settings.info_tab")} /> 
                        }  */}

            {/* {config.runmode()===Mode.dev &&
                             // default in dev mode
                             <Tab default id='dev' name={t("settings.dev_tab")} /> 
                        }  */}



            <Button icn={icns.revert} enabled={dirty} type="danger" onClick={() => reset()}>{t("common.buttons.revert")}</Button>
            <Button icn={icns.save} enabled={dirty} disabled={report.errors() > 0} dot={report.errors() > 0}
                type="primary" onClick={() => settings.save(edited).then(() => reset(edited, false))}>{t("common.buttons.save")}</Button>

        </Topbar>


        {
            stab === 'info' && config.runmode() === Mode.dev ? <Info /> :
                activePlugin ? React.createElement(activePlugin[settingsType].component, childprops) : <NoSuchRoute />
        }

    </Drawer>

    

}