
import { useT } from '#app/intl/api'
import { usePreload } from 'apprise-frontend-core/client/preload'
import { useCalls } from "../call/call"
import { showAndThrow } from "../utils/feedback"
import { BaseSettings, Info } from "./model"

const infoApi = "/info"

export const useInfo = () => { 
    
    const t = useT()

    const {at} = useCalls()

    const singular = "service"
    
    const preload = usePreload()

    return {

        fetchInfo: (service?:string) => 
            
            (preload.get<Info>(infoApi) ?? at(infoApi,service).get<Info>()).catch( e => showAndThrow(e,t("common.calls.fetch_one_error",{singular})))
   
            
    }

}


export const settingsApi = "/settings"

export const useSettingsCalls = () => {

    const {at} = useCalls()
   
 
    return ({

        fetch: () =>  at(settingsApi).get<BaseSettings>()

        ,

        save: (s:BaseSettings) =>  at(settingsApi).put<BaseSettings>(s)
        
        
        
    })
}
