import { PushEventSlot } from '#app/push/module';
import shortid from 'shortid';
import { pusheventSettingsTopic } from './constants';
import { BaseSettings } from './model';
import { useSettingsStore } from './store';

export type SettingsChangeEvent = {

    settings: BaseSettings
}



export const usePushSettingsSlot = (): PushEventSlot<SettingsChangeEvent> => {

    const store = useSettingsStore()

    return {

        onSubcribe: () => {

            console.log("subscribing for settings changes...")

            return [{

                topics: [pusheventSettingsTopic]

                ,

                onEvent: (event: SettingsChangeEvent, history, location) => {

                    const { settings } = event

                    console.log("updating settings on push notification...", { settings })

                    const currentUrl = `${location.pathname}${location.search}`

                    store.set(settings)

                    console.log(location.search)

                    history.push(currentUrl, shortid())
                }

            }]
        }
    }
}