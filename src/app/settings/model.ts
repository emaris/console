import { FormState } from "#app/form/hooks"
import { useModuleRegistry } from '#app/module/registry'
import { ValidationReport, withReport } from "#app/utils/validation"
import { settingsType } from "./constants"
import { SettingsSlot } from "./module"

export type Info = {
    name: string
    build: {
        version: string
        timestamp: string
        commit: string
        user: string
        message: string
        time: string
    }
    javaopts: string
    toggles: string[]
}

export type SettingsProps<T extends BaseSettings = BaseSettings> = {

    state: FormState<T>
    report: ValidationReport
    close: () => void

}


export type BaseSettings = {}

export const useSettingsModel = () => {

    const registry = useModuleRegistry()

    const self = {

        allPlugins: () => registry.allWith(settingsType)
            .filter(m => {

                const renderIf = (m[settingsType] as SettingsSlot).renderIf

                return !renderIf || renderIf()

            })

        ,

        valiadatePluginsFor: (settings: BaseSettings) => withReport(

            self.allPlugins().map(p => (p[settingsType] as SettingsSlot).validate?.(settings as any) ?? {})
                .reduce((acc, next) => ({ ...acc, ...next }), {})
        )

        ,

        overlayDefaults: (settings: BaseSettings) => {

        
           const defaults =  registry.allSlotsWith<SettingsSlot>(settingsType).map(slot => slot.defaults()).reduce((acc, next) => ({ ...acc, ...next }), {})

            return {...settings,...defaults}

         }


            
    }

    return self;
}