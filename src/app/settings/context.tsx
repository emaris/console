import { fallbackStateOver, State } from 'apprise-frontend-core/state/api';
import { createContext } from 'react';
import { SettingsState } from './store';







export const initialSettings: SettingsState = {

    appsettings: undefined!
}


export const SettingsContext = createContext<State<SettingsState>>(fallbackStateOver(initialSettings))
