import { Button } from "#app/components/Button"
import { Form } from "#app/form/Form"
import { FormState } from "#app/form/hooks"
import { icns } from "#app/icons"
import { Topbar } from "#app/scaffold/Topbar"
import * as React from "react"
import { useT } from '#app/intl/api'


type Props<T> = React.PropsWithChildren<{

    state: FormState<T>
    onSave: () => Promise<any>
    
    className?: string
    style?: React.CSSProperties
    
    
}>


export const SettingsForm = <T extends any> (props:Props<T>) => {

    const t = useT()

    const { state, onSave, className='', style, children } = props

    const {initial,dirty,reset} = state
    

    const revertBtn = <Button icn={icns.revert} enabled={dirty} onClick={() => reset(initial,true)}>{t("common.buttons.revert")}</Button>
    const saveBtn = <Button type="primary" icn={icns.save} enabled={dirty} onClick={()=>onSave()}>{t("common.buttons.save")}</Button>

    return <div className={className} style={style}>

                <Topbar autoGroupButtons={false}>

                    {saveBtn}
                    {revertBtn}

                </Topbar>

                <Form className="settings-form" state={state} sidebar>
                    { children }
                </Form>
    </div>
}