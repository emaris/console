import { useConfig } from '#app/config/state'
import { useT } from '#app/intl/api'
import { buildinfo } from '../buildinfo'
import { Row, ServiceRow, Sheet } from '../components/Sheet'
import { Paragraph, Text } from '../components/Typography'
import "./styles.scss"

export const Info = () =>{

   const t = useT()

   const {version, timestamp, commit} = buildinfo

   const config = useConfig()

   const services = config.get().services;

 
   return <Sheet>

                <Row label="Interface">
                    <Paragraph><Text>{version} <Text secondary>({commit.id})</Text></Text></Paragraph>
                    <Paragraph><Text secondary smaller>{t("settings.builtOn")} {timestamp}</Text></Paragraph>
                    <Paragraph><Text secondary smaller>{t("settings.builtWith")} </Text><Text secondary smaller italics>"{commit.message}"</Text></Paragraph>
                    <Paragraph><Text secondary small>{t("settings.committedBy")} {commit.author}</Text></Paragraph>
                    <Paragraph><Text secondary small>{t("settings.committedOn")} {commit.time}</Text></Paragraph>
                </Row>


                {Object.keys(services).map(k=>
           
                    <ServiceRow key={k} service={k} label={services[k].label} />

                )}
               
                {/* <Row startsGroup label="Active Toggles">
                    <Text code>DEV</Text>
                    <Text code>FEATURE-X</Text>
                </Row> */}

            </Sheet>

}