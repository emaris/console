import { HtmlSnippet } from "#app/components/HtmlSnippet";
import { Form } from "antd";
import { FormItemProps } from "antd/lib/form/FormItem";
import * as React from "react";
import { Paragraph } from "../components/Typography";
import { success, Validation } from "../utils/validation";
import { FieldRow } from "./FieldRow";
import "./styles.scss";




export type FieldProps = Pick<FormItemProps, 'id' | 'label'|'help' | 'validateStatus' | 'className' | 'style' | 'required' > &  {

    disabled?: boolean
    validation?: Validation
    fieldClassName?: string
    fieldStyle?: React.CSSProperties
    grouped?:boolean                        //  placement in row is handled externally.
    standalone?:boolean                     //  does not partecipate of a form.
    inactive?: boolean

    labelStyle?: React.CSSProperties

    labelDecorations?: React.ReactNode[]
    focusDecorations?: React.ReactNode[]
   
}

export type Props = React.PropsWithChildren<FieldProps>

export const Field = ( props  :Props) =>  {

    const {validation=success(), fieldStyle, fieldClassName, children, standalone,grouped,disabled=false,inactive=false,...rest}  = props

    const {label,labelStyle,className,style,required,labelDecorations = [],focusDecorations = []} = rest

    const decorations = [

        ...[label, ...labelDecorations].map((d, i) => <div key={`decoration-${i}`} className='field-label-item'>{d}</div>),
        ...focusDecorations.map((d, i) => <div key={`silent-decoration-${i}`} className='field-label-item label-silent-item'>{d}</div>)
    ]
    
    const itemprops : FormItemProps = {className,style,required}
    
    const item = <Form.Item style={{...fieldStyle}} 
                    label={label ? <div className='label-decorations' style={labelStyle ?? {}}>{decorations}</div> : undefined}
                    className={fieldClassName}
                    help={ validation.msg && <Paragraph type={disabled ? undefined : typeFor(validation) } ellipsis={{ rows: 1 }}><HtmlSnippet snippet={validation.msg} /></Paragraph>}
                    colon={false} validateStatus={disabled ? 'success' : validation.status} {...itemprops} hasFeedback={false}>

                {children}

                </Form.Item>

    const content =  inactive ? 
                        <div className="form-field-inactive">{item}</div>
                    : item

   return grouped ? content : <FieldRow className={fieldClassName} standalone={standalone} side={validation.help}> 
                                    <div style={{flexGrow:1}}>{content}</div>
                              </FieldRow>
}

const typeFor = (v:Validation) => {
    switch(v.status) {
        case "error": return "danger"
        case "warning": return "warning"
        default: return
    }

}