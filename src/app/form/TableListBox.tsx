import { Table } from "antd"
import * as React from "react"
import { controlsFor, ListBoxProps, ListBoxState } from "./ListBox"


export type TableType<T> = {

    name: string

    columns : {

        key:string, 
        title?: string, 
        width?:number,
        sort?: false | ( (a:T,b:T) => boolean )

    }[]

    noClickSelect?: boolean 

}

export const TableListBox = <T extends any> (props:ListBoxProps<T> & ListBoxState & {readOnly?:boolean}) => {

    
    const {items, type, idOf,select,height} = props
    const {columns,noClickSelect} = type as TableType<T>

    const className = props.readOnly ? "table-read-only" : ""
 

    const cols = columns.map( c=> { 

        const sort = c.sort===false || (c.sort instanceof Function ? c.sort : (a:T,b:T)=> a[c.key].localeCompare(b[c.key]) )

        return <Table.Column width={c.width} key={c.key} dataIndex={c.key} title={c.title || c.key } sorter={sort} />
        
    });


    const actioncol =   <Table.Column className="table-controls-column" key="actions" render={ (_, t:T) => 

       <div className="flex-controls"> {controlsFor(t,props)} </div>
        
    } />
    
    return   <Table className={className} dataSource={items} scroll={{y:height}} pagination={false} rowKey={idOf} onRow={(t,_)=> noClickSelect ? {}: {onDoubleClick: () => select(t)}}>
                
                {   [...cols, actioncol] }
               
            </Table>
        
    
        
    

}