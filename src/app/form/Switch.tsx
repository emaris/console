import { Switch as AntSwitch } from 'antd';
import * as React from 'react';
import { Field, FieldProps } from './Field';
import { SwitchProps } from 'antd/lib/switch';
import { ReadOnlyContext } from '../scaffold/ReadOnly';

type Props = FieldProps & SwitchProps & {
    children: boolean | undefined
    enabledOnReadOnly?: boolean
    light?:boolean
}

export const Switch = ({children,enabledOnReadOnly,fieldStyle,light,grouped,disabled=false,...rest}:Props) => {

    const readOnlyContext = React.useContext(ReadOnlyContext)
    const readOnlyState = readOnlyContext && !enabledOnReadOnly

    const content =  <AntSwitch {...rest} checked={children} disabled={disabled || readOnlyState}></AntSwitch>

    return  light ? content : <Field fieldStyle={fieldStyle} grouped={grouped} disabled={disabled} className="switch" {...rest}>{content}</Field>
}
