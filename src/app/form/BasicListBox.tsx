import { List } from "antd"
import * as React from "react"
import { SortableContainer, SortableElement } from "react-sortable-hoc"
import { controlsFor, ListBoxProps, ListBoxState } from "./ListBox"
import arrayMove from "array-move"


export type BasicType<T> = {

    name: string

    render: (item:T) => React.ReactElement
    noClickSelect?: boolean 

}

export const BasicListBox =   <T extends any=any>   (props:ListBoxProps<T> & ListBoxState & {readOnly?:boolean}) => {

    const {items,select} = props

    const {render, noClickSelect } = props.type as BasicType<T>

    const className = props.readOnly ? "item item-disabled" : "item"

    const SortableItem = SortableElement( (item:T,index:number)  => 
    
        <div className={className} key={index}> 

            <div className="item-content" onDoubleClick={()=>noClickSelect || select(item)}> {/* click shortcut does not inhibit links in render() */}
            
                <div>{render(item)}</div>
            </div>

            {controlsFor(item,props)} 
        
        </div>
        
    )

    const SortableList = SortableContainer( ()=> <List dataSource={items} renderItem={(t:T,i:number) => <SortableItem {...t as any} index={i} /> } />) 
 
    return <SortableList distance={1} helperClass="dragged" lockAxis="y" 
                         onSortEnd={ ({oldIndex, newIndex}) => props.readOnly ? props.onChange(arrayMove(items,oldIndex,oldIndex)) : props.onChange(arrayMove(items,oldIndex,newIndex)) } />

} 
