import * as React from 'react';
import { Field, FieldProps } from './Field';

type Props = React.PropsWithChildren<FieldProps & {

    light?: boolean
}>

export const SimpleBox = (props:Props) => {

    const {children, light,...rest} = props

    const content =  <div className="simplebox">
                        {children}    
                    </div>
   
    return  light? <div style={rest.style} className={rest.className}>{content}</div>: <Field  {...rest}>{content}</Field>
}
