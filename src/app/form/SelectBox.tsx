import { Select } from 'antd';
import { utils } from 'apprise-frontend-core/utils/common';
import { isArray } from 'lodash';
import * as React from 'react';
import { icns } from '../icons';
import { Field, FieldProps } from './Field';
import { ReadonlyProps, useReadonly } from './ReadonlyBox';
import { SimpleBox } from './SimpleBox';
import { ResetBtn } from './resetbtn';

type KeyType = string | number | undefined

export type SelectBoxProps<T> = FieldProps & ReadonlyProps & {

    multi?: boolean

    children: T[]
    defaultValue?: KeyType | KeyType[]
    selectedKey: KeyType | KeyType[]

    getkey?: (t: T) => KeyType
    getlbl?: (t: T) => React.ReactNode

    lblText?: (t: T) => string,
    match?: (input: string, t: T) => boolean

    disabledOption?: (t: T) => boolean

    onChange: ((t: any) => void) | ((ts: any[]) => void)

    renderDisabled?: () => JSX.Element

    placeholder?: string

    includeSelected?: boolean

    allowClear?: boolean
    small?: boolean

    undefinedOption?: React.ReactNode

}

const noKey = 'undefined'
const defmatch = (i: string, t: any) => t === noKey ? false : JSON.stringify(t).indexOf(i) >= 0




export const SelectBox = <T extends Object>(props: SelectBoxProps<T>) => {

    const {
        children: $children = [],
        className,
        disabledOption,
        renderDisabled,
        focusDecorations,
        undefinedOption,
        fieldStyle,
        getkey: $getkey = t => t as unknown as KeyType,
        getlbl: $getlbl = $getkey,
        match: $match = defmatch,
        standalone,
        grouped,
        selectedKey: $selectedKey,
        lblText,
        placeholder,
        ReadOnlyBox,
        disabled,
        includeSelected,
        allowClear,
        small,
        defaultValue,
        onChange: $onChange,
        light=standalone,

        ...rest } = useReadonly(props)


    const { readonly, readonlyMode, readonlyIcon, readonlyUnlocked } = rest

    const noOption = noKey as unknown as T

    const children = undefinedOption ? [noOption, ...$children] : $children
    const getkey = undefinedOption ? t => t === noOption ? noKey : $getkey(t) : $getkey
    const getlbl = undefinedOption ? t => t === noOption ? undefinedOption : $getlbl(t) : $getlbl

    const multi = $selectedKey ? Array.isArray($selectedKey) : props.multi    // single or multiple choice?

    const selectedKey = undefinedOption ?
        multi ?
            ($selectedKey as KeyType[]).length === 0 ? [noKey] : $selectedKey
            : $selectedKey ?? noKey
        : $selectedKey ?? (multi ? [] : undefined)

    const match = undefinedOption ? (i, t) => t === noOption ? false : $match(i, t) : $match

    const onChange = undefinedOption ?
        multi ?
            ts => $onChange((selectedKey as KeyType[]).includes(noKey) ?
                ts.filter(t => t !== noOption)
                : ts.includes(noOption) ?
                    []
                    : ts)
            : t => $onChange(t === noOption ? undefined! : t)
        : $onChange

    const map = children.reduce((acc, t) => ({ [getkey(t)!]: t, ...acc }), {})    // key=>t lookup map.

    const fold = (key: KeyType) => key && ({ key, label: getlbl(map[key]) })    // turns to ants labelInValue format. can be undefined, eg to begin with


    const unselected = (t: T) => multi ? !(selectedKey as KeyType[]).includes(getkey(t)) : selectedKey !== getkey(t)    // exclude already selected options.

    const options = children.filter(t => includeSelected || unselected(t)).map((t, i) => <Select.Option disabled={disabledOption?.(t)} key={i} value={getkey(t)}>{getlbl(t)}</Select.Option>)

    const multidef = defaultValue ? isArray(defaultValue) : multi 
       
    const defval = multidef ? ((defaultValue ?? [] ) as KeyType[]).map(fold) : isArray(defaultValue) ? fold(defaultValue[0] as KeyType ) : defaultValue ? fold(defaultValue as KeyType )  : defaultValue

    const folded = multi ? (selectedKey as KeyType[]).map(fold) : fold(selectedKey as KeyType)

    const emptyValue = !(multi ? (folded as any)?.length : folded)
    const emptyDefault = !(multi ? (defval as any)?.length : defval)

    // should defaulting logic trigger at all? it should on mount and after a reset. 
    // without it, defaulting would trigger also when a user blanks a field: it'd look like a reset.
    const defaultMode = React.useRef(true)
 
    // show we show default?
    const showDefault = defaultMode.current ? !emptyDefault && emptyValue : false
   
    // show we show allow reset to default?
    const resettable =  !(defaultMode.current || readonly || emptyDefault || utils().deepequals(folded, defval))

    // remount if default mode or defaults change (or field will not refresh).
    const key = defaultMode.current && showDefault ? JSON.stringify(defaultValue): undefined 

   // console.info({key, multi, defaultMode, showDefault, defaultValue, multidef, emptyDefault, emptyValue, resettable})
    
    const decorations = [...focusDecorations ?? []]


    // add readonly icon only if can be edited or has been unlocked. 
    if (readonlyMode === 'chrome' && (readonly || readonlyUnlocked))
        decorations.push(readonlyIcon)

    if (resettable)
        decorations.push(<ResetBtn enabledOnReadonly={true} onClick={() => {
            defaultMode.current = true; 
            onChange(undefined!)
    
    }} />)

    const foldedOnChange = folded =>{ 
    
        defaultMode.current = false

        onChange(Array.isArray(folded) ? folded.map(v => v.key) : folded?.key)
    
    }


    const classes = `selectbox ${className ?? ''} ${readonly ? 'readonlybox' : ''} ${readonlyMode === 'content' ? 'readonlybox-content' : ''} ${showDefault ? 'selectbox-default-value' : ''}`

    const ref = React.useRef<any>(undefined!)

    const valueprops = showDefault ? { defaultValue: defval } : { value: folded }

    const select = () => <Select key={key} className={showDefault ? 'selectbox-default-value' : ''} ref={ref} style={fieldStyle}

        labelInValue mode={multi ? 'multiple' : 'default'}

        {...valueprops}

        onChange={readonly ? () => { } : foldedOnChange}

        filterOption={(i, o) => {
            const key = lblText ? lblText(map[o.props.value!]) : o.props.children ? o.props.children.toString().toLowerCase() : map[o.props.value!.toString().toLowerCase()]
            return o.props.value ? match(i.toLowerCase(), key.toLowerCase()) : false
        }}

        suffixIcon={icns.down}
        removeIcon={icns.close}

        // we must manually propagate readonly classes to dropdown mounted out-of-band, at the bottom of the dom.
        dropdownClassName={`${classes} ${readonly ? 'readonlybox' : ''} ${readonlyMode === 'content' ? 'readonlybox-content' : ''}`}


        allowClear={allowClear}
        clearIcon={icns.removeEntry}

        disabled={disabled}

        showSearch
        showArrow


        size={small ? 'small' : 'default'}

        placeholder={placeholder}>

        {options}

    </Select>

    return standalone ?

        <div style={{ ...props.style, display: "flex", alignItems: "center" }} >

            {light ?

                select() :

                <Field grouped={grouped} disabled={disabled} className={classes} {...rest} >
                    {select()}
                </Field>
            }
        </div>

        :

        props.disabled && renderDisabled ?

            <SimpleBox grouped={grouped} {...rest} >
                {renderDisabled()}
            </SimpleBox>

            :

            light ?

                <div className={classes} style={rest.style} >
                    {select()}
                </div>

                :

                <Field grouped={grouped} disabled={disabled} className={classes} focusDecorations={decorations}  {...rest} >{

                    // if we're not showing chrome, we show a fallback for no content.
                    readonly && readonlyMode === 'content' && emptyValue && emptyDefault ?

                        <div className='readonly-placeholder'>-</div>

                        :

                        select()

                }</Field>


}