import { Tooltip } from 'antd'
import { Button } from '#app/components/Button'
import { icns } from '#app/icons'
import * as React from 'react'
import { useT } from '#app/intl/api'

export type Props = {

    disabled?: boolean | undefined
    enabled?: boolean | undefined

    isProtected: boolean | undefined
    onChange: (_: boolean) => void

    size?: 'small' | 'default'
    mode?: 'icon' | 'text'

    singular: string
    plural: string


}

export const ProtectButton = (props: Props) => {

    const t = useT()

    const { mode = 'icon', size = 'default', isProtected, disabled, enabled, onChange, singular, plural } = props

    const tflag = isProtected ? 'protected' : 'unprotected'

   const tip =  <>
            <div>{t(`common.fields.protected.msg_${tflag}_action`, { singular: t(singular).toLowerCase() })}</div>
            <div style={{ marginTop: 4 }}>{t(`common.fields.protected.help_${tflag}`, { plural: t(plural).toLowerCase() })}</div>
        </>



    return mode === 'icon' ?

        <Tooltip mouseEnterDelay={.7} title={tip}>
              <Button enabledOnReadOnly noborder disabled={disabled} enabled={enabled} className={`button-lock ${size} ${isProtected ? 'btn-locked' : 'btn-unlocked'}`} onClick={() => onChange(!isProtected)}>
                    {isProtected ? icns.lock() : icns.unlock()}
                </Button>
        </Tooltip>

        :

        <Button enabledOnReadOnly icn={isProtected ? icns.lock() : icns.unlock()} noborder disabled={disabled} enabled={enabled} className={`button-lock ${isProtected ? 'btn-locked' : 'btn-unlocked'}`} onClick={() => onChange(!isProtected)}>
            {isProtected ? t('common.fields.protected.msg_unprotect') : t('common.fields.protected.msg_protect')}
        </Button>
}

