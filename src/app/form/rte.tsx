import { useRef } from 'react'
import ReactQuill from 'react-quill'

import '#app/form/quill-mention'
// import { useLogged } from '#app/user/store'


const defaultToolbar = [

    [{ 'header': [1, 2, 3, false] }],
    ['bold', 'italic', 'underline', { 'list': 'ordered' }, { 'list': 'bullet' }, 'link', 'clean']

]


export const mentionmodule = {
    allowedChars: /^[A-Za-z0-9\-._]*$/,
    maxChars: 100,
    mentionDenotationChars: ['@'],
    showDenotationChar: true,
    dataAttributes: [],
    mentionBlotTagName: 'span',
    mentionBlotClassname: 'mention',
    contentEditable: false
}

export type Mention = {
    id: string
    value: string
}

export const mentionList = (term, mentions: Mention[]) => term.length === 0 ? mentions : mentions.filter(v => v.value?.toLowerCase().includes(term.toLowerCase()))

const mentionModule = (mentions: Mention[]) => ({

    ...mentionmodule,
    source: (term, render) => render(mentionList(term, mentions), term)
})


export type QuillModules = {
    color: boolean
    toolbar: boolean
    image: boolean
    alignment: boolean
}

export const useQuillModules = (props: { mentions?: Mention[], modules: QuillModules, ref: React.RefObject<ReactQuill>, imageHandler?: (file: File) => string }) => {

    const { mentions, modules: modulesToLoad, ref: quillRef, imageHandler } = props

    const noToolbar = modulesToLoad.toolbar

    let toolbar = defaultToolbar as any
    let handlers = undefined as any

    // const logged = useLogged()

    const innerImageHandler = async () => {
        const input = document.createElement('input');

        input.setAttribute('type', 'file');
        input.setAttribute('accept', 'image/*');
        input.click();

        input.onchange = async () => {
            var file: any = input.files?.[0];
            var formData = new FormData();

            formData.append('image', file);

            uploadFile(file, quillRef);
        };
    }

    const uploadFile = (file: File, ref: React.RefObject<ReactQuill>) => {
        if (imageHandler) {
            const url = imageHandler(file)
            if (url) {
                const quill = ref.current

                if (!quill) {
                    console.warn("No quill ref found")
                    return
                }

                const range = quill.getEditorSelection();

                const delta = quill.getEditor().insertEmbed(range!.index, 'image', url, 'user')
                quill.getEditor().updateContents(delta)
            }
        }
    }

    if (modulesToLoad.image) toolbar = [toolbar[0], [...toolbar[1].map(m => m), 'image']]
    if (modulesToLoad.color) toolbar = [toolbar[0], [...toolbar[1].map(m => m), { 'color': [] }]]
    if (modulesToLoad.alignment) toolbar = [toolbar[0], [...toolbar[1].map(m => m), { 'align': [] }]]
    if (imageHandler) handlers = handlers ? { ...handlers, image: innerImageHandler } : { image: innerImageHandler }

    // const attributesToRemove = {
    //     'color' : (_) => undefined,  // re-enabling color as we offer it in palette (!)
    //     'background': (_) => undefined,
    //     'image' : (image) => image ? (image as string).toLowerCase().includes('base64') ? undefined : image : undefined
    // }

    // const clipboardMatcher = (_: Node, delta) => {
    //     // console.log({node: _, delta})
    //     delta.ops = delta.ops.map(d => {

    //         let attributes = d.attributes ?? {}
    //         let insert = d.insert ?? {}

    //         attributes = Object.keys(attributes).reduce((acc, cur) => Object.keys(attributesToRemove).includes(cur) ? { ...acc, [cur]: attributesToRemove[cur](attributes[cur]) } : { ...acc, [cur]: attributes[cur] }, {})
    //         if (typeof d.insert === 'string') insert = d.insert
    //         else if (typeof d.insert === 'object') {

    //             insert = Object.keys(insert).reduce((acc, cur) => Object.keys(attributesToRemove).includes(cur) ? { ...acc, [cur]: attributesToRemove[cur](insert[cur]) } : { ...acc, [cur]: insert[cur] }, {})
    //         } else {
    //             insert = d.insert
    //         }

    //         return { ...d, attributes, insert }
    //     })
    //     return delta
    // }

    const modules = useRef({

        toolbar: noToolbar ? false : handlers ? { container: [...toolbar], handlers } : { container: [...toolbar] },
        clipboard: {
            matchVisual: false,
            // matchers: logged.hasNoTenant() ? [] : [
            //     [Node.ELEMENT_NODE, clipboardMatcher]
            // ]
            matchers: [] // All clipboard matchers are disabled because we moved the cleanup of the pasted code on the onChange of the Input boxes layout widget. We leave all the other RTE with default behavior.
        },
        ...mentions ? { mention: mentionModule(mentions) } : {}

    })

    return { modules: modules.current }

}


