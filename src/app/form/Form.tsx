import { Form as AntForm } from 'antd';
import { FormProps } from 'antd/lib/form';
import * as React from 'react';
import { TocEntry, TocLayout } from "../scaffold/TocLayout";
import { givenChildren } from '../utils/children';
import { FormState } from './hooks';


type ContextProps =  {
    sidebar?:boolean
    light?:boolean
}

type Props = FormProps & ContextProps & {
    state?: FormState<any>,
    children: any
    offsetTop?: number 
    className?: string
}

export const FormContext = React.createContext<FormState<any> & ContextProps>(undefined!)

export const Form = (props: Props) => {

    const {state, children, offsetTop=10, className='', sidebar, light, style, ...rest} =props

    const {entries, other=[]} = givenChildren(children).byTypes([["entries",TocEntry]]);

    const ctx =  {...state, sidebar, light} as FormState<any> & ContextProps
    const lightClassName = `ant-form ant-form-horizontal ${className}`

    return <FormContext.Provider value={ctx}>

                <TocLayout className={`page-form ${className}`} offsetTop={offsetTop}>
                    {entries}
                    {light ?  <div style={style} className={lightClassName}>{other}</div> : <AntForm style={style} className={className} {...rest}>{other}</AntForm>}
                </TocLayout>

            </FormContext.Provider>
}
