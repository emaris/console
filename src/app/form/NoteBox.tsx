import { TextBox, TextBoxProps } from '#app/form/TextBox'
import { useLogged } from '#app/user/store'
import * as React from 'react'



export type NoteProps = Omit<TextBoxProps, 'children'> & {
    children : Record<string, string | undefined > | undefined
}

export const NoteBox = (props: NoteProps) => {

    const {children, ...rest} = props

    const logged = useLogged()

    const val = children ? children[logged.tenant] : undefined

    return <TextBox {...rest}>{val}</TextBox>
}