import { useT } from '#app/intl/api';
import { Checkbox as AntCheckbox } from 'antd';
import { CheckboxGroupProps, CheckboxOptionType } from 'antd/lib/checkbox';
import { utils } from 'apprise-frontend-core/utils/common';
import * as React from 'react';
import { Field, FieldProps } from './Field';
import { ReadonlyProps, useReadonly } from './ReadonlyBox';
import { ResetBtn } from './resetbtn';





type GroupProps = FieldProps & CheckboxGroupProps & ReadonlyProps & {

    mode?: 'light' | 'default'

    children: CheckboxOptionType[]

    enabledOnReadOnly?: boolean
}

export function CheckGroupBox(props: GroupProps) {


    const { 

        mode = 'default', 
        
        className, style,
        
        focusDecorations,

        ReadOnlyBox,
        disabled,

        children, 
        value,
        onChange,
        defaultValue,


        ...rest } = useReadonly(props)

    const { readonly, readonlyIcon, readonlyUnlocked, readonlyMode } = rest

    const changeIfNotReadonly = readonly ? () => { } : onChange

    const emptyValue = !value
    const emptyDefault = !defaultValue

    const defaulting = !emptyDefault && emptyValue
    const resettable = !emptyDefault && !emptyValue && !utils().deepequals(value,defaultValue) && !readonly

    // console.log({value, defaultValue, defaulting, resettable, emptyDefault, emptyValue})

    const decorations = [...focusDecorations ?? []]

    // add readonly icon only if can be edited or has been unlocked. 
    if (readonlyMode==='chrome' && (readonly || readonlyUnlocked))
        decorations.push(readonlyIcon)

    if (resettable)
        decorations.push(<ResetBtn enabledOnReadonly={true} onClick={() => onChange?.(undefined!)} />)

    const valueprops = defaulting ? { defaultValue } : { value }

    const content = <AntCheckbox.Group key={defaulting ? JSON.stringify(defaultValue) : undefined} style={style}
    
        {...valueprops} 
        options={children} 
        onChange={changeIfNotReadonly}

        disabled={disabled} />

    
    const classes = `checkbox ${className ?? ''} ${readonly ? 'readonlybox' : ''} ${readonlyMode === 'content' ? 'readonlybox-content' : ''} ${defaulting ? 'checkbox-default-value' : ''}`

    return mode === 'light' ?

        <div className={classes} style={style} >
            {content}
        </div>

        :

        <Field disabled={disabled} className={classes} focusDecorations={decorations}  {...rest} >{
                   
            // if we're not showing chrome, we show a fallback for no content.
            readonly && readonlyMode === 'content' && emptyValue && emptyDefault  ?

                <div className='readonly-placeholder'>-</div>

                :

                content

        }</Field>

}

type SingleProps = FieldProps & {


    indeterminate?: boolean
    title: React.ReactNode
    children: boolean | undefined

    light?: boolean

    onChange: (_: boolean | undefined) => void

}


export function CheckBox(props: SingleProps) {

    const { title, indeterminate, light, onChange, children: value, validation = {}, ...rest } = props

    const t = useT()

    const standardChange = () => onChange(!value)
    const indeterminateChange = () => onChange(value === undefined ? true : (value === true ? false : undefined))
    const change = indeterminate ? indeterminateChange : standardChange

    const vldtn = { msg: t("common.labels.selectclick"), ...validation }

    const field = <AntCheckbox checked={value} indeterminate={indeterminate && value === undefined} onClick={change}>
        {title}
    </AntCheckbox>

    const classes = `checkbox ${props.className ?? ''}`

    return light ? <div className={classes} style={props.style}>{field}</div> : <Field className={classes} style={props.style} validation={vldtn} {...rest}>{field}</Field>
}