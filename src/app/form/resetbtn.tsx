import { Button } from '#app/components/Button'
import { Icon } from 'antd'
import { ButtonProps } from 'antd/lib/button'
import { BiUndo } from 'react-icons/bi'



export const ResetBtn = (props: ButtonProps & {

    enabledOnReadonly: boolean | undefined

}) => {

    const {enabledOnReadonly, ...rest} = props

    return <Button type='link' enabledOnReadOnly={enabledOnReadonly} className='suffix-reset-default-btn' {...rest}>
        <Icon component={BiUndo} />
    </Button>

}
