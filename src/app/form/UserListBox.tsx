import { Label } from '#app/components/Label'
import { userIcon } from '#app/user/constants'
import { UserLabel } from '#app/user/Label'
import { nameOf } from '#app/user/model'
import { useUserStore } from '#app/user/store'
import * as React from 'react'
import { useT } from '#app/intl/api'
import { VSelectBox, VSelectBoxProps } from './VSelectBox'

export type UserListBoxProps = Partial<VSelectBoxProps> & {
    tenant?: string
    children: any
    onChange: (values:any) => void
}

export const UserListBox = (props: UserListBoxProps) => {

    const {tenant, children, ...rest} = props

    const t = useT()
   
    const userstore = useUserStore()

    const entries = tenant ? userstore.all().filter(u=>u.tenant===tenant).map(u=>u.username) : []

    return <VSelectBox 
                {...rest} 
                options={entries} 
                lblTxt={u=>nameOf(userstore.safeLookup(u))}
                undefinedOption={<Label noLink title={t("common.labels.no_focal_point")} icon={userIcon} />}
                renderOption={u=><UserLabel user={userstore.lookup(u)} noLink />}>
                    {[children]}
            </VSelectBox>
}