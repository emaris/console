import { Button } from '#app/components/Button';
import { icns } from '#app/icons';
import { useT } from '#app/intl/api';
import { ReadOnlyContext } from '#app/scaffold/ReadOnly';
import { Icon, Tooltip } from 'antd';
import * as React from 'react';
import { AiOutlineLock, AiOutlineUnlock } from 'react-icons/ai';
import { Field, FieldProps } from './Field';
import { SuffixHelper } from './SuffixHelper';

export type ReadonlyProps = Partial<{


    disabled: boolean

    light: boolean
    readonlyMode: ReadonlyMode
    enabledOnReadOnly: boolean
    readonly: boolean

    canUnlock: boolean

}>

export type ReadonlyMode = 'chrome' | 'content'

export type PublicProps = FieldProps & React.PropsWithChildren<ReadonlyProps> & {

    suffixStyle?: React.CSSProperties

}

export type Props = PublicProps & {

    locked: boolean
    toggleLock: (v: boolean) => any
}

export const useReadonly = <P extends ReadonlyProps>(props: P) => {

    const t = useT()

    const { readonly, enabledOnReadOnly, disabled, readonlyMode = 'chrome', canUnlock } = props

    const readOnlyContext = React.useContext(ReadOnlyContext)

    // context directives, without an oveerride for the client.
    const contextIsReadonly = readOnlyContext && !enabledOnReadOnly

    // readonly if explicitly or contextually so, but if it's disabled then no point of adding readonly semantics.
    const isReadonly = (readonly || contextIsReadonly) && !disabled

    const readonlyModeIfLocked = canUnlock ? 'chrome' : readonlyMode

    // tracks locked state when readonly.
    const [locked, lockedSet] = React.useState(isReadonly)

    // on readonly, resets locked to match.
    React.useEffect(() => {

        if (isReadonly)
            lockedSet(true)

    }, [isReadonly])


    // tracks readonly and locked with refererences that can be accessed from proxy closure.
    const readonlyRef = React.useRef<boolean>(undefined!)
    readonlyRef.current = isReadonly

    // tracks locked as a ref that can be accessed from proxy closure
    const lockedRef = React.useRef<boolean>(undefined!)
    lockedRef.current = locked

    // proxies that uses managed state
    const Proxy = React.useCallback((props: PublicProps) => <ReadOnlyBox {...props} readonly={readonlyRef.current} locked={lockedRef.current} toggleLock={lockedSet} />, [])

    // clients see readonly overlayed with locked. 
    const readonlyAndLocked = isReadonly && locked

    const icon = <Icon style={{color: locked ? 'inherit' : 'orange' }} component={locked ? AiOutlineLock : AiOutlineUnlock} />

   const readonlyIcon =  canUnlock ?

    <Button enabledOnReadOnly tooltipPlacement='right' tooltip={t(locked ? 'common.components.readonlybox.unlock' : 'common.components.readonlybox.lock')}
        type='ghost' noborder onClick={() => lockedSet(s => !s)}>
            {icon}
    </Button>

    :

    <Tooltip placement='right' title={t('common.components.readonlybox.readonly')}>
        {icon}
    </Tooltip>


    return { ...props, ReadOnlyBox: Proxy, enabledOnReadOnly: enabledOnReadOnly || !locked, readonly: readonlyAndLocked, canUnlock, readonlyUnlocked: isReadonly && !locked, readonlyMode: readonlyModeIfLocked, readonlyIcon }

}

const ReadOnlyBox = (props: Props) => {

    const t = useT()

    const { children } = props

    const { id, light, readonly, readonlyUnlocked, className = '', readonlyMode, toggleLock, locked, canUnlock, ReadOnlyBox, suffixStyle, enabledOnReadOnly, ...rest } = useReadonly(props)

    const { style, disabled } = rest

    //console.log("readonly box",readonly, "canUnlock",canUnlock, "locked",locked)

    const classes = `${className} ${readonly ? locked ? 'readonlybox' : 'readonlybox-unlocked' : ''} ${readonly && readonlyMode === 'content' ? 'readonlybox-content' : ''}`

    const icon = locked ? icns.lock() : icns.unlock({ color: 'orange' })

    const suffix = canUnlock ?

        <Button enabledOnReadOnly tooltipDelay={1.2} tooltipPlacement='left' tooltip={t(locked ? 'common.components.readonlybox.unlock' : 'common.components.readonlybox.lock')}
            type='ghost' noborder onClick={() => toggleLock?.(!locked)}>{icon}</Button>

        :

        <Tooltip placement='left' title={t('common.components.readonlybox.readonly')}>{icon}</Tooltip>

    const content = <SuffixHelper style={suffixStyle} suffix={suffix} show={!!readonly && readonlyMode !== 'content' && !disabled}>
        {children}
    </SuffixHelper>

    return light ? <div style={style} className={classes}>{content}</div> : <Field className={classes} {...rest}>{content}</Field>

}