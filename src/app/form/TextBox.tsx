import { useT } from '#app/intl/api';
import { Input } from "antd";
import { AutoSizeType } from "antd/lib/input/ResizableTextArea";
import TextArea from 'antd/lib/input/TextArea';
import { debounce } from "lodash";
import * as React from "react";
import { FieldProps } from "./Field";
import { ReadonlyProps, useReadonly } from './ReadonlyBox';
import { ResetSuffixHelper } from './SuffixHelper';


export type TextBoxProps = FieldProps & ReadonlyProps & {


    placeholder?: true | string
    onChange: (value: any) => void
    children: string | undefined
    delay?:number
    autoSize?: boolean | AutoSizeType | undefined
    defaultValue?: string

}


export const TextBox = (props: TextBoxProps) => {

    const t = useT()

    // these props we use here and don't pass to <Field>
    const {

        className,
        children,
        autoSize,
        placeholder,
        onChange,
        delay=200,
        defaultValue,

        ReadOnlyBox,
        readonlyUnlocked,
       

        ...rest } = useReadonly(props)

    const { readonly, disabled } = rest

    const ref = React.useRef<Input | TextArea>(null)

    const latestValue = children ?? defaultValue


    React.useEffect(() => {

        // resyncs control with an external value change (but doesn't reset the default).
        if (ref.current && latestValue !== ref.current.state.value) 
            ref.current.setValue(latestValue!)
        

    })

    // eslint-disable-next-line
    const onChangeDebounced = React.useCallback(debounce(onChange, delay), [onChange])

    const controlprops = {

        placeholder: placeholder === true ? t('common.buttons.type_placeholder') : placeholder,

        defaultValue,

        onChange: (e: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>) => {

            let value = e.target.value

            // we revert empty string to undefined if that was the initial value, so we avoid false positives in change tracking. 
            // but we leave it empty if we have a default value, so don't end up resetting it if we wanted instead to empty the field.
            const initialNotEmpty = initial.current && initial.current.trim().length > 0
            const currentNotEmpty = value.trim().length > 0
            const normalised = currentNotEmpty || initialNotEmpty || defaultValue ? value : initial.current

            return onChangeDebounced(normalised)

        }

    }

    const initial = React.useRef(children)

    const lockvisible = readonly || readonlyUnlocked
    const resettable = !readonly && !!defaultValue && latestValue !== defaultValue

    const classes = defaultValue && !children ? 'input-default-value' : ''

    return <ReadOnlyBox className={`textbox ${className ?? ''}`} {...rest} >
        <ResetSuffixHelper style={lockvisible ? { right: 30 } : undefined} show={resettable}
            onReset={() => onChange(undefined!)}>
            {

                autoSize ?

                    <Input.TextArea className={classes} ref={ref as any} autoSize={autoSize} readOnly={readonly} disabled={disabled} {...controlprops} />

                    :

                    <Input className={classes} ref={ref as any} readOnly={readonly} disabled={disabled} {...controlprops} />

            }
        </ResetSuffixHelper>
    </ReadOnlyBox>
}