import Radio, { RadioChangeEvent, RadioGroupProps } from "antd/lib/radio"
import { ReadOnlyContext } from "#app/scaffold/ReadOnly"
import * as React from "react"
import { FieldProps } from "./Field"
import { ReadonlyProps, useReadonly } from './ReadonlyBox'



type Props = FieldProps & ReadonlyProps & {

    children: JSX.Element[]

    standalone?: boolean

    orientation?: 'horizontal' | 'vertical'

    enabledOnReadOnly?: boolean
    onChange: (_: any) => void

} & Omit<RadioGroupProps, 'children' | 'onChange'>


const verticalStyle = {
    display: 'block',
    height: '30px',
    lineHeight: '30px',
};

const onChangeNoOp = () => { }

export const RadioBox = (props: Props) => {

    const { id, defaultValue, standalone, readonly, ReadOnlyBox, readonlyMode, enabledOnReadOnly, disabled, children, orientation='vertical', onChange, buttonStyle, className = '', ...rest } = useReadonly(props)

    const btns = React.Children.map(children, c => React.cloneElement(c, {style: {...c.props.style, ...(orientation === 'vertical' ? verticalStyle : {})}}))

    // not easy to implement readonly directly, so we make it onChange no-op.
    const change = (e: RadioChangeEvent) => readonly ? onChangeNoOp : onChange(e.target.value)


    const classes = `radiobox ${className ?? ''} ${readonly ? 'radiobox-readonly' : ''}`

    const readOnlyContext = React.useContext(ReadOnlyContext)
    const innerReadonly = readonly || (readOnlyContext && !enabledOnReadOnly)

    const innerDisabled = disabled || innerReadonly

    const contents = <Radio.Group defaultValue={defaultValue} buttonStyle={buttonStyle} disabled={innerDisabled} {...rest} onChange={change} >
        {btns}
    </Radio.Group>

    return standalone ?

        <ReadOnlyBox light className={classes} style={rest.style}>
            {contents}
        </ReadOnlyBox>

        :

        <ReadOnlyBox readonly={readonly} className={classes} {...rest}>
            {contents}
        </ReadOnlyBox> 

}