import { Empty } from 'antd';
import { isArray } from 'lodash';
import * as React from 'react';
import Select, { components, MenuListComponentProps } from "react-select";
import VirtualList from 'react-tiny-virtual-list';
import { icns } from '../icons';
import { FieldProps } from './Field';
import { ReadonlyProps, useReadonly } from './ReadonlyBox';


export type VSelectBoxProps = FieldProps & ReadonlyProps & {

    mode?: "multiple" | "default" | undefined
    children: any
    placeholder?: string
    onChange: (values: any) => void
    options: any[]
    renderOption?: (o: any) => any
    searchOption?: (o: any) => React.ReactNode
    optionId?: (o: any) => any
    undefinedOption?: React.ReactNode
    hideSelectedOptions?: boolean


    menuPlacement?: 'auto' | 'top' | 'bottom'

    noVirtualisation?: boolean

    lblTxt?: (x: any) => string

    light?: boolean
    size?: "small" | "default"

    clearable?: boolean

    closeMenuOnSelect?: boolean
    enabledOnReadOnly?: boolean
}

const A = components.DropdownIndicator as any

const DropdownIndicator = props => <A {...props}>
    {icns.down}
</A>

const ClearIndicator = props => <A className="vselect__clear-indicator" {...props}>
    {icns.removeEntry}
</A>

export const VSelectBox = (props: VSelectBoxProps) => {

    const { 
        light, 
        size, 
        mode, 
        children, 
        hideSelectedOptions = true, undefinedOption, 
        clearable, options, optionId, onChange, renderOption, 
        searchOption = renderOption, className, closeMenuOnSelect, 
        placeholder, disabled = false, noVirtualisation, enabledOnReadOnly, lblTxt,
        menuPlacement,
        
        ReadOnlyBox,
        readonly,

        ...rest } = useReadonly(props)



    const isMulti = mode === 'multiple'

    const classNames = `vselectbox ${isMulti ? "vselectbox-multi" : "vselectbox-single"} ${size === 'small' ? 'small' : ''}`

    const opts = undefinedOption ? [undefinedOption, ...options] : options
    const render = undefinedOption && renderOption ? c => c === undefinedOption ? undefinedOption : renderOption(c) : renderOption
    const search = undefinedOption && searchOption ? c => c === undefinedOption ? undefinedOption : searchOption(c) : searchOption
    const id = undefinedOption && optionId ? c => c === undefinedOption ? undefined : optionId(c) : optionId
    const change = undefinedOption ? (v: any) => onChange(v === undefinedOption || v?.[0] === undefinedOption ? undefined : v) : onChange

    const isClearable = clearable === undefined ? isMulti : clearable

    const isSearchable = lblTxt ? true : false

    const filterOption = isSearchable ? (candidate, input) => input ? lblTxt!(candidate.data)?.toLocaleLowerCase().includes(input.toLocaleLowerCase()) : true
        : () => true


    const virtualisationProps = noVirtualisation ? {
        components: { DropdownIndicator, ClearIndicator}

    } : {

        components: { DropdownIndicator, ClearIndicator, MenuList },
        captureMenuScroll:false
        
    }


    const select = <Select className="vselect" classNamePrefix="vselect" isMulti={isMulti} 

        value={children?.[0] === undefined ? [undefinedOption] : children}
        options={opts}
        onChange={change}

        formatOptionLabel={render}
        isSearchable={isSearchable && !readonly}
        isClearable={isClearable && !readonly}
        filterOption={filterOption}

        placeholder={placeholder || ""}
        maxMenuHeight={200}
       
        {...virtualisationProps}

        menuPlacement={menuPlacement}
v
        closeMenuOnSelect={props.closeMenuOnSelect || !isMulti}
        hideSelectedOptions={hideSelectedOptions}
        // menuPortalTarget= {document.body}                               // avoids dependency on parent height

        getOptionLabel={search || (o => o)}
        getOptionValue={id || search || (o => o)}
        isDisabled={disabled}
        styles={{ menuPortal: base => ({ ...base, zIndex: 1000 }) }}    // makes sure options are visibile
        {...rest}

    />

    return light ?

        <div className={className ? `${className} ${classNames}` : `${classNames}`} style={props.style}>
            {select}
        </div>

        :

        <ReadOnlyBox disabled={disabled} className={className ? `${className} ${classNames}` : `${classNames}`}  {...rest} >
                {select}
        </ReadOnlyBox>

}





const defaultItemHeight = 35
const placeHolderheight = 120

const MenuList = (props: MenuListComponentProps<any, any>) => {

    const { options, children, maxHeight, getValue } = props

    const matches = isArray(children)

    const [value] = getValue()
    const initialOffset = options.indexOf(value) * defaultItemHeight
    const childrenOptions = React.Children.toArray(children)
    const wrapperHeight = matches ? Math.min(maxHeight, childrenOptions.length * defaultItemHeight) : placeHolderheight

    const renderItem = ({ index, style }) => matches ?

        <div key={index} style={style}>{children[index]}</div>
        :

        <Empty key={index} style={{ height: placeHolderheight, padding:25, margin:0 }} image={Empty.PRESENTED_IMAGE_SIMPLE} />

    return (
        <span>
            <VirtualList width="100%"
                height={wrapperHeight}
                scrollOffset={initialOffset}
                itemCount={childrenOptions.length}
                itemSize={defaultItemHeight}
                renderItem={renderItem}
            />
        </span>
    );
}
