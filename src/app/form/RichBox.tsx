
import { useT } from '#app/intl/api';
import { debounce } from "lodash";
import * as React from 'react';
import ReactQuill from 'react-quill';
import 'react-quill/dist/quill.snow.css';
import { FieldProps } from './Field';
import { ReadonlyProps, useReadonly } from './ReadonlyBox';
import { Mention, useQuillModules } from "./rte";
import { ResetSuffixHelper } from './SuffixHelper';



export type RichBoxProps = FieldProps & ReadonlyProps & {

    placeholder?: true | string | undefined

    children: string | undefined
    defaultValue?: string | undefined

    onChange: (value: any) => void

    delay?: number

    height?: 'min' | 'mid' | 'max'

    noToolbar?: boolean
    noImage?: boolean

    withNoTextColor?: boolean
    withNoAlignment?: boolean

    mentions?: Mention[]

    imageHandler?: (file:File) => string
}

export const RichBox = (props: RichBoxProps) => {

    const t = useT()

    // these props we use here and don't pass to <Field>
    const {

        className,
        fieldClassName = '',
        fieldStyle,
        height = 'min',
        placeholder,

        children,
        defaultValue,
        onChange,
        delay=200,

        noToolbar,
        mentions,

        ReadOnlyBox,
        readonlyUnlocked,
       

        noImage = false,
        withNoTextColor = false,
        withNoAlignment = false,
        imageHandler,

        ...rest } = useReadonly(props)


    // these props we use AND pass to <Field>
    const { readonly, readonlyMode, disabled } = rest


    const trackedOnChange = (v: string | undefined) => {
        lastChange.current = v
        onChange(v)
    }

    // if client memoises on change, we exploit it.
    // eslint-disable-next-line
    const onChangeDebounced = React.useCallback(debounce(trackedOnChange, delay), [onChange])

    const initial = React.useRef(children)

    // normalises html for no-content to undefined.
    const clean = (value: string | undefined) => value ? value === "<p><br></p>" ? '' : value : undefined

    const ref = React.useRef<ReactQuill>(null)

    const latestValue = clean(children ?? defaultValue)

    const readonlyFallback = readonly && readonlyMode === 'content' ? '-' : undefined

    const lastChange = React.useRef<string|undefined>(latestValue)

    //eslint-disable-next-line
    React.useEffect(() => {
        const editor = ref.current?.getEditor()
        if (latestValue !== lastChange.current && editor) {
            (ref.current as any).setEditorContents(editor, latestValue, 0)
            lastChange.current = latestValue
        }
    })

    // React.useEffect(() => {

    //     const editor = ref.current?.getEditor()

    //     if (editor) {
            
    //         const value = clean((ref.current as any)?.makeUnprivilegedEditor(editor).getHTML()) || undefined
            
    //         if (ref.current && latestValue !== value) { 
    //             (ref.current as any).setEditorContents(editor, latestValue, 0)
    //         }

    //     }
    // })


    const placeholderOrFallback = readonly ? undefined : (placeholder === true ? t('common.buttons.type_placeholder') : placeholder)
    
    const valueprops = {

        defaultValue: latestValue ?? readonlyFallback ?? '',   // empty string is a workaround for an unreleased fix: see https://github.com/zenoamaro/react-quill/issues/498

        onChange: (content, _, source) => {

            // we react only to user changes, not api change. this avoids rerender on mount, when the first value set triggers a programmatic change.
            if (source !== 'user')
                return


            const trimmedValue = clean(content)
            
            // we revert empty string to undefined if that was the initial value, so we avoid false positives in change tracking. 
            // but we leave it empty if we have a default value, so don't end up resetting it if we wanted instead to empty the field.
            const initialNotEmpty = initial.current && initial.current.trim().length > 0
            const currentNotEmpty = (trimmedValue?.length ?? 0) > 0
            const normalised = currentNotEmpty || initialNotEmpty || defaultValue ? trimmedValue : initial.current



            onChangeDebounced(normalised)
        }

    }

    //const { modules } = useQuillModules({ mentions, noToolbar, noImage })
    
    const { modules } = useQuillModules({ mentions, modules: {toolbar: !!noToolbar, color: !withNoTextColor, alignment: !withNoAlignment, image: !noImage}, ref, imageHandler })

    const lockvisible = readonly || readonlyUnlocked
    const resettable = !readonly && !!defaultValue && latestValue !== defaultValue

    const heightClass = height === 'min' ? "" : height === 'mid' ? 'richbox-height-mid' : 'richbox-height-max'

    const classes = `${className ?? ''} richbox ${heightClass} ${disabled ? 'richbox-disabled' : ''} ${noToolbar ? 'richbox-notoolbar' : ''} ${defaultValue && !children ? 'input-default-value' : ''} ${!latestValue &&readonlyFallback ? 'readonly-fallback' : '' }`

    const top = noToolbar ? 6 : 50

    return <ReadOnlyBox key={placeholderOrFallback ? `${placeholderOrFallback}` : undefined} suffixStyle={!readonly ? { top } : {}} className={classes} {...rest} >
        <ResetSuffixHelper style={lockvisible ? { right: 30, top } : { top }} show={resettable}
            onReset={() => onChange(undefined!)}>
            <ReactQuill ref={ref}

                className={fieldClassName}
                style={fieldStyle as any}  // react-quill gives it an ad-hoc type.
                readOnly={readonly}
                placeholder={placeholderOrFallback}
                modules={modules}
                preserveWhitespace
         

                {...valueprops}
            />
        </ResetSuffixHelper >
    </ReadOnlyBox >

}