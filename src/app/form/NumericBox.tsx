import { InputNumber } from "antd";
import { debounce } from "lodash";
import { Field, FieldProps } from "./Field";
import { ReadonlyProps, useReadonly } from "./ReadonlyBox";
import { ResetBtn } from './resetbtn';


export type NumberBoxProps = FieldProps & ReadonlyProps & {

    placeholder?: string

    onChange: (value: any) => void

    children: number | undefined
    defaultValue?: number

    min?: number
    max?: number

    step?: number
}


export const NumericBox = (props: NumberBoxProps) => {

    const { 
        
        className, fieldClassName, style, fieldStyle,
        
        children, 
        onChange, 
        defaultValue,
        placeholder, 
        
        readonly, 
        readonlyMode,
        enabledOnReadOnly,
        readonlyUnlocked,
        readonlyIcon,

        focusDecorations, 
        
        min, 
        max, 
        step, 
        
        ...rest } = useReadonly(props)

    const debounceOnChange = debounce(onChange, 200)

    const defaulting = defaultValue!==undefined && children === undefined
    const resettable = defaultValue!==undefined && children !== undefined && defaultValue!==children && !readonly
    
   // console.log({defaultValue, children, typeofdefault: typeof defaultValue, typeofchildren: typeof children, resettable})

    const decorations = [...focusDecorations ?? []]

    // add readonly icon only if can be edited or has been unlocked. 
    if (readonlyMode==='chrome' && (readonly || readonlyUnlocked))
        decorations.push(readonlyIcon)

    if (resettable)
        decorations.push(<ResetBtn enabledOnReadonly={enabledOnReadOnly} onClick={() => onChange(undefined)} />)

    const valueProps = defaulting ? { defaultValue } : { value: children }

    const readonlyFallback = readonly && readonlyMode === 'content' ? '-' : undefined

    const readonlyData = children ?? defaultValue

    const classes = `${className ?? ''} numberbox ${defaulting ? 'numberbox-default-value' : ''}`

    return readonly && readonlyMode === 'content' ?

        <Field label={props.label} >
            <div className={`${classes}  ${readonlyData!==undefined ? '' : 'readonly-placeholder'}`}>{readonlyData ?? readonlyFallback}</div>
        </Field>

        :

        <Field style={fieldStyle} className={fieldClassName} focusDecorations={decorations} {...rest}  >

            <InputNumber key={defaulting ? JSON.stringify(defaultValue) : undefined} type="number"
                className={classes}
                style={style}
                placeholder={placeholder}
                readOnly={readonly}
                step={step} min={min} max={max}
                onChange={(value: any) => debounceOnChange(value)}
                {...valueProps} />

        </Field>
}