
import { useFeedback } from '#app/utils/feedback'
import produce from "immer"
import * as React from "react"
import { cleanWith, deepclone, deepequals } from "../utils/common"




export type FormState<T=any> = {

    edited:T
    initial:T
  
    dirty:boolean
  
    resetted:boolean
  
    // changes part of the state
    change: (f: (t: T, ...[any]) => any, noClean?:boolean) => (...[any]) => void
  
    // changes the whole state
    set: (t: T) => void

    // changes the whole and the initial state
    softReset: (t:T) => void
    
    // changes the whole state, the initial state, and triggers a reset cycle 
    reset: (t?:T,confirm?:boolean | (() => void)) => void
    
  
    auxstate: Object
    auxchange: (f: (t: T, ...[any]) => any) => (...[any]) => void
  
  }
  
  
  export const useFormState = <T> ( start:T, initialstart:T=start) : FormState<T>  => {


    const fb = useFeedback()
  
    
    //  keeps an editable copy of the initial value.
    const [state, setState ] = React.useState({edited:deepclone(start),initial:initialstart})
  
    //  keeps a bag for arbitrary additional state required by children.
    //  this solves problems with state local to children:
    //  1) aux state is synced along the main state on reset (see doreset())
    //  2) aux state has the same lifecycle as the form, persists even if children are not rendered (e.g. unselected tabs)
    const [auxstate, setAuxstate ] = React.useState<Object>({})
  
    //  keeps track of a reset and serves as a marker for the subsequent re-render.
    //  in that render, components are informed and can take action to sync pieces of state other than edited.
    //  effectively, it acts like a s'witch'
    const [resetted,doReset] = React.useState(false)
  
    const {edited, initial} = state
  
  
    // eslint-disable-next-line
    React.useEffect(()=> {
      if (resetted) // immediately reverts the reset status, flicking the 'switch'.
       doReset(false)
    })
  
    const dirty = !deepequals(edited,initial )  //  aux state doesn't partecipate here.
  
     // updates edited, leaves initial untouched
    const set = (edited:T )=> setState( s=> ({...s,edited}) )
  
    // reset edited to a fresh copy of initial, 
    // optionally set both to a new value (eg. on add and save with remote data), 
    // and flicks the reset switch
    const doreset = (edited?:T) => { edited ? setState( {edited:deepclone(edited),initial:edited}) : set(deepclone(initial)) ; doReset(true); setAuxstate({}) }
    
    const reset = (edited?:T, confirm: (boolean | (()=>void)) = true )=> {
  
            if (confirm)
              fb.askConsent( {
                ...fb.unsavedConsentDialog(),
                onOk: ()=>Promise.resolve(doreset(edited)).then(()=> { if (typeof confirm === 'function') confirm() })
              })
            else
              doreset(edited)
  
    }
  
    //  change part of main state (and merge changes)
    const change = (f:(t:T, ...[args])=>any, noClean) => (...[args]) => {
      
      const produced = produce(edited,t =>void(f(t as any,args)))
      return set( noClean ? produced : cleanWith(produced,initial)  )
    
  }
  
    // change the entire state (like a reset, but without the extra renders)
    const softReset = (edited:T )=> { setState( {edited:deepclone(edited),initial:edited}); setAuxstate({})}  
      
    //  change part of aux state (and merge changes)
    const auxchange = (f:(t:T, ...[args])=>any) => (...[args]) => setAuxstate( produce(auxstate,t =>void(f(t as any,args)))  )
  
    return { edited,initial ,dirty, change,set, reset, softReset, resetted, auxstate, auxchange }
  
  }