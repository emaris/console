import { Icon } from 'antd'
import { Button } from '#app/components/Button'
import * as React from 'react'
import { useT } from '#app/intl/api'
import { BiUndo } from 'react-icons/bi'

export type SuffixHelperProps = React.PropsWithChildren<{

    style?: React.CSSProperties
    suffix: React.ReactNode
    show: boolean

}>

export const SuffixHelper = (props: SuffixHelperProps) => {

    const { style, suffix, show, children } = props

    return <div className='suffix-container' >

        {show &&

            <div style={style} className="suffix">{suffix}</div>
        }

        {children}

    </div>
}

export type ResetHelperProps = Omit<SuffixHelperProps, 'suffix'> & {


    onReset: () => any

}

export const ResetSuffixHelper = (props: ResetHelperProps) => {

    const { children, onReset } = props

    const t = useT()

    const resetToDefault = <Button enabledOnReadOnly className='reset-default-btn' tooltipDelay={1.2} tooltipPlacement='left' tooltip={t('common.components.input.set_default')} type='ghost' noborder
        onClick={() => onReset()}>
        {<Icon component={BiUndo} />}
    </Button>


    return <SuffixHelper {...props} suffix={resetToDefault}>
        {children}
    </SuffixHelper>

}