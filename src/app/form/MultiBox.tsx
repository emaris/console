import { useConfig } from '#app/config/state';
import { useT } from '#app/intl/api';
import { Col, Dropdown, Input, Menu } from 'antd';
import { AutoSizeType } from 'antd/lib/input/ResizableTextArea';
import * as React from 'react';
import 'react-quill/dist/quill.snow.css';
import { Button } from '../components/Button';
import { icns } from '../icons';
import { allLanguages, defaultLanguage, fullnameOf, Language } from '../intl/model';
import { MultilangDto } from '../model/multilang';
import { cleanWith } from '../utils/common';
import { Field, FieldProps } from './Field';
import { FormContext } from './Form';
import { ReadonlyProps, useReadonly } from './ReadonlyBox';
import { RichBox } from './RichBox';
import { Mention } from './rte';
import "./styles.scss";
import { SuffixHelper } from './SuffixHelper';
import { TextBox } from './TextBox';


export type MultiboxProps = FieldProps & ReadonlyProps & {

    id: string
    placeholder?: true | MultilangDto
    onChange: (t: any) => void
    required?: Language[]
    languages?: Language[]

    children: MultilangDto | undefined
    defaultValue?: MultilangDto | undefined
    delay?: number,

    autoSize?: boolean | AutoSizeType | undefined

    rte?: boolean
    noToolbar?: boolean
    mentions?: Mention[]

    noImage?: boolean

    withNoTextColor?: boolean
    withNoAlignment?: boolean

    editorClassName? : string

    imageHandler?: (file:File) => string


    langProps?: (l: Language, val: string | undefined) => Partial<{

        disabled: boolean
        readonly: boolean

    }>

    readOnlyLang? : Language

}

export const MultiBox = (props: MultiboxProps) => {

    const t = useT()

    const { auxstate = {}, auxchange = ((() => { }) as Function) } = React.useContext(FormContext) ?? {}

    const {

        id,
        placeholder,

        children={},
        defaultValue,
        onChange,
        delay,

        autoSize,

        rte,
        noToolbar,
        mentions,

        readonly,
        enabledOnReadOnly,
        readonlyUnlocked,
        readonlyMode,

        canUnlock, 
        ReadOnlyBox,

        noImage=false,
        withNoTextColor = false,
        withNoAlignment = false,

        langProps,

        readOnlyLang,

        className,
        style,
        editorClassName='',
        imageHandler,


        ...rest } = useReadonly(props)

    const { disabled } = rest

    // for internal dierty tracking
    const initial = React.useRef(props.children)

    // changes whole value
    const replace = (value: MultilangDto) => {

        const cleaned = cleanWith(value, initial.current)
        return onChange(cleaned)

    }

    const { intl } = useConfig().get()

    const { languages, required } = requiredFrom(props, intl.languages, intl.required);

    //  uses aux state to remember the order of choices, and to sync on reset
    const [choices, setChoices]: [Language[], (l: Language[]) => any] = [auxstate[id] || initialChoicesFrom(props, intl.languages, required), auxchange((t, v) => t[id] = v)]

    const other = languages.filter(l => !choices.includes(l))

    const otherLanguageName = other.length === 1 ? t(fullnameOf(other[0])) : undefined

    const classes = `multibox ${className ?? ''} ${other.length > 0 ? 'with-language-choice' : ''} ${readonly ? 'readonlybox' : ''} ${readonly && readonlyMode === 'content' ? 'readonlybox-content' : ''}`

    // excludes 'empty' boxes from rendering in readonly content mode.
    // but show the default if the box is undefined.
    // let filteredChoices = readonly && readonlyMode === 'content' ? choices.filter(l => children[l] === undefined ? defaultValue?.[l] : children[l]!=='') : choices
    let filteredChoices = choices
    if (readonly && readonlyMode === 'content') {
        filteredChoices = filteredChoices.filter(l => children[l] === undefined ? !!defaultValue?.[l] : children[l]!=='')
        if (readOnlyLang)
            filteredChoices = children[readOnlyLang] ?  [readOnlyLang] : filteredChoices[0] ? [filteredChoices[0]] : []

        if (filteredChoices.length!==1)
            filteredChoices = [filteredChoices[0]]
    }

    

    const langclasses = filteredChoices.length > 1 ? 'in-multibox' : ''

    return <Field className={classes} {...rest}>

        <>

            {filteredChoices.map(l => {

                    const props = langProps?.(l, children[l])

                    const langreadonly = props?.readonly ?? readonly
                    const langdisabled = props?.disabled ?? disabled
                    const langplaceholder = placeholder === true ? t('common.buttons.type_lang_placeholder', { lang: t(fullnameOf(l)) }) : placeholder?.[l]

                    // changes value @ one language
                    const change = (v: string) => {

                        const value = { ...children, [l]: v }
                        replace(value)

                    }

                    const showRemove = choices.length > 1 && !required.includes(l)
                    const remove = () => {

                        const changed = { ...children }

                        changed[l] = undefined

                        setChoices(choices.filter(lang => lang !== l))

                        replace(changed)

                    }

                    return <Input.Group key={l} className="multibox-language" compact={true}>

                        { filteredChoices.length <=1 || 

                            <Col className="prefix-container" xs={1}>
                                <span className="prefix">{l}</span>
                            </Col>
                        }

                        <Col style={{ width: "100%" }}>
                            <SuffixHelper key={l} show={showRemove && !disabled && !readonly} suffix={

                                <Button noborder type='ghost' size="small"
                                    icn={icns.removeEntry}
                                    disabled={langdisabled}
                                    onClick={remove} />}>

                                <div draggable={true} onDragStart={e => {
                                                                            e.preventDefault();
                                                                            e.stopPropagation();
                                                                        }}
                                >

                                {rte ?


                                    <RichBox style={style} className={`${langclasses} ${editorClassName}`} placeholder={langplaceholder}
                                        defaultValue={defaultValue?.[l]}
                                        disabled={langdisabled}
                                        canUnlock={canUnlock}
                                        readonly={langreadonly}
                                        readonlyMode={readonlyMode}
                                        enabledOnReadOnly={enabledOnReadOnly}
                                        onChange={change}
                                        mentions={mentions}
                                        noToolbar={noToolbar}
                                        noImage={noImage}
                                        withNoAlignment={withNoAlignment}
                                        withNoTextColor={withNoTextColor}
                                        imageHandler={imageHandler}
                                        delay={delay}
                                        light>{children[l]}</RichBox>

                                    :

                                    <TextBox style={style} className={`${langclasses} ${editorClassName}`} placeholder={langplaceholder}
                                        defaultValue={defaultValue?.[l]}
                                        disabled={langdisabled}
                                        readonly={langreadonly}
                                        readonlyMode={readonlyMode}
                                        enabledOnReadOnly={enabledOnReadOnly}
                                        canUnlock={canUnlock}
                                        autoSize={autoSize}
                                        onChange={change}
                                        delay={delay}

                                        light>{children[l]}</TextBox>

                                }
                                </div>
                            </SuffixHelper>
                        </Col>
                    </Input.Group>
                }

                )}

            {readonly || other.length === 0 ||

                <div className="language-choice">

                    {other.length === 1 ?

                        <Button onClick={_ => setChoices([...choices, other[0]])} >
                            {t("common.buttons.add_one", { singular: otherLanguageName })}
                        </Button>

                        :
                        <Dropdown placement="bottomRight" overlay={
                            <Menu onClick={c => setChoices([...choices, c.key as Language])}>{other.map(l =>
                                <Menu.Item className="language-choice-item" key={l}>
                                    {icns.language} {t(fullnameOf(l))}
                                </Menu.Item>
                            )} </Menu>} >

                            <Button size="small" icn={icns.down}>{t("common.buttons.add_one", { singular: t("common.fields.language.name") })}</Button>

                        </Dropdown>


                    }
                </div>
            }
        </>
    </Field >

}

const requiredFrom = (props: MultiboxProps, configLanguages: Language[] = allLanguages, requiredLanguages: Language[] = []) => {

    // props may override
    const { languages = configLanguages, required = requiredLanguages } = props;

    return { languages, required };

}
const initialChoicesFrom = (props: MultiboxProps, languages: Language[], required: Language[]) => {

    const { children = [] } = props

    const defined = (languages as Language[]).filter(l => required.includes(l) || children[l])

    const choices = defined.length > 0 ? defined : [defaultLanguage]

    return choices;

}

