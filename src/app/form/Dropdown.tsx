import { Dropdown as AntDropDown, Icon, Menu } from 'antd';
import { Button } from 'antd/lib/radio';
import * as React from 'react';
import { Field, FieldProps } from './Field';
import { ReadOnlyContext } from '../scaffold/ReadOnly';
import { ReadonlyProps } from './ReadonlyBox';


export type Item = {  key: any,  label: string }

type Props = FieldProps & ReadonlyProps & {

    placeholder?: string,
    placement?: "bottomRight" | "topLeft" | "topCenter" | "topRight" | "bottomLeft" | "bottomCenter" | undefined
    items: Item[],
    icon?: string,
    onChange: (value:any) => void
    children:any
    enabledOnReadOnly?: boolean

}

export const Dropdown = (props : Props) => {

    const {placement="bottomRight", placeholder="Select one...", children, items, icon="down", onChange, disabled=false, enabledOnReadOnly, 
           ...rest} = props

    const readOnlyContext = React.useContext(ReadOnlyContext)
    const readOnlyState = enabledOnReadOnly || readOnlyContext
    
    const valueIn = (id) => {
        // eslint-disable-next-line
        const match = items.find(item =>item.key==id); 
        return (match && match.label) || undefined
    }
    
    const entries =    <Menu > {items.map( (item) => 
                             <Menu.Item key={item.key} onClick={e=>onChange(e.key)} >{item.label}</Menu.Item>)}
                        </Menu>
    

    return (
        <Field {...rest} disabled={disabled}>
            <AntDropDown placement={placement} overlay={entries} disabled={disabled || readOnlyState}>
                <Button disabled={disabled || readOnlyState}>
                    {valueIn(children) || placeholder} <Icon type={icon} />
                </Button>
            </AntDropDown>
        </Field>
    )

}