
import * as React from "react"
import { Paragraph } from "../components/Typography"
import { FormContext } from "./Form"

export type RowProps = {

    className?: string
    standalone?: boolean                    //  not part of a form
    side?: string | React.ReactNode         //  side content
    children?: any
    fieldStyle?: React.CSSProperties
}

export const FieldRow = ({children,standalone,side,className,fieldStyle}:RowProps) => {

    const ctx = React.useContext(FormContext)

    const useSidebar = ctx?.sidebar && !standalone

    const sidecontent = side  &&  <div className="row-side">
                                    { side && (typeof side === 'string') ?  <Paragraph>{side}</Paragraph> : side }
                                </div> 

    return <div className={`form-row ${className || ''}`} style={fieldStyle}>

            <div className="row-main field-group">
               {children}
            </div>

            {useSidebar && sidecontent}

        </div>
}