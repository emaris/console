import * as React from 'react'
//from http://casesandberg.github.io/react-color/
import { Field, FieldProps } from './Field'
import {
  SwatchesPicker, SketchPicker, PhotoshopPicker, BlockPicker,
  GithubPicker, HuePicker, SliderPicker, TwitterPicker, CompactPicker
} from 'react-color'
import { ReadonlyProps, useReadonly } from './ReadonlyBox'

type PickerType = 'swatches' | 'sketch' | 'photoshop' | 'block' | 'github' | 'hue' | 'slider' | 'twitter' | 'compact'

type Props = FieldProps & ReadonlyProps & {
  onChange: (value: any) => void
  type?: PickerType
  children: string | undefined
}

export const ColorBox = (props: Props) => {

  const { children, className, onChange, type = 'swatches', disabled = false, readonly, ReadOnlyBox, ...rest } = useReadonly(props)

  

  const [pickerVisible, showPicker] = React.useState(false)

  const innerprops = { color: children, onChange: c => onChange(c.hex) }

  const pickerOf = (pickerType: PickerType) => {

    switch (pickerType) {
      case 'sketch': return <SketchPicker {...innerprops} />
      case 'photoshop': return <PhotoshopPicker {...innerprops} />
      case 'block': return <BlockPicker triangle="hide" {...innerprops} />
      case 'github': return <GithubPicker triangle="hide" {...innerprops} />
      case 'hue': return <HuePicker {...innerprops} />
      case 'slider': return <SliderPicker {...innerprops} />
      case 'twitter': return <TwitterPicker triangle="hide" {...innerprops} />
      case 'compact': return <CompactPicker {...innerprops} />
      case 'swatches': return <SwatchesPicker {...innerprops} />
    }
  }

  return <ReadOnlyBox {...rest} className={`colorbox ${className || ''}`}  {...rest}>

    <Field disabled={disabled} >

      <div className="colorbox-value" onClick={() => readonly || showPicker(true)}>
        <div className="colorbox-value-inner" style={{ background: children }} />
      </div>

      {pickerVisible &&

        <div className="colorbox-popover">
          <div className={`colorbox-overlay`} onClick={() => readonly || showPicker(false)} />
          {pickerOf(type)}
        </div>
      }

    </Field>

  </ReadOnlyBox>
}