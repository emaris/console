import { Slider } from "antd"
import { SliderProps } from "antd/lib/slider"
import { Field, FieldProps } from "#app/form/Field"
import { ReadOnlyContext } from "#app/scaffold/ReadOnly"
import { debounce } from "lodash"
import * as React from "react"
import { FormContext } from "./Form"


type Props = FieldProps & SliderProps & {

    width?:number
    debounceDelay?: number,
    enabledOnReadonly? : boolean
    tooltipVisible? : boolean
    showValues? : boolean
    showValuesFormatter?: (_:number) => React.ReactNode
} 

export const SliderBox = (props:Props) => {

    const ctx = React.useContext(FormContext)

    const {className, width, debounceDelay, range, standalone, enabledOnReadonly, value, disabled, onChange, defaultValue, tipFormatter, tooltipVisible, marks, showValues=true, showValuesFormatter=(n:number)=>n,...rest} = props

    const readonly = React.useContext(ReadOnlyContext) && !enabledOnReadonly

    const onChangeDebounced = debounce (v=>onChange?.(v),debounceDelay ?? 200)

    const val = value ?? defaultValue

    const valueprop =  ctx?.resetted ? {value:val ?? 0} : {defaultValue:val}
    
    const [showVals, setShowVals] = React.useState(value ? value : defaultValue ? defaultValue : 0)

    const showVal = (val: number) => {
        if (!marks) return showValuesFormatter(val)
        if (marks[val] === undefined) return showValuesFormatter(val)
        return marks[val]
    }


    const contents =  <div className={`sliderbox-wrapper ${width ? '' : 'sliderbox-flex'}`}>
            <Slider 
                style={{width}} 
                range={range} 
                disabled={disabled || readonly} 
                tipFormatter={tipFormatter} 
                onAfterChange={setShowVals} 
                tooltipVisible={tooltipVisible}
                marks={marks}
                {...valueprop}  
                {...rest} 
                onChange={onChangeDebounced} />
            {showValues && 

            <div className="content-vals">{!range ? <>{showVal(showVals as number)}</> : <>{`[${showVal(showVals[0])} - ${showVal(showVals[1])}]`}</>}</div>

            
            }
            </div>

    

    return standalone ? 
    
            <div className={`sliderbox ${className || ''}`}>{contents}</div> 
        
            : 

    
            <Field {...rest} className={`sliderbox ${className || ''}`}  >
                {contents}
            </Field>

}