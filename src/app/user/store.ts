

import { useT } from '#app/intl/api';
import { useIntl } from '#app/intl/store';
import { useContext } from 'react';
import { useToggleBusy } from "../system/api";
import { deepclone, indexMap, through } from "../utils/common";
import { notify, showAndThrow, useFeedback } from "../utils/feedback";
import { Account, useAccounts } from './account';
import { useUserCalls } from "./calls";
import { userPlural, userRoute, userSingular, userType } from "./constants";
import { UserContext, UserNext } from './context';
import { User, UserPreferences, newUserIn, useNoUser, useUser, useUserModel } from "./model";




export const useLogged = () => {

  const state = useContext(UserContext)

  const withUser = useUser()

  return state.get().logged && withUser(state.get().logged)

}

export const useLogin = () => {

  const state = useContext(UserContext)

  const toggleBusy = useToggleBusy()

  const intl = useIntl()

  const withUser = useUser()

  const self = {

    logged: () => {
      return  state.get().logged &&  withUser(state.get().logged)
    },

    lastLogged: () => {

      const logged = sessionStorage.getItem('emaris.loggeduser')

      return logged ? JSON.parse(logged) as User : undefined

    }

    ,

    setLastLogged: (user: User) => {

      sessionStorage.setItem('emaris.loggeduser', JSON.stringify(user))

    }
    ,

    isLogged: () => state.get().logged !== undefined

    ,

    setLogged: (u: User) =>

      Promise.resolve(state.set(s => s.logged = deepclone(u)))
        .then(_ => intl.setCurrentLanguage(u.details.preferences.language || intl.defaultLanguage()))
        .then(_ => self.setLastLogged(u))

    ,



    login: (user: User) =>

      toggleBusy(`${userType}.login`, `Updating login info...`)  // dev feature: no need to translate           
        .then(_ => self.setLogged(user))

        .finally(() => toggleBusy(`${userType}.login`))

  }

  return self

}


export const useUserStore = () => {


  const state = useContext(UserContext)

  const t = useT()
  const toggleBusy = useToggleBusy()
  const fb = useFeedback()


  const call = useUserCalls()
  const model = useUserModel()

  const logged = useLogged()
  const login = useLogin()
  const accounts = useAccounts()

  const nouser = useNoUser()

  const type = userType

  const [singular, plural] = [t(userSingular), t(userPlural)].map(n => n.toLowerCase())
  
  const self = {

    updateUserProfile: (props: Partial<UserPreferences>) => {

      logged.details.preferences = { ...logged.details.preferences, ...props }

      return toggleBusy(`${type}.save`, t("common.feedback.save_changes"))

        .then(() => self.updateUserProfileQuietly(props))

        .catch(e => showAndThrow(e, t("common.calls.save_one_error", { singular })))
        .finally(() => toggleBusy(`${type}.save`))

        .catch(e => console.warn("could not update preferences"))

    }

    ,

    updateUserProfileQuietly: (props: Partial<UserPreferences>) => {

      logged.details.preferences = { ...logged.details.preferences, ...props }

      return call.updateProfile(logged)
        .then(through(saved => self.setAll(self.all().map(u => u.username === logged.username ? saved : u))))

    }
    ,

    fetchLogged: (): Promise<User> =>

      toggleBusy(`${type}.fetchLogged`, t("common.feedback.load_logged"))

        .then(_ => console.log("fetching logged user..."))
        .then(_ => call.fetchLogged())
        .then(through(login.setLogged))

        .catch(e => showAndThrow(e, t("common.calls.fetch_one_error", { singular })))
        .finally(() => toggleBusy(`${type}.fetchLogged`))




    ,

    routeTo: (t: User) => t ? `${userRoute}/${encodeURIComponent(t.username!)}` : userRoute

    ,

    fetchOne: (username: string): Promise<User> =>

      toggleBusy(`${type}.fetchOne`, t("common.feedback.load_one", { singular }))

        .then(_ => call.fetchOne(username))
        .then(through(fetched => { if (!fetched) throw Error(`Invalid resource lookup for ${username}, returned undefined.`) }))
        .then(through(fetched => self.setAll(self.all().map(u => u.username === username ? fetched : u))))

        .catch(e => showAndThrow(e, t("common.calls.fetch_one_error", { singular })))
        .finally(() => toggleBusy(`${type}.fetchOne`))

    ,

    areReady: () => !!state.get().users.all

    ,

    all: () => state.get().users.all

    ,

    allSorted: () => {

      return [...self.all()].sort(model.comparator)

    }

    ,

    setAll: (users: User[]) => state.set(s => { s.users.all = users; s.users.map = indexMap(users).by(u => u.username) })

    ,

    setAllRefs: (refs: User[]) => state.set(s => { s.users.refs = indexMap(refs).by(u => u.username) })

    ,

    get: (username: string) => state.get().users.all[username]

    ,

    lookup: (username: string | undefined): User | undefined => username ? state.get().users.map[username] ?? state.get().users.refs[username] : undefined


    ,

    safeLookup: (username: string | undefined): User => self.lookup(username) ?? nouser.get()


    ,

    fetchAll: (forceRefresh = false): Promise<User[]> =>

      self.areReady() && !forceRefresh ? Promise.resolve(self.all())

        :

        toggleBusy(`${type}.fetchAll`, t("common.feedback.load", { plural }))

          .then(_ => console.log(`fetching users...`))
          .then(_ => Promise.all([call.fetchAll(), call.fetchAllReferences()]))
          .then(through(([users, refs]) => {

            self.setAll(users)
            self.setAllRefs(refs)

          }))
          .then(([users]) => users)
          .catch(e => showAndThrow(e, t("common.calls.fetch_all_error", { plural })))
          .finally(() => toggleBusy(`${type}.fetchAll`))

    ,

    next: () => {

      const next = state.get().users.next || { model: newUserIn(state.get().logged.tenant), linkBack: undefined }
      return next;

    }

    ,

    resetNext: () => {

      self.setNext(undefined)
    }

    ,

    setNext: (t: UserNext | User | undefined): UserNext | undefined => {
      const model = t ? t.hasOwnProperty('model') ? t as UserNext : { model: t as User } : undefined
      state.set(s => s.users.next = model)
      return model

    }

    ,

    remove: (user: User, onConfirm?: () => void, challenge?: boolean) => {

      const { fetchAccount, removeAccountQuietly } = accounts


      fb.askConsent({

        title: t('common.consent.remove_one_title', { singular }),
        content: t("common.consent.remove_one_msg", { singular }),
        okText: t("common.consent.remove_one_confirm", { singular }),

        okChallenge: challenge ? t('common.consent.remove_challenge', { singular }) : undefined,


        onOk: () => {


          toggleBusy(`${type}.removeOne`, t("common.feedback.save_changes"))

            // remove account if it exists. 
            .then(_ => fetchAccount(user).then(account => ['none', 'unknown'].includes(account.state) ? account : removeAccountQuietly(user)))  // won't tolerate failure on remove, as this cannot be compensated
            .then(_ => call.delete(user.username))
            .then(_ => self.all().filter(u => user.username !== u.username))
            .then(self.setAll)
            .then(_ => notify(t('common.feedback.saved')))
            .then(_ => onConfirm && onConfirm())

            .catch(e => showAndThrow(e, t("common.calls.remove_one_error", { singular })))
            .finally(() => toggleBusy(`${type}.removeOne`))


        }
      })

    }
    ,


    removeAll: (list: string[], onConfirm?: () => void) =>

      fb.askConsent({


        title: t('common.consent.remove_many_title', { count: list.length, plural }),
        content: t('common.consent.remove_many_msg', { plural }),
        okText: t('common.consent.remove_many_confirm', { count: list.length, plural }),

        onOk: () => {


          toggleBusy(`${type}.removeAll`, t("common.feedback.save_changes"))

            .then(_ => list.forEach(id => call.delete(id)))
            .then(_ => self.all().filter(u => !list.includes(u.username)))
            .then(self.setAll)
            .then(_ => notify(t('common.feedback.saved')))
            .then(_ => onConfirm && onConfirm())


            .catch(e => showAndThrow(e, t("common.calls.remove_one_error", { singular })))
            .finally(() => toggleBusy(`${type}.removeAll`))


        }
      })


    ,

    // adds or updates a user, returning it along with an associated account.
    // if the user is being activated, ask consent to create or activate account too (if applicable).
    // if the user is being deactivated, ask consent and deactivate account too. 
    save: async (user: User, then: ([User, Account]) => void) => {

      const isNew = !user.lifecycle.created

      const current = self.lookup(user.username)

      const activate = isNew ? user.lifecycle.state === 'active' : user.lifecycle.state === 'active' && current?.lifecycle.state === 'inactive'
      const deactivate = user.lifecycle.state === 'inactive' && current?.lifecycle.state === 'active'

      const save = isNew ? self.addQuietly(user) : self.updateQuietly(user)

      toggleBusy(`${type}.save`, t("common.feedback.save_changes"))

      // wraps a spinneer/notifications/callbacks aorund the save task.
      const task = async (activate: boolean, deactivate: boolean) => {

        try {

          const savedAndAccount = await save(activate, deactivate)

          notify(t('common.feedback.saved'))

          then(savedAndAccount)

        }
        catch (e) {

          showAndThrow(e as Error, t("common.calls.save_one_error", { singular }))

        }
        finally {
          toggleBusy(`${type}.save`)
        }

      }

      // asks consent before activating.
      if (activate)

        fb.askConsent({

          title: t('user.consent.activate_title'),
          content: t('user.consent.activate_content'),
          okText: t('user.consent.activate_confirm'),
          cancelText: t('user.consent.activate_cancel'),

          noValediction: true,

          onOk: () => task(true, false),
          onCancel: () => task(false, false)


        })

      // asks consent before activating.
      else if (deactivate)

        fb.askConsent({

          title: t('user.consent.deactivate_title'),
          content: t('user.consent.deactivate_content'),
          okText: t('user.consent.deactivate_confirm'),

          onOk: () => task(false, true)

        })


      else task(false, false)
    }

    ,

    addQuietly: (user: User) => {

      return async (activate: boolean, _: boolean) => {

        let saved = await call.add(user)   // store user first.

        let account: Account = accounts.noAccount(saved)

        if (activate) { // create account and update user immediately with account id.

          account = await accounts.createAccountQuietly(account)

          saved.details.preferences.accountId = account.id

          saved = await call.update(saved)

        }

        self.setAll([saved, ...self.all()]) // updates local state

        return [saved, account] as [User, Account]

      }

    }
    ,

    updateQuietly: (user: User) => {

      return async (activate: boolean, deactivate: boolean) => {


        let saved = await call.update(user) // update user first.

        let account = await accounts.fetchAccount(user)

        // sync account if it needed.
        if (!accounts.isSynced(saved, account))
          account = await accounts.syncAccount(account, user)

        if (activate)

          switch (account.state) {

            case 'none': account = await accounts.createAccountQuietly(account).catch(() => account); break;
            case 'suspended': account = await accounts.activateAccountQuietly(user).catch(() => account); break;

          }

        // adds/update user again with account id if needed.
        if (account.id && saved.details.preferences.accountId !== account.id) {

          saved.details.preferences.accountId = account.id
          saved = await call.update(saved)

        }

        else if (deactivate && account.state === 'active')
          account = await accounts.suspendAccountQuietly(user).catch(() => account)

        self.setAll(self.all().map(u => u.username === user.username ? user : u)) //updates local state

        return [saved, account] as [User, Account]

      }

    }

  }

  return self

}


