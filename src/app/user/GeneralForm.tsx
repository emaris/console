import { Form } from '#app/form/Form'
import { NoteBox } from '#app/form/NoteBox'
import { Switch } from '#app/form/Switch'
import { TextBox } from '#app/form/TextBox'
import { useT } from '#app/intl/api'
import { LanguageBox } from '#app/intl/LanguageBox'
import { MailProfileBox } from '#app/mail/MailProfileBox'
import { useModuleRegistry } from '#app/module/registry'
import { noTenant } from '#app/tenant/constants'
import { useTenantStore } from '#app/tenant/store'
import { FieldRow } from '../form/FieldRow'
import { FormState } from '../form/hooks'
import { TagBoxset } from '../tag/TagBoxset'
import { TenantBox } from '../tenant/TenantBox'
import { userType } from './constants'
import { User, useUser, useUserPreferences } from "./model"
import { UserSlot, useUserModule } from './module'
import { useLogged } from './store'
import { UserValidation } from './validation'



type Props = FormState<User> & {

    isNew: boolean
    report: ReturnType<UserValidation['validateProfile']>
    profileOnly?: boolean
}



export const UserGeneralForm = (props: Props) => {

    const registry = useModuleRegistry()
    const module = useUserModule()

    const t = useT()

    const { isNew, edited, change, report, profileOnly = false } = props;

    const withUser = useUser()
    const user = withUser(edited)


    const logged = useLogged()

    const users = useUserPreferences()
    const tenants = useTenantStore()


    const defaultLanguage = users.defaultLanguage(edited)

    const isNoteAvailable = !(profileOnly || logged.username === edited.username)

    const showPartyField = !logged.isSingleTenantUser()

    const isProtected = edited.guarded

    const tenant = tenants.lookup(user.tenant)

    const tenantActive = user.tenant===noTenant || tenant?.lifecycle.state==='active'

    return <Form state={props} sidebar={!profileOnly}>


        {profileOnly ||

            <Switch disabled={isProtected || !tenantActive}  label={t("common.fields.active.name")} onChange={change((u, v) => u.lifecycle.state = v ? "active" : "inactive")} validation={report.active}>
                {edited.lifecycle.state === 'active'}
            </Switch>

        }


        <TextBox disabled={!isNew || isProtected} label={t("user.fields.username.name")} onChange={change((u, v) => u.username = v)} validation={report.username}>
            {edited.username}
        </TextBox>

        <FieldRow side={report.lastname.help}>

            <TextBox grouped label={t("user.fields.name.first_name")} style={{ marginRight: 15 }} onChange={change((t, v) => t.details.firstname = v)} validation={report.firstname}>
                {edited.details.firstname}
            </TextBox>

            <TextBox grouped label={t("user.fields.name.last_name")} style={{ flexGrow: 1 }} onChange={change((t, v) => t.details.lastname = v)} validation={report.lastname}>
                {edited.details.lastname}
            </TextBox>

        </FieldRow>

        {profileOnly || (showPartyField &&

            <TenantBox disabled={isProtected} editable={isNew} module={module} resource={edited} onChange={change((u, v) => u.tenant = v === undefined ? noTenant : v)} />
        )
        }

        <TextBox disabled={isProtected} label={t("user.fields.email.name")} onChange={change((t, v) => t.details.email = v)} validation={report.email}>
            {edited.details.email}
        </TextBox>

        <TextBox label={t("user.fields.address.name")} onChange={change((t, v) => t.details.preferences.address = v)} validation={report.address}>
            {edited.details.preferences.address}
        </TextBox>

        <TextBox label={t("user.fields.phone.name")} onChange={change((t, v) => t.details.preferences.phone = v)} validation={report.phone}>
            {edited.details.preferences.phone}
        </TextBox>

        {profileOnly ||

            <TagBoxset disabled={isProtected} edited={edited.tags} type={userType} validation={report} onChange={change((t, v) => t.tags = v)} />

        }

        <LanguageBox disabled={isProtected} defaultLanguage={defaultLanguage} selectedKey={user.language} onChange={change((t, v) => t.details.preferences.language = v)} />

        {registry.allSlotsWith<UserSlot>(userType).filter(s => s.preferencesComponent).map((s, i) => <div key={i}>{s.preferencesComponent?.(props)}</div>)}


        <TextBox disabled={isProtected} label={t("user.fields.alternative_email.name")} onChange={change((t, v) => t.details.preferences.alternativeEmails = [v])} validation={report.alternativeEmail}>
            {edited.details.preferences.alternativeEmails?.[0]}
        </TextBox>

        <MailProfileBox onChange={change((t, v) => v ? t.details.preferences.mailProfile = v : delete t.details.preferences.mailProfile ) }>
            {edited.details.preferences.mailProfile}
        </MailProfileBox>

        {isNoteAvailable && <NoteBox label={t("common.fields.note.name")} validation={report.note} autoSize={{ minRows: 4, maxRows: 6 }} onChange={change((t, v) => t.details.preferences.note = t.details.preferences.note ? { ...t.details.preferences.note, [logged.tenant]: v } : { [logged.tenant]: v })} >
            {edited.details.preferences.note}
        </NoteBox>}


    </Form>

}
