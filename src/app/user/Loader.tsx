
import { defaultLanguage } from "#app/intl/model"
import { useIntl } from '#app/intl/store'
import { useTenantStore } from '#app/tenant/store'
import { useRenderGuard } from 'apprise-frontend-core/utils/renderguard'
import * as React from "react"
import { useLogged, useLogin, useUserStore } from './store'

type Props = React.PropsWithChildren<{}>
  
export const LoggedUserLoader = (props:Props) => {

  const store = useUserStore()
  const login = useLogin()
  const logged = useLogged()

  const {areReady, all} = useTenantStore()
  const intl = useIntl()

  const {content} = useRenderGuard({
    when: login.isLogged(),
    render:props.children,
    orRun:store.fetchLogged
  })

  const areTenantsReady = areReady()

  //Set the app language to the tenant's language if the logged user does not have a default language
  // The effect runs as soon as the tenants are loaded
  React.useEffect(() => {

        if (areTenantsReady) {
            if (logged.details.preferences.language === undefined && logged.isTenantUser()) {
                const loggedUserTenant = all().find(t => t.id === logged.tenant)
                loggedUserTenant && intl.setCurrentLanguage(loggedUserTenant.preferences.language ?? defaultLanguage)
            }
        }
        //eslint-disable-next-line
    }, [areTenantsReady])


  return content

}