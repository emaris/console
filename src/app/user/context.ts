import { fallbackStateOver, State } from 'apprise-frontend-core/state/api'
import { createContext } from 'react'
import { User } from './model'



export type UserNext = {
    model?: User
    linkBack?: string
  }
  
  export type UserState = {
  
    logged: User
    users: {
      all: User[]
      refs: Record<string, User>
      map: Record<string, User>
      next: UserNext | undefined
      selected: string[]
      edited: User
    }
  
  }
  
  export const initialUsers: UserState = {
  
    logged: undefined!,
    users: {
      all: undefined!,
      refs: undefined!,
      map: undefined!,
      next: undefined,
      selected: [],
      edited: undefined!
    }
  }


  export const UserContext = createContext<State<UserState>>(fallbackStateOver(initialUsers))