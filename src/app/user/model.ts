
import { useT } from '#app/intl/api';
import { useIntl } from '#app/intl/store';
import { MailProfile } from '#app/mail/model';
import { useTagModel } from '#app/tag/model';
import { useTenantStore } from '#app/tenant/store';
import change from "immer";
import { standardActions } from "../iam/actions";
import { Action, ActionDto, any, idOf, impliedBy, impliedByAny, likeAny, specialise, useActions } from "../iam/model";
import { Language } from "../intl/model";
import { Lifecycle, newLifecycle } from "../model/lifecycle";
import { tenantActions } from "../tenant/actions";
import { noTenant } from "../tenant/constants";
import { TenantResource, useTenantResourceType } from "../tenant/model";
import { deepclone } from "../utils/common";

export type UserState = "active" | "inactive"

export type UserDetails = {

  firstname: string
  lastname: string
  email: string
  preferences: Partial<UserPreferences>
}

type BaseUser = TenantResource & {
  username: string
  lifecycle: Lifecycle<UserState>
  details: UserDetails
  tags: string[]
  guarded?: boolean
}


export type UserDto = BaseUser & {
  permissions: ActionDto[]
}

export type User = BaseUser & {
  permissions: Action[]
}

export type UserPreferences = Partial<{

  accountId: string,
  language: Language
  address: string
  phone: string
  mailProfile: MailProfile
  alternativeEmails: string[]
  note: Record<string, string>

}> & { [key: string]: any }

export type UserReference = {

  username: string
  name: string

}


export const useUserPreferences = () => {

  const intl = useIntl()

  const tenantstore = useTenantStore()

  return {

    defaultLanguage: (user: User) => user.tenant === noTenant ? intl.defaultLanguage() : tenantstore.safeLookup(user.tenant).preferences.language

    ,

    isFocalPoint: (user: User) => user.tenant === noTenant ? false : tenantstore.all().some(t => t.preferences.focalPoint && (t.preferences.focalPoint === user.username))


  }
}


export const useUserModel = () => {

  const tags = useTagModel()

  return {


    stringify: (u: User) => `${u.username} ${u.tenant ?? ''} ${nameOf(u)} ${u.details.email} ${u.details.preferences.alternativeEmails?.join(' ')} ${tags.stringifyRefs(u.tags)}`

    ,

    clone: (u: User): User => ({ ...deepclone(u), lifecycle: newLifecycle('inactive') })

    ,

    comparator: (o1: User, o2: User) => nameOf(o1).localeCompare(nameOf(o2))

  }
}



export const newUserIn = (tenant: string): User => ({

  tenant,
  username: undefined!,
  lifecycle: newLifecycle("inactive"),
  details: {
    firstname: undefined!,
    lastname: undefined!,
    email: undefined!,
    preferences: {}
  },
  tags: [],
  permissions: []

})




export const nameOf = (u: User, fallback?: string) => u.details.firstname || u.details.lastname ? ((u.details.firstname && u.details.firstname + " ") || '') + (u.details.lastname || '') : fallback ? fallback : u.username ? u.username : ''

export const useNoUser = () => {

  const t = useT()

  const unknown = t('common.labels.unknown')

  return {

    get: (): User => ({ ...newUserIn(unknown), username: unknown })

  }


}


export const useUser = () => {

  const tenantResourceType = useTenantResourceType()
  const actions = useActions()

  const { doAnything } = standardActions

  const api = (user: User) => {

    const self = {

      ...user,

      /////////////////////////  authz & tenancy

      // Actions are unaware of tenancy: if they're allowed, they're allowed eveywhere and anytime in principle.
      // Tenancy is enforced instead via scoping -- ahead of authorization -- by hiding/showing to users only resources in their tenant (tenant users) or
      // in any tenant they have the right to manage (multimanagers). For all these users, tenant management rights are like full admin rights, only limited to the
      // scope of specific tenants. Users that are NOT in a tenant see the resources of ALL tenants, but these remain readonly where the users have no
      // tenant management rights. 
      //
      // Currently, resource scoping is performed at the backend, by filtering data requests.



      can: (a: Action, resourceTenant?: string): boolean => {

        let passthrough = false;

        // NOTE ON PASSHTROUGH: on tenant resources there's a passthrough rule for tenant managers: they are like admins, can do eveything.
        // the action doesn't tell us the tenant of the resource though, how do we know we have to apply the passthrough rule?
        // if users _are_ in a tenant, they see only tenant resources so we can just check they're managers.
        // if users have _no_ tenant, they see all resources and while they might manage some tenant (a regional admin) 
        // the may not manage the tenant of the resource.
        // here we expect clients to cooperate: if they want to cover regional admins they need to indicate the tenant.
        // if they don't, the passthrough rule will not apply and users will face a standard check based on their other permissions. 
        if ( a.actionType !== 'admin' && tenantResourceType(a.type))
          passthrough = self.hasNoTenant() ? !!resourceTenant && self.isManagerOf(resourceTenant) : self.managesSomeTenant()


        //console.log(self.username,"can",a,"passthrough",passthrough,"tenant resource?",moduleRegistry.all(),isTenantResourceType(a.type))
        return passthrough || impliedByAny(a, user.permissions)

      }

      ,


      isAdmin: () => self.allPermissions().some(p => idOf(p) === idOf(doAnything)),

      canAny: (actions: Action[], tenant?: string) => actions.length === 0 || actions.some(a => self.can(a, tenant)),

      canForSome: (a: Action) => likeAny(a, user.permissions),

      allPermissions: () => user.permissions,

      // reconciles permissions
      effectivePermissions: () => actions.reconcile(self.allPermissions()),




      // --------------------------------------------------------------------------------------------  no-tenant roles

      //  has no tenant.
      hasNoTenant: () => user.tenant === noTenant,

      // has no tenant and is tasked explicitly with managing at least one then.
      isRegionalAdmin: () => self.hasNoTenant() && self.managesSomeTenant('direct'),

      // has no tenant and doesn't manage any.
      isOfficer: () => self.hasNoTenant() && !self.managesSomeTenant(),


      //  -------------------------------------------------------------------------------------------- tenant roles

      //   has a tenant.
      isTenantUser: () => !self.hasNoTenant()

      ,

      //TODO: check if it can be removed.
      isRegularTenantUser: () => self.isTenantUser() && !self.managesSomeTenant()

      ,

      // has a tenant and operates only in that tenant (with management rights or less).
      isSingleTenantUser: () => self.isTenantUser() && !self.isMultiManager()

      ,

      isTenantManager: () => self.isTenantUser() && self.isManagerOf(user.tenant, 'direct')

      ,

      // a tenant user that manages multiple tenants other than its own. 
      isMultiManager: () => self.isTenantUser() && self.managesMultipleTenants('direct')


      ,


      //  ---------------------------------------------------------------------------------------------- tenancy-based privilege checks.


      // manages at least one tenant, possibly his own, either directly or via inheritance.
      managesSomeTenant: (mode: 'direct' | 'normal' = 'normal') => mode === 'normal' ? self.canForSome(tenantActions.manage) : user.permissions.some(a => impliedBy(a, tenantActions.manage))

      ,

      // manages a given tenant, either directly or via inheritance.
      isManagerOf: (tenant: string, mode: 'direct' | 'normal' = 'normal') => tenant !== any && (mode === 'normal' ? self.can(specialise(tenantActions.manage, tenant)) : user.permissions.some(a => impliedBy(a, specialise(tenantActions.manage, tenant))))

      ,

      // manages at least one tenant other than his own (possibly all) 
      managesMultipleTenants: (mode: 'direct' | 'normal' = 'normal') =>

        // manages other tenants directly.
        (user.permissions.some(p => impliedBy(p, tenantActions.manage) && p.resource !== user.tenant))

        ||

        // or has no tenant and can manage them all, indirectly
        //(mode==='normal' && self.can(tenantActions.manage))
        (mode === 'normal' && self.hasNoTenant() && self.can(tenantActions.manage))

      ,

      managedTenants: () => user.permissions.filter(p => p.resource !== any && impliedBy(p, tenantActions.manage)).map(a => a.resource)

      ,


      //  other 

      language: user.details.preferences?.language,

      changeLanguage: (lang: Language) => api(change(user, s => { s.details.preferences.language = lang }))

    }

    return self

  }

  return api

}