
import { useActions } from '#app/iam/model';
import { noTenant } from '#app/tenant/constants';
import { usePreload } from 'apprise-frontend-core/client/preload';
import { useCalls } from "../call/call";
import { Account } from './account';
import { useNoUser, User, UserDto, UserReference } from "./model";

const svc = "admin"
export const loggedApi = "/logged"
export const userApi = "/user"
export const userrefApi = "/userrefs"
export const accountApi = `/account`

export const useUserCalls = () => {
  
  const actions = useActions()


    const { at } = useCalls()

    const intern = (dto: UserDto): User => ({
        ...dto,
        details: { ...dto.details, preferences: dto.details.preferences || {} },
        permissions: actions.reconcile(dto.permissions.map(actions.intern))
      })


      const extern = (u: User) => ({
        ...u,
        permissions: u.permissions.map(actions.extern)
      })


    const nouser = useNoUser();

    const preload = usePreload()

    return {


        fetchLogged: () => (preload.get<User>(loggedApi) ?? at(loggedApi, svc).get<User>()).then(intern)

        ,

        fetchAll: (permissions: boolean = false) => preload.get<User[]>(userApi) ??  at(`${userApi}?permissions=${permissions}`, svc).get<User[]>()

        ,

        fetchAllReferences: () => (preload.get<UserReference[]>(userrefApi) ?? at(userrefApi).get<UserReference[]>()).then(users => users.map(({ username, name }) => {

            const base = nouser.get()

            base.lifecycle.state = 'active'
            base.username = username
            base.tenant = noTenant

            const [firstName, ...lastnames] = name.split(' ')


            base.details.firstname = firstName
            base.details.lastname = lastnames.join(' ')

            return base

        }))

        ,

        fetchOne: (username: string) => at(`${userApi}/${encodeURIComponent(username)}`, svc).get<User>().then(intern)

        ,

        add: (user: User) => at(userApi, svc).post<User>(extern(user)).then(intern)

        ,

        update: (user: User) => at(`${userApi}/${user.username}`, svc).put<User>(extern(user)).then(intern)

        ,

        updateMany: (many: User[]) => at(`${userApi}/bulk`, svc).put<User[]>(many.map(extern)).then(saved=>saved.map(intern))


        ,

        updateProfile: (user: User) => at(`${userApi}/${user.username}/profile`, svc).put<User>(extern(user)).then(intern)

        ,

        delete: (username: string) => at(`${userApi}/${username}`, svc).delete()

        ,

        fetchAccount: (username: string) => at(`${accountApi}/${username}`, svc).get<Account>()

        ,

        createAccount: (account: Account) => at(`${accountApi}`, svc).post<Account>(account)

        ,

        updateAccount: (account: Account) => at(`${accountApi}/${account.username}`, svc).put<Account>(account)


        ,

        deleteAccount: (username: string) => at(`${accountApi}/${username}`, svc).delete()

        ,

        verifyAccount: (username: string) => at(`${accountApi}/${username}/verify`, svc).post<Account>()

        ,

        activateAccount: (username: string) => at(`${accountApi}/${username}/activate`, svc).post<Account>()

        ,

        deactivateAccount: (username: string) => at(`${accountApi}/${username}/suspend`, svc).post<Account>()

        ,

        deactivateAccounts: (usernames: string[]) => at(`${accountApi}/suspend`, svc).post<string[]>(usernames)

        ,

        resetAccount: (username: string) => at(`${accountApi}/${username}/reset`, svc).post<Account>()


      }

    
}