

import { FormState } from "#app/form/hooks";
import { useCurrentLanguage, useT } from '#app/intl/api';
import { useModuleRegistry } from '#app/module/registry';
import { useTagStore } from '#app/tag/store';
import { useTenantStore } from '#app/tenant/store';
import { Paragraph } from "../components/Typography";
import { noTenant } from "../tenant/constants";
import { check, checkIt, empty, invalidCharacters, invalidEmail, reservedKeyword, uniqueWith, Validation, withReport } from "../utils/validation";
import { userPlural, userSingular, userType } from "./constants";
import { nameOf, User } from "./model";
import { UserSlot } from './module';
import { useLogged, useUserStore } from './store';



export type UserValidation = ReturnType<typeof useUserValidation>

export const useUserValidation = () => {

    const t = useT()
           
          
    const registry = useModuleRegistry()
    const userstore = useUserStore()
    const currentLanguage = useCurrentLanguage()

    const logged = useLogged()

    const tags = useTagStore()
    const tenantstore = useTenantStore()

    return {

        validateProfile: (formstate: FormState<User>) => {

            const { edited, initial } = formstate

            const isNew = !edited.lifecycle.created

            const users = userstore.all()

            const [singular, plural] = [t(userSingular).toLowerCase(), t(userPlural).toLowerCase()]

            const pluginReports = registry.allSlotsWith<UserSlot>(userType).reduce((acc, slot) => ({ ...acc, ...(slot.preferencesValidation?.(formstate) ?? {}) }), {} as Record<string,Validation>)

            return withReport({

                active: checkIt().nowOr(t("common.fields.active.msg"),
                    <>
                        <Paragraph>
                            {t("common.fields.active.help", { plural })}
                        </Paragraph>
                        <Paragraph style={{ paddingTop: 5 }}>
                            {t("common.fields.active.help_remove", { plural })}
                        </Paragraph>
                    </>),

                note: check(edited.details.preferences.note ? edited.details.preferences.note[logged.tenant] : undefined).nowOr(t("common.fields.note.msg"), t("common.fields.note.help", { singular })),


                username: check(edited.username)
                    .with(empty(t))
                    .with(invalidCharacters(t))
                    .with(reservedKeyword(t))
                    .with(uniqueWith(users, u => edited.username === undefined ? true : u.username.toUpperCase() === edited.username.toUpperCase() && (isNew === true ? true : u.username !== initial.username), t))
                    .nowOr(t("common.fields.id.msg"),
                        <>
                            <Paragraph ellipsis={{ rows: 4, expandable: true }}>
                                {t("common.fields.id.help1", { singular, plural })}
                            </Paragraph>
                            <Paragraph style={{ paddingTop: 5 }}>
                                {t("common.fields.id.help2")}
                            </Paragraph>
                        </>
                    ),


                firstname: check(edited.details.firstname).with(empty(t)).now(),

                lastname: check(edited.details.lastname)
                    .with(empty(t))
                    .with(uniqueWith(users.filter(user => user.tenant === edited.tenant), u => u.username !== edited.username && nameOf(u) === nameOf(edited), t), { msg: edited.tenant === noTenant ? t("common.validation.duplicate_error") : t("common.validation.lastname_taken", { lastname: nameOf(edited), tenant: tenantstore.lookup(edited.tenant)?.name[currentLanguage] }) })
                    .nowOr("", t("user.fields.name.help")),


                email: check(edited.details.email)
                    .with(empty(t))
                    .with(invalidEmail(t))
                    .with(uniqueWith(users, u => u.details.email === edited.details.email && (isNew === true ? true : u.username !== initial.username), t))
                    .nowOr(t("user.fields.email.msg"), t("user.fields.email.help")),

                alternativeEmail: check(edited.details.preferences.alternativeEmails?.[0])
                    .with(invalidEmail(t))
                    .nowOr(t("user.fields.alternative_email.msg"), t("user.fields.alternative_email.help")),

                address: checkIt().nowOr(t("user.fields.address.msg"), t("user.fields.address.help")),

                phone: checkIt().nowOr(t("user.fields.phone.msg"), t("user.fields.phone.help")),

                home: checkIt().nowOr(t("common.fields.preferred_zone.msg"), t("cuser.fields.location.help")),

                timezone: checkIt().nowOr(t("common.fields.preferred_zone.msg"), t("common.fields.preferred_zone.help")),


                ...tags.validateCategories(edited.tags).for(userType)

                ,

                ...pluginReports

            })


        }

    }
}


