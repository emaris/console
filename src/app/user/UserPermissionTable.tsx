
import * as React from "react";
import { useT } from '#app/intl/api';
import { PermissionProps, PermissionTable, ResourceProps, StateProps, SubjectProps } from "../iam/PermissionTable";
import { userPlural, userSingular } from "./constants";
import { UserLabel } from "./Label";
import { nameOf, User } from "./model";
import { useLogged, useUserStore } from './store';


//  Wraps PermissionTable when subjects are users, so as to inject appropriate defaults.

type PermissionsProps<R> = ResourceProps<R> & PermissionProps & Partial<SubjectProps<User>> & Partial<StateProps<User,R>> & {

    id: string
}

export const UserPermissions = <R extends Object> (props:PermissionsProps<R>) => {

    const t = useT()

    const logged = useLogged()
    
    const {all} = useUserStore()

    return  <PermissionTable logged={logged!} {...props}
                subjectId = {props.subjectId || (u => u.username)}
                renderSubject={props.renderSubject || ((u:User) => <UserLabel user={u} />) } 
                renderSubjectOption={props.renderSubjectOption || ((u:User) => <UserLabel noLink user={u}/>)} 
                subjectText={nameOf}
                subjectRange={props.subjectRange || all()}  
                subjectSingular={t(userSingular)}
                subjectPlural={t(userPlural)}/>


           
}
