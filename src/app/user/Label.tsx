

import { Tooltip } from "antd"
import * as React from "react"
import { useT } from '#app/intl/api'
import { useLocation } from "react-router"
import { Label, LabelProps, sameLabel } from "../components/Label"
import { icns } from "../icons"
import { userIcon, userRoute, userType } from "./constants"
import { nameOf, User, useUser } from "./model"
import { useLogged, useUserStore } from './store'

//  note: cf. memoisation strategy in generic Label.

type Props = LabelProps & {

    user: string | User | undefined

}

const innerrouteRegexp = new RegExp(`${userRoute}\\/(.+?)(\\?|\\/|$)`)



export const UserLabel = (props: Props) => {

    const location = useLocation()

    const logged = useLogged()
    const userstore = useUserStore()

    const { user } = props

    if (!user)
        return null

    const resolved = typeof user === 'string' ? userstore.lookup(user) : user

    if (!resolved)
        return <Label icon={userIcon} title={user} />

    return <PureMemoLabel {...props} user={resolved} users={userstore} location={location} logged={logged} />


}

const PureMemoLabel = React.memo(function PureUserLabel(props:  Omit<Props,'user'> & { 
        
        user: User 
        users: ReturnType<typeof useUserStore>
        logged: ReturnType<typeof useLogged>
        location: ReturnType<typeof useLocation> 
    }) {

    const t = useT()

    const { user:u, logged, location, users, inactive, linkTo, decorations = [], ...rest } = props

    const withUser = useUser()

    const user = withUser(u)

    const { pathname, search } = location

    const note = user.details.preferences.note && user.details.preferences.note[logged.tenant] ? user.details.preferences.note[logged.tenant] : undefined
    // const note = user.details.preferences.note

    if (note)
        decorations.push(<Tooltip title={note}>{icns.note}</Tooltip>)

    const inactiveState = user.lifecycle.state === 'inactive'

    // like routeTo() if the current route is not a campaign route, otherwise replaces the current campaign with the target one.
    // undefined, if the two campaigns coincide.
    const innerRouteTo = (target: User) => {

        const regexpmatch = pathname.match(innerrouteRegexp)?.[1]
        const linkname = encodeURIComponent(target.username ?? '')

        return regexpmatch ?
            regexpmatch === linkname ? undefined : `${pathname.replace(regexpmatch, linkname!)}${search}`
            : users.routeTo(target)


    }

    const route = linkTo ?? innerRouteTo(user)

    const coordinator = logged.isTenantUser() && user.hasNoTenant()

    const title = props.title ?? (coordinator ? t("user.notenant_name") : nameOf(user, t("common.labels.user_lookup_fallback"))
    )
    return <Label {...rest} inactive={inactive ?? inactiveState} linkTarget={userType} title={title} icon={userIcon} decorations={decorations} linkTo={route} />

}, ($, $$) => $.user === $$.user && sameLabel($, $$))

