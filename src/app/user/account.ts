import { pushLogoutTopic } from '#app/call/constants';
import { PushLogoutEvent } from '#app/call/Logout';
import { useT } from '#app/intl/api';
import { usePushEvents } from '#app/push/api';
import { useToggleBusy } from '#app/system/api';
import { through } from '#app/utils/common';
import { notify, showAndThrow, useFeedback } from '#app/utils/feedback';
import { useUserCalls } from './calls';
import { User } from './model';


export type AccountState = 'unknown' | 'none' | "pending" | "active" | "suspended"

export type Account = {

    id?:string
    username: string
    firstname: string
    email: string
    language?: string

    state: AccountState
}


export const unknownAccount = (username: string): Account => ({ username, state: 'unknown' } as Account)

export const useAccounts = () => {

    const t = useT()

    const toggleBusy = useToggleBusy()

    const fb = useFeedback()

    const singular = t("user.account.shortname").toLowerCase()
    
    const calls = useUserCalls()

    const push = usePushEvents()

    const self = {

        accountFor: (user: User): Account => ({ username: user.username, firstname:user.details.firstname, email: user.details.email, language: user.details.preferences.language }) as Account

        ,


        noAccount: (user: User): Account => ({ ...self.accountFor(user), state: 'none' })

        ,

        // sync only on active/pending accounts (excludes also suspended, fusionauth won't accept updates on locked items)
        isSynced: (user:User, account:Account) => 

            (account.state !== 'active' && account.state !== 'pending') ||
                
            (account.email?.toLowerCase().trim() === user.details.email?.toLowerCase().trim() && 
             account.firstname?.toLocaleLowerCase().trim() === user.details.firstname.toLowerCase().trim() &&
             
                // disable this until fusionauth sorts this mess out: https://github.com/FusionAuth/fusionauth-issues/issues/441
                true //account.language === (user.details.preferences.language ?? users.defaultLanguage(user)) 
             )

        ,


        syncWith: (account:Account, user:User) => ({...account, ...self.accountFor(user)}) 

        ,

        fetchAccount: (user: User) =>

            toggleBusy(`fetch-account-${user.username}`)

                .then(() => calls.fetchAccount(user.username))

                .catch(e => e.response?.status === 404 ? self.noAccount(user) : unknownAccount(user.username))
                .finally(() => toggleBusy(`fetch-account-${user.username}`))

        ,

        createAccount: (account: Account) => self.createAccountQuietly(account).then(through(_ => notify(t('common.feedback.saved'))))


        ,

        createAccountQuietly: (account: Account) =>

        toggleBusy(`create-account-${account.username}`, t("common.feedback.save_changes"))

            .then(() => calls.createAccount({...account,state:undefined!}))
           
            .catch(e => showAndThrow(e, t("common.calls.save_one_error", { singular })))
            .finally(() => toggleBusy(`create-account-${account.username}`))

    ,

        syncAccount: (account:Account, user:User) => {
                
            const updatedAccount = self.syncWith(account,user)

            console.log({updatedAccount})

            return toggleBusy(`sync-account-${account.username}`)

            .then(() => console.log(`syncing account for ${account.username}`))
            .then(() => calls.updateAccount(updatedAccount))

            .catch(e => showAndThrow(e, t("common.calls.save_one_error", { singular })))
            .finally(() => toggleBusy(`sync-account-${account.username}`))
            
        }

        ,

        verifyAccount: (user: User) =>

            toggleBusy(`verify-account-${user.username}`)

                .then(() => calls.verifyAccount(user.username))
                .then(through(_ => notify(t('common.feedback.saved'))))

                .catch(e => showAndThrow(e, t("common.calls.save_one_error", { singular })))
                .finally(() => toggleBusy(`verify-account-${user.username}`))

        ,

        verifyAccountWithConsent: (user: User, onConfirm: (_: Account) => any = () => { }) =>


                fb.askConsent({

                    title: t('user.account.verify_pwd_action', { singular }),
                    content: t("user.account.verify_pwd_action_consent", { singular }),
                    okText: t("user.account.verify_pwd_action_confirm", { singular }),

                    onOk: () => self.verifyAccount(user).then(onConfirm)
                })


        ,


        activateAccount: (user: User) => self.activateAccountQuietly(user).then(through(_ => notify(t('common.feedback.saved'))))

        ,

        activateAccountQuietly: (user: User) =>

            toggleBusy(`activate-account-${user.username}`)

                .then(() => console.log(`reactivating account for ${user.username}`))
                .then(() => calls.activateAccount(user.username))

                .catch(e => showAndThrow(e, t("common.calls.save_one_error", { singular })))
                .finally(() => toggleBusy(`activate-account-${user.username}`))

        ,


        suspendAccount: (user: User, onConfirm: (_: Account) => any = () => { }) =>


            fb.askConsent({

                title: t('user.account.suspend_action', { singular }),
                content: t("user.account.suspend_action_consent", { singular }),
                okText: t("user.account.suspend_action_confirm", { singular }),

                onOk: () => self.suspendAccountQuietly(user)
                    .then(through(_ => notify(t('common.feedback.saved'))))
                    .then(onConfirm)
            })

        ,

        suspendAccountQuietly: (user: User) =>

            toggleBusy(`suspend-account-${user.username}`)

                .then(() => console.log(`suspending account for ${user.username}`))
                .then(() => calls.deactivateAccount(user.username))
                .then(through(() => push.publish<PushLogoutEvent>(pushLogoutTopic, { users: [user.username] })))

                .catch(e => showAndThrow(e, t("common.calls.save_one_error", { singular })))
                .finally(() => toggleBusy(`suspend-account-${user.username}`))


        ,

        resetAccount: (user: User, onConfirm: (_: Account) => any = () => { }) =>

            fb.askConsent({

                title: t('user.account.change_pwd_action', { singular }),
                content: t("user.account.change_pwd_action_consent", { singular }),
                okText: t("user.account.change_pwd_action_confirm", { singular }),

                onOk: () => {

                    toggleBusy(`reset-account-${user.username}`)

                        .then(() => console.log(`resetting account for ${user.username}`))
                        .then(() => calls.resetAccount(user.username))
                        .then(through(_ => notify(t('common.feedback.saved'))))
                        .then(onConfirm)
                        .then(through(() => push.publish<PushLogoutEvent>(pushLogoutTopic, { users: [user.username] })))

                        .catch(e => showAndThrow(e, t("common.calls.save_one_error", { singular })))
                        .finally(() => toggleBusy(`reset-account-${user.username}`))


                }
            })

        ,

        removeAccount: (user: User, onConfirm: (_: Account) => any = () => { }) =>

            fb.askConsent({

                title: t('user.account.remove_action', { singular }),
                content: t("user.account.remove_action_consent", { singular }),
                okText: t("user.account.remove_action_confirm", { singular }),

                onOk: () => {

                    self.removeAccountQuietly(user)
                        .then(through(_ => notify(t('common.feedback.saved'))))
                        .then(onConfirm)

                }
            })

        ,

        removeAccountQuietly: (user: User) =>

            toggleBusy(`remove-account-${user.username}`)

                .then(() => console.log(`removing account for ${user.username}`))
                .then(() => calls.deleteAccount(user.username)).then(() => self.noAccount(user))

                .catch(e => showAndThrow(e, t("common.calls.save_one_error", { singular })))
                .finally(() => toggleBusy(`remove-account-${user.username}`))



    }

    return self;


}