
import { Icon, Tooltip } from 'antd'
import { Button } from '#app/components/Button'
import { Label, LabelProps } from '#app/components/Label'
import { Paragraph, Text } from '#app/components/Typography'
import { useT } from '#app/intl/api'
import * as faicns from "react-icons/fa"
import { useAuth } from '../call/auth'
import { Account, useAccounts } from './account'
import { userIcon } from './constants'
import { User } from './model'


type Props = {

    user: User
    account: Account
    setAccount: (_: Account) => any
}


export const AccountView = (props: Props) => {

    const t = useT()

    const { isSynced, createAccount, verifyAccount, suspendAccount, removeAccount, resetAccount, verifyAccountWithConsent, activateAccount, syncAccount } = useAccounts()

    const { user, account, setAccount } = props

    const auth = useAuth()

    const { email, language } = account

    const state = isSynced(user,account) ? account.state : 'offsync'

    const canRemove = state !== 'none'
    const canReset = state === 'active'
    const canSuspend = state === 'active' || state === 'pending'

    return <div className="account-view">

        <Paragraph>{t('user.account.explainer')}</Paragraph>
        <div className={`account-panel ${state}`}>
            <Tooltip overlayClassName='avatar-tooltip' title={<div>
                <Paragraph>
                    <Text className="label" smallcaps>e-mail:</Text>&nbsp;<Text smallcaps>{email}</Text>
                </Paragraph>
                <Paragraph>
                    <Text className="label" smallcaps>language:</Text>&nbsp;<Text smallcaps>{language}</Text>
                </Paragraph>
            </div>}>
                <Icon className='state-icon' component={faicns.FaUserCircle} />
            </Tooltip>
            <AccountLabel noIcon user={user} account={account} />
            <Paragraph className='state-description'>{t(`user.account.state_${state}_description`)}</Paragraph>
            <div className='lifecycle-actions'>
                {state === 'none' &&
                    <Button type='primary' onClick={() => createAccount(account).then(setAccount)}>{t("user.account.create_action")}</Button>
                }
                {state === 'pending' &&

                    <Button type='primary' onClick={() => verifyAccount(user).then(setAccount)}>{t("user.account.resend_verification_action")}</Button>
                }
                {state === 'suspended' &&

                    <Button type='primary' onClick={() => activateAccount(user).then(setAccount)}>{t("user.account.activate_action")}</Button>
                }
                {state === 'offsync' &&

                    <Button type='primary' onClick={() => syncAccount(account,user).then(setAccount)}>{t("user.account.sync_action")}</Button>
                }
            </div>
        </div>

        <div className={`account-secondary-panel ${state}`}>

            <div className={`action-section section-${canSuspend ? 'enabled' : 'disabled'}`}>
                <div className='section-title'>{t("user.account.suspend_action")}</div>
                <Paragraph className='section-explainer'>{t('user.account.suspend_action_explainer')}</Paragraph>
                <Button type='danger' enabled={canSuspend} onClick={() => suspendAccount(user, setAccount)}>{t("user.account.suspend_action")}</Button>
            </div>
            <div className={`action-section section-${canReset ? 'enabled' : 'disabled'}`}>
                <div className='section-title'>{t("user.account.verify_pwd_action")}</div>
                <Paragraph className='section-explainer'>{t('user.account.verify_pwd_action_explainer')}</Paragraph>
                <Button type='danger' enabled={canReset} onClick={() => verifyAccountWithConsent(user,setAccount)}>{t("user.account.verify_pwd_action")}</Button>
            </div>
            <div className={`action-section section-${canReset ? 'enabled' : 'disabled'}`}>
                <div className='section-title'>{t("user.account.change_pwd_action")}</div>
                <Paragraph className='section-explainer'>{t('user.account.change_pwd_action_explainer')}</Paragraph>
                <Button type='danger' enabled={canReset} onClick={() => resetAccount(user,setAccount)}>{t("user.account.change_pwd_action")}</Button>
            </div>
            <div className={`action-section section-${canRemove ? 'enabled' : 'disabled'}`} >
                <div className='section-title'>{t("user.account.remove_action")}</div>
                <Paragraph className='section-explainer'>{t('user.account.remove_action_explainer')}</Paragraph>
                <Button type='danger' enabled={canRemove} onClick={() => removeAccount(user, setAccount)}>{t("user.account.remove_action")}</Button>
            </div>
            <div className={`action-section section-${canReset ? 'enabled' : 'disabled'}`} >
                <div className='section-title'>{t("user.account.logout_action")}</div>
                <Paragraph className='section-explainer'>{t('user.account.logout_action_explainer')}</Paragraph>
                <Button type='danger' enabled={canReset} onClick={() => auth.pushLogout(user)}>{t("user.account.logout_action")}</Button>
            </div>

        </div>


    </div>
}



export const AccountLabel = (props: LabelProps & {

    user: User
    account: Account

}) => {

    const t = useT()

    const users = useAccounts()

    const { user,account } = props

    // add psedo-state offsync for active records.
    const state = account.state !== 'active' || users.isSynced(user,account) ? account.state : 'offsync'

    return <Label className={`account-label state-${state}`} icon={userIcon} title={t(`user.account.state_${state}_label`)} tip={t(`user.account.state_${state}_tip`)} mode='tag' {...props} />

}