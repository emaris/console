import { userIcon, userType } from "./constants";
import { Action } from "#app/iam/model";


export const base = { icon:userIcon, type:userType}


//  at the time being, this remains a virtual specialisation of the administrator's role that cannot be granted directly. 
//  it is used however, for authorizaton checks.

export const userActions : {[id:string]:Action} = {  
    
    manage:  { ...base, labels:["manage"], virtual:true, shortName:"user.actions.manage.short", name:"user.actions.manage.name",  description: "user.actions.manage.name."}

}