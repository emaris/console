import { FormState } from "#app/form/hooks"
import { Module } from '#app/module/model'
import { pushEventType } from '#app/push/constants'
import { tenantType } from "#app/tenant/constants"
import { TenantSlot } from '#app/tenant/module'
import { Validation } from "#app/utils/validation"
import { ReactElement } from "react"
import { SubjectProps } from "../iam/PermissionTable"
import { iamType } from "../iam/constants"
import { IamSlot } from "../iam/module"
import { ChangesProps } from "../iam/permission"
import { userActions } from "./actions"
import { userIcon, userPlural, userSingular, userType } from "./constants"
import { User } from "./model"
import { usePushUserSlot } from './pushevents'


export const useUserModule = () : Module => {

      const pushUserSlot = usePushUserSlot()

      return {

            icon: userIcon,
            type: userType,

            nameSingular: userSingular,
            namePlural: userPlural,

            [pushEventType]: pushUserSlot,

            [tenantType]: {

                  tenantResource: true

            } as TenantSlot,

            [iamType]: {
                  actions: userActions,

            } as IamSlot
      }

}

export type ComponentProps = ChangesProps & Partial<SubjectProps<User>>

export type UserSlot = {

      singular?: string
      plural?: string,
      preferencesComponent?: (formstate: FormState<User>) => ReactElement
      preferencesValidation?: (formstate: FormState<User>) => { [key: string]: Validation }

}