import { useT } from '#app/intl/api';
import { useIntl } from '#app/intl/store';
import { useDevMail } from "#app/mail/DevMail";
import { tenantSingular } from "#app/tenant/constants";
import { paramsInQuery, updateQuery } from "#app/utils/routes";
import { Select, Tag, Tooltip } from "antd";
import * as React from "react";
import { useHistory, useLocation } from "react-router-dom";
import { Button } from "../components/Button";
import { Drawer } from "../components/Drawer";
import { Row, Sheet } from "../components/Sheet";
import { Paragraph, Text } from "../components/Typography";
import { useFormState } from "../form/hooks";
import { Action, any, match, useActions } from "../iam/model";
import { icns } from "../icons";
import { fullnameOf } from "../intl/model";
import { MenuItem } from "../scaffold/MenuItem";
import { DrawerContext, actionDrawerParam } from "../scaffold/Scaffold";
import { Topbar } from "../scaffold/Topbar";
import { TenantLabel } from "../tenant/Label";
import { UserGeneralForm } from "./GeneralForm";
import { userIcon } from "./constants";
import { User, nameOf, useUser } from "./model";
import { useLogged, useLogin, useUserStore } from './store';
import './styles.scss';
import { useUserValidation } from './validation';





export const userprofileId = "uprofile"
export const editProfileParam = "upedit-drawer"

export const UserProfileItem = () => {

    const t = useT()

    return <MenuItem style={{ fontWeight: "bold" }} icon={userIcon} title={t("user.profile.name")} onClickToggle={userprofileId} />

}

export const UserProfile = () => {

    const t = useT()

    const history = useHistory()
    const { pathname, search } = useLocation()

    const editdrawer = !!paramsInQuery(search)[editProfileParam]

    // const {logged,setLogged,save} = useUsers()
    const logged = useLogged()
    const intl = useIntl();

    const ctx = React.useContext(DrawerContext)

    const toggleEditDrawer = (v: boolean) => history.push(`${pathname}?${updateQuery(search).with(p => p[editProfileParam] = v ? 'true' : null)}`)

    const effectivePermissionCoalesced = useEffectivePermissionCoalesced()

    // const [saving,setSaving] = React.useState<String | undefined>(undefined)

    return <Drawer routeId={editdrawer ? 'inactive' : actionDrawerParam}
        visible={ctx.isVisible(userprofileId)}
        onClose={() => {

            ctx.close(userprofileId)

        }}
        destroyOnClose={true}
        width={400}
        icon={userIcon}
        title={`${t("user.profile.name")}`}
        className={editdrawer ? 'translate-user-menu-further' : ''}>


        <Sheet>

            <Row label={`${t("user.profile.fields.details")}`}>
                <Paragraph><Text strong>{nameOf(logged)}</Text></Paragraph>
                <Paragraph><Text smaller secondary>({logged.username})</Text></Paragraph>
                <Paragraph><Text small secondary>{logged.details.email}</Text></Paragraph>
                <Button style={{ marginTop: 5 }} size="small" icn={icns.edit} onClick={() => toggleEditDrawer(true)}>{`${t("common.buttons.edit")}`}</Button>
                {/* <Button disabled style={{marginLeft:5}} size="small" iconLeft icn={icns.logout}>{`${t("user.profile.buttons.logout")}`}</Button> */}
            </Row>

            <Row label={t(tenantSingular)}>
                <TenantLabel tenant={logged.tenant} />
            </Row>

            <Row label={`${t("user.profile.fields.ican")}`}>
                {effectivePermissionCoalesced(logged).map((action, i) => <PermissionEntry action={action} key={i} actionkey={i} />)}
            </Row>

            <Row label={`${t("common.fields.language.name")}`}>

                <Select style={{ width: 150 }} size="small"
                    value={intl.currentLanguage() || intl.defaultLanguage()}
                    onChange={intl.setCurrentLanguage}>
                    {
                        intl.languages().map((l, i) => <Select.Option key={i} value={l}>{icns.language}&nbsp;&nbsp;{t(fullnameOf(l))}</Select.Option>)
                    }
                </Select>
            </Row>

            <Row label={t('mail.singular')}>
                {useDevMail()}
            </Row>

        </Sheet>


        {editdrawer && <EditDrawer visible onClose={() => toggleEditDrawer(false)} />}

    </Drawer>



};

type EditDrawerProps = {

    onClose: () => void
    visible: boolean

}


const PermissionEntry = ({ action, actionkey }: { action: Action, actionkey: number }) => {


    const t = useT()


    return <Paragraph style={{ marginTop: actionkey === 0 ? 0 : 5 }}>
        <Tooltip title={`${t(action.description)} ${action["group"] ? `${action["group"].join(",")}` : ""}`}>
            <Tag key={actionkey} className={action["fromRole"] ? "role" : ""}>
                {action.icon}
                &nbsp;&nbsp;{t(action.name)} {action.resource ? (action.resource === any ? `(${t("common.labels.all")})` : "(" + (action["group"].length) + ")") : ""}
            </Tag>
        </Tooltip>
    </Paragraph>

}


const EditDrawer = (props: EditDrawerProps) => {

    const { onClose, visible } = props

    const t = useT()

    const logged = useLogged()

    const login = useLogin()

    const { save } = useUserStore()

    const { validateProfile } = useUserValidation();

    const formstate = useFormState(logged as User)

    const { edited, dirty, reset } = formstate

    const report = validateProfile(formstate)

    const revert = <Button icn={icns.revert} enabled={dirty} type="danger" onClick={() => reset()}>Revert</Button>
    const saveact = <Button icn={icns.save} enabled={dirty} disabled={report.errors() > 0} dot={report.errors() > 0}
        type="primary"
        onClick={() => save(edited, ([saved]) => { login.setLogged(saved); reset(saved, false) })}>
        Save
    </Button>

    return <Drawer routeId={editProfileParam}
        onClose={onClose} warnOnClose={dirty}
        visible={visible}
        width={600}
        icon={icns.edit}
        title={`${t("user.profile.edit")}`}>

        <Topbar autoGroupButtons={false}>
            {saveact}
            {revert}
        </Topbar>

        <UserGeneralForm {...formstate} isNew={false} report={report} profileOnly />

    </Drawer>

}


//  computes effective actions and groups the non-templates
const useEffectivePermissionCoalesced = () => {

    const withUser = useUser()
    const actions = useActions()

    return ((logged: User) => {
        // refactoring patch: user has been loaded before actions were registered, so they won't have been interned.
        return withUser(logged).effectivePermissions().map(actions.intern).reduce((acc, next) => {

            // leaves templates untouched
            if (!next.resource || next.resource === any)
                return [next, ...acc]

            // find a previous match to coalesce
            const found = acc.find(a => match(a, next))

            // add to the group
            if (found) {
                found["group"] = [...found["group"] ?? [], next.resource]
                return acc
            }
            //  create the group
            else {
                next["group"] = [next.resource]
                return [next, ...acc]
            }
        }, [] as Action[])
    })

}