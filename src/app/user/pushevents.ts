import { useAuth } from '#app/call/auth';
import { pushLogoutTopic } from '#app/call/constants';
import { PushLogoutEvent } from '#app/call/Logout';
import { PushEventSlot } from '#app/push/module';
import { User } from './model';
import { useLogin } from './store';

export type UserChangeEvent = {

    user: User
    type: 'add' | 'remove' | 'change'

}


export const usePushUserSlot = (): PushEventSlot<any> => {

    const login = useLogin()
    const auth = useAuth()
    
    return {


        onSubcribe: () => {


            console.log("subscribing for user changes...")

            return [


                {


                    topics: [pushLogoutTopic]

                    ,

                    onEvent: (event: PushLogoutEvent) => {

                        console.log(`received force logout event...`)

                        if (event.users.includes(login.logged().username))
                            auth.logoutAndInitialLogin({ askConsent: true, keepPath: false })


                    }

                }]
        }
    }
}