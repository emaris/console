
import { Icon } from "antd"
import * as React from "react"
import { AiOutlineUser } from "react-icons/ai"


export const userSingular = "user.module.name_singular"
export const userPlural =  "user.module.name_plural"


export const userType = "user"
export const userIcon = <Icon component={AiOutlineUser} />
export const userRoute="/user"


