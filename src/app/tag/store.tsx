import * as React from "react";
import { useL } from "../model/multilang";
import { capitalise } from "../utils/common";
import { error } from "../utils/validation";
import { TagContext } from './context';
import { noCategory, noRef, noTag, Tag, TagCategory } from "./model";



export const useTagStore = () => {

  const state = React.useContext(TagContext)

  const l = useL()

  
  const self = {

    allTags: () => state.get().tags.allTags

    ,

    allCategories: () => state.get().tags.allCategories

    ,

    areTagsReady: () => !!self.allTags()

    ,

    areCategoriesReady: () => !!self.allCategories()

    ,

    lookupTag: (tag: string | Tag | undefined): Tag =>

      typeof tag === 'string' ? self.allTags().find(t => t.id === tag) ?? noTag(tag) : tag ?? noTag(undefined!)


    ,

    lookupCategory: (category: string | undefined) => self.allCategories().find(t => t.id === category) ?? noCategory

    ,

    setAllTags: (tags: Tag[]) => state.set(s => s.tags.allTags = tags)
 
    ,

    setAllCategories: (categories: TagCategory[]) => state.set(s => s.tags.allCategories = categories)

    ,


    allTagsOf: (type: string) => self.allTags().filter(t => t.type === type)

    ,

    allTagsOfCategory: (ref: string) => self.allTags().filter(tag => tag.category === ref)

    ,

    isCategoryActive: (cat: TagCategory) => cat.lifecycle.state === 'active' && self.allTagsOfCategory(cat.id).some(self.isTagActive)

    ,

    isTagActive: (tag: Tag) => tag.lifecycle.state === 'active'

    ,

    allCategoriesOf: (type: string) => self.allCategories().filter(c => c.type === type)

    ,

    validateCategories: (tagrefs: string[] = []) => {


      const validate = (...catrefs: (TagCategory | string)[]) => {

        // lookup those in some category
        const tags = tagrefs.map(self.lookupTag)

        const categories = catrefs.map(c => typeof c === 'string' ? self.lookupCategory(c) : c)

        // { category.id -> validation}
        return categories.filter(c => self.isCategoryActive(c)).map(cat => {

          const tagsincat = tags.filter(t => t.category === cat.id)

          const { min, max } = cat.cardinality

          const suffix = cat.properties.field?.enabled ? '' : ` for '${capitalise(l(cat.properties.field?.title ?? cat.name))}'`

          // tailore cardinality failure message .
          const failure = (min && tagsincat.length < min && (min === 1 ? `Make a choice${suffix}.` : `Choose at least ${min}${suffix}.`))
            || (max && tagsincat.length > max && (max === 1 ? `Choose only one${suffix}.` : `Too many, choose pat most ${max}${suffix}.`))

          return { [cat.id]: failure ? error(failure) : { status: "success" } }


        }).reduce((acc, next) => ({ ...acc, ...next }), {}) // merges objects

      }

      return { for: (type: string) => validate(...self.allCategoriesOf(type)), include: validate }


    }

    ,


    //  groups all known tags by category, but - unless the field is to be explicitly ignored - 
    //  the tags of categories that don't map to fields are put in the no-category group.
    allTagsByCategory: (type: string, ignoreField?: boolean) => {

      // prelim indexing by category
      const catmap: { [key: string]: Tag[] } = self.allTagsOf(type)?.reduce((acc, next) => {

        const cat = next.category && (ignoreField || self.lookupCategory(next.category).properties.field?.enabled) ? next.category : noRef

        return { ...acc, [cat]: [next, ...(acc[cat] ?? [])] }

      }, {})


      return [...self.allCategoriesOf(type), noCategory].map(c => ({ category: c, categoryTags: catmap[c.id] }))

    }


  }

  return self;

}