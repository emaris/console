import { useT } from '#app/intl/api';
import { usePreload } from 'apprise-frontend-core/client/preload';
import { useToggleBusy } from "../system/api";
import { through } from "../utils/common";
import { showAndThrow } from "../utils/feedback";
import { categoryApi, tagApi, useTagcalls } from "./calls";
import { categoryPlural, tagPlural, tagType } from "./constants";
import { Tag, TagCategory } from "./model";
import { useTagStore } from './store';



export const useTagClient = () => {

  const t = useT()
 
  const toggleBusy = useToggleBusy()

  const store = useTagStore()
  const call = useTagcalls()

  const plural = t(tagPlural).toLowerCase()
  const pluralCategory = t(categoryPlural).toLowerCase()

  const preload = usePreload()


  const self = {

    
    areTagsReady: () => !!store.allTags()

    ,

    areCategoriesReady: () => !!store.allCategories()

    ,

    fetchAllTags: (forceRefresh = false): Promise<Tag[]> =>

      self.areTagsReady() && !forceRefresh ? Promise.resolve(Object.values(store.allTags()).flatMap(v => v))

        :

        toggleBusy(`${tagType}.fetchAll`, t("common.feedback.load", { plural }))

          .then(_ => console.log(`fetching tags...`))
          .then(_ =>preload.get<Tag[]>(tagApi) ?? call.fetchAllTags())
          .then(through(store.setAllTags))


          .catch(e => showAndThrow(e, t("common.calls.fetch_all_error", { plural })))
          .finally(() => toggleBusy(`${tagType}.fetchAll`))

    ,

    fetchAllCategories: (forceRefresh = false): Promise<TagCategory[]> =>

      self.areTagsReady() && !forceRefresh ? Promise.resolve(Object.values(store.allCategories()).flatMap(v => v))

        :

        toggleBusy(`${tagType}.fetchAllCategories`, t("common.feedback.load", { plural: pluralCategory }))

          .then(_ => console.log(`fetching tags categories...`))
          .then(_ => preload.get<TagCategory[]>(categoryApi) ?? call.fetchAllCategories())
          .then(through(store.setAllCategories))

          .catch(e => showAndThrow(e, t("common.calls.fetch_all_error", { plural: pluralCategory })))
          .finally(() => toggleBusy(`${tagType}.fetchAllCategories`))


  }

  return self;

}