import { updateQuery } from "#app/utils/routes"
import { Tooltip } from "antd"
import * as React from "react"
import { Link, useLocation } from "react-router-dom"
import TruncatedList from "react-truncate-list"
import { Label, LabelProps, UnknownLabel } from "../components/Label"
import { useLocale } from "../model/hooks"
import { categoryParam, categoryTypeParam, tagIcon, tagType } from "./constants"
import { Tag, TagCategory, noCategory, useTagModel } from "./model"
import { useTagStore } from './store'
import "./styles.scss"


type Props = LabelProps & {

    tag: Tag | string | undefined
    displayMode?: 'default' | 'list'

}

export const TagLabel = (props: Props) => {

    const { search } = useLocation()
    const { l } = useLocale()
    const { lookupTag, lookupCategory } = useTagStore()

    return React.useMemo(() => {

        const { displayMode = 'default', noIcon, tag, linkTo, mode = 'tag', className, strikethrough, disabled, ...rest } = props

        if (!tag)
            return <UnknownLabel />

        const resolved = lookupTag(tag)
        const category = lookupCategory(resolved.category)

        const tagDisabled = resolved.unknown || resolved.lifecycle.state === 'inactive' || category?.lifecycle.state === 'inactive'
        const tagGuarded = resolved.guarded || category?.guarded


        if (displayMode === 'default') {

            return <Label className={`tag-label ${className ?? ''}`} mode={mode}

                linkTarget={tagType} icon={tagIcon} title={l(resolved.name)}
                tip={l(resolved.description)?.length > 0 ? () => l(resolved.description) : undefined}
                disabled={tagDisabled} isProtected={tagGuarded} noIcon={noIcon}
                {...rest}  />

        }
        else return <span className={`tag-label ${className ?? ''} ${tagDisabled && 'disabled'} ${strikethrough && 'strike-through'}`}>
            {l(resolved.description)?.length > 0 ? <Tooltip title={() => l(resolved.description)}>{l(resolved.name)}</Tooltip> : l(resolved.name)}
        </span>


        // eslint-disable-next-line 
    }, [props.tag, search])      // depndency on search here is due to a particular combination of tag labels appearing in slides and how the slider routing works.
}

type LightProps = {

    tag: string | Tag
    strikeThrough?: boolean
    border?: boolean
}

export const TagLabelLight = (props: LightProps) => {

    const { tag, strikeThrough, border = true } = props

    const { l } = useLocale()
    const { lookupTag, lookupCategory } = useTagStore()

    const resolved = lookupTag(tag)
    const category = lookupCategory(resolved.category)

    const disabled = resolved.unknown || resolved.lifecycle.state === 'inactive' || category?.lifecycle.state === 'inactive'

    const markedClasses = strikeThrough ? 'label-strikethrough' : disabled ? 'disabled' : ''

    const classNames = border ? ` tag-label label-tag ant-tag ${markedClasses}` : markedClasses

    return <span className={classNames}>
        {
            l(resolved.description) ?

                <Tooltip title={() => l(resolved.description)}>{l(resolved.name)}</Tooltip>

                :

                l(resolved.name)
        }</span>
}


type CategoryProps = LabelProps & {

    category: TagCategory | string | undefined

}

export const CategoryLabel = (props: CategoryProps) => {

    const { pathname, search } = useLocation()
    const { l } = useLocale()
    const { lookupCategory } = useTagStore()

    const { category, className, mode = 'tag', linkTo, disabled, ...rest } = props

    const resolved = (category ? typeof category === 'string' ? lookupCategory(category) : category : noCategory)

    const route = linkTo ?? (() => `${pathname}?${updateQuery(search).with(params => { params[categoryParam] = resolved.id; params[categoryTypeParam] = null })}`)

    return <Label
        noMemo
        isProtected={resolved.guarded}
        className={`category-label ${className ?? ''}`}
        tip={l(resolved.description) ? () => l(resolved.description) : undefined}
        linkTo={route}
        linkTarget={tagType}
        mode={mode}
        icon={tagIcon}
        title={l(resolved.name)}
        disabled={disabled || resolved.lifecycle.state === 'inactive'}
        {...rest} />

}


type ListProps = {

    taglist: (string | Tag)[]
    noTruncate?: boolean
    light?: boolean,
    noStretch?: boolean
    truncateEllipsisLink?: string
    renderProps? : LabelProps

}

export const TagList = (props: ListProps) => {
    const { taglist: unorderedTags = [], light= true, noTruncate = false, noStretch, truncateEllipsisLink, renderProps } = props

    const tagmodel = useTagModel()
    const tagstore = useTagStore()
    const alltags = tagstore.allTags()

    // eslint-disable-next-line
    const taglist = React.useMemo(() => [...unorderedTags].map(t => typeof t === 'string' ? tagstore.lookupTag(t) : t).sort(tagmodel.comparator), [unorderedTags,alltags])

    const children = taglist.map((tag, i) => light ? <TagLabelLight {...renderProps} key={i} tag={tag} /> : <TagLabel {...renderProps} key={i} tag={tag} />)

    const childrenArray = React.Children.toArray(children)


    return <span className="taglist-container" style={{width: noStretch ? 'auto' :'100%'}}>{!noTruncate ?
        <TruncatedList
            className="truncated-list"
            renderTruncator={({ hiddenItemsCount }) => {
                const itemCount = truncateEllipsisLink ? <Link to={truncateEllipsisLink}>{`+${hiddenItemsCount}`}</Link> : `+${hiddenItemsCount}`
                return <Tooltip overlayClassName="taglist-overflow" title={childrenArray.slice(childrenArray.length - hiddenItemsCount)}><div className="listItem">{itemCount}</div></Tooltip>
            }}
        >{children}</TruncatedList>
        :
        children}</span>
}
