import { OptionMenu } from '#app/components/OptionMenu';
import { useT } from '#app/intl/api';
import { useFilterState } from '#app/utils/filter';
import * as React from 'react';
import { useMemo } from 'react';
import { tagIcon, tagPlural } from './constants';
import { TagLabelLight } from './Label';
import { Tagged, useTagModel } from './model';
import { useTagStore } from './store';




export type TagFilterProps<T = any> = {

        tagged?: T[]
        filtered: T[]
        tagsOf: (_: T) => string[] | undefined
        placeholder?: React.ReactNode
        excludeCategories?: string[]
        visible?: boolean
        key: string,
        group:string

}

export const useTagFilter = <T extends Tagged>(props: Omit<TagFilterProps<T>, 'tagsOf'>) => {

        const t = useT()

        return useTagHolderFilter({ ...props, tagsOf: t => t.tags, placeholder: t(tagPlural)})

}

// computes the range of tags used a by a set of tagged objects and turns them into options for an <OptionMenu>.
// computes a filter from the selected options and applies it to the same objects or another set of objects (filtered).
// options are recomputed whenver the the objects change, and the filter is reapplied when the objects or the selection changes.
export const useTagHolderFilter = <T extends any = any>(props: TagFilterProps<T>) => {

        const { filtered = [], tagged = filtered, tagsOf, placeholder, key='tags', group, excludeCategories=[] } = props

        const tagstore = useTagStore()
        const tagmodel = useTagModel()

        const ctx = useFilterState(group)

        // computes distinct tags from population.
        const distinctTags = useMemo(() => {

                const alltags = tagged.flatMap(r => tagsOf(r) ?? [])
                        .filter((v, i, a) => a.indexOf(v) === i)
                        .map(tagstore.lookupTag)
                        .filter(tag => tag.category === undefined || !excludeCategories.includes(tag.category))

                alltags.sort(tagmodel.comparator)

                return alltags
        }
                // eslint-disable-next-line
                , [tagged])                             // recompute on data change.




        const selected = ctx.get(props.key) ?? distinctTags


        const TagFilter = (

                <OptionMenu id={t => t.id} selected={selected} setSelected={ctx.set(key)} placeholderIcon={tagIcon} placeholder={placeholder}>
                        {distinctTags.map(tag => <OptionMenu.Option key={tag.id} value={tag} label={<TagLabelLight tag={tag} />} />)}
                </OptionMenu>
        )


        // filter function
        const tagfilter = React.useCallback(

                (o: T) => (tagsOf(o) ?? []).length === 0 || selected.some(t => tagsOf(o)?.includes(t.id))

                // eslint-disable-next-line
                , [selected])  // recompute when selection changes

        // filtered data
        const tagFilteredData = useMemo(

                () => filtered.filter(tagfilter),

                [filtered, tagfilter])    // recompute on data or filter change


        return { TagFilter, tagfilter, tagFilteredData }
}