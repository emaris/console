

import { useConfig } from '#app/config/state';
import { useT } from '#app/intl/api';
import { check, checkIt, duplicateWith, empty, notdefined, requireLanguages, selectedFieldType, uniqueLanguages, withReport } from "../utils/validation";
import { categoryPlural, tagPlural } from "./constants";
import { Tag, TagCategory } from "./model";
import { useTagStore } from './store';

export type TagValidation = ReturnType<typeof useTagValidation>

export const useTagValidation = () => {
    
    const t = useT()
    
    const config = useConfig()
    const store = useTagStore()
    
    return {

        validateTag : (edited:Tag) => { 

            const requiredLangs = config.get().intl.required || [];
            const plural= t(tagPlural).toLowerCase()
            
            const otherCodesInCategory = (tag: Tag) : string[] => {
                if (tag.category === undefined) return []
                const allTagsInCategory = store.allTagsOfCategory(tag.category)

                return allTagsInCategory.filter(t => t.code !== undefined && t.id !== tag.id).map(t => t.code!)
            }
    
        
            return withReport({

                active: checkIt().nowOr(t("common.fields.active.msg"),t("common.fields.active.help",{plural}))
                
                ,
        
                name: check(edited.name).with(notdefined(t))
                                        .with(requireLanguages(requiredLangs,t))
                                        .nowOr(
                                            t("common.fields.name_multi.msg"),
                                            t("common.fields.name_multi.help",{plural,requiredLangs})
                ),

                
                description: check(edited.description).nowOr(
                     t("common.fields.description_multi.msg"),
                        t("common.fields.description_multi.help",{plural}),
                )
                
                ,


                value: check(edited.properties.value).nowOr(t("tag.fields.value.msg"),t("tag.fields.value.help"))
                
                , 

                code: check(edited.code).with(duplicateWith(otherCodesInCategory(edited), t)).nowOr(t("tag.fields.code.msg"),t("tag.fields.code.help"))
        
        })

        },

        validateCategory : (edited:TagCategory) => { 

            const categories = store.allCategoriesOf(edited.type);
            const requiredLangs = config.get().intl.required || [];
            const plural= t(categoryPlural).toLowerCase()
        
            return withReport({
        
                active: checkIt().nowOr(t("common.fields.active.msg"),t("common.fields.active.help",{plural}))
                
                ,

                color: check(edited.properties.color).with(empty(t)).nowOr(t("common.fields.color.msg"),t("tag.fields.color.help"))
        
                ,

                name: check(edited.name).with(notdefined(t))
                                        .with(requireLanguages(requiredLangs,t))
                                        .with(uniqueLanguages(categories.filter(t=>t.id!==edited.id).map(t=>t.name),t))
                                        .nowOr(
                                            t("common.fields.name_multi.msg"),
                                            t("common.fields.name_multi.help",{plural,requiredLangs})
                )
                
                ,
                
                description: check(edited.description).nowOr( t("common.fields.description_multi.msg"),t("common.fields.description_multi.help",{plural}),
                )
                
                ,

                cardinality: check(edited.cardinality).with(notdefined(t)).nowOr(t("tag.fields.cardinality.msg"),t("tag.fields.cardinality.help"))
                
                ,
        
                enabled: checkIt().nowOr(t("tag.fields.showas.msg"),t("tag.fields.showas.help"))

                ,

                fieldtype: check(edited).with(selectedFieldType(t)).nowOr(t("tag.fields.type.msg"),t("tag.fields.type.help"))
                
                ,
        
                title:  checkIt().nowOr(t("common.fields.label.msg"),t("tag.fields.title.help"))
                
                ,
                
                message: checkIt().nowOr(t("tag.fields.msg.msg"),t("tag.fields.msg.help"))
                
                ,

                help:   checkIt().nowOr(t("tag.fields.help.msg"),t("tag.fields.help.help"))

        })
        
        
        }

    }

}
  

