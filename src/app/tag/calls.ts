
import { useCalls } from "../call/call";
import { Tag, TagCategory } from "./model";

const svc="admin"
export const tagApi = "/tag"
export const categoryApi = "/tagcategory"


export const useTagcalls = () => {

    const {at} = useCalls()
   
 
    return ({

        fetchAllTags: () =>  at(`${tagApi}`,svc).get<Tag[]>()
        
        , 
       
        addTag: (tag:Tag)  =>  at(tagApi,svc).post<Tag>(tag)
        
        ,  

        updateTag: (tag:Tag)  =>  at(`${tagApi}/${tag.id}`,svc).put<Tag>(tag)
        
        ,  

        deleteTag: (tag:Tag)  =>  at(`${tagApi}/${tag.id}`,svc).delete()
        
        ,  

        fetchAllCategories: ()  =>  at(`${categoryApi}`,svc).get<TagCategory[]>()
        
        , 
       
        addCategory: (category:TagCategory)  =>  at(categoryApi,svc).post<TagCategory>(category)
        
        ,  

        updateCategory: (category:TagCategory)  => at(`${categoryApi}/${category.id}`,svc).put<TagCategory>(category)
        
        ,  

        deleteCategory: (category:TagCategory)  =>  at(`${categoryApi}/${category.id}`,svc).delete()
        
          

    })
}