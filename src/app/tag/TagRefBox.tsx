import { Select } from "antd";
import { CardlistBox } from "#app/components/CardlistBox";
import { Field, FieldProps } from "#app/form/Field";
import { FieldRow } from "#app/form/FieldRow";
import { ReadonlyProps } from '#app/form/ReadonlyBox';
import { SelectBox } from "#app/form/SelectBox";
import { useLocale } from "#app/model/hooks";
import { ReadOnlyContext } from "#app/scaffold/ReadOnly";
import produce from "immer";
import * as React from "react";
import { CSSProperties, useState } from "react";
import { useT } from '#app/intl/api';
import { categorySingular, tagPlural } from "./constants";
import { CategoryLabel, TagLabel } from "./Label";
import { ExpressionTerm, TagCategory, TagExpression, useTagModel } from "./model";
import { useTagStore } from './store';

type Props = FieldProps & ReadonlyProps & {

    id?: string
    mode?: 'multi' | 'single'

    type? : string
    categories?: TagCategory[]

    expression: TagExpression | undefined
    onChange: (_:TagExpression) => void

    className?: string
    style?: CSSProperties

} 

export const TagRefBox = (props:Props) => {

    const tags = useTagStore()

    const {id="tagrefbox", mode, expression={terms:[]},onChange,type,categories=type? tags.allCategoriesOf(type).filter(c=>c.lifecycle.state==='active') : [], fieldClassName=''
    
        , ...rest} = props

    const [terms,setTerms] = useState<number>(Math.max(expression.terms.length,1))

    const change = (updater: (_:TagExpression)=>void) => onChange(produce(expression,updater)!)

    return mode==='multi' && categories.length>1 ? 

                <CardlistBox id={id} singleLabel="" fieldClassName={`tagrefbox ${fieldClassName}`} {...rest} 
                        disabled={props.disabled}
                        addIf={terms===expression.terms.length && terms<categories.length} 
                        removeIf={index => terms>1 && index<terms}

                        onAdd={()=>setTerms(t=>++t)} 
                        onRemove={index=>{ 
                                
                            Promise.resolve(setTerms(t=>--t)).then(_=>change(e => { e.terms.splice(index,1) } )) 
                            
                        } }>

                        { categories.slice(0,terms).map( (_,i)=> 

                                <RefRow key={i} {...props}  change={change} expression={expression} categories={categories} index={i} /> 
                        )}
                </CardlistBox>
    
            : 
                <Field fieldClassName={`tagrefbox ${fieldClassName}`} {...rest}>
                    <RefRow {...props} change={change} expression={expression} categories={categories} index={0} />
                </Field>
}


type RowProps = Props & {

    index: number

    expression: TagExpression
    categories: TagCategory[]

    change:  (updater: (_: TagExpression) => void) => void
}


const RefRow = (props:RowProps) => {


    const t = useT()
    const {l} = useLocale()


    const tagmodel = useTagModel()
    const tagstore = useTagStore()

    const {mode,change,index,expression,categories,disabled} = props

    const initialCategory = () => {

        if (categories.length===1)
            return categories[0]

        const ref = expression.terms[index]?.category 

        return ref ? tagstore.lookupCategory(ref) : undefined!
    }

    const readOnlyContext = React.useContext(ReadOnlyContext)

    const [category,selectCategory] = useState<TagCategory>(initialCategory)

    const categoryTags = category ? tagstore.allTagsOf(category.type).filter(t=>t.category===category.id).sort(tagmodel.comparator) : []

    return <FieldRow>

                { categories.length===1 ||

                    <SelectBox standalone grouped style={{minWidth:200,marginRight:2}}
                                placeholder={t("common.buttons.select_one",{singular:t(categorySingular).toLowerCase()})} 
                                readonly={expression.terms[index]?.tags.length > 0}
                                disabled={disabled  || props.disabled}  

                                getlbl={c=><CategoryLabel mode='light' noLink category={c}/>}
                                selectedKey={ category && category.id }  
                                lblText={cat => l(tagstore.lookupCategory(cat).name)}
                                onChange={cat=> 
                                    
                                    Promise.resolve(
                                                selectCategory(categories.find(c=>c.id===cat)!)
                                            )
                                        .then(_=>change(e=> { if (e.terms[index])
                                                                e.terms[index] = {category:cat,tags:[],op:undefined}
                                                            
                                        }))
                                
                                } >
                            
                            { categories.map(c=>c.id) }

                    </SelectBox>
                }

                {  expression.terms[index] && expression.terms[index]?.tags.length>0 && 
                
                    <Select dropdownStyle={{minWidth:80}} className="tag-combinator" showArrow={false} disabled={props.disabled || readOnlyContext} 
                        defaultValue="anyOf"
                        value={expression.terms[index].op || 'anyOf'}
                        onChange={ (val: ExpressionTerm['op']) => change(e=>{ e.terms[index] = {...e.terms[index],op:val} })}  >
                        <Select.Option  className="combinator-option" value="anyOf">{t("tag.ref.combine_any")}</Select.Option>
                        <Select.Option  className="combinator-option" value="allOf">{t("tag.ref.combine_all")}</Select.Option>
                        <Select.Option  className="combinator-option" value="noneOf">{t("tag.ref.combine_none")}</Select.Option>            
                    </Select>
                }

                <SelectBox standalone grouped style={{flexGrow:1}} allowClear={mode!=='multi'} 
                            placeholder={t("common.buttons.select_many",{plural:t(tagPlural).toLowerCase()})} 
                            
                            disabled={!category || props.disabled} 
                            getlbl={c=><TagLabel mode='light' noLink tag={c}/>}
                            selectedKey={ expression.terms[index]?.tags ?? [] } 
                            lblText={tag => l(tagstore.lookupTag(tag).name)}
                            onChange={ ts=> change( e=> { 
                                if (ts ===undefined || ts.length===0)
                                    e.terms.splice(index,1)
                                else
                                    e.terms[index] = {...e.terms[index],category:category.id,tags:ts,op:e.terms[index]?.op === undefined ? 'anyOf' : e.terms[index].op}
                                
                            }) } >
                        
                        { categoryTags.filter(t=>t.lifecycle.state === 'active' ? true : expression.terms[index]?.tags.includes(t.id)).map(t=>t.id) }

                </SelectBox>
                
                
                    

            </FieldRow>


}