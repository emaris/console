
import { Radio } from "antd"
import { RadioBox } from "#app/form/RadioBox"
import { ReadonlyProps, useReadonly } from '#app/form/ReadonlyBox'
import { useT } from '#app/intl/api'
import _, { partition } from "lodash"
import * as React from "react"
import { SelectBox } from "../form/SelectBox"
import { useLocale } from "../model/hooks"
import { capitalise } from "../utils/common"
import { ReportMethods } from "../utils/validation"
import { TagLabel, TagLabelLight } from "./Label"
import { cardinalities, noCategory, Tag, TagCategory, useTagModel } from "./model"
import { useTagStore } from './store'
import { useModuleRegistry } from '#app/module/registry'

type BoxsetProps = ReadonlyProps & {

    edited: string[] | undefined
    validation?: ReportMethods
    onChange: (_: string[]) => void
    categories?: TagCategory[]
    type?: string
    bare?: boolean
}

export const TagBoxset = (props: BoxsetProps) => {

    const { allTagsOf, allCategoriesOf, allTagsOfCategory } = useTagStore()
    const { categoryComparator } = useTagModel()

    const { type, categories = type ? allCategoriesOf(type).filter(c => c.lifecycle.state === 'active') : [] } = props

    const [fielded, other] = partition(categories, c => c.properties.field?.enabled)

    const otherRefs = other.map(c => c.id)

    const fieldedFiltered = fielded.filter(cat => {

        const tags = allTagsOfCategory(cat.id)
        const categoryIsActiveAndHasActiveTags = cat.lifecycle.state !== 'inactive' && tags.some(t => t.lifecycle.state !== 'inactive')
        const editedHasTagsInCategory = props.edited?.some(tag => tags.some(t => t.id === tag))

        return categoryIsActiveAndHasActiveTags || editedHasTagsInCategory

    })

    return <React.Fragment>
        {fieldedFiltered.sort(categoryComparator).map((c, i) =>
            <TagField key={i} {...props} category={c} categoryTags={allTagsOf(c.type).filter(t => t.category === c.id)} />)
        }
        {other.length > 0 && type &&
            <TagField key="common" {...props} category={noCategory} categoryTags={allTagsOf(type).filter(t => t.category === undefined || otherRefs.includes(t.category))} />}
    </React.Fragment>
}

type TagFieldProps = BoxsetProps & { 
    category: TagCategory, 
    categoryTags: Tag[] 
    bare?: boolean
}

const TagField = (props: TagFieldProps) => {

    const { onChange, disabled = false, edited = [], category, type = category.type, validation = {}, categoryTags, bare=false, ...rest } = useReadonly(props)

    const { l } = useLocale()
    const t = useT()

    const { lookupTag, lookupCategory } = useTagStore()
    const { comparator:tagComparator } = useTagModel()
    
    const mod = useModuleRegistry().get(type)

    const [singular, plural] = [t(mod?.nameSingular??'').toLowerCase(), t(mod?.namePlural??'').toLowerCase()]

    const editedtags = edited.map(lookupTag)

    // isolates the tags under editin that belong to this category
    const [editedInCategory, other] = partition(editedtags, t => category === noCategory ? t.category === undefined || !lookupCategory(t.category).properties.field?.enabled : t.category === category.id)

    const label = !bare && capitalise(t(l(category.properties.field?.title ?? category.name)))
    const msg = !bare && t(l(category.properties.field?.message ?? { en: `${t("common.buttons.select_one", { singular: t('common.labels.option') })}.` }))
    const help = !bare && t(l(category.properties.field?.help ?? category.description ?? { en: '<missing>' }), { singular, plural })

    // for validation: find outcome for current category, unless it is the no-category.
    // in the case, report the first error outcome of a non-fielded category, if any.

    const validationId = category === noCategory ? categoryTags.map(t => t.category)
        .map(lookupCategory)
        .filter(c => !c.properties.field?.enabled)
        .find(c => validation[c.id]?.status === "error")
        ?.id
        : category.id


    const categoryValidation = validationId ? validation[validationId] : {}

    // const augmentedCategoryTags = [...categoryTags, ...(editedInCategory.filter(t => t.unknown) ?? [])].sort((a, b) => {
    //     if (a.properties.value && b.properties.value) {
    //         if (a.properties.value < b.properties.value) return -1
    //         if (a.properties.value > b.properties.value) return 1
    //     } else if (a.properties.value && b.properties.value === undefined) {
    //         return 1
    //     } else if (a.properties.value === undefined && b.properties.value) {
    //         return -1
    //     }
    //     return l(a.name).localeCompare(l(b.name))
    // })

    const augmentedCategoryTags = [...categoryTags, ...(editedInCategory.filter(t => t.unknown) ?? [])].sort(tagComparator)

    const multi = _.isMatch(cardinalities.atLeastOne, category.cardinality) || _.isMatch(cardinalities.any, category.cardinality)

    const minCardinality = category.cardinality?.min ?? 0

    switch (category.properties.field?.type) {

        case 'radio':

            return <RadioBox {...rest} label={label} disabled={disabled}
                value={edited.find(t => augmentedCategoryTags.some(a => t === a.id))}
                onChange={(v: any) => onChange([...(v ? multi ? v : [v] : []), ...other.map(t => t.id)])}
                validation={{ msg, help, ...categoryValidation }}>
                {augmentedCategoryTags.filter(t => t.lifecycle.state === 'active' ? true : t.id === edited[0]).map((r, i) => <Radio key={i} value={r.id}><TagLabelLight border={false} tag={r} /></Radio>)}
            </RadioBox>

        default:

            return <SelectBox
                {...rest}
                key={category.id}
                label={label}
                disabled={disabled}
                allowClear={minCardinality === 0}
                inactive={category.lifecycle.state === 'inactive'}
                selectedKey={multi ? editedInCategory.map(t => t.id) : editedInCategory.map(t => t.id)[0]}
                getkey={t => t.id}
                getlbl={tag => <TagLabel mode='light' noLink tag={tag} />}
                lblText={tag => l(tag.name)}
                //  merges update for this category with other tags, if any
                onChange={(v: any) => onChange([...(v ? multi ? v : [v] : []), ...other.map(t => t.id)])}

                validation={{ msg, help, ...categoryValidation }}>


                {augmentedCategoryTags.filter(t => t.lifecycle.state === 'active' ? true : editedInCategory.includes(t))}

            </SelectBox>

    }

}
