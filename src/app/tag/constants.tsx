
import * as React from "react"
import { AiOutlineTags } from "react-icons/ai"
import { Icon } from "antd"
import { GoDot, GoSquare } from "react-icons/go"
export const tagType = "tag"
export const tagIcon = <Icon component={AiOutlineTags} />

export const tagLabelIcon = GoDot
export const categoryLabelIcon = GoSquare
export const timelinessLabelIcon = GoSquare

export const tagSingular = "tag.module.name_singular"
export const tagPlural = "tag.module.name_plural"

export const categorySingular = "category.module.name_singular"
export const categoryPlural = "category.module.name_plural"

export const categoryParam ="category-drawer"
export const tagParam ="tag-drawer"
export const tagTypeParam ="tagType"
export const categoryTypeParam ="categoryType"
