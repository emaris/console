
import { fallbackStateOver, State } from 'apprise-frontend-core/state/api'
import { createContext } from 'react'
import { Tag, TagCategory } from './model'


export type TagState = {

    tags: {
      allTags: Tag[];
      allCategories: TagCategory[];
    }
  
  }
  
  export const initialTags: TagState = {
  
    tags: {
  
      allTags: undefined!,
      allCategories: undefined!
  
    }
  
  }

  
  export const TagContext =createContext<State<TagState>>(fallbackStateOver(initialTags))