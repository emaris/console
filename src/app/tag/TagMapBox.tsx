import { CardlistBox } from "#app/components/CardlistBox"
import { ReadonlyProps } from "#app/form/ReadonlyBox"
import { ReportMethods, Validation } from "#app/utils/validation"
import React from "react"
import { CategoryBox } from "./CategoryBox"
import { TagBoxset } from "./TagBoxset"
import { TagCategory, TagMap, useTagModel } from "./model"
import { useTagStore } from "./store"

export type TagMapBoxProps = ReadonlyProps & {
    children: TagMap[] | undefined
    type?: string
    categories?: TagCategory[] | string[]
    onChange: (_: TagMap[] | undefined) => void
    label?: string
    className?: string
    singleLabel?: string
    validationReport?: ReportMethods
    validation?: Validation
}

export const TagMapBox = (props: TagMapBoxProps) => {

    const { type, categories: userCategories, children = [], onChange, validationReport, className, label, singleLabel, ...rest } = props


    const tagmodel = useTagModel()
    const tagstore = useTagStore()

    const categoryFilter = (cat: TagCategory) => cat.lifecycle.state === 'active' && cat.properties.field?.enabled

    const categories = userCategories ?
        userCategories.map(cat => typeof cat === 'string' ? tagstore.lookupCategory(cat) : cat as TagCategory).filter(categoryFilter) :
        type ?
            tagstore.allCategoriesOf(type).filter(categoryFilter)
            : []

    const sortedCategories = categories.sort(tagmodel.categoryNameComparator)

    const restOfCategories = sortedCategories.filter(cat => !children.some(child => child.category === cat.id))

    const onAdd = () => onChange([...children ?? [], { category: restOfCategories.length > 0 && restOfCategories[0].id } as TagMap])

    const onRemove = (i: number) => {
        const newMap = children.filter((_, idx) => idx !== i)
        onChange(newMap.length === 0 ? undefined : newMap)
    }

    const onCardChange = (tagmap: TagMap, index: number) => onChange((children).map((cat, i) => i === index ? tagmap : cat))

    const tagBoxes = children.map((row, i) =>
    
     <TagMapRow key={i} index={i} validation={validationReport} categories={sortedCategories.filter(cat => cat.id === row.category || !children.some(child => child.category === cat.id))} selected={row} onChange={onCardChange} />
    )

    return <CardlistBox
        id={'tagmap'}
        className={`tag-boxes ${className}`}
        validation={{help:"kkk"}}
        label={label}
        singleLabel={singleLabel!}
        disabled={sortedCategories.length === 0}
        onAdd={onAdd}
        onRemove={onRemove}
        addIf={children.length < sortedCategories.length} {...rest}>
        {tagBoxes}
    </CardlistBox>

}


type TagBoxProps = {
    categories: TagCategory[]
    selected: TagMap
    onChange: (map: TagMap, index: number) => void
    index: number
    validation?: ReportMethods
}

const TagMapRow = (props: TagBoxProps) => {

    const { categories, selected, onChange, index, validation } = props

    const tags = useTagStore()

    const [selectedCategory, selectedCategorySet] = React.useState(selected.category)

    const onCategoryChange = (category: string) => {
        selectedCategorySet(category)
        onChange({ category, tags: undefined! }, index)
    }

    return <div className="tag-box">
        <div className="selector">
            <CategoryBox
                onChange={onCategoryChange}
                type={''}
                categories={categories}
            >{selectedCategory}</CategoryBox>

        </div>
        <div className='field-box'>
            {selectedCategory && <TagBoxset bare validation={validation} onChange={(v) => onChange({ category: selectedCategory!, tags: v }, index)} edited={selected.tags} categories={[tags.lookupCategory(selectedCategory)]} />}
        </div>
    </div>


}