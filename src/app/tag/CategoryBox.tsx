
import { FieldProps } from "#app/form/Field"
import * as React from "react"
import { SelectBox } from "../form/SelectBox"
import { CategoryLabel } from "./Label"
import { TagCategory, useTagModel } from "./model"
import { useTagStore } from './store'

type BoxsetProps = FieldProps & {

    onChange: (_:string) => void
    categories?: TagCategory[]
    type?: string
    children: string | string[] | undefined

    allowClear?: boolean
    undefinedOption?: React.ReactNode

}

export const CategoryBox = ( props:BoxsetProps) => {

    const store = useTagStore()
    const model = useTagModel()

    const {type,categories, children, ...rest} = props

    const categoriesOrInferred = categories ?? (type ? store.allCategoriesOf(type).filter(c=>c.lifecycle.state==='active'): [])

    return <SelectBox key={`category-boxset-${type}`} {...rest}
                        selectedKey = {children}
                        getkey= {c=>c.id}
                        lblText={model.stringifyCategory}
                        getlbl={category=><CategoryLabel mode='light' noDecorations noLink category={category}/>}>
                {categoriesOrInferred}                                  
            </SelectBox>
}

