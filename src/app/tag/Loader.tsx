
import { useRenderGuard } from 'apprise-frontend-core/utils/renderguard'
import { PropsWithChildren } from 'react'
import { useTagClient } from './client'


export const TagLoader = (props:PropsWithChildren<{}>) => {

  const client = useTagClient()

  const {content} = useRenderGuard({
    when: client.areTagsReady(),
    render:props.children,
    orRun: () =>  Promise.all([client.fetchAllCategories(),client.fetchAllTags()]),
  })

  return content

}
