
import { Lifecycle } from "#app/model/lifecycle"
import { MultilangDto, newMultiLang, useCompareMultiLang, useL } from "../model/multilang"
import { index } from "../utils/common"
import { useTagStore } from './store'
import "./styles.scss"



export const tagseparator = "@"

export type Tag = {

    id: string
    type: string
    category?: string

    guarded?: boolean
    predefined?: boolean,

    lifecycle: Lifecycle<TagCategoryState>

    name: MultilangDto
    description: MultilangDto

    properties: any

    unknown?: boolean

    // codifies the underlying concept from some codelist of relevance in the application domain.
    code?: string

}

export type TagState = "active" | "inactive"


export type TagCategory = {

    id: string
    type: string
    cardinality: CategoryCardinality

    guarded?: boolean
    predefined?: boolean

    lifecycle: Lifecycle<TagCategoryState>


    name: MultilangDto
    description?: MultilangDto

    properties: CategoryProperties



}

export type TagCategoryState = "active" | "inactive"

export type TagCategoryField = {

    enabled: boolean
    type?: 'radio'
    title: MultilangDto
    message: MultilangDto
    help: MultilangDto
}

export type Tagged = {

    tags: string[]
}

export const newTagged = {

    tags: []
}

export type CategoryCardinality = { min: number, max: number | undefined }

export type CardinalityNames = 'any' | 'zeroOrOne' | 'one' | 'atLeastOne'

export const cardinalities: { [key in CardinalityNames]: CategoryCardinality } = {

    any: { min: 0, max: undefined },
    zeroOrOne: { min: 0, max: 1 },
    one: { min: 1, max: 1 },
    atLeastOne: { min: 1, max: undefined },


}

export const keyOf = (card: CategoryCardinality) => Object.keys(cardinalities).find(k => cardinalities[k].min === card?.min && cardinalities[k].max === card?.max)!

export type CategoryProperties = {


    color?: string
    field?: Partial<TagCategoryField>

}



export const noRef = 'noneOf'

export const noTag = (id: string): Tag => ({

    unknown: true,
    id: id,
    type: "unknown",
    name: { en: id },
    lifecycle: { state: 'active' } as Lifecycle<TagState>,
    description: { en: "unknown tag" },
    properties: {}

})

export const noCategory: TagCategory = {

    id: noRef,
    type: undefined as any,
    cardinality: cardinalities.any,
    name: { en: 'None' },
    lifecycle: { state: 'active' } as Lifecycle<TagCategoryState>,
    properties: {
        color: "black",
        field: {
            enabled: true, title: { en: 'common.fields.tags.name' },
            message: { en: 'common.fields.tags.msg' },
            help: { en: 'common.fields.tags.help' }
        }
    }
}



export type TagExpression = {

    terms: ExpressionTerm[]
}

export type ExpressionTerm = {

    category: string,
    tags: string[],
    op?: 'anyOf' | 'allOf' | 'noneOf'
}

export type TagMap = {
    category: string
    tags: string[]
}

export const tagRefsIn = (expr: TagExpression | undefined) => expr?.terms.flatMap(ts => ts.tags) ?? []


// stateless part
export const withTags = {

    newTag: (type: string): Tag => {
        return ({
            id: undefined!,
            type,
            lifecycle: { state: 'inactive' } as Lifecycle<TagState>,
            name: newMultiLang(),
            description: newMultiLang(),
            properties: {}
        })
    },

    newCategory: (type: string): TagCategory => ({
        id: undefined!,
        type,
        cardinality: cardinalities.any,
        lifecycle: { state: 'inactive' } as Lifecycle<TagCategoryState>,
        name: newMultiLang(),
        description: newMultiLang(),
        properties: {
            color: undefined,
            field: { enabled: true }
        }
    }),

    tagsByType: (tags: Tag[]) => index(tags).by(t => t.type),

    tagsByCategories: (tags: Tag[]) => index(tags.filter(t => t.category)).by(t => t.category),

    categoriesByType: (categories: TagCategory[]) => index(categories).by(c => c.type),

}


// stateful part
export const useTagModel = () => {

    const l = useL()

    const store = useTagStore()

    const ml = useCompareMultiLang()


    const self = {

        stringify: (t: Tag | undefined) => t ? `${l(t.name)}` : ''

        ,

        stringifyCategory: (t: TagCategory | undefined) => t ? `${l(t.name)}` : ''

        ,


        stringifyRefs: (ts: string[]) => ts?.map(store.lookupTag).map(self.stringify)

        ,

        given: (o: Tagged) => {


            const ts = o.tags.map(store.lookupTag)

            const api = {


                allWithCategory: (catRef: string) => ts.filter(t => t.category === catRef),
                allWithType: (type: string) => ts.filter(t => t.type === type),
                matches: (e: TagExpression | undefined) => !e || self.expression(e).matches(o)

            }

            return api
        }
        
        ,



        expression: (expr: TagExpression) => ({
            matches: (t: Tagged) => {


                const evaluateTerm = (term: ExpressionTerm) => {

                    const targets = self.given(t).allWithCategory(term.category).map(t=>t.id)
                    const matched = targets.filter(t => term.tags.includes(t))

                    let match;

                    switch (term.op) {

                        case 'noneOf': match = matched.length === 0; break
                        case 'anyOf': match = matched.length > 0; break
                        case 'allOf': match = matched.length === term.tags.length; break
                    }


                    //console.log("REMOVE ME AFTER","matching",t,"with term",term,"results in",match)

                    return match
                }


                return !expr.terms.map(evaluateTerm).some(r => r === false)

            }
        })


        ,

        isTagged: (o: any): o is Tagged => o?.tags

        ,

        comparator: (o1: Tag, o2: Tag) => {
            
            if (o1.properties.sort || o2.properties.sort) {
                const o1SortValue = o1.properties.sort ?? Number.MAX_VALUE
                const o2SortValue = o2.properties.sort ?? Number.MAX_VALUE

                if (o1SortValue === o2SortValue) return ml(o1.name, o2.name)

                return o1SortValue - o2SortValue

            }

            return ml(o1.name, o2.name)
}

        ,

        categoryComparator: (o1: TagCategory, o2: TagCategory) => l(o1.properties.field?.title ?? o1.name).localeCompare(l(o2.properties.field?.title ?? o2.name))

        ,

        categoryNameComparator: (o1: TagCategory, o2: TagCategory) => l(o1.name).localeCompare(l(o2.properties.field?.title ?? o2.name))

        ,

        biasedComparator: (bias: Tag) => (o1: Tag, o2: Tag) => l(bias.name) === l(o1.name) ?
            l(bias.name) === l(o2.name) ? 0 : -1
            :
            l(bias.name) === l(o2.name) ? 1 :
                self.comparator(o1, o2)


        ,


        ...withTags


    }

    return self

}