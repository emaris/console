import { pushEventType } from '#app/push/constants'
import { Module } from "../module/model"
import { usePushSettingsSlot } from '../settings/pushevents'
import { systemIcon, systemPlural, systemSingular, systemType } from "./constants"


export const useSystemModule = (): Module => {

   const pushSettingsSlot = usePushSettingsSlot()

   return {

      icon: systemIcon,
      type: systemType,

      nameSingular: systemSingular,
      namePlural: systemPlural,

      [pushEventType]: pushSettingsSlot

   }
}