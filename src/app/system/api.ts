import { useContext } from 'react';
import { SystemContext } from './context';


export const useToggleBusy = () => {

    const state = useContext(SystemContext)

    return (key: string, msg?: string) => state.set(s => s.busy[key] = { status: !s.busy[key]?.status, msg: msg })

}


export const useSystem = () => {

    const state = useContext(SystemContext)

    const self = {

        findBusy: () => Object.values(state.get().busy).find(e => e.status)

        ,

        updateBusy: (key: string, msg: string) => state.set(s => s.busy[key].msg = msg ? msg : msg)

    }

    return self
}