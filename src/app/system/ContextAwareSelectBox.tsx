
import * as React from 'react'
import { VSelectBox, VSelectBoxProps } from '../form/VSelectBox'
import { useLocale } from '../model/hooks'
import { Tag, useTagModel } from '../tag/model'
import { group } from '../utils/common'
import { useFacets } from './facets'



type Props = VSelectBoxProps & {

    singleContext?:boolean
    currentContext?: Tag|undefined
 
}


export const ContextAwareSelectBox = (props: Props) => {

    const {currentContext,singleContext,options, ...rest} = props

    const {l} = useLocale()
    
    const {contextOf} = useFacets()
    const {comparator,biasedComparator} = useTagModel()
    
    
    const groupoptions = ()=>group(options)
                                    .by(contextOf, currentContext ? biasedComparator(currentContext) : comparator)
                                    .map(({key:ctx,group})=>({label:l(ctx.name),options:group}))

    const filteroptions = ()=>options.filter(t=> contextOf(t)?.id === currentContext?.id)

     return  <VSelectBox {...rest} options={singleContext ? filteroptions() : groupoptions()} noVirtualisation>
                {props.children}
            </VSelectBox>
}