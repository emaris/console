import { useSettings } from '#app/settings/store'
import { useTagStore } from '#app/tag/store'
import { systemType } from "./constants"
"#app/settings/api"


export type SystemAppSettings = {

    [systemType] : {
        crosscontext:boolean,
        defaultContext: string,
        defaultRequirementImportance: string[],
        defaultProductImportance: string[]
    }
}

export const useSystemModel = () => {

    const tags = useTagStore()

    const settings = useSettings().get<SystemAppSettings>()[systemType] ?? {}    // lazy to avoid loops at this delicate junction.

    const self = {

        crossContext: ()=> settings.crosscontext,
        defaultContext: ()=> settings.defaultContext ? tags.lookupTag(settings.defaultContext) : undefined,

    }
    

    return self;
}