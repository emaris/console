import { FieldProps } from '#app/form/Field'
import { useT } from '#app/intl/api'
import * as React from 'react'
import { VSelectBox } from '../form/VSelectBox'
import { TagLabel } from '../tag/Label'
import { Tag } from '../tag/model'
import { useFacets } from './facets'
import { useSystemModel } from './model'



type ProxyProps = Partial<FieldProps> & {
    useAll? : boolean
    disabled? : boolean
    style?:React.CSSProperties
    size? : "small" | "default"
    standalone?: boolean
}

export type ConstextSelectorProps = ProxyProps & {
    selectedContext: Tag | undefined
    onChange: (context: Tag | undefined) => void

}

export const useContextSelector = (initialContext?: Tag) => {

    const system = useSystemModel()    

    const [currentContext, setCurrentContext] = React.useState<Tag | undefined>(initialContext || system.defaultContext() )
    
    const Proxy = React.useCallback((props: ProxyProps) => <ContextSelector  {...props} selectedContext={currentContext} onChange={setCurrentContext}  />, [currentContext])

    return [Proxy, currentContext, setCurrentContext] as [(_: ProxyProps) => JSX.Element, Tag | undefined, (_:Tag | undefined)=>void]
}

export const ContextSelector = (props: ConstextSelectorProps) => {
    
    const {style,size,disabled=false, selectedContext, onChange, useAll=false, standalone=true, ...rest} = props

    const t = useT()
    const {allContexts} = useFacets()    

    const allTemplates = t('system.categories.module.name_plural')

    const contexts = allContexts()

    if (contexts.length<2)
        return null

    const options = useAll ? [allTemplates, ...contexts] : contexts


    const defaultStyle = {width:250}

    return <VSelectBox light={standalone} style={{...defaultStyle, ...style}} {...rest}
        size={size}
        disabled={disabled}
        options={options}
        onChange={(cx: any) => onChange(cx === allTemplates ? undefined : cx)}
        renderOption={c => c === allTemplates ? c : <TagLabel noLink mode='light' tag={c} />} //TagLabel is light to grayout contexts in case of disabled VSelectBox
        optionId={c => c.id}>
        {[selectedContext ??  (useAll && allTemplates) ] }

    </VSelectBox>
}