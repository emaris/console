import { ConstextSelectorProps, ContextSelector } from '#app/system/ContextSelector'
import { Tagged } from '#app/tag/model'
import { useFilterState } from '#app/utils/filter'
import * as React from 'react'
import { useFacets } from './facets'

type ContextProps<T extends Tagged>  = Partial<ConstextSelectorProps> & {

    data:  T[]
    group:string
    key?:string
} 

export const useContextFilter = <T extends Tagged>  (props:ContextProps<T>) => {

    const {data,key="context", group, ...rest} = props

    const {contextOf} = useFacets()

    const filterContext = useFilterState(group)

    const currentContext = filterContext.get(key)

    const selector = <ContextSelector selectedContext={currentContext} onChange={filterContext.set(key)} useAll {...rest} />

    // eslint-disable-next-line
    const contextfilter = React.useCallback(t => !currentContext || (contextOf(t) ?? currentContext).id === currentContext.id, [currentContext])
    
    const contextFilteredData = React.useMemo(()=>data.filter(contextfilter),[data,contextfilter])

    return {ContextSelector:selector,currentContext,contextfilter,contextFilteredData}
}