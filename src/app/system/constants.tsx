import { icns } from "../icons"



export const systemSingular = "system.module.name_singular"
export const systemPlural = "system.module.name_plural"

export const systemType = "system"
export const systemIcon = icns.admin



export const contextCategory = "TGC-system-context"
export const defaultContext = "TG-system-defaultcontext"