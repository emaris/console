import { fallbackStateOver, State } from 'apprise-frontend-core/state/api';
import { createContext } from 'react';
import { initialSystemState, SystemState } from './state';




export const SystemContext = createContext<State<SystemState>>(fallbackStateOver(initialSystemState))

