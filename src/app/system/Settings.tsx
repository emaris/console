import { prodImportanceCategory, productType } from '#product/constants'
import { reqImportanceCategory, requirementType } from '#requirement/constants'
import { Form } from '#app/form/Form'
import { Switch } from '#app/form/Switch'
import { useT } from '#app/intl/api'
import { useTagStore } from '#app/tag/store'
import { TagBoxset } from '#app/tag/TagBoxset'
import * as React from 'react'
import { SettingsProps } from '../settings/model'
import { systemType } from './constants'
import { ContextSelector } from './ContextSelector'
import { SystemAppSettings } from './model'


export const SystemSettingsFields = (props:SettingsProps<SystemAppSettings>) => {

    const t = useT()
    const { lookupCategory, lookupTag } = useTagStore()


    const {state,report} =  props

    const {edited,change} = state

    const requirementCategory = lookupCategory(reqImportanceCategory)
    const productCategory = lookupCategory(prodImportanceCategory)
    

    return  <Form state={state} sidebar>
                 <Switch label={t("system.fields.cross_context.name")} onChange={change((t,v)=>t[systemType].crosscontext = v)} 
                         validation={report.crosscontext}>
                     {edited[systemType].crosscontext}
                </Switch>
                 
                 <ContextSelector label={t("system.fields.default_context.name")} 
                                  validation={report.defaultcontext} 
                                  standalone={false}
                                  selectedContext={edited[systemType].defaultContext ? lookupTag(edited[systemType].defaultContext) : undefined} 
                                  onChange={change((t,v)=>t[systemType].defaultContext = v ? v.id:v)} />

                <TagBoxset edited={edited[systemType].defaultRequirementImportance} onChange={change((t,v)=>t[systemType].defaultRequirementImportance = [...v])} type={requirementType} categories={[requirementCategory]} />
                <TagBoxset edited={edited[systemType].defaultProductImportance} onChange={change((t,v)=>t[systemType].defaultProductImportance = [...v])} type={productType} categories={[productCategory]} />
    
 
            </Form>

}