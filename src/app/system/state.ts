import { Tag } from "../tag/model"



export type SystemState = {

    busy: {[id:string]:{status:boolean,msg:string|undefined}}
    drawer: {[id:string]:{status:boolean,timestamp:number}}
    settings:{
        crosscontext:boolean,
        defaultContext: Tag | undefined,
    }
    session: { [id:string] : any}

}

export const initialSystemState: SystemState = {

    busy:{},
    drawer:{},
    settings:{
        crosscontext:true,
        defaultContext: undefined!
    },
    session: {}
}