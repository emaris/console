import { useTagStore } from '#app/tag/store';
import { useMultilang } from "../model/multilang";
import { Tagged, useTagModel } from "../tag/model";
import { contextCategory, systemType } from "./constants";


export const useFacets = () => {

    const tags ={ ...useTagModel(), ...useTagStore()}
    const ml =  useMultilang()

    const api = {


        contextComparator: (o1: Tagged, o2: Tagged) =>

            ml(api.contextOf(o1)?.name ?? { en: "" }).inCurrent().localeCompare(
                ml(api.contextOf(o2)?.name ?? { en: "" }).inCurrent()
            )
        ,

        allContexts: () => tags.allTagsOf(systemType).filter(t => t.category === contextCategory).sort(tags.comparator),

        contextOf: (o: Tagged) => tags.given(o).allWithCategory(contextCategory)[0],

    }

    return api
}