
import { useT } from '#app/intl/api';
import { useLogin } from '#app/user/store';
import { showFailure } from '#app/utils/feedback';
import { usePushEvents } from '../push/api';
import { useToggleBusy } from '../system/api';
import { User } from '../user/model';
import { through, wait } from '../utils/common';
import { notify, showAndThrow } from '../utils/feedback';
import { logoutBaseUrl, pushLogoutTopic } from './constants';




type LogoutProperties = {

    askConsent?: boolean
    keepPath: boolean
}

export const useAuth = () => {

    const user = useLogin()
    
    const toggleBusy = useToggleBusy()

    const push = usePushEvents()
    
    const t = useT()

    const self = {


        // initiates a login.
        logoutAndInitialLogin: (props: LogoutProperties) => {

            const {askConsent = user.isLogged(), keepPath } = props
            
            
            // the logout flow clears things up before starting a login flow.
            const logoutUrl = `${logoutBaseUrl}${keepPath? window.location.pathname : ''}`

            
            // if a user was previously logged, asks consent to re-authenticate and re-load the app (sesion expired).
            if (askConsent)
        
                showFailure({
                        title: t("common.auth.login_title"),
                        message: t("common.auth.login_message"),
                        okText: t("common.auth.login_btn"),
                        onOk: () => window.location.href = logoutUrl,
                        width: 550,
                        okCancel: false
           })
           

           // otherwise go ahead silently (first request in dev build, without api load ).
           else  window.location.href = logoutUrl
        }

        ,

        pushLogout : (...users:User[]) =>

            toggleBusy(`pushlogout`, t("auth.push_logout"))

                .then(() =>  push.publish(pushLogoutTopic,{users: users.map(u=>u.username)}))
                .then(wait(500))
                .then(through(() => notify(t('common.feedback.saved'))))

                .catch(e => showAndThrow(e, t("auth.push_logout_error")))
                .finally(() => toggleBusy(`pushlogout`))

        


    }

    return self;
}