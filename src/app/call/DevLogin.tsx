import { buildinfo } from '#app/buildinfo';
import * as React from "react";
import { useCalls } from './call';

// In dev builds the first login doesn't occur on app loads. but on the first api request that fails.
// We force it here by making the simplest backend call.
export const DevLogin = (props: React.PropsWithChildren<{}>) => {

    const calls = useCalls()

    const [logged, loggedSet] = React.useState<any>(!buildinfo.development)

   React.useEffect(() => {

        // make the call, if we haven't yet. if it fails for authentication, we'll log in and restart with an auth session (cookie).
        // then it won't fail again.
        if (!logged)
            calls.atPath('/console/config.json').get().then(()=>loggedSet(true))

        // eslint-disable-next-line
    }, [])

    return <>{logged ? props.children : null}</>


}