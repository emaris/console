

import { useT } from '#app/intl/api';
import { sessionKey, useClientSession } from 'apprise-frontend-core/client/session';
import axios, { AxiosRequestConfig, AxiosRequestHeaders, AxiosResponse } from "axios";
import { buildinfo } from "../buildinfo";
import { useConfig } from "../config/state";
import { useAuth } from './auth';
import { errorHandled } from './constants';

const axiosapi = axios.create({ timeout: 600000 });

export const useCalls = () => {

  const config = useConfig().get();

  const handleConfig = useHandleConfig()
  const handleError = useHandleError()


  const self = {


    fq: (path: string, targetName?: string) => {


      const target = targetName ? config.service(targetName) : config.defaultService();
      const prefix = target.prefix;
      return `/${prefix}${path}`;


    }

    ,

    //  use to call target services, path is prefixed.
    at: (path: string, targetName?: string) => self.atPath(self.fq(path, targetName)),


    //  use to fetch static resources, path is prefixed.
    staticAt: (path: string) => {
      return self.atPath(`${buildinfo.prefix}${path}`)
    },


    // use to call without prefixing
    atPath: (path: string) => ({

      rawget: <T>(config?: AxiosRequestConfig) => axiosapi.get<T>(path, handleConfig.with(config)).catch(handleError.with(path)),
      get: <T>(config?: AxiosRequestConfig) => axiosapi.get<T>(path, handleConfig.with(config)).catch(handleError.with(path)).then(r => r.data),
      post: <T,R=any>(data?: R, config?: AxiosRequestConfig<R>) => axiosapi.post<T,AxiosResponse<T>,R>(path, data, handleConfig.with(config)).catch(handleError.with(path)).then(r => r.data),
      put: <T,R=any>(data?: R, config?: AxiosRequestConfig) => axiosapi.put<T,AxiosResponse<T>,R>(path, data, handleConfig.with(config)).catch(handleError.with(path)).then(r => r.data),
      delete: (config?: AxiosRequestConfig) => axiosapi.delete<void>(path, handleConfig.with(config)).catch(handleError.with( path)).then(r => r.data)
    })

  }

  return self

}

const useHandleConfig = () => {

  const session = useClientSession()

  return {

    with: (config: AxiosRequestConfig | undefined) => {

      let headers: AxiosRequestHeaders = {...config?.headers ?? {}, [sessionKey]: session.get() ?? ''}

      return { ...config, headers };
    }
  }

}

const useHandleError = () => {

  const t = useT()

  const auth = useAuth() 

  return {

    with: (url: string) => (e: any) => {
      

      if (e.response) {


        // handles session expiry directly.
        //  if the response payload follows error conventions, do extract relevant parts
        //  otherwise use the bare message provided with the error.
        const baremsg = e.message
        e.message = t((e.response.data && e.response.data.message) || baremsg)
        e["details"] = (e.response.data && e.response.data.stacktrace) || e.response.data || baremsg // force details as a custom property of axios errors

        const loginRequired = e.response.status === 401

        // handles session expiry directly.
        if (loginRequired) {

          e[errorHandled] = true

          auth.logoutAndInitialLogin({ keepPath: true })

        }


      }
      else

        e.message = `${e.request ? t("calls.noresponse", { url }) : t("calls.norequest", { url })}: ${e.message}`

      throw e;

    }
  }
}