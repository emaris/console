import { Icon } from 'antd'
import * as React from 'react'
import { AiOutlineLogout } from 'react-icons/ai'

export const nosec = 'nosec'

export const errorHandled = "errorHandled"
export const logoutBaseUrl= '/oauth2/logout'

export const logoutIcon = <Icon component={AiOutlineLogout} />

export const pushLogoutTopic = 'logout'