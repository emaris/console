import { MenuItem } from '#app/scaffold/MenuItem';
import * as React from 'react';
import { useT } from '#app/intl/api';
import { useAuth } from './auth';
import { logoutIcon } from './constants';

export const LogoutItem = () => {
    
    const t = useT();
    const auth = useAuth()

    return <MenuItem icon={logoutIcon} title={`${t("common.auth.logout_message")}`}  onClick={()=>auth.logoutAndInitialLogin({askConsent: false, keepPath:false})} />

}

export type PushLogoutEvent = {

    users: string[]
}