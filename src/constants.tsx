import { buildinfo } from '#app/buildinfo'
import { AiFillAppstore, AiFillContainer, AiFillSafetyCertificate } from "react-icons/ai"
import { IconBaseProps } from 'react-icons/lib'

export const iotcRoute = "https://www.iotc.org/"

export const mcColor ="#0e9785"
export const McIcon =(props:IconBaseProps) => <AiFillAppstore size={22} color={mcColor} {...props} />
export const mcRoute = "/mc"


export const mclogo = <img alt="logo" src={`${buildinfo.prefix}/images/mclogo.png`}  className='app-logo'  height={200}  />
export const mcbanner = <img alt="banner" src={`${buildinfo.prefix}/images/mcbanner.png`}  className='appbanner' height={20} />

export const acColor =" #0895c3"
export const AcIcon = (props:IconBaseProps) => <AiFillSafetyCertificate size={22} color={acColor} {...props} />
export const acRoute = "/ac"

export const aclogo = <img alt="logo" src={`${buildinfo.prefix}/images/aclogo.png`}  className='app-logo'  height={200}  />
export const acbanner = <img alt="banner" src={`${buildinfo.prefix}/images/acbanner.png`}  className='appbanner' height={20} />


export const rcColor ="#0ca4b8"
export const RcIcon = (props:IconBaseProps) => <AiFillContainer size={22} color={rcColor} {...props} />
export const rcRoute = "/rc"

export const rclogo = <img alt="logo" src={`${buildinfo.prefix}/images/rclogo.png`}  className='app-logo'  height={200}  />
export const rcbanner = <img alt="banner" src={`${buildinfo.prefix}/images/rcbanner.png`}  className='appbanner' height={20} />


export const iotcLogo = <img alt="iotc" src={`${buildinfo.prefix}/images/iotc.png`}  height={24}  />


export const domainService = "domain"


