import { mailType } from '#app/mail/constants'
import { MailSlot } from '#app/mail/module'
import { Module } from "#app/module/model"
import { useModuleRegistry } from '#app/module/registry'
import { pushEventType } from '#app/push/constants'
import { allMessageMailTopic, messageIcon, messagePlural, messageSingular, messageType } from "./constants"
import { usePushMessageSlot } from './pushevents'
import { RouteResolver, TopicResolver, useMessageRegistry } from "./registry"
import "./styles.scss"





export const useMessageModule = (): Module => {

    const moduleRegistry = useModuleRegistry()
    const messageRegistry = useMessageRegistry()
    const pushMessageSlot = usePushMessageSlot()

    return {

        icon: messageIcon,
        type: messageType,

        nameSingular: messageSingular,
        namePlural: messagePlural,


        onInit: () => {

            const slots = moduleRegistry.allSlotsWith<MessageSlot>(messageType)

            messageRegistry.addResolvers(slots.flatMap(slot => slot.resolvers()))
            messageRegistry.addRouteResolvers(slots.flatMap(slot => slot.routes?.() ?? []))
        }

        ,


        [pushEventType]: pushMessageSlot,


        [mailType]: {

            topics: [{ key: 'message.mail.topic.all', value: allMessageMailTopic }]

        } as MailSlot
    }

}



export type MessageSlot = {


    resolvers: () => TopicResolver[],
    routes?: () => RouteResolver

}

