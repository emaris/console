import { fallbackStateOver, State } from 'apprise-frontend-core/state/api'
import { createContext } from 'react'
import { Flow, Message } from './model'
import { RouteResolver, TopicResolver } from './registry'

export type MessageState = {

    messages: {

        all: Record<string, FlowState>
    }
}


export type FlowState = {

    flow: Flow
    unreadCount: number,
    active: boolean
    notifications: Message[]

}

export const initialMessages = {

    messages: {

        all: {}

    }

}

export const MessageContext = createContext<State<MessageState>>(fallbackStateOver(initialMessages))


export type MessageRegistryState = {

    resolvers: { [type: string]: TopicResolver },
    routeResolvers: RouteResolver[]
}

export const initialMessageRegistry: MessageRegistryState = {

    resolvers: {},
    routeResolvers: []

}


export const MessageRegistryContext = createContext<State<MessageRegistryState>>(fallbackStateOver(initialMessageRegistry))
