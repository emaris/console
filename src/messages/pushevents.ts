import { PushEventSlot } from '#app/push/module';
import { useLogged } from '#app/user/store';
import { messageType } from './constants';
import { Message } from './model';
import { useMessageStore } from './store';


export type MessageEvent = {

    type: 'added' | 'updated'
    message: Message
}


export const usePushMessageSlot = (): PushEventSlot<MessageEvent> => {

    const messagestore = useMessageStore()

    const logged = useLogged()

    // all users listen for messages sent directly to their own tenant, including the "no tenant".
    const topics = [`${logged.tenant}.${messageType}`]

    // tenant users also listen for messages sent to all tenants.
    if (logged.isTenantUser())
        topics.push(`any.${messageType}`)

    return {

        onSubcribe: () => {

            console.log("subscribing for messages @", topics)

            return [{

                topics

                ,

                onEvent: (event: MessageEvent) => {

                    switch (event.type) {

                        case 'added': messagestore.addOne(event.message); break
                        case 'updated': messagestore.updateOne(event.message); break
                    }

                }

            }]
        }
    }
}