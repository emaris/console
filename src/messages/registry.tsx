
import { ReactNode, useContext } from 'react'
import { MessageRegistryContext } from './context'
import { Message, Topic } from './model'



// information to render a topic (typically in the context of displaying the message).
export type TopicInfo = {

    label: ReactNode
    tip?: ReactNode
    link?: string
}

// returns type infomation about a topic of a given type
export type TopicResolver = {

    type: string
    resolve: (_: Topic) => TopicInfo | undefined         // undefined if it cannot resolve the message
}


// information to route a message (typically to its most specific message flow).
export type RouteInfo = {

    route: string

}

//  may returns (a promise to try and return) information to route a message.
//  may look at the entire message (typically its topics) to decide if it can.
//  if it can, it may need to lookup information in order to build the route, and lookup may fail.
//  so it may still not be able to return a route in practice.
export type RouteResolver = {
   
    // undefined if it doen't now how to route the message, or it knows but it cannot in practice.
    routeFor: (_: Message) => Promise<RouteInfo | undefined > | undefined     
}




// collects and publishes topic and route resolve callbacks.
export const useMessageRegistry = () => {

    const registry = useContext(MessageRegistryContext)

    const self = {

        addResolvers : (resolvers: TopicResolver | TopicResolver[]) => { 
            
            const all = Array.isArray(resolvers) ? resolvers : [resolvers]

            registry.set( s => all.forEach(r => s.resolvers[r.type] = r))
            
            return self
        
        }
        
        ,

        lookupResolver: (type:string) => registry.get().resolvers[type]

        ,

        addRouteResolvers : (resolvers : RouteResolver | RouteResolver[]) => {
           
            const all = Array.isArray(resolvers) ? resolvers : [resolvers]

            registry.set(s => all.forEach(r => s.routeResolvers.push(r)))
            
            return self
        }
        

        , 

        routeResolvers: () => registry.get().routeResolvers
    }

    return self;


}
