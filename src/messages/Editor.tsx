import { Button } from '#app/components/Button'
import { useConfig } from '#app/config/state'
import { icns } from '#app/icons'
import { useT } from '#app/intl/api'
import { nameOf } from '#app/user/model'
import { useUserStore } from '#app/user/store'
import { useSharedRef } from "#app/utils/hooks"
import TextArea from 'antd/lib/input/TextArea'
import * as React from 'react'
import { MessageReferenceViewer } from './Message'
import { Message, MessageDynamics } from './model'

export type DynamicEditorProps = {

    replies: Message | undefined
    dynamics: MessageDynamics
    onChange: (_: MessageDynamics) => void
}

export type DynamicsEditor = React.FC<DynamicEditorProps>

const DefaultDynamicsEditor: DynamicsEditor = () => <React.Fragment />


export type Props = {


    author?: string
    splitWith?: string
    onSend: (_: string, __: MessageDynamics) => Promise<any>

    debug?: boolean

    postLabel?: string

    collapsed?: boolean
    collapsedMessage?: string

    replies: Message | undefined

    onReset: () => void

    readOnly?: boolean

    dynamicTopicSelector?: DynamicsEditor

}

const initialEditorState = { posting: false, text: undefined as string | undefined }

const emptyDynamics = { recipients: [], topics: [] }

export const MessageEditor = React.forwardRef((props: Props, ref: React.Ref<TextArea>) => {

    const config = useConfig()
    const users = useUserStore()

    const t = useT()

    const [state, setState] = React.useState(initialEditorState)

    const { posting, text } = state

    const sharedRef = useSharedRef<TextArea | null>(null, [ref])

    const { onSend, author, splitWith, postLabel = 'message.send', replies, collapsed = false, collapsedMessage, onReset, readOnly = false, dynamicTopicSelector } = props

    const placeholder = config.devmode() && splitWith ? `${t('message.placeholder_as', { user: nameOf(users.safeLookup(author)) })}` : t('message.placeholder')

    const [editorVisible, setEditorVisible] = React.useState(!collapsed || !!replies)

    const toggleEditor = () => collapsed && setEditorVisible(v => !v)
        
    const DynamicTopicSelector = dynamicTopicSelector! ?? DefaultDynamicsEditor

    const [dynamics, setDynamics] = React.useState<MessageDynamics>(emptyDynamics)

    React.useEffect(() => {

        if (collapsed && editorVisible)
            sharedRef?.current?.focus()


    }, [collapsed, editorVisible, sharedRef])

    const reset = () => {

        toggleEditor()
        setState(initialEditorState)
        setDynamics(emptyDynamics)
        onReset()

    }



    return replies || editorVisible ?

        <div className='message-editor message-editor-full'>

            <div className='message-editor-content'>

                {replies && <MessageReferenceViewer className='message-editor-reply' messageRef={replies}
                    actions={[
                        <Button key='clear' enabledOnReadOnly className='message-editor-reply-clear' type='ghost' noborder icn={icns.removeEntry} onClick={onReset} />
                    ]}
                />
                }

                <DynamicTopicSelector replies={replies} dynamics={dynamics} onChange={setDynamics} />

                <TextArea ref={sharedRef} readOnly={readOnly} disabled={readOnly} className='message-editor-input' autoSize placeholder={placeholder}
                    value={text} onChange={({ target: { value } }) => setState(s => ({ ...s, text: value }))} />


            </div>


            <div className='message-editor-buttons'>
                <Button disabled={text === undefined || text.length === 0 || posting || readOnly} enabledOnReadOnly type="primary"
                    icn={posting ? icns.loading(true) : icns.upload}
                    onClick={() => Promise.resolve(setState(s => ({ ...s, posting: true })))
                        .then(() => onSend(text!, dynamics))
                        .then(() => reset())}>

                    {t(postLabel)}

                </Button>
            </div>
        </div>

        :

        <div className='message-editor message-editor-placeholder' >
            <Button iconLeft type='primary' className='message-editor-edit-button' onClick={toggleEditor} icn={icns.edit} disabled={readOnly}>
                {collapsedMessage || t('message.expand_editor')}
            </Button>
        </div>

})