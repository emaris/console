import { Icon } from "antd"
import { AiFillMail, AiOutlineMail } from "react-icons/ai"



export const messageType = 'message'
export const messageIcon = <Icon component={AiOutlineMail} />
export const messageFullIcon = <Icon component={AiFillMail} />

export const messageSingular = "message.module.name_singular"
export const messagePlural = "message.module.name_plural"

export const messageapiroute = "/message"

export const allMessageMailTopic = 'message'
export const allMessageMailTemplate = 'usrmessage'

