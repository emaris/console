import { useCalls } from "#app/call/call";
import { messageapiroute } from "./constants";
import { Flow, Message, MessageDto } from "./model";



export type MessageCallFilter = {

    size?: number
    cursor?: string
    flow:Flow


}

export type MessageBatch = {

    messages: Message[]
    cursor: string
    
}



const messages = messageapiroute

export const useMessageCalls = () => {

    const calls = useCalls()

    const externFlow = (flow:Flow) => (

        {
        scope: flow.scope, 
        matchers: flow.channels.map(c=>
            ({topics:c.channel.read, mode: c.channel.mode})
        )}
            
    )

    const self = {

        getBatch : (filter:MessageCallFilter) => calls.at(`${messages}/search`).post<MessageBatch>(
            {

                ...externFlow(filter.flow),
                size:filter.size,
                cursor:filter.cursor
    
            }
        )
        
        ,


        markRead: (flow: Flow,  reader: string) => calls.at(`${messages}/read`).post<number>(

            {

                ...externFlow(flow),
                reader

            }
        )

        ,

        getUnreadCount: (flow: Flow,reader:string) => calls.at(`${messages}/count`).post<number>(

            {

                ...externFlow(flow),
                reader
    
            }
        )
        
        ,

        updateMessage : (message:Message) =>calls.at(`${messages}/${message.id}`).put<MessageDto>(message)
        
        ,

        postMessage: (message:Message) => calls.at(messages).post<MessageDto>(message)
    }

    return self

}