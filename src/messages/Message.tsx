import { Avatar, Icon, Tooltip } from 'antd'
import { Button } from '#app/components/Button'
import { Label } from '#app/components/Label'
import { icns } from '#app/icons'
import { useT } from '#app/intl/api'
import { Language } from '#app/intl/model'
import { useIntl } from '#app/intl/store'
import { useLocale } from '#app/model/hooks'
import { isMultilang } from '#app/model/multilang'
import { noTenant } from '#app/tenant/constants'
import { TenantLabel } from '#app/tenant/Label'
import { TimeLabel } from '#app/time/Label'
import { userType } from '#app/user/constants'
import { UserLabel } from '#app/user/Label'
import { useLogged, useUserStore } from '#app/user/store'
import moment from 'moment-timezone'
import * as React from 'react'
import { AiOutlineDoubleRight } from 'react-icons/ai'
import { Link } from 'react-router-dom'
import { messageIcon } from './constants'
import { FlowMessage, Message, MessageReference, MultilangContent, useMessageFlow, useMessageModel, useMessageTopics } from './model'

export type Props = {


    author: string | undefined
    message: FlowMessage

    colorOf: (tenant: string) => string | undefined
    onRead: (m: Message, flag: boolean) => void

    noReply?: boolean
    onReply: (m: FlowMessage) => void

    readOnly?: boolean

}


export const MessageViewer = (props: Props) => {

    const [loading, setLoading] = React.useState(false)

    const { currentLanguage } = useIntl()

    return <InnerMessageViewer lang={currentLanguage()} loading={loading} setLoading={setLoading} {...props} />
}


const InnerMessageViewer = React.memo((props: Props & {

    lang: Language
    setLoading: (_: boolean) => any
    loading: boolean

}) => {

    const { loading, setLoading, message, author, onRead, colorOf, noReply, onReply, readOnly = false } = props

    const t = useT()

    const logged = useLogged()
    const userstore = useUserStore()

    const { authorInfo, isRead } = useMessageModel()
    const { subtopics } = useMessageFlow()
    const { resolveTopic } = useMessageTopics()


    const contentOf = useMessageContent()

    
    const currentTenant = userstore.safeLookup(author).tenant

    const { content, date } = message

    const { type, user, tenant, tenantId, tenantName, icon, } = authorInfo(message)

    const read = isRead(message, currentTenant)

    // helper: wraps a callback to control loading state
    const loadwrap = <S extends any, P extends any[]>(fun: (...args: P) => S) => (...ps: P) =>

        Promise.resolve(setLoading(true))
            .then(() => fun(...ps))
            .finally(() => setLoading(false))


    const loadingOr = icon => loading ? icns.loading(true) : icon
    const loadingAndClass = (other: string = '') => `message-action ${loading ? 'action-loading' : other}`


    const avatar = type === userType ?

        <Tooltip title={<TenantLabel tenant={tenant} />}>
            <Avatar style={colorOf(user!.tenant) ? { background: colorOf(user!.tenant) } : undefined}
                className={`message-avatar message-avatar-user`} >{tenantName[0] ?? '?'}</Avatar>
        </Tooltip>
        :

        <Tooltip title={<Label icon={icon} title={tenantName} />}>
            <Avatar icon={icon} className={`message-avatar message-avatar-${type}`} />
        </Tooltip>


    const unreadtag = !read && <Label className="unread-tag" noIcon mode='tag' title={t("message.unread")} />
    const authortag = <span className="message-author">{message.author ? user?.tenant === noTenant && logged.isTenantUser() ? t('user.notenant_name') : <UserLabel noIcon user={user?.username} /> : t('message.system_author')}</span>
    const datelbl = <TimeLabel noIcon noTip className="message-date" value={moment(new Date(date))} />


    const topics = subtopics(message).map(topic => {

        const resolved = resolveTopic(topic)

        return resolved ?

            <span className="message-topic" key={topic.name}>
                {
                    resolved.tip ?

                        <Label noIcon title={resolved.label} decorations={resolved.link ? [

                            <Tooltip overlayClassName='topic-tip-overlay' title={
                                <div style={{ display: 'flex', alignItems: 'center' }}>{t('message.topic_tip_prefix')}&nbsp;{resolved.tip}</div>
                            }>
                                <Link to={resolved.link}>{messageIcon}</Link>
                            </Tooltip>

                        ] : []} />

                        :

                        resolved.label
                }
            </span>
            : null
    })

    const readAction = tenantId === currentTenant ? null :

        message.readBy.includes(currentTenant) ?

            <Button disabled={readOnly} className={loadingAndClass('action-unread')} onClick={() => loadwrap(onRead)(message, false)} noborder enabledOnReadOnly iconLeft icn={loadingOr(icns.dot)} size="small">{t('message.mark_unread')}</Button>
            :
            <Button disabled={readOnly} className={loadingAndClass()} onClick={() => loadwrap(onRead)(message, true)} noborder enabledOnReadOnly iconLeft icn={loadingOr(icns.dot)} size="small">{t('message.mark_read')}</Button>


    const replyAction = noReply ? null :

        <Button disabled={readOnly} className='message-action' onClick={() => onReply(message)} noborder enabledOnReadOnly iconLeft icn={<Icon component={AiOutlineDoubleRight} />} size="small">{t('message.reply')}</Button>

    const ownClass = tenantId === currentTenant ? 'own-message' : 'other-message'
    const readClass = read ? 'read-message' : 'unread-message'
    const withReplyClass = message.replies ? 'message-with-reply' : ''

    return <div className={`flow-message ${ownClass} ${withReplyClass} ${readClass}`}>

        <div className="message-sidebar">
            {avatar}
        </div>

        <div className="message-main">

            {topics.length > 0 && <div className='message-topics'>
                {unreadtag}
                {topics}
            </div>}

            <div className="message-topbar">
                {topics.length === 0 && unreadtag}
                {authortag}
                {datelbl}
            </div>

            <div className={`message-content ${topics.length > 0 ? 'content-with-topics' : ''}`}>
                {message.replies && <MessageReferenceViewer className='message-content-reply' messageRef={message.replies} />}
                {contentOf(content)}
            </div>

            <div className="message-bottombar">
                {readAction}
                {replyAction}
            </div>
        </div>

    </div>
}
    , ($, $$) => $.message === $$.message && $.lang === $$.lang && $.loading === $$.loading)


type ReferenceProps = {

    className?: string
    messageRef: MessageReference

    actions?: React.ReactNode[]

}

export const MessageReferenceViewer = (props: ReferenceProps) => {

    const t = useT()

    const { className = '', messageRef, actions } = props

    const model = useMessageModel()
  
    const { user } = model.authorInfo(messageRef)

    const { author, content } = messageRef

    const contentOf = useMessageContent()

    return <div className={`message-reply ${className}`}>
        <div className='message-reply-main'>
            <div className='message-reply-author'>
                <span className="message-author">{author ? <UserLabel user={user?.username} noOptions noDecorations /> : t('message.system_author')}</span>
            </div>
            <div className='message-reply-content'>{contentOf(content)}</div>
        </div>
        {actions && <div className="message-reply-actions">{actions}</div>
        }
    </div>
}


const useMessageContent = () => {

    const t = useT()
    const { l } = useLocale()

    // trasnlate params using embedded multilang or references to other keys.
    const translatedParams = (content: MultilangContent) => Object.entries(content.parameters ?? {}).reduce((acc, [name, val]) => {

        if (typeof val === 'string')
            return ({ ...acc, [name]: val })

        const { key, params = {}, options = {} } = isMultilang(val) ? { key: l(val) } : val

        return ({ ...acc, [name]: t(key, { ...options, ...params }) })

    }, {})

    return content => typeof content === 'string' ? content : t(content.keys, translatedParams(content))

}