import { BackwardInfiniteList, BackwardInfiniteListApi } from '#app/components/BackwardInfiniteList'
import { Button } from '#app/components/Button'
import { useT } from '#app/intl/api'
import { tenantType } from '#app/tenant/constants'
import { User } from '#app/user/model'
import { useLogged } from '#app/user/store'
import { useAsyncRunner } from '#app/utils/asyncrunner'
import { randomColor, through } from '#app/utils/common'
import { anyPartyTopic } from '#campaign/constants'
import { Tooltip } from 'antd'
import TextArea from 'antd/lib/input/TextArea'
import * as React from 'react'
import { MdOutlineMoreVert } from 'react-icons/md'
import { DynamicsEditor, MessageEditor } from './Editor'
import { MessageViewer } from './Message'
import { messageIcon, messagePlural } from './constants'
import { Flow, FlowMessage, Message, MessageDynamics, Topic, newMessage, refOf, useMessageFlow, useMessageModel } from './model'
import { useMessageStore } from './store'
import "./styles.scss"




export const useFlowCount = (id: string, flow: Flow) => {

    const [count, setCount] = React.useState(0)

    const store = useMessageStore()

    // runs on all state changes, but will read local flowstate where possible.
    // marking messages read/unread and also message notifications will trigger changes to the local flowstate.

    //eslint-disable-next-line
    React.useEffect(() => {

        store.fetchUnreadNotifications(id, flow).then(setCount)

    })


    return count;
}

export type Props = {

    // univocally identifies this flow among any other (only the client knows how).
    id: string

    //  the specification for this flow.
    flow: Flow

    // the username to use as the author of new messages and replies, it's the current user by default.
    author?: User

    // set to a tenant id, splits the view at dev time, showing at the bottom the flow from the perspective of a random user of the tenant.
    splitWith?: string

    // if the flow spans messages authored by more than one party.
    multiTenant?: boolean

    // the number of messages to fetch at a time.
    batchSize?: number

    // if it should display more logs.
    debug?: boolean

    // a custom message for the Send button.
    postLabel?: string

    collapsed?: boolean

    collapsedMessage?: string

    readOnly?: boolean

    dynamicsEditor?: DynamicsEditor

    filterMessageBy?: (_: FlowMessage) => boolean

    filterTopicBy?: (_: Topic) => boolean
}




const initialData = {

    // a token to request the next batch from the backend. 
    cursor: undefined as string | undefined,

    // the messages downloaded or posted so far.
    messages: undefined! as FlowMessage[]


}


export const recipientSeparator = ':'
const defaultBatchSize = 5

export const FlowViewer = (props: Props) => {


    const t = useT()

    const logged = useLogged()

    const model = useMessageModel()
    const store = useMessageStore()

    const { fetchBatch, markOneRead, fetchUnreadNotifications, allNotificationsMatching, postOne, mailOne, openFlow, closeFlow, updateOne } = store

    const { intern } = useMessageFlow()

    const { id, flow, debug, author = logged, multiTenant, batchSize = defaultBatchSize, collapsed = false, collapsedMessage, readOnly = false, filterMessageBy = (_) => true, filterTopicBy = () => true, dynamicsEditor } = props


    // the oveall state of this component.
    const [data, setData] = React.useState(initialData)

    const [reply, setReply] = React.useState<FlowMessage | undefined>()

    const api = React.useRef<BackwardInfiniteListApi>()

    const editor = React.useRef<TextArea>(null)

    // the of date of the most recent message in the first batch downloaded from the backend.
    // serves as a threshold to identify message notifications received after downloading the first batch.
    // use the current date until we have the first batch, so that we don't start tracking messages
    // that we're about to download.
    const mostRecentDownload = React.useRef<string>(new Date().toISOString())

    // records the function to close the flow with the latest state (where the flow is open), 
    // so that it can be used in the cleanup function registered on mount (when the flow is not open yet).
    const closeFlowRef = React.useRef<() => void>(undefined!)


    // signal flow is open on mount and register cleanup to close it.
    React.useEffect(() => {

        openFlow(id, flow)

        return () => closeFlowRef.current()   // uses reference to close function that work with state at the time of unmounting, not mounting.


        // eslint-disable-next-line
    }, [])

    // updates reference to latest close function.
    React.useEffect(() => {

        closeFlowRef.current = () => closeFlow(id)

    })

    const unreadCount = useFlowCount(id, flow)

    const { waitFor, PulsingDotIcon } = useAsyncRunner()


    const loadNext = () =>

        // fetch next batch
        fetchBatch({ size: batchSize, cursor: data.cursor, flow })

            .then(({ cursor, messages }) => ({ cursor, messages: messages.map(m => intern(m, flow)).filter(filterMessageBy) }))

            // store messages and new cursor
            .then(through(({ cursor, messages }) => setData(d => ({ ...d, cursor, messages: [...messages, ...data.messages ?? []] }))))

            // if it's the first batch and we have some data, then record date of most recent message.
            .then(({ messages }) => {

                if (data.messages === undefined && messages[0]?.date)  // first batch: no messages yet at this render
                    mostRecentDownload.current = new Date(messages[0].date).toISOString()

                // messages must be in list order.
                return messages.reverse()
            })





    const recentFilter = (m: Message) => !mostRecentDownload.current || m.date > mostRecentDownload.current


    // all matching messages notified after the download of the first batch.
    const flowFilter = { author: author.username }
    const notifications = allNotificationsMatching(id, flowFilter).filter(m => recentFilter(m)).map(m => intern(m, flow)).filter(filterMessageBy)


    // merge old and new messages, then sort from oldest to newest.
    // (sorting is required because new messages in global state and posts in local state may interleave.
    //  one could optimise, separate downloads from updates, keep downloads ordered batch by batch, 
    //  and limit sorting to updates. added complexity is likely unjustified at this stage.)
    const messages = [...data.messages ?? [], ...notifications].sort(model.ascending)


    const postMessage = (text: string, dynamics: MessageDynamics) => {

        // note: if required, this must evolve to allow posting to any channel. user needs to select it then.
        const target = reply ? reply.channel : flow.channels.find(c => !c.channel.noPost)?.channel

        if (!target)
            throw Error("cannot post to flow " + JSON.stringify(flow))

        // use the target channel topics, or if we're reply with the original topics, and finally fallbackl to the read topics.
        let topics = [...target.write ?? (reply ? reply.topics : target.read), ...dynamics.topics ?? []]

        
        // hack: elimates any topic if there are more specific ones
        if (dynamics.topics.find(t => t.type === tenantType && t.name !== anyPartyTopic.name))
            topics = topics.filter(t => t.name !== anyPartyTopic.name)
        

        const recipient = dynamics.recipients.length ? dynamics.recipients.join(recipientSeparator) : flow.recipient ?? reply?.recipient

        let message = { ...newMessage(text, topics, author), recipient, scope: flow.scope, replies: refOf(reply) } as Message

        if (target.onReply)
            message = target.onReply(message)

        return postOne(message)

            // if this is a reply of an unread message, the mark it read first.
            .then(m => reply && !reply.readBy.includes(author.tenant) ? readMessage(reply, true).then(() => m) : m)
            .then(m => intern(m, flow))
            // scrolls before appending so that the list keeps itself scrolled.
            .then(through(() => api.current?.scroll()))
            .then(m => setData(d => ({ ...d, messages: [...d.messages ?? [], m] })))

            // sends an email if it finds a mail profile on the channel or flow.
            .finally(() => {

                try {

                    const profile = target?.mailProfile?.(message) ?? flow.mailProfile?.(message)

                    if (!profile)   // this may also be intentional: no mail for this message.
                        return

                    mailOne(message, author, profile)

                }
                catch (error) {
                    console.warn(`can't send email for message ${message}:${error}`)
                }


            })



        //  note: posting a message should automatically scroll the list down if it isn't yet.
        //  this is a problem with the virtual list, because we don't know the size of the new item until we've scrolled down and rendered it a first time.
        //  TODO: somehow, find a way to preview these items exactly as those that are downloaded.

    }



    const readMessage = (m: Message, value: boolean) => {

        const mark = markOneRead(m, value, author.tenant)

        return data.messages.find(mm => mm.id === m.id) ?

            mark
                .then(read => setData(d => ({ ...d, messages: d.messages.map(mm => mm.id === read.id ? intern(read, flow) : mm) })))
                .then(() => fetchUnreadNotifications(id, flow, true))

            :

            mark.then(updateOne)

    }

    const colorOf = useTenantColours(multiTenant)

    const replyMessage = (m: FlowMessage) => Promise.resolve(setReply(m)).then(() => editor.current?.focus()).then(() => api.current?.scroll())

    if (debug)
        console.log("messages", data.messages?.length, "cursor", data.cursor)


    const [actionActive, setActionActive] = React.useState(false)

    const readAll = async () => {

        await store.markAllRead(id, flow)

        setData(d => ({ ...d, messages: d.messages.map(m => m.readBy.includes(logged.tenant) ? m : { ...m, readBy: [...m.readBy ?? [], logged.tenant] }) }))

    }

    const canEdit = flow.channels.some(c => !c.channel.noPost || !c.channel.noReply)

    return <div className='flow-viewer'>


        <div className='messages'>

            <BackwardInfiniteList data={messages} apiRef={a => api.current = a}
                debug={debug}
                itemIcon={messageIcon}
                loadingPlaceholder={t(messagePlural)}
                itemKey={m => m.id}
                hasNext={data.messages === undefined || !!data.cursor}
                loadNext={loadNext}
                renderItem={m =>

                    <MessageViewer author={author.username} message={{ ...m, topics: m.topics.filter(filterTopicBy) }}
                        onRead={readMessage}
                        noReply={m.channel?.noReply} onReply={replyMessage}
                        colorOf={colorOf} readOnly={readOnly} />
                }

            />
        </div>




        {canEdit &&

            <React.Fragment>
                <div className={`message-actions ${actionActive ? 'active' : ''}`} >

                    <Button enabledOnReadOnly title={t('message.action_read_all')} disabled={!unreadCount} iconLeft icn={<PulsingDotIcon />} noborder size='small' type='ghost' onClick={() => waitFor(readAll)()}>
                        {t('message.action_read_all')}
                    </Button>


                </div>



                <div className='message-bottom-bar'>



                    <MessageEditor key={reply?.id} ref={editor}
                        onSend={postMessage}
                        postLabel={props.postLabel}
                        replies={reply}
                        collapsed={collapsed}
                        onReset={() => setReply(undefined)}
                        collapsedMessage={collapsedMessage}
                        readOnly={readOnly}
                        dynamicTopicSelector={dynamicsEditor} />


                    <div className='message-action-trigger'>
                        <Tooltip mouseEnterDelay={1} placement='topLeft' title={t('message.action_trigger_tooltip')}>
                            <Button enabledOnReadOnly iconLeft icn={<MdOutlineMoreVert size={18} />} type={actionActive ? 'primary' : 'default'} onClick={() => setActionActive(s => !s)} />
                        </Tooltip>
                    </div>

                </div>

            </React.Fragment>
        }

    </div>
}

//  hook helper: associates tenants with given random colors for the lifecycle of the client.
const useTenantColours = (multiTenant?: boolean) => {

    const colors = React.useRef<Record<string, string>>({})

    const colorOf = React.useCallback((tenant: string) => {

        if (!multiTenant)
            return undefined;

        if (!colors.current[tenant])
            colors.current[tenant] = randomColor()

        return colors.current[tenant]


    }, [multiTenant])

    return colorOf
}

