import { icns } from "#app/icons"
import { useT } from '#app/intl/api'
import { TParams } from '#app/mail/model'
import { MultilangDto, useL } from "#app/model/multilang"
import { systemType } from "#app/system/constants"
import { noTenant, tenantIcon } from "#app/tenant/constants"
import { Tenant } from "#app/tenant/model"
import { useTenantStore } from '#app/tenant/store'
import { userType } from "#app/user/constants"
import { User } from "#app/user/model"
import { useLogged, useUserStore } from '#app/user/store'
import { compareDates } from '#app/utils/common'
import { useMessageRegistry } from './registry'

export type MessageDto = {

    id: string
    // broad grouping of messages for query partioning, typically the campaign.
    scope?: string

    // what the message is about.        
    topics: Topic[]

    date: string
    
    // if missing, the sender is the system.
    author?: string

    // the client session from which the message originates 
    session?: string

    // the default target or targets (colon-separated) for outgoing message notifications.
    // the backend will look at it to determine where to send notifications on the event bus.
    recipient?: string


    // thread summary
    replies?: MessageReference


    // list of parties that have read/replied/processed the message.
    readBy: string[]


    content: MessageContent

    
    
}

// user-to-user messages are in the author's language, system messages adapt to the language of the reader.
export type MessageContent = string | MultilangContent

// system messages are translatable:
// 1. keys identify the text with decreasing priority.
// 2. parameters are injected into the text and can be pre-translated content in multiple language or parameters of the translation function.
export type MultilangContent = {

    keys: string[]
    parameters:  Record<string,string | MultilangDto | Record<string,any>>

}

export type Message = MessageDto        // they may diverge later

export type MessageReference = Pick<Message, 'id' | 'author' | 'content'>

export const refOf = (m: Message | undefined): MessageReference | undefined => m ? ({ id: m.id, author: m.author, content: m.content }) : undefined

export const newMessage = (content: string, topics: Topic[], author?: User): Message => {

    return {

        id: undefined!,
        date: undefined!,
        author: author ? author.username : undefined,  // no author => system message.
        content,
        topics,
        readBy: [author ? author.tenant : noTenant]  // system messages are implicitly read by the coordinator.

    }

}



export const useMessageModel = () => {

    const t = useT()
    const l = useL()

    const logged = useLogged()
    const userstore = useUserStore()
    const tenants = useTenantStore()

    const self = {

        authorInfo: (m: MessageReference) => {

            if (!m.author)
                return {type:systemType,tenantName:t('message.system_author'),icon: icns.system}

            const user = userstore.lookup(m.author)
            const tenantId = user?.tenant ?? noTenant
            const tenant = user ? tenantId === noTenant ? noTenant : tenants.safeLookup(tenantId) : noTenant
            const icon = tenant === noTenant ? icns.admin : tenantIcon

            const type = tenant === noTenant ? 'none' : userType
            const tenantName = type === userType ? l((tenant as Tenant).name) : t('message.notenant_author')


            return { type, user, tenantId, tenantName, tenant, icon }

        }

        ,

        isRead: (m: Message, target) => target === self.authorInfo(m).tenantId || m.readBy.includes(target ?? logged.tenant)

        ,

        // id encodes date and author.
        ascending: (m1: Message, m2: Message) => compareDates(m1.date, m2.date)

    }

    return self
}


export type Topic = {

    name: string
    type: string

}


export const messageTopicSeparator = ":"

export const useMessageTopics = () => {

    const registry = useMessageRegistry()

    const self = {


        // splits a set of topics depending on whetehr they match one of another set of topics.
        partitionTopics: (topics: Topic[], other: Topic[]) => {

            let matched = [] as Topic[]
            let extra = [] as Topic[]

            topics.forEach(t => (other.some(ot => ot.name === t.name && ot.type === t.type) ? matched : extra).push(t))

            return { matched, extra }

        }

        ,

        resolveTopic: (topic: Topic) =>  registry.lookupResolver(topic.type)?.resolve(topic)

        

    }

    return self;

}


export type ChannelMode = 'lax' | 'strict'


// stream of messages about given topics.
export type Channel = {

    // topics messages must match.
    read: Topic[]
    // mode to match read topics: at least all (lax) vs. exactly all (strict).
    mode?: ChannelMode

    // disallows sending new posts.
    noPost?: boolean
    // disallows replying to existing posts.
    noReply?: boolean
   
    // topics for outgoing posts, new or replies.
    // by default new posts have the read topics.
    // by default, replies have the topics of the original message.
    write?: Topic[]


    
    onReply?: (message_: Message) => Message

    mailProfile?: (message: Message) => FlowMailProfile


}


// names and aggregates multiple channels to capture different rules for inbound and outboung messages.
export type Flow = {

    // broad partitioning of messages to aid retrieval performance at the backend.
    // will be set to campaign id.
    scope?: string


    // the default recipient of new posts.
    recipient?: string,

    // if no channel allow posting, the flow is readonly.
    // if multiple channels allow posting, a channel must be chosen. 
    channels: {

        name: string
        channel: Channel

    }[]

    mailProfile?: (message: Message) => FlowMailProfile

   

}

// bridges between message properties and email properties.
export type FlowMailProfile = {

    targets: string[]               // who should the message be mailed to.
    parameters: TParams             // what parameters should the mail message contain.
}


export const useMessageChannel = () => {

    const topics = useMessageTopics()

    const self = {


        // a message matches a channel if it has all the read topics of the channel.
        // the match is 'strict' if it has only and only those topics. 
        matches: (m: Message, { read, mode = 'lax' }: Channel) => {

            const { matched, extra } = topics.partitionTopics(m.topics, read)

            return matched.length === read.length && (mode === 'lax' || extra.length === 0)

        }
    }

    return self;

}



export type FlowMessage = Message & {

    channel: Channel

}


// message properties that can be set per message
export type MessageDynamics = {
    recipients:string[], 
    topics: Topic[]
}





export const useMessageFlow = () => {

    const messages =  useMessageTopics()
    const channel = useMessageChannel()

    const self = {

        matches: (m: Message, flow: Flow | undefined) => flow?.channels.some(c => channel.matches(m, c.channel))

        ,

        intern: (m: Message, flow: Flow): FlowMessage =>( { ...m, channel: flow.channels.map(c => c.channel).find(c => channel.matches(m, c))! })

        ,

        subtopics: (m: FlowMessage) => messages.partitionTopics(m.topics, m.channel.read).extra
    
    }

    return self;

}

