import { useT } from '#app/intl/api';
import { useMail } from '#app/mail/api';
import { TemplatedMailContent } from '#app/mail/model';
import { noTenant } from '#app/tenant/constants';
import { nameOf, User } from '#app/user/model';
import { useLogged, useUserStore } from '#app/user/store';
import { wait } from "#app/utils/common";
import { useFeedback } from '#app/utils/feedback';
import { Flow, FlowMailProfile, Message, MessageContent, useMessageFlow, useMessageModel } from "#messages/model";
import { useClientSession } from 'apprise-frontend-core/client/session';
import { useContext } from 'react';
import { MessageCallFilter, useMessageCalls } from "./calls";
import { allMessageMailTemplate, allMessageMailTopic, messageIcon } from "./constants";
import { MessageContext } from './context';
import { useMessageRegistry } from './registry';





export type MessageFilter = {


    author: string
    scope: 'all' | 'mine' | 'other'
    unreadBy: string


}



export const useMessageStore = () => {

    const state = useContext(MessageContext)

    const t = useT()

    const logged = useLogged()
    const fb = useFeedback()

    const userstore = useUserStore()

    const registry = useMessageRegistry()
    const calls = useMessageCalls()
    const model = useMessageModel()
    const flow = useMessageFlow()

    const mail = useMail()

    const session = useClientSession()

    const self = {


        fetchBatch: (filter: MessageCallFilter) => Promise.resolve(console.log("fetching messages...")).then(wait(100)).then(() => calls.getBatch(filter))

        ,

        markOneRead: (m: Message, value: boolean, t: string) => {

            const loggedTenant = t ?? logged.tenant

            return calls.updateMessage({ ...m, readBy: [...(m.readBy ?? []).filter(t => t !== loggedTenant), ...value ? [loggedTenant] : []] })
                .then(wait(100))

        }
        ,

        postOne: (m: Message) => Promise.resolve(console.log(`posting message from ${m.author ?? 'the system'}...`))
            .then(() => calls.postMessage(m))

        ,

        mailOne: (message: Message, author: User, profile: FlowMailProfile) => {

            // adapts message content to mail content, which is slightly different...
            const adapt = ($: MessageContent) => typeof $ === 'string' ? $ : { key: $.keys[0], params: $.parameters }

            const content = {

                template: allMessageMailTemplate,
                parameters: {

                    content: adapt(message.content),
                    author: nameOf(author),
                    authorTenant: author.tenant,

                    ...profile.parameters
                }
            } as TemplatedMailContent

            if (message.replies)
                content.parameters.reply = adapt(message.replies.content)

            mail.sendTo(profile.targets).content(content).onTopic(allMessageMailTopic)

        }

        ,

        addOne: async (message: Message) => {

            const authorInfo = model.authorInfo(message)

            console.log(`received message from ${message.author} (session ${session.get() ?? 'none'})`, message)

            // dispatch message to active matching message flows.
            state.set(s => {

                Object.values(s.messages.all).forEach(f => {

                    if (flow.matches(message, f.flow)) {

                        // 1. add message to active flow
                        if (f.active)
                            f.notifications.push(message)

                        // 2. increase unread count if message is not from logged tenant.
                        if (!message.readBy.includes(logged.tenant))
                            f.unreadCount++
                    }

                })
            })

            let route

            try {

                route = await registry.routeResolvers().map(r => r.routeFor(message)).find(r => r !== undefined)

                if (!route)
                    return
    
                console.log(`routing message notification to ${route.route}`)
    
                fb.toast(t('message.notify_new',
                    { sender: authorInfo.tenantName }),
                    {
                        icon: messageIcon,
                        duration: 4000,
                        route: route.route
                    })    

            }
            finally {
                
                if (!route)
                    console.log('silencing unroutable message notification')
                
            }
        },

        updateOne: (message: Message) => {

            const author = userstore.lookup(message.author)
            const authorTenant = author?.tenant ?? noTenant
            const loggedTenant = logged.tenant

            console.log(`received message update from ${message.author}`, message)

            Object.keys(state.get().messages.all).forEach(id => {

                const f = state.get().messages.all[id]

                if (flow.matches(message, f.flow)) {

                    if (f.active) {

                        const match = f.notifications?.findIndex(m => m.id === message.id)

                        state.set(s => {

                            const f = s.messages.all[id]

                            // if we have seen the original message, replace it. otherwise append it.
                            if (match >= 0)
                                f.notifications[match] = message
                          
                            // if we add an update about a message sent from an active flow then we're going to re-add it twice to the flow.
                            // else
                            //     f.notifications.push(message)
                        })
                    }

                    // decrease/increase unread count if message is not from logged tenant but has/has not been read by it.
                    if (message.author && authorTenant !== loggedTenant)
                        self.fetchUnreadNotifications(id, f.flow, true)




                }

            })


            // Object.keys(s.messages.all).forEach(id => {

            //     const f = s.messages.all[id]

            //     if (matches(message, f.flow)) {

            //        console.log("author tenant",authorTenant)

            //         // decrease/increase unread count if message is not from logged tenant but has/has not been read by it.
            //         if (message.author && authorTenant !== loggedTenant)
            //             self.fetchUnreadNotifications(id, f.flow).then(count => s.changeWith(s => s.messages.all[id].unreadCount = count) )

            //     }
            // })
        },


        allFlows: () => state.get().messages.all

        ,

        lookupFlow: (id: string) => self.allFlows()[id]

        ,

        allNotificationsMatching: (id: string, filter: Partial<MessageFilter>) => {

            const { unreadBy } = filter ?? {}

            return (self.lookupFlow(id)?.notifications ?? []).filter(m =>

                (unreadBy === undefined || (!m.readBy.includes(unreadBy)))

            )

        }

        ,


        markAllRead: async (id: string, flow: Flow) => {

            await calls.markRead(flow, logged.tenant)

            state.set(s =>  s.messages.all[id] = {

                ...s.messages.all[id],

                notifications:
                s.messages.all[id]?.notifications?.map(m =>

                        m.readBy.includes(logged.tenant) ? m : { ...m, readBy: [...m.readBy ?? [], logged.tenant] }


                    ) ?? [],
                unreadCount: 0
            })


        }

        ,

        fetchUnreadNotifications: async (id: string, flow: Flow, force?: boolean): Promise<number> => {

            const flowstate = self.lookupFlow(id)

            if (!force && flowstate?.unreadCount !== undefined)
                return flowstate.unreadCount

            //console.log('fetching unread count for flow', JSON.stringify(flow))

            const count = await calls.getUnreadCount(flow, logged.tenant)

            state.set(s => s.messages.all[id] = { ...s.messages.all[id] ?? {}, unreadCount: count })

            return count

        }

        ,


        openFlow: (id: string, flow: Flow) => {

            state.set(s => s.messages.all[id] = { ...s.messages.all[id] ?? {}, flow, active: true, notifications: [] })
        }

        ,

        closeFlow: (id: string) => {

            // keep track to preserve after closing.
            const flowstate = self.lookupFlow(id)

            // do nothing if there's no flow to close.
            if (!flowstate)
                return

            state.set(s => delete s.messages.all[id])


        }

    }

    return self;
}


