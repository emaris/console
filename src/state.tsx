
import { ConfigContext, ConfigState, initialConfig } from '#app/config/context'
import { IntlContext, IntlState, initialIntl } from '#app/intl/context'
import { ModuleRegistryContext, ModuleRegistryState, initialModuleRegistry } from '#app/module/context'
import { PushEventState, PushStateContext, initialPushEventState } from '#app/push/context'
import { SettingsContext, initialSettings } from '#app/settings/context'
import { SettingsState } from '#app/settings/store'
import { SystemContext } from '#app/system/context'
import { SystemState, initialSystemState } from '#app/system/state'
import { TagContext, TagState, initialTags } from '#app/tag/context'
import { TenantContext, TenantState, initialTenants } from '#app/tenant/context'
import { useIAMSettings } from '#app/tenant/settings'
import { TimeContext } from '#app/time/context'
import { TimeState, initialTime } from '#app/time/state'
import { UserContext, UserState, initialUsers } from '#app/user/context'
import { useLogged } from '#app/user/store'
import { CampaignContext, CampaignState, initialCampaigns } from '#campaign/context'
import { EventContext, EventState, initialEvents } from '#event/context'
import { LayoutRegistryContext, LayoutRegistryState, initialLayoutRegistry } from '#layout/context'
import { MessageContext, MessageRegistryContext, MessageRegistryState, MessageState, initialMessageRegistry, initialMessages } from '#messages/context'
import { ProductContext, ProductState, initialProducts } from '#product/context'
import { RequirementContext, RequirementState, initialRequirements } from '#requirement/context'
import { useMode } from 'apprise-frontend-core/config/api'
import { StateProvider } from 'apprise-frontend-core/state/provider'
import { ToggleContext, ToggleState } from 'apprise-frontend-core/toggle/context'
import { ToggleProps } from 'apprise-frontend-core/toggle/toggles'
import { PropsWithChildren, useContext } from 'react'


export type AppState = {

    modules: ModuleRegistryState
    intl: IntlState
    config: ConfigState,
    toggles: ToggleState,
    system: SystemState,
    settings: SettingsState,
    time: TimeState,
    push: PushEventState,
    tags: TagState,
    tenants: TenantState,
    users: UserState
    requirements: RequirementState
    products: ProductState
    events: EventState
    campaigns: CampaignState
    messageRegistry: MessageRegistryState
    messages: MessageState,
    layout: LayoutRegistryState
}

export const AppState = (props: PropsWithChildren<{ value: AppState } >) => {

    const { value, children } = props

    return <StateProvider initialState={value.modules} context={ModuleRegistryContext} >
        <StateProvider initialState={value.intl} context={IntlContext}>
            <StateProvider initialState={value.config} context={ConfigContext} >
                <StateProvider initialState={value.toggles} context={ToggleContext}>
                    <StateProvider initialState={value.system} context={SystemContext}>
                        <StateProvider initialState={value.settings} context={SettingsContext}>
                            <StateProvider initialState={value.time} context={TimeContext}>
                                <StateProvider initialState={value.push} context={PushStateContext}>
                                    <StateProvider initialState={value.tags} context={TagContext}>
                                        <StateProvider initialState={value.tenants} context={TenantContext}>
                                            <StateProvider initialState={value.users} context={UserContext}>
                                                <StateProvider initialState={value.requirements} context={RequirementContext}>
                                                    <StateProvider initialState={value.products} context={ProductContext}>
                                                        <StateProvider initialState={value.events} context={EventContext}>
                                                            <StateProvider initialState={value.messageRegistry} context={MessageRegistryContext}>
                                                                <StateProvider initialState={initialMessages} context={MessageContext}>
                                                                    <StateProvider initialState={value.campaigns} context={CampaignContext}>
                                                                        {children}
                                                                    </StateProvider>
                                                                </StateProvider>
                                                            </StateProvider>
                                                        </StateProvider>
                                                    </StateProvider>
                                                </StateProvider>
                                            </StateProvider>
                                        </StateProvider>
                                    </StateProvider>
                                </StateProvider>
                            </StateProvider>
                        </StateProvider>
                    </StateProvider>
                </StateProvider>
            </StateProvider>
        </StateProvider >
    </StateProvider>
}


export const useCurrentAppState = (): AppState => {

    return {
        modules: useContext(ModuleRegistryContext).get(),
        intl: useContext(IntlContext).get(),
        config: useContext(ConfigContext).get(),
        toggles: useContext(ToggleContext).get(),
        system: useContext(SystemContext).get(),
        settings: useContext(SettingsContext).get(),
        time: useContext(TimeContext).get(),
        push: useContext(PushStateContext).get(),
        tags: useContext(TagContext).get(),
        tenants: useContext(TenantContext).get(),
        users: useContext(UserContext).get(),
        requirements: useContext(RequirementContext).get(),
        products: useContext(ProductContext).get(),
        events: useContext(EventContext).get(),
        campaigns: useContext(CampaignContext).get(),
        messageRegistry: useContext(MessageRegistryContext).get(),
        messages: useContext(MessageContext).get(),
        layout: useContext(LayoutRegistryContext).get()
    }
}

export const useIsToggleActiveForBetaTester = () => {

    const mode = useMode()

    const logged = useLogged()

    const { betaTesters } = useIAMSettings()

    // logged user must be betatester, or if there are none env must be dev/staging.
    // the 'pass-through` works as a default for envs where we test all features using any users: we can leave it empty there.
    // but it still allows for testing the feature toggle: we configure some users and see the feature disappear for others.
    return betaTesters?.length ? betaTesters.includes(logged.username) : !mode.production
}

export const initialState = (props: ToggleProps): AppState => {

    return {
        modules: initialModuleRegistry,
        intl: initialIntl,
        config: initialConfig,
        toggles: { toggles: props.toggles ?? [], isActive: useIsToggleActiveForBetaTester },
        system: initialSystemState,
        settings: initialSettings,
        time: initialTime,
        push: initialPushEventState,
        tags: initialTags,
        tenants: initialTenants,
        users: initialUsers,
        requirements: initialRequirements,
        products: initialProducts,
        events: initialEvents,
        campaigns: initialCampaigns,
        messageRegistry: initialMessageRegistry,
        messages: initialMessages,
        layout: initialLayoutRegistry
    }
}