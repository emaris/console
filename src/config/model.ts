import { BaseConfig, withBaseConfig } from "#app/config/model"
import { useLogged } from '#app/user/store'
import { campaignType } from '#campaign/constants'
import { submissionType } from '#campaign/submission/constants'
import { eventType } from '#event/constants'
import { productType } from '#product/constants'
import { requirementType } from '#requirement/constants'

export type Config = BaseConfig & {

    someprop : string
}


export const withConfig =  (self:Config) => ({

    ...withBaseConfig(self),

    // todo

})


export const useRoutedTypes = () => {

    const logged = useLogged()

    return logged.hasNoTenant() ?  [requirementType,productType,eventType, campaignType, submissionType] : [submissionType]
     
      
      
}